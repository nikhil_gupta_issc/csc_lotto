<?php
	$page_title = "Deposit!";
	
	$top_left_fixed_img = '<img src="/images/baseball.png">';
	$top_right_fixed_img = '<img src="/images/football.png">';

	include("header.php");
	include('page_top.php');

        if(!empty($_GET['transaction_id'])){

             $q = "SELECT 
		    *,
		    `u`.`firstname` AS `customer_firstname`,
		    `u`.`lastname` AS `customer_lastname`
		   
		FROM
		    `payment_logs` AS `p`
		       LEFT JOIN
		    `users` AS `u` ON `u`.`id` = `p`.`user_id`       
			LEFT JOIN
		    `customers` AS `c` ON `c`.`user_id` = `u`.`id`
			LEFT join
		    `customer_transaction` AS `t` ON `t`.`transaction_id` = `p`.`customer_transaction_id`
		
		WHERE
		    `p`.`id` = %d";
	    $params = array($_GET['transaction_id']);
	    $transaction_info = $db->queryOneRow($q, $params);
        }
	
?>
	<div class='well' style='min-height: 400px; text-align: center; margin:0;'>
     <?php
          echo '<h2><br><br>Your deposit of $'.$transaction_info['originalamount'].' is '.(($transaction_info['result'] == 0) ? 'Successfull.' : 'Unsuccessfull.').'<br><br></h2>';                   

     ?>
                                          <div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Transaction Details</h3>
								</div>
								<div class="panel-body">
									<div class="col-sm-12" style="text-align: center; min-height: 250px;">
                                       
								 <div class="col-sm-4">
								    <div class="form-group">
									<label for="first_name">Reference Number</label>
						<span class="form-control" id="first_name"  readonly="readonly"><?php echo $transaction_info['referencenumber']; ?></span>
									</div>
								    </div>
                                                                <div class="col-sm-4">
								    <div class="form-group">
									<label for="first_name">Transcation ID</label>
									<span class="form-control" id="first_name"  readonly="readonly"><?php echo $transaction_info['customer_transaction_id']; ?></span>
									</div>
								    </div>
                                                                <div class="col-sm-4">
								    <div class="form-group">
									<label for="first_name">Transaction Date</label>
									<span class="form-control" id="first_name"  readonly="readonly"><?php echo $transaction_info['transaction_date']; ?></span>
									</div>
								    </div>
                                                               
								    
<div class="col-sm-4">
								    <div class="form-group">
									<label for="first_name">Transaction Amount</label>
									<span class="form-control" id="first_name"  readonly="readonly"><?php echo $transaction_info['originalamount']; ?></span>
									</div>
								    </div>
<div class="col-sm-4">
								    <div class="form-group">
									<label for="first_name">Initial Balance</label>
									<span class="form-control" id="first_name"  readonly="readonly"><?php echo $transaction_info['initial_balance']; ?></span>
									</div>
								    </div>
<div class="col-sm-4">
								    <div class="form-group">
									<label for="first_name">After Balance</label>
									<span class="form-control" id="first_name"  readonly="readonly"><?php echo $transaction_info['balance']; ?></span>
									</div>
								    </div>

								     <div class="col-sm-4">
								    <div class="form-group">
									<label for="first_name">Status</label>
									<span class="form-control" id="first_name"  readonly="readonly"><?php echo $transaction_info['message']; ?></span>
									</div>
								    </div>
<div class="col-sm-12" style="text-align: center;">
   <a style="margin: 10px; padding: 10px;" class="btn btn-magenta" transcation_id="<?php echo $_GET['transaction_id'];?>" id="Print_reciept">Print reciept</a>
</div>


											
										
									</div>
								</div>
							</div>

	</div>
		</div>
<!-- Modal for Slider Pic Selection -->
	<div class="modal fade" id="slider_pic_modal" tabindex="-1" role="dialog" aria-labelledby="slider_pic_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="slider_pic_modal_label">Change Slider Photos</h4>

					<div id="customer_name2" class="pull-right" style="padding-right: 6px;">
						
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div id="upload_photos" class="col-sm-12">
						<?php include($_SERVER['DOCUMENT_ROOT'].'/upload_slider_photos.php'); ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
  $('#Print_reciept').on('click',function(){
        window.open("/panel/receipts/get_receipt.php?customer_transaction="+$(this).attr('transcation_id'),"receipt");
  });

</script>
<?php
	
	
	include("footer.php");
?>
