<?php

	
	require_once("config.php");
	
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title><?php if(isset($page_title)){echo $page_title;}else{echo "CSCLotto.com - DING DING !";}?></title>
		
		<link rel="icon" type="image/x-icon" href="/images/favicon.ico" />
		
		<meta charset="utf-8"/>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<meta name="description" content="Online Gaming Website - Play 2, 3, 4 Ball and Fruity, Serpents, Luck Pets etc." />
		<meta name="keywords" content="csclotto, casino, lottery, numbers, fruity slots, serpents treasure, pirates revenge, island luck, asue draw, whatfall" />
		
		<!-- Modernizr: Polyfill Feature Detection -->
		<script src="/lib/assets/modernizr/polyfills/Placeholders.min.js"></script>
		
		<!-- jQuery -->
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery/jquery-1.11.0.js"></script>
		
		<!-- jQuery UI -->
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery-ui-1.11.4/jquery-ui.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/lib/assets/jquery-ui-1.11.4/jquery-ui.min.css">
		<link rel="stylesheet" type="text/css" href="/lib/assets/jquery-ui-1.11.4/jquery-ui.theme.min.css">
		
		<script type="text/javascript">
			// fix compatibility issues with same class names in jquery ui and bootstrap
			$.widget.bridge('uitooltip', $.ui.tooltip);
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		
		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap.min.css" media="screen">
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap-buttons.min.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/modern-business.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/tooltips.css" />
		<!-- <link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-xl/BootstrapXL.css" /> -->
		
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/jqBootstrapValidation.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/tooltips.js"></script>
		
		<!-- Bootstrap FormValidation Plugin (http://formvalidation.io/) -->
		<link rel="stylesheet" href="/lib/assets/formvalidation-0.6.2/dist/css/formValidation.min.css">
		<script src="/lib/assets/formvalidation-0.6.2/dist/js/formValidation.min.js"></script>
		<script src="/lib/assets/formvalidation-0.6.2/dist/js/framework/bootstrap.min.js"></script>
		
		<!-- Bootstrap Select Plugin -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-select/dist/css/bootstrap-select.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
		
		<!-- Bootstrap Form Helper Plugin -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-formhelpers/css/bootstrap-formhelpers.min.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-formhelpers/js/bootstrap-formhelpers.min.js"></script>
		
		<!-- Bootstrap Date Range Picker Plugin (http://www.daterangepicker.com/) -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
		
		<!-- Bootstrap Toggle Plugin (http://www.bootstraptoggle.com/) -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-toggle/css/bootstrap-toggle.min.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
		
		<!-- DataTables -->
		<!-- <link rel="stylesheet" type="text/css" href="/lib/assets/datatables/media/css/jquery.dataTables.css"> -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/datatables/extensions/Plugins/integration/bootstrap/3/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="/lib/assets/datatables/extensions/Responsive/css/dataTables.responsive.css">
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/media/js/jquery.dataTables.js"></script>
		<script type="type/javascript" language="javascript" src="/lib/assets/datatables/extensions/TableTools/js/dataTables.tableTools.js"></script>
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/extensions/Plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
		
		<!-- High Charts -->
		<script src="/lib/assets/highstock-2.1.5/js/highstock.js"></script>
		<script src="/lib/assets/highstock-2.1.5/js/modules/exporting.js"></script>
		
		<!-- DataTables Responsive Plugin (have to load it here after main datatables js finishes loading -->
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/extensions/Responsive/js/dataTables.responsive.js"></script>
		
		<!-- Modernizr: For HTML5/CSS3 Feature Detection -->
		<script src="/lib/assets/modernizr/modernizr.custom_min.js"></script>
		
		
		<!-- Bootstrap DateTimePicker (http://eonasdan.github.io/bootstrap-datetimepicker/) -->
		<script type="text/javascript" src="/lib/assets/moment/moment.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
		<link rel="stylesheet" href="/lib/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" />
		
		<!-- BootBox Alerts -->
		<script type="text/javascript" src="/lib/assets/bootbox/bootbox.min.js"></script>
		
		<!-- AJAX Push -->
		<script src="/lib/framework/push_ajax.js"></script>
		
		
		

		
		<!-- Main UI -->
		<link rel="stylesheet" type="text/css" href="/css/default.css?v=<?=CSS_VERSION?>" />
		<link rel="stylesheet" type="text/css" href="/admin/css/global.css?v=<?=CSS_VERSION?>" />
		<script>
			$(function () {
				// initialize inline datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
				
				// initialize datepickers
				$('.dp').datetimepicker({
					showTodayButton: true,
					format: 'MM/DD/YYYY'
				});
				
				// initialize datetimepickers
				$('.dtp').datetimepicker({
					showTodayButton: true,
					format: 'MM/DD/YYYY h:mm A'
				});
			});
			$(document).ready(function(){
				//$.fn.bootstrapBtn = $.fn.button.noConflict();
				
				Modernizr.load({
					test: Modernizr.placeholder,
					nope: '/lib/assets/modernizr/polyfills/Placeholders.min.js'
				});
				
				// Enable Bootstrap Select 
				$('.selectpicker').selectpicker();
				
				// thumbnail hover image changer
				$('.thumbnail').hover(
					function(){
						$(this).find('.thumbnail-caption').slideDown(250); //.fadeIn(250)
					},
					function(){
						$(this).find('.thumbnail-caption').slideUp(250); //.fadeOut(250)
					}
				);
			});
		</script>

			
		<link rel="stylesheet" type="text/css" href="/rapidballs/css/balls.css?v=<?=CSS_VERSION?>">
		<link rel="stylesheet" type="text/css" href="/rapidballs/css/global.css?v=<?=CSS_VERSION?>">
		<script type="text/javascript" src="/rapidballs/js/global.js?v=<?=JS_VERSION?>"></script>

		<script src="/lib/assets/animate_table_change/animator.js"></script>
		<script src="/lib/assets/animate_table_change/rankingTableUpdate.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				function get_recent_winners(){
					$.ajax({
						url: "/ajax/recent_winners_fullscreen.php",
						method: "POST",
						data:{
							type : 'recent'
						}
					})
					.done(function(response){
						if(response){
							var old_table = $('#recent_winners_list').html();
							$('#temp_recent_winners_list').html(response);
							var new_table = $('#temp_recent_winners_list').html();
							
							if(old_table != new_table){
								$('#recent_winners_list').rankingTableUpdate('<table class="table" id="recent_winners_list">'+new_table+'</table>', {
									duration: [1000, 0, 700, 0, 500],
									onComplete: function(){
										updating = false;
									},
									animationSettings: {
										up: {
											left: -25,
											backgroundColor: '#CCFFCC'
										},
										down: {
											left: 25,
											backgroundColor: '#8C008C'
										},
										fresh: {
											left: 0,
											backgroundColor: '#CCFFCC'
										},
										drop: {
											left: 0,
											backgroundColor: '#8C008C'
										}
									}
								});
							}
							setTimeout(get_recent_winners, 5000);
						}
					});
				}
				
				function get_big_winners(){
					$.ajax({
						url: "/ajax/recent_winners.php",
						method: "POST",
						data:{
							type : 'big',
							data : 'rapidballs'
						}
					})
					.done(function(response){
						if(response){
							var old_table = $('#big_winners_list').html();
							$('#temp_big_winners_list').html(response);
							var new_table = $('#temp_big_winners_list').html();
							
							if(old_table != new_table){
								$('#big_winners_list').rankingTableUpdate('<table class="table" id="big_winners_list">'+new_table+'</table>', {
									duration: [1000, 0, 700, 0, 500],
									onComplete: function(){
										updating = false;
									},
									animationSettings: {
										up: {
											left: -25,
											backgroundColor: '#CCFFCC'
										},
										down: {
											left: 25,
											backgroundColor: '#8C008C'
										},
										fresh: {
											left: 0,
											backgroundColor: '#CCFFCC'
										},
										drop: {
											left: 0,
											backgroundColor: '#8C008C'
										}
									}
								});
							}
							setTimeout(get_big_winners, 5000);
						}
					});
				}
				get_recent_winners();
				get_big_winners();
			});
		</script>
		<link rel="stylesheet" href="/lib/assets/FlipClock/flipclock.css">
	<script src="/lib/assets/FlipClock/flipclock.min.js"></script>
	
	<script type="text/javascript" src="/rapidballs/lib/assets/simplyscroll/jquery.simplyscroll.js"></script>
	<link rel="stylesheet" href="/rapidballs/lib/assets/simplyscroll/jquery.simplyscroll.css" media="all" type="text/css">
	<script type="text/javascript">
		(function($) {
			$(function() {
				$(".scroller").simplyScroll({
					orientation: 'vertical', 
					customClass: 'auto-scroll',
					auto: true,
					autoMode: 'bounce',
					direction: 'forwards'
				});
			});
		})(jQuery);
	</script>

	<script>
		var clock;

		$(document).ready(function() {
			// Grab the current date
			var currentDate = new Date();

			// Calculate the difference in seconds between the future and current date
			var diff;
			start_counter = function(){
				$.ajax({
					url: "/rapidballs/ajax/rapidballs_countdown.php",
					method: "POST"
				})
				.done(function(response){
					console.log(response);
					diff = response;
					
					// Instantiate a coutdown FlipClock
					clock = $('#countdown_flipclock').FlipClock(diff, {
						clockFace: 'MinuteCounter',
						countdown: true,
						autoStart: false,
						callbacks: {
							stop: function() {
								start_counter();
							},
							interval: function () {
								var time = clock.getTime().time;
								// show warning style if near next draw
								if (time < 30) {
									if(!$("#countdown-contain").hasClass("ending-soon")){
										$("#countdown-contain").addClass("ending-soon");
									}
								}else{
									if($("#countdown-contain").hasClass("ending-soon")){
										$("#countdown-contain").removeClass("ending-soon");
									}
								}
								
								if(time >= 295){
									// check for up to 5 seconds after draw is supposed to happen
									update_drawings();
								}
							}
						}
					});
					
					clock.start();
				});
			};
			start_counter();
			
			function update_drawings(){
				// grab most recent drawing
				$.ajax({
					data: {
						size: "lg",
						limit: 1,
						type: 'normal'
					},
					url: "/rapidballs/ajax/rapidballs_recent_drawings.php",
					method: "POST",
					dataType: "json"
				})
				.done(function(response){
					$("#last-drawing").html(response.balls);
					$("#last-total").text(response.total);
					$("#last-lucky").text(response.lucky);
				});
			
				// update last 6 most recent
				$.ajax({
					data: {
						size: "sm",
						limit: 6,
						type: 'table'
					},
					url: "/rapidballs/ajax/rapidballs_recent_drawings.php",
					method: "POST"
				})
				.done(function(response){
					$("#recent-6").html(response);
				});
			}
			
			//Update the total on select change.
			$(document).on("change", "#number_of_draws", function(){
				var num_draws = parseFloat($(this).val());
					stake_per_draw = parseFloat($("#stake_per_draw").val().replace("$",""));
					
				$("#total").text((num_draws * stake_per_draw).toFixed(2));
			});
			
			$(document).on("change", "#stake_per_draw", function(){
				var num_draws = parseFloat($("#number_of_draws").val());
					stake_per_draw = parseFloat($(this).val().replace("$",""));
					
				$("#total").text((num_draws * stake_per_draw).toFixed(2));
			});
			
			// load initial recent drawings
			update_drawings();
		});
	</script>
		<style>
		#countdown-contain{
			padding: 15px;
			text-align: left;
			display: inline-block;
			
		}
		
		#last-drawing-contain{
			padding: 15px;
			text-align: left;
			display: inline-block;
			
	
		}
		
		.ending-soon{
			animation: blink .5s step-end infinite alternate;
		}
		
		@keyframes blink {
			50% { border: 2px solid red; } 
		}
		
		.auto-scroll .simply-scroll-clip {
			height: 220px;
		}
	</style>
		</head>
	<body>
		<div class="bg">
			<div class="col-sm-12 winners_board ">
				<div class="win-bg-plain">
					<h3 class="headings"><img src="/images/CSCLotto_chip.png" width="60">&nbsp;Latest CSCLotto Winners</h2>
				</div>
				<marquee style="color:yellow;font-size: 30px;">Today's Lucky Numbers are 2 4 7 </marquee>
						<div class="col-sm-2"  style="text-align:center; ">
			<script type="text/javascript">
		$(document).ready(function(){
			$('#slider_photo_btn').click(function(){
				$("#slider_pic_modal").modal("show");
			});
		});
	</script>
	
	
	
	
		
		<?php
			$files = scandir('images/fullscreen/');
			$total = count($files);
			$images = array();
			for($x = 0; $x <= $total; $x++){
				if($files[$x] != '.' && $files[$x] != '..' && is_file('images/slider/'.$files[$x])){
					$images[] = $files[$x];
				}
			}
		?>
	
		<div id="carousel-index" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'" class="active"></li>';
						}else{
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'"></li>';
						}
					}
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '
				<div class="item active">
					<a href="#">
						<img src="/images/fullscreen/'.$images[$x].'" style="width:350px;" class="carousel-image">
					</a>
				</div>
							';
						}else{
							echo '
				<div class="item">
					<a href="#">
						<img src="/images/fullscreen/'.$images[$x].'"  style="width:350px;" class="carousel-image">
					</a>
				</div>
							';
						}
					}
				?>
				
			</div>
		
		</div>
			
	</div>
				<div class="col-sm-8 winners_board ">
					<table id='temp_recent_winners_list' style='display: none;'></table>
					
					<table class="table" id="recent_winners_list">
						<thead>
							<tr>
								<th class="position anim:id anim:number hidden" />
								<th class="driverName anim:number hidden" />
								<th class="pointsTotal anim:update hidden" />
								<th class="pointsTotal anim:update hidden" />
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					
				</div>
				
					<div class="col-sm-2"  style="text-align:center; ">
			<script type="text/javascript">
		$(document).ready(function(){
			$('#slider_photo_btn').click(function(){
				$("#slider_pic_modal").modal("show");
			});
		});
	</script>
	
	
	
	
		
		<?php
			$files = scandir('images/fullscreen/');
			$total = count($files);
			$images = array();
			for($x = 0; $x <= $total; $x++){
				if($files[$x] != '.' && $files[$x] != '..' && is_file('images/slider/'.$files[$x])){
					$images[] = $files[$x];
				}
			}
		?>
	
		<div id="carousel-index" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'" class="active"></li>';
						}else{
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'"></li>';
						}
					}
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '
				<div class="item active">
					<a href="#">
						<img src="/images/fullscreen/'.$images[$x].'" style="width:350px;" class="carousel-image">
					</a>
				</div>
							';
						}else{
							echo '
				<div class="item">
					<a href="#">
						<img src="/images/fullscreen/'.$images[$x].'"  style="width:350px;" class="carousel-image">
					</a>
				</div>
							';
						}
					}
				?>
				
			</div>
		
		</div>
			
	</div>

	
	
	
				<link rel="stylesheet" type="text/css" href="/lib/assets/slick-1.5.7/slick.css"/>
			<link rel="stylesheet" type="text/css" href="/lib/assets/slick-1.5.7/slick-theme.css"/>
			
			<script type="text/javascript" src="/lib/assets/slick-1.5.7/jquery-migrate-1.2.1.min.js"></script>
			<script type="text/javascript" src="/lib/assets/slick-1.5.7/slick.min.js"></script>
			
			<script>
				$(document).ready(function(){				
					// get casino game list
					$.ajax({
						url: "/api/casino/get_casino_games.php",
						type: "POST",
						dataType : "json",
						success: function(response){
							if(response.token != ''){
								$.each(response.games, function(index, game) {
									var tags = "";
									var new_ribbon = "";
									$.each(game.tags, function(index, tag){
										tags += tag+" ";
										if(tag == 'new'){
											new_ribbon = "	<div class='clearfix new_ribbon'></div>";
										}
										if(tag == 'least_played'){
											new_ribbon += "	<div class='clearfix featured_ribbon'></div>";
										}
									});							
									
									$('#casino_games_preview').append(
										"<div id='"+escape(game.LobbyImageAltText.toLowerCase())+"' class='casino_game "+game.GameType.toLowerCase()+" "+tags+"col-xs-6 col-sm-4 col-md-3 col-lg-2' style='padding: 5px;'>"+
										new_ribbon+
										"	<div class='col-sm-12 panel casino_game_inner' style='border: 1px solid purple; background:#000; padding-top: 15px;padding-bottom: 15px;'>"+
										"		<img onError='hide_game(this);' style='width: 100%;' alt='"+game.LobbyImageAltText+"' src='"+game.LobbyImage+"'>"+
										"	</div>"+
										"</div>"
									);
								});
								
								$('#casino_games_preview').slick({
									autoplay: true,
									dots: false,
									infinite: true,
									speed: 1000,
									slidesToShow: 6,
									slidesToScroll: 3,
									arrows: false,
									responsive: [
										{
											breakpoint: 900,
											settings: {
												slidesToShow: 4,
												slidesToScroll: 2
											}
										},
										{
											breakpoint: 600,
											settings: {
												slidesToShow: 3,
												slidesToScroll: 2
											}
										},
										{
											breakpoint: 480,
											settings: {
												slidesToShow: 2,
												slidesToScroll: 1
											}
										}
									]
								});
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							bootbox.alert("Unexpected Error!");
						}
					});
					
					
				});
			</script>
			
			<style>
				.casino_game_type{
					background:#595959;
					background:-moz-linear-gradient(center top,#595959 0%,#1a1a1a 100%);
					-pie-background:linear-gradient(center top,#595959 0%,#1a1a1a 100%);
					background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#595959),color-stop(1,#1a1a1a));
					background-image:-o-linear-gradient(top,#595959 0%,#1a1a1a 100%);
					background:-ms-linear-gradient(center top,#595959 0%,#1a1a1a 100%);
					background:linear-gradient(center top,#595959 0%,#1a1a1a 100%);
					position:relative;
					color:#fff;
					margin:0px;
					overflow:hidden;
					border-bottom: 1px solid #8c8c8c;
					margin-top: -20px;
					
				}
				
				.casino_game_inner:hover{
					outline: none;
					box-shadow: 0 0 10px #FBB4F0;
				}

				.casino_game_type li{
					display:inline-block;
					position:relative;
					line-height: 30px;
					padding:5px 20px 4px 20px;
					border-right:1px #8c8c8c solid;
					border-bottom:2px transparent solid;
					text-decoration:none;
					text-align:center;
					cursor:pointer;
					-webkit-transform:skew(25deg);
					-moz-transform:skew(25deg);
					-moz-transform:skewX(25deg);
					-ms-transform:skew(25deg);
					-o-transform:skew(25deg);
					transform:skew(25deg);
					outline:none!important;
				}

				.casino_game_type li.active{
					background:-moz-linear-gradient(center top,#212121 50%,#383e28 100%);
					background:-webkit-gradient(linear,left top,left bottom,color-stop(0.5,#212121),color-stop(1,#383e28));
					background:-o-linear-gradient(top,#212121 50%,#383e28 100%);
					background:-ms-linear-gradient(top,#212121 50%,#383e28 100%);
					background:linear-gradient(top,#212121 50%,#383e28 100%);
					border-right: #8c8c8c 1px solid;
					border-bottom: #FBB4F0 2px solid;
					margin-left: -5px;
					padding-right: 25px;
				}

				.casino_game_type li span{
					font-size:12px;
					font-weight:normal;
					color: white;
					display:block;
					text-decoration:none;
					-webkit-transform:skew(-25deg);
					-moz-transform:skew(-25deg);
					-moz-transform:skewX(-25deg);
					-ms-transform:skew(-25deg);
					-o-transform:skew(-25deg);
					transform:skew(-25deg);
					outline:none!important;
					-moz-user-select: none;
					-khtml-user-select: none;
					-webkit-user-select: none;
					-o-user-select: none;
					user-select: none;
				}

				.casino_game_type li.active span, .casino_game_type li span:hover{
					color: #FBB4F0;
					-moz-user-select: none;
					-khtml-user-select: none;
					-webkit-user-select: none;
					-o-user-select: none;
					user-select: none;
				}

				.casino_game_type li.active span{
					font-weight:bold;
					-moz-user-select: none;
					-khtml-user-select: none;
					-webkit-user-select: none;
					-o-user-select: none;
					user-select: none;
				}
				
				.new_ribbon{
					background: url('/images/casino/new_ribbon.png');
					background-size: 80px 80px;
					background-repeat: no-repeat;
					position: absolute;
					top: -2px;
					right: -1px;
					margin: 0;
					width: 80px;
					height: 80px;
					z-index: 999;
				}
				
				.featured_ribbon{
					background: url('/images/casino/featured_ribbon.png');
					background-size: 80px 80px;
					background-repeat: no-repeat;
					position: absolute;
					top: -2px;
					left: -1px;
					margin: 0;
					width: 80px;
					height: 80px;
					z-index: 999;
				}
			</style>
				
			
				<div class='col-md-12' style="height: 260px; overflow: hidden; margin-top: -10px;">
					<div id="casino_games_preview" style="padding: 15px;">
					</div>
				</div>
			
			
			
			
			</div>
			<div class="col-sm-12" style="background-image: url('/images/purple.jpg');">
				<div class="col-sm-6"  style="text-align:center;">		<p>&nbsp;</p>
					<div id="countdown-contain">
						<h3 style="margin-top: -10px; margin-bottom: 40px;color:#FFF">Next FastBallz Drawing In:</h3>
						<div id="countdown_flipclock"></div>
					</div>
					<p>&nbsp;</p>
				</div>
				<div class="col-sm-6"  style="text-align:center;">				
					<div id="last-drawing-contain">
						<p>&nbsp;</p>
						<h3 style=" margin-top: -10px; margin-bottom: 40px;color:#FFF">Last Drawing:</h3>
						<div id="last-drawing"></div>
						<p>&nbsp;</p>
						<div class="col-sm-6" style="text-align:center;color:#FFF"">Total: <span id="last-total"></span></div>
					<div class="col-sm-6" style="text-align:center;color:#FFF"">Lucky: <span id="last-lucky"></span></div>
					</div>
					<p>&nbsp;</p>
					
			</div>	
				
			</div>
		</div>
			<!-- Controls -->
			


		
	

</body>
</html>
