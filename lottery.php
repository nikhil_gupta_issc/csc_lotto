<?php
	$page_title = "2, 3, 4, 5 Ball Lotto";
	$menu_color = "purple";
	$footer_color = "purple";

	
	$top_left_fixed_img = "<img src='/images/lotto_lft.png'>";
	$top_right_fixed_img = "";

	include("header.php");
	include('page_top.php');

	if(!$session->logged_in){
		include('public_lottery.php');
		include("footer.php");
		die;
	}else{
?>
<script>
	$(document).on("click", '#all_ball', function(){
		
		$('#two_ball').addClass("black_tabs");
		$('#four_ball').addClass("black_tabs");
		$('#all_ball').removeClass("black_tabs");
		$('#all_ball').addClass("btn-primary");
		
	});
	
	$(document).on("click", '#two_ball', function(){
		
		$('#all_ball').addClass("black_tabs");
		$('#four_ball').addClass("black_tabs");
		$('#two_ball').removeClass("black_tabs");
		$('#two_ball').addClass("btn-primary");
		
	});
	
	$(document).on("click", '#four_ball', function(){
		
		$('#all_ball').addClass("black_tabs");
		$('#two_ball').removeClass("btn-primary");
		$('#two_ball').addClass("black_tabs");
		$('#four_ball').removeClass("black_tabs");
		$('#four_ball').addClass("btn-primary");
		
	});
	/* Late House Tab Script for Change Class */
	$(document).on("click", '#late_all_ball', function(){
		
		$('#late_two_ball').addClass("black_tabs");
		$('#late_four_ball').addClass("black_tabs");
		$('#late_all_ball').removeClass("black_tabs");
		$('#late_all_ball').addClass("btn-primary");
		
	});
	
	$(document).on("click", '#late_two_ball', function(){
		
		$('#late_all_ball').addClass("black_tabs");
		$('#late_four_ball').addClass("black_tabs");
		$('#late_two_ball').removeClass("black_tabs");
		$('#late_two_ball').addClass("btn-primary");
		
	});
	
	$(document).on("click", '#late_four_ball', function(){
		
		$('#late_all_ball').addClass("black_tabs");
		$('#late_two_ball').removeClass("btn-primary");
		$('#late_two_ball').addClass("black_tabs");
		$('#late_four_ball').removeClass("black_tabs");
		$('#late_four_ball').addClass("btn-primary");
		
	});
	
	/* END */
</script>
	<style>
		.nav.nav-tabs a
		{
			color:#fff;
			
		}
		.active > a
		{
			color:#fff !important;
		}
		.nav.nav-tabs a:hover
		{
			color:#fff !important;
		}
		.nav.nav-tabs
		{
			border:none;
			margin-left:12px;
		}
		.nav.nav-tabs > li
		{
			width:98px;
		}
		.red
		{
			background-color:red !important;
			
		}
		.content_color
		{
			color:#000;
		}
a{ color:#FFFFFF !important;}
.text {
  color: black;
}
.new_ribbon {
    background: url('/images/casino/new_ribbon.png');
  background-size: 53px 53px;
  background-repeat: no-repeat;
  position: absolute;
  top: -2px;
  right: -29px;
  margin: 0;
  width: 76px;
  height: 80px;
  z-index: 999;
}
		.bg{
			background: black url('/images/lotteryh2_03.jpg') no-repeat center top;
		}
		ul.nav.nav-pills.head-menu>li>a{
			color: white !important;
		}
		ul.nav.nav-pills.head-menu>li:hover>a{
			color: hsl(288, 100%, 18%) !important;
		}
		ul.nav.nav-pills.head-menu>li.active:hover>a{
			color: white !important;
		}
	</style>
<script type="text/javascript">
		$(document).ready(function(){
			$("#clear_bets_cart").hide();
			$("#validation_err").hide();
			$('#slider_photo_btn').click(function(){
				$("#slider_pic_modal").modal("show");
			});
		});
	</script>

	<div class="clearfix" style="background:#fff">	
	
	<div class="row" style="background:#fff !important;">
		<div class="container" style="background: #fff;">
<?php
			if($session->userlevel >= 9){
				echo '
		<div style="text-align: center;">
			<button class="btn btn-primary" id="slider_photo_btn" style="margin: 5px;">Change Photos</button>
		</div>
				';
			}
		?>
			<div class="header_image">

		<?php
			$files = scandir('images/lottery_slider_image/');
			$total = count($files);
			$images = array();
			for($x = 0; $x <= $total; $x++){
				if($files[$x] != '.' && $files[$x] != '..' && is_file('images/lottery_slider_image/'.$files[$x])){
					$images[] = $files[$x];
				}
			}
		?>
	
		<div id="carousel-index" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'" class="active"></li>';
						}else{
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'"></li>';
						}
					}
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '
				<div class="item active">

						<img src="/images/lottery_slider_image/'.$images[$x].'" class="carousel-image">

				</div>
							';
						}else{
							echo '
				<div class="item">

						<img src="/images/lottery_slider_image/'.$images[$x].'" class="carousel-image">

				</div>
							';
						}
					}
				?>
			</div>
		   <!--<div class="header_image">
				<img src="/images/lottobanner.jpg" class="img_grow_wide">
			</div>-->
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="js/lotto_betting.js?v=<?=JS_VERSION?>"></script>

	<div class="clearfix" style="background:#fff !important;">
	
	
	
	<script>
		$(document).ready(function(){
			<?php 
				if(isset($_GET['tab'])){
					if($_GET['tab'] == 2){
						echo '$("#2_ball_tab").trigger("click");';
					}elseif($_GET['tab'] == 5){
						echo '$("#5_ball_tab").trigger("click");';
					}elseif($_GET['tab'] == 6){
						echo '$("#6_ball_tab").trigger("click");';
					}elseif($_GET['tab'] == 'fav_bets'){
						echo '$("#fav_bets_tab").trigger("click");';
					}
					elseif($_GET['tab'] == 'redeem_voucher'){
						echo '$("#redeem_bets_tab").trigger("click");';
					}
					elseif($_GET['tab'] == 'scheduled_bets'){
						echo '$("#scheduled_bets_tab").trigger("click");';
					}elseif($_GET['tab'] == 'lotto_help'){
						echo '$("#lotto_help_tab").trigger("click");';
					}
				}
			?>                        
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"columns": [
					{ "data": "game" },
					{ "data": "pick" },
					{ "data": "bet_type" },
					{ "data": "bets_remaining" },
					{ "data": "amount" },
					{ "data": "next_draw_date" }
				],
				"order": [[0, 'desc']],
				"ajax": "/ajax/datatables/scheduled_bets.php",
				"dom": 'lfrtp'
			});


       setInterval(function (){
		$.ajax({
			url: "ajax/lotto_betting.php",
			dataType: 'json',
			data: {
				action: "get_active_games"
			},
			method: "POST"
		})
		.done(function(data){		
			$.each(data, function(index, house){
				
				if(house.time_remaining == 'Closed'){
					
					$(".game_"+house.id).attr("disabled", true);
					$(".game_"+house.id).closest('tr').delay(1000).fadeOut(1000);
				}
			});
		});
	}, 1000);

		});
		
                
			$(document).on("click", "#redeem_ticket", function(){
			
				$.ajax({
					url: "/ajax/my_account.php",
					type: "POST",
					data: {
						action : "redeem_ticket",
						ticket_number : $("#ticket").val()
					},
					dataType: "json",
					success: function(data){
						if(data.success==true){
							bootbox.alert("Winner!! "+data.amount+" has been added to your balance");
							$("#redemption_errors").html("");
							$("#redemption_errors").slideUp("fast");
						}else{
							$("#redemption_errors").html(data.errors);
							$("#redemption_errors").slideDown("fast");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Redemption attempts exceeded.  Your account has been locked out.", function(){ 
							window.location.reload();
						});
					}
				});
			});

	</script>
	<div role="tabpanel" style="background:#fff !important;">
		<!-- Nav tabs -->
		<ul class="black_tabs" role="tablist">
			<li role="presentation"><a href="#2_ball" aria-controls="2_ball" role="tab" data-toggle="tab" id="2_ball_tab"><span>2/3/4 Ball</span></a></li>
			<!--<li role="presentation"><a href="#5_ball" aria-controls="5_ball" role="tab" data-toggle="tab" id="5_ball_tab"><span>5 Ball</span></a></li>-->
			<!--<li role="presentation"><a href="#6_ball" aria-controls="6_ball" role="tab" data-toggle="tab" id="6_ball_tab"><span>6 Ball</span></a></li> -->
			<li role="presentation"><a href="#fav_bets" aria-controls="fav_bets" role="tab" data-toggle="tab" id="fav_bets_tab"> <div class="clearfix new_ribbon"></div><span>Favorite Bets</span></a></li>
			<li role="presentation"><a href="#scheduled_bets" aria-controls="scheduled_bets" role="tab" data-toggle="tab" id="scheduled_bets_tab"><div class="clearfix new_ribbon"></div><span>Advance Play</span></a></li>
            <li role="presentation"><a href="#ticket_history" aria-controls="ticket_history" role="tab" data-toggle="tab" id="ticket_history_tab"><span>Ticket Purchase History</span></a></li>

                      <!-- <li role="presentation"><a href="#even_odds" aria-controls="even_odds" role="tab" data-toggle="tab" id="even_odds_tab"><span>Even/Odds</span></a></li>-->

                      <!-- <li role="presentation"><a href="#hundred_triples" aria-controls="hundred_triples" role="tab" data-toggle="tab" id="hundred_triples_tab"><span>Hundreds/Triples</span></a></li>-->
	<li role="presentation"><a href="#redeem_voucher" aria-controls="redeem_voucher" role="tab" data-toggle="tab" id="redeem_bets_tab"> <span>Redeem Ticket</span></a></li>
	<li role="presentation"><a href="#how_to_play" aria-controls="how_to_play" role="tab" data-toggle="tab" id="how_to_play_tab"><span>How To Play</span></a></li>
   <li role="presentation"><a href="#lotto_history" aria-controls="lotto_history" role="tab" data-toggle="tab" id="lotto_history_tab"><span>Detailed Lotto history</span></a></li>
		</ul>
		
		<div id="autohide_games_contain" class="pull-right" style="margin-right: 15px; padding-top: 1.5px;">
			<input id="show_inactive" data-toggle="toggle" data-on="Show Inactive" data-off="Hide Inactive" type="checkbox" <?php echo $session->userinfo['customers']['show_inactive_games'] == 1 ? "checked" : ""; ?> >
		</div>
		<div class="col-sm-9" style="background:#fff !important; padding-left: 0px; padding-right: 0px;">
			<div class="tab-content" style="margin-top:15px;">
				<div role="tabpanel" class="tab-pane fade in active" id="2_ball">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Choose Games (* Indicates no 4 ball, ** indicates 4 ball only)</h3>
						</div>
						<div class="panel-body large-panel-scroll">
							<div class="col-sm-6">
								
								<table class="table table-bordered">
									
										<tr>
											<th colspan="3">Early Houses</th>

										</tr>
										<tr>
											<td colspan="3">
												<div class="container col-sm-12">
													  <ul class="nav nav-tabs ">
													<!--	 <li class="active"><a data-toggle="tab" id="all_ball" class="btn-primary" href="#all"> All</a></li> 
														<li><a data-toggle="tab" class="black_tabs" id="two_ball" href="#23ball"> 2 /3 Balls</a></li>
														<li><a data-toggle="tab" href="#4ball" id="four_ball" class="black_tabs"> 4 Balls </a></li> !-->
													</ul>
													
													<div class="tab-content col-sm-12" style="margin-top:20px;">
															<div id="all" class="tab-pane fade in active">
															<table class="table table-bordered" style="background-color:black;width:290px; color: #fff;">
															<?php
															$curr_time = new datetime('now');
															$early_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=1 AND (g.number_of_balls=2 OR g.number_of_balls=3 OR g.number_of_balls=4) GROUP BY h.id ORDER BY h.web_cutoff_time");
															if(count($early_houses) > 0){
																	echo '
																	<thead>
																	<tr>
																		<th><input type="checkbox" id="all_early_2_check"></th>
																		<th>Houses</th>
																		<th>Closing Time</th>
																	</tr>
																</thead>
																<tbody>
																			';
																		}else{
																			echo '
																</thead>
																<tbody>
																	<tr>
																		<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Early 2 Ball Games</td>
																	</tr>
																			';
																		}
																		for($x=0;$x<count($early_houses);$x++){
																			echo "<tr>";
																			//initialize the datetime for the cuttoff time
																			$web_cutoff_time = new datetime($early_houses[$x]["web_cutoff_time"]);
																			if($early_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
																				echo "<td><input disabled type=\"checkbox\" ball=\"2\" name=\"chk_name[]\"  class=\"game_".$early_houses[$x]["id"]."\"  id=\"game_".$early_houses[$x]["id"]."\"></td>";
																				echo "<td><p class='text-muted'>".$early_houses[$x]["name"]."</p></td>";
																				echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																			}else{
																				if($web_cutoff_time > $curr_time){
																					echo "<td><input class=\"early_2 game_".$early_houses[$x]["id"]."\" name=\"chk_name[]\"  type=\"checkbox\" ball=\"2\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
																					echo "<td><p>".$early_houses[$x]["name"]."</p></td>";
																					echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																				}else{
																					echo "<td><input disabled type=\"checkbox\" name=\"chk_name[]\" ball=\"2\"  class=\"game_".$early_houses[$x]["id"]."\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
																					echo "<td class='text-muted'><p>".$early_houses[$x]["name"]."</p></td>";
																					echo "<td class='text-muted'><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																				}
																			}
																			echo "</tr>";
																		}
																	?>
																					  
																					  </table>
															</div>
															
															
														<?php /*	<div id="23ball" class="tab-pane fade in">
																<table class="table table-bordered" style="background-color:black;width:290px; color: #fff;">
															<?php
															$curr_time = new datetime('now');
															$early_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=1 AND (g.number_of_balls=2 OR g.number_of_balls=3) GROUP BY h.id ORDER BY h.web_cutoff_time");
															if(count($early_houses) > 0){
																	echo '
																	<thead>
																	<tr>
																		<th><input type="checkbox" id="all_early_2_check"></th>
																		<th>Houses</th>
																		<th>Closing Time</th>
																	</tr>
																</thead>
																<tbody>
																			';
																		}else{
																			echo '
																</thead>
																<tbody>
																	<tr>
																		<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Early 2 Ball Games</td>
																	</tr>
																			';
																		}
																		for($x=0;$x<count($early_houses);$x++){
																			echo "<tr>";
																			//initialize the datetime for the cuttoff time
																			$web_cutoff_time = new datetime($early_houses[$x]["web_cutoff_time"]);
																			if($early_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
																				echo "<td><input disabled type=\"checkbox\" ball=\"2\"  class=\"game_".$early_houses[$x]["id"]."\"  id=\"game_".$early_houses[$x]["id"]."\"></td>";
																				echo "<td><p class='text-muted'>".$early_houses[$x]["name"]."</p></td>";
																				echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																			}else{
																				if($web_cutoff_time > $curr_time){
																					echo "<td><input class=\"early_2 game_".$early_houses[$x]["id"]."\"  type=\"checkbox\" ball=\"2\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
																					echo "<td><p>".$early_houses[$x]["name"]."</p></td>";
																					echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																				}else{
																					echo "<td><input disabled type=\"checkbox\" ball=\"2\"   class=\"game_".$early_houses[$x]["id"]."\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
																					echo "<td class='text-muted'><p>".$early_houses[$x]["name"]."</p></td>";
																					echo "<td class='text-muted'><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																				}
																			}
																			echo "</tr>";
																		}
																	?>
																					  
																					  </table>
															</div>
															<div id="4ball" class="tab-pane fade in">
																	<table class="table table-bordered" style="background-color:black;width:290px; color: #fff;">
															<?php
															$curr_time = new datetime('now');
															//$early_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=1 AND (g.number_of_balls=2 OR g.number_of_balls=3 OR g.number_of_balls=4) GROUP BY h.id ORDER BY h.web_cutoff_time");
															$early_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=1 AND (g.number_of_balls=4) GROUP BY h.id ORDER BY h.web_cutoff_time");
													
															if(count($early_houses) > 0){
																	echo '
																	<thead>
																	<tr>
																		<th><input type="checkbox" id="all_early_2_check"></th>
																		<th>Houses</th>
																			<th>Closing Time</th>
																		</tr>
																	</thead>
																	<tbody>
																				';
																			}else{
																				echo '
																	</thead>
																	<tbody>
																	<tr>
																		<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Early 2 Ball Games</td>
																	</tr>
																			';
																		}
																		for($x=0;$x<count($early_houses);$x++){
																			echo "<tr>";
																			//initialize the datetime for the cuttoff time
																			$web_cutoff_time = new datetime($early_houses[$x]["web_cutoff_time"]);
																			if($early_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
																				echo "<td><input disabled type=\"checkbox\" ball=\"2\"  class=\"game_".$early_houses[$x]["id"]."\"  id=\"game_".$early_houses[$x]["id"]."\"></td>";
																				echo "<td><p class='text-muted'>".$early_houses[$x]["name"]."</p></td>";
																				echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																			}else{
																				if($web_cutoff_time > $curr_time){
																					echo "<td><input class=\"early_2 game_".$early_houses[$x]["id"]."\"   type=\"checkbox\" ball=\"2\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
																					echo "<td><p>".$early_houses[$x]["name"]."</p></td>";
																					echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																				}else{
																					echo "<td><input disabled type=\"checkbox\" ball=\"2\"   class=\"game_".$early_houses[$x]["id"]."\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
																					echo "<td class='text-muted'><p>".$early_houses[$x]["name"]."</p></td>";
																					echo "<td class='text-muted'><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																				}
																			}
																			echo "</tr>";
																		}
																	?>
																					  
																</table>
															</div> */?>
													</div>
												 </div>
											</td>
											
										</tr>
										
									</tbody>
								</table>
							</div>
							<div class="col-sm-6">
								<table class="table table-bordered">
									
										<tr>
											<th colspan="3">Late Houses</th>
										</tr>
										
										<tr>
											<td colspan="3">
												<div class="container col-sm-12">
													  <ul class="nav nav-tabs ">
													<!--	 <li class="active"><a data-toggle="tab" id="late_all_ball" class="btn-primary" href="#late_all"> All</a></li> 
														<li><a data-toggle="tab" class="black_tabs" id="late_two_ball" href="#late_23ball"> 2 /3 Balls</a></li>
														<li><a data-toggle="tab" href="#late_4ball" id="late_four_ball" class="black_tabs"> 4 Balls </a></li> !-->
													</ul>
														<div class="tab-content col-sm-12" style="margin-top:20px;">
															<div id="late_all" class="tab-pane fade in active">
																<table class="table table-bordered" style="background-color:black;width:290px; color: #fff;">
																						<?php
																			$curr_time = new datetime('now');
																			$late_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=0 AND (g.number_of_balls=2 || g.number_of_balls=3 || g.number_of_balls=4) GROUP BY h.id ORDER BY h.web_cutoff_time");
																			
																			if(count($late_houses) > 0){
																				echo '
																		<tr>
																			<th><input type="checkbox" id="all_late_2_check"></th>
																			<th>Houses</th>
																			<th>Closing Time</th>
																		</tr>
																	</thead>
																	<tbody>
																				';
																			}else{
																				echo '
																	</thead>
																	<tbody>
																		<tr>
																			<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Late 2 Ball Games</td>
																		</tr>
																				';
																			}
																			
																			for($x=0;$x<count($late_houses);$x++){
																				echo "<tr>";
																				//initialize the datetime for the cuttoff time
																				$web_cutoff_time = new datetime($curr_time->format("Y-m-d")." ".$late_houses[$x]["web_cutoff_time"]);
																				if($late_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
																					echo "<td><input disabled type=\"checkbox\" ball=\"2\" name=\"chk_name[]\"  class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
																					echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
																					echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																				}else{
																					if($web_cutoff_time > $curr_time){
																						echo "<td><input class=\"late_2 game_".$late_houses[$x]["id"]."\" name=\"chk_name[]\" type=\"checkbox\" ball=\"2\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
																						echo "<td><p>".$late_houses[$x]["name"]."</p></td>";
																						echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																					}else{
																						echo "<td><input disabled type=\"checkbox\" ball=\"2\" name=\"chk_name[]\" class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
																						echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
																						echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																					}
																				}
																				echo "</tr>";
																			}
																		?>
																													  
																	  </table>
															</div>

														<?php /*	<div id="late_23ball" class="tab-pane fade in">
																<table class="table table-bordered" style="background-color:black;width:290px;">
																						<?php
																			$curr_time = new datetime('now');
																			$late_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=0 AND (g.number_of_balls=2 || g.number_of_balls=3) GROUP BY h.id ORDER BY h.web_cutoff_time");
																			
																			if(count($late_houses) > 0){
																				echo '
																		<tr>
																			<th><input type="checkbox" id="all_late_2_check"></th>
																			<th>Houses</th>
																			<th>Closing Time</th>
																		</tr>
																	</thead>
																	<tbody>
																				';
																			}else{
																				echo '
																	</thead>
																	<tbody>
																		<tr>
																			<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Late 2 Ball Games</td>
																		</tr>
																				';
																			}
																			
																			for($x=0;$x<count($late_houses);$x++){
																				echo "<tr>";
																				//initialize the datetime for the cuttoff time
																				$web_cutoff_time = new datetime($curr_time->format("Y-m-d")." ".$late_houses[$x]["web_cutoff_time"]);
																				if($late_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
																					echo "<td><input disabled type=\"checkbox\" ball=\"2\"  class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
																					echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
																					echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																				}else{
																					if($web_cutoff_time > $curr_time){
																						echo "<td><input class=\"late_2 game_".$late_houses[$x]["id"]."\" type=\"checkbox\" ball=\"2\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
																						echo "<td><p>".$late_houses[$x]["name"]."</p></td>";
																						echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																					}else{
																						echo "<td><input disabled type=\"checkbox\" ball=\"2\" class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
																						echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
																						echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																					}
																				}
																				echo "</tr>";
																			}
																		?>
																													  
																	  </table>
															</div>
															<div id="late_4ball" class="tab-pane fade in">
																<table class="table table-bordered" style="background-color:black;width:290px;">
																						<?php
																			$curr_time = new datetime('now');
																			$late_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=0 AND (g.number_of_balls=4) GROUP BY h.id ORDER BY h.web_cutoff_time");
																			
																			if(count($late_houses) > 0){
																				echo '
																		<tr>
																			<th><input type="checkbox" id="all_late_2_check"></th>
																			<th>Houses</th>
																			<th>Closing Time</th>
																		</tr>
																	</thead>
																	<tbody>
																				';
																			}else{
																				echo '
																	</thead>
																	<tbody>
																		<tr>
																			<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Late 2 Ball Games</td>
																		</tr>
																				';
																			}
																			
																			for($x=0;$x<count($late_houses);$x++){
																				echo "<tr>";
																				//initialize the datetime for the cuttoff time
																				$web_cutoff_time = new datetime($curr_time->format("Y-m-d")." ".$late_houses[$x]["web_cutoff_time"]);
																				if($late_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
																					echo "<td><input disabled type=\"checkbox\" ball=\"2\"  class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
																					echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
																					echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																				}else{
																					if($web_cutoff_time > $curr_time){
																						echo "<td><input class=\"late_2 game_".$late_houses[$x]["id"]."\" type=\"checkbox\" ball=\"2\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
																						echo "<td><p>".$late_houses[$x]["name"]."</p></td>";
																						echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																					}else{
																						echo "<td><input disabled type=\"checkbox\" ball=\"2\" class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
																						echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
																						echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
																					}
																				}
																				echo "</tr>";
																			}
																		?>
																													  
																	  </table>
															</div>
															*/ ?>
															
														</div>
														
													
													
												</div>
											</td>
										</tr>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="5_ball">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Choose Games</h3>
						</div>
						<div class="panel-body large-panel-scroll">
							<div class="col-sm-6">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th colspan="3">Early Houses</th>
										</tr>
										<?php
											$curr_time = new datetime('now');
											$early_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=1 AND g.number_of_balls=5 ORDER BY h.web_cutoff_time");
											
											if(count($early_houses) > 0){
												echo '
										<tr>
											<th><input type="checkbox" id="all_early_5_check"></th>
											<th>Houses</th>
											<th>Closing Time</th>
										</tr>
									</thead>
									<tbody>
												';
											}else{
												echo '
									</thead>
									<tbody>
										<tr>
											<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Early 5 Ball Games</td>
										</tr>
												';
											}
											
											for($x=0;$x<count($early_houses);$x++){
												echo "<tr>";
												//initialize the datetime for the cuttoff time
												$web_cutoff_time = new datetime($early_houses[$x]["web_cutoff_time"]);
												if($early_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
													echo "<td><input disabled type=\"checkbox\" ball=\"5\" class=\"game_".$early_houses[$x]["id"]."\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
													echo "<td><p class='text-muted'>".$early_houses[$x]["name"]."</p></td>";
													echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
												}else{
													if($web_cutoff_time > $curr_time){
														echo "<td><input class=\"early_5 game_".$early_houses[$x]["id"]."\" type=\"checkbox\" ball=\"5\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
														echo "<td><p>".$early_houses[$x]["name"]."</p></td>";
														echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
													}else{
														echo "<td><input disabled type=\"checkbox\" ball=\"5\" class=\"game_".$early_houses[$x]["id"]."\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
														echo "<td><p class='text-muted'>".$early_houses[$x]["name"]."</p></td>";
														echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
													}
												}
												echo "</tr>";
											}
										?>
									</tbody>
								</table>
							</div>
							<div class="col-sm-6">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th colspan="3">Late Houses</th>
										</tr>
										<?php
											$curr_time = new datetime('now');
											$late_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=0 AND g.number_of_balls=5 ORDER BY h.web_cutoff_time");
											
											if(count($late_houses) > 0){
												echo '
										<tr>
											<th><input type="checkbox" id="all_late_5_check"></th>
											<th>Houses</th>
											<th>Closing Time</th>
										</tr>
									</thead>
									<tbody>
												';
											}else{
												echo '
									</thead>
									<tbody>
										<tr>
											<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Late 5 Ball Games</td>
										</tr>
												';
											}
											
											for($x=0;$x<count($late_houses);$x++){
												echo "<tr>";
												//initialize the datetime for the cuttoff time
												$web_cutoff_time = new datetime($curr_time->format("Y-m-d")." ".$late_houses[$x]["web_cutoff_time"]);
												if($late_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
													echo "<td><input disabled type=\"checkbox\" ball=\"5\" class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
													echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
													echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
												}else{
													if($web_cutoff_time > $curr_time){
														echo "<td><input class=\"late_5 game_".$late_houses[$x]["id"]."\" type=\"checkbox\" ball=\"5\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
														echo "<td><p>".$late_houses[$x]["name"]."</p></td>";
														echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
													}else{
														echo "<td><input disabled type=\"checkbox\" ball=\"5\" class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
														echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
														echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
													}
												}
												echo "</tr>";
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="6_ball">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Choose Games</h3>
						</div>
						<div class="panel-body large-panel-scroll">
							<div class="col-sm-6">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th colspan="3">Early Houses</th>
										</tr>

										<?php
											$curr_time = new datetime('now');
											$early_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=1 AND g.number_of_balls=6 ORDER BY h.web_cutoff_time");
											
											if(count($early_houses) > 0){
												echo '
										<tr>
											<th><input type="checkbox" id="all_early_6_check"></th>
											<th>Houses</th>
											<th>Closing Time</th>
										</tr>
									</thead>
									<tbody>
												';
											}else{
												echo '
									</thead>
									<tbody>
										<tr>
											<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Early 6 Ball Games</td>
										</tr>
												';
											}
											
											for($x=0;$x<count($early_houses);$x++){
												echo "<tr>";
												//initialize the datetime for the cuttoff time
												$web_cutoff_time = new datetime($early_houses[$x]["web_cutoff_time"]);
												if($early_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
													echo "<td><input disabled type=\"checkbox\" ball=\"6\" class=\"game_".$early_houses[$x]["id"]."\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
													echo "<td><p class='text-muted'>".$early_houses[$x]["name"]."</td>";
													echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</td>";
												}else{
													if($web_cutoff_time > $curr_time){
														echo "<td><input class=\"early_6 game_".$early_houses[$x]["id"]."\" type=\"checkbox\" ball=\"6\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
														echo "<td><p>".$early_houses[$x]["name"]."</p></td>";
														echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
													}else{
														echo "<td><input disabled type=\"checkbox\" ball=\"6\" class=\"game_".$early_houses[$x]["id"]."\" id=\"game_".$early_houses[$x]["id"]."\"></td>";
														echo "<td><p class='text-muted'>".$early_houses[$x]["name"]."</p></td>";
														echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
													}
												}
												echo "</tr>";
											}
										?>
									</tbody>
								</table>
							</div>
							<div class="col-sm-6">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th colspan="3">Late Houses</th>
										</tr>

										<?php
											$curr_time = new datetime('now');
											$late_houses = $db->query("SELECT h.web_cutoff_time, h.plays_on_sunday, h.id, h.name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 AND h.is_early_house=0 AND g.number_of_balls=6 ORDER BY h.web_cutoff_time");
											
											if(count($late_houses) > 0){
												echo '
										<tr>
											<th><input type="checkbox" id="all_late_6_check"></th>
											<th>Houses</th>
											<th>Closing Time</th>
										</tr>
									</thead>
									<tbody>
												';
											}else{
												echo '
									</thead>
									<tbody>
										<tr>
											<td colspan="3" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">There are No Active Late 6 Ball Games</td>
										</tr>
												';
											}
											
											for($x=0;$x<count($late_houses);$x++){
												echo "<tr>";
												//initialize the datetime for the cuttoff time
												$web_cutoff_time = new datetime($curr_time->format("Y-m-d")." ".$late_houses[$x]["web_cutoff_time"]);
												if($late_houses[$x]["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0){
													echo "<td><input disabled type=\"checkbox\" ball=\"6\" class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
													echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
													echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
												}else{
													if($web_cutoff_time > $curr_time){
														echo "<td><input class=\"late_6 game_".$late_houses[$x]["id"]."\" type=\"checkbox\" ball=\"6\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
														echo "<td><p>".$late_houses[$x]["name"]."</p></td>";
														echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
													}else{
														echo "<td><input disabled type=\"checkbox\" ball=\"6\" class=\"game_".$late_houses[$x]["id"]."\" id=\"game_".$late_houses[$x]["id"]."\"></td>";
														echo "<td><p class='text-muted'>".$late_houses[$x]["name"]."</p></td>";
														echo "<td><p class='text-muted'>".$web_cutoff_time->format("h:i:s a")."</p></td>";
													}
												}
												echo "</tr>";
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade in" id="scheduled_bets">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Advance Play</h3>
						</div>
						<div class="panel-body large-panel-scroll" style="min-height: 250px; padding-left: 15px; padding-right: 15px;">							
							<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th class="text-left">Game</th>
										<th class="text-left">Pick</th>
										<th class="text-left">Bet Type</th>
										<th class="text-left">Bets Remaining</th>
										<th class="text-left">Amount</th>
										<th class="text-left">Next Drawing</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="6" class="dataTables_empty">Loading data from server</td>
									</tr>
								</tbody>
							</table>
							<button class="btn btn-magenta pull-left" id="schedule_advance_play">Schedule Advance Play</button>
						</div>
					</div>
				</div>
                                <div role="tabpanel" class="tab-pane fade in" id="lotto_history">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Detailed Lotto history</h3>
						</div>
					<!-- <div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div> -->

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div class="panel-body large-panel-scroll" style="min-height: 250px;overflow-x: scroll; padding-right: 15px; padding-left: 15px;">							
							<table id="lot_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th class="text-left">Purchase Date</th>
												<th class="text-left">Draw Date</th>
												<th class="text-left">Game</th>
												<th class="text-left">Ticket Number</th>
												<th class="text-left">Pick</th>
												<th class="text-left">Winning Draw</th>
												<th class="text-left">Bet Type</th>
												<th class="text-left">Bet Amount</th>
												<th class="text-left">Loyalty Points</th>
												<th class="text-left">Payout</th>
												<th class="text-left">Status</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="6" class="dataTables_empty">Loading data from server</td>
											</tr>
										</tbody>
									</table>							
						</div>
					</div>
				</div>
                              
                                <div role="tabpanel" class="tab-pane fade in" id="ticket_history">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Ticket Purchase History</h3>
						</div>
<!-- <div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div> -->

						<script type="text/javascript">
							$(function () {
								$("#ticket_start_date").on("dp.change", function (e) {
									$('#ticket_end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#ticket_end_date").on("dp.change", function (e) {
									$('#ticket_start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div class="panel-body large-panel-scroll" style="min-height: 250px;overflow-x: scroll; padding-left: 15px; padding-right: 15px;">							
							<table id="ticket_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th class="text-left">Purchase Date</th>
												<th class="text-left">Draw Date</th>
												<th class="text-left">Game</th>
												<th class="text-left">Ticket Number</th>
												<th class="text-left">Pick</th>
												<th class="text-left">Winning Draw</th>
												<th class="text-left">Bet Type</th>
												<th class="text-left">Bet Amount</th>
												
												<th class="text-left">Payout</th>
												<th class="text-left">Status</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="6" class="dataTables_empty">Loading data from server</td>
											</tr>
										</tbody>
									</table>							
						</div>
					</div>
				</div>


				<div role="tabpanel" class="tab-pane fade in" id="how_to_play">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">How To Play</h3>
						</div>
						<div class="panel-body large-panel-scroll" style="min-height: 250px; padding-right: 15px; padding-left: 15px;">
							<h3>How to Win</h3>

							<ol>
								<li>Select two, three, or four numbers (from 0 through 9).</li>
								<li>Select your play type — Straight (match in exact order), Box (match in any order), or Straight/Box. Select the amount you want to play.</li>
								<li>You can play the same numbers for more than one draw by placing your bets and then saving them as your "Favorites". Multiple different favorites can be saved for each user.</li>
								<li>Confirm that your bets are correct, and then click the Checkout button to charge the bets to your account.</li>
								<li>You may also Continue Betting, or Save your Favorites on this page.</li>
								<li>Once you have placed your bets any winnings will automatically be posted back onto the customer’s account. It is not necessary for a customer to go to a cashier for payout if the original bets were placed online or when the bets were charged to the customer’s account. Winnings can be obtained from any CSCLotto Location or though a Payment Processor of choice.</li>
							</ol>

							<p><b>Please check your order before you click the Checkout button. You are responsible for the accuracy of your order.</b></p>
							
							<h3>With purchasing tickets you accept our Terms and Conditions, in special: </h3>
							
							<ul>
								<li>Payouts will only be made on tickets purchased before houses are closed. </li>
								<li>We will not be held liable for inaccurate postings due to system error. </li>
								<li>Payouts will not be made on tickets due to system error.</li>
							</ul>
							
							
							<h3>Examples:</h3>
							
							<ul>
								<li>
									New York Early
									<br>Drawings are held seven days per week you can place bets up to 12:20 p.m
								</li>
								<li>
									Early Miami
									<br>Drawings are held seven days per week you can place bets up to 1:16 p.m.
								</li>
								<li>
									Early Chicago
									<br>Drawings are held seven days per week you can place bets up to 1:30 p.m.
								</li>
								<li>
									Early California
									<br>Drawings are held seven days per week you can place bets up to 3:45 p.m.
								</li>
								<li>
									Late New York
									<br>Drawings are held seven days per week you can place bets up to 7:25 p.m.
								</li>
								<li>
									Late Miami
									<br>Drawings are held seven days per week you can place bets up to 7:51 p.m.
								</li>
								<li>
									Late California
									<br>Drawings are held seven days per week you can place bets up to 9:30 p.m
								</li>
								<li>
									Late Chicago
									<br>Drawings are held seven days per week you can place bets up to 10:15 p.m.
								</li>
							</ul>

							<h3>How to Win</h3>
							
							<p>Players who match all of the winning numbers drawn in the official drawing for the date played will win. Actual amounts won will depend on the type of play purchased and the amount played.</p>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade in" id="fav_bets">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Favorite Bets</h3>
						</div>
						<div class="panel-body large-panel-scroll" style="min-height: 250px;">
							<div class="col-sm-12">						
								<?php
									$q = "SELECT *, f.id as fav_id, g.name AS game_name, h.id as game_id FROM `lotto_game` AS `g` LEFT JOIN `lotto_bet_favorites` AS `f` ON `f`.`game_id` = `g`.`id` LEFT JOIN `lotto_house` AS `h` ON `g`.`house_id` = `h`.`id` WHERE `user_id` = '".$session->userinfo['id']."' ORDER BY `game_name` ASC";
									$favorites = $db->query($q);
									if(count($favorites) >= 1){
								?>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th><input type="checkbox" id="all_fav_bets"></th>
											<th>Game</th>
											<th>Pick</th>
											<th>Bet Type</th>
											<th>Closing Time</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$curr_time = new datetime('now');
											foreach($favorites as $favorite){
												//$boxed_str = $favorite["is_boxed"] == 1 ? "Boxed" : "Straight";
												 $web_cutoff_time = new datetime($curr_time->format("Y-m-d")." ".$favorite["web_cutoff_time"]);
												if($favorite['is_boxed']==1){ $boxed_str="Boxed" ;}else {$boxed_str='Straight';}
												if($favorite['is_boxed']==2){ $boxed_str="Straight & Boxed"; }
												echo "<tr id='fav_row_".$favorite["fav_id"]."'>";
												echo "<td><input class=\"fav_check game_".$favorite["house_id"]."\" type=\"checkbox\" pick=\"".$favorite["pick"]."\" id=\"game_".$favorite["house_id"]."\"></td>";
												echo "<td><p>".$favorite["game_name"]."</p></td>";
												echo "<td><p>".$favorite["pick"]."</p></td>";
												echo "<td><p>".$boxed_str."</p></td>";
												echo "<td><p>".$web_cutoff_time->format("h:i:s a")."</p></td>";
												
												// add delete button
												echo "<td><button id='remove_fav_".$favorite["fav_id"]."' class='btn btn-sm btn-danger remove_fav'>Remove</button></td>";
												
												echo "</tr>";
											}
										?>
									</tbody>
								</table>
								
								<div class="panel clearfix">
									<div class="col-sm-4">
										<div class="form-group">
											<label for="fav_bet_amount">Bet Amount</label>
											<input type="text" class="form-control" id="fav_bet_amount" placeholder="$0.00">
										</div>
									</div>
								</div>
							</div>
							<?php									
								}else{
									echo "<span style=\"color: #fff\">No favorite bets have been created.<br><br></span>";
								}
							?>					
						</div>
						
						<div class="panel-footer">
						<?php
							if(count($favorites) >= 1){
						?>
							<button class="btn btn-magenta" id="add_fav_to_cart">Add to Cart</button>
						<?php
							}
						?>
							<button class="btn btn-primary pull-right" id="add_fav_bet" style="margin-right: 10px;">New Favorite Bet</button>
						</div>
					</div>
				</div>
				
				<!-- REDEEM VOUCHER !-->
			
				<div role="tabpanel" class="tab-pane fade in" id="redeem_voucher">
					<div  class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Redeem Ticket</h3>
							</div>
							<div class="panel-body large-panel-scroll" style="min-height: 250px;">
								<div class="col-sm-12">						
									
										<div class="col-sm-6">
											<input style="margin-bottom: 5px;" class="form-control" id="ticket"></input>
										</div>
										<div class="col-sm-6">
											<button style="margin-bottom: 5px;" class="btn btn-magenta" id="redeem_ticket">Redeem Ticket</button>
										</div>
										
								</div>
								<div id="redemption_errors" class="alert alert-danger col-sm-12" style="display:none; margin-bottom: 5px;">
						
								</div>
							
							</div>
					</div>

			</div>
			
			<!-- END !-->
				
				
			</div>
			
			<div id="choose_numbers_panel" class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Choose Numbers</h3>
				</div>
				<div class="panel-body">
					<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Number</h3>
							</div>
							<div class="panel-body">
								<div class="col-sm-12" style="padding-bottom: 10px;">
									<input type="text" class="form-control" id="number_text"<?php echo isset($_GET['number']) ? " value='".$_GET['number']."'": ""; ?>>
									
								</div>
								<div class="col-sm-12" style="text-align: center;">
									<button class="btn btn-default" id="button_qk2"<?php echo (isset($_GET['tab'])&&$_GET['tab']==2)||!isset($_GET['tab']) ? "": " style=\"display:none;\"";?>>Quick Pick 2</button>
									<button class="btn btn-default" id="button_qk3"<?php echo (isset($_GET['tab'])&&$_GET['tab']==2)||!isset($_GET['tab']) ? "": " style=\"display:none;\"";?>>Quick Pick 3</button>
									<button class="btn btn-default" id="button_qk4"<?php echo (isset($_GET['tab'])&&$_GET['tab']==2)||!isset($_GET['tab']) ? "": " style=\"display:none;\"";?>>Quick Pick 4</button>
									<button class="btn btn-default" id="button_qk5"<?php echo isset($_GET['tab'])&&$_GET['tab']==5 ? "": " style=\"display:none;\"";?>>Quick Pick 5</button>
									<button class="btn btn-default" id="button_qk6"<?php echo isset($_GET['tab'])&&$_GET['tab']==6 ? "": " style=\"display:none;\"";?>>Quick Pick 6</button>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Straight</h3>
							</div>
							<div class="panel-body">
								<input type="text" class="form-control" id="straight_amount_text" placeholder="0 CSC">
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Boxed</h3>
							</div>
							<div class="panel-body">
								<input type="text" class="form-control" value="" id="boxed_amount_text" placeholder="0 CSC">
							</div>
						</div>
					</div>
					<div id="add_cart_errors" class="alert alert-danger col-sm-12" style="display:none;">
					
					</div>
					<div class="push-right col-sm-12">
						<div class="col-sm-4">
						<button class="btn btn-magenta" id="button_add_cart" value="<?php echo isset($_GET['tab']) ? $_GET['tab'] : 2; ?>">Add to Cart</button>
						</div>
						
						<div class="col-sm-4">
							<button class="btn btn-magenta" id="button_all_triples" value="<?php echo isset($_GET['tab']) ? $_GET['tab'] : 2; ?>">All Triples</button>
							&nbsp;
							
							<button class="btn btn-magenta" id="button_add_hundread" value="<?php echo isset($_GET['tab']) ? $_GET['tab'] : 2; ?>">All Hundreds</button>
						</div>
						
						<a href="/lotto_payouts.php"><button class="btn btn-primary pull-right">Payouts & Odds</button></a>
					</div>
					<br>
						<div id="validation_err" class="alert alert-danger col-sm-10" style="display:none;margin-left:10px;margin-top:20px;">
						 	
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-3" style="padding-top: 15px; background:#fff !important;">
		<?php include($_SERVER['DOCUMENT_ROOT']."/balances.php"); ?>
	</div>	
	<div class="col-sm-12" style="background:#fff !important; padding-right: 0px; padding-left: 0px;">
		<div class="panel panel-default" style="background:#fff !important;">
			<div class="panel-heading clearfix"> 
				<span class="pull-left">
					<h3 class="panel-title">Cart</h3>
				</span>
				<span class="pull-right">
					<button class="btn btn-primary btn-sm" id="clear_bets_cart">Delete Highlited Bets</button>
					<button class="btn btn-primary btn-sm" id="button_clear_cart">Clear Cart</button>
				</span>
			</div>
			<div class="panel-body" style="padding-left: 15px; padding-right: 15px;>
				<div class="table-responsive">
					<table class="table table-bordered"  id="bet_table">
						<thead>
							<tr>
								<th>Pick</th>
								<th>Game</th>
								<th>S/B</th>
								<th>Amount</th>
								<th>Payout</th>
								<th>Delete</th>
							<tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" style="text-align: right; border-right: 0px;">Total:</td>
								<td colspan="3" style="border-left: 0px;"><span id="total">0</span> CSC</td>
							</tr>
						</tfoot>
					</table>
				</div>
				<div id="bet_error" class="alert alert-danger" role="alert" style="display:none;">Hello</div>
				<div id="error_reporting" class="alert alert-danger" role="alert" style="display:none;">
					
				</div>
				<button class="btn btn-magenta" id="purchase_bets_button"  data-loading-text="Purchasing...">Purchase</button>
			</div>
		</div>
	</div>
	<!-- <div class="col-sm-12 clearfix" style="background:#fff !important;">
		<?php
		
			$q = "SELECT `value` FROM `settings` WHERE `setting` = 'lottery_version';";
			$result = $db->queryOneRow($q);
		?>
		<p style="text-align: center; padding-top: 20px; color: white;">Lottery v<?php echo $result['value']; ?></p>
	</div> -->
</div>

<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
			</div>
			<div class="modal-body">
				Are you sure you would like to remove this bet from the cart?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="schedule_advance_play_modal" tabindex="-1" role="dialog" aria-labelledby="schedule_advance_play_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="schedule_advance_play_modal_label">Schedule Advance Play</h4>
			</div>
			<div class="modal-body clearfix">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="favorite_bet">Favorite Bet</label>
						<select name="favorite_bet" id="favorite_bet" class="form-control">
							<?php
								$q = "SELECT * FROM `lotto_bet_favorites` WHERE `user_id`=%i";
								$favorites = $db->query($q, array($session->userinfo['id']));
								
								foreach($favorites as $fav){
									if($fav['is_boxed']==1)
									{
										$fav_box='Boxed';
									}
									else if($fav['is_boxed']==2)
									{
										$fav_box='Straight & Boxed';
									}
									else
									{
										$fav_box='Straight';
									}
									
									echo '<option value="'.$fav['id'].'" data-boxed="'.$fav_box.'" data-pick="'.$fav['pick'].'">'.$fav['name'].' ( '.$fav_box.' ) </option>';
								}
							?>
						</select>
						<script>
							$(document).ready(function(){
								
								$('#favorite_bet').change(function(){
									$('#bet_string').val($('#favorite_bet option:selected').data('pick'));
									$('#bettype').val($('#favorite_bet option:selected').data('boxed'));
																		
								});
								
								$('#favorite_bet').trigger("change");
							});
						</script>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<label for="bet_string">Pick:</label>
						<input type="text" name="bet_string" id="bet_string" class="form-control" placeholder="123"></input>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<label for="bet_type">Bet Type:</label>
						<input type="text" name="bet_type" id="bettype" class="form-control" placeholder="S/B"></input>
					</div>
				</div>
				
				<div class="col-sm-12">
					<div class="form-group">
						<label for="advance_amount">Amount per bet?</label>
						<input type="text" name="advance_amount" id="advance_amount" class="form-control" placeholder="$0.00"></input>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<label for="number_of_drawings">How many times would you like to repeat this bet (Max of 7)?</label>
						<input type="text" name="number_of_drawings" id="number_of_drawings" class="form-control" maxlength="1"></input>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<label for="advance_total">Total: $</label>
						<input type="text" name="number_of_drawings" id="advance_total" class="form-control" disabled></input>
					</div>
				</div>
				<div id="advance_play_errors" class="alert alert-danger col-sm-12" style="display:none;">
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="confirm_advance_play">Purchase/Schedule</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="clear_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="clear_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="clear_confirmation_modal_label">Confirm Clearing the Cart</h4>
			</div>
			<div class="modal-body">
				Are you sure you would like to clear all of the bets in the cart?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="clear_confirm">Clear</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="bets_clear_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="bets_clear_confirmation_modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="bets_clear_confirmation_modal">Confirm Clearing Bets</h4>
			</div>
			<div class="modal-body">
				Are you sure you would like to delete all highlighted Bets?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="bet_clear_confirm">Delete</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add_fav_bet_modal" tabindex="-1" role="dialog" aria-labelledby="add_fav_bet_modal_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header clearfix">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="add_fav_bet_modal_label">New Favorite Bet</h4>
			</div>
			<div class="modal-body clearfix">
				<div class="col-sm-5">
					<div class="form-group">
						<label for="fav_game_id">Game:</label>
						<select id="fav_game_id" name="fav_game_id" class="form-control selectpicker">
							<option value=""></option>
							<?php
								$q = "SELECT * FROM lotto_game WHERE `id` > 0 AND `is_disabled` = 0 ORDER BY name ASC";
								$games = $db->query($q);
								foreach($games as $game){
									echo "<option value='".$game['id']."'>".$game['name']."</option>";
								}
							?>
						</select>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="fav_pick">Pick:</label>
						<input type="text" class="form-control" id="fav_pick" placeholder="123" maxlength="6">
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="fav_boxed">Type:</label>
						<select id="fav_boxed" name="fav_boxed" class="form-control selectpicker">
							<option value=""></option>
							<option value="1">Boxed</option>
							<option value="0">Straight</option>
                           <option value="2">Straight & Boxed</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer clearfix">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="fav_bet_confirm">Add Favorite</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal for Slider Pic Selection -->
	<div class="modal fade" id="slider_pic_modal" tabindex="-1" role="dialog" aria-labelledby="slider_pic_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="slider_pic_modal_label">Change Slider Photos</h4>

					<div id="customer_name2" class="pull-right" style="padding-right: 6px;">
						
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div id="upload_photos" class="col-sm-12">
						<?php include($_SERVER['DOCUMENT_ROOT'].'/upload_slider_photos.php'); ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	</div>
<?php
	}
	
	include("footer.php");
?>
