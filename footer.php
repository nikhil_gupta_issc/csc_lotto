			
		<footer>
			<div class="container" style="background: #000;
			color: #fff; margin-right: 0pxpx; margin-left: 0px; margin-bottom: 15px; padding-bottom: 10px; width: auto;">
				<div class="col-md-4">
					<h3 class="footer-nav-header">GAMES</h3>
					
					<ul class="footer-nav list-style-none">
						<li><a href="/lottery.php">Lottery</a></li>
                                                <?php if(!empty($b_rapidball_settings)){ ?>
						<li><a href="/fastballz.php">FastBallz</a></li>
                                                <?php } ?>
						<li><a href="/results.php">Winning Numbers</a></li>
						<li><a href="/about_us.php">About CSCLotto</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<h3 class="footer-nav-header">QUICK LINKS</h3>
					
					<ul class="footer-nav list-style-none">
						<li><a href="/news.php">News</a></li>
						<li><a href="/responsible_gaming.php">Responsible Gaming</a></li>
						<li><a href="/privacy_policy.php">Privacy Policy</a></li>
						<li><a href="/terms_conditions.php">Terms & Conditions</a></li>
					</ul>
				</div>
				
				<div class="col-md-4">
				
				<h3 class="footer-nav-header">FOLLOW US</h3>
				<ul class="footer-nav list-style-none">
					<li><p class="social_ico">
						<a target="_blank" href="https://www.facebook.com/CasinoCoin/"><img alt="Facebook" src="images/social/facebook.png" width="24" height="24" style="margin-left: 3px; margin-right: 3px;"></a>
						<a target="_blank" href="https://www.twitter.com/casinocoin"><img alt="Twitter" src="images/social/twitter.png" width="24" height="24" style="margin-left: 3px; margin-right: 3px;"></a>
						<a target="_blank" href="https://www.reddit.com/r/CasinoCoin"><img alt="Reddit" src="images/social/reddit.png" width="24" height="24" style="margin-left: 3px; margin-right: 3px;"></a>
						<a target="_blank" href="http://CasinoCoin.chat/"><img alt="Discord" src="images/social/discord.png" width="24" height="24" style="margin-left: 3px; margin-right: 3px;"></a>
						<a target="_blank" href="http://t.me/casinocoin"><img alt="Telegram" src="images/social/telegram.png" width="24" height="24" style="margin-left: 3px; margin-right: 3px;"></a>
						<a target="_blank" href="https://www.linkedin.com/company/11267967/"><img alt="Linkedin" src="images/social/linkedin.png" width="24" height="24" style="margin-left: 3px; margin-right: 3px;"></a>
						<a target="_blank" href="https://medium.com/@CSCFoundation"><img alt="Medium" src="images/social/medium.png" width="24" height="24" style="margin-left: 3px; margin-right: 3px;"></a>
						<a target="_blank" href="https://www.youtube.com/CasinoCoin"><img alt="YouTube" src="images/social/youtube.png" width="24" height="24" style="margin-left: 3px; margin-right: 3px;"></a>
					</p></li>
					
					<br />
					<!-- <button type="button" onclick="$('#dispute_modal').modal('show')" class="btn-primary">Create Dispute</button><br><br></ul> -->
				</div>
				
				<div class="col-md-12" style="text-align: center;">
					Your IP Address of <?php echo $_SERVER['REMOTE_ADDR']; ?> has been logged.
				</div>
			</div>
		</footer>
		
		<script>
			$(document).ready(function(){
				$(document).on("click", "#submit_dispute", function(){
					$(this).prop("disabled", true);
					$.ajax({
						url: "/ajax/dispute.php",
						data: {
							action : "submit",
							from_name : $("#dispute_from_name").val(),
							from_email : $("#dispute_from_email").val(),
							user_id : <?php echo $session->logged_in ? $session->userinfo['id'] : -1; ?>,
							subject : $("#dispute_subject").val(),
							message : $("#dispute_message").val()
						},
						method: "POST",
						dataType: "json"
					})
					.done(function(data){
						if(data.success == true){
							bootbox.alert("Your dispute was submitted. You will recieve an email with a response to your dispute.");
							$("#dispute_modal").modal("hide");
						}else{
							bootbox.alert(data.errors);
						}
						$("#submit_dispute").prop("disabled", false);
					});
				});
			});
		</script>
		
		<div class="modal fade" id="dispute_modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Create Dispute</h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="dispute_from_name" class="col-sm-3 control-label">Your Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="dispute_from_name" placeholder="Your Name" value="<?php echo $session->logged_in ? $session->userinfo['firstname']." ".$session->userinfo['lastname'] : ""; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="dispute_from_email" class="col-sm-3 control-label">Email Address</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="dispute_from_email" placeholder="Your Email Address" value="<?php echo $session->logged_in ? $session->userinfo['email'] : ""; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="subject" class="col-sm-3 control-label">Subject</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="dispute_subject" placeholder="Subject">
								</div>
							</div>
							<div class="form-group">
								<label for="dispute" class="col-sm-3 control-label">Dispute</label>
								<div class="col-sm-9">
									<textarea class="form-control" id="dispute_message" placeholder="Please describe the problem in detail."></textarea>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-magenta" id="submit_dispute">Submit</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</body>
</html>
