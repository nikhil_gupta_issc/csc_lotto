<?php
	$page_title = "Invalid Location";
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	include($_SERVER['DOCUMENT_ROOT'].'/page_top.php');

	if($session->logged_in){
		$session->logout();	
	}
?>
	<div id="wrapper" class="clearfix">
		<div style="text-align:center">
			<h3>You must online to access CSCLotto. If you are, be sure that location tracking is enabled in your browser.</h3>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); 
?>
