$(document).ready(function() {
	$("#picture_webcam").scriptcam({
		path: 'ScriptCam/',
		showMicrophoneErrors:false,
		showDebug:true,
		onError:onError,
		cornerRadius:20,
		disableHardwareAcceleration:1,
		cornerColor:'e3e5e2',
		uploadImage:'ScriptCam/upload.gif',
		width:550,
		height:400,
		onPictureAsBase64:base64_tofield_and_image
	});
});
var max_time = 3;
var cinterval;	
function countdown_before_base64_toimage(){
    cinterval = setInterval(function(){
    max_time--;
    document.getElementById('countdown').innerHTML = max_time;
    if(max_time == 0){
        max_time = 3;
        document.getElementById('countdown').innerHTML = max_time;
        clearInterval(cinterval);
        base64_toimage();
    }
    },1000)
}
		/*
function countdown_before_base64_toimage() {
	var secondscounter = 3,
    countdown = setInterval(function(){ 
    $('#countdown').html( secondscounter-- );
    if ( !secondscounter ) {
        clearInterval( countdown ); 
        base64_toimage();
    }
    }, 1000 );
	
	var interval;
    var countdown = 4;
	interval = setInterval(function() {
		$('#countdown').html(--countdown);
		if(countdown === 0){
			base64_toimage();
			countdown = 4;
		};
	}, 1000);
	return false;
}*/
function base64_toimage() {
	$('#arrow_img_diagonal').hide();
	$('#arrow_img').hide();
	$('#preview_image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64());
	var preview_image = "data:image/png;base64,"+$.scriptcam.getFrameAsBase64();
	$.ajax({
		url: 'upload_photo.php',
		type: 'POST',
		data: { preview_image: preview_image },
		success: function(response) {
			window.top.window.close_popup('popup_webcam');
			window.top.window.show_popup_crop(response);
			//console.log('Sent images on: '+ response );
		}
	});
}
function base64_tofield_and_image(b64) {
	$('#preview_image').attr("src","data:image/png;base64,"+b64);
}
function onError(errorId,errorMsg) {
	$( "#btn1" ).attr( "disabled", true );
	$( "#btn2" ).attr( "disabled", true );
	alert(errorMsg);
}
