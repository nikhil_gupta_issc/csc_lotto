			$(document).ready(function() {
				$("#video_webcam").scriptcam({
					path: './ScriptCam/',
					fileReady:fileReady,
					cornerRadius:20,
					cornerColor:'e3e5e2',
					onError:onError,
					promptWillShow:promptWillShow,
					showMicrophoneErrors:false,
					showDebug:true,
					maximumTime: 20,
					timeLeft:timeLeft,
					fileName:'demo109230',
					connected:showRecord
				});
			});
			function showRecord() {
				$( "#recordStartButton" ).attr( "disabled", false );
			}
			function startRecording() {
				$('#arrow_img_diagonal').hide();
				$('#arrow_img').hide();
				$( "#recordStartButton" ).attr( "disabled", true );
				$.scriptcam.startRecording();
			}
			function stopVideoRecording() {
				//$('#recorder').hide();
				$.scriptcam.closeCamera();
				$('#message').html('<img src="images/loader.gif" /><h3>Please wait for the file conversion to finish.</h3>');
			}
			function videoHTML(videoSrc) {
				return '<video id="video-js" class="video-js vjs-default-skin" ' +
					'controls preload="auto" width="400" height="300" ' +
					'data-setup=\'{"example_option":true}\'>' +
					'\t<source src="'+videoSrc+'" type="video/mp4" /> \n' +
					'\t\t<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>\n' +
				'</video>';
			}
			function fileReady(fileName) {
				$('#recorder').hide();
				//$('#message').html('<h3>The MP4 file is now dowloadable for five minutes over <a href="'+fileName+'">here</a>.</h3>');
				$('#message').empty();
				var fileNameNoExtension=fileName.replace(".mp4", "");
				$('div#mediaplayer').append(videoHTML(fileName));
				player = videojs('#video-js');
				$('#mediaplayer').show();
				/*jwplayer("mediaplayer").setup({
					width:320,
					height:240,
					file: fileName,
					image: fileNameNoExtension+'_0000.jpg',
					'modes': [{ type: "html5", src: "html5/jwplayer.html5.js",
								config: {
									file: fileName
								}
							},
							//I commented this line out so it doesn't fallback to flash
							//{ type: "flash", src: "/jwplayer/player.swf" },
					],
					tracks: [{ 
						file: fileNameNoExtension+'.vtt', 
						kind: 'thumbnails'
					}]
				});*/
			}
			function save_video(){
				//alert("wait we are saving your recording into database");
				$('#message').html('<img src="images/loader.gif" /><h3>Please wait..we are saving recording to database.</h3>');
				var x=player.currentSrc();
				if (x != null){
					$.ajax({
						url: 'download_video.php',
						type: 'POST',
						data: { x: x },
						success: function(response) {
							$('div#video_container').html(videoHTML(response));
							player = videojs('#video-js');
							window.top.window.close_popup('popup_video_recorder');
							//console.log(response );
						}
					});
				}else{
					alert("No Video URL Found");
				}
			}
			function onError(errorId,errorMsg) {
				alert(errorMsg);
			}
			function promptWillShow() {
				alert('A security dialog will be shown. Please click on ALLOW.');
			}
			function timeLeft(value) {
				$('#timeLeft').val(value);
				if(value=='00:00'){
					stopVideoRecording();
				}
			}
			function showRecorder(){
				$('#mediaplayer').hide();
				$('#recorder').show();
			}