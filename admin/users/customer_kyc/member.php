<?php
include_once "config.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Upload User Picture</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Latest compiled and minified CSS -->

<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jquery.Jcrop.min.css" type="text/css" />


<script src="jeditable/jquery.editable.js"></script>


<script src="js/jquery.Jcrop.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
//loading different js as per tab selection


$(function() {
	
	var url=$('#photo_container').attr('src');
	//var customer_id = data_table.$('tr.selected').attr('id').replace("row_", "");
	//alert(customer_id);
	$('.show_js_css_video').hide();
	var pic_js = ["js/jquery.Jcrop.min.js", "ScriptCam/swfobject.js", "ScriptCam/scriptcam.js", "ScriptCam/picture_scriptcam.js"];
	var $head = $(".show_js_css_picture");
	for (var i = 0; i < pic_js.length; i++) {
		$head.append("<script src=\"" + pic_js[i] + "\" type=\"text/javascript\"></scr" + "ipt>");
	}
	$('#tabs li a').click(function (e) {
	
		if(e.target.hash=="#picture"){
				
			//empty video js
			$(".show_js_css_video").empty();
			$('.show_js_css_video').hide();
			//reload pic js
			$('.show_js_css_picture').empty();
			var pic_js = ["js/jquery.Jcrop.min.js", "ScriptCam/swfobject.js", "ScriptCam/scriptcam.js", "ScriptCam/picture_scriptcam.js"];
			var $head = $(".show_js_css_picture");
			for (var i = 0; i < pic_js.length; i++) {
				$head.append("<script src=\"" + pic_js[i] + "\" type=\"text/javascript\"></scr" + "ipt>");
			}
		}else if(e.target.hash=="#video"){
			//empty picture js
			$(".show_js_css_picture").empty();
			$('.show_js_css_picture').hide();
			//reload video js
			var js = ["js/jquery-ui.min.js", "ScriptCam/swfobject.js", "ScriptCam/scriptcam.js", "ScriptCam/video_scriptcam.js", "video-js/video.js", "video-js/init_video_callback.js"];
			var $head = $(".show_js_css_video");
			for (var i = 0; i < js.length; i++) {
				$head.append("<script src=\"" + js[i] + "\" type=\"text/javascript\"></scr" + "ipt>");
			}
		}
	});
});

$(document).on('click','.web-image',function(){
	
	$('.image-id').removeClass('active_image');
	$(this).closest('.image-id').addClass('active_image');
});

</script>
<script>
	
	$(document).on('click','#pop',function(){
		
	
   $('#imagepreview').attr('src', $('#photo_container').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
   
});   
   $(document).on('click','#pop_id_proof',function(){
	
   $('#imagepreview').attr('src', $('#photo_container_id').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
   
});
	
</script>

</head>

<body>
	
	<div class="row">
	<div class="col-sm-12">
	<label class="col-sm-3 control-label" for="profile">Upload Profile Image <i class="fa fa-picture-o" id="pop" aria-hidden="true"></i></label>
			
	<label class="col-sm-3 control-label" for="id">Passport <i class="fa fa-picture-o" id="pop_id_proof" aria-hidden="true"></i></label>
	
	<label class="col-sm-3 control-label" for="id">Driving License <i class="fa fa-picture-o" id="pop_driving_proof" aria-hidden="true"></i></label>
	
	<label class="col-sm-3 control-label" for="id">NSB/NIB <i class="fa fa-picture-o" id="pop_nsb" aria-hidden="true"></i></label>
		  
			</div>
			<div class="header col-sm-12">
			
				<div class="col-sm-3 image-id">
					<img class="web-image" name="photo_container" id="photo_container" src="" onclick="show_popup('popup_upload')" width="205px" height="170px" />
				</div>
		    
				<div class="col-sm-3 image-id">
					<img class="web-image" name="photo_container_id" id="photo_container_id" src="" onclick="show_popup('popup_upload')" width="205px" height="170px" />
				</div>
				
				<div class="col-sm-3 image-id">
					<img class="web-image" name="photo_container_driving" id="photo_container_driving" src="" onclick="show_popup('popup_upload')" width="205px" height="170px" />
				</div>
				
				<div class="col-sm-3 image-id">
					<img class="web-image" name="photo_container_nsb" id="photo_container_nsb" src="" onclick="show_popup('popup_upload')" width="205px" height="170px" />
				</div>
				
				
			</div>
		</div>
		
	
	<!-- The popup for upload new photo -->
    <div id="popup_upload">
        <div class="form_upload">
            <span class="close" onclick="close_popup('popup_upload')">x</span>
            <h2>Upload Picture</h2>
            <form id="upload_form" action="upload_photo.php" method="post" enctype="multipart/form-data" target="upload_frame">
               <div class="upload_file col-sm-12" style="text-align:center;">
                <!--<input type="file" name="photo" id="photo" class="upload_btn" >-->
              </div>
                <div id="loading_progress"></div>
            </form>
            <iframe name="upload_frame" class="upload_frame"></iframe>
                    <div class="col-sm-8" style="text-align:left;">
			<!--OR-->
                       </div>
<div class="col-sm-8" style="text-align:center;">
			<button type="button" class="btn btn-primary pull-left" onclick="show_popup('popup_webcam');close_popup('popup_upload');" style="margin-bottom: 20px;">photo webcam</button>
</div>
        </div>
    </div>

    <!-- The popup for crop the uploaded photo -->
     <div id="popup_crop">
        <div class="form_crop">
            <span class="close" onclick="close_popup('popup_crop')">x</span>
            <h2>Crop photo</h2>
            <!-- This is the image we're attaching the crop to -->
            <img id="cropbox" />
            
            <!-- This is the form that our event handler fills -->
            <form>
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <input type="hidden" id="photo_url" name="photo_url" />
                <input type="button" value="Crop Image" id="crop_btn" onclick="crop_photo()" />
            </form>
        </div>
    </div>
    
    
    
   <!-- POPUP FOR VIEW IMAGE -->
   
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"></span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" style="width: 450px; height: 350px;" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    <!-->
    
	<!-- The popup for showing webcame image -->
	<div id="popup_webcam">
	
	 
	 <div class="form_crop">
	    <link rel="stylesheet" href="css/jquery.Jcrop.min.css" type="text/css" />
		<div class="show_js_css_picture"></div>
		<span class="close" onclick="close_popup('popup_webcam')">x</span>
		<h3>Take a Picture</h3>
		<div id="picture_webcam"></div>
		<br/>
		<div id="countdown"></div>
		<br/><br/>
		<p style="text-align:center;font-size:12px">
			<button onclick="countdown_before_base64_toimage()" class="btn btn-primary" id="btn1">Take a picture after 3 seconds</button>
			<button onclick="base64_toimage()" class="btn btn-primary" id="btn2">Take a picture instantly</button>
		</p>
               <div style="text-align:center;">
		<img id="preview_image" name="preview_image" style="width:200px;height:153px;"/>
             </div>
	  </div>
	</div>
	
</body>
</html>
