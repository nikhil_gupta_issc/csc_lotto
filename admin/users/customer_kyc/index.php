<?php
	$page_title = "Manage Customers";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	include_once($_SERVER['DOCUMENT_ROOT'].'/lib/assets/jquery-file-upload/dependencies.php');
?>

	<script type="text/javascript">
		$(document).ready(function(){
	
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			var forced_filter = "";
			
			// main datatable initialization
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/manage_customers.php",
				"columns": [
						{ "data": "customer_name" },
				/*	{ "data": "available_balance" },
					{ "data": "bonus_balance" },
					{ "data": "island" },*/
					{ "data": "status" },
					{ "data": "kyc_verified" },
				/*	{ "data": "customer_number" },
					{ "data": "email" },
					{ "data": "phones" },
					{ "data": "status_details" }*/
				],
				"order": [[1, 'asc']]

			});
		
			// temporary transactions datatable initialization
			// definition redefined on button click
			var trans_data_table = $('#transactions_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				//bInfo: false,
				"ajax": "/admin/ajax/datatables/manage_customers_transactions.php",
				"columns": [
					{ "data": "transaction_date" },
					{ "data": "transaction_type" },
					{ "data": "transaction_details" },
					{ "data": "amount" }
				]
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var row_array = data_table.row(this).data();
				
				if(row_array['status'] == "<span style='color:green;'>Enabled</span>"){
					$('#disable_btn').text("Disable");
				}else if(row_array['status'] == "<span style='color:orange;'>Locked</span>"){
					$('#disable_btn').text("Unlock");
				}else{
					$('#disable_btn').text("Enable");
				}
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			$('#disable_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				
				$.ajax({
					url: "/admin/ajax/datatables/manage_customers_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
		<!-- Add/Edit Modal Start -->	
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				forced_filter = "";
				
				$('#submit_edit').html("Save Changes");
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				console.log(columns);
				$('#user_id').val(columns['user_id']).combobox('refresh');
				$('#modal_row_id').html("ID #"+id);
				$('#customer_num').val(columns['customer_number']);
				$('#firstname').val(columns['firstname']);
				$('#midname').val(columns['midname']);
				$('#lastname').val(columns['lastname']);
				$('#gender').val(columns['gender']).change();
				$('#date_of_birth').data("DateTimePicker").clear().date(moment(columns['date_of_birth']));
				$('#address').val(columns['address']);
				$('#address2').val(columns['address2']);
				$('#city').val(columns['city']);
				$('#island_id').val(columns['island_id']).change();
				$('#country').val(columns['country']);
				$('#email').val(columns['email']);
				$('#telephone').val(columns['telephone']);
				$('#cellphone').val(columns['cellphone']);
				$('#available_balance').val(columns['available_balance']);
				$('#bonus_balance').val(columns['bonus_balance']);
				
				// show previously saved pic if its valid
				if(columns['profile_picture_path']){
					$("#user_pic").attr("src", columns['profile_picture_path']+"&auth=<?php echo sha1(md5($session->userinfo['email'].$session->userinfo['id'].date('Ymdh'))); ?>");
				}else{
					// $("#user_pic").attr("src", "/imaages/CSCLotto_chip.png"); //default to logo for CSCLotto employees
					$("#user_pic").attr("src", "/admin/images/generic-user-purple.png"); //default to generic icon for customers
				}
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#name').focus();
				}, 500);
			})

			$(document).on('click', '#submit_edit', function() {				
				var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
				
				var columns = data_table.row('.selected').data();
				console.log(columns);
				
				var dob = "";
				if($('#date_of_birth').data("DateTimePicker").date()){
					dob = $('#date_of_birth').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// deteremine whether this is update or insert
				if(id > 0){
					var action = "update";
					$.ajax({
						url: "/admin/ajax/datatables/manage_customers_buttons.php",
						type: "POST",
						data: {
							action : action,
							cust_id : id,
							user_id : $('#user_id').val(),
							customer_number : $('#customer_num').val(),
							firstname : $('#firstname').val(),
							midname : $('#midname').val(),
							lastname : $('#lastname').val(),
							gender : $('#gender').val(),
							date_of_birth : dob,
							address : $('#address').val(),
							address2 : $('#address2').val(),
							city : $('#city').val(),
							island_id : $('#island_id').val(),
							country : $('#country').val(),
							email : $('#email').val(),
							telephone : $('#telephone').val(),
							cellphone : $('#cellphone').val(),
							bonus_balance : $('#bonus_balance').val()
						},
						dataType: "json",
						success: function(response){
							if(response['result'] != false){
								// hide buttons and reload table if successful
								$('.btn-row-action').hide();
								data_table.draw(true);
								
								$('#edit_row_modal').modal('hide');
							}else{
								alert("Operation Failed!\n"+response['query']);
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							alert("Operation Failed!");
						}
					});
				}else{
					var action = "insert";
					
					
				}
			});
			
			$(document).on("change", "#user_id", function(){
				console.log($('#user_id').val());
				if($(this).val() == ""){
					$('#new_user_password').show();
					$('#new_user_force_change').show();
				}else{
					$('#new_user_password').hide();
					$('#new_user_force_change').hide();
				}
			});
		<!-- Add/Edit Modal End -->	
			
		<!-- Login Account Modal Start -->
			$(document).on("change", "#locked", function(){
				if($("#locked").is(":checked")){
					// set expiration to use default lockout time
					var expire_moment = moment().add(<?php echo LOCKED_MINUTES; ?>, 'minutes');;
					$('#locked_expiration').data("DateTimePicker").clear().date(expire_moment);
					
					$("#lockout_expiration_contain").slideDown("fast");
				}else{
					$("#lockout_expiration_contain").slideUp("fast");
				}
			});
		
			$('#login_btn').click(function(){	
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				// grab user info from database and store in session
				$.ajax({
					url: "/admin/ajax/get_customer_user_info.php",
					type: "POST",
					data: {
						user_id : columns['user_id']
					},
					dataType: "json",
					success: function(response){						
						if(response['success'] == true){
							// load data to modal
							$('#customer_name').html(columns['customer_name']);
							$('#pin').val(response['customer_info']['pin']);
							$('#google_auth_secret').html(response['user_info']['google_auth_secret']);
							$('#userlevel').val(response['user_info']['userlevel']).change();
							
							if(response['user_info']['locked'] == true){
								$('#locked').bootstrapToggle('on');
								var expire_moment = new moment(response['user_info']['locked_expiration']);
								$('#locked_expiration').data("DateTimePicker").clear().date(expire_moment);
							}
							if(response['user_info']['google_auth_enabled'] == true){
								$('#google_auth_enabled').bootstrapToggle('on');
							}
							if(response['user_info']['force_password_change'] == true){
								$('#force_password_change').bootstrapToggle('on');
							}
							
							// show modal
							$('#login_modal').modal('show');
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					}
				});
			});
			
			$('#login_save').click(function(){
				// get database id of selected row
				var customer_id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				var locked_expire = "";
				if($('#locked_expiration').data("DateTimePicker").date()){
					locked_expire = $('#locked_expiration').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss');
				}
		
				$.ajax({
					url: "/admin/ajax/datatables/manage_customers_buttons.php",
					type: "POST",
					data: {
						action : "update_security",
						id : customer_id,
						user_id : columns['user_id'],
						pin : $('#pin').val(),
						locked : +$('#locked').is(":checked"),
						google_auth_enabled : +$('#google_auth_enabled').is(":checked"),
						force_password_change : +$('#force_password_change').is(":checked"),
						locked_expiration: locked_expire,
						pass : $('#pass').val(),
						conf_pass : $('#conf_pass').val(),
                                                userlevel : $('#userlevel').val()
					},
					dataType: "json",
					success: function(response){						
						if(response['result'] != false){
							$('#login_modal').modal('hide');
							data_table.draw(true);							
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					}
				});
			});
		<!-- Login Account Modal End -->
		
		<!-- Transactions Modal Start -->
			// start date change
			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				//console.log("from: "+start_date+" to: "+end_date);
				
				var columns = data_table.row('.selected').data();
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#transactions_datatable')){
					trans_data_table.destroy();
				}
				
				// transactions datatable initialization
				trans_data_table = $('#transactions_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					//bInfo: false,
					"ajax": "/admin/ajax/datatables/manage_customers_transactions.php?user_id="+columns['user_id']+"&forced_filter="+forced_filter+"&filter_date_from="+start_date+"&filter_date_to="+end_date,
					"columns": [
						{ "data": "transaction_date" },
						{ "data": "transaction_type" },
						{ "data": "transaction_details" },
						{ "data": "amount" }
					],
					"order": [[1, 'desc']]
				});
				
				trans_data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				//console.log("from: "+start_date+" to: "+end_date);
				
				var columns = data_table.row('.selected').data();
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#transactions_datatable')){
					trans_data_table.destroy();
				}
				
				// transactions datatable initialization
				trans_data_table = $('#transactions_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					//bInfo: false,
					"ajax": "/admin/ajax/datatables/manage_customers_transactions.php?user_id="+columns['user_id']+"&forced_filter="+forced_filter+"&filter_date_from="+start_date+"&filter_date_to="+end_date,
					"columns": [
						{ "data": "transaction_date" },
						{ "data": "transaction_type" },
						{ "data": "transaction_details" },
						{ "data": "amount" }
					],
					"order": [[1, 'desc']]
				});
				
				trans_data_table.draw(true);
			});
		
			$('#transactions_modal').on('shown.bs.modal', function() {
				//recalculate the dimensions of datatable
				trans_data_table.columns.adjust().responsive.recalc();
			});
			
			$('#transactions_btn').click(function(){
				var columns = data_table.row('.selected').data();
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#transactions_datatable')){
					trans_data_table.destroy();
				}
				
				// set modal title
				$('#transactions_modal_label').html("Transactions");
				
				// transactions datatable initialization
				trans_data_table = $('#transactions_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					//bInfo: false,
					"ajax": "/admin/ajax/datatables/manage_customers_transactions.php?user_id="+columns['user_id'],
					"columns": [
						{ "data": "transaction_date" },
						{ "data": "transaction_type" },
						{ "data": "transaction_details" },
						{ "data": "amount" }
					],
					"order": [[1, 'desc']]
				});
				
				trans_data_table.draw(true);
				
				// show modal
				$('#transactions_modal').modal('show');
			});
		<!-- Transactions Modal End -->
		
		<!-- Withdrawals / Deposits Modal Start -->			
			$('#withdraw_btn').click(function(){
				forced_filter = "withdraw";
				
				var columns = data_table.row('.selected').data();
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#transactions_datatable')){
					trans_data_table.destroy();
				}
				
				// set modal title
				$('#transactions_modal_label').html("Withdrawals");
				
				// transactions datatable initialization
				trans_data_table = $('#transactions_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					//bInfo: false,
					"ajax": "/admin/ajax/datatables/manage_customers_transactions.php?user_id="+columns['user_id']+"&forced_filter="+forced_filter,
					"columns": [
						{ "data": "transaction_date" },
						{ "data": "transaction_type" },
						{ "data": "transaction_details" },
						{ "data": "amount" }
					],
					"order": [[1, 'desc']]
				});
				
				trans_data_table.draw(true);
				
				// show modal
				$('#transactions_modal').modal('show');
			});
			
			$('#deposit_btn').click(function(){
				forced_filter = "deposit";
				
				var columns = data_table.row('.selected').data();
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#transactions_datatable')){
					trans_data_table.destroy();
				}
				
				// set modal title
				$('#transactions_modal_label').html("Deposits");
				
				// transactions datatable initialization
				trans_data_table = $('#transactions_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					//bInfo: false,
					"ajax": "/admin/ajax/datatables/manage_customers_transactions.php?user_id="+columns['user_id']+"&forced_filter="+forced_filter,
					"columns": [
						{ "data": "transaction_date" },
						{ "data": "transaction_type" },
						{ "data": "transaction_details" },
						{ "data": "amount" }
					],
					"order": [[1, 'desc']]
				});
				
				trans_data_table.draw(true);
				
				// show modal
				$('#transactions_modal').modal('show');
			});
		<!-- Withdrawals / Deposits Modal End -->
		
		<!-- KYC Modal Start -->
			$('#verify_btn').click(function(){
				
				$('#loading-spinner').show();
				
				
				// wipe out upload photos modal so there are no conflicts
				$('#upload_user_photos').html('');
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				// change upload user id so KYC Verify button loads correct files
				$.ajax({
					url: "/admin/ajax/upload_user_id_change.php",
					type: "POST",
					data: {
						user_id : columns['user_id'],
						checksum : columns['upload_checksum']
					},
					success: function(response){						
						$.ajax({
							url: "/admin/ajax/upload_user_id_change.php",
							type: "POST",
							data: {
								user_id : columns['user_id'],
								checksum : columns['upload_checksum'],
								handler : 'verification'
							},
							success: function(response){
								
								// reload data from upload_files script into div
									$('#upload_files').html(response).promise().done(function(){
									$('#modal_row_id').html("ID #"+id);
									$('#customer_name').html(columns['customer_name']);
									$('#drivers_license').val(columns['drivers_license']);
									$('#voters_card').val(columns['voters_card']);
									$('#passport').val(columns['passport']);
									$('#national_security').val(columns['national_security']);
								
								/* Check file Path is NULL for Profile Pic  */	
								
									var image_file_path=columns['profile_picture_path'];
									if(image_file_path == null)
									{
										$('#photo_container').attr('src','');
									}
									else
									{
										$('#photo_container').attr('src',columns['profile_picture_path']);
										
									}
									
								/* Check file Path is NULL for Id Proof */	
								
									var id_file_path=columns['id_image_path'];
									if(id_file_path == null)
									{
										
										$('#photo_container').attr('src','');
									}
									else
									{
										$('#photo_container_id').attr('src',columns['id_image_path']);
										
									}
									
									
									/* Check file Path is NULL for Driving Proof */	
								
									var id_driving_path=columns['id_driving_path'];
									if(id_driving_path == null)
									{
										
										$('#photo_container_driving').attr('src','');
									}
									else
									{
										$('#photo_container_driving').attr('src',columns['id_driving_path']);
										
									}
									
									/* Check file Path is NULL for NSB Proof */	
								
									var id_nsb_path=columns['id_nsb_path'];
									if(id_nsb_path == null)
									{
										
										$('#photo_container_nsb').attr('src','');
									}
									else
									{
										$('#photo_container_nsb').attr('src',columns['id_nsb_path']);
										
									}
									
									
								//	$('#photo_container').attr('src',columns['profile_picture_path']);
								//	$('#photo_container_id').attr('src',columns['id_image_path']);
									
									// toggle verification checkboxes
									if(columns['is_drivers_license_verified'] == true){
										$('#verified_drivers_license').bootstrapToggle('on');
									}
									if(columns['is_voters_card_verified'] == true){
										$('#verified_voters_card').bootstrapToggle('on');
									}
									if(columns['is_passport_verified'] == true){
										$('#verified_passport').bootstrapToggle('on');
									}
									if(columns['is_national_security_verified'] == true){
										$('#verified_national_security').bootstrapToggle('on');
									}
								 /* Code for Hide Show View Image Icon */   
								    var url1=$('#photo_container').attr('src');
								    var url2=$('#photo_container_id').attr('src');
								    var url3=$('#photo_container_driving').attr('src');
								    var url4=$('#photo_container_nsb').attr('src');
								   
										if(url1=='')
										{
											$('#pop').hide();
											 url1='';
											 $("#photo_container").attr('src','images/default_user_avatar.png');						
										}	
										else
										{
											$('#pop').show();
											 url1='';			
										}
									
									   if(url2=='')
										{
											$('#pop_id_proof').hide();
											 url2='';	
											  $("#photo_container_id").attr('src','images/default_user_avatar.png');			
										}
										else
										{
										
											$('#pop_id_proof').show();
											url2='';			
										}
										
										
										 if(url3=='')
										{
											$('#pop_driving_proof').hide();
											 url3='';	
											  $("#photo_container_driving").attr('src','images/default_user_avatar.png');			
										}
										else
										{
										
											$('#pop_driving_proof').show();
											url3='';			
										}
										
										
										 if(url4=='')
										{
											$('#pop_nsb').hide();
											 url4='';	
											  $("#photo_container_nsb").attr('src','images/default_user_avatar.png');			
										}
										else
										{
										
											$('#pop_nsb').show();
											url4='';			
										}
								   /*End Code for Hide Show View Image Icon */
								   
										$("#verify_modal").modal("show");
										$('#loading-spinner').hide();
									});
								}
							});
						}
					});
				});
											
				$('#verify_save').click(function(){
				// get database id of selected row
				var customer_id = data_table.$('tr.selected').attr('id').replace("row_", "");
		
				$.ajax({
					url: "/admin/ajax/datatables/manage_customers_buttons.php",
					type: "POST",
					data: {
						action : "update_verification",
						id : customer_id,  
					
						id_image_path : $('#photo_container_id').attr('src'), // id proof 
						
						profile_picture_path : $('#photo_container').attr('src'), // profile pic
						
						driving_picture_path : $('#photo_container_driving').attr('src'), // Driving Picture Path
						
						nsb_picture_path : $('#photo_container_nsb').attr('src'), // Driving Picture Path
						
						drivers_license : $('#drivers_license').val(),
						voters_card : $('#voters_card').val(),
						passport : $('#passport').val(),
						national_security : $('#national_security').val(),
						verified_drivers_license : +$('#verified_drivers_license').is(":checked"),
						verified_voters_card : +$('#verified_voters_card').is(":checked"),
						verified_passport : +$('#verified_passport').is(":checked"),
						verified_national_security : +$('#verified_national_security').is(":checked")
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
							
							$("#verify_modal").modal("hide");
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
		<!-- KYC Modal End -->
		
		<!-- User Photo Modal Start -->
			$('#change_user_pic').click(function(){
				$('#loading-spinner').show();
				
				// wipe out upload verification modal so there are no conflicts
				$('#upload_files').html('');
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				// change upload user id so change user button loads correct files
				$.ajax({
					url: "/admin/ajax/upload_user_id_change.php",
					type: "POST",
					data: {
						user_id : columns['user_id'],
						checksum : columns['upload_checksum'],
						handler: 'photos'
					},
					success: function(response){						
						// reload data from upload_files script into div
						$('#upload_photos').html(response);
						
						$('#modal_row_id').html("ID #"+id);
						$('#customer_name2').html(columns['customer_name']);
						
						$("#user_pic_modal").modal("show");
						$('#loading-spinner').hide();
					}
				});
			});
			$('#user_pic_save').click(function(){
				// get database id of selected row
				var customer_id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				var photo_path = $('input[name=selected_photo]:checked').val();
				
				// save pic path to db
				$.ajax({
					url: "/admin/ajax/datatables/manage_customers_buttons.php",
					type: "POST",
					data: {
						action : "update_photo",
						id : customer_id,
						photo_path: photo_path
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// set picture to modal
							$("#user_pic").attr("src", photo_path);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#user_pic_modal").modal("hide");
			});
		<!-- User Photo Modal End -->
		});
	</script>
	<!-- The popup for upload new photo -->
   
	<div class="container-fluid">
		<div id="loading-spinner"><div></div></div>
	
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
					
					<!-- <div class="btn-group pull-right" style="margin-left: 15px;">
						<a id="transactions_btn" class="btn btn-default btn-sm btn-row-action">Transactions</a>
						<a id="withdraw_btn" class="btn btn-default btn-sm btn-row-action">Withdrawals</a>
						<a id="deposit_btn" class="btn btn-default btn-sm btn-row-action">Deposits</a> 
					</div> !-->
					
					<div class="btn-group pull-right" style="margin-left: 15px;">
					<!--	<a id="login_btn" class="btn btn-default btn-sm btn-row-action">Security Settings</a> !-->
						<a id="verify_btn" class="btn btn-default btn-sm btn-row-action">KYC Verification</a>
					</div>
					
					<div class="btn-group pull-right">
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a> 
						<a id="disable_btn" class="btn btn-default btn-sm btn-row-action">Disable</a>
					</div>
				</div>
				<div class="clear"></div>
				<div class="panel-body">
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Customer Name</th>
								<!-- <th class="text-left">Balance</th>
								<th class="text-left">Bonus</th>
								<th class="text-left">Island</th> !-->
								<th class="text-left">Status</th>
								<th class="text-left">KYC</th>
							 <!--<th class="text-left">Account #</th>
								<th class="text-left">Email</th>
								<th class="text-left">Phone</th>
								<th class="text-left">Details</th> 
							!-->
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
        <br /><br />
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for adding / editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Customer Details</h4>
						<div id="customer_number" class="pull-right" style="padding-right: 6px;">
							
						</div>
						<div id="modal_row_id" style="display: none;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-3">
							<div class="thumbnail" style="width: 125px; height: 125px;">
								<div class="thumbnail-caption">
									<h4>User's Photo</h4>
									<p><label id="change_user_pic" class="label label-danger" title="Change">Change Image</label></p>
								</div>
								<img id="user_pic" style="text-align: center; width: 100%; height: 100%;" alt="User Pic" src="/admin/images/generic-user-purple.png">
							</div>
						</div>
						
						<div class="col-sm-7">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="user_id">Login User Account</label>
									<select id="user_id" name="user_id" class="form-control combobox" disabled>
										<option value=""></option>
										<?php
											$q = "SELECT * FROM `users` WHERE `id`>0 ORDER BY `username` ASC";
											$users = $db->query($q);
											foreach($users as $user){
												echo "<option value='".$user['id']."'>".$user['username']."</option>";
											}
										?>
									</select>
									<input type="hidden" id="user_id_new" name="user_id_new" value=""> <!-- must be id_new for new values -->
								</div>
							</div>
							<div id="new_user_password" class="col-sm-6" style="display: none;">
								<div class="form-group">
									<label for="password">New Users Password</label>
									<input id="password" type="text" class="form-control" value="ASureWin1">
								</div>
							</div>
							<div id="new_user_force_change" class="col-sm-6" style="display: none;">
								<div class="form-group">
									<label class="control-label" for="new_user_force_change">Force Password Change</label><br>
									<input id="new_user_force_change_toggle" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="130">
								</div>
							</div>
						</div>
					
						<div class="col-sm-12">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="firstname">First Name</label>
									<input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name">
								</div>
							</div> 

							<div class="col-sm-4">
								<div class="form-group">
									<label for="midname">Middle Name</label>
									<input type="text" class="form-control" id="midname" name="midname" placeholder="Middle Name">
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="lastname">Last Name</label>
									<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name">
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="gender">Gender</label>
									<select class="form-control selectpicker" id='gender' name="gender">
										<option value=''></option>
										<option value='Female'>Female</option>
										<option value='Male'>Male</option>
									</select>
								</div>	
							</div>
						
							<div class="col-sm-4">
								<div class="form-group">
									<label for="date_of_birth">Date of Birth</label>
									<div class='input-group date dp' id='date_of_birth'>
										<input type='text' class="form-control" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="customer_num">Customer Number</label>
									<input type="text" class="form-control" id="customer_num" name="customer_num" placeholder="Customer Number">
								</div>
							</div>
						</div>											
						
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="address">Address 1</label>
									<input type="text" class="form-control" id="address" name="address">
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<label for="address2">Address 2</label>
									<input type="text" class="form-control" id="address2" name="address2">
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="city">City </label>
									<input type="text" class="form-control" id="city" name="city">
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="island_id">Island:</label>
									<select id="island_id" name="island_id" class="form-control selectpicker">
										<option value="-1"></option>
										<?php
											$q = "SELECT * FROM island WHERE id>0 ORDER BY name ASC";
											$islands = $db->query($q);
											foreach($islands as $island){
												echo "<option value='".$island['id']."'>".$island['name']."</option>";
											}
										?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="country">Country</label>
									<input type="text" class="form-control" id="country" name="country">
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-4">								
								<div class="form-group">
									<label for="email">Email</label>
									<input type="text" class="form-control" id="email" name="email" placeholder="Email Address">
								</div>	
							</div>
						
							<div class="col-sm-4">								
								<div class="form-group">
									<label for="telephone">Home Phone</label>
									<input type="text" class="form-control" id="telephone" name="telephone" placeholder="Home Phone Number">
								</div>	
							</div> 

							<div class="col-sm-4">
								<div class="form-group">
									<label for="cellphone">Mobile Phone</label>
									<input type="text" class="form-control" id="cellphone" name="cellphone" placeholder="Cellular Phone Number">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Modal for Login Account Settings -->
	<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="login_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="login_modal_label">Login Account Settings</h4>

					<div id="customer_name" class="pull-right" style="padding-right: 6px;">
					
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">			
					<div class="col-sm-6">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="google_auth_enabled">2FA Enabled</label><br>
								<input id="google_auth_enabled" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="130">
								<span style="float: right; padding-top: 5px;" id="google_auth_secret"></span>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="force_password_change">Force Password Change</label><br>
								<input id="force_password_change" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="130">
							</div>
						</div>
		
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="pass">New Password</label>
								<input type="password" class="form-control" id="pass" placeholder="New Password">
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="conf_pass">Confirm Password</label>
								<input type="password" class="form-control" id="conf_pass" placeholder="Confirm Password">
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<label for="userlevel">User Level</label>
								<select id="userlevel" name="userlevel" class="form-control selectpicker">
									<option value="1">1 - Email Activation Not Completed</option>
									<option value="2">2 - Admin Activation Not Completed</option>
									<option value="3">3 - Registered User</option>
									<option value="4">4 - Not Defined</option>
									<option value="5">5 - Not Defined</option>
									<option value="6">6 - Not Defined</option>
									<option value="7">7 - Privileged User</option>
									<option value="8">8 - Administration</option>
									<option value="9">9 - Global Admin</option>
								</select>
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label" for="pin">Withdrawal PIN</label>
							<input type="text" class="form-control" id="pin" placeholder="PIN Number">
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label" for="locked">Account Locked</label><br>
							<input id="locked" type="checkbox" data-toggle="toggle" data-on="Locked" data-off="Not Locked" data-width="130">
						</div>
					</div>
					
					<div id="lockout_expiration_contain" class="col-sm-6" style="display: none;">
						<label class="col-sm-12 control-label" for="locked_expiration" style="text-align: center;">Account Lock Expiration:<br><br></label>

						<div class="dtp-inlne" id="locked_expiration"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="login_save">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal to Confirm Deletion -->
	<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
				</div>
				<div class="modal-body">
					Are you sure you would like to remove this customer?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal for Customer Transactions -->
	<div class="modal fade" id="transactions_modal" tabindex="-1" role="dialog" aria-labelledby="transactions_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="transactions_modal_label">Transactions</h4>
					
					<div id="date_range_selector" class="pull-right col-sm-6" style="padding-right: 6px;">
						<div class='col-sm-6'>
							<div class="form-group">
								<div class='input-group date dp' id='start_date'>
									<input type='text' class="form-control" placeholder="From Date" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
						<div class='col-sm-6'>
							<div class="form-group">
								<div class='input-group date dp' id='end_date'>
									<input type='text' class="form-control" placeholder="To Date"/>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
					</div>
					
					<div id="trans_modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div class="col-sm-12">
						<table id="transactions_datatable" class="display table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Date</th>
									<th class="text-left">Type</th>
									<th class="text-left">Description</th>
									<th class="text-left">Amount</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="6" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal for KYC Verification -->
	<div class="modal fade" id="verify_modal" tabindex="-1" role="dialog" aria-labelledby="verify_modal_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="verify_modal_label">KYC Verification Status</h4>

					<div id="customer_name" class="pull-right" style="padding-right: 6px;">
						
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
			    	<div id="upload_files" class="col-sm-12">
						
			    	</div>
					<div class="col-sm-12">						
						<?php include('member.php'); ?>						
					</div>
					<div class="col-sm-12">
						<label class="col-sm-3 control-label" for="drivers_license">Driver's License Number</label>
						
						<div class="col-sm-4">
							<input type="text" id="drivers_license" class="form-control">
						</div>
						
						<div class="col-sm-5">
							<input id="verified_drivers_license" type="checkbox" data-toggle="toggle" data-on="Verified" data-off="Not Verified" data-width="130">
						</div>
						
						<br><br>
					</div>

					<div class="col-sm-12">
						<label class="col-sm-3 control-label" for="voters_card">Voter's Card Number</label>
						
						<div class="col-sm-4">
							<input type="text" id="voters_card" class="form-control">
						</div>
						
						<div class="col-sm-5">
							<input id="verified_voters_card" type="checkbox" data-toggle="toggle" data-on="Verified" data-off="Not Verified" data-width="130">
						</div>
						
						<br><br>
					</div>

					<div class="col-sm-12">
						<label class="col-sm-3 control-label" for="passport">Passport Number</label>
						
						<div class="col-sm-4">
							<input type="text" id="passport" class="form-control">
						</div>
						
						<div class="col-sm-5">
							<input id="verified_passport" type="checkbox" data-toggle="toggle" data-on="Verified" data-off="Not Verified" data-width="130">
						</div>
						
						<br><br>
					</div>
					
					<div class="col-sm-12">
						<label class="col-sm-3 control-label" for="national_security">National Insurance Number</label>
						
						<div class="col-sm-4">
							<input type="text" id="national_security" class="form-control">
						</div>
						
						<div class="col-sm-5">
							<input id="verified_national_security" type="checkbox" data-toggle="toggle" data-on="Verified" data-off="Not Verified" data-width="130">
						</div>
						
						<br><br>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="close_verify" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="verify_save">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal for User Pic Selection -->
	<div class="modal fade" id="user_pic_modal" tabindex="-1" role="dialog" aria-labelledby="user_pic_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="cuser_pic_modal_label">Change User's Photo</h4>

					<div id="customer_name2" class="pull-right" style="padding-right: 6px;">
						
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
			    	<div id="upload_photos" class="col-sm-12">						
			    	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="user_pic_save">Use Selected</button>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
