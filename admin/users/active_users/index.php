<?php
	$page_title = "Active Users";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "username" },
					{ "data": "name" },
					{ "data": "ip" },
					{ "data": "latest_activity" },
					{ "data": "inactive" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/active_users.php"
			});
			
			$(document).on("click", "#force_logout_btn", function(){
				var columns = data_table.row('.selected').data();
				
				$.ajax({
					url: "/admin/ajax/datatables/active_users_buttons.php",
					type: "POST",
					data: {
						action : "force_logout",
						username : columns['username']
					},
					dataType: "json",
					success: function(response){						
						if(response['result'] != false){
							data_table.draw(true);
						}else{
							bootbox.alert("Operation Failed!\n<br>"+response['errors']);
						}
					}
				});
			});
			
			$('#datatable tbody').on('click', 'tr', function(){	
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
			
			// auto refresh dataTable
			function referesh_table(){
				if($('#auto_refresh').is(':checked')){
					data_table.draw();
				}
				setTimeout(referesh_table, 5000);
			}
			referesh_table();
		});
	</script>
	<!--<script>
	$(document).ready(function() {
                setInterval(function() {
                        var randomnumber=Math.floor(Math.random()*100)
                        $('#display').text($("#display").val());
                }, 2000);
        });
	</script>-->

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix" style="text-align: center;">
				<?php 	//$user_count ="SELECT count(username) as `user_count` FROM `active_users`";
					//$result= $db->queryOneRow($user_count);
				?>
					<!-- <h3 class="panel-title pull-left" style="padding-top: 7.5px;">Active Users (<?php echo $result['user_count'];?>)</h3> !-->
					<div id="display">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">
					Active Users : 	<?php 
					$admin_count="SELECT users.id,users.userlevel,users.username,count(users.id) as total FROM   active_users JOIN   users ON users.username = active_users.username
						WHERE  users.userlevel = '9' ";
					$admin_result=$db->query($admin_count);
					echo "Admin (".$admin_result[0]['total'].")";
				
					echo "&nbsp";
					
					$usercount="select users.id,users.userlevel,users.username,count(id) as total from users,customers,active_users  where users.id=customers.user_id	
						and active_users.username=users.username  and userlevel=3";
					$user_result=$db->query($usercount);
					echo "Customer (".$user_result[0]['total'].")";
					echo "&nbsp";
					
					$sql="select users.id,users.userlevel,users.username,count(users.id) as total from users,panel_user,active_users where users.id=panel_user.user_id
					 and active_users.username=users.username and userlevel=3";
					$result=$db->query($sql);
					echo "Cashier (".$result[0]['total'].")";
					?>	
					
					</h3> 	
					
					<span style="margin-top: 5px; display: inline-block;">
						<input id="auto_refresh" type="checkbox" checked> Auto-Refresh
					</span>
					<div class="btn-group pull-right">
						<a id="force_logout_btn" class="btn btn-default btn-sm btn-row-action" style="display:none;">Force Logout</a>
					</div>
					<div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Username</th>
								<th class="text-left">Name</th>
								<th class="text-left">IP</th>
								<th class="text-left">Last Activity</th>
								<th class="text-left">Inactive Time</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
