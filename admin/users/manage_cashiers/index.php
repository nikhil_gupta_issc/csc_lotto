<?php
	$page_title = "Manage Cashiers";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			// main scheme level datatable initialization
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				"ajax": "/admin/ajax/datatables/manage_cashiers.php",
				"columns": [ 
					{ "data": "cashier_name" },
					{ "data": "location" },
					{ "data": "island" },
					{ "data": "phones" },
					{ "data": "balance" },
					{ "data": "last_login" },
					{ "data": "status" },
					{ "data": "status_details" },                                        
                                        { "data": "virtual_money_offset" },
                                        { "data": "location_change" },
                                        { "data": "petty_cash_rights" }
				],
				"order": [[3, 'asc']]
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var row_array = data_table.row(this).data();
				//console.log(row_array);
				if(row_array['status'] == "<span style='color:green;'>Enabled</span>"){
					$('#disable_btn').text("Disable");
				}else if(row_array['status'] == "<span style='color:orange;'>Locked</span>"){
					$('#disable_btn').text("Unlock");
				}else{
					$('#disable_btn').text("Enable");
				}
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			$('#disable_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				
				$.ajax({
					url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			// confirmation of deletion
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
					type: "POST",
					data: {
						action : 'delete',
						id : id
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				$('#submit_edit').html("Save Changes");
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
                                $('#cashier_number').val("").prop('disabled', false);
				$('#firstname').val("").prop('disabled', false);
				$('#midname').val("").prop('disabled', false);
				$('#lastname').val("").prop('disabled', false);
				$('#gender').val("").change().prop('disabled', false);
				$('#date_of_birth').prop('disabled', false); 
				$('#address').val("").prop('disabled', false);
				$('#address2').val("").prop('disabled', false);
				$('#city').val("").prop('disabled', false);
				$('#island_id').val("").change().prop('disabled', false);
				$('#country').val("Bahamas").prop('disabled', false);
				$('#email').val("").prop('disabled', false);
				$('#telephone').val("").prop('disabled', false);
				$('#cellphone').val("").prop('disabled', false);
				$('#balance').val("").prop('disabled', false);
				$('#last_login').val("").prop('disabled', false);

				var columns = data_table.row('.selected').data();
				$('#user_id').val(columns['user_id']).combobox('refresh');
				$('#cashier_number').html("Account # "+columns['cashier_id']);
				$('#modal_row_id').html("ID #"+id);
				$('#cashier_id').val(columns['user_id']);
				$('#firstname').val(columns['firstname']);
				$('#midname').val(columns['midname']);
				$('#lastname').val(columns['lastname']);
				$('#gender').val(columns['gender']).change();
				$('#date_of_birth').data("DateTimePicker").clear().date(moment(columns['date_of_birth']));
				$('#address').val(columns['address']);
				$('#address2').val(columns['address2']);
				$('#city').val(columns['city']);
				$('#island_id').val(columns['island_id']).change();
				$('#country').val(columns['country']);
				$('#email').val(columns['email']);
				$('#telephone').val(columns['telephone']);
				$('#cellphone').val(columns['cellphone']);
				$('#balance').val(columns['balance']);
				$('#last_login').val(columns['last_login']);
				$('#start_date').val(columns['start_date']);
				$('#location_id').val(columns['location_id']).change();
                                
                                if(columns['p_location_change'] == 1){
                                    $('#location_rights_add').prop('checked',true);
                                }else{
                                    $('#petty_cash_add').prop('checked',false);
                                }
                                if(columns['p_petty_cash_rights'] == 1){
                                    $('#petty_cash_add').prop('checked',true);
                                }else{
                                    $('#petty_cash_add').prop('checked',false);
                                }

				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#name').focus();
				}, 500);                        
			})
			
			$(document).on('click', '#submit_edit', function() {
				$('#edit_row_modal').modal('hide');
				
				var id = parseInt($('#modal_row_id').html().replace("ID #", ""));

				
				// deteremine whether this is update or insert
				var action = "insert";
				if(id > 0){
					var columns = data_table.row('.selected').data();
					//console.log(columns);					
					var action = "update";
					var cashier_id = columns['cashier_id'];
				}else{
					var action = "insert";
					var cashier_id = -1;	
				}
				
				try{
					var dob = $('#date_of_birth').data("DateTimePicker").date().format('YYYY-MM-DD');
				}catch(err){
					var dob = "";
				}
				
				try{
					var start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}catch(err){
					var start_date = "";
				}
                                                      
			
				$.ajax({					
					url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
					type: "POST",
					data: {
						action : action,
						cashier_id : cashier_id,
						user_id : $('#user_id').val(),
						cashier_number : $('#cashier_number').val(),
						firstname : $('#firstname').val(),
						midname : $('#midname').val(),
						lastname : $('#lastname').val(),
						gender : $('#gender').val(),
						date_of_birth : dob,
						address : $('#address').val(),
						address2 : $('#address2').val(),
						city : $('#city').val(),
						island_id : $('#island_id').val(),
						country : $('#country').val(),
						email : $('#email').val(),
						telephone : $('#telephone').val(),
						cellphone : $('#cellphone').val(),
						balance : $('#bonus_balance').val(),
						start_date : start_date,
						location_change	: (($('#location_rights_add').prop('checked') == false) ? '0' : '1'),
                                                petty_cash_rights : (($('#petty_cash_add').prop('checked') == false) ? '0' : '1'),
                                                location_id : $('#location_id').val()
					},
					dataType: "json",
					success: function(response){

						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
							
							$('#edit_row_modal').modal('hide');
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#add_btn').click(function(){				
				// init elements
				$('#submit_edit').html("Add Cashier");
				$('#modal_row_id').html("New");
				
				// clear elements
				$('#name').val("");
				$('#user_id').data('combobox').clearTarget();
				$('#user_id').data('combobox').clearElement();
				$('#user_id').val("").change().combobox('refresh');
				$('#new_user_force_change_toggle').bootstrapToggle('on');
				$('#cashier_number').val("").prop('disabled', true);
				$('#firstname').val("").prop('disabled', true);
				$('#midname').val("").prop('disabled', true);
				$('#lastname').val("").prop('disabled', true);
				$('#gender').val("").change().prop('disabled', true);
				$('#date_of_birth').data("DateTimePicker").clear();
				$('#date_of_birth').prop('disabled', true); // fix this
				$('#address').val("").prop('disabled', true);
				$('#address2').val("").prop('disabled', true);
				$('#city').val("").prop('disabled', true);
				$('#island_id').val("").change().prop('disabled', true);
				$('#country').val("Bahamas").prop('disabled', true);
				$('#email').val("").prop('disabled', true);
				$('#telephone').val("").prop('disabled', true);
				$('#cellphone').val("").prop('disabled', true);
				$('#balance').val("").prop('disabled', true);
				// $("#user_pic").attr("src", "/images/CSCLotto_chip.png"); //default to logo for CSCLotto employees
				$("#user_pic").attr("src", "/admin/images/generic-user-purple.png"); //default to generic icon for cashiers
				$('#last_login').val("").prop('disabled', true);
				$('#start_date').data("DateTimePicker").clear();
				$('#location_id').val("");
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			$(document).on("change", "#user_id", function(){
				// clear elements
				$('#new_user_force_change_toggle').bootstrapToggle('on');
				$('#cashier_number').val("").prop('disabled', true);
				$('#firstname').val("").prop('disabled', true);
				$('#midname').val("").prop('disabled', true);
				$('#lastname').val("").prop('disabled', true);
				$('#gender').val("").change().prop('disabled', true);
				$('#date_of_birth').data("DateTimePicker").clear();
				$('#date_of_birth').prop('disabled', true); // fix this
				$('#address').val("").prop('disabled', true);
				$('#address2').val("").prop('disabled', true);
				$('#city').val("").prop('disabled', true);
				$('#island_id').val("").change().prop('disabled', true);
				$('#country').val("Bahamas").prop('disabled', true);
				$('#email').val("").prop('disabled', true);
				$('#telephone').val("").prop('disabled', true);
				$('#cellphone').val("").prop('disabled', true);
				$('#balance').val("").prop('disabled', true);
				// $("#user_pic").attr("src", "/images/CSCLotto_chip.png"); //default to logo for CSCLotto employees
				$("#user_pic").attr("src", "/admin/images/generic-user-purple.png"); //default to generic icon for cashiers
				$('#last_login').val("").prop('disabled', true);
				$('#start_date').data("DateTimePicker").clear();
				$('#location_id').val("");
				
				if($(this).val() == ""){
					// don't allow this for now
				}else{
					$.ajax({					
						url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
						type: "POST",
						data: {
							action : 'get_user_info',
							user_id : $('#user_id').val()
						},
						dataType: "json",
						success: function(data){
							var columns = data['result'][0];
							//console.log(columns);
							
							// populate input fields
							$('#user_id').val(columns['user_id']).combobox('refresh');
                                                        if(columns['cashier_id'] != undefined){
							   $('#cashier_number').html("Account # "+columns['cashier_id']);
							   $('#modal_row_id').html("ID #"+columns['user_id']);
                                                        }
							$('#cashier_id').val(columns['user_id']);
							$('#firstname').val(columns['firstname']);
							$('#midname').val(columns['midname']);
							$('#lastname').val(columns['lastname']);
							$('#gender').val(columns['gender']).change();
							$('#date_of_birth').data("DateTimePicker").clear().date(moment(columns['date_of_birth']));
							$('#address').val(columns['address']);
							$('#address2').val(columns['address2']);
							$('#city').val(columns['city']);
							$('#island_id').val(columns['island_id']).change();
							$('#country').val(columns['country']);
							$('#email').val(columns['email']);
							$('#telephone').val(columns['telephone']);
							$('#cellphone').val(columns['cellphone']);
							$('#balance').val(columns['balance']);
							$('#last_login').val(columns['last_login']);
							$('#start_date').val(columns['start_date']);
							$('#location_id').val(columns['location_id']).change();
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							alert("Operation Failed!");
						}
					});
				}
			});
		
		$('#add_funds_btn').click(function(){
			$("#add_funds_modal").modal("show");
		});
		
		$('#add_funds_confirm').click(function(){
			var columns = data_table.row('.selected').data();
			
			$.ajax({					
				url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
				type: "POST",
				data: {
					action : 'add_funds',
					add_amount : $('#add_funds').val(),
					user_id : columns['user_id']
				},
				dataType: "json",
				success: function(data){
					if(data.success == "false"){
						
					}else{
						bootbox.alert("Funds added successfully");
						$("#add_funds_modal").modal("show");
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
		});
             
/**---------------- Virtual money scripts ---------------------------**/
             $('#add_vm_btn').click(function(){

                       var columns = data_table.row('.selected').data();
			
			$.ajax({					
				url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
				type: "POST",
				data: {
					action : 'vm_info',
					user_id : columns['user_id']
				},
				dataType: "json",
				success: function(data){
					if(data.success == true){
					
                                             $('#current_balance').text(data.current_balance);
					     $('#opening_balance').text(data.opening_balance);
					     $('#expected_balance').text(data.expected_balance);
					     $('#sales').text(data.sales);
					     $('#payouts').text(data.payouts);
					     $('#total_virtual_money').text(data.total_vm);
					     $('#current_virtual_balance').text(data.current_vm);
                                             $('#used_vm').text(data.used_vm);
                                             $("#add_vm_modal").modal("show");

					}else{
					     bootbox.alert(data.errors);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});

		});
             $('#add_vm_confirm').click(function(){
                        if(parseFloat($('#sales').text()) < parseFloat($('#used_vm').text())){  
                              $('.close').click();
                              $("#continue_anyway_modal").modal("show");	
                        } else {                          
                              add_virtual_money();
                        }
		});

                $(document).on('click','#continue_anyway_confirm',function(){
                          add_virtual_money();
               });


               function add_virtual_money(){

                       var columns = data_table.row('.selected').data();
			
				$.ajax({					
					url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
					type: "POST",
					data: {
						action : 'add_vm',
						add_amount : $('#add_vm').val(),
						user_id : columns['user_id']
					},
					dataType: "json",
					success: function(data){
		                               $('#add_vm').val('');
		                               $('.close').click();
						if(data.success == "false"){
						      bootbox.alert(data.errors);                                              	
						}else{
						      bootbox.alert("Virtual Balance has been added successfully for the cashier.");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
               }

                $(document).on('click','.reset_vm',function(){
                     var vm_value = $(this).closest('td').find('.cashier_vm').val();
                     var user_id = $(this).closest('td').find('.cashier_vm').attr('user_id');

                     var self = $(this);
                     if(vm_value != '' && vm_value != 'NULL'){
                           
                          $.ajax({					
				url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
				type: "POST",
				data: {
					action : 'reset_cashier_vm',
					user_id : user_id
				},
				dataType: "json",
				success: function(data){
                                       
					if(data.success == "false"){
					      bootbox.alert(data.errors);                                              	
					}else{
                                              self.closest('td').find('.cashier_vm').val('');                                             
					      bootbox.alert("Default virtual money offset has been changed to default value.");
					}
                                   
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			 });
                     }
                });

                $(document).on('click','.save_vm',function(){
                     var vm_value = $(this).closest('td').find('.cashier_vm').val();
                     var user_id = $(this).closest('td').find('.cashier_vm').attr('user_id');

                     if(vm_value != '' && vm_value != 'NULL' && !isNaN(vm_value) && vm_value > 0){
                           
                          $.ajax({					
				url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
				type: "POST",
				data: {
					action : 'add_cashier_vm',
					add_amount : vm_value,
					user_id : user_id
				},
				dataType: "json",
				success: function(data){
                                       
					if(data.success == "false"){
					      bootbox.alert(data.errors);                                              	
					}else{
					      bootbox.alert("Default virtual money offset has been changed successfully for this cashier.");
					}
                                   
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			 });
                     } else {
                         bootbox.alert("Please Enter valid amount.");
                    }

               });
 
/** ---------------------- End virtual money scripts ------------------------ **/

                      $(document).on('change','.cashier_location_change',function(){
                          if($(this).prop('checked')){
                              var value = 1;
                          } else {
                              var value = 0;
                          }

                          $.ajax({					
				url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
				type: "POST",
				data: {
					action : 'change_cashier_location_change',
					value : value,
					user_id : $(this).attr('user_id')
				},
				dataType: "json",
				success: function(data){
                                       
					if(data.success == "false"){
					      bootbox.alert(data.errors);                                              	
					}else{
					      bootbox.alert("Location change rights has been changed.");
					}
                                   
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			 });

                      });


                      $(document).on('change','.petty_cash_change',function(){
                          if($(this).prop('checked')){
                              var value = 1;
                          } else {
                              var value = 0;
                          }

                          $.ajax({					
				url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
				type: "POST",
				data: {
					action : 'change_petty_cash_rights',
					value : value,
					user_id : $(this).attr('user_id')
				},
				dataType: "json",
				success: function(data){
                                       
					if(data.success == "false"){
					      bootbox.alert(data.errors);                                              	
					}else{
					      bootbox.alert("Petty cash right has been changed successfully.");
					}
                                   
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			 });








                     });

		        /*** Security Modal Start ***/
			$(document).on("change", "#locked", function(){
				if($("#locked").is(":checked")){
					// set expiration to use default lockout time
					var expire_moment = moment().add(<?php echo LOCKED_MINUTES; ?>, 'minutes');;
					$('#locked_expiration').data("DateTimePicker").date(expire_moment);
			
					$("#lockout_expiration_contain").slideDown("fast");
				}else{
					$("#lockout_expiration_contain").slideUp("fast");
				}
			});

			$('#security_btn').click(function(){	
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
		
				// grab user info from database and store in session
				$.ajax({
					url: "/admin/ajax/get_cashier_user_info.php",
					type: "POST",
					data: {
						user_id : columns['user_id']
					},
					dataType: "json",
					success: function(response){						
						if(response['success'] == true){
							// load data to modal
							$('#security_name').html(columns['name']);
							$('#google_auth_secret').html(response['user_info']['google_auth_secret']);
							$('#userlevel').val(response['user_info']['userlevel']).change();
					
							if(response['user_info']['locked'] == true){
								$('#locked').bootstrapToggle('on');
								var expire_moment = new moment(response['user_info']['locked_expiration']);
								$('#locked_expiration').data("DateTimePicker").date(expire_moment);
							}
							if(response['user_info']['google_auth_enabled'] == true){
								$('#google_auth_enabled').bootstrapToggle('on');
							}
							if(response['user_info']['force_password_change'] == true){
								$('#force_password_change').bootstrapToggle('on');
							}
					
							// show modal
							$('#security_modal').modal('show');
						}else{
							bootbox.alert("Operation Failed!\n<br>"+response['query']);
						}
					}
				});
			});
	
			$('#security_save').click(function(){
				// get database id of selected row
				var cashier_id = data_table.$('tr.selected').attr('id').replace("row_", "");
		
				// populate elements with data from table
				var columns = data_table.row('.selected').data();

				$.ajax({
					url: "/admin/ajax/datatables/manage_users_buttons.php",
					type: "POST",
					data: {
						action : "update_security",
						id : cashier_id,
						user_id : columns['user_id'],
						userlevel: $('#userlevel').val(),
						pin : $('#pin').val(),
						locked : +$('#locked').is(":checked"),
						google_auth_enabled : +$('#google_auth_enabled').is(":checked"),
						force_password_change : +$('#force_password_change').is(":checked"),
						user_id : columns['user_id'],
						locked_expiration: $('#locked_expiration').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss'),
						pass : $('#pass').val(),
						conf_pass : $('#conf_pass').val()
					},
					dataType: "json",
					success: function(response){						
						if(response['result'] != false){
							$('#security_modal').modal('hide');
							data_table.draw(true);
						}else{
							bootbox.alert("Operation Failed!\n<br>"+response['query']);
						}
					}
				});
			});
		<!-- Security Modal End -->	
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<div class="col-sm-3">
						<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
					</div>
					
					<div>
						<div class="btn-group pull-right" style="margin-left: 15px;">
							<a id="add_funds_btn" class="btn btn-default btn-sm btn-row-action">Add Funds</a>
                                                        <a id="add_vm_btn" class="btn btn-default btn-sm btn-row-action">Add Virtual Money</a>
							<a id="security_btn" class="btn btn-default btn-sm btn-row-action">Security Settings</a>
						</div>
						
						<div class="btn-group pull-right">
							<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
							<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
							<a id="disable_btn" class="btn btn-default btn-sm btn-row-action">Disable</a>
							<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Delete</a>
						</div>
					</div>
<div class="col-sm-12 no-padding" style="margin-top: 12px;">
<div class="col-sm-7"></div>
                                           
                                            <div class="col-sm-5 no-padding"><div class="col-sm-2"></div><div class="col-sm-5 no-padding"><span class="" >Default Virtual Money</span></div><div class="col-sm-5 no-padding"><input style="color:black;" type="text" name="default_virtual_money" id="default_virtual_money" class="default_virtual_money pull-right" maxlength="10" ></div></div>
                                         
                                        </div>
				</div>
                                <script type="text/javascript" src="../../js/virtual_money.js" ></script>
				<div class="panel-body">
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Cashier Name</th>
								<th class="text-left">Default Location</th>
								<th class="text-left">Island</th>
								<th class="text-left">Tel</th>
								<th class="text-left">Balance</th>
								<th class="text-left">Last Login</th>
								<th class="text-left">Status</th>
								<th class="text-left">Status Details</th>  
                                                                <th class="text-left">Virtual Money Offset</th>
                                                                <th class="text-left">Location change rights</th>
                                                                <th class="text-left">Petty cash Rights</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Cashier Details</h4>
						<div id="cashier_number" class="pull-right" style="padding-right: 6px;">
							
						</div>
						<div id="modal_row_id" style="display: none;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-2">
							<div class="thumbnail" style="width: 150px; height: 150px;">
								<div class="thumbnail-caption">
									<h4>User's Photo</h4>
									<p><label id="change_user_pic" class="label label-danger" title="Change">Change Image</label></p>
								</div>
								<img id="user_pic" style="text-align: center; width: 100%; height: 100%;" alt="User Pic" src="/admin/images/generic-user-purple.png">
							</div>
						</div>

						<div class="col-sm-10">
							<div class="col-sm-12">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="user_id">Login User Account</label>
										<select id="user_id" name="user_id" class="form-control combobox">
											<option value=""></option>
											<?php
												$q = "SELECT * FROM `users` WHERE `id`>0 ORDER BY `username` ASC";
												$users = $db->query($q);
												foreach($users as $user){
													echo "<option value='".$user['id']."'>".$user['username']."</option>";
												}
											?>
										</select>
										<input type="hidden" id="user_id_new" name="user_id_new" value=""> <!-- must be id_new for new values -->
									</div>
								</div>
								<div id="new_user_password" class="col-sm-4" style="display: none;">
									<div class="form-group">
										<label for="password">New Users Password</label>
										<input id="password" type="text" class="form-control" value="ASureWin1">
									</div>
								</div>
								<div id="new_user_force_change" class="col-sm-4" style="display: none;">
									<div class="form-group">
										<label class="control-label" for="new_user_force_change">Force Password Change</label><br>
										<input id="new_user_force_change_toggle" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="130">
									</div>
								</div>
							</div>
						
							<div class="col-sm-12">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="firstname">First Name</label>
										<input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name">
									</div>
								</div> 

								<div class="col-sm-4">
									<div class="form-group">
										<label for="midname">Middle Name</label>
										<input type="text" class="form-control" id="midname" name="midname" placeholder="Middle Name">
									</div>
								</div>
								
								<div class="col-sm-4">
									<div class="form-group">
										<label for="lastname">Last Name</label>
										<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name">
									</div>
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="gender">Gender</label>
										<select class="form-control selectpicker" id='gender' name="gender">
											<option value=''></option>
											<option value='Female'>Female</option>
											<option value='Male'>Male</option>
										</select>
									</div>	
								</div>
							
								<div class="col-sm-4">
									<div class="form-group">
										<label for="date_of_birth">Date of Birth</label>
										<div class='input-group date dp' id='date_of_birth'>
											<input type='text' class="form-control" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>
								
								<div class="col-sm-4">
									<div class="form-group">
										<label for="cashier_id">Cashier User ID</label>
										<input type="text" class="form-control" id="cashier_id" name="cashier_id" placeholder="Cashier Number" disabled>
									</div>
								</div>
							</div>											
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="address">Address 1</label>
									<input type="text" class="form-control" id="address" name="address">
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<label for="address2">Address 2</label>
									<input type="text" class="form-control" id="address2" name="address2">
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="city">City </label>
									<input type="text" class="form-control" id="city" name="city">
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="island_id">Island:</label>
									<select id="island_id" name="island_id" class="form-control selectpicker">
										<option value="-1"></option>
										<?php
											$q = "SELECT * FROM island WHERE id>0 ORDER BY name ASC";
											$islands = $db->query($q);
											foreach($islands as $island){
												echo "<option value='".$island['id']."'>".$island['name']."</option>";
											}
										?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="country">Country</label>
									<input type="text" class="form-control" id="country" name="country">
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-4">								
								<div class="form-group">
									<label for="email">Email</label>
									<input type="text" class="form-control" id="email" name="email" placeholder="Email Address">
								</div>	
							</div>
						
							<div class="col-sm-4">								
								<div class="form-group">
									<label for="telephone">Home Phone</label>
									<input type="text" class="form-control" id="telephone" name="telephone" placeholder="Home Phone Number">
								</div>	
							</div> 

							<div class="col-sm-4">
								<div class="form-group">
									<label for="cellphone">Mobile Phone</label>
									<input type="text" class="form-control" id="cellphone" name="cellphone" placeholder="Cellular Phone Number">
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="balance">Balance</label>
									<input type="text" class="form-control" id="balance" placeholder="$0.00" disabled>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="last_login">Last Login</label>
									<input type="text" class="form-control" id="last_login" placeholder="Never" disabled>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="start_date">Start Date</label>
									<div class='input-group date dp' id='start_date'>
										<input type='text' class="form-control" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">						
							<div class="col-sm-6">
								<div class="form-group">
									<label for="location_id">Default Location:</label>
									<select id="location_id" name="location_id" class="form-control selectpicker">
										<option value="-1"></option>
										<?php
											$q = "SELECT * FROM panel_location WHERE id>0 ORDER BY name ASC";
											$locations = $db->query($q);
											foreach($locations as $location){
												echo "<option value='".$location['id']."'>".$location['name']."</option>";
											}
										?>
									</select>
								</div>
							</div>
                                                       <div class="col-sm-3">
								<div class="form-group" style="text-align:center;" >
									<label for="location_rights_add">Location change rights:</label><br>
                                                                        <input type="checkbox" id="location_rights_add" class="location_rights_add">
								</div>
							</div>
                                                       <div class="col-sm-3">
								<div class="form-group" style="text-align:center;" >
									<label for="petty_cash_add">Petty Cash Rights:</label><br>
                                                       <input type="checkbox" id="petty_cash_add" class="petty_cash_add" >
								
								
                                                       </div>
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Modal for Security Settings -->
	<div class="modal fade" id="security_modal" tabindex="-1" role="dialog" aria-labelledby="security_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="security_modal_label">Security Settings</h4>

					<div id="security_name" class="pull-right" style="padding-right: 6px;">
					
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div class="col-sm-6">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="google_auth_enabled">2FA Enabled</label><br>
								<input id="google_auth_enabled" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="130">
								<span style="float: right; padding-top: 5px;" id="google_auth_secret"></span>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="force_password_change">Force Password Change</label><br>
								<input id="force_password_change" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="130">
							</div>
						</div>
		
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="pass">New Password</label>
								<input type="password" class="form-control" id="pass" placeholder="New Password">
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="conf_pass">Confirm Password</label>
								<input type="password" class="form-control" id="conf_pass" placeholder="Confirm Password">
							</div>
						</div>
			
						<div class="col-sm-12">
							<div class="form-group">
								<label for="userlevel">User Level</label>
								<select id="userlevel" name="userlevel" class="form-control selectpicker">
									<option value="1">1 - Email Activation Not Completed</option>
									<option value="2">2 - Admin Activation Not Completed</option>
									<option value="3">3 - Registered User</option>
									<option value="4">4 - Not Defined</option>
									<option value="5">5 - Not Defined</option>
									<option value="6">6 - Not Defined</option>
									<option value="7">7 - Privileged User</option>
									<option value="8">8 - Administration</option>
									<option value="9">9 - Global Admin</option>
								</select>
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label" for="locked">Account Locked</label><br>
							<input id="locked" type="checkbox" data-toggle="toggle" data-on="Locked" data-off="Not Locked" data-width="130">
						</div>
					</div>
					
					<div class="col-sm-6">

					</div>
					
					<div id="lockout_expiration_contain" class="col-sm-6" style="display: none;">
						<label class="col-sm-12 control-label" for="locked_expiration" style="text-align: center;"><br>Account Lock Expiration:<br><br></label>

						<div class="dtp-inlne" id="locked_expiration"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="security_save">Save Changes</button>
				</div>
			</div>
		</div>
	</div>	
	
	<!-- Modal to Confirm Deletion -->
	<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
				</div>
				<div class="modal-body">
					Are you sure you would like to remove this cashier?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal to Add Funds -->
	<div class="modal fade" id="add_funds_modal" tabindex="-1" role="dialog" aria-labelledby="add_funds_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="add_funds_modal_label">Add Funds</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label" for="add_funds">Amount to Add</label><br>
						<input id="add_funds" type="text" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="add_funds_confirm">Confirm Additional Funds</button>
				</div>
			</div>
		</div>
	</div>

<!---------------------------- Virtual Money Models ------------------------------>
<!-- Modal to Confirm Deletion -->
	<div class="modal fade" id="continue_anyway_modal" tabindex="-1" role="dialog" aria-labelledby="continue_anyway_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="continue_anyway_modal_label">Confirm Virtual Money Add</h4>
				</div>
				
                                     <div class="col-sm-12" style="text-align: center;" >
					<i class="fa fa-exclamation-triangle fa-4x" style="color:red; ">
      </div><div class="col-sm-12"></i>Cashier's Expected balance is less then the Virtual Balance. Continue anyway?</div>
<div class="col-sm-12"></div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="continue_anyway_confirm">Continue</button>
				</div>
			</div>
		</div>
	</div>
       <!-- Default virtual money confirmation  -->
        <div class="modal fade" id="default_vm_modal" tabindex="-1" role="dialog" aria-labelledby="default_vm_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="default_vm_modal_label">Virtual Money Offset confirmation</h4>
				</div>
				<div class="modal-body">
					Do you want to change Virtual Money Offset?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="default_vm_confirm">Save</button>
				</div>
			</div>
		</div>
	</div>

	 <!-- Modal to Add Virtual Funds -->
	<div class="modal fade" id="add_vm_modal" tabindex="-1" role="dialog" aria-labelledby="add_vm_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="add_vm_modal_label">Add Virtual Money</h4>
				</div>
				
                                    <div class="col-sm-12">
		                             <div class="col-sm-6">
		                                  <div class="form-group">
							<label class="control-label" for="opening_balance">Opening Balance</label><br>
							$<span id="opening_balance">0.00</span>
						  </div>
		                             </div>
		                             <div class="col-sm-6">
		                                  <div class="form-group">
							<label class="control-label" for="current_balance">Current Balance</label><br>
							$<span id="current_balance">0.00</span>
						  </div>
		                             </div>
		                             <div class="col-sm-6">
		                                  <div class="form-group">
							<label class="control-label" for="sales">Sales</label><br>
							$<span id="sales">0.00</span>
						  </div>
		                             </div>
		                             <div class="col-sm-6">
		                                  <div class="form-group">
							<label class="control-label" for="payouts">Payouts</label><br>
							$<span id="payouts">0.00</span>
						  </div>
		                             </div>
		                             <div class="col-sm-6">
		                                  <div class="form-group">
							<label class="control-label" for="total_virtual_money">Total Virtual Money</label><br>
							$<span id="total_virtual_money">0.00</span>
						  </div>
		                             </div>
		                             <div class="col-sm-6">
		                                  <div class="form-group">
							<label class="control-label" for="current_virtual_balance">Current Virtual Money</label><br>
							$<span id="current_virtual_balance">0.00</span>
						  </div>
		                             </div>

		                             <div class="col-sm-6">
		                                  <div class="form-group">
							<label class="control-label" for="expected_balance">Expected Balance</label><br>
							$<span id="expected_balance">0.00</span>
						  </div>
		                             </div>
		                             <div class="col-sm-6">
		                                   <div class="form-group">
							<label class="control-label" for="used_vm">Used Virtual Money</label><br>
							$<span id="used_vm">0.00</span>
						   </div>
		                            </div>
		                            <div class="col-sm-12">
						<div class="form-group">
							<label class="control-label" for="add_vm">Virtual amount to Add</label><br>
							<input id="add_vm" type="text" maxlength="10" class="form-control">
						</div>
		                            </div>
                                 </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="add_vm_confirm">Confirm Additional Virtual Money</button>
				</div>
			</div>
		</div>
	</div>

        <!-- Modal to Add Virtual Funds -->
	<div class="modal fade" id="confirm_vm_modal" tabindex="-1" role="dialog" aria-labelledby="confirm_vm_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="confirm_vm_modal_label">Add Virtual Money</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label" for="add_vm">Amount to Add</label><br>
						<input id="add_vm" type="text" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="confirm_virtual_money">Confirm Additional Virtual Money</button>
				</div>
			</div>
		</div>
	</div>
<!---------------------------- End Virtual Money Models ------------------------------>
	
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
