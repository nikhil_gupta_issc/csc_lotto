<?php
$page_title = "Backoffice Users";
include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>

	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#add_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "name" },
					{ "data": "username" },
					{ "data": "last_login" }
				],
				"ajax": "/admin/ajax/datatables/backoffice_users.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var enabled = data_table.row(this).data();
				if(enabled[6] == "Enabled"){
					$('#disable_btn').text("Disable");
				}else{
					$('#disable_btn').text("Enable");
				}
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide add/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			$('#disable_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				
				$.ajax({
					url: "/admin/ajax/datatables/backoffice_users_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/backoffice_users_buttons.php",
					type: "POST",
					data: {
						action : 'delete',
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to add button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				$('#submit_add').html("Save Changes");
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				$('#user_id').val(columns['user_id']).combobox('refresh');
				
				// show modal
				$('#add_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#add_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#user_id').focus();
				}, 350);                        
			})
			
			// data validation for add modal
			$(document).on('click', '#submit_add', function() {
				$('#add_row_modal').modal('hide');
				
				var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
				
				// deteremine whether this is update or insert
				var action = "insert";
				if(id > 0){
					var columns = data_table.row('.selected').data();		
					var action = "update";
				}else{
					var action = "insert";	
				}

				$.ajax({
					url: "/admin/ajax/datatables/backoffice_users_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : $('#user_id').val(),
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#add_btn').click(function(){				
				// clear elements
				var columns = data_table.row('.selected').data();
				$('#modal_row_id').html("New");
				
				// show modal
				$('#add_row_modal').modal('show');
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
	<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Manage Backoffice Users</h3>
					<div class="btn-group pull-right">
						<a id="add_btn" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Delete</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Name</th>
								<th class="text-left">Username</th>
								<th class="text-left">Last Login</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for adding user -->
	<div class="modal fade" id="add_row_modal" tabindex="-1" role="dialog" aria-labelledby="add_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="add_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="add_row_modal_label">Back Office User Details</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-12">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="user_id">Login User Account</label>
									<select id="user_id" name="user_id" class="form-control combobox">
										<option value=""></option>
										<?php
											$q = "SELECT * FROM `users` WHERE `id`>0 ORDER BY `username` ASC";
											$users = $db->query($q);
											foreach($users as $user){
												echo "<option value='".$user['id']."'>".$user['username']."</option>";
											}
										?>
									</select>
									<input type="hidden" id="user_id_new" name="user_id_new" value=""> <!-- must be id_new for new values -->
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_add">Add User</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	
	<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
				</div>
				<div class="modal-body">
					Are you sure you would like to remove this user?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
				</div>
			</div>
		</div>
	</div>

<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 