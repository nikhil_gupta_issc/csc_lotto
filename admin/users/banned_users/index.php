<?php
	$page_title = "Banned Users";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "username" },
					{ "data": "name" },
					{ "data": "banned_on" },
					{ "data": "deleted_by" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/banned_users.php"
			});
			
			$(document).on("click", "#reactivate_btn", function(){
				var columns = data_table.row('.selected').data();
				
				$.ajax({
					url: "/admin/ajax/datatables/banned_users_buttons.php",
					type: "POST",
					data: {
						action : "reactivate",
						username : columns['username']
					},
					dataType: "json",
					success: function(response){						
						if(response['result'] != false){
							data_table.draw(true);
						}else{
							bootbox.alert("Operation Failed!\n<br>"+response['errors']);
						}
					}
				});
			});
			
			$('#datatable tbody').on('click', 'tr', function(){	
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Banned Users</h3>
					<div class="btn-group pull-right">
						<a id="reactivate_btn" class="btn btn-default btn-sm btn-row-action" style="display:none;">Reactivate</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Username</th>
								<th class="text-left">Name</th>
								<th class="text-left">Banned On</th>
								<th class="text-left">Banned By</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 