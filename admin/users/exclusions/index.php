<?php
	$page_title = "Exclusions";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// update dormant users
	$core->disable_dormant_accounts();
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "name" },
					{ "data": "type" },
					{ "data": "reason" },
					{ "data": "start" },
					{ "data": "end" },
					{ "data": "total" }
				],
				"order": [[3, 'asc']],
				"ajax": "/admin/ajax/datatables/exclusions.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){	
				var row_array = data_table.row(this).data();
			
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// enable / disable status
				if(row_array['status'] == "<span style='color:green;'>Enabled</span>"){
					$('#disable_btn').text("Disable");
				}else if(row_array['status'] == "<span style='color:orange;'>Locked</span>"){
					$('#disable_btn').text("Unlock");
				}else{
					$('#disable_btn').text("Enable");
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Exclusions</h3>
					<div class="btn-group pull-right">
					
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Name</th>
								<th class="text-left">Exclusion Type</th>
								<th class="text-left">Reason</th>
								<th class="text-left">Start</th>
								<th class="text-left">End</th>
								<th class="text-left">Total Exclusions</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 