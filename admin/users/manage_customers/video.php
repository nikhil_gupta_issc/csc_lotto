<!DOCTYPE html>
<html>
	<head>
		<script language="JavaScript" src="js/jquery.min.js"></script>
		<script language="JavaScript" src="ScriptCam/swfobject.js"></script>
		<script language="JavaScript" src="js/jquery-ui.min.js"></script>
		<!-- Please download the JW Player plugin from http://www.longtailvideo.com/jw-player/download -->
		<script type="text/javascript" src="js/jwplayer.js"></script>
		<script language="JavaScript" src="ScriptCam/scriptcam.js"></script>
		<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<style>
			#webcam {
				float:left;
			}
			#volumeMeter {
				background-image:url('ScriptCam/ledsbg.png');
				width:19px;
				height:133px;
				padding-top:5px;
			}
			#volumeMeter img {
				padding-left:4px;
				padding-top:1px;
				display:block;
			}
			.ui-slider {
				background:none;
				background-image:url('ScriptCam/trackslider.png');
				border:0;
				height:107px;
				margin-top:16px;
			}
			.ui-slider .ui-slider-handle {
				width:14px;
				height:32px;
				margin-left:7px;
				margin-bottom:-16px;
				background:url(ScriptCam/volumeslider.png) no-repeat; 
			}
			#volumePanel {
				-moz-border-radius: 0px 5px 5px 0px;
				border-radius: 0px 5px 5px 0px;
				background-color:#4B4B4B;
				width:55px;
				height:160px;
				-moz-box-shadow: 0px 3px 3px #333333;
				-webkit-box-shadow: 0px 3px 3px  #333333;
				shadow: 0px 3px 3px #333333;
			}
			#setupPanel {
				width:400px;
				height:30px;
				margin:5px;
			}
		</style>
		<script>
			$(document).ready(function() {
				$("#webcam").scriptcam({
					path: './ScriptCam/',
					fileReady:fileReady,
					cornerRadius:20,
					cornerColor:'e3e5e2',
					onError:onError,
					promptWillShow:promptWillShow,
					showMicrophoneErrors:false,
					showDebug:true,
					onWebcamReady:onWebcamReady,
					setVolume:setVolume,
					maximumTime: 20,
					timeLeft:timeLeft,
					fileName:'demo109230',
					connected:showRecord
				});
				setVolume(0);
				$("#slider").slider({ animate: true, min: 0, max: 100 , value: 50, orientation: 'vertical', disabled:true});
				$("#slider").bind( "slidechange", function(event, ui) {
				$.scriptcam.changeVolume($( "#slider" ).slider( "option", "value" ));
				});
			});
			function showRecord() {
				$( "#recordStartButton" ).attr( "disabled", false );
			}
			function startRecording() {
				$( "#recordStartButton" ).attr( "disabled", true );
				$( "#recordStopButton" ).attr( "disabled", false );
				$( "#recordPauseResumeButton" ).attr( "disabled", false );
				$.scriptcam.startRecording();
			}
			function closeCamera() {
				$("#slider").slider( "option", "disabled", true );
				$("#recordPauseResumeButton" ).attr( "disabled", true );
				$("#recordStopButton" ).attr( "disabled", true );
				$.scriptcam.closeCamera();
				$('#message').html('Please wait for the file conversion to finish...');
			}
			function pauseResumeCamera() {
				if ($( "#recordPauseResumeButton" ).html() == 'Pause Recording') {
					$( "#recordPauseResumeButton" ).html( "Resume Recording" );
					$.scriptcam.pauseRecording();
				}
				else {
					$( "#recordPauseResumeButton" ).html( "Pause Recording" );
					$.scriptcam.resumeRecording();
				}
			}
			function fileReady(fileName) {
				$('#recorder').hide();
				$.ajax({
					url: 'download_video.php',
					type: 'POST',
					data: {fileName:fileName},
					success:function(data){
						// display the croped photo
						$('.upload_btn').hide();
						$('img#photo_container').attr('src', data);
					}
				});
				//fileName2=fileName.replace('mp4','gif');
				$('#message').html('The MP4 file is now dowloadable for five minutes over <a href="'+fileName+'">here</a>.');
				var fileNameNoExtension=fileName.replace(".mp4", "");
				jwplayer("mediaplayer").setup({
					width:320,
					height:240,
					file: fileName,
					image: fileNameNoExtension+'_0000.jpg',
					'modes': [{ type: "html5",
								config: {
									file: fileName
								}
							},
							//I commented this line out so it doesn't fallback to flash
							//{ type: "flash", src: "/jwplayer/player.swf" },
							{ type: "download" }
					],
					tracks: [{ 
						file: fileNameNoExtension+'.vtt', 
						kind: 'thumbnails'
					}]
				});
				$('#mediaplayer').show();
			}
			function onError(errorId,errorMsg) {
				alert(errorMsg);
			}
			function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
				$( "#slider" ).slider( "option", "disabled", false );
				$( "#slider" ).slider( "option", "value", volume );
				$.each(cameraNames, function(index, text) {
					$('#cameraNames').append( $('<option></option>').val(index).html(text) )
				}); 
				$('#cameraNames').val(camera);
				$.each(microphoneNames, function(index, text) {
					$('#microphoneNames').append( $('<option></option>').val(index).html(text) )
				}); 
				$('#microphoneNames').val(microphone);
			}
			function promptWillShow() {
				alert('A security dialog will be shown. Please click on ALLOW.');
			}
			function setVolume(value) {
				value=parseInt(32 * value / 100) + 1;
				for (var i=1; i < value; i++) {
					$('#LedBar' + i).css('visibility','visible');
				}
				for (i=value; i < 33; i++) {
					$('#LedBar' + i).css('visibility','hidden');
				}
			}
			function timeLeft(value) {
				$('#timeLeft').val(value);
				if(value=='00:00'){
					closeCamera();
				}
			}
			function changeCamera() {
				$.scriptcam.changeCamera($('#cameraNames').val());
			}
			function changeMicrophone() {
				$.scriptcam.changeMicrophone($('#microphoneNames').val());
			}
			function showRecorder(){
				$('#recorder').show();
			}
		</script>
	</head>
	<body>
		<div id="recorder">
			<div id="webcam"></div>
			<button id="recordStartButton" class="btn btn-small btn-primary" onclick="startRecording()" disabled>Start Recording</button>&nbsp;
			<span style="padding-left:5px;padding-right:5px;">Time left:
				<input type="text" id="timeLeft" style="width:50px;font-size:10px;">&nbsp;
			</span>
			<!--<button id="recordStopButton" class="btn btn-small btn-primary" onclick="closeCamera()" disabled>Stop Recording</button>-->
		</div>
		<div id="mediaplayer" style="display:none;">
			<button id="recordStopButton" class="btn btn-small btn-primary" onclick="closeCamera()" disabled>Start New Recording</button>
		</div>
		<div id="message"></div>
	</body>
</html>