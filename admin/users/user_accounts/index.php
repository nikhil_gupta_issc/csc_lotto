<?php
	$page_title = "User Accounts";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<!-- Bootstrap DateTimePicker (http://eonasdan.github.io/bootstrap-datetimepicker/) -->
	<script type="text/javascript" src="/lib/assets/moment/moment.js"></script>
	<script type="text/javascript" src="/lib/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<link rel="stylesheet" href="/lib/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" />
	
	<script type="text/javascript">
		$(function () {
			// initialize datetimepickers
			$('.dtp-inlne').datetimepicker({
				inline: true,
				sideBySide: true
			});
		});

		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			// main datatable initialization
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/manage_users.php",
				"columns": [
					{ "data": "user_id" },
					{ "data": "name" },
					{ "data": "username" },
					{ "data": "email" },
					{ "data": "last_login" },
					{ "data": "locked" }
				],
				"order": [[1, 'asc']]
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var row_array = data_table.row(this).data();
				
				//console.log(row_array);
				
				if(row_array['status'] == "Enabled"){
					$('#disable_btn').text("Disable");
				}else{
					$('#disable_btn').text("Enable");
				}
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			// confirmation of deletion
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/manage_users_buttons.php",
					type: "POST",
					data: {
						action : 'ban',
						id : id
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}else{
							bootbox.alert("Operation Failed!\n<br>"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
		<!-- Add/Edit Modal Start -->	
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				$('#modal_row_id').html("ID #"+id);
				$('#firstname').val(columns['firstname']);
				$('#midname').val(columns['midname']);
				$('#lastname').val(columns['lastname']);
				$('#username').val(columns['username']);
				$('#email').val(columns['email']);
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#firstname').focus();
				}, 500);
			})
			
			$(document).on('click', '#submit_edit', function() {				
				var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
				
				var columns = data_table.row('.selected').data();
				
				// deteremine whether this is update or insert
				if(id > 0){
					var action = "update";
				}else{
					var action = "insert";
				}
		
				$.ajax({
					url: "/admin/ajax/datatables/manage_users_buttons.php",
					type: "POST",
					data: {
						action : action,
						user_id : id,
						firstname : $('#firstname').val(),
						midname : $('#midname').val(),
						lastname : $('#lastname').val(),
						username : $('#username').val(),
						email : $('#email').val()
					},
					dataType: "json",
					success: function(response){
						if(response.result != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
							
							$('#edit_row_modal').modal('hide');
						}else{
							bootbox.alert("Operation Failed!\n<br>"+response.query);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
				});
			});
			
			$('#add_btn').click(function(){				
				// clear elements
				$('#modal_row_id').html("New");
				$('#name').val("New User");
				$('#firstname').val("");
				$('#midname').val("");
				$('#lastname').val("");
				$('#username').val("");
				$('#email').val("");
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
		<!-- Add/Edit Modal End -->	
			
		<!-- Security Modal Start -->
			$(document).on("change", "#locked", function(){
				if($("#locked").is(":checked")){
					// set expiration to use default lockout time
					var expire_moment = moment().add(<?php echo LOCKED_MINUTES; ?>, 'minutes');;
					$('#locked_expiration').data("DateTimePicker").date(expire_moment);
					
					$("#lockout_expiration_contain").slideDown("fast");
				}else{
					$("#lockout_expiration_contain").slideUp("fast");
				}
			});
		
			$('#security_btn').click(function(){	
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				// grab user info from database and store in session
				$.ajax({
					url: "/admin/ajax/get_user_info.php",
					type: "POST",
					data: {
						user_id : columns['user_id']
					},
					dataType: "json",
					success: function(response){						
						if(response['success'] == true){
							// load data to modal
							$('#security_name').html(columns['name']);
							$('#google_auth_secret').html(response['user_info']['google_auth_secret']);
							$('#userlevel').val(response['user_info']['userlevel']).change();
							
							if(response['user_info']['locked'] == true){
								$('#locked').bootstrapToggle('on');
								var expire_moment = new moment(response['user_info']['locked_expiration']);
								$('#locked_expiration').data("DateTimePicker").date(expire_moment);
							}
							if(response['user_info']['google_auth_enabled'] == true){
								$('#google_auth_enabled').bootstrapToggle('on');
							}
							if(response['user_info']['force_password_change'] == true){
								$('#force_password_change').bootstrapToggle('on');
							}
							
							// show modal
							$('#security_modal').modal('show');
						}else{
							bootbox.alert("Operation Failed!\n<br>"+response['query']);
						}
					}
				});
			});
			
			$('#security_save').click(function(){
				// get database id of selected row
				var customer_id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
		
				$.ajax({
					url: "/admin/ajax/datatables/manage_users_buttons.php",
					type: "POST",
					data: {
						action : "update_security",
						id : customer_id,
						user_id : columns['user_id'],
						userlevel: $('#userlevel').val(),
						pin : $('#pin').val(),
						locked : +$('#locked').is(":checked"),
						google_auth_enabled : +$('#google_auth_enabled').is(":checked"),
						force_password_change : +$('#force_password_change').is(":checked"),
						user_id : columns['user_id'],
						locked_expiration: $('#locked_expiration').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss'),
						pass : $('#pass').val(),
						conf_pass : $('#conf_pass').val()
					},
					dataType: "json",
					success: function(response){						
						if(response['result'] != false){
							$('#security_modal').modal('hide');
							data_table.draw(true);							
						}else{
							bootbox.alert("Operation Failed!\n<br>"+response['query']);
						}
					}
				});
			});
		<!-- Security Modal End -->	
		});
	</script>
	
	<div class="container-fluid">
		<div id="loading-spinner"><div></div></div>
	
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
					
					<div class="btn-group pull-right" style="margin-left: 15px;">
						<a id="security_btn" class="btn btn-default btn-sm btn-row-action">Security Settings</a>
					</div>
					
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Ban User</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">User ID</th>
								<th class="text-left">Name</th>
								<th class="text-left">Username</th>
								<th class="text-left">Email</th>
								<th class="text-left">Last Login</th>
								<th class="text-left">Locked</th> 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for adding / editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">User Details</h4>
						<div id="name" class="pull-right" style="padding-right: 6px;">
							
						</div>
						<div id="modal_row_id" style="display: none;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="firstname">First Name</label>
								<input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name">
							</div>
						</div> 

						<div class="col-sm-4">
							<div class="form-group">
								<label for="midname">Middle Name</label>
								<input type="text" class="form-control" id="midname" name="midname" placeholder="Middle Name">
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">
								<label for="lastname">Last Name</label>
								<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name">
							</div>
						</div>

						<div class="col-sm-4">								
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" class="form-control" id="username" name="username" placeholder="Username">
							</div>	
						</div>
						
						<div class="col-sm-4">								
							<div class="form-group">
								<label for="email">Email</label>
								<input type="text" class="form-control" id="email" name="email" placeholder="Email Address">
							</div>	
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Modal for Security Settings -->
	<div class="modal fade" id="security_modal" tabindex="-1" role="dialog" aria-labelledby="security_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="security_modal_label">Security Settings</h4>

					<div id="security_name" class="pull-right" style="padding-right: 6px;">
					
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div class="col-sm-6">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="google_auth_enabled">2FA Enabled</label><br>
								<input id="google_auth_enabled" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="130">
								<span style="float: right; padding-top: 5px;" id="google_auth_secret"></span>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="force_password_change">Force Password Change</label><br>
								<input id="force_password_change" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="130">
							</div>
						</div>
		
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="pass">New Password</label>
								<input type="password" class="form-control" id="pass" placeholder="New Password">
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="conf_pass">Confirm Password</label>
								<input type="password" class="form-control" id="conf_pass" placeholder="Confirm Password">
							</div>
						</div>
			
						<div class="col-sm-12">
							<div class="form-group">
								<label for="userlevel">User Level</label>
								<select id="userlevel" name="userlevel" class="form-control selectpicker">
									<option value="1">1 - Email Activation Not Completed</option>
									<option value="2">2 - Admin Activation Not Completed</option>
									<option value="3">3 - Registered User</option>
									<option value="4">4 - Not Defined</option>
									<option value="5">5 - Not Defined</option>
									<option value="6">6 - Not Defined</option>
									<option value="7">7 - Privileged User</option>
									<option value="8">8 - Administration</option>
									<option value="9">9 - Global Admin</option>
								</select>
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label" for="locked">Account Locked</label><br>
							<input id="locked" type="checkbox" data-toggle="toggle" data-on="Locked" data-off="Not Locked" data-width="130">
						</div>
					</div>
					
					<div class="col-sm-6">

					</div>
					
					<div id="lockout_expiration_contain" class="col-sm-6" style="display: none;">
						<label class="col-sm-12 control-label" for="locked_expiration" style="text-align: center;"><br>Account Lock Expiration:<br><br></label>

						<div class="dtp-inlne" id="locked_expiration"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="security_save">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal to Confirm Ban -->
	<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Ban</h4>
				</div>
				<div class="modal-body">
					Are you sure you would like to exclude this user from using the site?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="remove_confirm">Ban</button>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
