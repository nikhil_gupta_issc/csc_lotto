<?php
	/*
	No cache!!
	*/
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");// always modified
	header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache"); // HTTP/1.0
	/*
	End of No cache
	*/

	// add this to convert line feeds from mac files so fgetcsv will work
	// otherwise CR wil not work, but CRLF and LF will
	ini_set('auto_detect_line_endings', true);
	
	include("header.php");
	
	$db_table = 'customers';
	$item_name = "customer";

	function get_table_fields($db_table){
		global $db;
		
		$results = array();
		
		$excluded_fields = array();
		
		$q = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = '$db_table' ORDER BY COLUMN_NAME ASC";
		$columns = $db->query($q);
		
		if(count($columns) > 0){
			foreach($columns as $col){
				if(!in_array($col['COLUMN_NAME'], $excluded_fields)){
					$results[] = $col['COLUMN_NAME'];
				}
			}
		}
		
		return $results;
	}
	
	function guess_column($search_term, $db_fields){
		// try to guess the correct column based on header in file
		$search_term = strtolower($search_term);
		$search_term = str_ireplace(".", "", $search_term);
		$search_term = str_ireplace(" ", "", $search_term);
		$search_term = str_ireplace("_", "", $search_term);
		
		// help some fields match
		if($search_term == "iCustomerID"){
			$search_term = "customer_id";
			$table = "customers";
		}elseif($search_term == "sCustomerNo"){
			$search_term = "customer_number";
			$table = "customers";
		}elseif($search_term == "sUsername"){
			$search_term = "shifttype";
			$table = "users";
		}elseif($search_term == "sPassword"){
			$search_term = "pin";
			$table = "customers";
		}elseif($search_term == "sFirstName"){
			$search_term = "firstname";
			$table = "users";
		}elseif($search_term == "sLastName"){
			$search_term = "lastname";
			$table = "users";
		}elseif($search_term == "sTel"){
			$search_term = "telephone";
			$table = "user_info";
		}elseif($search_term == "sCell"){
			$search_term = "cellphone";
			$table = "user_info";
		}elseif($search_term == "sAddress"){
			$search_term = "address";
			$table = "user_info";
		}elseif($search_term == "sCountry"){
			$search_term = "country";
			$table = "user_info";
		}elseif($search_term == "sIsland#"){
			$search_term = "island_id";
			$table = "user_info";
		}elseif($search_term == "sEmail"){
			$search_term = "email";
			$table = "users";
		}elseif($search_term == "sGender"){
			$search_term = "gender";
			$table = "user_info";
		}elseif($search_term == "dtDOB"){
			$search_term = "date_of_birth";
			$table = "user_info";
		}elseif($search_term == "iIdentifierID"){
			$search_term = "";
			$table = "";
		}elseif($search_term == "sIdentifierNo"){
			$search_term = "";
			$table = "";
		}elseif($search_term == "bLocked"){
			$search_term = "locked";
			$table = "users";
		}elseif($search_term == "bChangePassword"){
			$search_term = "force_password_change";
			$table = "users";
		}elseif($search_term == "bDisabled"){
			$search_term = "is_disabled";
			$table = "customers";
		}elseif($search_term == "bIsVerified"){
			$search_term = "is_verified";
			$table = "customers";
		}elseif($search_term == "iVerifiedBy"){
			$search_term = "verified_by";
			$table = "customers";
		}elseif($search_term == "dtLastLogin"){
			$search_term = "last_login";
			$table = "users";
		}elseif($search_term == "sLastLoginIP"){
			$search_term = "ip";
			$table = "users";
		}elseif($search_term == "iCreatedBy"){
			$search_term = "created_by";
			$table = "customers";
		}elseif($search_term == "dtCreatedOn"){
			$search_term = "created_on";
			$table = "customers";
		}elseif($search_term == "iUpdatedBy"){
			$search_term = "updated_by";
			$table = "customers";
		}elseif($search_term == "dtUpdatedOn"){
			$search_term = "updated_on";
			$table = "customers";
		}
		
		$match = "";
		foreach($db_fields as $db_field){
			$db_term = strtolower($db_field);
			$db_term = str_ireplace(" ", "", $db_term);
			$db_term = str_ireplace("_", "", $db_term);
			
			if($db_term == $search_term){
				$match = $db_field;
			}
		}
		
		return $match;
	}
?> 
	<script>
		function updateBGcolor(id, selected){
			var td = document.getElementById(id);
			if (selected=='0'){
				td.style.backgroundColor = "#ffcccc";
			}else if (selected=='99'){
				td.style.backgroundColor = "#cccccc";
			}else{
				td.style.backgroundColor = "#ccffcc";
			}
		}
		
		$(document).ready(function() {
			$('INPUT[type="file"]').change(function () {
				var ext_arr = this.value.match(/\.([0-9a-z]+)(?:[\?#]|$)/);
				if(ext_arr){
					ext = ext_arr[1];
					switch (ext) {
						case 'csv':
						case 'txt':
						case 'text':
							$('#upload_btn').attr('disabled', false);
							break;
						default:
							alert('This is not an allowed file type.');
							this.value = '';
					}
				}else{
					$('#upload_btn').attr('disabled', false);
				}
			});
		});
	</script>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<div class="container">
		<div class="well clearfix">
	<?php 
		if(isset($_POST["filename"])){ 
			// open previously uploaded file and modify it according to the field types selected

			// get proper column types set on the previous page
			$fields_selected = $_POST["fields"];
			
			$db_data = array();

			$filename = $_POST["filename"];
			$upload = $_POST["upload"];
			$handle = fopen($filename, "r");
			$row = 0;
			$rr = 1;
			$emp_names = array();
			while(($data = fgetcsv($handle, 8000, ",")) !== FALSE){
				$col = 0;
				$col_count = count($data);
				
				// check for blank names
				$blank_name = false;
				for($c = 0; $c < $col_count; $c++){
					if($fields_selected[$c] != "-1"){
						if($fields_selected[$c] != "0"){							
							if($row != 0){
								if(($fields_selected[$c] == "first_name") && ($data[$c] == '')){
									$blank_name = true;
								}
								if(($fields_selected[$c] == "last_name") && ($data[$c] == '')){
									$blank_name = true;
								}
							}
							$col++;
						}
					}
				}
				
				$rr++;
				if($blank_name == false){
					for($c = 0; $c < $col_count; $c++){
						if($fields_selected[$c] != "-1"){
							if($fields_selected[$c] != "0"){							
								if($row != 0){										
									if($blank_name == false){
										$db_data[$row][$fields_selected[$c]] = $data[$c];
									}
								}
								$col++;
							}
						}
					}
					$row++;
				}else{
					echo "
						<div class='alert alert-warning alert-block'>
							<i class='icon-warning-sign'></i><button type='button' class='close' data-dismiss='alert'>&times;</button>
							<strong>Warning:</strong> Row ".$rr." was ignored because it did not contain a first and last name.
						</div>
					";	
				}
			}
			
			// get current list of employees before importing anything
			if(isset($_POST["delete_missing"])){
				if($_POST["delete_missing"] == 1){
					$q = "SELECT * FROM `$db_table` WHERE `org_id`='".$org_id."' AND `org_type`='".$session->userinfo['org_type']."'";
					$employees = $db->query($q);
				}
			}
			$imported_ids = array();
			
			$i = 0;
			foreach($db_data as $db_row){
				// set org id and type			
				$key_value_pairs = array();
				$key_value_pairs[$i] = "`org_type` = '".$session->userinfo['org_type']."'";
				if($session->userinfo['org_type'] == "district"){
					$i++;
					$key_value_pairs[$i] = " `org_id` = '".$session->userinfo['district_id']."'";
					$org_id = $session->userinfo['district_id'];
				}elseif($session->userinfo['org_type'] == "iu"){
					$i++;
					$key_value_pairs[$i] = " `org_id` = '".$session->userinfo['iu_id']."'";
					$org_id = $session->userinfo['iu_id'];
				}
				
				foreach($db_row as $key => $value){						
					// see if key is a valid column name in db
					$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$db_table'";
					$key_exists = $db->queryOneRow($q);
					if($key_exists['count'] > 0){
						$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
						$num_digits = $key_exists['NUMERIC_PRECISION'];
						$num_places = $key_exists['NUMERIC_SCALE'];
					
						// validate data against data type defined in database
						$data_type = $key_exists['DATA_TYPE'];
						switch ($data_type){
							case "tinyint":
								settype($value , "integer");
								break;
							case "mediumint":
								settype($value , "integer");
								break;
							case "smallint":
								settype($value , "integer");
								break;
							case "int":
								settype($value , "integer");
								break;
							case "decimal":
								settype($value , "float");
								break;
							case "float":
								settype($value , "float");
								break;
							case "double":
								settype($value , "float");
								break;
							case "real":
								settype($value , "float");
								break;
							case "bit":
								settype($value , "boolean");
								break;
							case "boolean":
								settype($value , "boolean");
								break;
							case "date":
								if(!$value){
									$value = "NULL";
								}else{
									$value = date('Y-m-d', strtotime($value));
								}
								break;
							case "datetime":
								if(!$value){
									$value = "NULL";
								}else{
									$value = date('Y-m-d H:i:s', strtotime($value));
								}					
								break;
							case "time":
								if(!$value){
									$value = "NULL";
								}else{
									$value = date('H:i:s', strtotime($value));
								}
								break;
							case "timestamp":
								settype($value , "integer");
								break;
							case "year":
								if(!$value){
									$value = "NULL";
								}
								break;
						}
						
						//echo "Type: $data_type, Value: $value<br>";
						
						$i++;
						if($value == "NULL"){
							$key_value_pairs[$i] = " `".$key."` = ".$value;
						}else{
							$key_value_pairs[$i] = " `".$key."` = '".$value."'";
						}
					}
				}
				
				$value_str = implode(",", $key_value_pairs);
				
				// see if this employee already exists
				$q = "SELECT id FROM `$db_table` WHERE `last_name`='".$db_row['last_name']."' AND `first_name`='".$db_row['first_name']."' AND `org_id`='".$org_id."' AND `org_type`='".$session->userinfo['org_type']."'";
				//echo $q;
				$id_info = $db->query($q);
				
				if(count($id_info) == 1){
					$id = $id_info[0]['id'];
					
					// keep track of all successful updates
					// this way we can delete all employees that were not in import file
					$imported_ids[] = $id;
				
					// un-delete it
					$i++;
					$key_value_pairs[$i] = " `deleted` = '0'";
					$value_str = implode(",", $key_value_pairs);
					
					// update info to db		
					$q = "UPDATE `$db_table` SET $value_str WHERE id=$id";
					$result = $db->queryDirect($q);
					if($result){
						echo "
							<div class='alert alert-info alert-block'>
								<i class='icon-ok-sign'></i><button type='button' class='close' data-dismiss='alert'>&times;</button>
								<strong>Success:</strong> ".$db_row['last_name'].", ".$db_row['first_name']." has been updated.
							</div>
						";
					}else{
						echo "
							<div class='alert alert-danger alert-block'>
								<i class='icon-warning-sign'></i><button type='button' class='close' data-dismiss='alert'>&times;</button>
								<strong>Failure:</strong> ".$db_row['last_name'].", ".$db_row['first_name']." could not be updated.<br>
							</div>
						";		
					}
				}elseif(count($id_info) > 1){
					// more than one match (should not be possible but ignore by setting to invalid id)
					$id = -1000;
				}else{
					// try to insert new info into db		
					$q  = "INSERT INTO `$db_table` SET $value_str";					
					$new_id = $db->queryInsert($q);
					
					if($new_id > 0){
						$id = $new_id;

						echo "
							<div class='alert alert-success alert-block'>
								<i class='icon-ok-sign'></i><button type='button' class='close' data-dismiss='alert'>&times;</button>
								<strong>Success:</strong> ".$db_row['last_name'].", ".$db_row['first_name']." has been added.
							</div>
						";				
					}
				}
			}
			
			// if option was set to delete missing employees, do so npw
			if(isset($_POST["delete_missing"])){
				if($_POST["delete_missing"] == 1){
					foreach($employees as $employee){
						// if not imported/updated, set it as deleted
						if(!in_array($employee['id'], $imported_ids)){
							$q = "UPDATE `$db_table` SET `deleted`=1 WHERE id=".$employee['id'];
							//echo $q."<br>";
							$db->query($q);
						}
					}
				}
			}
		}else{ ?>
			<fieldset>
				<legend><b>Select a CSV File to Upload:</b></legend>
				<div style="padding:10px; text-align: center;">
					<form action="" method="post" enctype="multipart/form-data">
						<input id="file_data" name="file_data" type="file" class="file" size="30" accept="text/comma-separated-values,.csv,text/csv,text/plain,application/csv,application/vnd.ms-excel">
						<input id="folder" name="folder" type="hidden" value="/srv/www/uploads/">
						<input id="upload_btn" class="btn btn-default" name="submit" type="submit" value="Upload" disabled="true">
						<br><br><i><b>Note:</b> Files must be <?php echo ini_get("upload_max_filesize"); ?> or smaller and should be plain text with comma separated values.  In addition, the first row of the file must contain column headers.</i>
					</form>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend><b>Preview Data:</b></legend>
				<form action="" method="post" enctype="multipart/form-data">
				<?php
					// Returns a file size limit in bytes based on the PHP upload_max_filesize
					// and post_max_size
					function file_upload_max_size() {
						static $max_size = -1;

						if ($max_size < 0) {
							// Start with post_max_size.
							$max_size = parse_size(ini_get('post_max_size'));

							// If upload_max_size is less, then reduce. Except if upload_max_size is
							// zero, which indicates no limit.
							$upload_max = parse_size(ini_get('upload_max_filesize'));
							if ($upload_max > 0 && $upload_max < $max_size) {
								$max_size = $upload_max;
							}
						}
						return $max_size;
					}

					function parse_size($size) {
						$unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
						$size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
						if ($unit) {
							// Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
							return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
						} else {
							return round($size);
						}
					}
				
					if(isset($_POST["folder"])){	
						// Valid file extensions (csv txt text)
						$valid_ext_regex = "/^\.(csv|txt|text|tsv){1}$/i";
					
						if(is_uploaded_file($_FILES['file_data']['tmp_name'])){
							//  sanitize file name
							//     - remove extra spaces/convert to _,
							//     - remove non 0-9a-Z._- characters,
							//     - remove leading/trailing spaces
							//  check if under 10MB,
							//  check file extension for legal file types
							$org_name = $_FILES['file_data']['name'];
							$safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($_FILES['file_data']['name']));
							$target_file = $_POST['folder'] . time() . "-" . $safe_filename;
							if($_FILES['file_data']['size'] <= file_upload_max_size() && preg_match($valid_ext_regex, strrchr($safe_filename, '.'))){
								move_uploaded_file ($_FILES['file_data']['tmp_name'], $target_file);
							}
						}
						
						// Store data from csv file into an array
						$r = 0;
						$col_count = 99;
						$handle = fopen($target_file, "r");
						while(($data = fgetcsv($handle, 8000, ",")) !== FALSE) {
							
							//print_r($data);  die();
							
							$col_count = count($data);
							for($c=0; $c<$col_count; $c++){									
								$csv_data[$c][$r] = $data[$c];											
							}
							$r++;
						}

						echo "Please verify that the correct field type is selected for all data shown. All fields highlighted in red must be corrected before continuing.<br><br>";

						$db_fields = get_table_fields($db_table);
						
						// show table with preview of the data
						$start = 0;
						while($start < $col_count){
							echo "<table border='1' bordercolor='grey' cellpadding='5' width='100%' style='border-spacing:0;'>\n";
							for($r = 0; $r < 4; $r++){
								if($r == 0){
									// header row with select boxes for field types
									echo "<tr>\n";
									for($c = $start; $c < $start + 4; $c++){
										if($c < $col_count){
											$selected = guess_column(trim($csv_data[$c][$r]), $db_fields);
											
											//set cell color
											if($selected == ""){
												$cell_color = "#ffcccc";
											}else{
												$cell_color = "#ccffcc";
											}
											echo "<td id='td$c' width='25%' bgcolor='$cell_color'>\n";
											
											$q='"';
											
											echo ($c+1).". 
											<select name=".$q."fields[$c]".$q." onchange=".$q."updateBGcolor('td$c',this.value);".$q.">
												<option value='0'>Please select...</option>
												<option value='-1'>Ignore this field</option>
											";
											
											foreach($db_fields as $field){
												$sel = $field == $selected ? "selected" : "";
												
												// convert to proper case and remove underscores for more friendly view
												$nice_field = ucwords(str_replace("_", " ", $field));
												
												echo "<option value='".$field."' ".$sel.">".$nice_field."</option> ";
											}
											
											echo "
											</select>";
											echo "</td>\n";
										}
									}
									echo "</tr>\n";
								}
								
								// sample data rows
								echo "<tr>\n";
								for($c = $start; $c < $start + 4; $c++){
									if($c < $col_count){
										$col = trim($csv_data[$c][$r]);
										if(array_key_exists($c, $csv_data) && is_array($csv_data[$c])){
											if(array_key_exists($r, $csv_data[$c])){
												echo "<td title='".$csv_data[$c][$r]."'>$col</td>\n";
											}else{
												echo "<td></td>\n";
											}
										}else{
											echo "<td></td>\n";
										}
									}
								}
								echo "</tr>\n";
								
							}
							echo "</table><br>\n";
							$start = $start + 4;
						}
						echo "<div align='center'><i><b>Note:</b> Only the first three rows of data are shown.</i></div>";
					}else{
						echo "<div align='center'><br><br>After you upload the CSV file, a preview of the data will appear here.<br><br><br><br></div>";
					}
				?>
			</fieldset>
			<input id="filename" name="filename" type="hidden" value="<?php echo $target_file; ?>">
			<input id="upload" name="upload" type="hidden" value="<?php echo $org_name; ?>">
			<br>
			<span style='float:right; margin-top:12px;'><input name='submit' class='btn btn-large btn-primary' type='submit' value='Submit'/></span>
			<span style='float:right; margin: 22px;'><input style='margin-top: -2px;' type="checkbox" name="delete_missing" value="1"> Mark any <?php echo $item_name; ?> not found in this file as inactive</span>
			</form>
		<?php } ?>
		</div>
	</div>
	
	<?php include 'footer.php'; ?>