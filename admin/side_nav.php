<script>
	$(document).ready(function(){
		$('.dataTable').closest('.panel').children('.panel-heading').prepend("<div class='btn-group pull-right' style='padding-left:10px;'><a id='export_btn' href='#' class='btn btn-primary btn-sm'>Export</a></div>");
		
		$(document).on("click","#export_btn",function(){
			$.ajax({
				url: "/admin/ajax/datatables/get_datatable_url.php",
				type: "POST",
				success: function(data){
					// hide buttons and reload table if successful
					window.location.href = data;
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
		});
	});
</script>


<div class="panel-group col-sm-4 col-md-3 col-xl-2" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
		<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#collapse_tasks" data-parent="#accordion">
			<h4 class="panel-title">
				<a <?php if($accordion_opened != "tasks" || $accordion_opened == ""){ echo "class=\"collapsed\""; } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_tasks" aria-expanded="true" aria-controls="collapse_tasks">
					<i class="fa fa-tasks"></i> Tasks
				</a>
				<span style="margin-top: 7px;" class="caret pull-right"></span>
			</h4>
		</div>
		<div id="collapse_tasks" class="panel-collapse collapse <?php if($accordion_opened == "tasks" || $accordion_opened == ""){ echo "in"; } ?>" role="tabpanel" aria-labelledby="heading_tasks">
			<div class="panel-body">
				<div class="list-group">
					<a href="/admin/" class="list-group-item <?php if($link_opened == ""){ echo "active"; } ?>">Dashboard</a>
				
					<?php $perm = $core->permission_granted("/admin/tasks/disputes/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/tasks/disputes/" class="list-group-item <?php if($link_opened == "disputes"){ echo "active"; } ?>">Disputes</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/tasks/manage_payouts/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/tasks/manage_payouts/" class="list-group-item <?php if($link_opened == "manage_payouts"){ echo "active"; } ?>">Manage Payouts</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/tasks/manage_shifts/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/tasks/manage_shifts/" class="list-group-item <?php if($link_opened == "manage_shifts"){ echo "active"; } ?>">Manage Shifts</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/tasks/manage_winning_numbers/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/tasks/manage_winning_numbers/" class="list-group-item <?php if($link_opened == "manage_winning_numbers"){ echo "active"; } ?>">Manage Winning Numbers</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/tasks/view_void_rapidballs_ticket/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/tasks/view_void_rapidballs_ticket/" class="list-group-item <?php if($link_opened == "view_void_rapidballs_ticket"){ echo "active"; } ?>">View/Void Rapidballs Tickets</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/tasks/view_void_ticket/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/tasks/view_void_ticket/" class="list-group-item <?php if($link_opened == "view_void_ticket"){ echo "active"; } ?>">View/Void Tickets</a><?php } ?>
				</div>
			</div>
			
		
			
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading cursor-pointer" role="tab" id="heading_maintenance" data-toggle="collapse" data-target="#collapse_maintenance" data-parent="#accordion">
			<h4 class="panel-title">
				<a <?php if($accordion_opened != "maintenance"){ echo "class=\"collapsed\""; } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_maintenance" aria-expanded="false" aria-controls="collapse_maintenance">
					<i class="fa fa-wrench"></i> Maintenance
				</a>
				<span style="margin-top: 7px;" class="caret pull-right"></span>
			</h4>
		</div>
		<div id="collapse_maintenance" class="panel-collapse collapse <?php if($accordion_opened == "maintenance"){ echo "in"; } ?>" role="tabpanel" aria-labelledby="heading_maintenance">
			<div class="panel-body">
				<div class="list-group">
				<?php $perm = $core->permission_granted("/admin/maintenance/adollar/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/adollar/" class="list-group-item <?php if($link_opened == "adollar"){ echo "active"; } ?>">A Dollar Import</a><?php } ?>
				<?php $perm = $core->permission_granted("/admin/maintenance/adollar_report/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/adollar_report/" class="list-group-item <?php if($link_opened == "adollar_report"){ echo "active"; } ?>">A Dollar Report</a><?php } ?>
				<?php $perm = $core->permission_granted("/admin/maintenance/atmpins/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/atmpins/" class="list-group-item <?php if($link_opened == "atmpins"){ echo "active"; } ?>">ATM PINs</a><?php } ?>
				<?php $perm = $core->permission_granted("/admin/maintenance/casino_settings/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/casino_settings/" class="list-group-item <?php if($link_opened == "casino_settings"){ echo "active"; } ?>">Casino Settings</a><?php } ?>
				<?php $perm = $core->permission_granted("/admin/maintenance/cms_pages/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/cms_pages/" class="list-group-item <?php if($link_opened == "cms_pages"){ echo "active"; } ?>">CMS Pages</a><?php } ?>		
				<?php $perm = $core->permission_granted("/admin/maintenance/credit_settings/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/credit_settings/" class="list-group-item <?php if($link_opened == "credit_settings"){ echo "active"; } ?>">Creditcard Settings</a><?php } ?>
				<?php /*$perm = $core->permission_granted("/admin/maintenance/casino_games/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/casino_games/" class="list-group-item <?php if($link_opened == "casino_games"){ echo "active"; } ?>">Casino Games</a><?php }*/ ?>
				<?php $perm = $core->permission_granted("/admin/maintenance/file_checks/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/file_checks/" class="list-group-item <?php if($link_opened == "file_checks"){ echo "active"; } ?>">File Integrity Checks</a><?php } ?>
				<?php $perm = $core->permission_granted("/admin/maintenance/gift_cards/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/gift_cards/" class="list-group-item <?php if($link_opened == "gift_cards"){ echo "active"; } ?>">Gift Cards</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/approved_files/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/approved_files/" class="list-group-item <?php if($link_opened == "approved_files"){ echo "active"; } ?>">GLI Approved Files</a><?php } ?>
					
					<?php $perm = $core->permission_granted("/admin/maintenance/manage_locations/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/manage_locations/" class="list-group-item <?php if($link_opened == "manage_locations"){ echo "active"; } ?>">Locations</a><?php } ?>
				
					<?php $perm = $core->permission_granted("/admin/maintenance/games/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/games/" class="list-group-item <?php if($link_opened == "games"){ echo "active"; } ?>">Lotto Games</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/manage_houses/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/manage_houses/" class="list-group-item <?php if($link_opened == "manage_houses"){ echo "active"; } ?>">Lotto Houses</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/pay_rate_schemes/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/pay_rate_schemes/" class="list-group-item <?php if($link_opened == "pay_rate_schemes"){ echo "active"; } ?>">Lotto Pay Rate Schemes</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/lotto_limits/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/lotto_limits/" class="list-group-item <?php if($link_opened == "ball_limits"){ echo "active"; } ?>">Lotto Purchase Limits</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/loyalty_items/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/loyalty_items/" class="list-group-item <?php if($link_opened == "loyalty_items"){ echo "active"; } ?>">Loyalty Items</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/pos_news_ticker/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/pos_news_ticker/" class="list-group-item <?php if($link_opened == "pos_news_ticker"){ echo "active"; } ?>">POS News Ticker</a><?php } ?>
					<!--<?php $perm = $core->permission_granted("/admin/maintenance/p_pages/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/p_pages/" class="list-group-item <?php if($link_opened == "p_pages"){ echo "active"; } ?>">Promotion Pages</a><?php } ?>-->
						<?php $perm = $core->permission_granted("/admin/maintenance/loss_calculation/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/loss_calculation/" class="list-group-item <?php if($link_opened == "loss_calculation"){ echo "active"; } ?>">Rebate Settings</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/rapidballs_settings/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/rapidballs_settings/" class="list-group-item <?php if($link_opened == "rapidballs_settings"){ echo "active"; } ?>">Rapidballs Settings</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/rapidballs_ip_settings/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/rapidballs_ip_settings/" class="list-group-item <?php if($link_opened == "rapidballs_ip_settings"){ echo "active"; } ?>">Rapidball IP Settings</a><?php } ?>
                    <?php $perm = $core->permission_granted("/admin/maintenance/sportbook_setting/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/sportbook_setting/" class="list-group-item <?php if($link_opened == "sportbook_setting"){ echo "active"; } ?>">Sportbook Settings</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/receipt_messages/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/receipt_messages/" class="list-group-item <?php if($link_opened == "receipt_messages"){ echo "active"; } ?>">Receipt Messages</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/security_groups/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/security_groups/" class="list-group-item <?php if($link_opened == "security_groups"){ echo "active"; } ?>">Security Groups</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/system_settings/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/system_settings/" class="list-group-item <?php if($link_opened == "system_settings"){ echo "active"; } ?>">System Settings</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/topup_settings/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/topup_settings/" class="list-group-item <?php if($link_opened == "topup_settings"){ echo "active"; } ?>">Topup Settings</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/maintenance/vouchers/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/maintenance/vouchers/" class="list-group-item <?php if($link_opened == "vouchers"){ echo "active"; } ?>">Vouchers</a><?php } ?>
					
					
					
					
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading cursor-pointer" role="tab" id="heading_users" data-toggle="collapse" data-target="#collapse_users" data-parent="#accordion">
			<h4 class="panel-title">
				<a <?php if($accordion_opened != "users"){ echo "class=\"collapsed\""; } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_users" aria-expanded="false" aria-controls="collapse_users">
					<i class="fa fa-users"></i> Users
				</a>
				<span style="margin-top: 7px;" class="caret pull-right"></span>
			</h4>
		</div>
		<div id="collapse_users" class="panel-collapse collapse <?php if($accordion_opened == "users"){ echo "in"; } ?>" role="tabpanel" aria-labelledby="heading_users">
			<div class="panel-body">
				<div class="list-group">
					<?php $perm = $core->permission_granted("/admin/users/backoffice_users/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/users/backoffice_users/" class="list-group-item <?php if($link_opened == "backoffice_users"){ echo "active"; } ?>">Backoffice Users</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/users/banned_users/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/users/banned_users/" class="list-group-item <?php if($link_opened == "banned_users"){ echo "active"; } ?>">Banned Users</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/users/exclusions/index.php"); if($perm['exclusions'] !== 0){ ?><a href="/admin/users/exclusions/" class="list-group-item <?php if($link_opened == "exclusions"){ echo "active"; } ?>">Exclusions</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/users/manage_cashiers/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/users/manage_cashiers/" class="list-group-item <?php if($link_opened == "manage_cashiers"){ echo "active"; } ?>">Cashiers</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/users/manage_customers/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/users/manage_customers/" class="list-group-item <?php if($link_opened == "manage_customers"){ echo "active"; } ?>">Customers</a>  <?php } ?>
					<?php $perm = $core->permission_granted("/admin/users/customer_kyc/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/users/customer_kyc/" class="list-group-item <?php if($link_opened == "customer_kyc"){ echo "active"; } ?>">Customers KYC</a>  <?php } ?>
					<?php $perm = $core->permission_granted("/admin/users/dormant_users/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/users/dormant_users/" class="list-group-item <?php if($link_opened == "dormant_users"){ echo "active"; } ?>">Current Inactive / Dormant</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/users/active_users/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/users/active_users/" class="list-group-item <?php if($link_opened == "active_users"){ echo "active"; } ?>">Logged In Users</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/users/user_accounts/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/users/user_accounts/" class="list-group-item <?php if($link_opened == "user_accounts"){ echo "active"; } ?>">User Accounts</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/users/import_customers/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/users/import_customers/" class="list-group-item <?php if($link_opened == "import_customers"){ echo "active"; } ?>">Import Customers</a><?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading cursor-pointer" role="tab" id="heading_cashier_reporting" data-toggle="collapse" data-target="#collapse_cashier_reporting" data-parent="#accordion">
			<h4 class="panel-title">
				<a <?php if($accordion_opened != "cashier_reporting"){ echo "class=\"collapsed\""; } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_cashier_reporting" aria-expanded="false" aria-controls="collapse_cashier_reporting">
					<i class="fa fa-bar-chart"></i> Cashier Reporting
				</a>
				<span style="margin-top: 7px;" class="caret pull-right"></span>
			</h4>
		</div>
		<div id="collapse_cashier_reporting" class="panel-collapse collapse <?php if($accordion_opened == "cashier_reporting"){ echo "in"; } ?>" role="tabpanel" aria-labelledby="heading_cashier_reporting">
			<div class="panel-body">
				<div class="list-group">
					<?php $perm = $core->permission_granted("/admin/cashier_reporting/cashier_commission/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/cashier_reporting/cashier_commission/" class="list-group-item <?php if($link_opened == "cashier_commission"){ echo "active"; } ?>">Cashier Commission</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/cashier_reporting/cashier_lotto_sales/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/cashier_reporting/cashier_lotto_sales/" class="list-group-item <?php if($link_opened == "cashier_lotto_sales"){ echo "active"; } ?>">Cashier Lotto Sales</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/cashier_reporting/cashier_lotto_winnings/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/cashier_reporting/cashier_lotto_winnings/" class="list-group-item <?php if($link_opened == "cashier_lotto_winnings"){ echo "active"; } ?>">Cashier Lotto Winnings</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/cashier_reporting/open_shifts/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/cashier_reporting/open_shifts/" class="list-group-item <?php if($link_opened == "open_shifts"){ echo "active"; } ?>">Cashier Open Shifts</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/cashier_reporting/cashier_payouts/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/cashier_reporting/cashier_payouts/" class="list-group-item <?php if($link_opened == "cashier_payouts"){ echo "active"; } ?>">Cashier Payouts</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/cashier_reporting/cashier_pettycash/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/cashier_reporting/cashier_pettycash/" class="list-group-item --><?php if($link_opened == "cashier_pettycash"){ echo "active"; } ?>">Cashier Pettycash</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/cashier_reporting/cashier_phone_card_sales/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/cashier_reporting/cashier_phone_card_sales/" class="list-group-item <?php if($link_opened == "cashier_phone_card_sales"){ echo "active"; } ?>">Cashier Phone Card Sales</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/cashier_reporting/cashier_shift_transactions/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/cashier_reporting/cashier_shift_transactions/" class="list-group-item <?php if($link_opened == "cashier_shift_transactions"){ echo "active"; } ?>">Cashier Shift Transactions</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/cashier_reporting/cashier_shift_verification/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/cashier_reporting/cashier_shift_verification/" class="list-group-item <?php if($link_opened == "cashier_shift_verification"){ echo "active"; } ?>">Cashier Shift Verification</a><?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading cursor-pointer" role="tab" id="heading_location_reporting" data-toggle="collapse" data-target="#collapse_location_reporting" data-parent="#accordion">
			<h4 class="panel-title">
				<a <?php if($accordion_opened != "location_reporting"){ echo "class=\"collapsed\""; } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_location_reporting" aria-expanded="false" aria-controls="collapse_location_reporting">
					<i class="fa fa-area-chart"></i> Location Reporting
				</a>
				<span style="margin-top: 7px;" class="caret pull-right"></span>
			</h4>
		</div>
		<div id="collapse_location_reporting" class="panel-collapse collapse <?php if($accordion_opened == "location_reporting"){ echo "in"; } ?>" role="tabpanel" aria-labelledby="heading_location_reporting">
			<div class="panel-body">
				<div class="list-group">
					<?php $perm = $core->permission_granted("/admin/location_reporting/hourly_sales/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/location_reporting/hourly_sales/" class="list-group-item <?php if($link_opened == "hourly_sales"){ echo "active"; } ?>">Hourly Sales</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/location_reporting/location_commission/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/location_reporting/location_commission/" class="list-group-item <?php if($link_opened == "location_commission"){ echo "active"; } ?>">Location Commission</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/location_reporting/profitability/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/location_reporting/profitability/" class="list-group-item <?php if($link_opened == "profitability"){ echo "active"; } ?>">Profitability</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/location_reporting/transaction_summary/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/location_reporting/transaction_summary/" class="list-group-item <?php if($link_opened == "transaction_summary"){ echo "active"; } ?>">Transaction Summary</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/location_reporting/transfers/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/location_reporting/transfers/" class="list-group-item <?php if($link_opened == "transfers"){ echo "active"; } ?>">Transfers</a><?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading cursor-pointer" role="tab" id="heading_customer_reporting" data-toggle="collapse" data-target="#collapse_customer_reporting" data-parent="#accordion">
			<h4 class="panel-title">
				<a <?php if($accordion_opened != "customer_reporting"){ echo "class=\"collapsed\""; } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_customer_reporting" aria-expanded="false" aria-controls="collapse_customer_reporting">
					<i class="fa fa-line-chart"></i> Customer Reporting
				</a>
				<span style="margin-top: 7px;" class="caret pull-right"></span>
			</h4>
		</div>
		<div id="collapse_customer_reporting" class="panel-collapse collapse <?php if($accordion_opened == "customer_reporting"){ echo "in"; } ?>" role="tabpanel" aria-labelledby="heading_customer_reporting">
			<div class="panel-body">
				<div class="list-group">
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_activity_detail/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_activity_detail/" class="list-group-item <?php if($link_opened == "customer_activity_detail"){ echo "active"; } ?>">Customer Activity Detail</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_adollar_redemptions/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_adollar_redemptions/" class="list-group-item <?php if($link_opened == "customer_adollar_redemptions"){ echo "active"; } ?>">Customer Adollar Redemptions</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_activity_summary/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_activity_summary/" class="list-group-item <?php if($link_opened == "customer_activity_summary"){ echo "active"; } ?>">Customer Activity Summary</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_balances/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_balances/" class="list-group-item <?php if($link_opened == "customer_balances"){ echo "active"; } ?>">Customer Balances</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_cards/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_cards/" class="list-group-item <?php if($link_opened == "customer_cards"){ echo "active"; } ?>">Customer Cards</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/deposits/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/deposits/" class="list-group-item <?php if($link_opened == "deposits"){ echo "active"; } ?>">Customer Deposits</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_exclusions/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_exclusions/" class="list-group-item <?php if($link_opened == "customer_exclusions"){ echo "active"; } ?>">Customer Exclusions</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_lotto_sales/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_lotto_sales/" class="list-group-item <?php if($link_opened == "customer_lotto_sales"){ echo "active"; } ?>">Customer Lotto Sales</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_loyalty_points/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_loyalty_points/" class="list-group-item <?php if($link_opened == "customer_loyalty_points"){ echo "active"; } ?>">Customer Loyalty Points</a><?php } ?>
					
					<?php $perm = $core->permission_granted("/admin/customer_reporting/rebate_customer_estimation/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/rebate_customer_estimation/" class="list-group-item <?php if($link_opened == "rebate_customer_estimation"){ echo "active"; } ?>">Customer Rebate Estimation</a><?php } ?>
                                        <?php $perm = $core->permission_granted("/admin/customer_reporting/customer_rebate_history/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_rebate_history/" class="list-group-item <?php if($link_opened == "customer_rebate_history"){ echo "active"; } ?>">Customer Rebate History</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/rebate_report/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/rebate_report/" class="list-group-item <?php if($link_opened == "rebate_report"){ echo "active"; } ?>">Customer Rebate Report</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_sessions/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_sessions/" class="list-group-item <?php if($link_opened == "customer_sessions"){ echo "active"; } ?>">Customer Sessions</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/customer_voucher_redemptions/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/customer_voucher_redemptions/" class="list-group-item <?php if($link_opened == "customer_voucher_redemptions"){ echo "active"; } ?>">Customer Voucher Redemptions</a><?php } ?>
					
					<?php $perm = $core->permission_granted("/admin/customer_reporting/withdrawals/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/withdrawals/" class="list-group-item <?php if($link_opened == "withdrawals"){ echo "active"; } ?>">Customer Withdrawals</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/dormant_users_log/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/dormant_users_log/" class="list-group-item <?php if($link_opened == "dormant_users_log"){ echo "active"; } ?>">Dormant Customers Log</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/customer_reporting/large_wins/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/customer_reporting/large_wins/" class="list-group-item <?php if($link_opened == "large_wins"){ echo "active"; } ?>">Large Wins</a><?php } ?>
					
					
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading cursor-pointer" role="tab" id="heading_system_reporting" data-toggle="collapse" data-target="#collapse_system_reporting" data-parent="#accordion">
			<h4 class="panel-title">
				<a <?php if($accordion_opened != "system_reporting"){ echo "class=\"collapsed\""; } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_system_reporting" aria-expanded="false" aria-controls="collapse_system_reporting">
					<i class="fa fa-cogs"></i> System Reporting
				</a>
				<span style="margin-top: 7px;" class="caret pull-right"></span>
			</h4>
		</div>
		<div id="collapse_system_reporting" class="panel-collapse collapse <?php if($accordion_opened == "system_reporting"){ echo "in"; } ?>" role="tabpanel" aria-labelledby="heading_system_reporting">
			<div class="panel-body">
				<div class="list-group">
					<?php $perm = $core->permission_granted("/admin/system_reporting/casino_summary/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/casino_summary/" class="list-group-item <?php if($link_opened == "casino_summary"){ echo "active"; } ?>">Casino Summary</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/system_reporting/database_log/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/database_log/" class="list-group-item <?php if($link_opened == "database_log"){ echo "active"; } ?>">Database Log</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/system_reporting/direct_export/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/direct_export/" class="list-group-item <?php if($link_opened == "direct_export"){ echo "active"; } ?>">Direct Export</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/system_reporting/game_performance/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/game_performance/" class="list-group-item <?php if($link_opened == "game_performance"){ echo "active"; } ?>">Game Performance</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/system_reporting/login_attempts/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/login_attempts/" class="list-group-item <?php if($link_opened == "login_attempts"){ echo "active"; } ?>">Login Attempts</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/system_reporting/lotto_sales/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/lotto_sales/" class="list-group-item <?php if($link_opened == "lotto_sales"){ echo "active"; } ?>">Lotto Sales</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/system_reporting/page_loads/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/page_loads/" class="list-group-item <?php if($link_opened == "page_loads"){ echo "active"; } ?>">Page Loads</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/system_reporting/risk_exposure_spread/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/risk_exposure_spread/" class="list-group-item <?php if($link_opened == "risk_exposure_spread"){ echo "active"; } ?>">Risk/Exposure Spread</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/system_reporting/system_summary/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/system_summary/" class="list-group-item <?php if($link_opened == "system_summary"){ echo "active"; } ?>">System Summary</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/system_reporting/unclaimed_lotto/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/system_reporting/unclaimed_lotto/" class="list-group-item <?php if($link_opened == "unclaimed_lotto"){ echo "active"; } ?>">Unclaimed Lotto</a><?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading cursor-pointer" role="tab" id="heading_messaging" data-toggle="collapse" data-target="#collapse_messaging" data-parent="#accordion">
			<h4 class="panel-title">
				<a <?php if($accordion_opened != "messaging"){ echo "class=\"collapsed\""; } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_messaging" aria-expanded="false" aria-controls="collapse_messaging">
					<i class="fa fa-exclamation-circle"></i> Messaging & Alerts
				</a>
				<span style="margin-top: 7px;" class="caret pull-right"></span>
			</h4>
		</div>
		<div id="collapse_messaging" class="panel-collapse collapse <?php if($accordion_opened == "messaging"){ echo "in"; } ?>" role="tabpanel" aria-labelledby="heading_messaging">
			<div class="panel-body">
				<div class="list-group">
					<?php $perm = $core->permission_granted("/admin/messaging/alerts/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/messaging/alerts/" class="list-group-item <?php if($link_opened == "alerts"){ echo "active"; } ?>">Administrative Alerts</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/messaging/alert_log/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/messaging/alert_log/" class="list-group-item <?php if($link_opened == "alert_log"){ echo "active"; } ?>">Alert Log</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/messaging/birthday_messages/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/messaging/birthday_messages/" class="list-group-item <?php if($link_opened == "birthday_messages"){ echo "active"; } ?>">Birthday Messages</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/messaging/cashier_notices/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/messaging/cashier_notices/" class="list-group-item <?php if($link_opened == "cashier_notices"){ echo "active"; } ?>">Cashier Payout Notices</a><?php } ?>
					<?php $perm = $core->permission_granted("/admin/messaging/sms_messages/index.php"); if($perm['permission_granted'] !== 0){ ?><a href="/admin/messaging/sms_messages/" class="list-group-item <?php if($link_opened == "sms_messages"){ echo "active"; } ?>">SMS Message</a><?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
