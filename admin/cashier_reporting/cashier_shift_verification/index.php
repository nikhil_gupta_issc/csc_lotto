<?php
	$page_title = "Shift Verification";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	$end_date = isset($_GET['filter_date_to']) ? "&filter_date_to=".$_GET['filter_date_to'] : "&filter_date_to=".date("Y-m-d", time());
	$_GET['filter_date_to'] = isset($_GET['filter_date_to']) ? $_GET['filter_date_to'] : date("Y-m-d", time());
	$start_date = isset($_GET['filter_date_from']) ? "?filter_date_from=".$_GET['filter_date_from'] : "?filter_date_from=".date("Y-m-d", time());
	$_GET['filter_date_from'] = isset($_GET['filter_date_from']) ? $_GET['filter_date_from'] : date("Y-m-d", time());
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', strtotime($_GET['filter_date_from'])); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', strtotime($_GET['filter_date_to'])); ?>"));
			
			$(function () {
				// initialize datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
			});
			
			$(document).on("click", "#nonexact_verify_btn", function(){
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				$.ajax({
					url: "/admin/ajax/datatables/cashier_shift_verification_buttons.php",
					type: "POST",
					data: {
						action : "nonexact_verify",
						id : id
					},
					dataType: "json",
					success: function(data){
						if(data.success == "true"){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$(document).on("click", "#exact_verify_btn", function(){
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				$.ajax({
					url: "/admin/ajax/datatables/cashier_shift_verification_buttons.php",
					type: "POST",
					data: {
						action : "exact_verify",
						id : id
					},
					dataType: "json",
					success: function(data){
						if(data.success == "true"){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
		
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "cashier_name" },
					{ "data": "shift_id" },
					{ "data": "shift_start" },
					{ "data": "shift_end" },
					{ "data": "float_amount" },
					{ "data": "opening" },
					{ "data": "sales" },
					{ "data": "payouts" },
					{ "data": "closing" },
					{ "data": "expected" },
					{ "data": "over_short" },
					{ "data": "status" }
				],
				"order": [[11, 'asc']],
				"ajax": "/admin/ajax/datatables/cashier_shift_verification.php?filter_date_from="+$('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD')+"&filter_date_to="+$('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
			});
			
			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "cashier_name" },
						{ "data": "shift_id" },
						{ "data": "shift_start" },
						{ "data": "shift_end" },
						{ "data": "float_amount" },
						{ "data": "opening" },
						{ "data": "sales" },
						{ "data": "payouts" },
						{ "data": "closing" },
						{ "data": "expected" },
						{ "data": "over_short" },
						{ "data": "status" }
					],
					"order": [[11, 'asc']],
					"ajax": "/admin/ajax/datatables/cashier_shift_verification.php?filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "cashier_name" },
						{ "data": "shift_id" },
						{ "data": "shift_start" },
						{ "data": "shift_end" },
						{ "data": "float_amount" },
						{ "data": "opening" },
						{ "data": "sales" },
						{ "data": "payouts" },
						{ "data": "closing" },
						{ "data": "expected" },
						{ "data": "over_short" },
						{ "data": "status" }
					],
					"order": [[11, 'asc']],
					"ajax": "/admin/ajax/datatables/cashier_shift_verification.php?filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				//determine whether to show verify or quickbook export
				var columns = data_table.row('.selected').data();
				if(columns['is_verified']=="0"){
					$("#nonexact_verify_btn").show();
					$("#exact_verify_btn").show();
					$("#qb_export_btn").hide();
				}else{
					$("#nonexact_verify_btn").hide();
					$("#exact_verify_btn").hide();
					$("#qb_export_btn").show();
				}
			});
			
			$('#qb_export_btn').click(function(){
				window.location = "/admin/ajax/datatables/cashier_shift_verification_buttons.php?action=export";
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Shift Verification</h3>
					<div class="btn-group pull-right">
						<button id="nonexact_verify_btn" class="btn btn-sm btn-primary btn-row-action" type="button">Non-Exact</button>
						<button id="exact_verify_btn" class="btn btn-sm btn-primary btn-row-action" type="button">Exact</button>
						<button id="qb_export_btn" class="btn btn-sm btn-primary btn-row-action" type="button">Quickbooks Export</button>
					</div>
				</div>
				<div class="panel-body">
				<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Cashier</th>
								<th>Shift ID</th>
								<th>Start</th>
								<th>End</th>
								<th>Float</th>
								<th>Opening</th>
								<th>Sales</th>
								<th>Payouts</th>
								<th>Closing</th>
								<th>Expected</th>
								<th>O/S</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 