<?php
	$page_title = "Open Shifts";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$(function () {
				// initialize datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
			});
		
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "cashier" },
					{ "data": "location" },
					{ "data": "start_date" },
					{ "data": "current_balance" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/open_shifts.php"
			});
			
			$.ajax({
				url: "/admin/ajax/datatables/open_shifts_buttons.php",
				type: "POST",
				data: {
					action : "overall_total"
				},
				dataType: "json",
				success: function(response){
					if(response['result'] != false){
						$("#total_amount").text("$" + response['result']['total']);
					}else{
						alert("Operation Failed!\n"+response['query']);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Open Shifts</h3>
					<div class="btn-group pull-right">
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Cashier</th>
								<th class="text-left">Location</th>
								<th class="text-left">Start Date</th>
								<th class="text-left">Current Balance</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					<table id="totals" class="display table dt-responsive table-striped" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="25%"></th>
								<th width="25%"></th>
								<th width="25%"></th>
								<th>Total Balance</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td id="total_amount"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 