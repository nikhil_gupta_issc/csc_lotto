<?php
	$page_title = "Cashier Pettycash";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$("#cashier").val('<?php echo $_GET['cashier']; ?>');

			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "location" },
					{ "data": "cashier" },
					{ "data": "trans_date" },
					{ "data": "reason" },
					{ "data": "pettycash_amount" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/cashier_pettycash.php?cashier_id="+$('#cashier').val()+"&filter_date_from=<?php echo date('Y-m-d', time()); ?>"
			});
			
			$.ajax({
				url: "/admin/ajax/datatables/cashier_pettycash_buttons.php",
				type: "POST",
				data: {
					action : "overall_total",
					cashier_id : $('#cashier').val(),
					filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
					filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
				},
				dataType: "json",
				success: function(response){
					if(response['result'] != false){
						$("#total").text("$" + response['result']['total']);
					}else{
						alert("Operation Failed!\n"+response['query']);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});

			$(document).on('change', '#cashier', function(e){
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "location" },
						{ "data": "cashier" },	
						{ "data": "trans_date" },
						{ "data": "reason" },				
						{ "data": "pettycash_amount" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/cashier_pettycash.php?cashier_id="+$('#cashier').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				$.ajax({
					url: "/admin/ajax/datatables/cashier_pettycash_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						cashier_id : $('#cashier').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							$("#total").text("$" + response['result']['total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				data_table.draw(true);
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "location" },
						{ "data": "cashier" },	
						{ "data": "trans_date" },
						{ "data": "reason" },				
						{ "data": "pettycash_amount" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/cashier_pettycash.php?cashier_id="+$('#cashier').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				$.ajax({
					url: "/admin/ajax/datatables/cashier_pettycash_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						cashier_id : $('#cashier').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							$("#total").text("$" + response['result']['total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "location" },
						{ "data": "cashier" },
						{ "data": "date" },
						{ "data": "reason" },					
						{ "data": "pettycash_amount" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/cashier_pettycash.php?cashier_id="+$('#cashier').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				$.ajax({
					url: "/admin/ajax/datatables/cashier_pettycash_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						cashier_id : $('#cashier').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							$("#total").text("$" + response['result']['total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				data_table.draw(true);
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Cashier Pettycash</h3>
					<div class="btn-group pull-right">

					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="cashier" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Cashier</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="cashier">
									<option value="">All Cashiers</option>
									<?php
										$cashiers = $db->query("SELECT user_id, firstname, lastname FROM panel_user pu JOIN users u ON pu.user_id=u.id WHERE is_deleted=0 AND petty_cash_rights=1 ORDER BY firstname, lastname");
										foreach($cashiers as $cashier){
											echo "<option value=".$cashier['user_id'].">".$cashier['firstname']." ".$cashier['lastname']." (".$cashier['user_id'].")</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Location</th>
								<th class="text-left">Cashier</th>
								<th class="text-left">Date</th>	
						<th class="text-left">Reason</th>						
								<th class="text-left">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					<table id="totals" class="table table-striped" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="75%"></th>
								<th width="25%">Total Pettycash withdrawal</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td id="total"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
