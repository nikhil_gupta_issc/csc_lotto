<?php
	$page_title = "Cashier Lotto Sales";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));
			$("#cashier").val('<?php echo $_GET['cashier']; ?>');

			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "purchase_date" },
					{ "data": "ticket_number" },
					{ "data": "total" },
					{ "data": "cashier_id" },
					{ "data": "location_id" },
					{ "data": "initial_balance" },
					{ "data": "balance" },
					{ "data": "shift_id" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/cashier_lotto_sales.php?cashier_id="+$('#cashier').val()+"&filter_date_from=<?php echo date('Y-m-d', time()); ?>"
			});
			
			$.ajax({
				url: "/admin/ajax/datatables/cashier_lotto_sales_buttons.php",
				type: "POST",
				data: {
					action : "overall_total",
					cashier_id : $('#cashier').val(),
					filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
					filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
				},
				dataType: "json",
				success: function(response){
					if(response['result'] != false){
						$("#total").text("$" + response['result']['total']);
					}else{
						alert("Operation Failed!\n"+response['query']);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
			
			// temporary transactions datatable initialization
			// definition redefined on row click
			var bet_data_table = $('#bet_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"columns": [
					{ "data": "short_name" },
					{ "data": "ball_string" },
					{ "data": "is_boxed" },
					{ "data": "bet_amount" },
					{ "data": "is_processed" }
				],
				"ajax": "/admin/ajax/datatables/bets.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#view_details_btn').trigger('click');
			});
			
			$('#view_details_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				var type = "";
				
				if($.fn.dataTable.isDataTable('#bet_datatable')){
					bet_data_table.destroy();
				}
				
				bet_data_table = $('#bet_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "short_name" },
						{ "data": "ball_string" },
						{ "data": "is_boxed" },
						{ "data": "bet_amount" },
						{ "data": "is_processed" }
					],
					"ajax": "/admin/ajax/datatables/bets.php?ticket_id="+id
				});
				
				bet_data_table.draw(true);
				var columns = data_table.row('.selected').data();
				console.log(columns["ticket_number"]);
				$.ajax({
					url: "/admin/ajax/datatables/cashier_lotto_sales_buttons.php",
					type: "POST",
					data: {
						action : "bet_total",
						ticket_id : columns['ticket_number']
					},
					dataType: "json",
					success: function(response){
						console.log(response);
						if(response['result'] != false){
							$("#ticket_total").text("$" + response['result']['total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				
				// show modal
				$('#view_details_modal').modal('show');
			});
			
			$('#bet_datatable').on('shown.bs.modal', function() {
				//recalculate the dimensions of datatable
				bet_data_table.columns.adjust().responsive.recalc();
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "purchase_date" },
						{ "data": "ticket_number" },
						{ "data": "total" },
						{ "data": "cashier_id" },
						{ "data": "location_id" },
						{ "data": "initial_balance" },
						{ "data": "balance" },
						{ "data": "shift_id" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/cashier_lotto_sales.php?cashier_id="+$('#cashier').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				$.ajax({
					url: "/admin/ajax/datatables/cashier_lotto_sales_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						cashier_id : $('#cashier').val()
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							$("#total").text("$" + response['result']['total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "purchase_date" },
						{ "data": "ticket_number" },
						{ "data": "total" },
						{ "data": "cashier_id" },
						{ "data": "location_id" },
						{ "data": "initial_balance" },
						{ "data": "balance" },
						{ "data": "shift_id" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/cashier_lotto_sales.php?cashier_id="+$('#cashier').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				$.ajax({
					url: "/admin/ajax/datatables/cashier_lotto_sales_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						cashier_id : $('#cashier').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							$("#total").text("$" + response['result']['total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				data_table.draw(true);
			});
			// cashier change
			$("#cashier").on('change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "purchase_date" },
						{ "data": "ticket_number" },
						{ "data": "total" },
						{ "data": "cashier_id" },
						{ "data": "location_id" },
						{ "data": "initial_balance" },
						{ "data": "balance" },
						{ "data": "shift_id" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/cashier_lotto_sales.php?cashier_id="+$('#cashier').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				$.ajax({
					url: "/admin/ajax/datatables/cashier_lotto_sales_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						cashier_id : $('#cashier').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							$("#total").text("$" + response['result']['total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				data_table.draw(true);
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Cashier Lotto Sales</h3>
					<div class="btn-group pull-right">
						<a id="view_details_btn" href="#" class="btn btn-primary btn-sm btn-row-action">View Details</a>
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="house" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Cashier</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="cashier">
									<option value="">All Cashiers</option>
									<?php
										$cashiers = $db->query("SELECT u.firstname, u.lastname, c.user_id FROM panel_user c JOIN users u ON c.user_id=u.id WHERE is_disabled=0 ORDER BY firstname, lastname");
										foreach($cashiers as $cashier){
											echo "<option value=".$cashier['user_id'].">".$cashier['firstname']." ".$cashier['lastname']." (".$cashier['user_id'].")"."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Purchase Date</th>
								<th class="text-left">Ticket Number</th>
								<th class="text-left">Total</th>
								<th class="text-left">Cashier ID</th>
								<th class="text-left">Location ID</th>
								<th class="text-left">Initial Balance</th>
								<th class="text-left">Ending Balance</th>
								<th class="text-left">Shift ID</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					<table id="totals" class="display table dt-responsive table-striped" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="25%"></th>
								<th width="25%"></th>
								<th width="25%"></th>
								<th>Total Sales</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td id="total"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="view_details_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Ticket Details</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<table id="bet_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Game</th>
									<th class="text-left">Ball String</th>
									<th class="text-left">S/B</th>
									<th class="text-left">Bet Amount</th>
									<th class="text-left">Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="5" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
					<table id="totals" class="display table dt-responsive table-striped" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="25%"></th>
								<th width="25%"></th>
								<th width="25%"></th>
								<th>Total Sales</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td id="ticket_total"></td>
							</tr>
						</tbody>
					</table>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
