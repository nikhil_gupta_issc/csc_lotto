<?php
	$page_title = "Cashier Shift Transactions";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "transaction_id" },
					{ "data": "trans_date" },
					{ "data": "shift_id" },
					{ "data": "type" },
					{ "data": "description" },
					{ "data": "amount" },
					{ "data": "initial_balance" },
					{ "data": "balance" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/cashier_shift_transactions.php?cashier_id="+$('#cashier').val()+"&shift_id="+$('#shift').val()
			});
			
			$('#datatable tbody').on('click', 'tr', function(){				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			$(document).on('change', '#cashier', function(e){
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}

				$("#shift").prop("disabled", true);
				$.ajax({
					url: "/admin/ajax/cashier_shift_transactions.php",
					type: "POST",
					data: {
						action : "get_shifts",
						cashier_id : $("#cashier").val()
					},
					dataType: "json",
					success: function(data){
						if(data.success == true){
							// hide buttons and reload table if successful
							$('#shift').html(data.select_options);
							$("#shift").prop("disabled", false);
						}else{
							bootbox.alert("An unexpected error has occurred");
							$("#shift").prop("disabled", false);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});

				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "transaction_id" },
						{ "data": "trans_date" },
						{ "data": "shift_id" },
						{ "data": "type" },
						{ "data": "description" },
						{ "data": "amount" },
						{ "data": "initial_balance" },
						{ "data": "balance" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/cashier_shift_transactions.php?cashier_id="+$('#cashier').val()+"&shift_id="+$('#shift').val()
				});

				data_table.draw(true);
			});

			$(document).on('change', '#shift', function(e){
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}

				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "transaction_id" },
						{ "data": "trans_date" },
						{ "data": "shift_id" },
						{ "data": "type" },
						{ "data": "description" },
						{ "data": "amount" },
						{ "data": "initial_balance" },
						{ "data": "balance" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/cashier_shift_transactions.php?cashier_id="+$('#cashier').val()+"&shift_id="+$('#shift').val()
				});

				data_table.draw(true);
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Cashier Shift Transactions</h3>
					<div class="btn-group pull-right">
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="house" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Cashier</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="cashier">
									<option value="">All Cashiers</option>
									<?php
										$cashiers = $db->query("SELECT u.firstname, u.lastname, c.user_id FROM panel_user c JOIN users u ON c.user_id=u.id WHERE is_disabled=0 ORDER BY firstname, lastname");
										foreach($cashiers as $cashier){
											echo "<option value=".$cashier['user_id'].">".$cashier['firstname']." ".$cashier['lastname']." (".$cashier['user_id'].")</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>
						<div class="form-group">
							<label for="house" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Shift</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="shift">
									<option value="">All Shifts</option>
									<?php
										$shifts = $db->query("SELECT s.shift_start, s.shift_end, s.id FROM panel_user_shift s JOIN panel_user pu ON s.user_id=pu.user_id ORDER BY s.shift_start DESC");
										foreach($shifts as $shift){
											if($shift['shift_end'] == "0000-00-00 00:00:00"){
												$shift['shift_end'] = "Present";
											}else{
												$shift['shift_end'] = date("m/d/Y h:i A", strtotime($shift['shift_end']));
											}
											echo "<option value=".$shift['id'].">".date("m/d/Y h:i A", strtotime($shift['shift_start']))." - ".$shift['shift_end']." (".$shift['id'].")</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Transaction ID</th>
								<th class="text-left">Date</th>
								<th class="text-left">Shift ID</th>
								<th class="text-left">Type</th>
								<th class="text-left">Description</th>
								<th class="text-left">Amount</th>
								<th class="text-left">Initial Balance</th>
								<th class="text-left">Ending Balance</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 