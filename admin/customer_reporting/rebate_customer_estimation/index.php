<?php
	$page_title = "Customer Rebate Estimation";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#loading-spinner').show();
			$.ajax({
				url: "/admin/ajax/customer_rebate_estimation.php",
				type: "POST",
				data: {
					action : "get_summary",
					start_date : "<?php echo date('Y-m-d'); ?>",
					end_date : "<?php echo date('Y-m-d'); ?>",
					
				},
				dataType: "json",
				success: function(data){
				$('#loading-spinner').hide();
					$("#casino_bet_number").text(data.casino_percentage);
					$("#casino_bet_amount").text(data.casino_rb);
					
					$("#lotto_purchase_amount").text(data.lotto_rb);
					$("#lotto_purchase_number").text(data.lotto_percentage);
					
					
					$("#sportsbook_bet_amount").text(data.sports_rb);
					$("#sportsbook_bet_number").text(data.sports_percentage);
					
					$("#rapidballs_bet_amount").text(data.rapidballs_rb);
					$("#rapidballs_bet_number").text(data.rapidball_percentage);
					
					$("#casino_net_amount").text(data.casino_rb);
					$("#lotto_net_amount").text(data.lotto_rb);
					
					$("#sportsbook_net_amount").text(data.sports_rb);
					$("#rapidballs_net_amount").text(data.rapidball_rb);
					$("#overall_net_amount").text(data.total);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
			
			$(document).on('change', '#customer', function(){
			$('#loading-spinner').show();				
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				$.ajax({
					url: "/admin/ajax/customer_rebate_estimation.php",
					type: "POST",
					data: {
						action : "get_summary",
						start_date : start_date,
						end_date : end_date
						
					},
					dataType: "json",
					success: function(data){
					$('#loading-spinner').hide();
						$("#casino_bet_number").text(data.casino_percentage);
						$("#casino_bet_amount").text(data.casino_rb);
					
						$("#lotto_purchase_amount").text(data.lotto_rb);
						$("#lotto_purchase_number").text(data.lotto_percentage);
					
					
						$("#sportsbook_bet_amount").text(data.sports_rb);
						$("#sportsbook_bet_number").text(data.sports_percentage);
					
						$("#rapidballs_bet_amount").text(data.rapidball_rb);
						$("#rapidballs_bet_number").text(data.rapidball_percentage);
					
						$("#casino_net_amount").text(data.casino_rb);
						$("#lotto_net_amount").text(data.lotto_rb);
					
						$("#sportsbook_net_amount").text(data.sports_rb);
						$("#rapidballs_net_amount").text(data.rapidball_rb);
						$("#overall_net_amount").text(data.total);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
			$('#loading-spinner').show();
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}

				$.ajax({
					url: "/admin/ajax/customer_rebate_estimation.php",
					type: "POST",
					data: {
						action : "get_summary",
						start_date : start_date,
						end_date : end_date
						
					},
					dataType: "json",
					success: function(data){
					$('#loading-spinner').hide();
						$("#casino_bet_number").text(data.casino_percentage);
						$("#casino_bet_amount").text(data.casino_rb);
					
						$("#lotto_purchase_amount").text(data.lotto_rb);
						$("#lotto_purchase_number").text(data.lotto_percentage);
					
					
						$("#sportsbook_bet_amount").text(data.sports_rb);
						$("#sportsbook_bet_number").text(data.sports_percentage);
					
						$("#rapidballs_bet_amount").text(data.rapidball_rb);
						$("#rapidballs_bet_number").text(data.rapidball_percentage);
					
						$("#casino_net_amount").text(data.casino_rb);
						$("#lotto_net_amount").text(data.lotto_rb);
					
						$("#sportsbook_net_amount").text(data.sports_rb);
						$("#rapidballs_net_amount").text(data.rapidball_rb);
						$("#overall_net_amount").text(data.total);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
			$('#loading-spinner').show();
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}

				$.ajax({
					url: "/admin/ajax/customer_rebate_estimation.php",
					type: "POST",
					data: {
						action : "get_summary",
						start_date : start_date,
						end_date : end_date
						
					},
					dataType: "json",
					success: function(data){
					$('#loading-spinner').hide();
						$("#casino_bet_number").text(data.casino_percentage);
						$("#casino_bet_amount").text(data.casino_rb);
					
						$("#lotto_purchase_amount").text(data.lotto_rb);
						$("#lotto_purchase_number").text(data.lotto_percentage);
					
					
						$("#sportsbook_bet_amount").text(data.sports_rb);
						$("#sportsbook_bet_number").text(data.sports_percentage);
					
						$("#rapidballs_bet_amount").text(data.rapidball_rb);
						$("#rapidballs_bet_number").text(data.rapidball_percentage);
					
						$("#casino_net_amount").text(data.casino_rb);
						$("#lotto_net_amount").text(data.lotto_rb);
					
						$("#sportsbook_net_amount").text(data.sports_rb);
						$("#rapidballs_net_amount").text(data.rapidball_rb);
						$("#overall_net_amount").text(data.total);			
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
		});
	</script>

	<div class="container-fluid">
	<div id="loading-spinner"><div></div></div>
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Customer Rebate Estimation</h3>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<div class="table-responsive">
						<table id="summary_table" class="table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Type</th>
									<th class="text-left">Percentage</th>
									<th class="text-left">Amount</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Casino</td>
									<td id="casino_bet_number"></td>
									<td id="casino_bet_amount"></td>
								</tr>
								
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Casino Net:</td>
									<td id="casino_net_amount" style="font-weight:bold;"></td>
								</tr>
								<tr>
									<td>Lotto</td>
									<td id="lotto_purchase_number"></td>
									<td id="lotto_purchase_amount"></td>
								</tr>
								
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Lotto Net</td>
									<td id="lotto_net_amount" style="font-weight:bold;"></td>
								</tr>
								
								
								<tr>
									<td>Sportsbook</td>
									<td id="sportsbook_bet_number"></td>
									<td id="sportsbook_bet_amount"></td>
								</tr>
								
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Sportsbook Net</td>
									<td id="sportsbook_net_amount" style="font-weight:bold;"></td>
								</tr>
								<tr>
									<td>Rapidballs</td>
									<td id="rapidballs_bet_number"></td>
									<td id="rapidballs_bet_amount"></td>
								</tr>
								
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Rapidballs Net</td>
									<td id="rapidballs_net_amount" style="font-weight:bold;"></td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Overall Net</td>
									<td id="overall_net_amount" style="font-weight:bold;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
