<?php
	$page_title = "Customer Sessions";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));
			
			$(function () {
				// initialize datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
			});
		
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			// populate customer select box
			$.ajax({
				url: "/admin/ajax/get_customers.php",
				type: "POST",
				dataType: "json",
				success: function(data){
					$("#customer").html(data.options);
					$("#customer").val('<?php echo isset($_GET['customer']) ? $_GET['customer'] : -1; ?>');
				}
			});
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "customer" },
					{ "data": "session_id" },
					{ "data": "start" },
					{ "data": "end" },
					{ "data": "duration" },
					{ "data": "amount_wagered" },
					{ "data": "amount_won" },
					{ "data": "promo_wagered" },
					{ "data": "promo_received" },
					{ "data": "funds_incomplete" },
					{ "data": "status" }
				],
				"order": [[0, 'asc']],
				"ajax": "/admin/ajax/datatables/customer_sessions.php?filter_date_from=<?php echo date('Y-m-d', time()); ?>"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var row_array = data_table.row(this).data();
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
						
			$("#customer").on('change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "customer" },
						{ "data": "session_id" },
						{ "data": "start" },
						{ "data": "end" },
						{ "data": "duration" },
						{ "data": "amount_wagered" },
						{ "data": "amount_won" },
						{ "data": "promo_wagered" },
						{ "data": "promo_received" },
						{ "data": "funds_incomplete" },
						{ "data": "status" }
					],
					"order": [[0, 'asc']],
					"ajax": "/admin/ajax/datatables/customer_sessions.php?filter_date_from="+start_date+"&filter_date_to="+end_date+"&customer_id="+$("#customer").val()
				});
				
				data_table.draw(true);
			});
			
			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "customer" },
						{ "data": "session_id" },
						{ "data": "start" },
						{ "data": "end" },
						{ "data": "duration" },
						{ "data": "amount_wagered" },
						{ "data": "amount_won" },
						{ "data": "promo_wagered" },
						{ "data": "promo_received" },
						{ "data": "funds_incomplete" },
						{ "data": "status" }
					],
					"order": [[0, 'asc']],
					"ajax": "/admin/ajax/datatables/customer_sessions.php?filter_date_from="+start_date+"&filter_date_to="+end_date+"&customer_id="+$("#customer").val()
				});
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "customer" },
						{ "data": "session_id" },
						{ "data": "start" },
						{ "data": "end" },
						{ "data": "duration" },
						{ "data": "amount_wagered" },
						{ "data": "amount_won" },
						{ "data": "promo_wagered" },
						{ "data": "promo_received" },
						{ "data": "funds_incomplete" },
						{ "data": "status" }
					],
					"order": [[0, 'asc']],
					"ajax": "/admin/ajax/datatables/customer_sessions.php?filter_date_from="+start_date+"&filter_date_to="+end_date+"&customer_id="+$("#customer").val()
				});
				
				data_table.draw(true);
			});
			
		<!-- Geolocations Modal Start -->
			// temporary geolocation datatable initialization
			// definition redefined on button click
			var geo_data_table = $('#geolocation_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				//bInfo: false,
				"ajax": "/admin/ajax/datatables/geolocation.php",
				"columns": [
					{ "data": "user_session_id" },
					{ "data": "date_time" },
					{ "data": "city" },
					{ "data": "region" },
					{ "data": "country_name" },
					{ "data": "zipcode" },
					{ "data": "latitude" },
					{ "data": "longitude" }
				]
			});
			
			$('#geolocation_modal').on('shown.bs.modal', function() {
				//recalculate the dimensions of datatable
				geo_data_table.columns.adjust().responsive.recalc();
			});
			
			$('#geolocation_btn').click(function(){
				var columns = data_table.row('.selected').data();
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// load geolocation datatable
				if($.fn.dataTable.isDataTable('#geolocation_datatable')){
					geo_data_table.destroy();
				}
				
				// set modal title
				$('#geolocation_modal_label').html("Geolocation Information");
				
				// geolocation datatable initialization
				geo_data_table = $('#geolocation_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					//bInfo: false,
					"ajax": "/admin/ajax/datatables/geolocation.php?user_session_id="+columns['session_id'],
					"columns": [
						{ "data": "user_session_id" },
						{ "data": "date_time" },
						{ "data": "city" },
						{ "data": "region" },
						{ "data": "country_name" },
						{ "data": "zipcode" },
						{ "data": "latitude" },
						{ "data": "longitude" }
					],
					"order": [[0, 'desc']]
				});
				
				geo_data_table.draw(true);
				
				// show modal
				$('#geolocation_modal').modal('show');
			});
		<!-- geolocation Modal End -->
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Customer Sessions</h3>
					<div class="btn-group pull-right">
						<a id="geolocation_btn" class="btn btn-default btn-sm btn-row-action">Geolocation Info</a>
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div class="form-group">
							<label for="customer" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Customer</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="customer">
									<option value="-1">All Customers</option>
									<option value="-2">Loading... Please Wait.</option>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Customer</th>
								<th class="text-left">Session Id</th>
								<th class="text-left">Start Date</th>
								<th class="text-left">End Date</th>
								<th class="text-left">Duration</th>
								<th class="text-left">Amount Wagered</th>
								<th class="text-left">Amount Won</th>
								<th class="text-left">Bonus Wagered</th>
								<th class="text-left">Bonus Receieved</th>
								<th class="text-left">Pending Games (RB&Lotto)</th>
								<th class="text-left">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal for Geolocation -->
	<div class="modal fade" id="geolocation_modal" tabindex="-1" role="dialog" aria-labelledby="geolocation_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="geolocation_modal_label">Geolocation Information</h4>

					<div id="trans_modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div class="col-sm-12">
						<table id="geolocation_datatable" class="display table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Session Id</th>
									<th class="text-left">Date</th>
									<th class="text-left">City</th>
									<th class="text-left">Region</th>
									<th class="text-left">Country</th>
									<th class="text-left">Zipcode</th>
									<th class="text-left">Latitude</th>
									<th class="text-left">Longitude</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="6" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
