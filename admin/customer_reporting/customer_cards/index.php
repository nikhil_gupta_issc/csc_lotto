<?php

	$page_title = "Customer Cards";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	
?>
	<script type="text/javascript">
		$(document).ready(function(){
	
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			var forced_filter = "";
			
			// main datatable initialization
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/customer_cards.php",
				"columns": [
					{ "data": "customer_id" },
					{ "data": "customer_name" },					
					{ "data": "customer_number" },
					{ "data": "card_number" },
					
				],
				"order": [[1, 'asc']]

			});		
		});
	</script>
	<!-- The popup for upload new photo -->
   
	<div class="container-fluid">
		<div id="loading-spinner"><div></div></div>
	
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
			
				<div class="clear"></div>
				<div class="panel-body">
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">ID</th>
								<th class="text-left">Customer Name</th>
								<th class="text-left">Customer Number</th>
								<th class="text-left">Card number</th>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
        <br /><br />
				</div>
			</div>
		</div>
	</div>
	
	
	
	

	
	
	
	
	
	
	
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
