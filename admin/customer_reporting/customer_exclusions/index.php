<?php
	$page_title = "Customer Exclusions";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));
			
			// populate customer select box
			$.ajax({
				url: "/admin/ajax/get_customers.php",
				type: "POST",
				dataType: "json",
				success: function(data){
					$("#customer").html(data.options);
					$("#customer").val('<?php echo isset($_GET['customer']) ? $_GET['customer'] : -1; ?>');
				}
			});
			
			$(function () {
				// initialize datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
			});
		
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "customer" },
					{ "data": "exclusion_type" },
					{ "data": "excluded_on" },
					{ "data": "exclusion_end" },
					{ "data": "exclusion_count" }
				],
				"order": [[0, 'asc']],
				"ajax": "/admin/ajax/datatables/customer_exclusions.php?filter_date_from=<?php echo date('Y-m-d', time()); ?>"
			});
			
			$("#customer").on('change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "customer" },
						{ "data": "exclusion_type" },
						{ "data": "excluded_on" },
						{ "data": "exclusion_end" },
						{ "data": "exclusion_count" }
					],
					"order": [[0, 'asc']],
					"ajax": "/admin/ajax/datatables/customer_exclusions.php?filter_date_from="+start_date+"&filter_date_to="+end_date+"&customer_id="+$("#customer").val()
				});
				
				data_table.draw(true);
			});
			
			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "customer" },
						{ "data": "exclusion_type" },
						{ "data": "excluded_on" },
						{ "data": "exclusion_end" },
						{ "data": "exclusion_count" }
					],
					"order": [[0, 'asc']],
					"ajax": "/admin/ajax/datatables/customer_exclusions.php?filter_date_from="+start_date+"&filter_date_to="+end_date+"&customer_id="+$("#customer").val()
				});
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "customer" },
						{ "data": "exclusion_type" },
						{ "data": "excluded_on" },
						{ "data": "exclusion_end" },
						{ "data": "exclusion_count" }
					],
					"order": [[0, 'asc']],
					"ajax": "/admin/ajax/datatables/customer_exclusions.php?filter_date_from="+start_date+"&filter_date_to="+end_date+"&customer_id="+$("#customer").val()
				});
				
				data_table.draw(true);
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Customer Exclusions</h3>
					<div class="btn-group pull-right">
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div class="form-group">
							<label for="customer" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Customer</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="customer">
									<option value="-1">All Customers</option>
									<option value="-2">Loading... Please Wait.</option>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Customer</th>
								<th class="text-left">Exclusion Type</th>
								<th class="text-left">Excluded On</th>
								<th class="text-left">Exclusion End</th>
								<th class="text-left">Total Exclusion Count</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 