<?php
	$page_title = "Customer Lotto Sales";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));
			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			// populate customer select box
			$.ajax({
				url: "/admin/ajax/get_customers.php",
				type: "POST",
				dataType: "json",
				success: function(data){
					$("#customer").html(data.options);
					$("#customer").val('<?php echo isset($_GET['customer']) ? $_GET['customer'] : -1; ?>');
				}
			});
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "purchase_date" },
					{ "data": "ticket_number" },
					{ "data": "cashier" },
					{ "data": "location" },
					{ "data": "customer" },
					{ "data": "total" }
				],
				"order": [[0, 'asc']],
				"ajax": "/admin/ajax/datatables/customer_lotto_sales.php?customer_id="+$('#customer').val()+"&filter_date_from=<?php echo date('Y-m-d', time()); ?>"
			});

			$.ajax({
				url: "/admin/ajax/datatables/customer_lotto_sales_buttons.php",
				type: "POST",
				data: {
					action : "overall_total",
					customer_id : $('#customer').val(),
					filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
					filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
				},
				dataType: "json",
				success: function(response){
					if(response['result'] != false){
						// set overall total
						$("#total").text("$" + response['result']['overall_total']);
					}else{
						alert("Operation Failed!\n"+response['query']);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});

			$(document).on('change', '#customer', function(){
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "purchase_date" },
						{ "data": "ticket_number" },
						{ "data": "cashier" },
						{ "data": "location" },
						{ "data": "customer" },
						{ "data": "total" }
					],
					"order": [[0, 'asc']],
					"ajax": "/admin/ajax/datatables/customer_lotto_sales.php?customer_id="+$('#customer').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);

				//Calculate overall total
				$.ajax({
					url: "/admin/ajax/datatables/customer_lotto_sales_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						customer_id : $('#customer').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// set overall total
							$("#total").text("$" + response['result']['overall_total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "purchase_date" },
						{ "data": "ticket_number" },
						{ "data": "cashier" },
						{ "data": "location" },
						{ "data": "customer" },
						{ "data": "total" }
					],
					"order": [[0, 'asc']],
					"ajax": "/admin/ajax/datatables/customer_lotto_sales.php?customer_id="+$('#customer').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);

				//Calculate overall total
				$.ajax({
					url: "/admin/ajax/datatables/customer_lotto_sales_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						customer_id : $('#customer').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// set overall total
							$("#total").text("$" + response['result']['overall_total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "purchase_date" },
						{ "data": "ticket_number" },
						{ "data": "cashier" },
						{ "data": "location" },
						{ "data": "customer" },
						{ "data": "total" }
					],
					"order": [[0, 'asc']],
					"ajax": "/admin/ajax/datatables/customer_lotto_sales.php?customer_id="+$('#customer').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);

				//Calculate overall total
				$.ajax({
					url: "/admin/ajax/datatables/customer_lotto_sales_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						customer_id : $('#customer').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// set overall total
							$("#total").text("$" + response['result']['overall_total']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Customer Lotto Sales</h3>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="customer" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Customer</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="customer">
									<option value="-1">All Customers</option>
									<option value="-2">Loading... Please Wait.</option>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Purchase Date</th>
								<th class="text-left">Ticket Number</th>
								<th class="text-left">Cashier</th>
								<th class="text-left">Location</th>
								<th class="text-left">User</th>
								<th class="text-left">Total</th> 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					<div class="pull-right">
						<table id="totals" class="table table-striped" cellspacing="0">
							<thead>
								<tr>
									<th>Overall Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="total"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="view_details_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Ticket Details</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<table id="bet_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Game</th>
									<th class="text-left">Ball String</th>
									<th class="text-left">S/B</th>
									<th class="text-left">Bet Amount</th>
									<th class="text-left">Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="6" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	
<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
			</div>
			<div class="modal-body">
				Are you sure you would like to remove this house?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="processed_bets_modal" tabindex="-1" role="dialog" aria-labelledby="processed_bets_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="processed_bets_modal_label">Processed Bets</h4>
			</div>
			<div class="modal-body">
				This ticket already has processed bets. For security and accounting purposes, you cannot void this ticket.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 