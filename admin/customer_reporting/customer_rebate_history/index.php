<?php

	$page_title = "Customer Rebate History";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	
?>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));
			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			var forced_filter = "";
			
			// main datatable initialization
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/customer_rebate_history.php<?php echo $start_date; echo $end_date; ?>",
				"columns": [					
						{ "data": "period" },					
						{ "data": "date_sent" },
						{ "data": "casino_chetu" },
						//{ "data": "casino_multislot" },
						{ "data": "lotto" },
						{ "data": "rapidball" },
						{ "data": "sports" },					
						{ "data": "total" }	
					
					
				],
				"order": [[1, 'asc']]

			});
			
			
			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "period" },					
						{ "data": "date_sent" },
						{ "data": "casino_chetu" },
						//{ "data": "casino_multislot" },
						{ "data": "lotto" },
						{ "data": "rapidball" },
						{ "data": "sports" },					
						{ "data": "total" }				
					
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/customer_rebate_history.php?filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "period" },					
						{ "data": "date_sent" },
						{ "data": "casino_chetu" },
						//{ "data": "casino_multislot" },
						{ "data": "lotto" },
						{ "data": "rapidball" },
						{ "data": "sports" },					
						{ "data": "total" }	
					
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/customer_rebate_history.php?filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});		
		});
	</script>
	<!-- The popup for upload new photo -->
   
	<div class="container-fluid">	
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Customer Rebate History</h3>
				</div>
				
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>	
								<th class="text-left">Period</th>
								<th class="text-left">Date Sent</th>
								<th class="text-left">Casino</th>
								<!--<th class="text-left">Casino (Multislot)</th>-->
								<th class="text-left">Lotto</th>
								<th class="text-left">Rapidball</th>					
								<th class="text-left">Sports</th>					
								<th class="text-left">Amount</th>					
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
        <br /><br />
				</div>
			</div>
		</div>
	</div>
	
	
	
	

	
	
	
	
	
	
	
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
