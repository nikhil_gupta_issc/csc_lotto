<?php
	date_default_timezone_set('America/New_York');

	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php'); // for testing

	require_once($_SERVER['DOCUMENT_ROOT'].'/lib/assets/mpdf/mpdf.php');
	
	class RECEIPTS{
		private static $initialized = false;
		private static $auto_print = false;

		function RECEIPTS(){
			if(RECEIPTS::$initialized === false){
				RECEIPTS::$initialized = true;
			}
		}
		
		public function print_shift_report($shift_id){ 
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name` FROM `panel_user_shift` AS `s` LEFT JOIN `users` AS `u` ON `u`.`id` = `s`.`user_id` LEFT JOIN `panel_location` AS `l` ON `l`.`id` = `s`.`location_id` WHERE `s`.`id`=%d";
			$params = array($shift_id);
			$shift_info = $db->queryOneRow($q, $params);
			
			$q = "SELECT SUM(CASE WHEN `type_id`=7 THEN `amount` END) AS `lotto_sales`, COUNT(CASE WHEN `type_id`=7 THEN `amount` END) AS `lotto_sales_count`, SUM(CASE WHEN `type_id`=10 THEN `amount` END) AS `withdrawals`, COUNT(CASE WHEN `type_id`=10 THEN `amount` END) AS `withdrawals_count`, SUM(CASE WHEN `type_id`=9 THEN `amount` END) AS `deposits`, COUNT(CASE WHEN `type_id`=9 THEN `amount` END) AS `deposits_count`, SUM(CASE WHEN `type_id`=8 THEN `amount` END) AS `voids`, COUNT(CASE WHEN `type_id`=8 THEN `amount` END) AS `voids_count`, SUM(CASE WHEN `type_id`=12 THEN `amount` END) AS `payouts`, COUNT(CASE WHEN `type_id`=12 THEN `amount` END) AS `payouts_count`, SUM(CASE WHEN `type_id`=31 THEN `amount` END) AS `transfer_out`, COUNT(CASE WHEN `type_id`=31 THEN `amount` END) AS `transfer_out_count`, SUM(CASE WHEN `type_id`=30 THEN `amount` END) AS `transfer_in`, COUNT(CASE WHEN `type_id`=30 THEN `amount` END) AS `transfer_in_count` FROM `panel_user_transaction` WHERE `shift_id`=%i";
			$transaction_info = $db->queryOneRow($q, array($shift_id));
			
			//print_r($shift_info);

			// [sales] => 274.00 [payouts] => -1000.00 [collected_by] => 0 [collected_on] => 0000-00-00 00:00:00 [transfer_ins] => 0.00 [transfer_outs] => 0.00
			$shift_id = str_pad($shift_id, 8, "0", STR_PAD_LEFT);
			$shift_start_dt = new DateTime($shift_info['shift_start']);
			$shift_start = $shift_start_dt->format('M d, Y h:i A');
			$shift_end_dt = new DateTime($shift_info['shift_end']);
			$shift_end = $shift_end_dt->format('M d, Y h:i A');
			$cashier = $shift_info['user_id'].' - '.$shift_info['firstname'].' '.$shift_info['lastname'];
			$location = $shift_info['location_id'].' - '.$shift_info['location_name'];
			
			$html = "				
				<div id='ticket_header' style='padding-top: 15px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$shift_id."' type='I25+' class='barcode' />
					</div>
					
					<h2>Z Report</h2>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Shift:</b>
							</td>
							<td>
								".$shift_id."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Shift Start:</b>
							</td>
							<td>
								".$shift_start."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Shift End:</b>
							</td>
							<td>
								".$shift_end."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Cashier:</b>
							</td>
							<td>
								".$cashier."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Location:</b>
							</td>
							<td>
								".$location."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<thead>
							<tr>
								<th style='width: 50%; text-align: left; border-bottom: 1px solid #333; font-weight: bold;'>
									Trx
								</th>
								<th style='width: 20%; text-align: center; border-bottom: 1px solid #333; font-weight: bold;'>
									# of Trx
								</th>
								<th style='width: 30%; text-align: center; border-bottom: 1px solid #333; font-weight: bold;'>
									Trx $
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Customer Deposit
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['deposits_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['deposits'])." ".CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Customer Withdrawal
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['withdrawals_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['withdrawals'])." ".CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Lotto Win Payout
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['payouts_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['payouts'])." ".CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Lotto Sale
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['lotto_sales_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['lotto_sales'])." ".CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Lotto Void
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['voids_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['voids'])." ".CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Transfer In
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['transfer_in_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['transfer_in'])." ".CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Transfer Out
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['transfer_out_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['transfer_out'])." ".CURRENCY_FORMAT."
								</td>
							</tr>
						</tbody>
					</table>
					
					<!-- Tally Sheet -->
					<h3 style='margin-bottom: 5px;'>Tally Sheet</h3>
					<table cellspacing='0' border='0' style='width: 65%'>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>Coins</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								
							</td>
							<td style='width: 40%; text-align: right;'>
								$".$shift_info['closing_coins']."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$1</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_1s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_1s'])." ".CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$5</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_5s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_5s']*5)." ".CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$10</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_10s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_10s']*10)." ".CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$20</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_20s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_20s']*20)." ".CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$50</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_50s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_50s']*50)." ".CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$100</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_100s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_100s']*100)." ".CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>Total</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing'])." ".CURRENCY_FORMAT."
							</td>
						</tr>
					</table>
					
					<hr>
					
					<!-- Summary -->
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Float:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['float_amount']."
							</td>
						</tr>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Opening Amount:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['opening']."
							</td>
						</tr>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Closing Amount:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['closing']."
							</td>
						</tr>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Expected Amount:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['expected']."
							</td>
						</tr>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Over / Short:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['over_short']."
							</td>
						</tr>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Deposit Bag:
							</td>
							<td style='width: 50%; text-align: right;'>
								".$shift_info['bag_number']."
							</td>
						</tr>
					</table>
					
					<!-- Signature -->
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 25%; text-align: left; font-weight: bold;'>
								Signature:
							</td>
							<td style='width: 75%; text-align: right;'>
								<br>
								<hr>
							</td>
						</tr>
					</table>
				</div>
			";
			
			//echo $html;
			
			//$this->create_pdf($html, '/srv/www/receipts/', 'test');
			$this->create_pdf($html);
		}
		
		public function print_ticket($ticket_number, $file_output = false){			
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name` FROM `lotto_ticket` AS `t` LEFT JOIN `users` AS `u` ON `u`.`id` = `t`.`cashier_id` LEFT JOIN `panel_location` AS `l` ON `l`.`id` = `t`.`location_id` WHERE `t`.`ticket_number`=%s";
			$params = array($ticket_number);
			$ticket_details = $db->queryOneRow($q, $params);
		
			$purchase_date_dt = new DateTime($ticket_details['purchase_date']);
			$purchase_date = $purchase_date_dt->format('M d, Y h:i A');
			$expire_date_dt = new DateTime($ticket_details['expiration_date']);
			$expire_date = $ticket_details['expiration_date'] > 0 ? $expire_date_dt->format('F d, Y @ h:i A') : "None";
			$cashier = $ticket_details['cashier_id'].' - '.$ticket_details['firstname'].' '.$ticket_details['lastname'];
			$location = $ticket_details['location_id'].' - '.$ticket_details['location_name'];
			
			// get receipt footer message from db
			$q = "SELECT * FROM `receipt_messages` WHERE `id`=%d";
			$params = array(1);
			$receipt_message_info = $db->queryOneRow($q, $params);
			$message = nl2br($receipt_message_info['message']);
			
			// get bets from this ticket
			$q = "SELECT * FROM `lotto_bet` AS `b` LEFT JOIN `lotto_game` AS `g` ON `g`.`id` = `b`.`game_id` LEFT JOIN `lotto_house` AS `h` ON `h`.`id` = `g`.`house_id`WHERE `ticket_id`=%d";
			$params = array($ticket_details['ticket_id']);
			$bets = $db->query($q, $params);
			
			$html = "				
				<div id='ticket_header' style='padding-top: 15px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$ticket_number."' type='I25+' class='barcode' />
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 25%;'>
								<b>Date:</b>
							</td>
							<td>
								".$purchase_date."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;'>
								<b>Cashier:</b>
							</td>
							<td>
								".$cashier."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;'>
								<b>Location:</b>
							</td>
							<td>
								".$location."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;';>
								<b>Ticket:</b>
							</td>
							<td>
								".$ticket_number."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<thead>
							<tr>
								<th style='width: 25%; text-align: left; border-bottom: 1px solid #333; font-weight: bold;'>
									Pick
								</th>
								<th style='width: 25%; text-align: left; border-bottom: 1px solid #333; font-weight: bold;'>
									Game
								</th>
								<th style='width: 25%; text-align: center; border-bottom: 1px solid #333; font-weight: bold;'>
									S/B
								</th>
								<th style='width: 25%; text-align: center; border-bottom: 1px solid #333; font-weight: bold;'>
									$
								</th>
							</tr>
						</thead>
						<tbody>";

			foreach($bets as $bet){
				$game_name = $bet['short_name'];
				$s_b = $bet['is_boxed'] == 1 ? "B" : "S";
				$pick = $bet['ball_string'];
				$amount = $bet['bet_amount'];

				$html .= "
							<tr>
								<td style='width: 25%; text-align: left;'>
									".$pick."
								</td>
								<td style='width: 25%; text-align: left;'>
									".$game_name."
								</td>
								<td style='width: 25%; text-align: center;'>
									".$s_b."
								</td>
								<td style='width: 25%; text-align: right;'>
									$".$amount."
								</td>
							</tr>
				";
			}
			
			$html .= "
						</tbody>
						<tfoot>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									# of Items:
								</td>
								<td colspan='2' style='width: 25%; text-align: right;'>
									".$ticket_details['number_of_bets']."
								</td>
							</tr>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									Total:
								</td>
								<td colspan='2' style='width: 25%; text-align: right;'>
									$".$ticket_details['total']."
								</td>
							</tr>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									Free Money:
								</td>
								<td colspan='2' style='width: 25%; text-align: right;'>
									$0.00
								</td>
							</tr>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									Tender:
								</td>
								<td colspan='2' style='width: 25%; text-align: right;'>
									$".$ticket_details['tender']."
								</td>
							</tr>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									Change:
								</td>
								<td colspan='2' style='width: 25%; text-align: right;'>
									$".$ticket_details['change']."
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					<div id='message_standard' style='padding-top: 12px;'>
						".CURRENCY_FORMAT."Lotto.com will only honor the contents of this receipt.  Please check your receipt before leaving location.
					</div>
					
					<div id='ticket_expiration' style='padding-top: 12px; text-align: center; font-weight: bold;'>
						Ticket Expiration: ".$expire_date."
					</div>
					
					<div id='message_custom' style='padding-top: 12px;'>
						".$message."
					</div>
					
					<div id='qr_code' style='padding-top: 12px; text-align: center; font-weight: bold;'>
						Scan the QR Code to redeem online:<br><br>
						<barcode code='".ROOTPATH."/my_account.php?redeem_ticket=".$ticket_number."' type='QR' class='barcode' size='0.8' error='M' />
					</div>
				</div>
			";
			
			//echo $html;
			if($file_output == true){
				$this->create_pdf($html, '/srv/www/receipts/', 'test');
				echo "<a href='/srv/www/receipts/test.pdf'>PDF File</a>";
			}else{
				$this->create_pdf($html);
			}
		}

		public function print_ticket_by_id($ticket_id){ 
			global $db;
			
			$q = "SELECT * FROM `lotto_ticket` WHERE `ticket_id`=%d";
			$params = array($ticket_id);
			$ticket_details = $db->queryOneRow($q, $params);
			$this->print_ticket($ticket_details['ticket_number']);
		}
		public function create_pdf($html_content, $folder = '', $filename = '', $add_timestamp = false){
			// 3 inch wide = 76.2 mm
			try{
				// create a temp pdf in memory to get it's height
				$test_mpdf = new mPDF(
					'c',   				// mode - default ''
					array(76.2, 10000),	// format - A4, for example, default '' -L for landscape
					0,     				// font size - default 0
					'',    				// default font family
					2,    				// margin_left
					2,    				// margin right
					2,    				// margin top
					2,    				// margin bottom
					0,    				// margin header
					0,     				// margin footer
					'P'
				);  					// L - landscape, P - portrait
				
				$test_mpdf->WriteHTML($html_content, 2);
				
				// set height of final receipt using temp receipt height
				$mpdf = new mPDF(
					'c',   				// mode - default ''
					array(76.2, $test_mpdf->y+10),		// format - A4, for example, default '' -L for landscape
					0,     				// font size - default 0
					'',    				// default font family
					2,    				// margin_left
					2,    				// margin right
					2,    				// margin top
					2,    				// margin bottom
					0,    				// margin header
					0,     				// margin footer
					'P'
				); 
				
				if(RECEIPTS::$auto_print == true){
					$javascript = 'this.print();';
					$mpdf->SetJS($javascript);
				}
				
				//$mpdf->showImageErrors = true;
				//$mpdf->debug = true;
				
				//$stylesheet = file_get_contents('global.css');
				//$mpdf->WriteHTML($stylesheet,1);
				
				$mpdf->WriteHTML($html_content, 2);
				
				if($filename != ''){
					// build file path
					if($add_timestamp == true){
						$filename .= '_' . time();
					}
					$filename .= '.pdf';
					$folder = rtrim($folder, DIRECTORY_SEPARATOR);
					$filepath = $folder . DIRECTORY_SEPARATOR . $filename;
					
					$mpdf->Output($filepath, 'F');
				}else{
					$mpdf->Output();
				}
			}catch(Exception $e){
				echo $e;
			}
		}
	}
	
	$receipts = new RECEIPTS();
