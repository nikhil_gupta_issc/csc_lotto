<?php
$page_title = "Profitability";
include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
$start_date = isset($_GET['filter_date_from']) ? "?filter_date_from=".$_GET['filter_date_from'] : "?filter_date_from=".date("Y-m-d", time());
$end_date = isset($_GET['filter_date_to']) ? "&filter_date_to=".$_GET['filter_date_to'] : "&filter_date_to=".date("Y-m-d", time());
$location_id = "&location_id=".$_GET['location_id'];
$_GET['filter_date_from'] = isset($_GET['filter_date_from']) ? $_GET['filter_date_from'] : date("Y-m-d", time());
$_GET['filter_date_to'] = isset($_GET['filter_date_to']) ? $_GET['filter_date_to'] : date("Y-m-d", time());
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', strtotime($_GET['filter_date_from'])); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', strtotime($_GET['filter_date_to'])); ?>"));
			$('#location').val('<?php echo $_GET["location_id"]; ?>');

			// main house level datatable initialization
			var data_table = $('#datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/profitability.php<?php echo $start_date; echo $end_date; echo $location_id; ?>",
				"columns": [
					{ "data": "location" },
					{ "data": "lotto_sold" },
					{ "data": "withdrawals" },
					{ "data": "deposits" },
					{ "data": "voids" },
					{ "data": "lotto_payouts" },
					{ "data": "net" }
				],
				"order": [[1, 'asc']]
			});
			
			$(document).on('change', '#location', function(e){
				window.location.href = 'index.php<?php echo $start_date; echo $end_date; ?>&location_id='+$("#location").val();
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
				window.location.href = 'index.php?filter_date_from='+$(this).data("DateTimePicker").date().format("YYYY-MM-DD")+'<?php echo $end_date; echo $location_id; ?>';
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				window.location.href = 'index.php<?php echo $start_date; echo $location_id; ?>&filter_date_to='+$(this).data("DateTimePicker").date().format("YYYY-MM-DD");
			});
		});
	</script>
	
	<style>
		td.details-control {
			background: url('/lib/assets/datatables/media/images/details_open.png') no-repeat center center;
			cursor: pointer;
		}
		tr.shown td.details-control {
			background: url('/lib/assets/datatables/media/images/details_close.png') no-repeat center center;
		}
		.child_table{
			padding-left: 50px;
		}
	</style>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
					<div class="btn-group pull-right">
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="location" class="control-label col-sm-1">Location</label>
							<div class="col-sm-3">
								<select type="text" class="form-control" id="location">
									<option value="">All Locations</option>
									<?php
										$locations = $db->query("SELECT name, id FROM panel_location WHERE is_deleted=0 ORDER BY name");
										foreach($locations as $location){
											echo "<option value=".$location['id'].">".$location['name']."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
				
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Location</th>
								<th class="text-left">Lotto Sold</th>
								<th class="text-left">Withdrawals</th>
								<th class="text-left">Deposits</th>
								<th class="text-left">Voids</th>
								<th class="text-left">Payouts</th>
								<th class="text-left">Net</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					<?php
						if(!isset($_GET['filter_date_to'])){
							$date = new DateTime('now');
							$date_str = $date->format("Y-m-d");
						}else{
							$date_str = $_GET['filter_date_to'];
						}
						
						if($_GET['location_id'] != ""){
							$extra_where = " AND pl.id = ".$_GET['location_id'];
						}
						
						$q = "SELECT SUM(CASE WHEN put.type_id = 7 THEN put.amount END) AS lotto_sold, SUM(CASE WHEN put.type_id = 10 THEN put.amount END) AS withdrawals, SUM(CASE WHEN put.type_id = 9 THEN put.amount END) AS deposits, SUM(CASE WHEN put.type_id = 8 THEN put.amount END) AS voids, SUM(CASE WHEN put.type_id = 12 THEN put.amount END) AS payouts, SUM(put.amount) AS net FROM `panel_user_transaction` AS `put` JOIN `panel_location` AS `pl` ON `put`.`location_id`=`pl`.`id` WHERE `put`.`transaction_date` >= '".$_GET['filter_date_from']."' AND `put`.`transaction_date` <= '".$_GET['filter_date_to']." 23:59:59'".$extra_where;
						$profitability_totals = $db->queryOneRow($q);
					?>
					
					<div class="table-responsive">
						<table id="totals" class="table table-striped" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Total Lotto Sold</th>
									<th>Total Withdrawals</th>
									<th>Total Deposits</th>
									<th>Total Voids</th>
									<th>Total Payouts</th>
									<th>Total Net</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="lotto_sold"><?php echo "".number_format($profitability_totals['lotto_sold'])." ".CURRENCY_FORMAT; ?></td>
									<td id="withdrawals"><?php echo "".number_format($profitability_totals['withdrawals'])." ".CURRENCY_FORMAT; ?></td>
									<td id="deposits"><?php echo "".number_format($profitability_totals['deposits'])." ".CURRENCY_FORMAT; ?></td>
									<td id="voids"><?php echo "".number_format($profitability_totals['voids'])." ".CURRENCY_FORMAT; ?></td>
									<td id="payouts"><?php echo "".number_format($profitability_totals['payouts'])." ".CURRENCY_FORMAT; ?></td>
									<td id="net"><?php echo "".number_format($profitability_totals['net'])." ".CURRENCY_FORMAT; ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
