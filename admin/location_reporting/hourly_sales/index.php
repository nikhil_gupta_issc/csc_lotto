<?php
	$page_title = "Hourly Sales";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');

	$today = date("Y-m-d",time());
	$todays_running_totals = $core->get_daily_totals($today);
	$yesterdays_totals = $core->get_daily_totals(date("Y-m-d",strtotime("-1 day", time($today))));
?>
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Hourly Sales</h3>
				</div>
				<div class="panel-body clearfix" style="min-height: 500px;">
					<div class="col-sm-3 form-group">
						<label for="location">Location:</label>
						<select type="text" class="form-control" id="location">
							<option value="">All Locations</option>
							<?php
								$locations = $db->query("SELECT name, id FROM panel_location WHERE is_deleted=0 ORDER BY name");
								foreach($locations as $location){
									echo "<option value=".$location['id'].">".$location['name']."</option>";
								}
							?>
						</select>
					</div>
			
					<div class="col-sm-6"></div>
					
					<div class="col-sm-3 form-group pull-right">
						<label for="chart_date_select_div">Date:</label>
						<div id="chart_date_select_div">
							<div class="input-group date dp" id='chart_date_select'>
								<input type='text' class="form-control" placeholder="Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					</div>
					
					<div id="container" style="height: 400px; margin: 0 auto"></div>
					
					<script>
						$(document).ready(function(){
							// set initial date to today
							$('#chart_date_select').data("DateTimePicker").clear().date(moment());
							
							var chart_date;
							if($('#chart_date_select').data("DateTimePicker").date()){
								chart_date = $('#chart_date_select').data("DateTimePicker").date().format('YYYY-MM-DD');
							}
							
							$.ajax({
								url: '/admin/ajax/hourly_sales.php',
								type: 'POST',
								data: {
									date : chart_date,
									location_id: $('#location').val()
								},
								async: true,
								dataType: "json",
								success: function (data) {
									sales_vs_hits(data.categories, data.sales, data.number_of_sales);
								}
							});
							
							$("#chart_date_select").datetimepicker().on('dp.change', function (e) {
								if($('#chart_date_select').data("DateTimePicker").date()){
									chart_date = $('#chart_date_select').data("DateTimePicker").date().format('YYYY-MM-DD');
								}
								
								$.ajax({
									url: '/admin/ajax/hourly_sales.php',
									type: 'POST',
									data: {
										date : chart_date,
										location_id: $('#location').val()
									},
									async: true,
									dataType: "json",
									success: function (data) {
										sales_vs_hits(data.categories, data.sales, data.number_of_sales);
									}
								});
							});
							
							$(document).on("change", "#location", function(){
								if($('#chart_date_select').data("DateTimePicker").date()){
									chart_date = $('#chart_date_select').data("DateTimePicker").date().format('YYYY-MM-DD');
								}
								
								$.ajax({
									url: '/admin/ajax/hourly_sales.php',
									type: 'POST',
									data: {
										date : chart_date,
										location_id: $('#location').val()
									},
									async: true,
									dataType: "json",
									success: function (data) {
										sales_vs_hits(data.categories, data.sales, data.number_of_sales);
									}
								});
							});
						});
						
						function sales_vs_hits(cat, sales, number_of_sales) {
							$('#container').highcharts({
								chart: {
									renderTo: 'container',
									defaultSeriesType: 'column',
									zoomType: 'xy'
								},
								title: {
									text: 'Hourly Sales'
								},
								subtitle: {
									text: ''
								},
								xAxis: {
									categories: cat,
									max: 15
								},
								scrollbar: {
									enabled: true
								},
								yAxis: {
									min: 0,
									title: {
										text: 'Dollars'
									}
								},
								legend: {
									shadow: true
								},
								tooltip: {
									useHTML: true,
									headerFormat: '<small>{point.key}</small><table>',
									pointFormat: '<tr><td style="color: {series.color}">{series.name}:{point.y} </td>',
									footerFormat: '</table>',
									valueDecimals: 2,
									crosshairs: [{
										width: 1,
										color: 'Gray'},
									{
										width: 1,
										color: 'gray'}]
								},
								plotOptions: {
									column: {
										pointPadding: 0.2,
										borderWidth: 0.5
									}
								},
								series: [{
									name: 'Sales',
									data: sales
								},{
									name: 'Number of Sales',
									data: number_of_sales
								}],
								 scrollbar: {
									enabled:true,
									barBackgroundColor: 'gray',
									barBorderRadius: 7,
									barBorderWidth: 0,
									buttonBackgroundColor: 'gray',
									buttonBorderWidth: 0,
									buttonArrowColor: 'purple',
									buttonBorderRadius: 7,
									rifleColor: 'purple',
									trackBackgroundColor: 'white',
									trackBorderWidth: 1,
									trackBorderColor: 'silver',
									trackBorderRadius: 7
								}
							});
						}
					</script>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 