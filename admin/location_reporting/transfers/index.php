<?php
	$page_title = "Transfers";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));

			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "from_location" },
					{ "data": "to_location" },
					{ "data": "amount" },
					{ "data": "sent_by" },
					{ "data": "created_on" },
					{ "data": "accepted" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/transfers.php?location_id="+$('#location').val()+"&filter_date_from=<?php echo date('Y-m-d', time()); ?>"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){	
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
			

			$(document).on('change', '#location', function(e){
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "from_location" },
						{ "data": "to_location" },
						{ "data": "amount" },
						{ "data": "sent_by" },
						{ "data": "created_on" },
						{ "data": "accepted" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/transfers.php?location_id="+$('#location').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "from_location" },
						{ "data": "to_location" },
						{ "data": "amount" },
						{ "data": "sent_by" },
						{ "data": "created_on" },
						{ "data": "accepted" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/transfers.php?location_id="+$('#location').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "from_location" },
						{ "data": "to_location" },
						{ "data": "amount" },
						{ "data": "sent_by" },
						{ "data": "created_on" },
						{ "data": "accepted" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/transfers.php?location_id="+$('#cashier').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Transfers</h3>
					<div class="btn-group pull-right">
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<label for="start_date" class="control-label col-sm-1">From Date</label>
						<div class="form-group">
							<div style="padding-left: 5px;" class="input-group date dp col-sm-2" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<label for="end_date" class="control-label col-sm-1">To Date</label>
						<div class="form-group">
							<div style="padding-left: 5px;" class="input-group date dp col-sm-2" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="location" class="control-label col-sm-1">Location</label>
							<div class="col-sm-3">
								<select type="text" class="form-control" id="location">
									<option value="">All Locations</option>
									<?php
										$locations = $db->query("SELECT name, id FROM panel_location WHERE is_deleted=0 ORDER BY name");
										foreach($locations as $location){
											echo "<option value=".$location['id'].">".$location['name']."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">From</th>
								<th class="text-left">To</th>
								<th class="text-left">Amount</th>
								<th class="text-left">Sent By</th>
								<th class="text-left">Transfer Date</th>
								<th class="text-left">Accepted</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 