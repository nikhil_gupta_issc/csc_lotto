<?php
	ini_set('max_execution_time', 99999);
	set_time_limit(0);
	
	/*
	No cache!!
	*/
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");// always modified
	header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache"); // HTTP/1.0
	/*
	End of No cache
	*/

	// add this to convert line feeds from mac files so fgetcsv will work
	// otherwise CR wil not work, but CRLF and LF will
	ini_set('auto_detect_line_endings', true);
	
	include("header.php");
	
	function match_columns($column){
		global $db;
		
		$result = array();

		if($column == "iCustomerID"){
			$column = "customer_id";
			$table = "customers";
		}elseif($column == "sCustomerNo"){
			$column = "customer_number";
			$table = "customers";
		}elseif($column == "sUsername"){
			$column = "username";
			$table = "users";
		}elseif($column == "sPassword"){
			$column = "pin";
			$table = "customers";
		}elseif($column == "sFirstName"){
			$column = "firstname";
			$table = "users";
		}elseif($column == "sLastName"){
			$column = "lastname";
			$table = "users";
		}elseif($column == "sTel"){
			$column = "telephone";
			$table = "user_info";
		}elseif($column == "sCell"){
			$column = "cellphone";
			$table = "user_info";
		}elseif($column == "sAddress"){
			$column = "address";
			$table = "user_info";
		}elseif($column == "sCountry"){
			$column = "country";
			$table = "user_info";
		}elseif($column == "sIsland"){
			$column = "island_id";
			$table = "user_info";
		}elseif($column == "sEmail"){
			$column = "email";
			$table = "users";
		}elseif($column == "sGender"){
			$column = "gender";
			$table = "user_info";
		}elseif($column == "dtDOB"){
			$column = "date_of_birth";
			$table = "user_info";
		}elseif($column == "iIdentifierID"){
			$column = "";
			$table = "";
		}elseif($column == "sIdentifierNo"){
			$column = "";
			$table = "";
		}elseif($column == "bLocked"){
			$column = "locked";
			$table = "users";
		}elseif($column == "bChangePassword"){
			$column = "force_password_change";
			$table = "users";
		}elseif($column == "bDisabled"){
			$column = "is_disabled";
			$table = "customers";
		}elseif($column == "bIsVerified"){
			$column = "is_verified";
			$table = "customers";
		}elseif($column == "iVerifiedBy"){
			$column = "verified_by";
			$table = "customers";
		}elseif($column == "dtLastLogin"){
			$column = "last_login";
			$table = "users";
		}elseif($column == "sLastLoginIP"){
			$column = "ip";
			$table = "users";
		}elseif($column == "iCreatedBy"){
			$column = "created_by";
			$table = "customers";
		}elseif($column == "dtCreatedOn"){
			$column = "created_on";
			$table = "customers";
		}elseif($column == "iUpdatedBy"){
			$column = "updated_by";
			$table = "customers";
		}elseif($column == "dtUpdatedOn"){
			$column = "updated_on";
			$table = "customers";
		}
		
		$result['column'] = $column;
		$result['table'] = $table;
		
		return $result;
	}
?> 
	<script>
		$(document).ready(function() {
			$('INPUT[type="file"]').change(function () {
				var ext_arr = this.value.match(/\.([0-9a-z]+)(?:[\?#]|$)/);
				if(ext_arr){
					ext = ext_arr[1];
					switch (ext) {
						case 'csv':
						case 'txt':
						case 'text':
							$('#upload_btn').attr('disabled', false);
							break;
						default:
							alert('This is not an allowed file type.');
							this.value = '';
					}
				}else{
					$('#upload_btn').attr('disabled', false);
				}
			});
		});
	</script>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<div class="container">
		<div class="well clearfix">
			<fieldset>
				<legend><b>Select a CSV File to Upload:</b></legend>
				<div style="padding:10px; text-align: center;">
					<form action="" method="post" enctype="multipart/form-data">
						<input id="file_data" name="file_data" type="file" class="file" size="30" accept="text/comma-separated-values,.csv,text/csv,text/plain,application/csv,application/vnd.ms-excel">
						<input id="folder" name="folder" type="hidden" value="/srv/www/uploads/">
						<input id="upload_btn" class="btn btn-default" name="submit" type="submit" value="Upload" disabled="true">
						<br><br><i><b>Note:</b> Files must be <?php echo ini_get("upload_max_filesize"); ?> or smaller and should be plain text with comma separated values.  In addition, the first row of the file must contain column headers.</i>
					</form>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend><b>Preview Data:</b></legend>
				<form action="" method="post" enctype="multipart/form-data">
				<?php
					// Returns a file size limit in bytes based on the PHP upload_max_filesize
					// and post_max_size
					function file_upload_max_size() {
						static $max_size = -1;

						if ($max_size < 0) {
							// Start with post_max_size.
							$max_size = parse_size(ini_get('post_max_size'));

							// If upload_max_size is less, then reduce. Except if upload_max_size is
							// zero, which indicates no limit.
							$upload_max = parse_size(ini_get('upload_max_filesize'));
							if ($upload_max > 0 && $upload_max < $max_size) {
								$max_size = $upload_max;
							}
						}
						return $max_size;
					}

					function parse_size($size) {
						$unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
						$size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
						if ($unit) {
							// Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
							return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
						} else {
							return round($size);
						}
					}
				
					// Upload button was clicked
					if(isset($_POST["folder"])){	
						// Valid file extensions (csv txt text)
						$valid_ext_regex = "/^\.(csv|txt|text|tsv){1}$/i";
					
						if(is_uploaded_file($_FILES['file_data']['tmp_name'])){
							//  sanitize file name
							//     - remove extra spaces/convert to _,
							//     - remove non 0-9a-Z._- characters,
							//     - remove leading/trailing spaces
							//  check if under 10MB,
							//  check file extension for legal file types
							$org_name = $_FILES['file_data']['name'];
							$safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($_FILES['file_data']['name']));
							$target_file = $_POST['folder'] . time() . "-" . $safe_filename;
							if($_FILES['file_data']['size'] <= file_upload_max_size() && preg_match($valid_ext_regex, strrchr($safe_filename, '.'))){
								move_uploaded_file ($_FILES['file_data']['tmp_name'], $target_file);
							}
						}
						
						// get island info so we can lookup ids later
						$q = "SELECT * FROM `island`";
						$islands_info = $db->query($q);
						$islands = array();
						foreach($islands_info as $island){
							$islands[$island['id']] = $island['name'];
						}
						
						// Store data from csv file into an array
						$r = 0;
						$col_count = 99;
						$headers = array();
						$csv_data = array();
						$handle = fopen($target_file, "r");
						while(($data = fgetcsv($handle, 8000, ",")) !== FALSE) {
							$col_count = count($data);
							for($c=0; $c<$col_count; $c++){
								// get column headers
								if($r == 0){
									$headers[$c] = match_columns($data[$c]);
								}else{
									// get value
									$value = trim($data[$c]);
									
									// fix poor formatting
									if($headers[$c]['column'] == 'firstname'){
										$value = ucwords(strtolower($value));
									}
									if($headers[$c]['column'] == 'lastname'){
										$value = ucwords(strtolower($value));
									}
									if(strtolower($value) == 'true'){
										$value = 1;
									}elseif(strtolower($value) == 'false'){
										$value = 0;
									}
									
									// lookup island id 
									if($headers[$c]['column'] == 'island_id'){
										$value = array_search($value, $islands);
									}
									
									// see if key is a valid column name in db
									$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '".$headers[$c]['column']."' and TABLE_NAME = '".$headers[$c]['table']."'";
									$key_exists = $db->queryOneRow($q);
									if($key_exists['count'] > 0){
										$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
										$num_digits = $key_exists['NUMERIC_PRECISION'];
										$num_places = $key_exists['NUMERIC_SCALE'];
									
										// validate data against data type defined in database
										$data_type = $key_exists['DATA_TYPE'];
										switch ($data_type){
											case "tinyint":
												settype($value , "integer");
												break;
											case "mediumint":
												settype($value , "integer");
												break;
											case "smallint":
												settype($value , "integer");
												break;
											case "int":
												settype($value , "integer");
												break;
											case "decimal":
												settype($value , "float");
												break;
											case "float":
												settype($value , "float");
												break;
											case "double":
												settype($value , "float");
												break;
											case "real":
												settype($value , "float");
												break;
											case "bit":
												settype($value , "boolean");
												break;
											case "boolean":
												settype($value , "boolean");
												break;
											case "date":
												if(!$value){
													$value = "NULL";
												}else{
													$value = date('Y-m-d', strtotime($value));
												}
												break;
											case "datetime":
												if(!$value){
													$value = "NULL";
												}else{
													$value = date('Y-m-d H:i:s', strtotime($value));
												}					
												break;
											case "time":
												if(!$value){
													$value = "NULL";
												}else{
													$value = date('H:i:s', strtotime($value));
												}
												break;
											case "timestamp":
												settype($value , "integer");
												break;
											case "year":
												if(!$value){
													$value = "NULL";
												}
												break;
										}
										
										// get next autoincrement id from users table
										$user_id = $db->getNextID('users');
									}
									
									if($headers[$c]['table']){
										$csv_data[$r][$headers[$c]['table']][$headers[$c]['column']] = $value;
									}
								}
							}
							
							if($r > 0){
								// build username from first name and previous username
								$csv_data[$r]['users']['username'] = str_ireplace(" ", "", strtolower($csv_data[$r]['users']['firstname'].$csv_data[$r]['customers']['customer_number']));
								
								// add user id
								$csv_data[$r]['users']['id'] = $user_id;
								$csv_data[$r]['user_info']['user_id'] = $user_id;
								$csv_data[$r]['customers']['user_id'] = $user_id;
								
								// create entry in users table
								$token = $database->generateRandStr(16);
								$usersalt = $database->generateRandStr(8);	
								$time = time();
								$ulevel = REGUSER_LEVEL; /* No activation required */
								$password = sha1($usersalt.$csv_data[$r]['customers']['pin']);
								$q = "INSERT INTO `".TBL_USERS."` (`id`, `username`, `firstname`, `lastname`, `password`, `usersalt`, `userid`, `userlevel`, `email`, `timestamp`, `actkey`, `ip`, `regdate`, `force_password_change`, `locked`) VALUES ('$user_id', '".$csv_data[$r]['users']['username']."', '".$csv_data[$r]['users']['firstname']."', '".$csv_data[$r]['users']['lastname']."', '$password', '$usersalt', '0', '$ulevel', '".$csv_data[$r]['users']['email']."', '$time', '$token', '".$csv_data[$r]['users']['ip']."', '$time', '".$csv_data[$r]['users']['force_password_change']."', '".$csv_data[$r]['users']['locked']."');";
								//echo $q."<br><br>";
								$insert_id = $db->queryInsert($q);
								echo "Importing User ID# ".$insert_id."...<br>";
								
								$success = true;
								if($insert_id == $user_id){
									// user created successfully
									// create customer record
									$q = "INSERT INTO `customers` (`customer_id`, `user_id`, `pin`, `customer_number`, `card_number`, `is_disabled`, `disabled_by`, `needs_to_deposit`, `drivers_license`, `voters_card`, `passport`, `national_security`, `is_drivers_license_verified`, `is_voters_card_verified`, `is_passport_verified`, `is_national_security_verified`, `is_verified`, `verified_on`, `verified_by`, `available_balance`, `bonus_balance`, `created_by`, `created_on`, `updated_by`, `updated_on`, `show_inactive_games`, `profile_picture_path`, `loyalty_points`, `daily_loss_limit`, `is_inactive`) VALUES ('".$csv_data[$r]['customers']['customer_id']."', '".$user_id."', '".$csv_data[$r]['customers']['pin']."', '".$csv_data[$r]['customers']['customer_number']."', NULL, '".$csv_data[$r]['customers']['is_disabled']."', NULL, '1', NULL, NULL, NULL, NULL, '0', '0', '0', '0', '".$csv_data[$r]['customers']['is_verified']."', NULL, ".$csv_data[$r]['customers']['verified_by'].", '0.00', '0.00', '".$csv_data[$r]['customers']['created_by']."', '".$csv_data[$r]['customers']['created_on']."', NULL, NULL, '0', NULL, '0', NULL, '0');";
									//echo $q."<br><br>";
									$insert_id = $db->queryInsert($q);
									if($insert_id == $csv_data[$r]['customers']['customer_id']){
										$q = "INSERT INTO `user_info` (`user_id`, `telephone`, `cellphone`, `address`, `address2`, `country`, `city`, `island_id`, `gender`, `date_of_birth`, `nickname`, `hide_balance`) VALUES ('".$user_id."', '".$csv_data[$r]['user_info']['telephone']."', '".$csv_data[$r]['user_info']['cellphone']."', '".$csv_data[$r]['user_info']['address']."', '', 'Bahamas', NULL, '".$csv_data[$r]['user_info']['island_id']."', '".$csv_data[$r]['user_info']['gender']."', '".$csv_data[$r]['user_info']['date_of_birth']."', NULL, '0');";
										//echo $q."<br><br>";
										$id = $db->queryInsert($q);
										if($id == $user_id){
											$success = true;
										}else{
											$success = false;
										}
									}else{
										$success = false;
									}
								}else{
									$success = false;
								}
								
								echo "Success: $success<br><hr>";
							}
							$r++;
						}
						
						//echo "<pre>";
						//print_r($csv_data);
						//echo "</pre>";
						//die();
					}
				?>
			</fieldset>
			<input id="filename" name="filename" type="hidden" value="<?php echo $target_file; ?>">
			<input id="upload" name="upload" type="hidden" value="<?php echo $org_name; ?>">
			<br>
			<span style='float:right; margin-top:12px;'><input name='submit' class='btn btn-large btn-primary' type='submit' value='Submit'/></span>
			</form>
		</div>
	</div>
	
	<?php include 'footer.php'; ?>