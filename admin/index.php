<?php
	$page_title = "Administration Dashboard";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');

	$today = date("Y-m-d",time());
	$todays_running_totals = $core->get_daily_totals($today);
	$yesterdays_totals = $core->get_daily_totals(date("Y-m-d",strtotime("-1 day", time($today))));
?>
	<script>
		$(document).ready(function(){
			// variable to store index of currently selected child table
			var selected_child_index = -1;
			var selected_ticket_number = -1;
			var cashiers_child_table = [];
			var withdraw_deposit_child_table = [];
			
			var withdraw_deposit_data_table = $('#withdraw_deposit_datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/dashboard_withdraw_deposit.php",
				"columns": [
					{
						"className":      	'details-control',
						"orderable":      	false,
						"data":           	null,
						"defaultContent": 	'',
						"width":			'20px'
					},
					{ "data": "location" },
					{ "data": "net" }
				],
				"order": [[1, 'asc']]
			});
			
			// function to dynamically create game child datatables
			function get_withdraw_deposit_child_table(table_id){				
				$withdraw_deposit_child_table = '<div class="child_table table-responsive"><table id="withdraw_deposit_child_'+table_id+'" class="display table dt-child-table" cellspacing="0" width="100%">'+
					'<thead>'+
						'<tr>'+
							'<th>Date</th>'+
							'<th>Type</th>'+
							'<th>Customer</th>'+
							'<th>Cashier</th>'+
							'<th>Amount</th>'+
						'</tr>'+
					'</thead>'+
					'<tbody>'+
						'<tr>'+
							'<td colspan="4" class="dataTables_empty">Loading data from server</td>'+
						'</tr>'+
					'</tbody>'+
				'</table></div>';
			
				return $withdraw_deposit_child_table;
			}
			
			// Add event listener for opening and closing game child tables
			$(document).on('click', '#withdraw_deposit_datatable tbody td.details-control', function () {			
				var tr = $(this).closest('tr');
				var row = withdraw_deposit_data_table.row( tr );
				
				if(row.child.isShown()){
					// This row is already open - close it
					row.child.hide();
					tr.removeClass('shown');
				}else{
					var row_data = row.data();
					var location_id = row_data['location_id'];
					
					// is child has not been dynamically created, create it
					if (typeof row.child() == 'undefined'){
						// create this row
						row.child(get_withdraw_deposit_child_table(location_id)).show();
						
						// create datatable
						cashiers_child_table[location_id] = $('#withdraw_deposit_child_'+location_id).DataTable({
							"processing": true,
							"serverSide": true,
							"deferRender": true,
							"ajax": "/admin/ajax/datatables/dashboard_withdraw_deposit_child_table.php?location_id="+location_id,
							"columns": [
								{ "data": "transaction_date" },
								{ "data": "type" },
								{ "data": "customer" },
								{ "data": "cashier" },				
								{ "data": "amount" }
							],
							"order": [[0, 'asc']],
							"paging":   	false,
							"searching": 	false,
							"info":     	false,
							//"scrollY": "150px",
							"dom": "frtiS"
						});
					}else{
						// otherwise, show what was already rendered
						row.child.show();
					}
					
					tr.addClass('shown');
				}
			});
			
			var customers_data_table = $('#customers_datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				"bInfo": false,
				"ajax": "/admin/ajax/datatables/dashboard_customers.php",
				"columns": [
					{ "data": "customer" },
					{ "data": "island" },
					{ "data": "sales" },
					{ "data": "winnings" },	
					{ "data": "net" }
				],
				"order": [[1, 'asc']]
			});
			
			var cashiers_data_table = $('#cashiers_datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				"bInfo": false,
				"fnDrawCallback": function (){
					collapse_expand_cashier_rows();
				},
				"ajax": "/admin/ajax/datatables/dashboard_cashiers.php",
				"columns": [
					{
						"className":      	'details-control',
						"orderable":      	false,
						"data":           	null,
						"defaultContent": 	'',
						"width":			'20px'
					},
					{ "data": "island" },
					{ "data": "income" },
					{ "data": "expense" },
					{ "data": "commission" },				
					{ "data": "net" }
				],
				"order": [[1, 'asc']]
			});
			
			// function to dynamically create game child datatables
			function get_cashiers_child_table(table_id){				
				$cashiers_child_table = '<div class="child_table table-responsive"><table id="cashier_child_'+table_id+'" class="display table dt-child-table" cellspacing="0" width="100%">'+
					'<thead>'+
						'<tr>'+
							'<th>Cashier</th>'+
							'<th>Income</th>'+
							'<th>Expenses</th>'+
							'<th>Commission</th>'+
							'<th>Net</th>'+
						'</tr>'+
					'</thead>'+
					'<tbody>'+
						'<tr>'+
							'<td colspan="4" class="dataTables_empty">Loading data from server</td>'+
						'</tr>'+
					'</tbody>'+
				'</table></div>';
			
				return $cashiers_child_table;
			}
			
			// Add event listener for opening and closing game child tables
			$(document).on('click', '#cashiers_datatable tbody td.details-control', function () {			
				var tr = $(this).closest('tr');
				var row = cashiers_data_table.row( tr );
				
				if(row.child.isShown()){
					// This row is already open - close it
					row.child.hide();
					tr.removeClass('shown');
				}else{
					var row_data = row.data();
					var island_id = row_data['island_id'];
					
					// is child has not been dynamically created, create it
					if (typeof row.child() == 'undefined'){
						// create this row
						row.child(get_cashiers_child_table(island_id)).show();
						
						// create datatable
						cashiers_child_table[island_id] = $('#cashier_child_'+island_id).DataTable({
							"processing": true,
							"serverSide": true,
							"deferRender": true,
							"ajax": "/admin/ajax/datatables/dashboard_cashiers_child_table.php?island_id="+island_id,
							"columns": [
								{ "data": "cashier" },
								{ "data": "income" },
								{ "data": "expense" },
								{ "data": "commission" },				
								{ "data": "net" }
							],
							"order": [[0, 'asc']],
							"paging":   	false,
							"searching": 	false,
							"info":     	false,
							//"scrollY": "150px",
							"dom": "frtiS"
						});
					}else{
						// otherwise, show what was already rendered
						row.child.show();
					}
					
					tr.addClass('shown');
				}
			});
			
			var isCollapse = 1;
			$('#collapse_expand_cashier').on('click', function() {
				if(isCollapse === 1){
					$('#collapse_expand_cashier').html('<img src="/lib/assets/datatables/media/images/details_close.png"></span> <span> Collapse all</span>');
					isCollapse = 0; 
				}else{
					$('#collapse_expand_cashier').html('<img src="/lib/assets/datatables/media/images/details_open.png"></span> <span> Expand all</span>');
					isCollapse = 1;
				}
				collapse_expand_cashier_rows();
			});

			function collapse_expand_cashier_rows(){
				var table_length = $('#cashiers_datatable tbody tr').length;
				for (var i = 0; i < table_length; i++) {
					var tr = $('.details-control').parents('tr').eq(i);

					var row = cashiers_data_table.row( tr );
			 
					if ( isCollapse === 1 ) {
						if ( row.child.isShown() ) {
							// This row is already open - close it
							row.child.hide();
							tr.removeClass('shown');
						}
					}else{
						var row_data = row.data();
						var island_id = row_data['island_id'];
						
						// is child has not been dynamically created, create it
						if (typeof row.child() == 'undefined'){
							// create this row
							row.child(get_cashiers_child_table(island_id)).show();
							
							cashiers_child_table[island_id] = $('#cashier_child_'+island_id).DataTable({
							"processing": true,
							"serverSide": true,
							"deferRender": true,
							"ajax": "/admin/ajax/datatables/dashboard_cashiers_child_table.php?island_id="+island_id,
							"columns": [
								{ "data": "cashier" },
								{ "data": "income" },
								{ "data": "expense" },
								{ "data": "commission" },				
								{ "data": "net" }
							],
							"order": [[0, 'asc']],
							"paging":   	false,
							"searching": 	false,
							"info":     	false,
							//"scrollY": "150px",
							"dom": "frtiS"
						});
						}else{
							// otherwise, show what was already rendered
							row.child.show();
						}
						
						tr.addClass('shown');
					}
				}
			}
		});
	</script>

	<style>
		td.details-control {
			background: url('/lib/assets/datatables/media/images/details_open.png') no-repeat center center;
			cursor: pointer;
		}
		tr.shown td.details-control {
			background: url('/lib/assets/datatables/media/images/details_close.png') no-repeat center center;
		}
		.child_table{
			padding-left: 50px;
		}
	</style>
	
	<style>
		.panel-body-fixed{
			min-height: 470px;
		}
	</style>

	<div class="container-fluid">
		<?php include('side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
		
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Dashboard</h3>
					</div>
					<div class="panel-body panel-body-fixed">
						<table class="table table-striped">
							<thead>
								<tr>
									<th colspan="4">Cash Statistics</th>
								</tr>
								<tr>
									<th colspan="2">Today</th>
									<th colspan="2">Yesterday</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Sales</td>
									<td><?php echo "".number_format($todays_running_totals['sales'])." " . CURRENCY_FORMAT; ?></td>
									<td>Sales</td>
									<td><?php echo "".number_format($yesterdays_totals['sales'])." " . CURRENCY_FORMAT; ?></td>
								</tr>
								<tr>
									<td>Deposits</td>
									<td><?php echo "".number_format($todays_running_totals['deposits'])." " . CURRENCY_FORMAT; ?></td>
									<td>Deposits</td>
									<td><?php echo "".number_format($yesterdays_totals['deposits'])." " . CURRENCY_FORMAT; ?></td>
								</tr>
								<tr>
									<td>Withdrawals</td>
									<td><?php echo "".number_format($todays_running_totals['withdrawals'])." " . CURRENCY_FORMAT; ?></td>
									<td>Withdrawals</td>
									<td><?php echo "".number_format($yesterdays_totals['withdrawals'])." " . CURRENCY_FORMAT; ?></td>
								</tr>
								<tr>
									<td>Commission</td>
									<td><?php echo "".number_format($todays_running_totals['commission'])." " . CURRENCY_FORMAT; ?></td>
									<td>Commission</td>
									<td><?php echo "".number_format($yesterdays_totals['commission'])." " . CURRENCY_FORMAT; ?></td>
								</tr>
								<tr>
									<td>Voids</td>
									<td><?php echo "".number_format($todays_running_totals['voids'])." " . CURRENCY_FORMAT; ?></td>
									<td>Voids</td>
									<td><?php echo "".number_format($yesterdays_totals['voids'])." " . CURRENCY_FORMAT; ?></td>
								</tr>
								<tr>
									<td>Winnings</td>
									<td><?php echo "".number_format($todays_running_totals['winnings'])." " . CURRENCY_FORMAT; ?></td>
									<td>Winnings</td>
									<td><?php echo "".number_format($yesterdays_totals['winnings'])." " . CURRENCY_FORMAT; ?></td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td>Net</td>
									<td><?php echo "".number_format($todays_running_totals['net'])." " . CURRENCY_FORMAT; ?></td>
									<td>Net</td>
									<td><?php echo "".number_format($yesterdays_totals['net'])." " . CURRENCY_FORMAT; ?></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		
			<div class="col-sm-12 col-xl-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Lottery Sales vs. Hits By Game</h3>
					</div>
					<div class="panel-body panel-body-fixed">
						<div id="chart_date_select_div" style="top: -50px; text-align: center;">
							<div style="padding-left: 10px; padding-right: 10px; width: 180px;" class="input-group date dp" id='chart_date_select'>
								<input type='text' class="form-control" placeholder="Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
						
						<div id="container" style="height:400px; margin: 0 auto"></div>
						
						<script>
							$(document).ready(function(){
								// set initial date to today
								$('#chart_date_select').data("DateTimePicker").clear().date(moment());
								
								var chart_date;
								if($('#chart_date_select').data("DateTimePicker").date()){
									chart_date = $('#chart_date_select').data("DateTimePicker").date().format('YYYY-MM-DD');
								}
								
								$.ajax({
									url: '/admin/ajax/sales_vs_hits.php',
									type: 'POST',
									data: {
										date : chart_date
									},
									async: true,
									dataType: "json",
									success: function (data) {
										sales_vs_hits(data.categories, data.sales, data.hits);
									}
								});
								
								$("#chart_date_select").datetimepicker().on('dp.change', function (e) {
									if($('#chart_date_select').data("DateTimePicker").date()){
										chart_date = $('#chart_date_select').data("DateTimePicker").date().format('YYYY-MM-DD');
									}
									
									$.ajax({
										url: '/admin/ajax/sales_vs_hits.php',
										type: 'POST',
										data: {
											date : chart_date
										},
										async: true,
										dataType: "json",
										success: function (data) {
											sales_vs_hits(data.categories, data.sales, data.hits);
										}
									});
								});
							});
							
							function sales_vs_hits(cat, sales, hits) {
								$('#container').highcharts({
									chart: {
										renderTo: 'container',
										defaultSeriesType: 'column',
										zoomType: 'xy'
									},
									title: {
										text: 'Sales vs. Hits By Game'
									},
									subtitle: {
										text: ''
									},
									xAxis: {
										categories: cat,
										max: 15
									},
									scrollbar: {
										enabled: true
									},
									yAxis: {
										min: 0,
										title: {
											text: 'Dollars'
										}
									},
									legend: {
										shadow: true
									},
									tooltip: {
										useHTML: true,
										headerFormat: '<small>{point.key}</small><table>',
										pointFormat: '<tr><td style="color: {series.color}">{series.name}:{point.y} </td>',
										footerFormat: '</table>',
										valueDecimals: 2,
										crosshairs: [{
											width: 1,
											color: 'Gray'},
										{
											width: 1,
											color: 'gray'}]
									},
									plotOptions: {
										column: {
											pointPadding: 0.2,
											borderWidth: 0.5
										}
									},
									series: [{
										name: 'Sales',
										data: sales
									}, {
										name: 'Hits',
										data: hits
									}],
									 scrollbar: {
										enabled:true,
										barBackgroundColor: 'gray',
										barBorderRadius: 7,
										barBorderWidth: 0,
										buttonBackgroundColor: 'gray',
										buttonBorderWidth: 0,
										buttonArrowColor: 'purple',
										buttonBorderRadius: 7,
										rifleColor: 'purple',
										trackBackgroundColor: 'white',
										trackBorderWidth: 1,
										trackBorderColor: 'silver',
										trackBorderRadius: 7
									}
								});
							}
						</script>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12 col-xl-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Net by Day Per Game Type</h3>
					</div>
					<div class="panel-body panel-body-fixed">
						<div id="net_game_container" style="height:400px; margin: 0 auto"></div>
						
						<script>
							$(document).ready(function(){
								
								$.ajax({
									url: '/admin/ajax/net_game_type.php',
									type: 'POST',
									async: true,
									dataType: "json",
									success: function (data) {
										net_game_type(data.categories, data.lotto, data.casino, data.sports, data.rapidballs);
									}
								});
							});
							
							function net_game_type(cat, lotto, casino, sports, rapidballs) {
								$('#net_game_container').highcharts({
									chart: {
										renderTo: 'net_game_container',
										defaultSeriesType: 'line',
										zoomType: 'xy'
									},
									title: {
										text: 'Net by Day Per Game Type'
									},
									subtitle: {
										text: ''
									},
									xAxis: {
										categories: cat,
										max: 15
									},
									yAxis: {
										title: {
											text: 'Dollars'
										}
									},
									legend: {
										shadow: true
									},
									series: [{
										name: 'Lotto',
										data: lotto
									},{
										name: 'Casino',
										data: casino
									},{
										name: 'Sports',
										data: sports
									},{
										name: 'Rapid Balls',
										data: rapidballs
									}]
								});
							}
						</script>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title">Cashiers</h3>
						<!--
						<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Cashiers</h3>
						<button id="collapse_expand_cashier" class="btn btn-primary btn-sm pull-right">
							<img src="/lib/assets/datatables/media/images/details_open.png"></span>
							<span>Expand all</span>
						</button>
						-->
					</div>
					<div class="panel-body panel-body-fixed">
						<table id="cashiers_datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th></th>
								<th class="text-left">Island</th>
								<th class="text-left">Income</th>
								<th class="text-left">Expense</th>
								<th class="text-left">Commission</th>
								<th class="text-left">Net</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Customers</h3>
					</div>
					<div class="panel-body panel-body-fixed">
						<table id="customers_datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Customer</th>
								<th class="text-left">Island</th>
								<th class="text-left">Sales</th>
								<th class="text-left">Winnings</th>
								<th class="text-left">Net</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Deposits / Withdrawals</h3>
					</div>
					<div class="panel-body panel-body-fixed">
						<table id="withdraw_deposit_datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th></th>
								<th class="text-left">Location</th>
								<th class="text-left">Net</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	include('footer.php'); 
