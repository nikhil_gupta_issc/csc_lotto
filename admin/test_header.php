<?php
	date_default_timezone_set('America/New_York');
	
	// force SSL connection
	//if($_SERVER["HTTPS"] != "on"){
		//header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
		//exit();
	//}
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/test_config.php');
	
	// see if we need to logout
	if(isset($_GET['action'])){
		if($_GET['action'] == 'logout'){
			$session->logout();
		}
	}
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title><?php if(isset($page_title)){echo $page_title;}else{echo "CSCLotto.com - DING DING DING!";}?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="icon" type="image/x-icon" href="/images/favicon.ico" />
		
		<!-- jQuery -->
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery/jquery-1.11.0.js"></script>
		
		<!-- jQuery UI -->
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery-ui-1.11.4/jquery-ui.min.js"></script> 
		
		<!-- Font Awesome -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/font-awesome-4.2.0/css/font-awesome.min.css" />

		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap.min.css" media="screen">
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap-buttons.min.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/modern-business.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/tooltips.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-xl/BootstrapXL.css" />
		
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/jqBootstrapValidation.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/tooltips.js"></script>
		
		<!-- Bootstrap FormValidation Plugin (http://formvalidation.io/) -->
		<link rel="stylesheet" href="/lib/assets/formvalidation-0.6.2/dist/css/formValidation.min.css">
		<script type="text/javascript" src="/lib/assets/formvalidation-0.6.2/dist/js/formValidation.min.js"></script>
		<script type="text/javascript" src="/lib/assets/formvalidation-0.6.2/dist/js/framework/bootstrap.min.js"></script>
		
		<!-- Bootstrap Select Plugin -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-select/dist/css/bootstrap-select.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
		
		<!-- Bootstrap Chosen Plugin -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/chosen/chosen.css">
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-chosen/bootstrap-chosen.css">
		<script type="text/javascript" src="/lib/assets/chosen/chosen.jquery.js"></script>
		
		<!-- Bootstrap Form Helper Plugin -->
		<!--
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-formhelpers/css/bootstrap-formhelpers.min.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-formhelpers/js/bootstrap-formhelpers.min.js"></script>
		-->
		
		<!-- Bootstrap Date Range Picker Plugin (http://www.daterangepicker.com/) -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-daterangepicker/daterangepicker.js"></script>

		<!-- Bootstrap Toggle Plugin (http://www.bootstraptoggle.com/) -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-toggle/css/bootstrap-toggle.min.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
		
		<!-- Google Fonts -->
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine|Inconsolata|Droid+Sans" media="screen">
		
		<!-- DataTables -->
		<!-- <link rel="stylesheet" type="text/css" href="/lib/assets/datatables/media/css/jquery.dataTables.css"> -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/datatables/extensions/Plugins/integration/bootstrap/3/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="/lib/assets/datatables/extensions/Responsive/css/dataTables.responsive.css">
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/media/js/jquery.dataTables.js"></script>
		<script type="type/javascript" language="javascript" src="/lib/assets/datatables/extensions/TableTools/js/dataTables.tableTools.js"></script>
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/extensions/Plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
		
		<!-- High Charts -->
		<script src="/lib/assets/highstock-2.1.5/js/highstock.js"></script>
		<script src="/lib/assets/highstock-2.1.5/js/modules/exporting.js"></script>
		
		<!-- DataTables Responsive Plugin (have to load it here after main datatables js finishes loading -->
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/extensions/Responsive/js/dataTables.responsive.js"></script>
			
		<!-- Modernizr: For HTML5/CSS3 Feature Detection -->
		<script src="/lib/assets/modernizr/modernizr.custom_min.js"></script>
			
		<!-- Bootstrap DateTimePicker (http://eonasdan.github.io/bootstrap-datetimepicker/) -->
		<script type="text/javascript" src="/lib/assets/moment/moment.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
		<link rel="stylesheet" href="/lib/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" />
		
		<!-- BootBox Alerts -->
		<script type="text/javascript" src="/lib/assets/bootbox/bootbox.min.js"></script>
		
		<!-- Bootstrap ComboBox -->
		<script type="text/javascript" src="/lib/assets/bootstrap-combobox-modified/js/bootstrap-combobox.js"></script>
		<link rel="stylesheet" href="/lib/assets/bootstrap-combobox-modified/css/bootstrap-combobox.css" />
		
		<script>
			$(function () {
				// initialize inline datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
				
				// initialize datepickers
				$('.dp').datetimepicker({
					showTodayButton: true,
					format: 'MM/DD/YYYY'
				});
				
				// initialize datetimepickers
				$('.dtp').datetimepicker({
					showTodayButton: true,
					format: 'MM/DD/YYYY h:mm A'
				});
			});
		
			$(document).ready(function() {
				Modernizr.load({
					test: Modernizr.placeholder,
					nope: '/lib/assets/modernizr/polyfills/Placeholders.min.js'
				});
				
				// Enable Bootstrap Select 
				$('.selectpicker').selectpicker();
				
				// Bootstrap Combobox
				$('.combobox').combobox();
				
				// Enable Bootstrap Chosen
				$('.chosen-select').chosen();
				
				// thumbnail hover image changer
				$('.thumbnail').hover(
					function(){
						$(this).find('.thumbnail-caption').slideDown(250); //.fadeIn(250)
					},
					function(){
						$(this).find('.thumbnail-caption').slideUp(250); //.fadeOut(250)
					}
				);
			});
		</script>
	</head>
	<body>
<?php
	// include 2FA Code
	include($_SERVER['DOCUMENT_ROOT'].'/lib/framework/2fa_modal.php');
	
	if(!$session->logged_in){
		if(isset($_GET['forgotpass'])){
			include 'forgotpass.php';
		}else{
			include 'login.php';
		}
	}
	
	// Determine the page that is loaded so that it is showed as the selected item
	// in the side navigation bar.

	$accordion_and_link = ltrim(str_replace($_SERVER['DOCUMENT_ROOT']."/admin", "",dirname($_SERVER["SCRIPT_FILENAME"])),"/");
	$accordion_and_link = explode("/", $accordion_and_link);
	if(count($accordion_and_link) == 2){
		$accordion_opened = $accordion_and_link[0];
		$link_opened = $accordion_and_link[1];
	} else {
		$accordion_opened = "";
		$link_opened = "";
	}
?>
	<link rel="stylesheet" type="text/css" href="/admin/css/global.css?v=<?=CSS_VERSION?>" />

<?php
	// force password change
	if($session->userinfo['force_password_change'] != 0){
?>	
		<div id="header_bar">
			<span id="welcome"><h3>Forced Password Change</h3></span>
			<span id="logout">
				<form action='<?php echo ROOTPATH; ?>/lib/framework/login.php' method='post'>
					<input name='logout' class='button' type='submit' action='logout' value='Logout'>
					<input name='referrer' type='hidden' value='/admin/index.php'>
				</form>
			</span>
			<span id="logged_inas"><?php echo "Logged in as: ".$session->userinfo['firstname']." ".$session->userinfo['lastname']; ?></span>
		</div>
		
		<form action="lib/framework/login.php" method="POST" style="padding-left: 30px;">
			<div class="row">
				<?php
				if($form->num_errors > 0){
				echo "<td><font size=\"2\" color=\"#ff0000\">".$form->num_errors." error(s) found</font></td>";
				}
				?>
			</div>
			<div class="row">
				<div class="col-md-4">Current Password:</div>
				<input type="password" name="curpass" maxlength="30" value="<?php echo $form->value("curpass"); ?>">
				<?php echo $form->error("curpass"); ?>
			</div>
			<div class="row">
				<div class="col-md-4">New Password:</div>
				<input type="password" name="newpass" maxlength="30" value="<?php echo $form->value("newpass"); ?>">
				<?php echo $form->error("newpass"); ?>
				<br>
			</div>
			<div class="row">	
				<div class="col-md-4">Confirm New Password:</div>
				<input type="password" name="conf_newpass" maxlength="30" value="<?php echo $form->value("newpass"); ?>">
				<?php echo $form->error("newpass"); ?>
				<br>
			</div>
			<div class="row">	
				<div class="col-md-4">Email:</div>
				<input style="width: 350px;" type="text" name="email" maxlength="100" value="<?php
				if($form->value("email") == ""){
				echo $session->userinfo['email'];
				}else{
				echo $form->value("email");
				}
				?>">
			</div>
			<div class="row">
				<?php echo $form->error("email"); ?>
				<br>
				<input type="hidden" name="subedit" value="1">
				<button class="btn btn-primary" type="submit">Submit</button>
			</div>
		</form>
<?php
		include("footer.php");
		die();
	}
?>
	
	<nav class="navbar navbar-inverse navbar-fixed-top" style="opacity: .8">
		<div class="container-fluid" style="color: white;">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#" style="padding: 0px; padding-left: 10px;"><img src="<?php echo ROOTPATH; ?>/images/logo.png" height="50" style></img></a>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<form action='<?php echo ROOTPATH; ?>/lib/framework/login.php' method='post' class="navbar-form navbar-right">
					Signed in as <a href="#" class="navbar-link"><?php echo $session->userinfo["firstname"]." ".$session->userinfo["lastname"]; ?></a>
					<input name='logout' class='btn btn-primary' type='submit' action='logout' value='Logout' style="margin-left: 10px">
					<input name='referrer' type='hidden' value='/admin/index.php'>
				</form>
			</div>
			<span style="z-index: -1; position: absolute; top: 0; left: 0; margin-top: -8px; color: pink; width: 100%; text-align: center; display: inline-block;"><h3>Admin Panel</h3></span>
		</div>
	</nav>
	
	<style>
		body{
			margin-top: 30px;
		}
	</style>
