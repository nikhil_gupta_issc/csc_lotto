<?php
	$page_title = "Lotto Sales";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));
			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
						{ "data": "game" },
						{ "data": "sales" },
						{ "data": "commission" },
						{ "data": "winnings" },
						{ "data": "net" }
					],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/lotto_sales.php?filter_date_from=<?php echo date('Y-m-d', time()); ?>"
			});

			//Update Totals
			$.ajax({
				url: "/admin/ajax/datatables/lotto_sales_buttons.php",
				type: "POST",
				data: {
					action : "get_summary",
					filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
					filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
				},
				dataType: "json",
				success: function(response){
					if(response['result'] != false){
						// set overall total
						$("#total_sales").text("$" + response['result']['total_sales']);
						$("#total_commission").text("$" + response['result']['total_commission']);
						$("#total_winnings").text("$" + response['result']['total_winnings']);
						$("#total_net").text("$" + response['result']['total_net']);
					}else{
						alert("Operation Failed!\n"+response['query']);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "game" },
						{ "data": "sales" },
						{ "data": "commission" },
						{ "data": "winnings" },
						{ "data": "net" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/lotto_sales.php?filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);

				//Update Totals
				$.ajax({
					url: "/admin/ajax/datatables/lotto_sales_buttons.php",
					type: "POST",
					data: {
						action : "get_summary",
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// set overall total
							$("#total_sales").text("$" + response['result']['total_sales']);
							$("#total_commission").text("$" + response['result']['total_commission']);
							$("#total_winnings").text("$" + response['result']['total_winnings']);
							$("#total_net").text("$" + response['result']['total_net']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "game" },
						{ "data": "sales" },
						{ "data": "commission" },
						{ "data": "winnings" },
						{ "data": "net" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/lotto_sales.php?filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);

				//Update Totals
				$.ajax({
					url: "/admin/ajax/datatables/lotto_sales_buttons.php",
					type: "POST",
					data: {
						action : "get_summary",
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// set overall total
							$("#total_sales").text("$" + response['result']['total_sales']);
							$("#total_commission").text("$" + response['result']['total_commission']);
							$("#total_winnings").text("$" + response['result']['total_winnings']);
							$("#total_net").text("$" + response['result']['total_net']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#bet_datatable').on('shown.bs.modal', function() {
				//recalculate the dimensions of datatable
				bet_data_table.columns.adjust().responsive.recalc();
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Lotto Sales</h3>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Game</th>
								<th class="text-left">Sales</th>
								<th class="text-left">Commission</th>
								<th class="text-left">Winnings</th>
								<th class="text-left">Net</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					<div class="table-responsive">
						<table id="totals" class="table table-striped" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Total Sales</th>
									<th>Total Commission</th>
									<th>Total Winnings</th>
									<th>Total Net</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="total_sales"></td>
									<td id="total_commission"></td>
									<td id="total_winnings"></td>
									<td id="total_net"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 