<?php
	$page_title = "Customer Activity Summary";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));
			
			$.ajax({
				url: "/admin/ajax/system_summary.php",
				type: "POST",
				data: {
					action : "get_summary",
					start_date : "<?php echo date('Y-m-d', time()); ?>",
					end_date : "<?php echo date('Y-m-d 23:59:59', time()); ?>",
					customer_id : ""
				},
				dataType: "json",
				success: function(data){
					$("#casino_bet_number").text(data.casino_bet_number);
					$("#casino_bet_amount").text(data.casino_bet_amount);
					$("#casino_win_number").text(data.casino_win_number);
					$("#casino_win_amount").text(data.casino_win_amount);
					$("#lotto_purchase_amount").text(data.lotto_purchase_amount);
					$("#lotto_purchase_number").text(data.lotto_purchase_number);
					$("#lotto_win_amount").text(data.lotto_win_amount);
					$("#lotto_win_number").text(data.lotto_win_number);
					$("#withdrawals_amount").text(data.withdrawal_amount);
					$("#withdrawals_number").text(data.withdrawal_number);
					$("#deposits_amount").text(data.deposit_amount);
					$("#deposits_number").text(data.deposit_number);
					$("#sportsbook_bet_amount").text(data.sportsbook_bet_amount);
					$("#sportsbook_bet_number").text(data.sportsbook_bet_number);
					$("#sportsbook_win_amount").text(data.sportsbook_win_amount);
					$("#sportsbook_win_number").text(data.sportsbook_win_number);
					$("#rapidballs_bet_amount").text(data.rapidballs_bet_amount);
					$("#rapidballs_bet_number").text(data.rapidballs_bet_number);
					$("#rapidballs_win_amount").text(data.rapidballs_win_amount);
					$("#rapidballs_win_number").text(data.rapidballs_win_number);
					$("#casino_net_amount").text(data.casino_net);
					$("#lotto_net_amount").text(data.lotto_net);
					$("#withdrawal_deposit_net_amount").text(data.withdrawal_deposit_net);
					$("#sportsbook_net_amount").text(data.sportsbook_net);
					$("#rapidballs_net_amount").text(data.rapidballs_net);
					$("#overall_net_amount").text(data.overall_net);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
			
			$(document).on('change', '#customer', function(){				
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				$.ajax({
					url: "/admin/ajax/system_summary.php",
					type: "POST",
					data: {
						action : "get_summary",
						start_date : start_date,
						end_date : end_date,
						customer_id : ""
					},
					dataType: "json",
					success: function(data){
						$("#casino_bet_number").text(data.casino_bet_number);
						$("#casino_bet_amount").text(data.casino_bet_amount);
						$("#casino_win_number").text(data.casino_win_number);
						$("#casino_win_amount").text(data.casino_win_amount);
						$("#lotto_purchase_amount").text(data.lotto_purchase_amount);
						$("#lotto_purchase_number").text(data.lotto_purchase_number);
						$("#lotto_win_amount").text(data.lotto_win_amount);
						$("#lotto_win_number").text(data.lotto_win_number);
						$("#withdrawals_amount").text(data.withdrawal_amount);
						$("#withdrawals_number").text(data.withdrawal_number);
						$("#deposits_amount").text(data.deposit_amount);
						$("#deposits_number").text(data.deposit_number);
						$("#sportsbook_bet_amount").text(data.sportsbook_bet_amount);
						$("#sportsbook_bet_number").text(data.sportsbook_bet_number);
						$("#sportsbook_win_amount").text(data.sportsbook_win_amount);
						$("#sportsbook_win_number").text(data.sportsbook_win_number);
						$("#rapidballs_bet_amount").text(data.rapidballs_bet_amount);
						$("#rapidballs_bet_number").text(data.rapidballs_bet_number);
						$("#rapidballs_win_amount").text(data.rapidballs_win_amount);
						$("#rapidballs_win_number").text(data.rapidballs_win_number);
						$("#casino_net_amount").text(data.casino_net);
						$("#lotto_net_amount").text(data.lotto_net);
						$("#withdrawal_deposit_net_amount").text(data.withdrawal_deposit_net);
						$("#sportsbook_net_amount").text(data.sportsbook_net);
						$("#rapidballs_net_amount").text(data.rapidballs_net);
						$("#overall_net_amount").text(data.overall_net);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}

				$.ajax({
					url: "/admin/ajax/system_summary.php",
					type: "POST",
					data: {
						action : "get_summary",
						start_date : start_date,
						end_date : end_date,
						customer_id : $("#customer").val()
					},
					dataType: "json",
					success: function(data){
						$("#casino_bet_number").text(data.casino_bet_number);
						$("#casino_bet_amount").text(data.casino_bet_amount);
						$("#casino_win_number").text(data.casino_win_number);
						$("#casino_win_amount").text(data.casino_win_amount);
						$("#lotto_purchase_amount").text(data.lotto_purchase_amount);
						$("#lotto_purchase_number").text(data.lotto_purchase_number);
						$("#lotto_win_amount").text(data.lotto_win_amount);
						$("#lotto_win_number").text(data.lotto_win_number);
						$("#withdrawals_amount").text(data.withdrawal_amount);
						$("#withdrawals_number").text(data.withdrawal_number);
						$("#deposits_amount").text(data.deposit_amount);
						$("#deposits_number").text(data.deposit_number);
						$("#sportsbook_bet_amount").text(data.sportsbook_bet_amount);
						$("#sportsbook_bet_number").text(data.sportsbook_bet_number);
						$("#sportsbook_win_amount").text(data.sportsbook_win_amount);
						$("#sportsbook_win_number").text(data.sportsbook_win_number);
						$("#rapidballs_bet_amount").text(data.rapidballs_bet_amount);
						$("#rapidballs_bet_number").text(data.rapidballs_bet_number);
						$("#rapidballs_win_amount").text(data.rapidballs_win_amount);
						$("#rapidballs_win_number").text(data.rapidballs_win_number);
						$("#casino_net_amount").text(data.casino_net);
						$("#lotto_net_amount").text(data.lotto_net);
						$("#withdrawal_deposit_net_amount").text(data.withdrawal_deposit_net);
						$("#sportsbook_net_amount").text(data.sportsbook_net);		
						$("#rapidballs_net_amount").text(data.rapidballs_net);
						$("#overall_net_amount").text(data.overall_net);		
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}

				$.ajax({
					url: "/admin/ajax/system_summary.php",
					type: "POST",
					data: {
						action : "get_summary",
						start_date : start_date,
						end_date : end_date,
						customer_id : $("#customer").val()
					},
					dataType: "json",
					success: function(data){
						$("#casino_bet_number").text(data.casino_bet_number);
						$("#casino_bet_amount").text(data.casino_bet_amount);
						$("#casino_win_number").text(data.casino_win_number);
						$("#casino_win_amount").text(data.casino_win_amount);
						$("#lotto_purchase_amount").text(data.lotto_purchase_amount);
						$("#lotto_purchase_number").text(data.lotto_purchase_number);
						$("#lotto_win_amount").text(data.lotto_win_amount);
						$("#lotto_win_number").text(data.lotto_win_number);
						$("#withdrawals_amount").text(data.withdrawal_amount);
						$("#withdrawals_number").text(data.withdrawal_number);
						$("#deposits_amount").text(data.deposit_amount);
						$("#deposits_number").text(data.deposit_number);
						$("#sportsbook_bet_amount").text(data.sportsbook_bet_amount);
						$("#sportsbook_bet_number").text(data.sportsbook_bet_number);
						$("#sportsbook_win_amount").text(data.sportsbook_win_amount);
						$("#sportsbook_win_number").text(data.sportsbook_win_number);
						$("#rapidballs_bet_amount").text(data.rapidballs_bet_amount);
						$("#rapidballs_bet_number").text(data.rapidballs_bet_number);
						$("#rapidballs_win_amount").text(data.rapidballs_win_amount);
						$("#rapidballs_win_number").text(data.rapidballs_win_number);		
						$("#casino_net_amount").text(data.casino_net);
						$("#lotto_net_amount").text(data.lotto_net);
						$("#withdrawal_deposit_net_amount").text(data.withdrawal_deposit_net);
						$("#sportsbook_net_amount").text(data.sportsbook_net);	
						$("#rapidballs_net_amount").text(data.rapidballs_net);	
						$("#overall_net_amount").text(data.overall_net);					
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">System Summary</h3>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<div class="table-responsive">
						<table id="summary_table" class="table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Type</th>
									<th class="text-left">Number of Transactions</th>
									<th class="text-left">Amount</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Casino Bet</td>
									<td id="casino_bet_number"></td>
									<td id="casino_bet_amount"></td>
								</tr>
								<tr>
									<td>Casino Win</td>
									<td id="casino_win_number"></td>
									<td id="casino_win_amount"></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Casino Net:</td>
									<td id="casino_net_amount" style="font-weight:bold;"></td>
								</tr>
								<tr>
									<td>Lotto Purchase</td>
									<td id="lotto_purchase_number"></td>
									<td id="lotto_purchase_amount"></td>
								</tr>
								<tr>
									<td>Lotto Win</td>
									<td id="lotto_win_number"></td>
									<td id="lotto_win_amount"></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Lotto Net</td>
									<td id="lotto_net_amount" style="font-weight:bold;"></td>
								</tr>
								<tr>
									<td>Withdrawals</td>
									<td id="withdrawals_number"></td>
									<td id="withdrawals_amount"></td>
								</tr>
								<tr>
									<td>Deposits</td>
									<td id="deposits_number"></td>
									<td id="deposits_amount"></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Withdrawal/Deposit Net</td>
									<td id="withdrawal_deposit_net_amount" style="font-weight:bold;"></td>
								</tr>
								<tr>
									<td>Sportsbook Bet</td>
									<td id="sportsbook_bet_number"></td>
									<td id="sportsbook_bet_amount"></td>
								</tr>
								<tr>
									<td>Sportsbook Win</td>
									<td id="sportsbook_win_number"></td>
									<td id="sportsbook_win_amount"></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Sportsbook Net</td>
									<td id="sportsbook_net_amount" style="font-weight:bold;"></td>
								</tr>
								<tr>
									<td>Rapidballs Bet</td>
									<td id="rapidballs_bet_number"></td>
									<td id="rapidballs_bet_amount"></td>
								</tr>
								<tr>
									<td>Rapidballs Win</td>
									<td id="rapidballs_win_number"></td>
									<td id="rapidballs_win_amount"></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Rapidballs Net</td>
									<td id="rapidballs_net_amount" style="font-weight:bold;"></td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2" style="text-align:right;font-weight:bold;">Overall Net</td>
									<td id="overall_net_amount" style="font-weight:bold;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 