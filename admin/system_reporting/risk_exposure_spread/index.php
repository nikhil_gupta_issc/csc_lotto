<?php
	$page_title = "Risk/Exposure Spread";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
<div class="container-fluid">
	<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
	<div class="col-sm-8 col-md-9 col-xl-10">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Amount Wagered vs. Exposure</h3>
			</div>
			<div class="panel-body">
				<div id="chart_game_select_div" style="top: -50px; text-align: center;">
					<select id="game_id">
						<?php
							$q = "SELECT * FROM `lotto_game` ORDER BY `name` ASC";
							$games = $db->query($q);
							foreach($games as $game){
								echo "<option value='".$game['id']."'>".$game['name']."</option>";
							}
						?>
					</select>
				</div>
			
				<div id="container" style="height:400px; margin: 0 auto"></div>
				
				<script>
					$(document).ready(function(){
						$.ajax({
							url: '/admin/ajax/risk_exposure.php',
							type: 'POST',
							data: {
								game_id : $("#game_id").val()
							},
							async: true,
							dataType: "json",
							success: function (data) {
								risk_exposure(data.picks, data.wagers, data.risks);
							}
						});
						
						$("#game_id").change(function() {
							$.ajax({
								url: '/admin/ajax/risk_exposure.php',
								type: 'POST',
								data: {
									game_id : $("#game_id").val()
								},
								async: true,
								dataType: "json",
								success: function (data) {
									risk_exposure(data.picks, data.wagers, data.risks);
								}
							});
						});
					});
					
					function risk_exposure(picks, wagers, risks) {
						$('#container').highcharts({
							chart: {
								renderTo: 'container',
								defaultSeriesType: 'column',
								zoomType: 'xy'
							},
							title: {
								text: 'Amount Wagered vs. Exposure'
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								categories: picks,
								max: 15
							},
							scrollbar: {
								enabled: true
							},
							yAxis: {
								min: 0,
								title: {
									text: 'Dollars'
								}
							},
							legend: {
								shadow: true
							},
							tooltip: {
								useHTML: true,
								headerFormat: '<small>{point.key}</small><table>',
								pointFormat: '<tr><td style="color: {series.color}">{series.name}:{point.y} </td>',
								footerFormat: '</table>',
								valueDecimals: 2,
								crosshairs: [{
									width: 1,
									color: 'Gray'},
								{
									width: 1,
									color: 'gray'}]
							},
							plotOptions: {
								column: {
									pointPadding: 0.2,
									borderWidth: 0.5
								}
							},
							series: [{
								name: 'Wagers',
								data: wagers
							}, {
								name: 'Risk',
								data: risks
							}],
							 scrollbar: {
								enabled:true,
								barBackgroundColor: 'gray',
								barBorderRadius: 7,
								barBorderWidth: 0,
								buttonBackgroundColor: 'gray',
								buttonBorderWidth: 0,
								buttonArrowColor: 'purple',
								buttonBorderRadius: 7,
								rifleColor: 'purple',
								trackBackgroundColor: 'white',
								trackBorderWidth: 1,
								trackBorderColor: 'silver',
								trackBorderRadius: 7
							}
						});
					}
				</script>
			</div>
		</div>
	</div>
</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 