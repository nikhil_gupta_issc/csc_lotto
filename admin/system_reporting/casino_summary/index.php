<?php
	$page_title = "Casino Summary";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));

			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "vendor_id" },
					{ "data": "transaction_details" },
					{ "data": "bets" },
					{ "data": "wins" },
					{ "data": "amount" },
					{ "data": "play_count" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/casino_summary.php?filter_date_from=<?php echo date('Y-m-d', time()); ?>"
			});
			
			// temporary details datatable initialization
			// definition redefined on row click
			var details_data_table = $('#details_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "transaction_date" },
					{ "data": "customer_name" },
					{ "data": "bet_amount" },
					{ "data": "win_amount" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/casino_summary_details.php"
			});
			
			
			$.ajax({
				url: "/admin/ajax/datatables/casino_summary_buttons.php",
				type: "POST",
				data: {
					action : "overall_total",
					game : $('#game').val(),
					filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
					filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
				},
				dataType: "json",
				success: function(response){
					if(response['result'] != false){
						$("#total_bets").text("$" + response['result']['bets']);
						$("#total_wins").text("$" + response['result']['wins']);
						$("#total_net").text("$" + response['result']['net']);
						$("#total_play_count").text(response['result']['play_count']);
					}else{
						alert("Operation Failed!\n"+response['query']);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "vendor_id" },
						{ "data": "transaction_details" },
						{ "data": "bets" },
						{ "data": "wins" },
						{ "data": "amount" },
						{ "data": "play_count" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/casino_summary.php?filter_date_from="+start_date+"&filter_date_to="+end_date+"&game="+$("#game").val()
				});
				
				$.ajax({
					url: "/admin/ajax/datatables/casino_summary_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						game : $('#game').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							$("#total_bets").text("$" + response['result']['bets']);
							$("#total_wins").text("$" + response['result']['wins']);
							$("#total_net").text("$" + response['result']['net']);
							$("#total_play_count").text(response['result']['play_count']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "vendor_id" },
						{ "data": "transaction_details" },
						{ "data": "bets" },
						{ "data": "wins" },
						{ "data": "amount" },
						{ "data": "play_count" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/casino_summary.php?filter_date_from="+start_date+"&filter_date_to="+end_date+"&game="+$("#game").val()
				});
				
				$.ajax({
					url: "/admin/ajax/datatables/casino_summary_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						game : $('#game').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							$("#total_bets").text("$" + response['result']['bets']);
							$("#total_wins").text("$" + response['result']['wins']);
							$("#total_net").text("$" + response['result']['net']);
							$("#total_play_count").text(response['result']['play_count']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				data_table.draw(true);
			});

			//Status selector change
			$(document).on("change", "#game", function(){
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "vendor_id" },
						{ "data": "transaction_details" },
						{ "data": "bets" },
						{ "data": "wins" },
						{ "data": "amount" },
						{ "data": "play_count" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/casino_summary.php?filter_date_from="+start_date+"&filter_date_to="+end_date+"&game="+$("#game").val()
				});
				
				$.ajax({
					url: "/admin/ajax/datatables/casino_summary_buttons.php",
					type: "POST",
					data: {
						action : "overall_total",
						game : $('#game').val(),
						filter_date_from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						filter_date_to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							$("#total_bets").text("$" + response['result']['bets']);
							$("#total_wins").text("$" + response['result']['wins']);
							$("#total_net").text("$" + response['result']['net']);
							$("#total_play_count").text(response['result']['play_count']);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				data_table.draw(true);
			});
			
			$('#view_details_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var columns = data_table.row('.selected').data();
				
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load details datatable
				if($.fn.dataTable.isDataTable('#details_datatable')){
					details_data_table.destroy();
				}
				
				details_data_table = $('#details_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "transaction_date" },
						{ "data": "customer_name" },
						{ "data": "bet_amount" },
						{ "data": "win_amount" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/casino_summary_details.php?game_id="+columns['game_id']+"&vendor_id="+columns['vendor_id']+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				details_data_table.draw(true);
				
				// load title info
				$('#details_game_name').html(columns['transaction_details']);
				$('#details_casino_id').html(columns['vendor_id']);
				
				// show modal
				$('#view_details_modal').modal('show');
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#view_details_btn').trigger('click');
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Casino Summary</h3>
					<div class="btn-group pull-right">
						<a id="view_details_btn" href="#" class="btn btn-primary btn-sm btn-row-action">View Details</a>
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="status" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Game</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="game">
									<option value="">All Games</option>
									<?php
										$q = "SELECT SUBSTR(`transaction_details`, INSTR(`transaction_details`,'-') + 2, LENGTH(`transaction_details`)) AS `game_name` FROM `customer_transaction` WHERE transaction_type_id=1 OR transaction_type_id=2 GROUP BY SUBSTR(`transaction_details`, INSTR(`transaction_details`,'-') + 2, LENGTH(`transaction_details`))";
										$games = $db->query($q);
										
										foreach($games as $game){
											echo "<option value='".$game['game_name']."'>".$game['game_name']."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Casino ID</th>
								<th class="text-left">Game</th>
								<th class="text-left">Bets</th>
								<th class="text-left">Wins</th>
								<th class="text-left">Net</th>
								<th class="text-left">Play Count</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="3" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					<table id="totals" class="display table dt-responsive table-striped" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="20%"></th>
								<th width="20%"></th>
								<th>Total Bets</th>
								<th>Total Wins</th>
								<th>Total Net</th>
								<th>Total Count</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td id="total_bets"></td>
								<td id="total_wins"></td>
								<td id="total_net"></td>
								<td id="total_play_count"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for ticket details -->
	<div class="modal fade" id="view_details_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label"><span id="details_game_name"></span></h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							Casino #<span id="details_casino_id"></span>
						</div>
					</div>
					<div class="modal-body clearfix">
						<table id="details_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Date</th>
									<th class="text-left">Customer</th>
									<th class="text-left">Bet Amount</th>
									<th class="text-left">Win Amount</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="6" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 