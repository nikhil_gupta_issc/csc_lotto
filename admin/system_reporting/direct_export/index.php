<?php
	// load csv if ready for download
	if(isset($_GET['download'])){
		$filepath = "/srv/www/uploads/direct_export.csv";
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: inline; filename="export.csv"');
		header('Expires: 0');
		header('Cache-Control: no-cache, must-revalidate');
		header('Pragma: public');
		header('Content-Length: '.filesize($filepath));
		header('Accept-Ranges: bytes');
		readfile($filepath);
		exit;
	}
	
	$page_title = "Direct Exports";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>

	$(document).ready(function(){
		$(document).on('change', '#table', function(){
			$.ajax({
				url: "/admin/ajax/direct_export.php",
				type: "POST",
				data: {
					action : "get_columns",
					table_name : $("#table").val()
				},
				dataType: "json",
				success: function(data){
					if(data.success==true){
						$('#where_columns').html("<option value=''></option>");
						$('#order_by_columns').html("<option value=''></option>");
						$.each(data.columns, function(index, value){
							$('#where_columns').append("<option value='"+value.COLUMN_NAME+"'>"+value.COLUMN_NAME+"</option>");
							$('#order_by_columns').append("<option value='"+value.COLUMN_NAME+"'>"+value.COLUMN_NAME+"</option>");
						});
					}else{
						
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
		});
		
		$("#table option:first").attr("selected", "selected");
		$('#table').trigger('change');
		
		$(document).on('click', '#get_export', function(){
			$.ajax({
				url: "/admin/ajax/direct_export.php",
				type: "POST",
				data: {
					action : "get_export",
					table : $("#table").val(),
					where : $("#where_columns").val(),
					where_oper : $("#operator").val(),
					where_condition : $("#where_condition").val(),
					order_by : $("#order_by_columns").val(),
					order_by_asc_desc : $("#asc_desc").val(),
					limit : $("#limit").val()
				},
				dataType: "json",
				success: function(data){
					if(data.success==true){
						window.location = "index.php?download=true";
					}else{
						alert("Operation Failed!");
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
		});
	});
	</script>
	
	<style>
		td.details-control {
			background: url('/lib/assets/datatables/media/images/details_open.png') no-repeat center center;
			cursor: pointer;
		}
		tr.shown td.details-control {
			background: url('/lib/assets/datatables/media/images/details_close.png') no-repeat center center;
		}
		.child_table{
			padding-left: 50px;
		}
	</style>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
					<div class="btn-group pull-right">
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">
						
						<div class="form-group">
							<label for="table" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Table:</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="table">
									<?php
										$tables = $db->query("SHOW TABLES");
										foreach($tables as $table){
											echo "<option value=".$table['Tables_in_asurewin'].">".$table['Tables_in_asurewin']."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>
						<div class="form-group">
							<label for="where_columns" class="control-label col-sm-1">Where:</label>
							<div class="col-sm-2">
								<select id="where_columns" class="form-control">
									<option value=""></option>
								</select>
							</div>
							<div class="col-sm-1">
								<select id="operator" class="form-control">
									<option value="=">=</option>
									<option value="<="><=</option>
									<option value=">=">>=</option>
									<option value="<"><</option>
									<option value=">">></option>
									<option value="!=">!=</option>
								</select>
							</div>
							<div class="col-sm-2">
								<input type="text" id="where_condition" class="form-control"></input>
							</div>
							<div class="col-sm-8"></div>
						</div>
						<div class="form-group">
							<label for="order_by_columns" class="control-label col-sm-1">Order By:</label>
							<div class="col-sm-2">
								<select id="order_by_columns" class="form-control">
									<option value=""></option>
								</select>
							</div>
							<div class="col-sm-2">
								<select id="asc_desc" class="form-control">
									<option value="asc">Ascending</option>
									<option value="desc">Descending</option>
								</select>
							</div>
							<div class="col-sm-7"></div>
						</div>
						<div class="form-group">
							<label for="limit" class="control-label col-sm-1">Limit:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="limit"></input>
							</div>
							<div class="col-sm-9"></div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-1 col-sm-1">
								<button type="button" class="btn btn-primary" id="get_export">Export CSV</button>
							</div>
							<div class="col-sm-10"></div>
						</div>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
