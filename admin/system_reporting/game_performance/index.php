<?php
	$page_title = "Game Performance";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));
			
			$.ajax({
				url: "/admin/ajax/game_performance.php",
				type: "POST",
				data: {
					action : "get_performance",
					from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
					to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
				},
				dataType: "json",
				success: function(response){
					if(response.result == true){
						// set overall total
						$("#casino_wins").text(response.casino_wins);
						$("#casino_bets").text(response.casino_bets);
						$("#casino_return").text(response.casino_return);
						$("#rapidballs_wins").text(response.rapidballs_wins);
						$("#rapidballs_bets").text(response.rapidballs_bets);
						$("#rapidballs_return").text(response.rapidballs_return);
						$("#lotto_wins").text(response.lotto_wins);
						$("#lotto_bets").text(response.lotto_bets);
						$("#lotto_return").text(response.lotto_return);
						$("#sports_wins").text(response.sports_wins);
						$("#sports_bets").text(response.sports_bets);
						$("#sports_returns").text(response.sports_returns);
					}else{
						bootbox.alert(response.errors);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});

			$("#start_date").datetimepicker().on('dp.change', function (e) {
				$.ajax({
					url: "/admin/ajax/game_performance.php",
					type: "POST",
					data: {
						action : "get_performance",
						from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response.result == true){
							// set overall total
							$("#casino_wins").text(response.casino_wins);
							$("#casino_bets").text(response.casino_bets);
							$("#casino_return").text(response.casino_return);
							$("#rapidballs_wins").text(response.rapidballs_wins);
							$("#rapidballs_bets").text(response.rapidballs_bets);
							$("#rapidballs_return").text(response.rapidballs_return);
							$("#lotto_wins").text(response.lotto_wins);
							$("#lotto_bets").text(response.lotto_bets);
							$("#lotto_return").text(response.lotto_return);
							$("#sports_wins").text(response.sports_wins);
							$("#sports_bets").text(response.sports_bets);
							$("#sports_returns").text(response.sports_returns);
						}else{
							bootbox.alert(response.errors);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				$.ajax({
					url: "/admin/ajax/game_performance.php",
					type: "POST",
					data: {
						action : "get_performance",
						from : $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD'),
						to : $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD')
					},
					dataType: "json",
					success: function(response){
						if(response.result == true){
							// set overall total
							$("#casino_wins").text(response.casino_wins);
							$("#casino_bets").text(response.casino_bets);
							$("#casino_return").text(response.casino_return);
							$("#rapidballs_wins").text(response.rapidballs_wins);
							$("#rapidballs_bets").text(response.rapidballs_bets);
							$("#rapidballs_return").text(response.rapidballs_return);
							$("#lotto_wins").text(response.lotto_wins);
							$("#lotto_bets").text(response.lotto_bets);
							$("#lotto_return").text(response.lotto_return);
							$("#sports_wins").text(response.sports_wins);
							$("#sports_bets").text(response.sports_bets);
							$("#sports_returns").text(response.sports_returns);
						}else{
							bootbox.alert(response.errors);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Game Performance</h3>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Game</th>
								<th class="text-left">Bets</th>
								<th class="text-left">Wins</th>
								<th class="text-left">%RTP</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Casino</td>
								<td>$<span id="casino_bets">0.00</span></td>
								<td>$<span id="casino_wins">0.00</span></td>
								<td><span id="casino_return">0.00</span>%</td>
							</tr>
							<tr>
								<td>Rapidballs</td>
								<td>$<span id="rapidballs_bets">0.00</span></td>
								<td>$<span id="rapidballs_wins">0.00</span></td>
								<td><span id="rapidballs_return">0.00</span>%</td>
							</tr>
							<tr>
								<td>Sports</td>
								<td>$<span id="sports_bets">0.00</span></td>
								<td>$<span id="sports_wins">0.00</span></td>
								<td><span id="sports_return">0.00</span>%</td>
							</tr>
							<tr>
								<td>Lotto</td>
								<td>$<span id="lotto_bets">0.00</span></td>
								<td>$<span id="lotto_wins">0.00</span></td>
								<td><span id="lotto_return">0.00</span>%</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 