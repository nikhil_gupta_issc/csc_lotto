<?php
	$page_title = "Lotto Bet Limits";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var exp_data_table = $('#exp_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"lengthMenu": [[10, 25, -1], [10, 25, "All"]],
				"columns": [
					{ "data": "name" },
					{ "data": "payout_rate" },
					{ "data": "max_exposure" }
				],
				"order": [[ 0, "asc" ]],
				"ajax": "/admin/ajax/datatables/ball_limit_exposure.php"
			});
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"lengthMenu": [[10, 25, -1], [10, 25, "All"]],
				"columns": [
					{ "data": "ball_count" },
					{ "data": "bet_limit" },
					{ "data": "added_by" },
					{ "data": "added_on" }
				],
				"order": [[ 1, "desc" ]],
				"ajax": "/admin/ajax/datatables/ball_limits.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				/*
				var enabled = data_table.row(this).data();
				if(enabled[6] == "Enabled"){
					$('#disable_btn').text("Disable");
				}else{
					$('#disable_btn').text("Enable");
				}
				*/
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			/*
			$('#disable_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				
				$.ajax({
					url: "/admin/ajax/datatables/ball_limits_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			*/			
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			$(document).on("change", "#ball_count", function(){
				if($.fn.dataTable.isDataTable('#exp_datatable')){
					exp_data_table.destroy();
				}
				
				exp_data_table = $('#exp_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"lengthMenu": [[10, 25, -1], [10, 25, "All"]],
					"columns": [
						{ "data": "name" },
						{ "data": "payout_rate" },
						{ "data": "max_exposure" }
					],
					"order": [[ 0, "desc" ]],
					"ajax": "/admin/ajax/datatables/ball_limit_exposure.php?ball_count="+$("#ball_count").val()+"&limit="+$("#bet_limit").val().replace("$","")
				});
				
				exp_data_table.draw(true);
			});
			
			$(document).on("change", "#bet_limit", function(){
				if($.fn.dataTable.isDataTable('#exp_datatable')){
					exp_data_table.destroy();
				}
				
				exp_data_table = $('#exp_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"lengthMenu": [[10, 25, -1], [10, 25, "All"]],
					"columns": [
						{ "data": "name" },
						{ "data": "payout_rate" },
						{ "data": "max_exposure" }
					],
					"order": [[ 0, "desc" ]],
					"ajax": "/admin/ajax/datatables/ball_limit_exposure.php?ball_count="+$("#ball_count").val()+"&limit="+$("#bet_limit").val().replace("$","")
				});
				
				exp_data_table.draw(true);
			});
			
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/ball_limits_buttons.php",
					type: "POST",
					data: {
						action : 'delete',
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				$('#modal_row_id').html("ID #"+id);
				$('#ball_count').val(columns['ball_count']);
				$('#bet_limit').val(columns['bet_limit']);
				
				if($.fn.dataTable.isDataTable('#exp_datatable')){
					exp_data_table.destroy();
				}
				
				exp_data_table = $('#exp_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"lengthMenu": [[10, 25, -1], [10, 25, "All"]],
					"columns": [
						{ "data": "name" },
						{ "data": "payout_rate" },
						{ "data": "max_exposure" }
					],
					"order": [[ 0, "desc" ]],
					"ajax": "/admin/ajax/datatables/ball_limit_exposure.php?ball_count="+$("#ball_count").val()+"&limit="+$("#bet_limit").val().replace("$","")
				});
				
				exp_data_table.draw(true);
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#ball_count').focus();
				}, 500);                        
			})
			
// data validation for edit modal
			$(document).on('click', '#submit_edit', function() {
				if($('#reason').val() == ""){
					$('#reason_error').html("<br><span style=\"color:#F00\">This field is required</span>");
					$('#reason').focus();
				}else if($('#employee_id').val() == ""){
					$('#reason_error').html("<br><span style=\"color:#F00\">ERROR: Employee ID not detected</span>");
					$('#reason').focus();
				}else{
					$('#edit_row_modal').modal('hide');
					
					var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
					
					// deteremine whether this is update or insert
					if(id > 0){
						var action = "update";
					}else{
						var action = "insert";
					}
			
					$.ajax({
						url: "/admin/ajax/datatables/ball_limits_buttons.php",
						type: "POST",
						data: {
							action : action,
							id : id,
							ball_count : $('#ball_count').val(),
							limit : $('#bet_limit').val().replace("$","")
						},
						dataType: "json",
						success: function(data){
							if(data.result == false){
								bootbox.alert(data.errors);
							}
							
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							alert("Operation Failed!");
						}
					});
				}
			});
			
			$('#add_btn').click(function(){				
				// clear elements
				$('#modal_row_id').html("New");
				$('#ball_count').val("");
				$('#bet_limit').val("");
				
				if($.fn.dataTable.isDataTable('#exp_datatable')){
					exp_data_table.destroy();
				}
				
				exp_data_table = $('#exp_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"lengthMenu": [[10, 25, -1], [10, 25, "All"]],
					"columns": [
						{ "data": "name" },
						{ "data": "payout_rate" },
						{ "data": "max_exposure" }
					],
					"order": [[ 0, "desc" ]],
					"ajax": "/admin/ajax/datatables/ball_limit_exposure.php?ball_count="+$("#ball_count").val()+"&limit="+$("#bet_limit").val().replace("$","")
				});
				
				exp_data_table.draw(true);
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Lotto Bet Limits</h3>
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<!-- <a id="disable_btn" class="btn btn-default btn-sm btn-row-action">Disable</a> -->
						<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Delete</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th  class="text-left">Number of Balls</th>
								<th class="text-left">Limit</th>
								<th class="text-left">Creator</th>
								<th class="text-left">Creation</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Lotto Bet Limit</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="ball_count">Ball Count</label>
									<input type="text" class="form-control" id="ball_count" placeholder="2">
								</div>
							</div>
						</div>
					
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="bet_limit">Bet Limit</label>
									<input type="text" class="form-control" id="bet_limit" placeholder="$0.00">
								</div>
							</div>
						</div>
						
						<table id="exp_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Pay Scheme</th>
									<th class="text-left">Payout Rate</th>
									<th class="text-left">Max Exposure</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="3" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
			</div>
			<div class="modal-body">
				Are you sure you would like to remove this?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
			</div>
		</div>
	</div>
</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 