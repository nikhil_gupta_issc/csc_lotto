<?php
	$page_title = "Manage Locations";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	  // get current settings from db
	
?>
	<script>
		$(document).ready(function(){			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			// main scheme level datatable initialization
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/manage_locations.php",
				"columns": [
					{ "data": "location_id" },
					{ "data": "island" },
					{ "data": "name" },
					{ "data": "type" },
					{ "data": "address" },
					{ "data": "contact_person" },
					{ "data": "telephone" },
					{ "data": "payout_scheme" },
					{ "data": "commission_rate" },
                                        { "data": "ip_address" }
				],
				"order": [[3, 'asc']]
			});
			
			// temporary cashiers datatable initialization
			// definition redefined on button click
			var cashiers_data_table = $('#cashiers_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				//bInfo: false,
				"ajax": "/admin/ajax/datatables/manage_locations_cashiers.php",
				"columns": [
					{ "data": "cashier_id" },
					{ "data": "cashier_fullname" },
					{ "data": "cashier_username" },
					{ "data": "cashier_email" },
					{ "data": "last_login" }
				],
				"order": [[1, 'asc']]
			});
			
			$('#datatable tbody').on('click', 'tr', function(){				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			// confirmation of deletion
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/manage_locations_buttons.php",
					type: "POST",
					data: {
						action : 'delete',
						id : id
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
			
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				console.log(columns);
				$('#modal_row_id').html("ID #"+id);
				$('#location_name').html("Location: "+columns['name']);
				$('#name').val(columns['name']);
				$('#type').val(columns['type']);
				$('#contact_person').val(columns['contact_person']);
				$('#email').val(columns['email']);
				$('#telephone').val(columns['telephone']);
				$('#address1').val(columns['address1']);
				$('#address2').val(columns['address2']);
				$('#island_id').val(columns['island_id']);
				$('#country').val(columns['country']);
				$('#pay_scheme_id').val(columns['pay_scheme_id']);
				$('#receipt_message_id').val(columns['receipt_message_id']);
				$('#commission_rate').val(columns['commission_rate']);
                                $('#ip_address').val(columns['ip_address']);
                                $('#location_link').val(columns['location_link']);
                                $('#location_enabled').val(columns['location_enabled']);
                                if(columns['location_enabled'] == '1'){
                                     $('#location_enabled').bootstrapToggle('on');    
                                } else {
                                     $('#location_enabled').bootstrapToggle('off');
                                }
				if(columns['start_date'] != ""){
					$('#start_date').data("DateTimePicker").clear().date(moment(columns['start_date']));
				}

				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#name').focus();
				}, 500);                        
			})

			$(document).on('click', '#submit_edit', function() {
				$('#edit_row_modal').modal('hide');
				var location_enabled = 0;
				if(!$('#location_enabled').closest(".toggle").hasClass("off")){
					location_enabled = 1;
				}
				var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
				
				// deteremine whether this is update or insert
				if(id > 0){
					var action = "update";
				}else{
					var action = "insert";
				}
				
				// make sure date isn't null
				if($('#start_date').data("DateTimePicker").date() != null){
					var start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss');
				}else{
					var start_date;
				}
				
				$.ajax({
					url: "/admin/ajax/datatables/manage_locations_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id,
						name : $('#name').val(),
						type : $('#type').val(),
						contact_person : $('#contact_person').val(),
						contact_email : $('#contact_email').val(),
						email : $('#email').val(),
						telephone : $('#telephone').val(),
						address : $('#address1').val(),
						address2 : $('#address2').val(),
						island_id : $('#island_id').val(),
                                                ip_address : $('#ip_address').val(),
						country : $('#country').val(),
						payout_scheme_id : $('#pay_scheme_id').val(),
						receipt_message_id : $('#receipt_message_id').val(),
						commission_rate : ($('#commission_rate').val().replace("%","")) / 100,
						location_link : $('#location_link').val(),
						location_enabled : location_enabled,
						start_date : start_date
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#add_btn').click(function(){				
				// clear elements
				$('#modal_row_id').html("New");
			
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			
		<!-- Limits Modal Start -->	
			$('#limits_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				$('#cashier_limit').val(columns['cashier_limit']);
				$('#location_limit').val(columns['location_limit']);
				if(columns['enforce_location_limit'] == true){
					$('#enforce_location_limit').bootstrapToggle('on');
				}
			
				// show modal
				$('#limits_modal').modal('show');
			});

			$('#submit_limits').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
		
				$.ajax({
					url: "/admin/ajax/datatables/manage_locations_buttons.php",
					type: "POST",
					data: {
						action : "update",
						id : id,
						cashier_limit : $('#cashier_limit').val(),
						location_limit : $('#location_limit').val(),
						enforce_location_limit : +$('#enforce_location_limit').is(":checked"),
					},
					dataType: "json",
					success: function(response){						
						if(response['result'] != false){
							$('#limits_modal').modal('hide');
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);							
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					}
				});
			});
		<!-- Limits Modal End -->	
		
		<!-- Cashiers Modal Start -->			
			$('#cashiers_modal').on('shown.bs.modal', function() {
				//recalculate the dimensions of datatable
				cashiers_data_table.columns.adjust().responsive.recalc();
			});
			
			$('#cashiers_btn').click(function(){
				var columns = data_table.row('.selected').data();
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// load cashiers datatable
				if($.fn.dataTable.isDataTable('#cashiers_datatable')){
					cashiers_data_table.destroy();
				}
				
				// cashiers datatable initialization
				cashiers_data_table = $('#cashiers_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					//bInfo: false,
					"ajax": "/admin/ajax/datatables/manage_locations_cashiers.php?location_id="+columns['location_id'],
					"columns": [
						{ "data": "cashier_id" },
						{ "data": "cashier_fullname" },
						{ "data": "cashier_username" },
						{ "data": "cashier_email" },
						{ "data": "last_login" }
					],
					"order": [[1, 'asc']]
				});
				
				cashiers_data_table.draw(true);
				
				// show modal
				$('#cashiers_modal').modal('show');
			});
		<!-- Cashiers Modal End -->
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>

					<div class="btn-group pull-right" style="margin-left: 15px;">
						<a id="cashiers_btn" class="btn btn-default btn-sm btn-row-action">Cashiers</a>
						<a id="limits_btn" class="btn btn-default btn-sm btn-row-action">Limits</a>
					</div>
					
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Delete</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">ID</th>
								<th class="text-left">Island</th>
								<th class="text-left">Location</th>
								<th class="text-left">Type</th>
								<th class="text-left">Address</th>
								
								<th class="text-left">Contact Person</th>								
								<th class="text-left">Telephone</th>
								<th class="text-left">Pay Scheme</th>
								<th class="text-left">Commission</th>
                                                                <th class="text-left">Ip Address</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Location Details</h4>
						<div id="customer_number" class="pull-right" style="padding-right: 6px;">
							
						</div>
						<div id="modal_row_id" style="display: none;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="name">Location Name</label>
									<input type="text" class="form-control" id="name" placeholder="Location Name">
								</div>
							</div>
                                                         <div class="col-sm-3">
								<div class="form-group">
									<label for="name">Ip Address</label>
                                                                        <input type="text" class="form-control" id="ip_address" placeholder="Location Name">
									
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="name">Location Type</label>
									<select class="form-control" id="type">
										<option value="Independent">Store</option>
										<option value="In-Store">Franchise</option>
									</select>
								</div>
							</div>
						</div>
							
						<div class="col-sm-12">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="contact_person">Contact Person</label>
									<input type="text" class="form-control" id="contact_person" placeholder="Contact Person">
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="contact_email">Contact Person Email</label>
									<input type="text" class="form-control" id="contact_email" placeholder="Contact Person Email">
								</div>
							</div>
							
							<div class="col-sm-4">								
								<div class="form-group">
									<label for="telephone">Phone Number</label>
									<input type="text" class="form-control" id="telephone" placeholder="Location Phone Number">
								</div>	
							</div> 							
						</div>	
							
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="address1">Address 1</label>
									<input type="text" class="form-control" id="address1" placeholder="Street">
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<label for="address2">Address 2</label>
									<input type="text" class="form-control" id="address2" placeholder="Street">
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="island_id">Island:</label>
									<select id="island_id" name="island_id" class="form-control">
										<option value="-1"></option>
										<?php
											$q = "SELECT * FROM island WHERE id>0 ORDER BY name ASC";
											$islands = $db->query($q);
											foreach($islands as $island){
												echo "<option value='".$island['id']."'>".$island['name']."</option>";
											}
										?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="country">Country</label>
									<input type="text" class="form-control" id="country" placeholder="Bahamas">
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="start_date">Start Date</label>
									<div class='input-group date dtp' id='start_date'>
										<input type='text' class="form-control" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="pay_scheme_id">Pay Scheme</label>
									<select id="pay_scheme_id" name="receipt_message_id" class="form-control">
										<option value="-1"></option>
										<?php
											$q = "SELECT * FROM `payout_scheme` WHERE `id`>0 ORDER BY `name` ASC";
											$pay_schemes = $db->query($q);
											foreach($pay_schemes as $pay_scheme){
												echo "<option value='".$pay_scheme['id']."'>".$pay_scheme['name']."</option>";
											}
										?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="receipt_message_id">Receipt Message</label>
									<select id="receipt_message_id" name="receipt_message_id" class="form-control">
										<option value="-1"></option>
										<?php
											$q = "SELECT * FROM `receipt_messages` WHERE `id`>0 ORDER BY `name` ASC";
											$receipt_msgs = $db->query($q);
											foreach($receipt_msgs as $receipt_msg){
												echo "<option value='".$receipt_msg['id']."'>".$receipt_msg['name']."</option>";
											}
										?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="commission_rate">Commission Rate</label>
									<input type="text" class="form-control" id="commission_rate" placeholder="0.00%">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="location_link">Location Link</label>
									<input type="text" class="form-control" id="location_link" placeholder="http://">
								</div>
							</div>
							
				<div class="panel-body">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="location_enabled">Location Enabled</label>
							<div id="has_geolocation_container">
								<input id="location_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" >
							</div>
						</div>
						<hr>
					</div>
				</div>
				
						</div>
					</div>
					<div class="modal-footer clearfix">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Modal window for limits -->
	<div class="modal fade" id="limits_modal" tabindex="-1" role="dialog" aria-labelledby="limits_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="limits_modal_label">Limits</h4>
						<div id="location_name" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">

						<div class="col-sm-12">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="location_limit">Location Limit</label>
									<input type="text" class="form-control" id="location_limit" placeholder="$0.00">
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="cashier_limit">Cashier Limit</label>
									<input type="text" class="form-control" id="cashier_limit" placeholder="$0.00">
								</div>
							</div>

							<div class="col-sm-4">								
								<div class="form-group">
									<label class="control-label" for="enforce_location_limit">Enforce Limits</label><br>
									<input id="enforce_location_limit" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="130">
									<span style="float: right; padding-top: 5px;" id="enforce_location_limit"></span>
								</div>
							</div> 							
						</div>	
					</div>
					<div class="modal-footer clearfix">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_limits">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Modal for Location Cashiers -->
	<div class="modal fade" id="cashiers_modal" tabindex="-1" role="dialog" aria-labelledby="cashiers_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="cashiers_modal_label">Cashiers</h4>
					
					<div id="cashiers_modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div class="col-sm-12">
						<table id="cashiers_datatable" class="display table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Cashier ID</th>
									<th class="text-left">Name</th>
									<th class="text-left">Username</th>
									<th class="text-left">Email</th>
									<th class="text-left">Last Login</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="6" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal to Confirm Deletion -->
	<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
				</div>
				<div class="modal-body">
					Are you sure you would like to remove this?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
				</div>
			</div>
		</div>
	</div>

<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
