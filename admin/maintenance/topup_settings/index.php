<?php
	$page_title = "Topup Settings";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// get current settings from db
	$enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'topup_enabled'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $api_enable = $enable_info['value'];
        }
?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#save_changes').click(function(){
				var topup_enabled = 0;
				if(!$('#topup_enabled').closest(".toggle").hasClass("off")){
					topup_enabled = 1;
				}
				
				$.ajax({
					url: "/admin/ajax/topup_settings.php",
					type: "POST",
					dataType : "json",
					data: {
						topup_enabled : topup_enabled
					},
					success: function(response){
						if(response.success != 'true'){
							bootbox.alert("Operation Failed!\n"+response);
						}else{
							bootbox.alert("Saved!");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
				});
			});
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Topup Settings</h3>
				</div>
				<div class="panel-body">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="topup_enabled">Topup Enabled</label>
							<div id="has_geolocation_container">
								<input id="topup_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $api_enable == 1 ? "checked" : ""; ?> >
							</div>
						</div>
						<hr>
					</div>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-primary" id="save_changes">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
