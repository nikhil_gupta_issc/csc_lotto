<?php
	$page_title = "Loyalty Items";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			// main scheme level datatable initialization
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/loyalty_items.php",
				"columns": [
					{ "data": "name" },
					{ "data": "type" },
					{ "data": "point_cost" },
					{ "data": "value" },
					{ "data": "is_disabled" }
				],
				"order": [[3, 'asc']]
			});
			
			$('#datatable tbody').on('click', 'tr', function(){				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
				
				// set name of enable/disable button
				var row_array = data_table.row(this).data();
				if(row_array['is_disabled'] == "<span style='color:green;'>Enabled</span>"){
					$('#disable_btn').text("Disable");
				}else{
					$('#disable_btn').text("Enable");
				}
			});
			
			$('#disable_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				//Get action
				var action = $('#disable_btn').text().toLowerCase();
				
				$.ajax({
					url: "/admin/ajax/datatables/loyalty_items_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				$('#modal_row_id').html("ID #"+id);
				$('#image_path').val(columns['image_path']).trigger("change");
				$('#name').val(columns['name']);
				$('#type').val(columns['type']);
				$('#point_cost').val(columns['point_cost']);
				$('#value').val(columns['value']);
				$('#description').val(columns['description']);

				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#name').focus();
				}, 500);                        
			})

			$(document).on('click', '#submit_edit', function() {
				$('#edit_row_modal').modal('hide');
				
				var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
				
				// deteremine whether this is update or insert
				if(id > 0){
					var action = "update";
				}else{
					var action = "insert";
				}
				
				$.ajax({
					url: "/admin/ajax/datatables/loyalty_items_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id,
						image_path : $('#image_path').val(),
						name : $('#name').val(),
						type : $('#type').val(),
						point_cost : $('#point_cost').val().replace(",",""),
						value : $('#value').val().replace("$","").replace(",",""),
						description : $('#description').val()
					},
					dataType: "json",
					success: function(response){
						if(response['result'] != false){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}else{
							alert("Operation Failed!\n"+response['query']);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#add_btn').click(function(){				
				// clear elements
				$('#modal_row_id').html("New");
				$('#image_path').val("").trigger("change");
				$('#name').val("");
				$('#type').val("");
				$('#point_cost').val("");
				$('#value').val("");
				$('#description').val("");
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			$(document).on("change", "#image_path", function(){
				if($(this).val() != ""){
					$('#image_preview').attr('src', '/images/loyalty/'+$(this).val());
					$('#image_preview').show();
				}else{
					$('#image_preview').hide();
				}
			});
			
			$('#manage_images_modal').on('hidden.bs.modal', function (){
				$.ajax({
					url: "/admin/ajax/update_loyalty_pic_list.php",
					type: "POST",
					success: function(data){
						$('#image_path').html(data);
					}
				});
			});
			
			$.ajax({
				url: "/admin/ajax/update_loyalty_pic_list.php",
				type: "POST",
				success: function(data){
					$('#image_path').html(data);
				}
			});
			
			$(document).on("click", "#manage_images_btn", function(){
				$('#manage_images_modal').modal("show");
			});
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
					
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<a id="disable_btn" class="btn btn-default btn-sm btn-row-action">Disable</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Name</th>
								<th class="text-left">Type</th>
								<th class="text-left">Point Cost</th>
								<th class="text-left">Value</th>
								<th class="text-left">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Item Details</h4>
						<div id="customer_number" class="pull-right" style="padding-right: 6px;">
							
						</div>
						<div id="modal_row_id" style="display: none;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="row">
							<div class="col-sm-3">
								<img id="image_preview" src="" style="width: 100%; display: none; border: 1px solid black;">
							</div>
						
							<div class="col-sm-6">
								<div class="form-group">
									<label for="image_path">Image</label>
									<select id="image_path" name="image_path" class="form-control">

									</select>
								</div>
							</div>
							
							<div class="col-sm-3" style="padding-top: 25px;">
								<button id="manage_images_btn" type="button" class="btn btn-primary">Manage Images</button>
							</div>
						</div>
					
						<div class="row">
							<div class="col-sm-9">
								<div class="form-group">
									<label for="name">Item Name</label>
									<input type="text" class="form-control" id="name" placeholder="Item Name">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="name">Item Type</label>
									<input type="text" class="form-control" id="type" placeholder="Item Type">
								</div>
							</div>
						</div>
							
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="contact_person">Points Cost</label>
									<input type="text" class="form-control" id="point_cost" placeholder="Points Cost">
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<label for="contact_email">Value</label>
									<input type="text" class="form-control" id="value" placeholder="Value">
								</div>
							</div>						
						</div>	
							
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="address1">Description</label>
									<textarea class="form-control" id="description"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer clearfix">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Manages Images -->
	<div class="modal fade" id="manage_images_modal" tabindex="-1" role="dialog" aria-labelledby="manage_images_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="manage_images_label">Manage Images</h4>
				</div>
				<div class="modal-body clearfix">
					<div id="upload_photos" class="col-sm-12">
						<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/upload_loyalty_photos.php'); ?>
					</div>					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /Manage Images -->
	
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
