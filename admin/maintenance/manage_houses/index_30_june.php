<?php
	$page_title = "Manage Houses";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "short_name" },
					{ "data": "name" },
					{ "data": "web_cutoff_time" },
					{ "data": "drawing_time" },
					{ "data": "is_disabled" },
					{ "data": "house_disable_reason"}
				],
				"ajax": "/admin/ajax/datatables/manage_houses.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var columns = data_table.row(this).data();
				if(columns['is_disabled'] == "Enabled"){
					$('#disable_btn').text("Disable");
				}else{
					$('#disable_btn').text("Enable");
				}
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			/* Code for disable  house */
				$('#submit_reason').click(function(){
				 $("#disable_reason").modal("hide");
				
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				var text_reason = $("#text_reason").val();
				
				
				var action = $('#disable_action').val();
				
				$.ajax({
					url: "/admin/ajax/datatables/manage_houses_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id,
						reason : $('#text_reason').val(),
						
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
					//	$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
					
			});
			

			$('#disable_btn').click(function(){
			
			var text_value=$('#disable_btn').text();	
				
				if(text_value=='Enable')
				{
			
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				var action = 'enable'
				
				$.ajax({
					url: "/admin/ajax/datatables/manage_houses_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id,
					},
					dataType: "json",
					success: function(data){
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
					
	
				}
				
				else
				{
					$("#disable_reason").modal("show");
				}
			});
		
			
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/manage_houses_buttons.php",
					type: "POST",
					data: {
						action : 'delete',
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				$('#modal_row_id').html("ID #"+id);
				$('#short_name').val(columns['short_name']);
				$('#full_name').val(columns['name']);
				$('#web_closing').val(convertTime(columns['web_cutoff_time']));
				$('#drawing_time').val(convertTime(columns['drawing_time']));
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#short_name').focus();
				}, 350);                        
			})
			
			// data validation for edit modal
			$(document).on('click', '#submit_edit', function() {
				if($('#reason').val() == ""){
					$('#reason_error').html("<br><span style=\"color:#F00\">This field is required</span>");
					$('#reason').focus();
				}else if($('#employee_id').val() == ""){
					$('#reason_error').html("<br><span style=\"color:#F00\">ERROR: Employee ID not detected</span>");
					$('#reason').focus();
				}else{
					$('#edit_row_modal').modal('hide');
					
					var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
					
					// deteremine whether this is update or insert
					if(id > 0){
						var action = "update";
					}else{
						var action = "insert";
					}
			
					$.ajax({
						url: "/admin/ajax/datatables/manage_houses_buttons.php",
						type: "POST",
						data: {
							action : action,
							id : id,
							short_name : $('#short_name').val(),
							full_name : $('#full_name').val(),
							web_closing : $('#web_closing').val(),
							drawing_time : $('#drawing_time').val()
						},
						dataType: "json",
						success: function(data){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							alert("Operation Failed!");
						}
					});
				}
			});
			
			$('#add_btn').click(function(){				
				// clear elements
				$('#modal_row_id').html("New");
				$('#short_name').val("");
				$('#full_name').val("");
				$('#web_closing').val("");
				$('#drawing_time').val("");
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
		});
		
		function convertTime(time){
			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours < 10) sHours = "0" + sHours;
			if(minutes < 10) sMinutes = "0" + sMinutes;
			return sHours + ":" + sMinutes;
        }
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Manage Houses</h3>
					
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<a id="disable_btn" class="btn btn-danger btn-sm btn-row-action">Disable</a>
						<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Delete</a>
					</div>
					
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Short Name</th>
								<th class="text-left">Full Name</th>
								<th class="text-left">Closing Time</th>
								<th class="text-left">Drawing Time</th>
								
								<th class="text-left">Status</th> 
								<th class="text-left">Disble Reason</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">House Details</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="short_name">Short Name</label>
									<input type="text" class="form-control" id="short_name" placeholder="Short Name">
								</div>
								
								<div class="form-group">
									<label for="full_name">Full Name</label>
									<input type="text" class="form-control" id="full_name" placeholder="Full Name">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="drawing_time">Drawing Time</label>
									<input type="time" class="form-control" id="drawing_time">
								</div>	
	
								<div class="form-group">
									<label for="web_closing">Closing Time</label>
									<input type="time" class="form-control" id="web_closing">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	
<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
			</div>
			<div class="modal-body">
				Are you sure you would like to remove this house?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="disable_reason" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		<form name=disable_reason_form method="post"> 
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="remove_confirmation_modal_label">Disable Reason</h4>
			</div>
			<div class="modal-body">
				<textarea name="text_reason" id="text_reason" cols="60"></textarea>
				<input type="hidden" value="disable" name="disable_action" id="disable_action"/>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="submit_reason">Submit</button>
			</div>
		</form>
		</div>
	</div>
</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
