<?php
	$page_title = "Vouchers";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			$(function () {
				// initialize datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
			});
		
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "code" },
					{ "data": "start_date" },
					{ "data": "end_date" },
					{ "data": "user" },
					{ "data": "amount" },
					{ "data": "is_enabled" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/vouchers.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var columns = data_table.row(this).data();
				if(columns['is_enabled'] == "Enabled"){
					$('#enable_btn').text("Disable");
				}else{
					$('#enable_btn').text("Enable");
				}
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			$('#add_btn').click(function(){	
				// show modal
				$('#add_row_modal').modal('show');
			});
			
			$(document).on('click', '#submit_add', function() {
				var action = "insert";
				
				$.ajax({
					url: "/admin/ajax/datatables/vouchers_buttons.php",
					type: "POST",
					data: {
						action : action,
						code: $('#code').val(),
						start_date: $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss'),
						end_date: $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss'),
						amount: $('#amount').val(),
						user_id: $('#user_id').val()
					},
					dataType: "json",
					success: function(response){
						if(response.successful == "false"){
							bootbox.alert(response.errors);
						}else{
							// hide buttons and reload table if successful
							$('#add_row_modal').modal('hide');
							$('.btn-row-action').hide();
							data_table.draw(true);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#enable_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				
				$.ajax({
					url: "/admin/ajax/datatables/vouchers_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id
					},
					dataType: "json",
					success: function(data){
						if(data == true){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Vouchers</h3>
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="enable_btn" class="btn btn-default btn-sm btn-row-action">Enable</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Code</th>
								<th class="text-left">Start Date</th>
								<th class="text-left">End Date</th>
								<th class="text-left">Customer</th>
								<th class="text-left">Amount</th>
								<th class="text-left">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="add_row_modal" tabindex="-1" role="dialog" aria-labelledby="add_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="add_row_modal_label">Add Voucher</h4>
						<div id="modal_row_id" style="display: none;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-12">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="code">Code</label>
									<input type="text" class="form-control" id="code" placeholder="Code">
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-sm-12 control-label" for="start_date" style="text-align: center;"><br>Start Date<br><br></label>
									<div class="dtp-inlne" id="start_date"></div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-sm-12 control-label" for="end_date" style="text-align: center;"><br>End Date<br><br></label>
									<div class="dtp-inlne" id="end_date"></div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="user_id">Customer</label>
									<select class="form-control" id="user_id">
										<option value="NULL">Global</option>
										<?php
											$q = "SELECT u.id, u.firstname, u.lastname FROM customers c JOIN users u ON c.user_id=u.id";
											$customers = $db->query($q);
											foreach($customers as $customer){
												echo "<option value='".$customer['id']."'>".$customer['firstname']." ".$customer['lastname']." (".$customer['id'].")</option>";
											}
										?>
									</select>
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="amount">Amount</label>
									<input type="text" class="form-control" id="amount" placeholder="Amount">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer clearfix">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_add">Add</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 