<?php
	$page_title = "System Settings";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// get current settings from db
	$settings = array();
	$q = "SELECT * FROM `settings`";
	$settings_info = $db->query($q);
	foreach($settings_info as $setting){
		$settings[$setting['setting']] = $setting['value'];
	}
?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#save_changes').click(function(){
				var geolocation = 0;
				if(!$('#has_geolocation').closest(".toggle").hasClass("off")){
					geolocation = 1;
				}
				
				var video = 0;
				if(!$('#has_video').closest(".toggle").hasClass("off")){
					video = 1;
				}
				
				var promotion = 0;
				if(!$('#has_promotion').closest(".toggle").hasClass("off")){
					promotion = 1;
				}
				
				var maintenance_mode = 0;
				if(!$('#maintenance_mode').closest(".toggle").hasClass("off")){
					maintenance_mode = 1;
				}
				
				$.ajax({
					url: "/admin/ajax/system_settings.php",
					type: "POST",
					dataType : "json",
					data: {
						action : "update",
						max_withdraw_per_day : $('#max_withdraw_per_day').val(),
						ticket_expiration : $('#ticket_expiration').val(),
						loyalty_points_lottery : $('#loyalty_points_lottery').val(),
						loyalty_points_casino : $('#loyalty_points_casino').val(),
						loyalty_points_sports : $('#loyalty_points_sports').val(),
						loyalty_points_rapidballs : $('#loyalty_points_rapidballs').val(),
						gold_point_amount : $('#gold_point_amount').val(),
						platinum_point_amount : $('#platinum_point_amount').val(),
						diamond_point_amount : $('#diamond_point_amount').val(),
						winning_customers_days : $('#winning_customers_days').val(),
						winning_customers_amount : $('#winning_customers_amount').val(),
						maintenance_mode : maintenance_mode,

						mailer_host : $('#mailer_host').val(),
						mailer_username : $('#mailer_username').val(),
						mailer_password : $('#mailer_password').val(),
						mailer_port : $('#mailer_port').val(),
						mailer_from_name : $('#mailer_from_name').val(),
						mailer_from_email : $('#mailer_from_email').val(),
						
						twilio_account_sid : $('#twilio_account_sid').val(),
						twilio_auth_token : $('#twilio_auth_token').val(),
						twilio_phone_number : $('#twilio_phone_number').val(),
						
						google_analytics_id : $('#google_analytics_id').val(),
						
						has_geolocation : geolocation,
						has_promotion : promotion,
						has_video : video,
						lottery_version : $('#lottery_version').val(),
						promo_url : $('#promo_url').val(),
                                                promo_point : $('#promo_point').val(),
                                                per_loyalty_points : $('#per_loyalty_points').val()
					},
					success: function(response){
						if(response.success != 'true'){
							bootbox.alert("Operation Failed!\n"+response);
						}else{
							bootbox.alert("Saved!");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
				});
			});
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">System Settings</h3>
				</div>
				<div class="panel-body">
					<form id="edit_row_modal_form" class="form" action="#" method="POST">
						<div class="col-sm-6">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="max_withdraw_per_day">Max Withdrawal Per Day </label>
									<input type="text" class="form-control" id="max_withdraw_per_day" placeholder="$0.00" value="<?php echo $settings['max_withdraw_per_day']; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="ticket_expiration">Ticket Expiration</label>
									<div class="input-group">
										<input type="text" class="form-control" id="ticket_expiration" placeholder="0" value="<?php echo $settings['ticket_expiration']; ?>" aria-describedby="ticket_expiration_addon">
										<span class="input-group-addon" id="ticket_expiration_addon">Days</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="loyalty_points_lottery">Loyalty Points Per $1.00 (Lottery)</label>
									<div class="input-group">
										<input type="text" class="form-control" id="loyalty_points_lottery" placeholder="0" value="<?php echo $settings['loyalty_points_lottery']; ?>" aria-describedby="loyalty_points_lottery_addon">
										<span class="input-group-addon" id="loyalty_points_lottery_addon">Points</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="loyalty_points_sports">Loyalty Points Per $1.00 (Sports)</label>
									<div class="input-group">
										<input type="text" class="form-control" id="loyalty_points_sports" placeholder="0" value="<?php echo $settings['loyalty_points_sports']; ?>" aria-describedby="loyalty_points_sports_addon">
										<span class="input-group-addon" id="loyalty_points_sports_addon">Points</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="loyalty_points_casino">Loyalty Points Per $1.00 (Casino)</label>
									<div class="input-group">
										<input type="text" class="form-control" id="loyalty_points_casino" placeholder="0" value="<?php echo $settings['loyalty_points_casino']; ?>" aria-describedby="loyalty_points_casino_addon">
										<span class="input-group-addon" id="loyalty_points_casino_addon">Points</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="loyalty_points_rapidballs">Loyalty Points Per $1.00 (Rapidballs)</label>
									<div class="input-group">
										<input type="text" class="form-control" id="loyalty_points_rapidballs" placeholder="0" value="<?php echo $settings['loyalty_points_rapidballs']; ?>" aria-describedby="loyalty_points_rapidballs_addon">
										<span class="input-group-addon" id="loyalty_points_rapidballs_addon">Points</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="gold_point_amount">Points Needed For Gold</label>
									<div class="input-group">
										<input type="text" class="form-control" id="gold_point_amount" placeholder="0" value="<?php echo $settings['gold_point_amount']; ?>" aria-describedby="gold_point_amount_addon">
										<span class="input-group-addon" id="gold_point_amount_addon">Points</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="platinum_point_amount">Points Needed For Platinum</label>
									<div class="input-group">
										<input type="text" class="form-control" id="platinum_point_amount" placeholder="0" value="<?php echo $settings['platinum_point_amount']; ?>" aria-describedby="platinum_point_amount_addon">
										<span class="input-group-addon" id="platinum_point_amount_addon">Points</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="diamond_point_amount">Points Needed For Diamond</label>
									<div class="input-group">
										<input type="text" class="form-control" id="diamond_point_amount" placeholder="0" value="<?php echo $settings['diamond_point_amount']; ?>" aria-describedby="diamond_point_amount_addon">
										<span class="input-group-addon" id="diamond_point_amount_addon">Points</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="winning_customers_amount">Minimum Amount Won to Show in Winning Customers (Homepage)</label>
									<input type="text" class="form-control" id="winning_customers_amount" placeholder="0" value="<?php echo $settings['winning_customers_amount']; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="winning_customers_days">Number of Days Previous of Today to Look For Winning Customers (Homepage)</label>
									<div class="input-group">
										<input type="text" class="form-control" id="winning_customers_days" placeholder="0" value="<?php echo $settings['winning_customers_days']; ?>" aria-describedby="winning_customers_days_addon">
										<span class="input-group-addon" id="winning_customers_days_addon">Days</span>
									</div>
								</div>
							</div>
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-6 no-padding">
								<div class="form-group">
									<label for="promo_point">Promo Point</label>
									<div class="input-group">
										<input type="text" class="form-control" id="promo_point" placeholder="0" value="<?php echo $settings['promo_point']; ?>" aria-describedby="promo_point_addon">
										
									</div>
								</div>
                                                              </div>
                                                             <div class="col-sm-6 no-padding">
								<div class="form-group">
									<label for="per_loyalty_points">Per Loyalty points</label>
									<div class="input-group">
										<input type="text" class="form-control" id="per_loyalty_points" placeholder="0" value="<?php echo $settings['per_loyalty_points']; ?>" aria-describedby="per_loyalty_points_addon">
										
									</div>
								</div>
                                                              </div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_from_email">Maintenace Mode</label>
									<div id="maintenance_mode_container">
										<input id="maintenance_mode" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $settings['maintenance_mode'] == 1 ? "checked" : ""; ?> >
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_host">Mailer SMTP Host</label>
									<input type="text" class="form-control" id="mailer_host" value="<?php echo $settings['mailer_host']; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_username">Mailer Username</label>
									<input type="text" class="form-control" id="mailer_username" value="<?php echo $settings['mailer_username']; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_password">Mailer Password</label>
									<input type="password" class="form-control" id="mailer_password" value="<?php echo $settings['mailer_password']; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_port">Mailer SMTP Port</label>
									<input type="text" class="form-control" id="mailer_port" value="<?php echo $settings['mailer_port']; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_from_name">Mailer From Name</label>
									<input type="text" class="form-control" id="mailer_from_name" value="<?php echo $settings['mailer_from_name']; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_from_email">Mailer From Email</label>
									<input type="text" class="form-control" id="mailer_from_email" value="<?php echo $settings['mailer_from_email']; ?>">
								</div>
								<hr>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="twilio_account_sid">Twilio Account SID</label>
									<input type="text" class="form-control" id="twilio_account_sid" value="<?php echo $settings['twilio_account_sid']; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="twilio_auth_token">Twilio Auth Token</label>
									<input type="password" class="form-control" id="twilio_auth_token" value="<?php echo $settings['twilio_auth_token']; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="twilio_phone_number">Twilio Phone Number</label>
									<input type="text" class="form-control" id="twilio_phone_number" value="<?php echo $settings['twilio_phone_number']; ?>">
								</div>
								<hr>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_from_email">Geolocation Enabled?</label>
									<div id="has_geolocation_container">
										<input id="has_geolocation" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $settings['has_geolocation'] == 1 ? "checked" : ""; ?> >
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_from_email">Video Enabled?</label>
									<div id="has_video_container">
										<input id="has_video" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $settings['has_video'] == 1 ? "checked" : ""; ?> >
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="mailer_from_email">Promotion Page Enabled?</label>
									<div id="has_promotion_container">
										<input id="has_promotion" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $settings['has_promotion'] == 1 ? "checked" : ""; ?> >
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="google_analytics_id">Google Analytics ID</label>
									<input type="text" class="form-control" id="google_analytics_id" value="<?php echo $settings['google_analytics_id']; ?>">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="mailer_from_email">Lottery Version</label>
									<input class="form-control" id="lottery_version" type="text" value="<?php echo $settings['lottery_version']; ?>" >
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="Advertise">URl video Advertise </label>
									<input class="form-control" id="promo_url" type="text" value="<?php echo $settings['promo_url']; ?>" >
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-primary" id="save_changes">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
