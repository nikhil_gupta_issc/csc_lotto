<?php
	$page_title = "GLI Approved Files";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			// main scheme level datatable initialization
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/file_hashes.php",
				"columns": [
					{ "data": "path" },
					{ "data": "size" },
					{ "data": "modified_on" },
					{ "data": "hash" },
					{ "data": "approved_on" }
				],
				"order": [[0, 'asc']]
			});

			$('#datatable tbody').on('click', 'tr', function(){				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
			
			$(document).on('click', '#approve_btn', function() {
				$('#approve_btn').html = "Please wait...";
				$('#approve_btn').prop('disabled', true);
				
				$.ajax({
					url: "/admin/ajax/datatables/file_hashes_buttons.php",
					type: "POST",
					dataType: "json",
					success: function(response){
						if(response.fail_count > 0){
							bootbox.alert("Failed to approve "+response.fail_count+" out of "+response.file_count+" files.");
						}else{
							bootbox.alert("Approved "+response.insert_count+" out of "+response.file_count+" files.");
							
							// hide buttons and reload table if successful
							$('#add_row_modal').modal('hide');
							$('.btn-row-action').hide();
							data_table.draw(true);
						}
						$('#approve_btn').html = "Approve Current Files";
						$('#approve_btn').prop('disabled', false);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
						$('#approve_btn').html = "Approve Current Files";
						$('#approve_btn').prop('disabled', false);
					}
				});
			});
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>

					<div class="btn-group pull-right">
						<a id="approve_btn" href="#" class="btn btn-primary btn-sm">Approve All Files</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">File Name</th>
								<th class="text-left">Size</th>
								<th class="text-left">Last Modified</th>
								<th class="text-left">Hash</th>
								<th class="text-left">Approved On</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
