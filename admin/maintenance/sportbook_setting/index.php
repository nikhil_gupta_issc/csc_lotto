<?php

	$page_title = "Sportsbook Settings";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// get current settings from db
	$settings = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'sportbook_api'";
	$settings_info = $db->queryOneRow($q);
	if(!empty($settings_info)){
           $api = $settings_info['value'];
        }
        
        // get current settings from db
	$enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'sports_enabled'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $api_enable = $enable_info['value'];
        }
        
?>

	<script type="text/javascript">
		$(document).ready(function(){

                        $('.sportbook_api').bootstrapToggle();
			$('#save_changes').click(function(){
			
                                var api = $('.sportbook_api:checked').val();
                                if(api != undefined){					
				var sports_enabled = 0;
				if(!$('#sports_enabled').closest(".toggle").hasClass("off")){
					sports_enabled = 1;
				}
					$.ajax({
						url: "/admin/ajax/sportbook_setting.php",
						type: "POST",
						dataType : "json",
						data: {
							sportbook_api : api,
							sports_enabled : sports_enabled
						},
						success: function(response){
							if(response.success != 'true'){
								bootbox.alert("Operation Failed!\n"+response);
							}else{
								bootbox.alert("Sportsbook API changed!");
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							bootbox.alert("Unexpected Error!");
						}
					});
                            } else {
                                bootbox.alert("You must select atleast one Sportsbook API.");
                             }
			});

                      /*$(document).on('change','.sportbook_api',function(){
                           $('#sbtech_enabled').bootstrapToggle('toggle');
                      });*/
		});

       
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Sportbook APIs</h3>
				</div> 


                               

				<div class="panel-body">
					<div class="col-sm-4">
						<div class="form-group">
							<label for="sbtech_enabled">Sbtech</label>
                                                        <div class="input-group">
								<input id="sbtech_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" value="sbtech" class="sportbook_api" <?php echo $api == 'sbtech' ? "checked" : ""; ?> >
</div>
							
						</div>
						<hr>
					</div>
                                        <div class="col-sm-4">
						<div class="form-group">
							<label for="royalsoft_enabled">RoyalSoft</label>
<div class="input-group">
								<input id="royalsoft_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" value="royalsoft" class="sportbook_api" <?php echo $api == 'royalsoft' ? "checked" : ""; ?> >
</div>
							
						</div>
						<hr>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label for="sports_enabled">Sportbook Enabled</label>
							<div class="input-group">			
								<input id="sports_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" value="sportbookenabled" <?php echo $api_enable == 1 ? "checked" : ""; ?> class="sportbook_api">						
						</div>
						<hr>
					</div>
				</div>			
				</div>	
				
				<div class="panel-footer">
					<button type="button" class="btn btn-primary" id="save_changes">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
