<?php
$page_title = "Games";
include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// variable to store index of currently selected child table
			var selected_child_index = -1;
			var selected_house_id = -1;
			var child_table = [];
			
			// function to dynamically create game child datatables
			function get_child_table(house_id){				
				var child_table = '<div class="child_table table-responsive"><table id="child_'+house_id+'" class="display table dt-child-table" cellspacing="0" width="100%">'+
					'<thead>'+
						'<tr>'+
							'<th>Game</th>'+
							'<th>Min Bet</th>'+
							'<th>Exposure</th>'+
							'<th>Max Bet</th>'+
							'<th>Status</th>'+
						'</tr>'+
					'</thead>'+
					'<tbody>'+
						'<tr>'+
							'<td colspan="10" class="dataTables_empty">Loading data from server</td>'+
						'</tr>'+
					'</tbody>'+
				'</table></div>';
			
				return child_table;
			}
			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			// main house level datatable initialization
			var data_table = $('#datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				"bInfo": false,
				"fnDrawCallback": function (){
					collapse_expand_rows();
				},
				"ajax": "/admin/ajax/datatables/game_settings.php",
				"columns": [
					{
						"className":      	'details-control',
						"orderable":      	false,
						"data":           	null,
						"defaultContent": 	'',
						"width":			'20px'
					},
					{ "data": "house_name" }
				],
				"order": [[1, 'asc']]
			});
			
			// track last clicked house
			$('#datatable tbody').on('click', 'tr', function () {
				if (typeof $(this).attr('id') != 'undefined'){
					selected_house_id = $(this).attr('id').replace("row_", "");
				}
			});
			
			// Add event listener for opening and closing game child tables
			$('#datatable tbody').on('click', 'td.details-control', function () {
				var tr = $(this).closest('tr');
				var row = data_table.row( tr );
		 
				if ( row.child.isShown() ) {
					// This row is already open - close it
					row.child.hide();
					tr.removeClass('shown');
				}else{
					var row_data = row.data();
					var house_id = row_data['DT_RowId'].replace("row_", "");
					
					// is child has not been dynamically created, create it
					if (typeof row.child() == 'undefined'){
						// create this row
						row.child(get_child_table(house_id)).show();
						
						// create datatable
						child_table[house_id] = $('#child_'+house_id).DataTable({
							"processing": true,
							"serverSide": true,
							"deferRender": true,
							"ajax": "/admin/ajax/datatables/game_settings_child_table.php?house_id="+house_id,
							"columns": [
								{ "data": "game_name" },
								{ "data": "web_min_bet" },
								{ "data": "web_max_per_ball" },
								{ "data": "web_max_bet" },
								{ "data": "status" }
							],
							"order": [[0, 'asc']],
							"paging":   	false,
							"searching": 	false,
							"info":     	false
						});
					}else{
						// otherwise, show what was already rendered
						row.child.show();
					}
					
					tr.addClass('shown');
				}
			});
			
			$(document).on('click', '.dt-child-table tbody tr', function(){					
				// update index of selected child table
				selected_child_index = $(this).closest('.dt-child-table').attr('id').replace("child_", "");
			
				// set name of enable/disable button
				var enabled = child_table[selected_child_index].row(this).data();
				if(enabled['status'] == "Enabled"){
					$('#disable_btn').text("Disable");
				}else{
					$('#disable_btn').text("Enable");
				}
			
				// remove selected class from all rows
				$('.dt-child-table tbody tr').removeClass('selected');
				
				// select row clicked and show action buttons
				$(this).addClass('selected');
				$('.btn-row-action').show();
			});
			
			$('#mass_update').click(function(){
				$("#mass_update_modal").modal("show");
			});
			
			$('#disable_btn').click(function(){
				// get database id of selected row
				var id = child_table[selected_child_index].$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				
				$.ajax({
					url: "/admin/ajax/datatables/game_settings_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						child_table[selected_child_index].draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = child_table[selected_child_index].$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/game_settings_buttons.php",
					type: "POST",
					data: {
						action : 'delete',
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						child_table[selected_child_index].draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
			$(document).on('dblclick', '.dt-child-table tbody tr', function(){					
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				// get database id of selected row
				var game_id = child_table[selected_child_index].$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = child_table[selected_child_index].row('.selected').data();
				
				// get house name
				house_name = child_table[selected_child_index].$('tr.selected').closest('.dt-child-table').closest('tr').prev().text();
				
				// populate fields in modal
				$('#game_id').html(game_id);
				$('#modal_subtitle').html(columns['name']);
				$('#short_name').val(columns['short_name']);
				$('#name').val(columns['name']);
				$('#house_id').val(columns['house_id']).change();
				$('#number_of_balls').val(columns['number_of_balls']);
				// trigger keyup to load xml game selector
				$('#number_of_balls').trigger('keyup');
				$('#web_min').val(columns['web_min_bet']);
				$('#web_exp').val(columns['web_max_per_ball']);
				$('#web_max').val(columns['web_max_bet']);
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#house_id').focus();
				}, 350);                        
			})
			
			$(document).on('click', '#submit_edit', function() {
				$('#edit_row_modal').modal('hide');
				
				var game_id = parseInt($('#game_id').html());
				
				// deteremine whether this is update or insert
				if(game_id > 0){
					var action = "update";
				}else{
					var action = "insert";
				}
				$.ajax({
					url: "/admin/ajax/datatables/game_settings_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : game_id,
						house_id : $('#house_id').val(),
						number_of_balls : $('#number_of_balls').val(),
						short_name : $('#short_name').val(),
						name : $('#name').val(),
						xml_feed : $('#xml_feed').val(),
						web_min : $('#web_min').val(),
						web_exp : $('#web_exp').val(),
						web_max : $('#web_max').val()
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						child_table[selected_child_index].draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$(document).on('click', '#submit_mass_edit', function(){
				$('#mass_update_modal').modal('hide');
				$.ajax({
					url: "/admin/ajax/datatables/game_settings_buttons.php",
					type: "POST",
					data: {
						action : "mass_update",
						web_min : $('#mass_web_min').val(),
						web_exp : $('#mass_web_exp').val(),
						web_max : $('#mass_web_max').val()
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						child_table[selected_child_index].draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#add_btn').click(function(){
				// get last clicked house id if it exists
				if (typeof selected_house_id == 'undefined'){
					house_id = -1;
				}else{
					house_id = selected_house_id;
					// default to last clicked house
					$('#house_id').val(house_id).change();
				}
				
				// clear elements
				$('#game_id').html("New");
				$('#modal_subtitle').html("New");
				$('#number_of_balls').val("");
				$('#short_name').val("");
				$("#xml_feed").empty();
				$('#name').val("");
				$('#web_min').val("");
				$('#web_exp').val("");
				$('#web_max').val("");
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			var isCollapse = 1;
			$('#collapse_expand').on('click', function() {
				if(isCollapse === 1){
					$('#collapse_expand').html('<img src="/lib/assets/datatables/media/images/details_close.png"></span> <span> Collapse all</span>');
					isCollapse = 0; 
				}else{
					$('#collapse_expand').html('<img src="/lib/assets/datatables/media/images/details_open.png"></span> <span> Expand all</span>');
					isCollapse = 1;
				}
				collapse_expand_rows();
			});

			function collapse_expand_rows(){
				var table_length = $('#datatable tbody tr').length;
				for (var i = 0; i < table_length; i++) {
					var tr = $('.details-control').parents('tr').eq(i);
					var row = data_table.row( tr );
			 
					if ( isCollapse === 1 ) {
						if ( row.child.isShown() ) {
							// This row is already open - close it
							row.child.hide();
							tr.removeClass('shown');
						}
					}else{
						var row_data = row.data();
						var house_id = row_data['DT_RowId'].replace("row_", "");
						
						// is child has not been dynamically created, create it
						if (typeof row.child() == 'undefined'){
							// create this row
							row.child(get_child_table(house_id)).show();
							
							// create datatable
							child_table[house_id] = $('#child_'+house_id).DataTable({
								"processing": true,
								"serverSide": true,
								"deferRender": true,
								"ajax": "/admin/ajax/datatables/game_settings_child_table.php?house_id="+house_id,
								"columns": [
									{ "data": "game_name" },
									{ "data": "web_min_bet" },
									{ "data": "web_max_per_ball" },
									{ "data": "web_max_bet" },
									{ "data": "status" }
								],
								"order": [[0, 'asc']],
								"paging":   	false,
								"searching": 	false,
								"info":     	false
							});
						}else{
							// otherwise, show what was already rendered
							row.child.show();
						}
						
						tr.addClass('shown');
					}
				}
			}
		});
		
		// only show xml feeds that are valid for number of balls chosen
		$(document).on('keyup', '#number_of_balls', function(){
			var num_balls = $(this).val();
			
			if(num_balls < 2){
				$('#xml_feed_warning').html("Invalid ball count");
			}else if(num_balls == 2){
				$('#xml_feed_warning').html("Uses last 2 digits");
			}else{
				$('#xml_feed_warning').html("");
			}
			
			$.ajax({
				url: "/admin/ajax/datatables/game_settings_buttons.php",
				type: "POST",
				data: {
					action : 'get_xml_games',
					num_balls : num_balls
				},
				dataType: "json",
				success: function(data){
					$("#xml_feed").empty();
					$("#xml_feed").append("<option value='-1'></option>");
					$.each(data, function(i, item){
						$("#xml_feed").append("<option value='"+item.game_id+"'>"+item.stateprov_name+" - "+item.name+" ("+item.game_id+")</option>");
					});
					
					var game_id = parseInt($('#game_id').html());
					
					$.ajax({
						url: "/admin/ajax/datatables/game_settings_buttons.php",
						type: "POST",
						data: {
							action : 'get_xml_game_id',
							id : game_id,
						},
						dataType: "json",
						success: function(data){
							// set selected xml feed
							$('#xml_feed option')
								.removeAttr('selected')
								.filter('[value='+data+']')
								.attr('selected', true);
						}
					});
				}
			});
		});
		
		function convertTime(time){
			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours < 10) sHours = "0" + sHours;
			if(minutes < 10) sMinutes = "0" + sMinutes;
			return sHours + ":" + sMinutes;
        }
	</script>
	
	<style>
		td.details-control {
			background: url('/lib/assets/datatables/media/images/details_open.png') no-repeat center center;
			cursor: pointer;
		}
		tr.shown td.details-control {
			background: url('/lib/assets/datatables/media/images/details_close.png') no-repeat center center;
		}
		.expand-icon {
			image: url('/lib/assets/datatables/media/images/details_open.png') no-repeat center center;
			width: 20px;
			height: 20px;
		}
		.collapse-icon {
			background: url('/lib/assets/datatables/media/images/details_close.png') no-repeat center center;
			width: 20px;
			height: 20px;
		}
		.child_table{
			padding-left: 50px;
		}
	</style>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<a id="disable_btn" class="btn btn-default btn-sm btn-row-action">Disable</a>
						<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Delete</a>
					</div>
					
					<button id="collapse_expand" class="btn btn-primary btn-sm pull-left" style="margin-right: 10px; margin-left: 10px;">
						<img src="/lib/assets/datatables/media/images/details_open.png"></span>
						<span>Expand all</span>
					</button>
					
					<a id="mass_update" href="#" class="btn btn-primary btn-sm pull-right" style="margin-right: 10px; margin-left: 10px;">Mass Update</a>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th></th>
								<th class="text-left">House</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for mass update -->
	<div class="modal fade" id="mass_update_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="mass_update_modal_label">Mass Update</h4>
						<div id="modal_subtitle" class="pull-right" style="padding-right: 6px;">
						</div>
						<div id="game_id" style="display: none"></div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="mass_web_min">Min Bet</label>
								<input type="text" class="form-control" id="mass_web_min" placeholder="Min">
							</div>
						</div>
						<div class="col-sm-4">	
							<div class="form-group">
								<label for="mass_web_exp">Exposure</label>
								<input type="text" class="form-control" id="mass_web_exp" placeholder="Exp">
							</div>
						</div>
						<div class="col-sm-4">		
							<div class="form-group">
								<label for="mass_web_max">Max Bet</label>
								<input type="text" class="form-control" id="mass_web_max" placeholder="Max">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_mass_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Game Settings</h4>
						<div id="modal_subtitle" class="pull-right" style="padding-right: 6px;">
						</div>
						<div id="game_id" style="display: none"></div>
					</div>
					<div class="modal-body clearfix">
						<div id="house_select" class="col-sm-12">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="house_id">House</label>
									<select class="form-control" id="house_id">
										<option value='-1'></option>
										<?php
											$q = "SELECT * from `lotto_house` ORDER BY short_name";
											$houses = $db->query($q);
											foreach($houses as $house){
												echo "<option value='".$house['id']."'>".$house['short_name']." - ".$house['name']."</option>";
											}
										?>									
									</select>
								</div>
								
								<div class="form-group">
									<label for="short_name">Short Name</label>
									<input type="text" class="form-control" id="short_name" placeholder="Short Name">
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<label for="number_of_balls">Number of Balls</label>
									<input type="text" class="form-control" id="number_of_balls" placeholder="Balls">
								</div>
								
								<div class="form-group">
									<label for="name">Full Name</label>
									<input type="text" class="form-control" id="name" placeholder="Full Name">
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<label for="xml_feed">XML Feed</label>
									<span style="color: red;" id="xml_feed_warning" class="pull-right"></span>
									<select class="form-control" id="xml_feed">
										<option value='-1'></option>								
									</select>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="web_min">Min Bet</label>
									<input type="text" class="form-control" id="web_min" placeholder="Min">
								</div>
							</div>
							<div class="col-sm-4">	
								<div class="form-group">
									<label for="web_exp">Exposure</label>
									<input type="text" class="form-control" id="web_exp" placeholder="Exp">
								</div>
							</div>
							<div class="col-sm-4">		
								<div class="form-group">
									<label for="web_max">Max Bet</label>
									<input type="text" class="form-control" id="web_max" placeholder="Max">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
				</div>
				<div class="modal-body">
					Are you sure you would like to remove this game?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
