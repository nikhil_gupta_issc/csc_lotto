<?php
	$page_title = "POS News Ticker";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			//Toggle the type and show the correct div
			$(document).on("change", "#ticker_type", function(){
				if($(this).val() == "1"){
					$("#location_container").show();
					$("#island_container").hide();
					$("#location_type_container").hide();
				}else if($(this).val() == "2"){
					$("#location_container").hide();
					$("#island_container").show();
					$("#location_type_container").show();
				}
			});
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
				"columns": [
					{ "data": "ticker_type" },
					{ "data": "area" },
					{ "data": "location_type" },
					{ "data": "content" },
					{ "data": "expiration_date" },
					{ "data": "added_by" },
					{ "data": "added_on" }
				],
				"order": [[ 1, "desc" ]],
				"ajax": "/admin/ajax/datatables/pos_news_ticker.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				/*
				var enabled = data_table.row(this).data();
				if(enabled[6] == "Enabled"){
					$('#disable_btn').text("Disable");
				}else{
					$('#disable_btn').text("Enable");
				}
				*/
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});	
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/pos_news_ticker_buttons.php",
					type: "POST",
					data: {
						action : 'delete',
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				$('#modal_row_id').html("ID #"+id);
				$('#content').val(columns['content']);
				$('#expiration_date').data("DateTimePicker").clear().date(moment(columns['expiration_date']));
				$('#location').val(columns['location_id']);
				$('#island').val(columns['island_id']);
				$('#location_type').val(columns['is_franchise']);
				$('#ticker_type').val(columns['ticker_type_id']);
				
				if(columns['ticker_type_id'] == 1){
					$("#location_container").show();
					$("#island_container").hide();
					$("#location_type_container").hide();
				}else if(columns['ticker_type_id'] == 2){
					$("#location_container").hide();
					$("#island_container").show();
					$("#location_type_container").show();
				}
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#content').focus();
				}, 500);                        
			})
			
// data validation for edit modal
			$(document).on('click', '#submit_edit', function() {
				if($('#reason').val() == ""){
					$('#reason_error').html("<br><span style=\"color:#F00\">This field is required</span>");
					$('#reason').focus();
				}else if($('#employee_id').val() == ""){
					$('#reason_error').html("<br><span style=\"color:#F00\">ERROR: Employee ID not detected</span>");
					$('#reason').focus();
				}else{
					$('#edit_row_modal').modal('hide');
					
					var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
					
					// deteremine whether this is update or insert
					if(id > 0){
						var action = "update";
					}else{
						var action = "insert";
					}
			
					$.ajax({
						url: "/admin/ajax/datatables/pos_news_ticker_buttons.php",
						type: "POST",
						data: {
							action : action,
							id : id,
							content : $('#content').val(),
							ticker_type_id : $('#ticker_type').val(),
							location_id : $('#location').val(),
							island_id : $('#island').val(),
							is_franchise : $('#location_type').val(),
							expiration_date : $('#expiration_date').data("DateTimePicker").date().format('YYYY-MM-DD')
						},
						dataType: "json",
						success: function(data){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							alert("Operation Failed!");
						}
					});
				}
			});
			
			$('#add_btn').click(function(){				
				// clear elements
				$('#modal_row_id').html("New");
				$('#content').val("");
				$('#expiration_date').data("DateTimePicker").clear().date(moment(<?php echo date("Y-m-d"); ?>));
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">POS News Ticker</h3>
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<!-- <a id="disable_btn" class="btn btn-default btn-sm btn-row-action">Disable</a> -->
						<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Delete</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Type</th>
								<th class="text-left">Area</th>
								<th class="text-left">Location Type</th>
								<th class="text-left">Content</th>
								<th class="text-left">Expiration</th>
								<th class="text-left">Creator</th>
								<th class="text-left">Creation</th> 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Ticker Details</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="ticker_type">Ticker Type</label>
								<select class="form-control" id="ticker_type">
									<option value='1'>Location</option>
									<option value='2'>Island</option>
								</select>
							</div>
						</div>
					
						<div id="location_container" class="col-sm-12">
							<div class="form-group">
								<label for="location">Location</label>
								<select class="form-control" id="location">
									<option value='-1'>All Locations</option>
									<?php
										$q = "SELECT id, name FROM `panel_location`";
										$locations = $db->query($q);
										foreach($locations as $location){
											echo "<option value='".$location['id']."'>".$location['name']."</option>";
										}
									?>
								</select>
							</div>
						</div>
						
						<div id="island_container" style="display:none;" class="col-sm-12">
							<div class="form-group">
								<label for="island">Island</label>
								<select class="form-control" id="island">
									<option value='-1'>All Islands</option>
									<?php
										$q = "SELECT id, name FROM `island`";
										$islands = $db->query($q);
										foreach($islands as $island){
											echo "<option value='".$island['id']."'>".$island['name']."</option>";
										}
									?>
								</select>
							</div>
						</div>
						
						<div id="location_type_container" style="display:none;" class="col-sm-12">
							<div class="form-group">
								<label for="location_type">Location Type</label>
								<select class="form-control" id="location_type">
									<option value='-1'>Any</option>
									<option value='1'>Franchise</option>
									<option value='0'>Non-Franchise</option>
								</select>
							</div>
						</div>
					
						<div class="col-sm-12">
							<div class="form-group">
								<label for="content">Content</label>
								<textarea rows="8" class="form-control" id="content"></textarea>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<label for="expiration_date" class="control-label" style="margin-right: 5px;">Expiration Date</label>
								<div class="input-group date dp" id='expiration_date'>
									<input type='text' class="form-control" placeholder="Expiration Date"/>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	
<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
			</div>
			<div class="modal-body">
				Are you sure you would like to remove this ticker?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
			</div>
		</div>
	</div>
</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 