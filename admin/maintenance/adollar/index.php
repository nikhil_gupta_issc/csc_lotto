<?php
	$page_title = "A Dollar";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>

	<script>
		$(document).ready(function(){
			$(function () {
				// initialize datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
			});
		
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "adollar_number" },
					
					{ "data": "adollar_value" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/adollar.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){			
				// toggle selection css class
				if($(this).hasClass('selected')){
					//	$(this).removeClass('selected');
				}else{
					//	data_table.$('tr.selected').removeClass('selected');
					//$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			$('#add_btn').click(function(){	
				// show modal
				$('#add_row_modal').modal('show');
			});
			
			
			
			$('#import_btn').click(function(){	
				// show modal
				$('#import_modal').modal('show');
			});

			$('#import_form').submit(function(e){
			
				e.preventDefault();
				$.ajax({
					url: "/admin/ajax/datatables/adollar_buttons.php",
					type: 'POST',
					dataType: "json",
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: function(response){
						if(response.success_count == 0){
							bootbox.alert("Error: Nothing was imported.");
						}else{
							bootbox.alert("Success: "+response.success_count+" of "+response.total_count+" rows were imported.");
							
							// hide buttons and reload table if successful
							$('#import_modal').modal('hide');
							$('.btn-row-action').hide();
							data_table.draw(true);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#import_file').change(function () {
				var ext_arr = this.value.match(/\.([0-9a-z]+)(?:[\?#]|$)/);
				if(ext_arr){
					ext = ext_arr[1];
					switch (ext) {
						case 'csv':
						case 'txt':
						case 'text':
							$('#submit_import').attr('disabled', false);
							break;
						default:
							alert('This is not an allowed file type.');
							this.value = '';
					}
				}else{
					$('#submit_import').attr('disabled', false);
				}
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">A Dollar</h3>
					<div class="btn-group pull-right">
						<a id="import_btn" class="btn btn-primary btn-sm">Import</a>
						
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left"> Number</th>
								
								<th class="text-left"> Value </th>
							
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="add_row_modal" tabindex="-1" role="dialog" aria-labelledby="add_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="add_row_modal_label">Add Gift Card</h4>
						<div id="modal_row_id" style="display: none;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="card_number">Card Number</label>
								<input type="text" class="form-control" id="card_number" placeholder="Card Number">
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<label for="security_code">Security Code</label>
								<input type="text" class="form-control" id="security_code" placeholder="Security Code">
							</div>
						</div>
					
						<div class="col-sm-6">
							<div class="form-group">
								<label for="expires_on" class="control-label">Expiration Date</label>
								<div class="input-group date dp col-sm-6" id='expires_on'>
									<input type='text' class="form-control" placeholder="Expiration"/>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
					
						<div class="col-sm-6">
							<div class="form-group">
								<label for="amount">Amount</label>
								<input type="text" class="form-control" id="amount" placeholder="Amount">
							</div>
						</div>
					</div>
					<div class="modal-footer clearfix">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_add">Add</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	
	<!-- Modal window for importing -->
	<div class="modal fade" id="import_modal" tabindex="-1" role="dialog" aria-labelledby="import_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="import_form" class="form" action="#" enctype="multipart/form-data" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="import_modal_label">Import A Dollar</h4>
					</div>
					<div class="modal-body clearfix">
						<br><br><br>
						<input type="file" id="import_file" name="import_file">
						<br><br><br>
					</div>
					<div class="modal-footer clearfix">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-primary" id="submit_import" value="Import">
					</div>
				</form>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
