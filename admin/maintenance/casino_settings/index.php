<?php
	$page_title = "Casino Settings";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// get current settings from db
	$settings = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_api_chetu'";
	$settings_info = $db->queryOneRow($q);
	if(!empty($settings_info)){
           $api = $settings_info['value'];
        }
        
        
        // get current settings from db
	$settings = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_api_multislot'";
	$settings_info = $db->queryOneRow($q);
	if(!empty($settings_info)){
           $api_multislot = $settings_info['value'];
        }
        	
	// get current settings from db
	$enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_enabled'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $api_enable = $enable_info['value'];
        }
?>

	<script type="text/javascript">
		$(document).ready(function(){
			
			$('#save_changes').click(function(){
				
				
				var api_chetu = $('#chetu_enabled:checked').val();
				if(api_chetu == undefined){
					api_chetu =0;
				}
				var api_multislot = $('#multislot_enabled:checked').val();
				if(api_multislot == undefined){
					api_multislot = 0;
				}
				var casino_enabled = 0;
				if(!$('#casino_enabled').closest(".toggle").hasClass("off")){
					casino_enabled = 1;
				}
				
				$.ajax({
					url: "/admin/ajax/casino_settings.php",
					type: "POST",
					dataType : "json",
					data: {
						casino_api_chetu : api_chetu,
						casino_api_multislot : api_multislot,
						casino_enabled : casino_enabled
					},
					success: function(response){
						if(response.success != 'true'){
							bootbox.alert("Operation Failed!\n"+response);
						}else{
							bootbox.alert("Saved!");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
				});
			});
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Casino Settings</h3>
				</div>
				<div class="panel-body">
					<div class="col-sm-4">
						<div class="form-group">
							<label for="casino_enabled">Casino Enabled</label>
							<div id="has_geolocation_container">
								<input id="casino_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $api_enable == 1 ? "checked" : ""; ?> >
							</div>
						</div>
						<hr>
					</div>
				
					<div class="col-sm-4">
						<div class="form-group">
							<label for="chetu_enabled">Chetu</label>
                                                        <div class="input-group">
								<input id="chetu_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" value="chetu" class="casino_api" <?php echo $api == 'chetu' ? "checked" : ""; ?> >
</div>
							
						</div>
						<hr>
						</div>
				<div class="col-sm-4">
					<div class="form-group">
							<label for="multislot_enabled">Multislot</label>
<div class="input-group">
								<input id="multislot_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" value="multislot" class="casino_api" <?php echo $api_multislot == 'multislot' ? "checked" : ""; ?> >
</div>
							
						</div>
						<hr>
						</div>
				</div>
				
				<div class="panel-footer">
					<button type="button" class="btn btn-primary" id="save_changes">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
