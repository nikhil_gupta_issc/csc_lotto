<?php
	$page_title = "Receipt Messages";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
				"columns": [
					{ "data": "name" },
					{ "data": "message" },
					{ "data": "added_by" },
					{ "data": "added_on" }
				],
				"order": [[ 1, "desc" ]],
				"ajax": "/admin/ajax/datatables/receipt_messages.php",
				"sDom": 'T<"H"plfr>t<"F"ip>',
				"oTableTools": {
					"aButtons": [ "csv", "pdf" ],
					"sSwfPath": "https://datatables.net/release-datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
				}
			});
			
			// TableTools
			//var tt = new $.fn.dataTable.TableTools(data_table);
			//$(tt.fnContainer()).insertBefore('div.dataTables_wrapper');
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				/*
				var enabled = data_table.row(this).data();
				if(enabled[6] == "Enabled"){
					$('#disable_btn').text("Disable");
				}else{
					$('#disable_btn').text("Enable");
				}
				*/
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			/*
			$('#disable_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				
				$.ajax({
					url: "/admin/ajax/datatables/receipt_messages_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			*/			
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/receipt_messages_buttons.php",
					type: "POST",
					data: {
						action : 'delete',
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				
				$('#modal_row_id').html("ID #"+id);
				$('#name').val(columns['name']);
				$('#message').val(columns['message']);
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					// focus on first input element
					$('#name').focus();
				}, 500);                        
			})
			
// data validation for edit modal
			$(document).on('click', '#submit_edit', function() {
				$('#edit_row_modal').modal('hide');
				
				var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
				
				// deteremine whether this is update or insert
				if(id > 0){
					var action = "update";
				}else{
					var action = "insert";
				}
		
				$.ajax({
					url: "/admin/ajax/datatables/receipt_messages_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id,
						name : $('#name').val(),
						message : $('#message').val()
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$(document).on('click', '#submit_mass_update', function() {
				$('#mass_update_modal').modal('hide');
		
				$.ajax({
					url: "/admin/ajax/datatables/receipt_messages_buttons.php",
					type: "POST",
					data: {
						action : "mass_update",
						island_id : $('#island').val(),
						name_id : $('#update_name').val()
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#add_btn').click(function(){				
				// clear elements
				$('#modal_row_id').html("New");
				$('#name').val("");
				$('#message').val("");
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			$('#mass_update_btn').click(function(){
				// show modal
				$('#mass_update_modal').modal('show');
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Receipt Messages</h3>
					<div class="btn-group pull-right">
						<a id="mass_update_btn" href="#" class="btn btn-primary btn-sm">Mass Update</a>
					</div>
					
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<!-- <a id="disable_btn" class="btn btn-default btn-sm btn-row-action">Disable</a> -->
						<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Delete</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Name</th>
								<th class="text-left">Message</th>
								<th class="text-left">Creator</th>
								<th class="text-left">Creation</th> 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Receipt Message</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="name">Name</label>
								<input type="text" class="form-control" id="name">
							</div>
						</div>
					
						<div class="col-sm-12">
							<div class="form-group">
								<label for="message">Message</label>
								<textarea rows="8" class="form-control" id="message"></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_edit">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Modal window for mass update -->
	<div class="modal fade" id="mass_update_modal" tabindex="-1" role="dialog" aria-labelledby="mass_update_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="mass_update_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Mass Update</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="update_name">Name</label>
								<select type="text" class="form-control" id="update_name">
									<?php
										$messages = $db->query("SELECT name, id FROM receipt_messages ORDER BY name");
										foreach($messages as $message){
											echo "<option value=".$message['id'].">".$message['name']."</option>";
										}
									?>
								</select>
							</div>
						</div>
					
						<div class="col-sm-12">
							<div class="form-group">
								<label for="island">Island</label>
								<select type="text" class="form-control" id="island">
									<?php
										$islands = $db->query("SELECT name, id FROM island ORDER BY name");
										foreach($islands as $island){
											echo "<option value=".$island['id'].">".$island['name']."</option>";
										}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="submit_mass_update">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
			</div>
			<div class="modal-body">
				Are you sure you would like to remove this?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
			</div>
		</div>
	</div>
</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 