<?php
	$page_title = "File Integrity Checks";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', time()); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d 23:59:59', time()); ?>"));
			$("#status").val('<?php echo $_GET['status']; ?>');
			
			// main scheme level datatable initialization
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/file_checks.php?status="+$('#status').val()+"&filter_date_from=<?php echo date('Y-m-d', time()); ?>",
				"columns": [
					{ "data": "datetime_checked" },
					{ "data": "path" },
					{ "data": "size" },
					{ "data": "modified_on" },
					{ "data": "hash" },
					{ "data": "status" }
				],
				"order": [[0, 'desc'], [1, 'asc']]
			});
			
			$(document).on('click', '#force_check_btn', function() {
				$('#force_check_btn').html = "Please wait...";
				$('#force_check_btn').prop('disabled', true);
				
				$.ajax({
					url: "/admin/ajax/datatables/file_checks_buttons.php",
					type: "POST",
					dataType: "json",
					success: function(response){
						if(response.approved_count > 0 && response.new_count == 0 && response.modified_count == 0 && response.deleted_count == 0){
							bootbox.alert("Check completed.  All files are the approved versions");
						}else{
							bootbox.alert("Check failed.  Found "+response.new_count+" new files, "+response.modified_count+" modified files and "+response.deleted_count+" deleted files.");
						}
						$('#force_check_btn').html = "Force Check";
						$('#force_check_btn').prop('disabled', false);
						
						// hide buttons and reload table if successful
						$('#add_row_modal').modal('hide');
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
						$('#force_check_btn').html = "Force Check";
						$('#force_check_btn').prop('disabled', false);
					}
				});
			});
			
			$("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}

				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					bInfo: false,
					"columns": [
						{ "data": "datetime_checked" },
						{ "data": "path" },
						{ "data": "size" },
						{ "data": "modified_on" },
						{ "data": "hash" },
						{ "data": "status" }
					],
					"order": [[0, 'desc'], [1, 'asc']],
					"ajax": "/admin/ajax/datatables/file_checks.php?status="+$('#status').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					bInfo: false,
					"columns": [
						{ "data": "datetime_checked" },
						{ "data": "path" },
						{ "data": "size" },
						{ "data": "modified_on" },
						{ "data": "hash" },
						{ "data": "status" }
					],
					"order": [[0, 'desc'], [1, 'asc']],
					"ajax": "/admin/ajax/datatables/file_checks.php?status="+$('#status').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});
			
			// status change
			$("#status").on('change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}

				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					bInfo: false,
					"columns": [
						{ "data": "datetime_checked" },
						{ "data": "path" },
						{ "data": "size" },
						{ "data": "modified_on" },
						{ "data": "hash" },
						{ "data": "status" }
					],
					"order": [[0, 'desc'], [1, 'asc']],
					"ajax": "/admin/ajax/datatables/file_checks.php?status="+$('#status').val()+"&filter_date_from="+start_date+"&filter_date_to="+end_date
				});
				
				data_table.draw(true);
			});
		});
	</script>
	
	<style>
		#datatable tbody tr {
			-moz-user-select: all;
			-khtml-user-select: all;
			-webkit-user-select: all;
			-o-user-select: all;
			user-select: all;
		}
	</style>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>

					<div class="btn-group pull-right">
						<a id="force_check_btn" href="#" class="btn btn-primary btn-sm">Force Check</a>
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="house" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Status</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="status">
									<option value="">All Statuses</option>
									<?php
										$statuses = $db->query("SELECT DISTINCT `status` FROM `file_hash_checks` ORDER BY `status` ASC");
										foreach($statuses as $status){
											echo "<option value=".$status['status'].">".ucwords($status['status'])."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
							
						</div>
					</form>
				
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Date Checked</th>
								<th class="text-left">File Name</th>
								<th class="text-left">Size</th>
								<th class="text-left">Last Modified</th>
								<th class="text-left">Hash</th>
								<th class="text-left">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
