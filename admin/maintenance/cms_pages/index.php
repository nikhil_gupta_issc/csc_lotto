<?php
	$page_title = "CMS Pages Setting";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');	
?>
<script type="text/javascript" src="/lib/assets/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">	
		$(document).ready(function(){
		
		CKEDITOR.replace("text");

			$.fn.modal.Constructor.prototype.enforceFocus = function () {
			    var $modalElement = this.$element;
			    $(document).on('focusin.modal', function (e) {
			        var $parent = $(e.target.parentNode);
			        if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length
			            // add whatever conditions you need here:
			            &&
			            !$parent.hasClass('cke_dialog_ui_input_select') && !$parent.hasClass('cke_dialog_ui_input_text')) {
			            $modalElement.focus()
			        }
			    })
			};	
					
			// populate customer select box
			$.ajax({
				url: "/admin/ajax/get_pages.php",
				type: "POST",
				dataType: "json",
				success: function(data){
					$("#page").html(data.options);
					$("#page").val('<?php echo isset($_GET['page']) ? $_GET['page'] : -1; ?>');
				}
			});	
			
			$('#save_changes').click(function(){
			var text = CKEDITOR.instances['text'].getData();
			var formData = new FormData();
			formData.append('image', $('input[type=file]')[0].files[0]); 
			formData.append('id', $("#page").val());
			formData.append('title', $("#title").val());
			formData.append('page_title', $("#page_title").val());
			formData.append('content', text);
			formData.append('action', 'update');
			
			var text = CKEDITOR.instances['text'].getData();
				  
   				 				
    				                            
  				  $.ajax({
					url: '/admin/ajax/get_pages.php', // point to server-side PHP script 
					dataType: 'json',  // what to expect back from the PHP script, if anything
					cache: false,
					contentType: false,
					processData: false,
					data: formData,                         
					type: 'post',
					success: function(response){
					console.log(response);
					
						if(response.success != 'true'){
							bootbox.alert("Operation Failed!\n"+response);
						}else{
							bootbox.alert("Saved!");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
    				 });
			
			
			});				
		});
	</script>
<script>
function getState(val) {
		$.ajax({
			type: "POST",
			url: "/admin/ajax/get_pages.php",
			dataType: 'json',
			data:{ action: 'info',
				id: val},
				
			success: function(data){
			
				$("#info").show();
				$('#title').val(data[0].title);
				$('#page_title').val(data[0].page_title);
				CKEDITOR.instances['text'].setData(data[0].content);
				//$("#file").val(data[0].image_path);
				$("#img").attr("src", '<?php echo ROOTPATH."/images/"?>'+data[0].image_path);
				//$("#state-list").html(data);
			}
		});
}
</script>	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">CMS Pages Setting</h3>
				</div>
			<div class="panel-body">
				<div class="col-sm-12">
					<div class="form-group">
						<div class="col-sm-3"><label for="name">Select Page</label></div>
						<div class="col-sm-6"><select type="text" class="form-control" id="page" onChange="getState(this.value);">
									<option value="-1">Select Page</option>
									<option value="-2">Loading... Please Wait.</option>
								       </select></div>
					</div>
				</div>
				<br><br>
				<form id="cms_form" name="cms_form">
				<hidden id="update" name="update">
				<div class="col-sm-12" id="info" style="display:none">
					<div class="form-group">
						<div class="col-sm-3"><label for="title">Title</label></div>
						<div class="col-sm-6"><input class="form-control" type="text" name="title" id="title"></div>
					</div>
					<br><br>
					<div class="form-group">
						<div class="col-sm-3"><label for="title">Page Title</label></div>
						<div class="col-sm-6"><input class="form-control" type="text" name="page_title" id="page_title"></div>
					</div>
					<br><br>
					<div class="form-group">
						<div class="col-sm-3"><label for="title">Upload Image</label></div>
						<div class="col-sm-6"><input class="form-control" type="file" name="file" id="file"></div>
						<div class="col-sm-3"><img src="" id="img" height="50px" width="50px"></div>
					</div>							
					<br><br>
					<div class="form-group">
						<div class="col-sm-3"><label for="title">Update Content</label></div>
						<div class="col-sm-6"><textarea id="text" type='text' class='form-control' name='text' rows='6'></textarea></div>
					</div>				
				</div>
				</form>	
			</div>
				
				<div class="panel-footer">
					<button type="button" class="btn btn-primary" id="save_changes">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
