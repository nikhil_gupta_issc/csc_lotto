<?php
	$page_title = "A Dollar Report";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">A Dollar Report</h3>
					
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-center">Amount</th>
								<th class="text-center">Redeemed</th>
								<th class="text-center">Remaining</th>
								<th class="text-center">Available</th>					
							</tr>
						</thead>
						<tbody>
						<?php
				$q = "SELECT adollar_value, count(adollar_value) as avilable FROM adollar group by adollar_value order by adollar_value desc";
				$result = $db->query($q);
				
				
				foreach($result as $value){
				
				$q= "SELECT count(card_number) as redeemed FROM adollar_redemptions where amount=".$value['adollar_value'];
				$result_count =  $db->query($q);
				
				foreach($result_count as $red){
				
				?>
							<tr>
								<td class="text-center"><?php echo $value['adollar_value']; ?></td>
								<td class="text-center"><?php echo $red['redeemed']; ?></td>
								<td class="text-center"><?php echo $value['avilable']-$red['redeemed']; ?></td>
								<td class="text-center"><?php echo $value['avilable']; ?></td>
							</tr>
							<?php }}?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
