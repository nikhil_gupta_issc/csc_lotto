<?php
	$page_title = "Rapidballs Settings";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// get current settings from db
	$settings = array();
	$q = "SELECT * FROM `settings`";
	$settings_info = $db->query($q);
	foreach($settings_info as $setting){
		$settings[$setting['setting']] = $setting['value'];
	}
?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#save_changes').click(function(){
				var rapidballs_enabled = 0;
				if(!$('#rapidballs_enabled').closest(".toggle").hasClass("off")){
					rapidballs_enabled = 1;
				}
				
				$.ajax({
					url: "/admin/ajax/rapidballs_settings.php",
					type: "POST",
					dataType : "json",
					data: {
						rapidballs_enabled : rapidballs_enabled
					},
					success: function(response){
						if(response.success != 'true'){
							bootbox.alert("Operation Failed!\n"+response);
						}else{
							bootbox.alert("Saved!");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
				});
			});
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Rapidballs Settings</h3>
				</div>
				<div class="panel-body">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="mailer_from_email">Rapidballs Enabled</label>
							<div id="has_geolocation_container">
								<input id="rapidballs_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $settings['rapidballs_enabled'] == 1 ? "checked" : ""; ?> >
							</div>
						</div>
						<hr>
					</div>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-primary" id="save_changes">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
