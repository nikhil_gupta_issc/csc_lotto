<?php
	$page_title = "Security Groups";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>

	<script type="text/javascript">
		$(document).ready(function(){
			// radio button toggles
			$('#allow_all').click(function(){
				$('.allow_opt').prop('checked', true);
			});
			$('#default_all').click(function(){
				$('.default_opt').prop('checked', true);
			});
			$('#deny_all').click(function(){
				$('.deny_opt').prop('checked', true);
			});
			
			// toggle show input box instead of select
			$('#add_group_btn').click(function(){
				if($('#add_group_btn').html() == "Add Group"){
					$('#group_name_input').show();
					$('#sec_group_id').hide();
					$('#cancel_add_group_btn').show();
					$('#add_group_btn').html("Save");
					
					// clear out description and members
					$('#group_name_input').val("");
					$('#group_description').val("");
					$("#group_description").prop('disabled', false);
					$("#members").prop('disabled', false);
					$('#members option:selected').removeAttr("selected").trigger('chosen:updated');
				}else if($('#add_group_btn').html() == "Save"){
					// add new group
					$.ajax({
						url: "/admin/ajax/sec_groups.php",
						type: "POST",
						data: {
							action : 'add',
							group_name : $('#group_name_input').val(),
							group_description : $('#group_description').val()
						},
						dataType: "json",
						success: function(response){
							if(response.success != false){
								// add group and select it
								if(response.new_id > 0){
									$('#sec_group_id').append($("<option/>", {
										value: response.new_id,
										text: $('#group_name_input').val()
									})).val(response.new_id);
									// clear group name input
									$('#group_name_input').val("");
									$('#group_description').val("");
								}
							}else{
								bootbox.alert("Operation Failed!\n\n"+response.query);
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							//bootbox.alert("Operation Failed!\n\n"+errorThrown);
						}
					});
					
					$('#group_name_input').hide();
					$('#sec_group_id').show();
					$('#cancel_add_group_btn').hide();
					$('#add_group_btn').html("Add Group");
				}
			});
			
			$('#cancel_add_group_btn').click(function(){				
				// clear out description and members
				$('#sec_group_id').val("");
				$('#group_name_input').val("");
				$('#group_description').val("");
				$("#group_description").prop('disabled', false);
				$("#members").prop('disabled', false);
				$('#members option:selected').removeAttr("selected").trigger('chosen:updated');
				
				$('#group_name_input').hide();
				$('#sec_group_id').show();
				$('#add_group_btn').html("Add Group");
				$('#cancel_add_group_btn').hide();
			});
			
			$(document).on("change", "#sec_group_id", function(){
				// load group members on group select change
				$.ajax({
					url: "/admin/ajax/sec_groups.php",
					type: "POST",
					data: {
						action : 'get_members',
						group_id : $('#sec_group_id').val()
					},
					dataType: "json",
					success: function(response){
						if(response.is_system == true){
							$("#group_description").prop('disabled', true);
							$("#members").prop('disabled', true);
						}else{
							$("#group_description").prop('disabled', false);
							$("#members").prop('disabled', false);
						}
						
						$("#members").val(response.members).trigger("chosen:updated");
						
						// set group description
						$("#group_description").val(response.description);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						//bootbox.alert("Operation Failed!\n\n"+errorThrown);
					}
				});
				
				// load page permissions on group select change
				$.ajax({
					url: "/admin/ajax/sec_groups.php",
					type: "POST",
					data: {
						action : 'get_permissions',
						group_id : $('#sec_group_id').val()
					},
					dataType: "json",
					success: function(response){
						if(response.success != false){
							// set everything to default
							$('.default_opt').prop('checked', true);
							$('.default_opt').prop('disabled', false);
							$('.allow_opt').prop('disabled', false);
							$('.deny_opt').prop('disabled', false);
							
							if(response.permissions){
								// disable radio boxes and set to allow if super users group
								if($('#sec_group_id').val() == 4){
									$('.allow_opt').prop('checked', true);
									$('.default_opt').prop('disabled', true);
									$('.allow_opt').prop('disabled', true);
									$('.deny_opt').prop('disabled', true);
									
								}else{
									$.each(response.permissions, function(action_id, perm_level){
										// force allow for Super Users
										if($('#sec_group_id').val() == 4){
											perm_level = 1;
										}
										
										if(perm_level == 1){
											$('#allow_'+action_id).prop('checked', true);
										}else if(perm_level == -1){
											$('#deny_'+action_id).prop('checked', true);
										}
									});
								}
							}
						}else{
							bootbox.alert("Operation Failed!\n\n"+response.query);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						//bootbox.alert("Operation Failed!\n\n"+errorThrown);
					}
				});
			});
			
			$('#remove_group_btn').click(function(){				
				$.ajax({
					url: "/admin/ajax/sec_groups.php",
					type: "POST",
					data: {
						action : 'remove',
						group_id : $('#sec_group_id').val()
					},
					dataType: "json",
					success: function(response){
						bootbox.alert("Removed");
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						//bootbox.alert("Operation Failed!\n\n"+errorThrown);
					}
				});
			});
			
			$('#save_changes_btn').click(function(){
				// build an array of the permissions selected
				var perm_array = [];
				var id = -1;
				$.each($("input:radio:checked"), function(dom_id, obj){
					id = obj.name.replace("perm_","");
					perm_array[id] = obj.value;
				});
				
				$.ajax({
					url: "/admin/ajax/sec_groups.php",
					type: "POST",
					data: {
						action : 'update',
						group_id : $('#sec_group_id').val(),
						group_name : $('#group_name_input').val(),
						group_description : $("#group_description").val(),
						members : $("#members").chosen().val(),
						permissions : perm_array
					},
					dataType: "json",
					success: function(response){
						bootbox.alert("Saved!");
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						//bootbox.alert("Operation Failed!\n\n"+errorThrown);
					}
				});
			});
			
			// trigger select change to load data
			$('#sec_group_id').trigger("change");
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Security Groups</h3>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 col-md-2 control-label" for="sec_group_id">Group Name</label>
							<div class="col-sm-5 col-md-7">
								<select id="sec_group_id" class="form-control">
									<?php
										$group_names = $db->query("SELECT `id`, `name` FROM `sec_groups` WHERE `is_deleted`='0' ORDER BY `name` ASC");
										foreach($group_names as $group_name){
											if(isset($_GET['group']) && $_GET['group'] == $group_name['id']){
												echo "<option value=".$group_name['id']." selected>".$group_name['name']."</option>";
											}else{
												echo "<option value=".$group_name['id'].">".$group_name['name']."</option>";
											}
										}
									?>
								</select>
								<input id="group_name_input" type="text" class="form-control" style="display:none;"></input>
							</div>
							<div class="col-sm-2 col-md-3">
								<button id="add_group_btn" type="button" class="btn btn-primary pull-left">Add Group</button>
								<button id="remove_group_btn" type="button" class="btn btn-danger pull-left" style="margin-left: 18px;">Remove</button>
								<button style="display:none; margin-left: 18px;" id="cancel_add_group_btn" type="button" class="btn btn-danger">Cancel</button>
							</div>
						</div>
						<div class="form-group">
							<label for="group_description" class="control-label col-sm-3 col-md-2">Group Description</label>
							<div class="col-sm-9 col-md-10">
								<textarea class="form-control" id="group_description"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="members" class="control-label col-sm-3 col-md-2">Group Members</label>
							<div class="col-sm-9 col-md-10">
								<style>
									.chosen-container {
										width: 100% !important;
									}
								</style>
								<select id="members" multiple class="form-control chosen-select" data-placeholder="Select Members">
									<?php
										$user_names = $db->query("SELECT `firstname`, `lastname`, `id` FROM `users` WHERE `deleted`='0' ORDER BY `lastname` ASC, `firstname` ASC");
										foreach($user_names as $user_name){
											echo "<option value=".$user_name['id'].">".$user_name['lastname'].", ".$user_name['firstname']." (".$user_name['id'].")</option>";
										}
									?>
								</select>
							</div>
						</div>
					</form>
					
					<div class="panel panel-default">
						<div class="panel-heading clearfix">
							<h4 class="panel-title">Permissions</h4>
							
							<div class="pull-right" style="margin-top: -18px; padding-right: 6px;">
								<button id="allow_all" class="btn btn-success btn-sm">Allow All</button>
								<button id="default_all" class="btn btn-default btn-sm">Default All</button>
								<button id="deny_all" class="btn btn-danger btn-sm">Deny All</button>
							</div>
						</div>
						<div class="panel-body clearfix">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Permission Category</th>
											<th style='text-align: center;'>Allow</th>
											<th style='text-align: center;'>Default</th>
											<th style='text-align: center;'>Deny</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if(isset($_GET['group'])){
												$q = "SELECT `cat`.`name`, `cat`.`id`, `p`.`path`, `g`.`permission_level_id` FROM `sec_pages` p JOIN `sec_categories` cat ON `p`.`category_id`=`cat`.`id` JOIN `sec_group_permissions` AS `g` ON `g`.`action_id` = `cat`.`id` WHERE `p`.`is_deleted`='0' AND `g`.`group_id` = '".$_GET['group']."'";
												$categories = $db->query($q);
												$group_permissions = $db->query("SELECT * FROM `sec_group_permissions` WHERE `group_id`='".$_GET['group']."'");
												foreach ($group_permissions as $perm){
													$perm_lookup[$perm['action_id']] = $perm['permission_level_id'];
												}
												print_r($perm_lookup);
											}else{
												//$q = "SELECT `p`.`name`, `pa`.`id`, `p`.`path`, `pa`.`action_name` FROM `sec_pages` p JOIN `sec_page_actions` pa ON `p`.`id`=`pa`.`page_id` WHERE `p`.`is_deleted`='0' ORDER BY `p`.`name` ASC, `p`.`path` ASC";
												$q = "SELECT * FROM `sec_categories` ORDER BY `name` ASC";
												$categories = $db->query($q);
											}
											foreach($categories as $cat){
												echo "<tr>";
												
												// show page name or path
												echo "<td style='overflow:hidden;'>";
												echo $cat["name"];
												echo "</td>";
												
												if(isset($perm_lookup[$cat['id']]) && $perm_lookup[$cat['id']] == "1"){
													echo "<td style='text-align: center;'><input type='radio' class='opt allow_opt' name='perm_".$cat['id']."' id='allow_".$cat['id']."' value='1' checked></td>";
												}else{
													echo "<td style='text-align: center;'><input type='radio' class='opt allow_opt' name='perm_".$cat['id']."' id='allow_".$cat['id']."' value='1'></td>";
												}
												if(!isset($perm_lookup[$cat['id']]) || $perm_lookup[$cat['id']] == "0"){
													echo "<td style='text-align: center;'><input type='radio' class='opt default_opt' name='perm_".$cat['id']."' id='default_".$cat['id']."' value='0' checked></td>";
												}else{
													echo "<td style='text-align: center;'><input type='radio' class='opt default_opt' name='perm_".$cat['id']."' id='default_".$cat['id']."' value='0'></td>";
												}
												if(isset($perm_lookup[$cat['id']]) && $perm_lookup[$cat['id']] == "-1"){
													echo "<td style='text-align: center;'><input type='radio' class='opt deny_opt' name='perm_".$cat['id']."' id='deny_".$cat['id']."' value='-1' checked></td>";
												}else{
													echo "<td style='text-align: center;'><input type='radio' class='opt deny_opt' name='perm_".$cat['id']."' id='deny_".$cat['id']."' value='-1'></td>";
												}
												echo "</tr>";
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer clearfix">
					<button id="save_changes_btn" type="button" class="btn btn-primary pull-right">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
	
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
