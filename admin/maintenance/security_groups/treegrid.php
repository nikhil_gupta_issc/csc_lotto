<!DOCTYPE html>
<html ng-app="treeGridTest">
	<head>
		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap.min.css" media="screen">
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap-buttons.min.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/modern-business.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/tooltips.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/tree-grid-directive/src/treeGrid.css">    
		<link rel="stylesheet" type="text/css" href="/lib/assets/font-awesome-4.2.0/css/font-awesome.min.css" />
	</head>
	<body ng-controller="treeGridController" style="margin:20px">
		<div ng-model="showMinDir" ng-value="false">
			<div ng-show="!showMinDir">
				<button ng-click="my_tree.expand_all()" class="btn btn-default btn-sm">Expand All</button>
				<button ng-click="my_tree.collapse_all()" class="btn btn-default btn-sm">Collapse All</button>
				<input class="input-sm pull-right" type="text" data-ng-model="filterString"	placeholder="Filter" />
				
				<tree-grid tree-data="tree_data" tree-control="my_tree" col-defs="col_defs" expand-on="expanding_property" on-select="my_tree_handler(branch)" expand-level="2" ></tree-grid>
			</div>
		</div>
		
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery/jquery-1.11.0.js"></script>
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery-ui-1.11.4/jquery-ui.min.js"></script> 
		<script src="/lib/assets/angularjs/angular.min.js"></script>
		<script src="/lib/assets/tree-grid-directive/src/tree-grid-directive.js"></script>    
		<script src="treegrid.js"></script>
	</body>
</html>