<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap.min.css" media="screen">
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap-buttons.min.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/modern-business.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/tooltips.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/tree-grid-directive/src/treeGrid.css">    
		<link rel="stylesheet" type="text/css" href="/lib/assets/font-awesome-4.2.0/css/font-awesome.min.css" />
		
        <link rel="stylesheet" href="/lib/assets/jquery-treegrid/css/jquery.treegrid.css">
        
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery/jquery-1.11.0.js"></script>	
        <script type="text/javascript" src="/lib/assets/jquery-treegrid/js/jquery.treegrid.js"></script>
        <script type="text/javascript" src="/lib/assets/jquery-treegrid/js/jquery.treegrid.bootstrap3.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.tree-grid').treegrid();
            });
        </script>
    </head>

    <body>
        <div class="container">
            <h1><a href="http://maxazan.github.io/jquery-treegrid/">TreeGrid</a> Bootstrap 3 examples</h1>

            <table class="table tree-grid table-bordered table-striped table-condensed">
                <tr class="treegrid-1">
                    <td>Root node 1</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-2 treegrid-parent-1">
                    <td>Node 1-1</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-3 treegrid-parent-1">
                    <td>Node 1-2</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-4 treegrid-parent-3">
                    <td>Node 1-2-1</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-5">
                    <td>Root node 2</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-6 treegrid-parent-5">
                    <td>Node 2-1</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-7 treegrid-parent-5">
                    <td>Node 2-2</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-8 treegrid-parent-7">
                    <td>Node 2-2-1</td><td>Additional info</td>
                </tr>        
            </table>
		
			<div class="panel panel-default">
				<div class="table-responsive">
					<table class="table tree-grid table-bordered table-striped table-condensed">
						<thead>
							<tr>
								<th>Page</th>
								<th>Action</th>
								<th style='text-align: center;'>Allow</th>
								<th style='text-align: center;'>Default</th>
								<th style='text-align: center;'>Deny</th>
							</tr>
						</thead>
						<tbody>
							<tr class="treegrid-0">
								<td>Administrative Back Office</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						
							<?php
								if(isset($_GET['group'])){
									$page_actions = $db->query("SELECT `p`.`name`, `pa`.`id`, `p`.`path`, `pa`.`action_name`, `g`.`permission_level_id` FROM `sec_pages` p JOIN `sec_page_actions` pa ON `p`.`id`=`pa`.`page_id` JOIN `sec_group_permissions` AS `g` ON `g`.`action_id` = `pa`.`id` WHERE `p`.`is_deleted`='0' AND `g`.`group_id` = '".$_GET['group']."'");
									$group_permissions = $db->query("SELECT * FROM `sec_group_permissions` WHERE `group_id`='".$_GET['group']."'");
									foreach ($group_permissions as $perm){
										$perm_lookup[$perm['action_id']] = $perm['permission_level_id'];
									}
									print_r($perm_lookup);
								}else{
									$page_actions = $db->query("SELECT `p`.`name`, `pa`.`id`, `p`.`path`, `pa`.`action_name` FROM `sec_pages` p JOIN `sec_page_actions` pa ON `p`.`id`=`pa`.`page_id` WHERE `p`.`is_deleted`='0' ORDER BY `p`.`name` ASC, `p`.`path` ASC");
								}
								foreach($page_actions as $page_action){
									echo "<tr class='treegrid-".$page_action['id']." treegrid-parent-0'>";
									echo "<td style='overflow:hidden;'>";
									if($page_action['name'] == ""){
										echo $page_action['path'];
									}else{
										echo $page_action["name"];
									}
									echo "</td>";
									echo "<td>".$page_action["action_name"]."</td>";
									if(isset($perm_lookup[$page_action['id']]) && $perm_lookup[$page_action['id']] == "1"){
										echo "<td style='text-align: center;'><input type='radio' class='opt allow_opt' name='perm_".$page_action['id']."' id='allow_".$page_action['id']."' value='1' checked></td>";
									}else{
										echo "<td style='text-align: center;'><input type='radio' class='opt allow_opt' name='perm_".$page_action['id']."' id='allow_".$page_action['id']."' value='1'></td>";
									}
									if(!isset($perm_lookup[$page_action['id']]) || $perm_lookup[$page_action['id']] == "0"){
										echo "<td style='text-align: center;'><input type='radio' class='opt default_opt' name='perm_".$page_action['id']."' id='default_".$page_action['id']."' value='0' checked></td>";
									}else{
										echo "<td style='text-align: center;'><input type='radio' class='opt default_opt' name='perm_".$page_action['id']."' id='default_".$page_action['id']."' value='0'></td>";
									}
									if(isset($perm_lookup[$page_action['id']]) && $perm_lookup[$page_action['id']] == "-1"){
										echo "<td style='text-align: center;'><input type='radio' class='opt deny_opt' name='perm_".$page_action['id']."' id='deny_".$page_action['id']."' value='-1' checked></td>";
									}else{
										echo "<td style='text-align: center;'><input type='radio' class='opt deny_opt' name='perm_".$page_action['id']."' id='deny_".$page_action['id']."' value='-1'></td>";
									}
									echo "</tr>";
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </body>
</html>
