<?php
	$page_title = "Rebate Settings";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// get current settings from db
	$settings = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'loss_calculation_enabled'";
	$settings_info = $db->queryOneRow($q);
	if(!empty($settings_info)){
           $loss_calculation_enabled = $settings_info['value'];
        }
        
        
        // get current settings from db
	$settings = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'lotto_loss_enabled'";
	$settings_info = $db->queryOneRow($q);
	if(!empty($settings_info)){
           $lotto_loss_enabled = $settings_info['value'];
        }
        	
	// get current settings from db
	$enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_loss_enabled'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $casino_loss_enabled = $enable_info['value'];
        }
        
        // get current settings from db
	$enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'rapidball_loss_enabled'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $rapidball_loss_enabled = $enable_info['value'];
        }
        
        // get current settings from db
	$enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'sports_loss_enabled'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $sports_loss_enabled = $enable_info['value'];
        }
        
        // get current settings from db
	$enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'lotto_loss_percentage'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $lotto_loss_percentage = $enable_info['value'];
        }
        
        $enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_loss_percentage'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $casino_loss_percentage = $enable_info['value'];
        }
        
        $enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'rapidball_loss_percentage'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $rapidball_loss_percentage = $enable_info['value'];
        }
        
        $enable = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'sports_loss_percentage'";
	$enable_info = $db->queryOneRow($q);
	if(!empty($enable_info)){
           $sports_loss_percentage = $enable_info['value'];
        }
?>

	<script type="text/javascript">
		$(document).ready(function(){
			
			$('#save_changes').click(function(){
				
				var loss_calculation_enabled = 0;
				if(!$('#loss_calculation_enabled').closest(".toggle").hasClass("off")){
					loss_calculation_enabled = 1;
				}
				
				var lotto_loss_enabled = 0;
				if(!$('#lotto_loss_enabled').closest(".toggle").hasClass("off")){
					lotto_loss_enabled = 1;
				}
				
				var casino_loss_enabled = 0;
				if(!$('#casino_loss_enabled').closest(".toggle").hasClass("off")){
					casino_loss_enabled = 1;
				}
				
				var rapidball_loss_enabled = 0;
				if(!$('#rapidball_loss_enabled').closest(".toggle").hasClass("off")){
					rapidball_loss_enabled = 1;
				}
				
				var sports_loss_enabled = 0;
				if(!$('#sports_loss_enabled').closest(".toggle").hasClass("off")){
					sports_loss_enabled = 1;
				}			
				
				if($('#lotto_loss_percentage').val() == ""){
					var lotto_loss_percentage = 0;
				}
				else {
					var lotto_loss_percentage = $('#lotto_loss_percentage').val();
				}
				
				if($('#casino_loss_percentage').val() == ""){
					var casino_loss_percentage = 0;
				}
				else {
					var casino_loss_percentage = $('#casino_loss_percentage').val();
				}
				
				if($('#rapidball_loss_percentage').val() == ""){
					var rapidball_loss_percentage = 0;
				}
				else {
					var rapidball_loss_percentage = $('#rapidball_loss_percentage').val();
				}
				
				if($('#sports_loss_percentage').val() == ""){
					var sports_loss_percentage = 0;
				}
				else {
					var sports_loss_percentage = $('#sports_loss_percentage').val();
				}
				
				
				
				
				
								
				$.ajax({
					url: "/admin/ajax/loss_calculation_settings.php",
					type: "POST",
					dataType : "json",
					data: {
						loss_calculation_enabled : loss_calculation_enabled,
						lotto_loss_enabled : lotto_loss_enabled,
						casino_loss_enabled : casino_loss_enabled,
						rapidball_loss_enabled : rapidball_loss_enabled,
						sports_loss_enabled : sports_loss_enabled,
						lotto_loss_percentage : lotto_loss_percentage,
						casino_loss_percentage : casino_loss_percentage,
						rapidball_loss_percentage : rapidball_loss_percentage,
						sports_loss_percentage : sports_loss_percentage
						
					},
					success: function(response){
						if(response.success != 'true'){
							bootbox.alert("Operation Failed!\n"+response);
						}else{
							bootbox.alert("Saved!");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
				});
			});
			
			$('.calculation').click(function(){
			$('#loading-spinner').show();
				var name = $(this).attr("id");
			
				if($('#'+name+'_percentage').val() == ""){
					var percentage = 0;
				}
				else {
					var percentage = $('#'+name+'_percentage').val();
				}
					
				
				
				$.ajax({
					url: "/admin/ajax/rebate_calculation.php",				
					type: "POST",
					dataType : "json",
					data: {
						action : name,
						percentage : percentage						
					},
					success: function(response){
						if(response.success != 'true'){
							bootbox.alert("Operation Failed!\n"+response);
						}else{
							$('#loading-spinner').hide();
							$('#'+name+'_total').val(response.total_lotto_amount);
							$('#'+name+'_cust_total').val(response.number_of_users);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
				});
							
			});
			
		});
	</script>
	
	<div class="container-fluid">
	<div id="loading-spinner"><div></div></div>
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Rebate Settings</h3>
				</div>
				<div class="panel-body">
					<div class="col-sm-4">
						<div class="form-group">
							<label for="loss_calculation_enabled">Rebate Enabled</label>
							<div id="loss_calculation_container">
								<input id="loss_calculation_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $loss_calculation_enabled == 1 ? "checked" : ""; ?> >
							</div>
						</div>
						<hr>
					</div>
				</div>
				<div class="panel-body">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="lotto_loss_enabled">Lotto</label>
                                                        <div class="input-group">
								<input id="lotto_loss_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $lotto_loss_enabled == '1' ? "checked" : ""; ?> >
</div>
							
						</div>
						<hr>
						</div>
				<div class="col-sm-3">
					<div class="form-group">
							<label for="casino_loss_enabled">Casino</label>
<div class="input-group">
								<input id="casino_loss_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $casino_loss_enabled == '1' ? "checked" : ""; ?> >
</div>
							
						</div>
						<hr>
						</div>
					<div class="col-sm-3">
					<div class="form-group">
							<label for="rapidball_loss_enabled">Rapidball</label>
<div class="input-group">
								<input id="rapidball_loss_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $rapidball_loss_enabled == '1' ? "checked" : ""; ?> >
</div>
							
						</div>
						<hr>
						</div>	
						<div class="col-sm-3">
					<div class="form-group">
							<label for="sports_loss_enabled">Sports</label>
<div class="input-group">
								<input id="sports_loss_enabled" data-toggle="toggle" data-on="Yes" data-off="No" type="checkbox" <?php echo $sports_loss_enabled == '1' ? "checked" : ""; ?> >
</div>
							
						</div>
						<hr>
						</div>
				</div>
		<div class="panel-body">
			<div class="col-sm-12">	
				<div class="form-group">	
					<label class="col-sm-3 control-label" for="lotto_loss_percentage">Lotto %</label>
					<div class="col-sm-4">
						<input type="text" id="lotto_loss_percentage" class="form-control" value="<?php echo $lotto_loss_percentage;?>">
					</div>
					<div class="col-sm-2"><input class="form-control" type="text" name="lotto_loss_total" id="lotto_loss_total" readonly></div>
					<div class="col-sm-2"><input class="form-control" type="text" name="lotto_loss_cust_total" id="lotto_loss_cust_total" readonly></div>
					<div class="col-sm-1">
						<i class="fa fa-calculator calculation" id="lotto_loss"></i>
					</div>
				</div>
				<br><br>
			</div>
				
			<div class="col-sm-12">	
				<div class="form-group">	
					<label class="col-sm-3 control-label" for="casino_loss_percentage">Casino %</label>
					<div class="col-sm-4">
						<input type="text" id="casino_loss_percentage" class="form-control" value="<?php echo $casino_loss_percentage;?>">
					</div>
					<div class="col-sm-2"><input class="form-control" type="text" name="casino_loss_total" id="casino_loss_total" readonly></div>
					<div class="col-sm-2"><input class="form-control" type="text" name="casino_loss_cust_total" id="casino_loss_cust_total" readonly></div>
					<div class="col-sm-1">
						<i class="fa fa-calculator calculation" id="casino_loss"></i>
					</div>
				</div>
				<br><br>
			</div>
			
			<div class="col-sm-12">	
				<div class="form-group">	
					<label class="col-sm-3 control-label" for="rapidball_loss_percentage">Rapidball %</label>
					<div class="col-sm-4">
						<input type="text" id="rapidball_loss_percentage" class="form-control" value="<?php echo $rapidball_loss_percentage;?>">
					</div>
					<div class="col-sm-2"><input class="form-control" type="text" name="rapidball_loss_total" id="rapidball_loss_total" readonly></div>
					<div class="col-sm-2"><input class="form-control" type="text" name="rapidball_loss_cust_total" id="rapidball_loss_cust_total" readonly></div>
					<div class="col-sm-1">
						<i class="fa fa-calculator calculation" id="rapidball_loss"></i>
					</div>
				</div>
				<br><br>
			</div>
			
			<div class="col-sm-12">	
				<div class="form-group">	
					<label class="col-sm-3 control-label" for="sports_loss_percentage">Sports %</label>
					<div class="col-sm-4">
						<input type="text" id="sports_loss_percentage" class="form-control" value="<?php echo $sports_loss_percentage; ?>">
					</div>
					<div class="col-sm-2"><input class="form-control" type="text" name="sports_loss_total" id="sports_loss_total" readonly></div>
					<div class="col-sm-2"><input class="form-control" type="text" name="sports_loss_cust_total" id="sports_loss_cust_total" readonly></div>
					<div class="col-sm-1">
						<i class="fa fa-calculator calculation" id="sports_loss"></i>
					</div>
				</div>
				<br><br>
			</div>			
			
		</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-primary" id="save_changes">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
