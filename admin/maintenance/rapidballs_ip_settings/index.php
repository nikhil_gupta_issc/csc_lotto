<?php
	$page_title = "Rapidballs Settings";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// get current settings from db
	$settings = array();
	$q = "SELECT * FROM `settings`";
	$settings_info = $db->query($q);
	foreach($settings_info as $setting){
		$settings[$setting['setting']] = $setting['value'];
	}
?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#save_changes').click(function(){
				
				
				var ips = $("#ip_address").val();
				$.ajax({
					url: "/admin/ajax/rapidballs_ip_settings.php",
					type: "POST",
					dataType : "json",
					data: {
						ip_address : ips
					},
					success: function(response){
						if(response.success != 'true'){
							bootbox.alert("Operation Failed!\n"+response);
						}else{
							bootbox.alert("Saved!");
							location.reload();
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
				});
			});
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Rapidball IP Settings</h3>
				</div>
				<div class="panel-body">
					<div class="col-sm-12">
						<div class="form-group">							
							<div class="col-sm-6">	<label for="address">Add IP Address</label></div>
								<div class="col-sm-6">	<input type="text" class="form-control" id="ip_address" name="ip_address"></div>
								
						</div>
						
					</div>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-primary" id="save_changes">Save Changes</button>
				</div>
			</div>
			<table id="datatable" class="display table" cellspacing="0" width="100%" style="background-color:#fff;">
						<thead>
							<tr>
								
								<th class="text-left">IP ADDRESS</th>
								
								
							</tr>
						</thead>
						<?php  $select_ip = "select ipaddress from ipaddress where status=1";
                             				$ip = $db->query($select_ip); 
                             				foreach($ip as $ip_n){
                             				?>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty"><?php echo $ip_n['ipaddress'];?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
