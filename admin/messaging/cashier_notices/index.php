<?php
	$page_title = "Cashier Payout Messages";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){		
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			$(document).on('change', '#table_name', function(){
				$.ajax({
					url: "/admin/ajax/direct_export.php",
					type: "POST",
					data: {
						action : "get_columns",
						table_name : $("#table_name").val()
					},
					dataType: "json",
					success: function(data){
						if(data.success==true){
							$('#column_name').html("<option value=''></option>");
							$('#item_name').html("<option value=''></option>");
							$.each(data.columns, function(index, value){
								$('#column_name').append("<option value='"+value.COLUMN_NAME+"'>"+value.COLUMN_NAME+"</option>");
							});
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
						{ "data": "rule_name" },
						{ "data": "title" },
						{ "data": "message" },
						{ "data": "table_name" },
						{ "data": "column_name" },
						{ "data": "comparison" },
						{ "data": "value" },
						{ "data": "status" }
					],
				"order": [[1, 'desc']],
				"ajax": "/admin/ajax/datatables/cashier_notices.php"
			});
		
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var columns = data_table.row(this).data();
				if(columns['is_disabled'] == "Enabled"){
					$('#disable_btn').text("Disable");
				}else{
					$('#disable_btn').text("Enable");
				}
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});

			$('#disable_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				
				$.ajax({
					url: "/admin/ajax/datatables/cashier_notices_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#delete_btn').click(function(){
				$("#remove_confirmation_modal").modal("show");
			});
			
			$(document).on("click", "#remove_confirm", function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/cashier_notices_buttons.php",
					type: "POST",
					data: {
						action : 'delete',
						id : id
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				$("#remove_confirmation_modal").modal("hide");
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#edit_btn').trigger('click');
			});
			
			$('#edit_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				// populate elements with data from table
				var columns = data_table.row('.selected').data();
				$('#modal_row_id').html("ID #"+id);
				$('#rule_name').val(columns['rule_name']);
				$('#table_name').val(columns['table_name']).trigger('change');
				$('#comparison').val(columns['comparison']).trigger('change');
				$('#value').val(columns['value']);
				$('#title').val(columns['title']);
				$('#message').val(columns['message']);
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
			
			// delay needed to set focus after modal is visible
			$('#edit_row_modal').on('show.bs.modal', function (e) {
				setTimeout(function(){
					var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
					// deteremine whether this is update or insert
					if(id > 0){
						var columns = data_table.row('.selected').data();
						$('#column_name').val(columns['column_name']);
					}
					
					// focus on first input element
					$('#rule_name').focus();
				}, 350);                        
			})
			
			// data validation for edit modal
			$(document).on('click', '#submit_edit', function() {
				$('#edit_row_modal').modal('hide');
				
				var id = parseInt($('#modal_row_id').html().replace("ID #", ""));
				
				// deteremine whether this is update or insert
				if(id > 0){
					var action = "update";
				}else{
					var action = "insert";
				}
		
				$.ajax({
					url: "/admin/ajax/datatables/cashier_notices_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id,
						rule_name : $('#rule_name').val(),
						table_name : $('#table_name').val(),
						comparison : $('#comparison').val(),
						value : $('#value').val(),
						title : $('#title').val(),
						message : $('#message').val(),
						column_name : $('#column_name').val()
					},
					dataType: "json",
					success: function(data){
						// hide buttons and reload table if successful
						$('.btn-row-action').hide();
						data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#add_btn').click(function(){				
				// clear elements
				$('#modal_row_id').html("New");
				$('#rule_name').val("");
				$('#table_name').val("");
				$('#comparison').val("");
				$('#value').val("");
				$('#title').val("");
				$('#message').val("");
				$('#column_name').val("");
				$('#column_name').html("");
				
				// show modal
				$('#edit_row_modal').modal('show');
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Cashier Payout Notices</h3>
					
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
						<a id="edit_btn" class="btn btn-default btn-sm btn-row-action">Edit</a>
						<a id="disable_btn" class="btn btn-danger btn-sm btn-row-action">Disable</a>
						<a id="delete_btn" class="btn btn-default btn-sm btn-row-action">Delete</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Rule Name</th>
								<th class="text-left">Title</th>
								<th class="text-left">Message</th>
								<th class="text-left">Table</th>
								<th class="text-left">Field</th>
								<th class="text-left">Comparison</th>
								<th class="text-left">Value</th>
								<th class="text-left">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="edit_row_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="edit_row_modal_label">Administrative Alert Rule</h4>
					<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="rule_name" class="control-label col-sm-2">Rule Name</label>
							<div class="col-sm-5">
								<input type="text" id="rule_name" class="form-control"></input>
							</div>
							<div class="col-sm-5"></div>
						</div>
						
						<div class="form-group">
							<label for="title" class="control-label col-sm-2">Message Title</label>
							<div class="col-sm-5">
								<input type="text" id="title" class="form-control"></input>
							</div>
							<div class="col-sm-5"></div>
						</div>
						
						<div class="form-group">
							<label for="message" class="control-label col-sm-2">Message</label>
							<div class="col-sm-5">
								<input type="text" id="message" class="form-control"></input>
							</div>
							<div class="col-sm-5"></div>
						</div>
					
						<div class="form-group">
							<label for="table_name" class="control-label col-sm-2">Table</label>
							<div class="col-sm-5">
								<select type="text" class="form-control" id="table_name">
									<?php
										$tables = $db->query("SHOW TABLES");
										foreach($tables as $table){
											echo "<option value=".$table['Tables_in_asurewin'].">".$table['Tables_in_asurewin']."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-5"></div>
						</div>
						<div class="form-group">
							<label for="column_name" class="control-label col-sm-2">Where</label>
							<div class="col-sm-4">
								<select id="column_name" class="form-control">
									<option value=""></option>
								</select>
							</div>
							<div class="col-sm-2">
								<select id="comparison" class="form-control">
									<option value="=">=</option>
									<option value="<="><=</option>
									<option value=">=">>=</option>
									<option value="<"><</option>
									<option value=">">></option>
									<option value="!=">!=</option>
								</select>
							</div>
							<div class="col-sm-3">
								<input type="text" id="value" class="form-control"></input>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-2">
								<button type="button" class="btn btn-primary" id="submit_edit">Save</button>
							</div>
							<div class="col-sm-10"></div>
						</div>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
							
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
		
	<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
				</div>
				<div class="modal-body">
					Are you sure you would like to remove this item?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 