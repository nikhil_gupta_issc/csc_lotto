<?php
	$page_title = "Birthday Messages";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// get current settings from db
	$settings = array();
	$q = "SELECT * FROM `settings`";
	$settings_info = $db->query($q);
	foreach($settings_info as $setting){
		$settings[$setting['setting']] = $setting['value'];
	}
?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#save_changes').click(function(){				
				$.ajax({
					url: "/admin/ajax/birthday_messages.php",
					type: "POST",
					data: {
						action : "update",
						birthday_email : $('#birthday_email').val(),
						birthday_text : $('#birthday_text').val(),
						time2send : $('#time2send').val(),
					},
					success: function(response){
						if(response != ""){
							bootbox.alert("Operation Failed!\n"+response['query']);
						}else{
							bootbox.alert("Saved!");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Operation Failed!");
					}
				});
			});
			
			$('#time2send').timepicker();
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $page_title; ?></h3>
				</div>
				<div class="panel-body">
					<div class="form-group col-sm-12">
						<label for="birthday_email">Birthday Email Message</label>
						<textarea rows="8" class="form-control" id="birthday_email"><?php echo $settings['birthday_email']; ?></textarea>
					</div>
					
					<div class="form-group col-sm-12">
						<label for="birthday_text">Birthday Text Message</label>
						<textarea rows="8" class="form-control" id="birthday_text"><?php echo $settings['birthday_text']; ?></textarea>
					</div>
					
					<div class="form-group col-sm-4 col-md-3 col-lg-2">
						<label for="time2send">Time To Send</label>
							<div class="bootstrap-timepicker">
								<input id="time2send" type="text" value="<?php echo $settings['birthday_time_to_send']; ?>" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer" style="margin-top: -21px;">
					<button type="button" class="btn btn-primary" id="save_changes">Save Changes</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 