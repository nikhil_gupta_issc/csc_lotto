<?php
	$page_title = "Send SMS Message";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
	
	// get current settings from db
	$settings = array();
	$q = "SELECT * FROM `settings`";
	$settings_info = $db->query($q);
	foreach($settings_info as $setting){
		$settings[$setting['setting']] = $setting['value'];
	}
?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#send_message').click(function(){				
				$.ajax({
					url: "/admin/ajax/send_sms_message.php",
					type: "POST",
					dataType : 'json',
					data: {
						cellphone : $('#cellphone').val(),
						message : $('#message').val()
					},
					success: function(response){
						if(response.status == "failed"){
							bootbox.alert("Operation Failed!");
						}else{
							bootbox.alert("Sent!");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Operation Failed!");
					}
				});
			});
		});
	</script>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $page_title; ?></h3>
				</div>
				<div class="panel-body">
					<div class="form-group col-sm-6">
						<label for="cellphone">Phone Number</label>
						<input type="text" class="form-control" id="cellphone" placeholder="Include +1">
					</div>
					
					<div class="form-group col-sm-12">
						<label for="message">Message</label>
						<textarea rows="8" class="form-control" id="message"></textarea>
					</div>
				</div>
				<div class="panel-footer" style="margin-top: -21px;">
					<button type="button" class="btn btn-primary" id="send_message">Send Message</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 