<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$cashier_id = isset($_POST['cashier_id']) ? $_POST['cashier_id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'get_summary':
			$summary = $core->get_customer_totals($_POST['start_date'],$_POST['end_date'],$_POST['customer_id']);

			foreach($summary as $key => $value){
				if(stripos($key,"amount") !== false || stripos($key,"net")){
					$summary[$key] = "".number_format($value) . " " . CURRENCY_FORMAT;
				}
			}

			$response = $summary;
			$response['success'] = true;
			break;
		default:
			$response['success'] = false;
	}
	
	echo json_encode($response);
