<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	// create game lookup array
	$q = "SELECT `g`.`short_name`, `g`.`id`, `h`.`web_cutoff_time` FROM `lotto_game` `g` JOIN `lotto_house` `h` ON `h`.`id` = `g`.`house_id` WHERE `g`.`is_disabled`=0 ORDER BY `h`.`web_cutoff_time` ASC, `g`.`number_of_balls` ASC";
	$games = $db->query($q);
	foreach($games as $game){
		$cut_time_dt = new DateTime($game['web_cutoff_time']);
		$cut_time = $cut_time_dt->format("h:i A");
		//$game_lookup[$game['id']]['name'] = $game['short_name']."<br>".$cut_time;
		$game_lookup[$game['id']]['name'] = $game['short_name'];
	}

	//$q = "SELECT g.id, SUM(bet_amount) AS sale_total FROM `lotto_bet` b JOIN `lotto_ticket` t ON t.ticket_id=b.ticket_id JOIN `lotto_game` g ON b.game_id=g.id JOIN `lotto_house` h ON h.id=g.house_id WHERE h.is_disabled=0 AND CAST(t.purchase_date AS DATE)='".$_POST['date']."' GROUP BY h.id";
	$q = "SELECT g.id, SUM(bet_amount) AS sale_total FROM `lotto_bet` b JOIN `lotto_ticket` t ON t.ticket_id=b.ticket_id JOIN `lotto_game` g ON b.game_id=g.id JOIN `lotto_house` h ON h.id=g.house_id WHERE h.is_disabled=0 AND CAST(t.purchase_date AS DATE)=%s GROUP BY h.id";
	//$sales = $db->query($q);	
	$sales = $db->query($q, array($_POST['date']));
	
	foreach($sales as $sale){
		$game_lookup[$sale['id']]['sales'] = floatval($sale['sale_total']);
	}
	
	//$q = "SELECT g.id, SUM(total_payout) AS win_total, `b`.`ball_string` AS winning_number FROM `lotto_ticket` AS `t` LEFT JOIN `lotto_bet` AS `b` ON `t`.`ticket_id`=`b`.`ticket_id` JOIN `lotto_game` g ON b.game_id=g.id JOIN `lotto_house` h ON h.id=g.house_id LEFT JOIN `winner_payout` AS `wp` ON `wp`.`ticket_number`=`t`.`ticket_number` WHERE h.is_disabled=0 AND b.is_winner=1 AND CAST(purchase_date AS DATE) = '".$_POST['date']."' GROUP BY `h`.`id`";
	$q = "SELECT g.id, SUM(total_payout) AS win_total, `b`.`ball_string` AS winning_number FROM `lotto_ticket` AS `t` LEFT JOIN `lotto_bet` AS `b` ON `t`.`ticket_id`=`b`.`ticket_id` JOIN `lotto_game` g ON b.game_id=g.id JOIN `lotto_house` h ON h.id=g.house_id LEFT JOIN `winner_payout` AS `wp` ON `wp`.`ticket_number`=`t`.`ticket_number` WHERE h.is_disabled=0 AND b.is_winner=1 AND CAST(purchase_date AS DATE) = %s GROUP BY `h`.`id`";
	//$wins = $db->query($q);
	$wins = $db->query($q, array($_POST['date']));

	foreach($wins as $win){
		$game_lookup[$win['id']]['hits'] = floatval($win['win_total']);
		
		// add winning number to name
		$game_lookup[$win['id']]['name'] .= "<br>#".$win['winning_number'];
	}
	
	foreach($game_lookup as $return_info){
		$categories[] = $return_info['name'];
		$sales_tot[] = $return_info['sales'];
		$hits_tot[] = $return_info['hits'];
	}

	echo json_encode(array("categories" => $categories, "sales" => $sales_tot, "hits" => $hits_tot));
