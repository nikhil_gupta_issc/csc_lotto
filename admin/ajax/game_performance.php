<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d");
	
	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$from = isset($_POST['from']) ? $_POST['from'] : $now;
	$to = isset($_POST['to']) ? $_POST['to'] : $now;
	
	switch ($action){
		case 'get_performance':
			$q = "SELECT SUM(casino_bets) as casino_bets, SUM(casino_wins) as casino_wins, SUM(lotto_wins) as lotto_wins, SUM(lotto_bets) as lotto_bets, SUM(sports_wins) AS sports_wins, SUM(sports_bets) AS sports_bets, SUM(rapidballs_wins) AS rapidballs_wins, SUM(rapidballs_bets) AS rapidballs_bets FROM cron_daily_totals WHERE CAST(total_date AS DATE) >= %s AND CAST(total_date AS DATE) <= %s";
			$total_information = $db->queryOneRow($q, array($from, $to));
			
			$response['casino_wins'] = number_format(abs($total_information['casino_wins']));
			$response['casino_bets'] = number_format(abs($total_information['casino_bets']));
			if($response['casino_bets'] != 0){
				$response['casino_return'] = number_format(abs($total_information['casino_wins'] / $total_information['casino_bets'] * 100));
			}else{
				$response['casino_return'] = "0";
			}
			$response['lotto_wins'] = number_format(abs($total_information['lotto_wins']));
			$response['lotto_bets'] = number_format(abs($total_information['lotto_bets']));
			if($response['lotto_bets'] != 0){
				$response['lotto_return'] = number_format(abs($total_information['lotto_wins'] / $total_information['lotto_bets'] * 100));
			}else{
				$response['lotto_return'] = "0";
			}
			$response['sports_wins'] = number_format(abs($total_information['sports_wins']));
			$response['sports_bets'] = number_format(abs($total_information['sports_bets']));
			if($response['sports_bets'] != 0){
				$response['sports_return'] = number_format(abs($total_information['sports_wins'] / $total_information['sports_bets'] * 100));
			}else{
				$response['sports_return'] = "0";
			}
			$response['rapidballs_wins'] = number_format(abs($total_information['rapidballs_wins']));
			$response['rapidballs_bets'] = number_format(abs($total_information['rapidballs_bets']));
			if($response['rapidballs_bets'] != 0){
				$response['rapidballs_return'] = number_format(abs($total_information['rapidballs_wins'] / $total_information['rapidballs_bets'] * 100));
			}else{
				$response['rapidballs_return'] = "0";
			}
			
			$response['result'] = true;
			
			break;
		default:
			$response['result'] = false;
			$response['errors'] = "An unexpected error occurred";
	}
	
	echo json_encode($response);
