<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$response = array();

	switch ($_POST['action']){
		case 'add':
			$group_name = isset($_POST['group_name']) ? $_POST['group_name'] : "";
			$group_description = isset($_POST['group_description']) ? $_POST['group_description'] : "";

			$q = "REPLACE INTO `sec_groups` (`id`, `name`, `description`, `is_deleted`, `is_system`, `system_lookup_query`) VALUES (NULL, %s, %s, '0', '0', NULL);";
			$response['new_id'] = $db->queryInsert($q, array($group_name, $group_description));
			
			if($response['new_id'] < 1){
				$response['query'] = $q;
				$response['success'] = false;
			}else{
				$response['success'] = true;
			}				
			
			break;
		case 'remove':
			$group_id = isset($_POST['group_id']) ? $_POST['group_id'] : "";
			
			$q = "UPDATE `sec_groups` SET `is_deleted`='1' WHERE `id`=%i";
			$result = $db->queryDirect($q, array($group_id));
			
			if($result){
				$response['query'] = $q;
				$response['success'] = false;
			}else{
				$response['success'] = true;
			}				
			
			break;
		case 'update':
			$group_id = isset($_POST['group_id']) ? $_POST['group_id'] : "";
			$group_name = isset($_POST['group_name']) ? $_POST['group_name'] : "";
			$group_description = isset($_POST['group_description']) ? $_POST['group_description'] : "";
			$members = isset($_POST['members']) ? $_POST['members'] : null;
			$permissions = isset($_POST['permissions']) ? $_POST['permissions'] : null;
			
			// get group info
			$q = "SELECT * FROM `sec_groups` WHERE `id` = %i;";
			$group_info = $db->queryOneRow($q, array($group_id));

			if($group_info['is_system'] == 0){
				// group
				$q = "REPLACE INTO `sec_groups` (`id`, `name`, `description`, `is_deleted`, `is_system`, `system_lookup_query`) VALUES (NULL, %s, %s, NULL, NULL, NULL);";
				$response['new_id'] = $db->queryInsert($q, array($group_name, $group_description));
				
				// members
				if(count($members) >= 1){
					foreach($members as $user_id){
						// clear out current members
						$q = "DELETE FROM `sec_group_members` WHERE `sec_group_id` = %i;";
						$db->query($q, array($group_id));
						
						$q = "INSERT INTO `sec_group_members` (`id`, `sec_group_id`, `user_id`) VALUES (NULL, %i, %i);";
						$db->queryInsert($q, array($group_id, $user_id));
					}
				}
			}
			
			// clear old permissions 
			$q = "DELETE FROM `sec_group_permissions` WHERE `group_id` = %i;";
			$db->query($q, array($group_id));
			
			// categories
			if(count($permissions) >= 1){
				foreach($permissions as $category_id => $perm_level){
					if($category_id > 0){
						$q = "REPLACE INTO `sec_group_permissions` (`id`, `group_id`, `category_id`, `permission_level_id`) VALUES (NULL, %i, %i, %i);";
						$db->queryInsert($q, array($group_id, $category_id, $perm_level));
					}
				}
			}
		
			break;
		case 'get_members':
			$group_id = isset($_POST['group_id']) ? $_POST['group_id'] : -1;
			
			$q = "SELECT * FROM `sec_groups` WHERE `id` = %i;";
			$group_info = $db->queryOneRow($q, array($group_id));
			
			$response['is_system'] = $group_info['is_system'];
			
			$response['description'] = $group_info['description'];
			
			// see if this is a system group with dynamic lookup rule
			if($group_info['is_system'] == 1){
				// use lookup query to get member user id's
				$q = $group_info['system_lookup_query'];
				$user_ids = $db->query($q);
			}else{
				// get member id's from members table
				$q = "SELECT `user_id` FROM `sec_group_members` WHERE `sec_group_id` = %i;";
				$user_ids = $db->query($q, array($group_id));
			}
			
			if(count($user_ids) > 0){
				foreach($user_ids as $user_id){
					$response['members'][] = $user_id['user_id'];
				}
			}
			
			$response['success'] = true;
			
			break;
		case 'get_permissions';
			$group_id = isset($_POST['group_id']) ? $_POST['group_id'] : -1;
			
			$q = "SELECT * FROM `sec_categories` WHERE `is_deleted`='0'";
			$categories = $db->query($q);

			$q = "SELECT * FROM `sec_group_permissions` WHERE `group_id`=%i;";
			$group_permissions = $db->query($q, array($group_id));
			
			foreach($categories as $cat){
				foreach($group_permissions as $perm){
					if($perm['category_id'] == $cat['id']){
						$response['permissions'][$cat['id']] = $perm['permission_level_id'];
					}
				}
			}
			
			break;
		default:
			$response['success'] = false;
			break;
	}
	
	echo json_encode($response);
