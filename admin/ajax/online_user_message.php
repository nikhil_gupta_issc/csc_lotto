<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	$core->send_alert_active_accounts($_POST['subject'], $_POST['message']);
	
	echo json_encode(array ("result" => true));
