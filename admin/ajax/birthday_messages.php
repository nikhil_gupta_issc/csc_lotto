<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	//$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('birthday_email', '".$_POST['birthday_email']."');";
	//$result = $db->query($q);
	
	//$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('birthday_text', '".$_POST['birthday_text']."');";
	//$result = $db->query($q);

	//$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('birthday_time_to_send', '".$_POST['time2send']."');";
	//$result = $db->query($q);

	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('birthday_email', %s);";
	$result = $db->query($q, array($_POST['birthday_email']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('birthday_text', %s);";
	$result = $db->query($q, array($_POST['birthday_text']));

	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('birthday_time_to_send', %s);";
	$result = $db->query($q, array($_POST['time2send']));
