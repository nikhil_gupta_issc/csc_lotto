<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	// create house lookup array
	for($x=0; $x<=23; $x++){
		$hour_lookup[$x]['hour'] = $x.":00";
	}

	if($_POST['location_id'] != ""){
		$q = "SELECT hour(t.purchase_date) AS hour, SUM(bet_amount) AS sale_total FROM `lotto_bet` b JOIN `lotto_ticket` t ON t.ticket_id=b.ticket_id WHERE CAST(t.purchase_date AS DATE)=%s AND t.location_id=%i GROUP BY hour(t.purchase_date);";
		$sales_per_hour = $db->query($q, array($_POST['date'], $_POST['location_id']));
	}else{
		$q = "SELECT hour(t.purchase_date) AS hour, SUM(bet_amount) AS sale_total FROM `lotto_bet` b JOIN `lotto_ticket` t ON t.ticket_id=b.ticket_id WHERE CAST(t.purchase_date AS DATE)=%s GROUP BY hour(t.purchase_date);";
		$sales_per_hour = $db->query($q, array($_POST['date']));
	}
	
	foreach($sales_per_hour as $sale){
		$hour_lookup[$sale['hour']]['sales'] = floatval($sale['sale_total']);
	}
	
	if($_POST['location_id'] != ""){
		$q = "SELECT hour(t.purchase_date) AS hour, count(t.purchase_date) AS number_of_sales FROM `lotto_ticket` t WHERE CAST(t.purchase_date AS DATE)=%s AND t.location_id=%i GROUP BY hour(t.purchase_date)";
		$number_of_sales = $db->query($q, array($_POST['date'], $_POST['location_id']));
	}else{
		$q = "SELECT hour(t.purchase_date) AS hour, count(t.purchase_date) AS number_of_sales FROM `lotto_ticket` t WHERE CAST(t.purchase_date AS DATE)=%s GROUP BY hour(t.purchase_date)";
		$number_of_sales = $db->query($q, array($_POST['date']));
	}
	
	foreach($number_of_sales as $sale){
		$hour_lookup[$sale['hour']]['number_of_sales'] = intval($sale['number_of_sales']);
	}
	
	foreach($hour_lookup as $return_info){
		$categories[] = $return_info['hour'];
		$sales_tot[] = $return_info['sales'];
		$number_of_sale[] = $return_info['number_of_sales'];
	}

	echo json_encode(array("categories" => $categories, "sales" => $sales_tot, "number_of_sales" => $number_of_sale));
