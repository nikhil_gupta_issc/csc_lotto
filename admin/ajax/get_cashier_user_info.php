<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$results = array();

	if(isset($_POST['user_id'])){
		$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : -1;
		
		if($user_id != -1){	
		
			//$q = "SELECT * FROM ".TBL_USERS." WHERE `id` = '".$user_id."'";
			$q = "SELECT * FROM ".TBL_USERS." WHERE `id` = %i";
			//if($results['user_info'] = $db->queryOneRow($q)){
			if($results['user_info'] = $db->queryOneRow($q, array($user_id))){

				$results['success'] = true;
			}else{
				$results['query'] = $q; // for debugging
				$results['success'] = false;
				echo json_encode($results);
				die();
			}
			
			// see if lockout expired... if so, clear it out
			if($results['user_info']['locked'] == 1){
				// account is locked out, see if login attempt should be allowed yet
				$now_dt = new DateTime();
				$expiration_dt = new DateTime($results['user_info']['locked_expiration']);
				
				if($now_dt > $expiration_dt){
					// reset login attempts

					//$db->query("UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '0' WHERE `id`='".$user_id."'");
					$q = "UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '0' WHERE `id`=%i;";
					$db->query($q, array($user_id));

					// unlock account

					//$db->query("UPDATE `".TBL_USERS."` SET `locked` = '0' WHERE `id`='".$user_id."'");
					$q = "UPDATE `".TBL_USERS."` SET `locked` = '0' WHERE `id`=%i;";
					$db->query($q, array($user_id));

					// reset lockout expiration

					//$db->query("UPDATE `".TBL_USERS."` SET `locked_expiration` = NULL WHERE `id`='".$user_id."'");
					$q = "UPDATE `".TBL_USERS."` SET `locked_expiration` = NULL WHERE `id`=%i;";
					$db->query($q, array($user_id));

					// reload from database

					//$q = "SELECT * FROM ".TBL_USERS." WHERE id = '".$user_id."'";
					$q = "SELECT * FROM ".TBL_USERS." WHERE id = %i;";
					//if($results['user_info'] = $db->queryOneRow($q)){
					if($results['user_info'] = $db->queryOneRow($q, array($user_id))){

						$results['success'] = true;
					}else{
						$results['query'] = $q; // for debugging
						$results['success'] = false;
					}
				}
			}
			
			// get cashier table info

			$q = "SELECT * FROM `panel_user` WHERE user_id = %i;";
			if($results['cashier_info'] = $db->queryOneRow($q, array($user_id))){

				$results['success'] = true;
			}else{
				$results['query'] = $q; // for debugging
				$results['success'] = false;
			}
		}
	}else{
		$results['success'] = false;
	}
	
	echo json_encode($results);
