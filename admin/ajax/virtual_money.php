<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$cashier_id = isset($_POST['cashier_id']) ? $_POST['cashier_id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
        
	switch ($action){
		case 'save_default_offset':

                         if (!empty($_POST['value']) && is_numeric($_POST['value']) && $_POST['value'] > 0) {
				$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('default_virtual_money_offset', %s);";
			        $result = $db->query($q, array($_POST['value']));
				$response['result'] = true;
                         }else{
                                $response['result'] = false;
                                $response['error'] = 'Please enter valid amount.';
                          }
                        break;

                 case 'get_default_offset':

                        $q = "SELECT value FROM settings WHERE setting = 'default_virtual_money_offset';";
                        $result = $db->query($q);

                        if(!empty($result[0]['value'])){
                            $response['value'] = $result[0]['value'];
                            $response['result'] = true;
                        } else {
                            $response['result'] = false;
                        }
               
	}
	
	echo json_encode($response);
