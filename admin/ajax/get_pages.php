<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$results = array();
	
	if(empty($_POST['action'])){
	
	$results['options'] = "<option value='-1'>Select Page</option>";
	
	$pages = $db->query("SELECT id,title FROM pages WHERE status=1 ORDER BY title");
	foreach($pages as $page){
		$results['options'] .= "<option value='".$page['id']."'>".$page['title']."</option>";
	}
	
	echo json_encode($results);
	}
	
	if($_POST['action'] == 'info' && !empty($_POST['action'])){
	
	$q = "SELECT * FROM pages WHERE id=".$_POST['id'];
	$result = $db->query($q);
	
	echo json_encode($result);
	}
	
	
	
	if($_POST['action'] != 'info' && $_POST['action'] == 'update' && !empty($_POST['action'])){
	
	
	$target_dir = DOMAIN_ROOT."/images/";
	$target_file = $target_dir . basename($_FILES["image"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	
	
	// Check file size
	if ($_FILES["image"]["size"] > 500000) {
    		//echo "Sorry, your file is too large.";
    		$uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
    		//$response['filenotupload'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    		$uploadOk = 0;
	}
	
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
    		//echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
    		if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
        		//echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
    	} else {
        	//echo "Sorry, there was an error uploading your file.";
    	}
}	
	$content = htmlentities($_POST['content']);		
	$q = "update pages set title='".$_POST['title']."',page_title='".$_POST['page_title']."',content='".$content."',image_path='".$_FILES["image"]["name"]."' WHERE id=".$_POST['id'];
	$result = $db->query($q);
	
	$response['success'] = 'true';
	echo json_encode($response);
	}
	
	
	
