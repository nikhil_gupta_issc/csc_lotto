<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$cashier_id = isset($_POST['cashier_id']) ? $_POST['cashier_id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d");
	
	switch ($action){
		case 'get_summary':
			//REBATE CALCULATION CODE STARTS
		$sumArray = array();
		//get current settings from db of loss caclulation on/off
		$settings = array();
		$q = "SELECT value FROM `settings` WHERE `setting` = 'loss_calculation_enabled'";
		$settings_info = $db->queryOneRow($q);
		if(!empty($settings_info)){
           		$loss_calculation_enabled = $settings_info['value'];
       		}
       		
       		if($loss_calculation_enabled == 1){      			
        		       		
        		$type_id = array();
        		// get current settings from db of lotto loss on/off
			$settings = array();
			$q = "SELECT value FROM `settings` WHERE `setting` = 'lotto_loss_enabled'";
			$settings_info = $db->queryOneRow($q);
			if(!empty($settings_info)){
			   $lotto_loss_enabled = $settings_info['value'];			   
			}
			if($lotto_loss_enabled == 1){		
				//array_push($type_id,'3','4');
				// get current settings of lotto loss percentage from db
				$enable = array();
				$q = "SELECT value FROM `settings` WHERE `setting` = 'lotto_loss_percentage'";
				$enable_info = $db->queryOneRow($q);
				if(!empty($enable_info)){
				   $lotto_loss_percentage = $enable_info['value'];
				}
				
				if($lotto_loss_percentage > 0){
        			//get all customer from last one week 
        			
        			$q ="SELECT SUM(lb.bet_amount) as loss, lt.user_id FROM lotto_bet as lb INNER JOIN lotto_ticket as lt ON lb.ticket_id = lt.ticket_id AND lt.user_id != - 9999 AND  lb.is_processed = 1 AND lb.is_winner = 0 where (DATE(lb.processed_date_time) BETWEEN '".$_POST['start_date']."' AND '".$_POST['end_date']."') GROUP BY lt.user_id";     
        			$lotto_user = $db->query($q);   			
        			
        			if(!empty($lotto_user)){
        					foreach($lotto_user as $lotto_value){
        						$sumArray[$lotto_value['user_id']]['lotto'] =$lotto_value['loss'];	
        					}
        					
        				}       			
        			}								
			}
			
			// get current settings of casino loss on/off from db
			$enable = array();
			$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_loss_enabled'";
			$enable_info = $db->queryOneRow($q);
			if(!empty($enable_info)){
			   $casino_loss_enabled = $enable_info['value'];			   
			}			
			if($casino_loss_enabled == 1){
				array_push($type_id,'1','2');
				// get current settings of casino loss percentage from db
				$enable = array();
				$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_loss_percentage'";
				$enable_info = $db->queryOneRow($q);
				if(!empty($enable_info)){
				   $casino_loss_percentage = $enable_info['value'];
				}
			}
			
			// get current settings of rapidball loss on/off from db
			$enable = array();
			$q = "SELECT value FROM `settings` WHERE `setting` = 'rapidball_loss_enabled'";
			$enable_info = $db->queryOneRow($q);
			if(!empty($enable_info)){
			   $rapidball_loss_enabled = $enable_info['value'];			    
			}			
			if($rapidball_loss_enabled == 1){
				//array_push($type_id,'46','47');	
				// get current settings of rapidball loss percentage from db
				$enable = array();
				$q = "SELECT value FROM `settings` WHERE `setting` = 'rapidball_loss_percentage'";
				$enable_info = $db->queryOneRow($q);
				if(!empty($enable_info)){
				   $rapidball_loss_percentage = $enable_info['value'];
				}
				
				if($rapidball_loss_percentage > 0){
        			//get all customer from last one week
        			
        			$q = "SELECT SUM(rb.bet_amount) as loss, rt.user_id FROM rapidballs_ticket rt INNER JOIN rapidballs_bet rb ON rb.ticket_id = rt.id AND is_processed = 1 AND is_winner = 0  where (DATE(rb.processed_datetime) BETWEEN '".$_POST['start_date']."' AND '".$_POST['end_date']."') GROUP BY rt.user_id";
        			$rb_user = $db->query($q);  
        			
        				if(!empty($rb_user)){
        					foreach($rb_user as $rb_value){
        						$sumArray[$rb_value['user_id']]['rb'] =$rb_value['loss'];	
        					}
        				}      			
        			}
        						
			}
			
			// get current settings from db
			$enable = array();
			$q = "SELECT value FROM `settings` WHERE `setting` = 'sports_loss_enabled'";
			$enable_info = $db->queryOneRow($q);
			if(!empty($enable_info)){
			   $sports_loss_enabled = $enable_info['value'];			   
			}
			if($sports_loss_enabled == 1){
				array_push($type_id,'17','45','18');
				// get current settings of sports loss percentage from db
				$enable = array();
				$q = "SELECT value FROM `settings` WHERE `setting` = 'sports_loss_percentage'";
				$enable_info = $db->queryOneRow($q);
				if(!empty($enable_info)){
				   $sports_loss_percentage = $enable_info['value'];
				}
			}			
			$types = implode(",",$type_id);	
			 		
			$q = "SELECT  ct.user_id, ct.amount , ct.transaction_type_id FROM customer_transaction as ct where
    				ct.transaction_type_id IN ($types) AND (DATE(ct.transaction_date) BETWEEN '".$_POST['start_date']."' AND '".$_POST['end_date']."') ";
        		$customer_transaction = $db->query($q);	        		
        		
					
			
			foreach ($customer_transaction as $k=>$subArray) {
				//$sumArray[$subArray['user_id']][$subArray['transaction_type_id']] +=$subArray['amount'];		
  				if($subArray['transaction_type_id'] == 1 || $subArray['transaction_type_id'] == 2){
  					$sumArray[$subArray['user_id']]['casino'] +=$subArray['amount'];
  				}
  				elseif($subArray['transaction_type_id'] == 17 || $subArray['transaction_type_id'] == 45 || $subArray['transaction_type_id'] == 18){
  					$sumArray[$subArray['user_id']]['sports'] +=$subArray['amount'];
  				}
			}
			
        			$casino_rb_amount = 0;
				$lotto_rb_amount = 0;
				$rapidball_rb_amount =0;
				$sports_rb_amount =0;
				$amount = 0;	
        		foreach($sumArray as $key=>$calculation){  						      		
        			
        			if(!empty($calculation['casino']) && $calculation['casino']<0){
        				$casino_rb_amount += round((($casino_loss_percentage * $calculation['casino'])/100),2);
        			}
        			if(!empty($calculation['lotto'])){
        				$lotto_rb_amount += round((($lotto_loss_percentage * $calculation['lotto'])/100),2);
        			}
        			if(!empty($calculation['rb'])){
        				$rapidball_rb_amount += round((($rapidball_loss_percentage * $calculation['rb'])/100),2);
        			}
        			if(!empty($calculation['sports']) && $calculation['sports']){
        				$sports_rb_amount += round((($sports_loss_percentage * $calculation['sports'])/100),2);
        			}
        			
        			$amount = round((abs($casino_rb_amount)+abs($lotto_rb_amount)+abs($rapidball_rb_amount)+abs($sports_rb_amount)),2); 
        			//die;
        			
        		}        		        		
		}

			$response['casino_rb'] = abs($casino_rb_amount);
			$response['lotto_rb'] = abs($lotto_rb_amount);
			$response['rapidball_rb'] = abs($rapidball_rb_amount);
			$response['sports_rb'] = abs($sports_rb_amount);
			
			$response['casino_percentage'] = $casino_loss_percentage;
			$response['lotto_percentage'] = $lotto_loss_percentage;
			$response['rapidball_percentage'] = $rapidball_loss_percentage;
			$response['sports_percentage'] = $sports_loss_percentage;
			
			$response['total'] = $amount;
			$response['success'] = true;
			break;
		default:
			$response['success'] = false;
	}
	
	echo json_encode($response);
