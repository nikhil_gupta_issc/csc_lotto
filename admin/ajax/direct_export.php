<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$response = array();

	switch ($_POST['action']){
		case 'get_columns':
			
			$q = "SELECT DISTINCT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$_POST['table_name']."'";
			$response['q'] = $q;
			$columns = $db->query($q);
			
			$response['columns'] = $columns;
			$response['success'] = true;
			
			break;

		case 'get_export':
			
			$q = "SELECT * FROM ".$_POST['table']." ";
			if($_POST['where'] != ""){
				$q .= " WHERE ".$_POST['where'].$_POST['where_oper']."'".$_POST['where_condition']."'";
			}
			if($_POST['order_by'] != ""){
				$q .= " ORDER BY ".$_POST['order_by']." ".$_POST['order_by_asc_desc'];
			}
			if($_POST['limit'] != ""){
				$q .= " LIMIT ".$_POST['limit'];
			}
			
			$data = $db->query($q);
			
			$df = fopen("/srv/www/uploads/direct_export.csv", 'w');
			fputcsv($df, array_keys($data[0]));
			foreach($data as $d){
				fputcsv($df, $d);
			}
			fclose($df);
			
			$response['q'] = $q;
			$response['success'] = true;
			
			break;
	}
	
	echo json_encode($response);
