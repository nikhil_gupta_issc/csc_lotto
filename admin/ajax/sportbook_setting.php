<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	// Enabled?
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('sportbook_api', %s);";
	$result = $db->query($q, array($_POST['sportbook_api']));
	$qe = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('sports_enabled', %i);";
	$resulte = $db->query($qe, array($_POST['sports_enabled']));
	
	$response['success'] = 'true';
	echo json_encode($response);
	
	// Enabled?
	
	/*$qe = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('sportbook_enabled', %i);";
	$resulte = $db->query($qe, array($_POST['sportbook_enabled']));
	
	$response['success'] = 'true';
	echo json_encode($response);*/
