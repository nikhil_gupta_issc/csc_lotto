<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$cashier_id = isset($_POST['cashier_id']) ? $_POST['cashier_id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'get_shifts':

			//$q = "SELECT s.shift_start, s.shift_end, s.id FROM panel_user_shift s JOIN panel_user pu ON s.user_id=pu.user_id WHERE s.user_id='".$cashier_id."' ORDER BY s.shift_start DESC";
			$q = "SELECT s.shift_start, s.shift_end, s.id FROM panel_user_shift s JOIN panel_user pu ON s.user_id=pu.user_id WHERE s.user_id=%i ORDER BY s.shift_start DESC";
			//$shifts = $db->query($q);			
			$shifts = $db->query($q, array($cashier_id));

			//initialize the options
			$response['select_options'] = "<option value=''>All Shifts</option>";

			foreach($shifts as $shift){
				if($shift['shift_end'] == "0000-00-00 00:00:00"){
					$shift['shift_end'] = "Present";
				}else{
					$shift['shift_end'] = date("m/d/Y h:i A", strtotime($shift['shift_end']));
				}
				$response['select_options'] .= "<option value=".$shift['id'].">".date("m/d/Y h:i A", strtotime($shift['shift_start']))." - ".$shift['shift_end']." (".$shift['id'].")</option>";
			}

			$response['success'] = true;
			break;
		default:
			$response['success'] = false;
	}
	
	echo json_encode($response);
