<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	// Enabled?
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('loss_calculation_enabled', %i);";
	$result = $db->query($q, array($_POST['loss_calculation_enabled']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('lotto_loss_enabled', %i);";
	$result = $db->query($q, array($_POST['lotto_loss_enabled']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('casino_loss_enabled', %i);";
	$result = $db->query($q, array($_POST['casino_loss_enabled']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('rapidball_loss_enabled', %i);";
	$result = $db->query($q, array($_POST['rapidball_loss_enabled']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('sports_loss_enabled', %i);";
	$result = $db->query($q, array($_POST['sports_loss_enabled']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('lotto_loss_percentage', %i);";
	$result = $db->query($q, array($_POST['lotto_loss_percentage']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('casino_loss_percentage', %i);";
	$result = $db->query($q, array($_POST['casino_loss_percentage']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('rapidball_loss_percentage', %i);";
	$result = $db->query($q, array($_POST['rapidball_loss_percentage']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('sports_loss_percentage', %i);";
	$result = $db->query($q, array($_POST['sports_loss_percentage']));
	
	$response['success'] = 'true';
	echo json_encode($response);
