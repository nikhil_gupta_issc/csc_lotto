<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$results = array();
	
	$results['options'] = "<option value='-1'>All Customers</option>";
	
	$customers = $db->query("SELECT u.firstname, u.lastname, c.user_id FROM customers c JOIN users u ON c.user_id=u.id WHERE is_disabled=0 ORDER BY firstname, lastname");
	foreach($customers as $customer){
		$results['options'] .= "<option value='".$customer['user_id']."'>".$customer['firstname']." ".$customer['lastname']." (".$customer['user_id'].")</option>";
	}
	
	echo json_encode($results);