<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	$q = "SELECT * FROM `settings`";
	$prev_values = $db->query($q);
	foreach($prev_values as $v){
		if($v['setting'] == "max_withdraw_per_day"){
			if($v['value'] != $_POST['max_withdraw_per_day']){
				$core->send_alert_by_group("System Setting Change", "Maximum withdrawal per day changed from ".$v['value']." to ".$_POST['max_withdraw_per_day'], -1);
			}
		}
		if($v['setting'] == "loyalty_points_lottery"){
			if($v['value'] != $_POST['loyalty_points_lottery']){
				$core->send_alert_by_group("System Setting Change", "Loyalty points given per $1 spent for lottery changed from ".$v['value']." to ".$_POST['loyalty_points_lottery'], -1);
			}
		}
		if($v['setting'] == "loyalty_points_casino"){
			if($v['value'] != $_POST['loyalty_points_casino']){
				$core->send_alert_by_group("System Setting Change", "Loyalty points given per $1 spent for casino games changed from ".$v['value']." to ".$_POST['loyalty_points_casino'], -1);
			}
		}
		if($v['setting'] == "loyalty_points_sports"){
			if($v['value'] != $_POST['loyalty_points_sports']){
				$core->send_alert_by_group("System Setting Change", "Loyalty points given per $1 spent for sports bets changed from ".$v['value']." to ".$_POST['loyalty_points_sports'], -1);
			}
		}
		if($v['setting'] == "loyalty_points_rapidballs"){
			if($v['value'] != $_POST['loyalty_points_rapidballs']){
				$core->send_alert_by_group("System Setting Change", "Loyalty points given per $1 spent for rapidballs bets changed from ".$v['value']." to ".$_POST['loyalty_points_rapidballs'], -1);
			}
		}
		if($v['setting'] == "gold_point_amount"){
			if($v['value'] != $_POST['gold_point_amount']){
				$core->send_alert_by_group("System Setting Change", "The number of total loyalty points needed for gold status changed from ".$v['value']." to ".$_POST['gold_point_amount'], -1);
			}
		}
		if($v['setting'] == "platinum_point_amount"){
			if($v['value'] != $_POST['platinum_point_amount']){
				$core->send_alert_by_group("System Setting Change", "The number of total loyalty points needed for platinum status changed from ".$v['value']." to ".$_POST['platinum_point_amount'], -1);
			}
		}
		if($v['setting'] == "diamond_point_amount"){
			if($v['value'] != $_POST['diamond_point_amount']){
				$core->send_alert_by_group("System Setting Change", "The number of total loyalty points needed for diamond status changed from ".$v['value']." to ".$_POST['diamond_point_amount'], -1);
			}
		}
		if($v['setting'] == "promo_url"){
			if($v['value'] != $_POST['promo_url']){
				$core->send_alert_by_group("System Setting Change", "Free Daily bet Offer video changed from ".$v['value']." to ".$_POST['promo_url'], -1);
			}
		}	
	}
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('max_withdraw_per_day', %s);";
	$result = $db->query($q, array($_POST['max_withdraw_per_day']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('promo_url', %s);";
	$result = $db->query($q, array($_POST['promo_url']));	
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('ticket_expiration', %s);";
	$result = $db->query($q, array($_POST['ticket_expiration']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('loyalty_points_lottery', %s);";
	$result = $db->query($q, array($_POST['loyalty_points_lottery']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('loyalty_points_casino', %s);";
	$result = $db->query($q, array($_POST['loyalty_points_casino']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('loyalty_points_sports', %s);";
	$result = $db->query($q, array($_POST['loyalty_points_sports']));

	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('loyalty_points_rapidballs', %s);";
	$result = $db->query($q, array($_POST['loyalty_points_rapidballs']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('gold_point_amount', %s);";
	$result = $db->query($q, array($_POST['gold_point_amount']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('platinum_point_amount', %s);";
	$result = $db->query($q, array($_POST['platinum_point_amount']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('diamond_point_amount', %s);";
	$result = $db->query($q, array($_POST['diamond_point_amount']));

	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('winning_customers_amount', %s);";
	$result = $db->query($q, array($_POST['winning_customers_amount']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('maintenance_mode', %s);";
	$result = $db->query($q, array($_POST['maintenance_mode']));
	
	// Twilio
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('twilio_account_sid', %s);";
	$result = $db->query($q, array($_POST['twilio_account_sid']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('twilio_auth_token', %s);";
	$result = $db->query($q, array($_POST['twilio_auth_token']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('twilio_phone_number', %s);";
	$result = $db->query($q, array($_POST['twilio_phone_number']));
	
	// Geolocation
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('has_geolocation', %s);";
	$result = $db->query($q, array($_POST['has_geolocation']));
	
	// Video
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('has_video', %s);";
	$result = $db->query($q, array($_POST['has_video']));
	
	// Promotion
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('has_promotion', %s);";
	$result = $db->query($q, array($_POST['has_promotion']));
	
	// Mail Server
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('mailer_host', %s);";
	$result = $db->query($q, array($_POST['mailer_host']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('mailer_username', %s);";
	$result = $db->query($q, array($_POST['mailer_username']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('mailer_password', %s);";
	$result = $db->query($q, array($_POST['mailer_password']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('mailer_port', %s);";
	$result = $db->query($q, array($_POST['mailer_port']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('mailer_from_name', %s);";
	$result = $db->query($q, array($_POST['mailer_from_name']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('mailer_from_email', %s);";
	$result = $db->query($q, array($_POST['mailer_from_email']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('google_analytics_id', %s);";
	$result = $db->query($q, array($_POST['google_analytics_id']));
	
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('lottery_version', %s);";
	$result = $db->query($q, array($_POST['lottery_version']));

        $q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('promo_point', %s);";
	$result = $db->query($q, array($_POST['promo_point']));

        $q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('per_loyalty_points', %s);";
	$result = $db->query($q, array($_POST['per_loyalty_points']));

	$response['success'] = 'true';
	echo json_encode($response);
