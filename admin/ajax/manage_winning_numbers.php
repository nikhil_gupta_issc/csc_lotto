<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$response = array();

	switch ($_POST['action']){
		case 'get_winning_numbers':
			$win_date = $_POST['win_date'];
			$house_id = $_POST['house_id'];

			//$q = "SELECT g.id FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id WHERE h.id=".$house_id;
			$q = "SELECT g.id FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id WHERE h.id=%i";
			//$game_ids = $db->query($q);
			$game_ids = $db->query($q, array($house_id));

			$ball_info = array();

			foreach($game_ids as $game_id){

				//$q = "SELECT *  FROM `lotto_game_xml_link_changes` WHERE `game_id` = '".$game_id['id']."' AND `changed_on` <= '".$win_date."' ORDER BY `changed_on` DESC LIMIT 1";
				$q = "SELECT *  FROM `lotto_game_xml_link_changes` WHERE `game_id` = %i AND `changed_on` <= %s ORDER BY `changed_on` DESC LIMIT 1";
				//$check_historical = $db->queryOneRow($q);
				$check_historical = $db->queryOneRow($q, array($game_id['id'], $win_date));
		
			/*
			 * File edited: 03/06/2016 12:29 PM
			 * Purpose: display winning Number Sorting by Drawing time Desc Order Line no 33,36.
			 * 
			 */
				if($check_historical == NULL){
					 $ball_info_temp = $db->query("SELECT * FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id JOIN `lotto_game_xml_link` link ON g.id=link.game_id JOIN `xml_lotto_winning_numbers` win ON link.xml_game_id=win.game_id LEFT JOIN `lotto_bet` b ON b.game_id=g.id WHERE b.draw_date=win.draw_date AND win.draw_date='".$win_date."' AND link.game_id='".$game_id['id']."' order by win.draw_datetime desc");
					 $ball_info = array_merge($ball_info, $ball_info_temp);
				}else{
					$ball_info_temp = $db->query("SELECT * FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id JOIN `lotto_game_xml_link_changes` link ON (link.game_id=g.id AND link.id=".$check_historical['id'].") JOIN `xml_lotto_winning_numbers` win ON link.xml_game_id=win.game_id LEFT JOIN `lotto_bet` b ON b.game_id=g.id WHERE b.draw_date=win.draw_date AND win.draw_date='".$win_date."' AND link.game_id='".$game_id['id']."' order by win.draw_datetime desc");
					$ball_info = array_merge($ball_info, $ball_info_temp);
				}
			}

			//Run this query and merge it to the existing ball_info in case no bets were made on the game for that day
			//Without this, it will not return a number in that instance
			$ball_info_temp = $db->query("SELECT * FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id JOIN `lotto_game_xml_link` link ON g.id=link.game_id JOIN `xml_lotto_winning_numbers` win ON link.xml_game_id=win.game_id WHERE win.draw_date='".$win_date."' AND house_id='".$house_id."'");
			$ball_info = array_merge($ball_info, $ball_info_temp);
			
			//initialize the processed variable
			for($x=0;$x<7;$x++){
				$processed[$x] = "disabled";
				$balls[$x] = "";
			}

			if($ball_info == NULL){
				$ball_info = $db->query("SELECT * FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id JOIN `lotto_game_xml_link` link ON g.id=link.game_id WHERE h.id=".$house_id);
				foreach($ball_info as $ball){
					$processed[$ball['number_of_balls']] = "";
					$xml_game_id[$ball['number_of_balls']] = $ball['xml_game_id'];
				}
			}else{
				foreach($ball_info as $ball){
					if($ball['is_processed'] != "1"){
						$processed[$ball['number_of_balls']] = "";
					}
					$balls[$ball['number_of_balls']] = $ball['draw_numbers'];
				}
			}

			if($xml_game_id['2'] == $xml_game_id['3']){
				$processed['2'] = "disabled";
			}

			if(strlen($balls['2']) > 2){
				$balls['2'] = substr($balls['2'], 1);
				$processed['2'] = "disabled";
			}
			$response['two_ball'] = $balls['2'];
			$response['three_ball'] = $balls['3'];
			$response['four_ball'] = $balls['4'];
			$response['five_ball'] = $balls['5'];
			$response['six_ball'] = $balls['6'];
			$response['two_ball_enabled'] = $processed['2'];
			$response['three_ball_enabled'] = $processed['3'];
			$response['four_ball_enabled'] = $processed['4'];
			$response['five_ball_enabled'] = $processed['5'];
			$response['six_ball_enabled'] = $processed['6'];
			break;

		case 'update_winning_numbers':
			$response['errors'] = "";
			$win_date = $_POST['win_date'];
			$house_id = $_POST['house_id'];
			$two_ball = $_POST['two_ball'];
			$three_ball = $_POST['three_ball'];
			$four_ball = $_POST['four_ball'];
			$five_ball = $_POST['five_ball'];
			$six_ball = $_POST['six_ball'];
			
			//$ball_info = $db->query("SELECT g.number_of_balls, b.is_processed, win.draw_numbers, win.id, win.original_number, g.id AS game_id FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id JOIN `lotto_game_xml_link` link ON g.id=link.game_id JOIN `xml_lotto_winning_numbers` win ON link.xml_game_id=win.game_id LEFT JOIN `lotto_bet` b ON b.game_id=g.id WHERE win.draw_date='".$win_date."' AND house_id='".$house_id."'");
			$q = "SELECT g.number_of_balls, b.is_processed, win.draw_numbers, win.id, win.original_number, g.id AS game_id FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id JOIN `lotto_game_xml_link` link ON g.id=link.game_id JOIN `xml_lotto_winning_numbers` win ON link.xml_game_id=win.game_id LEFT JOIN `lotto_bet` b ON b.game_id=g.id WHERE win.draw_date=%s AND house_id=%i";
			$ball_info = $db->query($q, array($win_date, $house_id));

			//initialize the processed variable
			for($x=0;$x<7;$x++){
				$balls[$x] = "";
				$xml_winning_number_id[$x] = "";
				$game_id[$x] = "";
			}
			
			if($ball_info == NULL){
				//$ball_info = $db->query("SELECT g.number_of_balls, link.xml_game_id, g.id AS game_id FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id JOIN `lotto_game_xml_link` link ON g.id=link.game_id WHERE h.id=".$house_id);
				$q = "SELECT g.number_of_balls, link.xml_game_id, g.id AS game_id FROM `lotto_house` h JOIN `lotto_game` g ON h.id=g.house_id JOIN `lotto_game_xml_link` link ON g.id=link.game_id WHERE h.id=%i";
				$ball_info = $db->query($q, array($house_id));		
				
				foreach($ball_info as $ball){
					$xml_game_id[$ball['number_of_balls']] = $ball['xml_game_id'];
					$game_id[$ball['number_of_balls']] = $ball['game_id'];
				}
			}else{
				foreach($ball_info as $ball){
					$balls[$ball['number_of_balls']] = $ball['draw_numbers'];
					$xml_winning_number_id[$ball['number_of_balls']] = $ball['id'];
					$game_id[$ball['number_of_balls']] = $ball['game_id'];
					if($ball['original_number'] == ""){
						$original_number[$ball['number_of_balls']] = $ball['draw_numbers'];
					}else{
						$original_number[$ball['number_of_balls']] = $ball['original_number'];
					}
				}
			}

			$response['success'] = true;
			
			if(strlen($two_ball) != 2 && $xml_game_id['2'] != $xml_game_id['3']){
				$response['success'] = false;
				$response['errors'] .= "Invalid number of digits in 2 ball.<br>";
			}
			if(strlen($three_ball) != 3 && $xml_winning_number_id['3'] <> ""){
				$response['success'] = false;
				$response['errors'] .= "Invalid number of digits in 3 ball.<br>";
			}
			if(strlen($four_ball) != 4 && $xml_winning_number_id['4'] <> ""){
				$response['success'] = false;
				$response['errors'] .= "Invalid number of digits in 4 ball.<br>";
			}
			if(strlen($five_ball) != 5 && $xml_winning_number_id['5'] <> ""){
				$response['success'] = false;
				$response['errors'] .= "Invalid number of digits in 5 ball.<br>";
			}
			if(strlen($six_ball) != 6 && $xml_winning_number_id['6'] <> ""){
				$response['success'] = false;
				$response['errors'] .= "Invalid number of digits in 6 ball.<br>";
			}

			if($response['success']){
				if($two_ball != $balls['2']){
					if($xml_winning_number_id['2'] != ""){

						//$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers='".$two_ball."', is_override=1, overridden_by='".$session->userinfo['id']."', original_number='".$original_number['2']."' WHERE id=".$xml_winning_number_id['2'];
						$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers=%i, is_override=1, overridden_by=%i, original_number=%i, is_archived=0 WHERE id=%i";
						$db->queryDirect($q, array($two_ball, $session->userinfo['id'], $original_number['2'], $xml_winning_number_id['2']));

					}else{
						if($xml_game_id['2'] != $xml_game_id['3']){
							//$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES ('".$xml_game_id['2']."','".$win_date."','".$two_ball."','1','".$session->userinfo['id']."')";
							$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES (%s,%s,%i,'1',%i)";
							$db->queryDirect($q, array($xml_game_id['2'], $win_date, $two_ball, $session->userinfo['id']));

						}
					}
					//$db->queryDirect("UPDATE lotto_bet SET is_processed=0 WHERE (ball_string='".$two_ball."' OR ball_string='".$three_ball."' OR ball_string='".$four_ball."' OR ball_string='".$five_ball."' OR ball_string='".$six_ball."') AND draw_date='".$win_date."' AND game_id='".$game_id['2']."'");
					$q = "UPDATE lotto_bet SET is_processed=0 WHERE (ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s) AND draw_date=%s AND game_id=%i";
					$db->queryDirect($q, array($two_ball, $three_ball, $four_ball, $five_ball, $six_ball, $win_date, $game_id['2']));
					
					//$db->queryDirect($q);
				}
				if($three_ball != $balls['3']){
					if($xml_winning_number_id['3'] != ""){

						//$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers='".$three_ball."', is_override=1, overridden_by='".$session->userinfo['id']."', original_number='".$original_number['3']."' WHERE id=".$xml_winning_number_id['3'];
						$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers=%i, is_override=1, overridden_by=%i, original_number=%i, is_archived=0 WHERE id=%i";
						$db->queryDirect($q, array($three_ball, $session->userinfo['id'], $original_number['3'], $xml_winning_number_id['3']));

					}else{
						$response['errors'] = json_encode($xml_game_id);
						//$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES ('".$xml_game_id['3']."','".$win_date."','".$three_ball."','1','".$session->userinfo['id']."')";
						$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES (%s,%s,%i,'1',%i)";
						$db->queryDirect($q, array($xml_game_id['3'], $win_date, $three_ball, $session->userinfo['id']));

					}

					//$db->queryDirect("UPDATE lotto_bet SET is_processed=0 WHERE (ball_string='".$two_ball."' OR ball_string='".$three_ball."' OR ball_string='".$four_ball."' OR ball_string='".$five_ball."' OR ball_string='".$six_ball."') AND draw_date='".$win_date."' AND game_id='".$game_id['3']."'");
					$q = "UPDATE lotto_bet SET is_processed=0 WHERE (ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s) AND draw_date=%s AND game_id=%i";
					$db->queryDirect($q, array($two_ball, $three_ball, $four_ball, $five_ball, $six_ball, $win_date, $game_id['3']));

				}
				if($four_ball != $balls['4']){
					if($xml_winning_number_id['4'] != ""){

						//$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers='".$four_ball."', is_override=1, overridden_by='".$session->userinfo['id']."', original_number='".$original_number['4']."' WHERE id=".$xml_winning_number_id['4'];
						$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers=%s, is_override=1, overridden_by=%i, original_number=%i, is_archived=0 WHERE id=%i";
						$db->queryDirect($q, array($four_ball, $session->userinfo['id'], $original_number['4'], $xml_winning_number_id['4']));

					}else{

						//$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES ('".$xml_game_id['4']."','".$win_date."','".$four_ball."','1','".$session->userinfo['id']."')";
						$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES (%s,%s,%s,'1',%i)";
						$db->queryDirect($q, array($xml_game_id['4'], $win_date, $four_ball, $session->userinfo['id']));

					}

					//$db->queryDirect("UPDATE lotto_bet SET is_processed=0 WHERE (ball_string='".$two_ball."' OR ball_string='".$three_ball."' OR ball_string='".$four_ball."' OR ball_string='".$five_ball."' OR ball_string='".$six_ball."') AND draw_date='".$win_date."' AND game_id='".$game_id['4']."'");
					$q = "UPDATE lotto_bet SET is_processed=0 WHERE (ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s) AND draw_date=%s AND game_id=%i";
					$db->queryDirect($q, array($two_ball, $three_ball, $four_ball, $five_ball, $six_ball, $win_date, $game_id['4']));

				}
				if($five_ball != $balls['5']){
					if($xml_winning_number_id['5'] != ""){

						//$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers='".$five_ball."', is_override=1, overridden_by='".$session->userinfo['id']."', original_number='".$original_number['5']."' WHERE id=".$xml_winning_number_id['5'];
						$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers=%s, is_override=1, overridden_by=%i, original_number=%i, is_archived=0 WHERE id=%i";
						$db->queryDirect($q, array($five_ball, $session->userinfo['id'], $original_number['5'], $xml_winning_number_id['5']));

					}else{

						//$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES ('".$xml_game_id['5']."','".$win_date."','".$five_ball."','1','".$session->userinfo['id']."')";
						$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES (%s,%s,%s,'1',%i)";
						$db->queryDirect($q, array($xml_game_id['5'], $win_date, $five_ball, $session->userinfo['id']));

					}

					//$db->queryDirect("UPDATE lotto_bet SET is_processed=0 WHERE (ball_string='".$two_ball."' OR ball_string='".$three_ball."' OR ball_string='".$four_ball."' OR ball_string='".$five_ball."' OR ball_string='".$six_ball."') AND draw_date='".$win_date."' AND game_id='".$game_id['5']."'");
					$q = "UPDATE lotto_bet SET is_processed=0 WHERE (ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s) AND draw_date=%s AND game_id=%i";
					$db->queryDirect($q, array($two_ball, $three_ball, $four_ball, $five_ball, $six_ball, $win_date, $game_id['5']));

				}
				if($six_ball != $balls['6']){
					if($xml_winning_number_id['6'] != ""){

						//$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers='".$six_ball."', is_override=1, overridden_by='".$session->userinfo['id']."', original_number='".$original_number['6']."' WHERE id=".$xml_winning_number_id['6'];
						$q = "UPDATE `xml_lotto_winning_numbers` SET draw_numbers=%s, is_override=1, overridden_by=%i, original_number=%i, is_archived=0 WHERE id=%i";
						$db->queryDirect($q, array($six_ball, $session->userinfo['id'], $original_number['6'], $xml_winning_number_id['6']));

					}else{

						//$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES ('".$xml_game_id['6']."','".$win_date."','".$six_ball."','1','".$session->userinfo['id']."')";
						$q = "INSERT INTO `xml_lotto_winning_numbers` (`game_id`,`draw_date`,`draw_numbers`,`is_override`,`overridden_by`) VALUES (%s,%s,%s,'1',%i)";					
						$db->queryDirect($q, array($xml_game_id['6'], $win_date, $six_ball, $session->userinfo['id']));

					}

					//$db->queryDirect("UPDATE lotto_bet SET is_processed=0 WHERE (ball_string='".$two_ball."' OR ball_string='".$three_ball."' OR ball_string='".$four_ball."' OR ball_string='".$five_ball."' OR ball_string='".$six_ball."') AND draw_date='".$win_date."' AND game_id='".$game_id['6']."'");
					$q = "UPDATE lotto_bet SET is_processed=0 WHERE (ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s OR ball_string=%s) AND draw_date=%s AND game_id=%i";
					$db->queryDirect($q, array($two_ball, $three_ball, $four_ball, $five_ball, $six_ball, $win_date, $game_id['6']));

				}
			}
			break;
	}

	echo json_encode($response);
