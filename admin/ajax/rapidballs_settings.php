<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	// Enabled?
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('rapidballs_enabled', %i);";
	$result = $db->query($q, array($_POST['rapidballs_enabled']));
	
	$response['success'] = 'true';
	echo json_encode($response);