<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	
	$monday = date("Y-m-d",strtotime('monday this week'));
	
	
	if(!empty($_POST['action']) && $_POST['action'] == "lotto_loss"){
		
	$q = "SELECT SUM(lb.bet_amount) as loss, lt.user_id FROM lotto_bet as lb INNER JOIN lotto_ticket as lt ON lb.ticket_id = lt.ticket_id AND lt.user_id != - 9999 AND DATE(lb.processed_date_time) <= '".date('Y-m-d')."' AND DATE(lb.processed_date_time) >= '".$monday."' AND lb.is_processed = 1 AND lb.is_winner = 0 GROUP BY lt.user_id";
       $lotto_user = $db->query($q);
       
     
      if(!empty($lotto_user)){
        	foreach($lotto_user as $lotto_value){
        		$sumArray[$lotto_value['user_id']]['lotto'] =$lotto_value['loss'];	
        		}
       }       
        $number_of_user= 0;   
        $lotto_rb_amount = 0;
        if(!empty($sumArray)) {
        	foreach($sumArray as $key=>$calculation){					
			       		
        		if(!empty($calculation['lotto'])){
        			$lotto_rb_amount += round((($_POST['percentage'] * $calculation['lotto'])/100),2);
        			$number_of_user++;			
        		}        			
        		
        	}  
        	}
        	if($lotto_rb_amount == 0){
        		 $number_of_user =0;	
        	}
        $response['success'] = 'true';
	$response['number_of_users'] = $number_of_user;
	$response['total_lotto_amount'] = $lotto_rb_amount;
	echo json_encode($response);     
	}
	if(!empty($_POST['action']) && $_POST['action'] == "casino_loss"){
	
	$q = "SELECT  ct.user_id, ct.amount , ct.transaction_type_id FROM customer_transaction as ct where
    				ct.transaction_type_id IN (1,2) and DATE(ct.transaction_date) <= '".date('Y-m-d')."' AND DATE(ct.transaction_date) >= '".$monday."'";
        		$customer_transaction = $db->query($q);	 			
			foreach ($customer_transaction as $k=>$subArray) {  				
  					$sumArray[$subArray['user_id']]['casino'] +=$subArray['amount'];  				
			}        		
        		$number_of_user = 0;	
        	
        		$casino_rb_amount = 0;
        		  if(!empty($sumArray)) { 
        		foreach($sumArray as $key=>$calculation){      		
        			
        			if(!empty($calculation['casino']) && $calculation['casino']<0){
        			$casino_rb_amount += round((($_POST['percentage'] * $calculation['casino'])/100),2);
        			$number_of_user++;
        			}        			
			}
			}
			if($casino_rb_amount == 0){
        		 $number_of_user =0;	
        		}
	$response['success'] = 'true';
	$response['number_of_users'] = $number_of_user;
	$response['total_lotto_amount'] = abs($casino_rb_amount);
	echo json_encode($response);
	}
	
	if(!empty($_POST['action']) && $_POST['action'] == "sports_loss"){
	
	$q = "SELECT  ct.user_id, ct.amount , ct.transaction_type_id FROM customer_transaction as ct where
    				ct.transaction_type_id IN (17,18,45) and DATE(ct.transaction_date) <= '".date('Y-m-d')."' AND DATE(ct.transaction_date) >= '".$monday."'";
        		$customer_transaction = $db->query($q);	 			
			foreach ($customer_transaction as $k=>$subArray) {  				
  					$sumArray[$subArray['user_id']]['sports'] +=$subArray['amount'];  				
			}        		
        		$number_of_user = 0;	
        	
        		$sports_rb_amount = 0; 
        		  if(!empty($sumArray)) {
        		foreach($sumArray as $key=>$calculation){      		
        			
        			if(!empty($calculation['sports']) && $calculation['sports']<0){
        			$sports_rb_amount += round((($_POST['percentage'] * $calculation['sports'])/100),2);
        			$number_of_user++;
        			}        			
			}
			}
			if($sports_rb_amount == 0){
        		 $number_of_user =0;	
        		}
	$response['success'] = 'true';
	$response['number_of_users'] = $number_of_user;
	$response['total_lotto_amount'] = abs($sports_rb_amount);
	echo json_encode($response);
	}
	
	if(!empty($_POST['action']) && $_POST['action'] == "rapidball_loss"){
		
	$q = "SELECT SUM(rb.bet_amount) as loss, rt.user_id FROM rapidballs_ticket rt INNER JOIN rapidballs_bet rb ON rb.ticket_id = rt.id AND is_processed = 1 AND is_winner = 0 AND DATE(rb.processed_datetime) <= '".date('Y-m-d')."' AND DATE(rb.processed_datetime) >= '".$monday."' GROUP BY rt.user_id";        		
        $rb_user = $db->query($q);  
        	if(!empty($rb_user)){
        		foreach($rb_user as $rb_value){
        			$sumArray[$rb_value['user_id']]['rb'] =$rb_value['loss'];	
        		}
        	}  
        $number_of_user= 0; 
        $rapidball_rb_amount = 0;  
          if(!empty($sumArray)) { 
        	foreach($sumArray as $key=>$calculation){        				
					
			         		
        		if(!empty($calculation['rb'])){
        			$rapidball_rb_amount += round((($_POST['percentage'] * $calculation['rb'])/100),2);
        			$number_of_user++;			
        		}        			
        		
        	}  
        	}
        	if($rapidball_rb_amount == 0){
        		 $number_of_user =0;	
        	}
        $response['success'] = 'true';
	$response['number_of_users'] = $number_of_user;
	$response['total_lotto_amount'] = $rapidball_rb_amount;
	echo json_encode($response);     
	}
	
	
	
	
