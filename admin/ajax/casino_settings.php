<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	// Enabled?
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('casino_api_chetu', %s);";
	$result = $db->query($q, array($_POST['casino_api_chetu']));
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('casino_api_multislot', %s);";
	$result = $db->query($q, array($_POST['casino_api_multislot']));
	$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('casino_enabled', %i);";
	$result = $db->query($q, array($_POST['casino_enabled']));
	
	$response['success'] = 'true';
	echo json_encode($response);
