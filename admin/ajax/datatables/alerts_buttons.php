<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$table = "alert_rules";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	switch ($action){
		case 'enable':
			$q = "UPDATE `$table` SET `is_disabled` = 0 WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			break;
		case 'disable':
			$q = "UPDATE `$table` SET `is_disabled` = 1 WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			break;
		case 'delete':
			$q = "DELETE FROM `$table` WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			break;
		case 'update':
			$failed = false;
			
			$q = "UPDATE `$table` SET `rule_name` = '".$_POST['rule_name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `$table` SET `table_name` = '".$_POST['table_name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `$table` SET `column_name` = '".$_POST['column_name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `$table` SET `item_name` = '".$_POST['item_name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `$table` SET `value` = '".$_POST['value']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `$table` SET `column_name` = '".$_POST['column_name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `$table` SET `alert_type` = '".$_POST['alert_type']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `$table` SET `function_name` = '".$_POST['function_name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			if($failed == true){
				die();
			}
			break;
		case 'insert':
			$q = "INSERT INTO `$table` (`id`, `rule_name`, `table_name`, `column_name`, `comparison`, `value`, `alert_type`, `item_name`,`function_name`) VALUES (NULL, '".$_POST['rule_name']."', '".$_POST['table_name']."', '".$_POST['column_name']."', '".$_POST['comparison']."', '".$_POST['value']."', '".$_POST['alert_type']."', '".$_POST['item_name']."','".$_POST['function_name']."')";
			$result = $db->queryInsert($q);
			if($result <= 0){
				echo $q;
				die();
			}
			break;
		default:
		   die("Invalid Action");
	}
	
	echo json_encode($result);
