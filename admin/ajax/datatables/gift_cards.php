<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'gift_cards';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'g.id',
			'dt' => 'DT_RowId',
			'field' => 'ticket_id',
			'as' => 'ticket_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'g.card_number',
			'dt'        => 'card_number',
			'field' => 'card_number',
			'as' => 'card_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'SUM(gcr.amount)',
			'dt'        => 'amount_redeemed',
			'field' => 'amount_redeemed',
			'as' => 'amount_redeemed',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'g.security_code',
			'dt'        => 'security_code',
			'field' => 'security_code',
			'as' => 'security_code',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'g.expires_on',
			'dt'        => 'expires_on',
			'field' => 'expires_on',
			'as' => 'expires_on',
			'formatter' => function( $d, $row ) {
				if($d){
					return date("m/d/Y", strtotime($d));
				}else{
					return "Never";
				}
			}
		),
		array(
			'db'        => 'ua.firstname',
			'dt'        => 'added_by_firstname',
			'field' => 'added_by_firstname',
			'as' => 'added_by_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ua.lastname',
			'dt'        => 'added_by_lastname',
			'field' => 'added_by_lastname',
			'as' => 'added_by_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'g.added_by',
			'dt'        => 'added_by_id',
			'field' => 'added_by_id',
			'as' => 'added_by_id',
			'formatter' => function( $d, $row ) {
				$d;
			}
		),
		array(
			'db'        => 'ua.id',
			'dt'        => 'added_by',
			'field' => 'added_by',
			'as' => 'added_by',
			'formatter' => function( $d, $row ) {
				return $row['added_by_firstname']." ".$row['added_by_lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'us.firstname',
			'dt'        => 'sold_by_firstname',
			'field' => 'sold_by_firstname',
			'as' => 'sold_by_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'us.lastname',
			'dt'        => 'sold_by_lastname',
			'field' => 'sold_by_lastname',
			'as' => 'sold_by_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'g.sold_by',
			'dt'        => 'sold_by_id',
			'field' => 'sold_by_id',
			'as' => 'sold_by_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'us.id',
			'dt'        => 'sold_by',
			'field' => 'sold_by',
			'as' => 'sold_by',
			'formatter' => function( $d, $row ) {
				return $row['sold_by_firstname']." ".$row['sold_by_lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'g.amount',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'g.is_sold',
			'dt'        => 'is_sold',
			'field' => 'is_sold',
			'as' => 'is_sold',
			'formatter' => function( $d, $row ) {
				return $d == "1" ? "Sold by ".$row['sold_by_firstname']." ".$row['sold_by_lastname']." (".$d.") on ".$row['sold_on'] : "Unsold";
			}
		),
		array(
			'db'        => 'g.sold_on',
			'dt'        => 'sold_on',
			'field' => 'sold_on',
			'as' => 'sold_on',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));;
			}
		)
	);

	$join_query = "FROM `gift_cards` AS `g` LEFT JOIN `gift_card_redemptions` AS `gcr` ON `gcr`.`card_number`=`g`.`card_number` LEFT JOIN `users` AS `ua` ON `ua`.`id`=`g`.`added_by` LEFT JOIN `users` AS `us` ON `us`.`id`=`g`.`sold_by`";
	
	//$extra_where = "";
	
	$group_by = "GROUP BY `g`.`id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
