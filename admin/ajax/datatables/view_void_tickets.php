<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_ticket';
	
	// Table's primary key
	$primary_key = 'ticket_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 't.ticket_id',
			'dt' => 'DT_RowId',
			'field' => 'ticket_id',
			'as' => 'ticket_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 't.ticket_number',
			'dt'        => 'ticket_number',
			'field' => 'ticket_number',
			'as' => 'ticket_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'c.firstname',
			'dt'        => 'c_firstname',
			'field' => 'c_firstname',
			'as' => 'c_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'c.lastname',
			'dt'        => 'c_lastname',
			'field' => 'c_lastname',
			'as' => 'c_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 't.cashier_id',
			'dt'        => 'cashier_id',
			'field' => 'cashier_id',
			'as' => 'cashier_id',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "Online" : ($d == "-9999" ? "Online" : $row['c_firstname']." ".$row['c_lastname']." (".$d.")");
			}
		),
		array(
			'db'        => 'l.name',
			'dt'        => 'location_id',
			'field' => 'location_id',
			'as' => 'location_id',
			'formatter' => function( $d, $row ) {
				return $d == "" ? "Online" : $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'u_firstname',
			'field' => 'u_firstname',
			'as' => 'u_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'u_lastname',
			'field' => 'u_lastname',
			'as' => 'u_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 't.user_id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d == "-9999" ? "Panel" : $row['u_firstname']." ".$row['u_lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 't.total',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'wp.total_payout',
			'dt'        => 'amount_won',
			'field' => 'amount_won',
			'as' => 'amount_won',
			'formatter' => function( $d, $row ) {
				return "".number_format(abs($d))." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 't.shift_id',
			'dt'        => 'shift_id',
			'field' => 'shift_id',
			'as' => 'shift_id',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "Online" : $d;
			}
		),
		array(
			'db'        => 't.purchase_date',
			'dt'        => 'purchase_date',
			'field' => 'purchase_date',
			'as' => 'purchase_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i:s A", strtotime($d));
			}
		),
		array(
			'db'        => 't.voided_on',
			'dt'        => 'voided_on',
			'field' => 'voided_on',
			'as' => 'voided_on',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i:s A", strtotime($d));
			}
		),
		array(
			'db'        => 'v.firstname',
			'dt'        => 'v_firstname',
			'field' => 'v_firstname',
			'as' => 'v_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'v.lastname',
			'dt'        => 'v_lastname',
			'field' => 'v_lastname',
			'as' => 'v_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 't.voided_by',
			'dt'        => 'voided_by_id',
			'field' => 'voided_by_id',
			'as' => 'voided_by_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 't.is_voided',
			'dt'        => 'is_voided',
			'field' => 'is_voided',
			'as' => 'is_voided',
			'formatter' => function( $d, $row ) {
				return $d == 1 ? "<span style='color: red;'>Voided by ".$row['v_firstname']." ".$row['v_lastname']." (".$row['voided_by_id'].") <br>on ".date("m/d/Y", strtotime($row['voided_on']))." at ".date("h:i:s A", strtotime($row['voided_on']))."</span>" : "<span style='color: green;'>Active</span>";
			}
		),
		array(
			'db'        => 't.is_voided',
			'dt'        => 'void_status',
			'field' => 'void_status',
			'as' => 'void_status',
			'formatter' => function( $d, $row ) {
				return $d == 1 ? "Void" : "Active";
			}
		),
		array(
			'db'        => 't.voided_comment',
			'dt'        => 'voided_comment',
			'field' => 'voided_comment',
			'as' => 'voided_comment',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'wp.status_id',
			'dt'        => 'status_id',
			'field' => 'status_id',
			'as' => 'status_id',
			'formatter' => function( $d, $row ) {
				if($d == 1){
                                  $status = "Paid";
                                } else if($d == 3){
                                  $status = "Partial Paid";
                                } else if($d == 2){
                                  $status = "Pending";
                                } else {
                                   $status = "-";
                                }
                           return $status;
			}
		)
	);

	$join_query = "FROM `lotto_ticket` AS `t` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id`=`l`.`id` LEFT JOIN `winner_payout` AS `wp` ON `t`.`ticket_number`=`wp`.`ticket_number` LEFT JOIN `users` AS `c` ON `c`.`id`=`t`.`cashier_id` LEFT JOIN `users` AS `u` ON `u`.`id`=`t`.`user_id` LEFT JOIN `users` AS `v` ON `v`.`id`=`t`.`voided_by`";
	
	if($_GET['ticket_number'] != ""){
		$extra_where = "t.ticket_number = '".$_GET['ticket_number']."'";
	}
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
