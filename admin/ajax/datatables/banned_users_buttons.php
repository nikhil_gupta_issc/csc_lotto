<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "banned_users";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$username = isset($_POST['username']) ? $_POST['username'] : NULL;
	
	// get user id from users table
	$q = "SELECT `id` FROM `users` WHERE username='".$username."'";
	$u_info = $db->queryOneRow($q);
	$id = $u_info['id'];

	switch ($action){
		case 'reactivate':
			// remove from banned users table
			$q = "DELETE FROM $table WHERE username=%s";
			$db->queryDirect($q, array($username));
			$response['result'] = true;
			
			// update users table
			$q = "UPDATE `users` SET `deleted` = '0' WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			if($response['result'] == true){
				// clear who deleted this user
				$q = "UPDATE `users` SET `deleted_by` = NULL WHERE id=".$id;
				$response['result'] = $db->queryDirect($q);
				
				// update related customer account
				$q = "UPDATE `customers` SET `is_disabled` = '0', `disabled_by` = NULL WHERE user_id=".$id;
				$response['result'] = $db->queryDirect($q);
				
				// update related cashier account
				$q = "UPDATE `panel_user` SET `is_disabled` = '0', `disabled_by` = NULL WHERE user_id=".$id;
				$response['result'] = $db->queryDirect($q);
				
				// send email to notify user.
				$text_body = "The CSCLotto account linked to this email address has been reactivated and is no longer disabled.\n\n";
				$html_body = "<h2>Account No Longer Disabled</h2>The CSCLotto account linked to this email address has been reactivated and is no longer disabled.<br><br>";
				$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Account Reactivated", $html_body, $text_body);
			}
			
			break;
		default:
			$response['result'] = false;
			$response['errors'] = "An unexpected error occurred";
	}
	
	echo json_encode($response);