<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "customers";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'enable':
			$q = "SELECT `user_id` FROM `customers` WHERE `customer_id`=%i";
			$user = $db->queryOneRow($q, array($id));
			
			$q = "UPDATE `dormant_log` SET `dormant_end` = %s WHERE `user_id`=%i AND `dormant_end` IS NULL";
			$db->queryDirect($q, array($now, $user['user_id']));
		
			$q = "UPDATE $table SET `is_disabled` = 0 WHERE customer_id=%i";
			$db->queryDirect($q, array($id));
			$response['result'] = true;
			$response['query'] = $q;
			
			// send email to notify user.
			$text_body = "The CSCLotto account linked to this email address has been reactivated and is no longer disabled.\n\n";
			$html_body = "<h2>Account No Longer Disabled</h2>The CSCLotto account linked to this email address has been reactivated and is no longer disabled.<br><br>";
			$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Account Reactivated", $html_body, $text_body);
			break;
		case 'disable':
			$q = "UPDATE $table SET `is_disabled` = 1, `disabled_by` = ".$session->userinfo['id']." WHERE customer_id=".$id;
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			
			// send email to notify user.
			$text_body = "The CSCLotto account linked to this email address has been disabled and is no longer usable.\n\n Please contact support with questions concerning this.";
			$html_body = "<h2>Account Disabled</h2>The CSCLotto account linked to this email address has been disabled and is no longer usable.<br><br> Please contact support with questions concerning this.<br><br>";
			$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Account Disabled", $html_body, $text_body);
			break;
		case 'unlock':
			$q = "SELECT user_id FROM `customers` WHERE customer_id=".$id.";";
			$user_id = $db->queryOneRow($q);

			$q = "UPDATE users SET `locked` = 0, `locked_expiration` = NULL WHERE id=".$user_id["user_id"].";";
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			break;
		case 'update':
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE customer_id=".$_POST['cust_id'];
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
			
			// process user fields
			$table = "users";
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE id=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
			
			// process user_info fields
			$table = "user_info";
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE user_id=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
			
			break;
		case 'insert':			
			// must be done from customer interface now to verify location
			
			break;
		/* case 'update_verification':
			$count_verified = $_POST['verified_drivers_license'] + $_POST['verified_voters_card'] + $_POST['verified_passport'] + $_POST['national_security'];;

			if($count_verified >= 2){
				$is_verified = 1;
			}else{
				$is_verified = 0;
			}
			$image = explode('?',$_POST['profile_picture_path']);
			$image_id = explode('?',$_POST['id_image_path']);
			$q = "UPDATE `$table` SET `id_image_path`='".$image_id[0]."',`profile_picture_path` = '".$image[0]."' ,`drivers_license` = '".$_POST['drivers_license']."', `voters_card` = '".$_POST['voters_card']."', `passport` = '".$_POST['passport']."', `national_security` = '".$_POST['national_security']."', `is_drivers_license_verified` = '".$_POST['verified_drivers_license']."', `is_voters_card_verified` = '".$_POST['verified_voters_card']."', `is_passport_verified` = '".$_POST['verified_passport']."', `is_national_security_verified` = '".$_POST['verified_national_security']."', `is_verified` = '".$is_verified."', `verified_on` = '$now', `verified_by`=".$session->userinfo['id']." WHERE customer_id=$id;";
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['result'] = false;
				$response['query'] = $q;
			}
			
			break; */
		case 'update_verification':
			$count_verified = $_POST['verified_drivers_license'] + $_POST['verified_voters_card'] + $_POST['verified_passport'] + $_POST['national_security'];;
			
			if($count_verified >= 2){
				$is_verified = 1;
			}else{
				$is_verified = 0;
			}
			$image = explode('?',$_POST['profile_picture_path']);
			$image_id = explode('?',$_POST['id_image_path']);
			$image_driving = explode('?',$_POST['driving_picture_path']);
			
			$image_nsb = explode('?',$_POST['nsb_picture_path']);
			
			
			$q = "UPDATE `$table` 
				SET `id_image_path`='".$image_id[0]."',
				`profile_picture_path` = '".$image[0]."' ,
				`id_driving_path` = '".$image_driving[0]."' ,
				`id_nsb_path` = '".$image_nsb[0]."' ,
				
				`drivers_license` = '".$_POST['drivers_license']."', 
				`voters_card` = '".$_POST['voters_card']."', `passport` = '".$_POST['passport']."', `national_security` = '".$_POST['national_security']."', `is_drivers_license_verified` = '".$_POST['verified_drivers_license']."', `is_voters_card_verified` = '".$_POST['verified_voters_card']."', 
				`is_passport_verified` = '".$_POST['verified_passport']."', 
				`is_national_security_verified` = '".$_POST['verified_national_security']."', 
				`is_verified` = '".$is_verified."',
				 
				`verified_on` = '$now', `verified_by`=".$session->userinfo['id']." WHERE customer_id=$id;";
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['result'] = false;
				$response['query'] = $q;
			}
			
			break;
			
		case 'update_photo':
			$photo_arr = explode("&", $_POST['photo_path']);
			$photo_path = $photo_arr[0];
		
			$q = "UPDATE `$table` SET `profile_picture_path` = '".$photo_path."' WHERE customer_id=$id;";
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['result'] = false;
				$response['query'] = $q;
			}
			
			break;
		case 'update_security':		
			// handle user table changes
			$q = "UPDATE `users` SET `locked` = '".$_POST['locked']."', `google_auth_enabled` = '".$_POST['google_auth_enabled']."', `force_password_change` = '".$_POST['force_password_change']."', `locked_expiration` = '".$_POST['locked_expiration']."', `userlevel` = '".$_POST['userlevel']."' WHERE `id`=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			if($response['result'] != true){
				$response['result'] = false;
				$response['query'] = $q;
				break;
			}
			
			// handle customer table changes
			$q = "UPDATE `customers` SET `pin` = '".$_POST['pin']."' WHERE `user_id`=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			if($response['result'] != true){
				$response['result'] = false;
				$response['query'] = $q;
				break;
			}
			
			if($_POST['pass'] != ""){
				if($_POST['pass'] == $_POST['conf_pass']){
					// change salt
					$usersalt = $session->generateRandStr(8);
					$database->updateUserField("", "usersalt", $usersalt, $_POST['user_id']);
					$response['result'] = $db->queryDirect($q);
					if($response['result'] != true){
						$response['result'] = false;
						$response['query'] = '$database->updateUserField("", "usersalt", $usersalt, '.$_POST['user_id'].')';
						break;
					}
					
					// change password hash
					$database->updateUserField("", "password", sha1($usersalt.$_POST['pass']), $_POST['user_id']);
					$response['result'] = $db->queryDirect($q);
					if($response['result'] != true){
						$response['result'] = false;
						$response['query'] = '$database->updateUserField("", "password", sha1($usersalt.'.$_POST['pass'].'), '.$_POST['user_id'].')';
						break;
					}
				}else{
					$response['result'] = false;
					$response['query'] = 'Password and confirmation did not match';
					break;
				}
			}
			
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
