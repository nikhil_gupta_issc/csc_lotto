<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$table = "gift_cards";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	switch ($action){
		case 'insert':
			$errors = "";
		
			if(strlen($_POST['card_number']) < 5){
				$errors .= "The card number must be at least 5 characters.<br>";
			}
			
			if(!is_numeric(trim($_POST['amount'], "$"))){
				$errors .= "The amount must be numeric<br>";
			}
		
			$added_on_dt = new DateTime();
			$added_on = $added_on_dt->format("Y-m-d H:i:s");
		
			if($errors == ""){
				$q = "INSERT INTO `$table` (`id`, `card_number`, `security_code`, `amount`, `added_on`, `added_by`, `expires_on`, `is_sold`, `sold_by`, `sold_on`, `transaction_id`) VALUES (NULL, '".$_POST['card_number']."', '".$_POST['security_code']."', '".$_POST['amount']."', '".$added_on."', '".$session->userinfo['id']."', NULL, '0', NULL, NULL, NULL);";
				$result2 = $db->queryInsert($q);
				if($result2 <= 0){
					echo $q;
					die();
				}
				$result['successful'] = "true";
			}else{
				$result['successful'] = "false";
				$result['errors'] = $errors;
			}
			break;
		default:
			// import
			if(isset($_FILES["import_file"])){ 
				// Store data from csv file into an array
				$r = 0;
				$col_count = 99;
				$result['success_count'] = 0;
				$handle = fopen($_FILES['import_file']['tmp_name'], "r");
				while(($data = fgetcsv($handle, 1000, "\t")) !== FALSE){
					$col_count = count($data);
					for($c=0; $c<$col_count; $c++){
						$import_data[$r][$c] = $data[$c];
					}
					
					$expires_on_dt = new DateTime($import_data[$r][2]);
					$expires_on = $expires_on_dt->format("Y-m-d");
					
					// try to import this row
					$q = "INSERT INTO `$table` (`id`, `card_number`, `security_code`, `amount`, `added_on`, `added_by`, `expires_on`, `is_sold`, `sold_by`, `sold_on`, `transaction_id`) VALUES (NULL, '".$import_data[$r][0]."', '".$import_data[$r][1]."', '".$import_data[$r][3]."', '".$added_on."', '".$session->userinfo['id']."', '".$expires_on."', '0', NULL, NULL, NULL);";
					if($db->queryInsert($q) > 0){
						$result['success_count']++;
					}
					$r++;
				}
			}
			
			$result['total_count'] = $r;			
			break;
	}
	
	echo json_encode($result);