<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user_transaction';

	// Table's primary key
	$primary_key = 'id';
	
	// get the island id
	$island_id = $_GET['island_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'put.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'pl.id',
			'dt'        => 'location_id',
			'field' => 'location_id',
			'as' => 'location_id',
			'formatter' => function( $d, $row ) {
				return "loc_".$d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'cashier',
			'field' => 'cashier',
			'as' => 'cashier',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db'        => 'pl.name',
			'dt'        => 'location',
			'field' => 'location',
			'as' => 'location',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN put.amount < 0 THEN put.amount END)',
			'dt'        => 'expense',
			'field' => 'expense',
			'as' => 'expense',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN put.amount > 0 THEN put.amount END)',
			'dt'        => 'income',
			'field' => 'income',
			'as' => 'income',
			'formatter' => function( $d, $row ) {
				return "<a href='/admin/cashier_reporting/cashier_lotto_sales/index.php?cashier=".$row['user_id']."'>".number_format($d)." ".CURRENCY_FORMAT."</a>";
			}
		),
		array(
			'db'        => 'SUM(put.amount)',
			'dt'        => 'net',
			'field' => 'net',
			'as' => 'net',
			'formatter' => function( $d, $row ) {
				return "".number_format($d-$row['commission'])." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'pl.id',
			'dt'        => 'transfer',
			'field' => 'transfer',
			'as' => 'transfer',
			'formatter' => function( $d, $row ) {
				return "N/A";
			}
		),
		array(
			'db'        => 'SUM(t.commission_amount)',
			'dt'        => 'commission',
			'field' => 'commission',
			'as' => 'commission',
			'formatter' => function( $d, $row ) {
				return "<a href='/admin/cashier_reporting/cashier_commission/index.php?cashier=".$row['user_id']."'>".number_format($d)." ".CURRENCY_FORMAT."</a>";
			}
		)
	);

	$join_query = "FROM `panel_user_transaction` AS `put` JOIN `panel_location` AS `pl` ON `put`.`location_id`=`pl`.`id` JOIN `island` AS `i` ON `i`.`id`=`pl`.`island_id` LEFT JOIN `users` AS `u` ON `u`.`id`=`put`.`made_by` LEFT JOIN `lotto_ticket` AS `t` ON `t`.`ticket_number`=`put`.`ticket_number`";
	
	$date = new DateTime('now');
	$date_str = $date->format("Y-m-d");
	$extra_where = "i.id='".$island_id."' AND CAST(`put`.`transaction_date` AS DATE) = '".$date_str."'";
	
	$group_by = "GROUP BY `put`.`made_by`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
