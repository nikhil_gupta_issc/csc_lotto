<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$table = "lotto_ticket";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	
	switch ($action){
		case 'void':
			$ticket_info = $db->query("SELECT * FROM `lotto_ticket` t JOIN `lotto_bet` b ON t.ticket_id=b.ticket_id WHERE t.ticket_id=".$id);
			foreach($ticket_info as $bet){
				if($bet['is_processed'] == 1){
					$return = array("success" => "false", "errors" => "Ticket Already Has Processed Bets");
					echo json_encode($return);
					die();
				}
			}

			$q = "UPDATE `$table` SET `is_voided` = 1 WHERE ticket_id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			
			$q = "UPDATE `$table` SET `voided_by` = '".$session->userinfo['id']."' WHERE ticket_id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			
			$q = "UPDATE `$table` SET `voided_on` = '$now' WHERE ticket_id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			
			$q = "UPDATE `$table` SET `voided_comment` = '".$_POST['voided_comment']."' WHERE ticket_id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			
			break;
		case 'active':
			$q = "UPDATE `$table` SET `is_voided` = 0 WHERE ticket_id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			break;
		case 'view details':
			if($_POST['type']=="cashier"){
				$q = "SELECT u.firstname as user_first, u.lastname as user_last, u.id, l.name FROM `$table` t LEFT JOIN `users` u ON u.id=t.cashier_id LEFT JOIN `panel_location` l ON l.id=t.location_id WHERE ticket_id=".$id;
				$data = $db->queryOneRow($q);
				$result = array("name"=>$data["user_first"]." ".$data["user_last"], "location"=>$data["name"]);
			}else{
				$q = "SELECT u.firstname as user_first, u.lastname as user_last, u.id FROM `$table` t LEFT JOIN `users` u ON u.id=t.user_id WHERE ticket_id=".$id;
				$data = $db->queryOneRow($q);
				$result = array("name"=>$data["user_first"]." ".$data["user_last"]);
			}
			
			break;
		default:
		   die("Invalid Action");
	}
	
	echo json_encode($result);