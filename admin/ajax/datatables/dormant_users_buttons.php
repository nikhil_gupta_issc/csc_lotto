<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "users";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$username = isset($_POST['username']) ? $_POST['username'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'enable':
			$q = "UPDATE `dormant_log` SET `dormant_end` = %s WHERE `user_id`=%i AND `dormant_end` IS NULL";
			$db->queryDirect($q, array($now, $id));
			
			$q = "UPDATE `customers` SET `is_disabled` = 0 WHERE `user_id`=%i";
			$db->queryDirect($q, array($id));
			$response['result'] = true;
			$response['query'] = $q;
			
			// send email to notify user.
			$text_body = "The CSCLotto account linked to this email address has been reactivated and is no longer disabled.\n\n";
			$html_body = "<h2>Account No Longer Disabled</h2>The CSCLotto account linked to this email address has been reactivated and is no longer disabled.<br><br>";
			$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Account Reactivated", $html_body, $text_body);
			break;
		case 'force_logout':
			$q = "DELETE FROM $table WHERE username=%s";
			$db->queryDirect($q, array($username));
			
			$response['result'] = true;
			
			break;
		default:
			$response['result'] = false;
			$response['errors'] = "An unexpected error occurred";
	}
	
	echo json_encode($response);