<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$game = isset($_POST['game']) ? $_POST['game'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'overall_total':
			$q = "SELECT SUM(CASE WHEN ct.transaction_type_id=1 THEN `amount` END) AS bets, SUM(CASE WHEN ct.transaction_type_id=2 THEN `amount` END) AS wins, SUM(ct.amount) AS net, COUNT(ct.transaction_id) AS play_count FROM `customer_transaction` AS `ct`";
			
			$extra_where = " WHERE (`ct`.`transaction_type_id`=1 OR `ct`.`transaction_type_id`=2)";
			if($game != ""){
				$extra_where .= " AND SUBSTR(`transaction_details`, INSTR(`transaction_details`,'-') + 2, LENGTH(`transaction_details`)) = '".$game."'";
			}

			if(isset($_POST['filter_date_from']) && $_POST['filter_date_from'] != "undefined"){
				$extra_where .= " AND `transaction_date` >= '".$_POST['filter_date_from']."'";
			}
			
			if(isset($_POST['filter_date_to'])&& $_POST['filter_date_to'] != "undefined"){
				$extra_where .= " AND `transaction_date` <= '".$_POST['filter_date_to']." 23:59:59'";
			}

			$returned = $db->queryOneRow($q.$extra_where);
			$returned['bets'] = $returned['bets'] == null ? "0.00" : number_format(-$returned['bets']);
			$returned['wins'] = $returned['wins'] == null ? "0.00" : number_format(-$returned['wins']);
			$returned['net'] = $returned['net'] == null ? "0.00" : number_format(-$returned['net']);
			$returned['play_count'] = $returned['play_count'] == null ? "0" : number_format($returned['play_count']);
			
			$response['result'] = $returned;
			$response['query'] = $q.$extra_where;
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
