<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$customer_id = isset($_POST['customer_id']) ? $_POST['customer_id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'overall_total':
			$q = "SELECT SUM(c.available_balance) AS total_balances, SUM(c.bonus_balance) AS total_bonus_balances FROM `customers` AS `c` LEFT JOIN `users` AS `u` ON `c`.`user_id`=`u`.`id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `u`.`id`";
			
			if($customer_id != "-1"){
				$extra_where = "WHERE c.user_id = '".$customer_id."'";
			}
			
			$returned = $db->queryOneRow($q.$extra_where);
			$returned['total_balances'] = $returned['total_balances'] == null ? "0" : number_format($returned['total_balances']);
			$returned['total_bonus_balances'] = $returned['total_bonus_balances'] == null ? "0" : number_format($returned['total_bonus_balances']);
			
			$q = "SELECT SUM(CASE WHEN transaction_type_id = 6 THEN amount END) AS deposits, SUM(CASE WHEN transaction_type_id = 5 THEN amount END) AS withdrawals FROM customer_transaction";
		
			$extra_where = "";
		
			if(isset($_POST['filter_date_from']) && $_POST['filter_date_from'] != "undefined"){
				if($extra_where == ""){
					$extra_where = " WHERE (transaction_date >= '".$_POST['filter_date_from']."'";
				}else{
					$extra_where .= " OR (transaction_date >= '".$_POST['filter_date_from']."'";
				}
			}
			
			if(isset($_POST['filter_date_to'])&& $_POST['filter_date_to'] != "undefined"){
				if($extra_where == ""){
					$extra_where = "transaction_date <= '".$_POST['filter_date_to']." 23:59:59'";
				}else{
					$extra_where .= " AND transaction_date <= '".$_POST['filter_date_to']." 23:59:59'";
				}
			}
			
			if($customer_id != "-1"){
				$extra_where .= ") AND user_id = '".$customer_id."'";
			}else{
				$extra_where .= ")";
			}
			
			$temp = $db->queryOneRow($q.$extra_where);
			$returned['withdrawals'] = $temp['withdrawals'] == null ? "0" : number_format($temp['withdrawals']);
			$returned['deposits'] = $temp['deposits'] == null ? "0" : number_format($temp['deposits']);
			
			$response['result'] = $returned;
			$response['query'] = $q.$extra_where;
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
