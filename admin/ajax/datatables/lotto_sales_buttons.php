<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "lotto_bet_limits";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'get_summary':
			$q = "SELECT SUM(b.bet_amount) AS total_sales, SUM(b.bet_amount*t.commission_rate) AS total_commission, SUM(w.winning_amount) AS total_winnings FROM `lotto_ticket` AS `t` JOIN `lotto_bet` AS `b` ON `t`.`ticket_id`=`b`.`ticket_id` JOIN `lotto_game` AS `g` ON `g`.`id`=`b`.`game_id` LEFT JOIN `winners` AS `w` ON `w`.`bet_id`=`b`.`bet_id`";

			if(isset($_POST['filter_date_from']) && $_POST['filter_date_from'] != "undefined"){
				if($extra_where == ""){
					$extra_where = " WHERE `purchase_date` >= '".$_POST['filter_date_from']."'";
				}else{
					$extra_where .= " AND `purchase_date` >= '".$_POST['filter_date_from']."'";
				}
			}
			
			if(isset($_POST['filter_date_to'])&& $_POST['filter_date_to'] != "undefined"){
				if($extra_where == ""){
					$extra_where = " WHERE `purchase_date` <= '".$_POST['filter_date_to']." 23:59:59'";
				}else{
					$extra_where .= " AND `purchase_date` <= '".$_POST['filter_date_to']." 23:59:59'";
				}
			}

			$response['result'] = $db->queryOneRow($q.$extra_where);

			if($response['result']['total_sales'] == null){
				$response['result']['total_sales'] = "0.00000000";
			}else{
				$response['result']['total_sales'] = number_format($response['result']['total_sales']);
			}
			if($response['result']['total_winnings'] == null){
				$response['result']['total_winnings'] = "0.00000000";
			}else{
				$response['result']['total_winnings'] = number_format($response['result']['total_winnings']);
			}
			if($response['result']['total_commission'] == null){
				$response['result']['total_commission'] = "0.00000000";
			}else{
				$response['result']['total_commission'] = number_format($response['result']['total_commission']);
			}
			$response['result']['total_net'] = number_format(($response['result']['total_sales'] - $response['result']['total_winnings'] - $response['result']['total_commission']));

			$response['query'] = $q.$extra_where;
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
