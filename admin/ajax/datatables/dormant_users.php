<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'users';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'u.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'u.username',
			'dt'        => 'username',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'u.ip',
			'dt'        => 'ip',
			'field' => 'ip',
			'as' => 'ip',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.last_login',
			'dt'        => 'latest_activity',
			'field' => 'latest_activity',
			'as' => 'latest_activity',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format("m/d/Y h:i A");
			}
		),
		array(
			'db'        => 'u.last_login',
			'dt'        => 'inactive',
			'field' => 'inactive',
			'as' => 'inactive',
			'formatter' => function( $d, $row ) {
				global $core;
				$now = new DateTime();
				$dt = new DateTime($d);
				$interval = $now->diff($dt);
				return $core->format_interval($interval, true);
			}
		),
		array(
			'db' => 'u.locked',
			'dt' => 'locked',
			'field' => 'locked',
			'as' => 'locked',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.customer_id',
			'dt' => 'customer_id',
			'field' => 'customer_id',
			'as' => 'customer_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.disabled_by',
			'dt' => 'disabled_by',
			'field' => 'disabled_by',
			'as' => 'disabled_by',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'c.is_disabled',
			'dt'        => 'status',
			'field'		=> 'status',
			'as' 		=> 'status',
			'formatter' => function( $d, $row ) {
				if($row["locked"] == 1){
					$status = "<span style='color:orange;'>Locked</span>";
				}else{
					if($row["customer_id"]){
						if($d == 1){
							$status = "<span style='color:red;'>Dormant</span>";
						}elseif($d == 0){
							$status = "<span style='color:orange;'>Inactive</span>";
						}
					}else{
						$status = "No Customer Account";
					}
				}
				
				return $status;
			}
		),
		array(
			'db' => 'u.locked_expiration',
			'dt' => 'status_details',
			'field' => 'status_details',
			'as' => 'status_details',
			'formatter' => function( $d, $row ) {
			
				$row["disabled_by"] != "" ?
				$l = "Disabled by user ".$row["disabled_by"]."." :
				$l = "<i class='fa fa-check-circle-o' style='color:green;'></i>";	
			
				return $row["locked"] == 1 ? "Locked until ".$d : $l;
			}
		)
	);

	$join_query = "FROM `users` AS `u` LEFT JOIN `customers` AS `c` ON `c`.`user_id`=`u`.`id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `u`.`id`";
	
	//$group_by = "GROUP BY ";

	$now = new DateTime();
	$ninety_days_less_dt = $now->modify("-90 days");
	$ninety_days_less = $ninety_days_less_dt->format('Y-m-d');
	
	$extra_where = " `u`.`last_login` <= '$ninety_days_less'";
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);