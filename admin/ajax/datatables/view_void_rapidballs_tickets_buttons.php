<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$table = "rapidballs_ticket";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	
	switch ($action){
		case 'void':
			$ticket_info = $db->queryOneRow("SELECT SUM(`rb`.`bet_amount`) AS bet_amount, rt.user_id FROM `$table` AS `rt` JOIN `rapidballs_bet` AS `rb` ON `rt`.`id`=`rb`.`ticket_id` WHERE rb.ticket_id=%i AND rb.is_processed=0 GROUP BY rb.ticket_id", array($id));
			
			if(!empty($ticket_info)){
				//Refund the users money for the unprocessed bets	
				$core->make_customer_transaction($ticket_info['bet_amount'], 50, "Rapidballs refund for voided rapidballs ticket (".$id.")", "NULL", $ticket_info['user_id']);
				
				$q = "UPDATE `$table` SET `is_voided` = 1 WHERE id=%i";
				$result = $db->queryDirect($q, array($id));
				
				$q = "UPDATE `rapidballs_bet` SET `is_voided` = 1 WHERE `is_processed` = 0 AND ticket_id=%i";
				$result = $db->queryDirect($q, array($id));
				
				$q = "UPDATE `rapidballs_bet` SET `is_processed` = 1 WHERE ticket_id=%i";
				$result = $db->queryDirect($q, array($id));
				
				$q = "UPDATE `$table` SET `voided_by` = '".$session->userinfo['id']."' WHERE id=%i";
				$result = $db->queryDirect($q, array($id));
				
				$q = "UPDATE `$table` SET `voided_on` = '$now' WHERE id=%i";
				$result = $db->queryDirect($q, array($id));
				
				$q = "UPDATE `$table` SET `void_reason` = %s WHERE id=%i";
				$result = $db->queryDirect($q, array($_POST['voided_comment'], $id));
				
				$return = array("success" => "true", "bet_amount" => $ticket_info['bet_amount'], "user_id" => $ticket_info['user_id'], "ticket_id" => $id);
			}else{
				$return = array("success" => "false", "errors" => "This whole ticket has already been processed.");
			}
			
			break;
		default:
		   die("Invalid Action");
	}
	
	echo json_encode($return);