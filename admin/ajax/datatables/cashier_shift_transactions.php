<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user_transaction';
	
	// Table's primary key
	$primary_key = 'id';

	//Get the cashier and to - from filter dates
	$cashier_id = $_GET['cashier_id'];
	$shift_id = $_GET['shift_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'put.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'put.id',
			'dt' => 'transaction_id',
			'field' => 'transaction_id',
			'as' => 'transaction_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'put.transaction_date',
			'dt'        => 'trans_date',
			'field' => 'trans_date',
			'as' => 'trans_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 's.shift_start',
			'dt'        => 'shift_start',
			'field' => 'shift_start',
			'as' => 'shift_start',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 's.shift_end',
			'dt'        => 'shift_end',
			'field' => 'shift_end',
			'as' => 'shift_end',
			'formatter' => function( $d, $row ) {
				return $d == "0000-00-00 00:00:00" ? "Present" : date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 's.id',
			'dt'        => 'shift_id',
			'field' => 'shift_id',
			'as' => 'shift_id',
			'formatter' => function( $d, $row ) {
				$shift_end = $row['shift_end'] == "0000-00-00 00:00:00" ? "Present" : date("m/d/Y h:i A", strtotime($row['shift_end']));
				return date("m/d/Y h:i A", strtotime($row['shift_start']))." - ".$shift_end." (".$d.")";
			}
		),
		array(
			'db'        => 'tt.name',
			'dt'        => 'type',
			'field' => 'type',
			'as' => 'type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'put.transaction_details',
			'dt'        => 'description',
			'field' => 'description',
			'as' => 'description',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'put.amount',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'put.balance',
			'dt'        => 'balance',
			'field' => 'balance',
			'as' => 'balance',
			'formatter' => function( $d, $row ) {
				return "".number_format($d). " ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'put.initial_balance',
			'dt'        => 'initial_balance',
			'field' => 'initial_balance',
			'as' => 'initial_balance',
			'formatter' => function( $d, $row ) {
				return "".number_format($d). " ".CURRENCY_FORMAT."";
			}
		)
	);

	$join_query = "FROM `panel_user_transaction` AS `put` JOIN `panel_user_shift` AS `s` ON `put`.`shift_id`=`s`.`id` JOIN `transaction_types` AS `tt` ON `put`.`type_id`=`tt`.`id`";
	
	if($cashier_id != ""){
		if($extra_where == ""){
			$extra_where = "s.user_id = '".$cashier_id."'";
		}else{
			$extra_where .= " AND s.user_id = '".$cashier_id."'";
		}
	}
	if($shift_id != ""){
		$extra_where = "s.id = '".$shift_id."'";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`transaction_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `transaction_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`transaction_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `transaction_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "GROUP BY `put`.`made_by`, CAST(`put`.`transaction_date` AS DATE)";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
