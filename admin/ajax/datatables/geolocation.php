<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'user_geolocation_info';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. 
	$columns = array(
		array(
			'db' => 'g.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'g.user_session_id',
			'dt' => 'user_session_id',
			'field' => 'user_session_id',
			'as' => 'user_session_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'g.city',
			'dt' => 'city',
			'field' => 'city',
			'as' => 'city',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'g.region',
			'dt' => 'region',
			'field' => 'region',
			'as' => 'region',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'g.date_time',
			'dt' => 'date_time',
			'field' => 'date_time',
			'as' => 'date_time',
			'formatter' => function( $d, $row ) {
				return date('m/d/Y h:i:s A' , strtotime($d));
			}
		),
		array(
			'db' => 'g.country_name',
			'dt' => 'country_name',
			'field' => 'country_name',
			'as' => 'country_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'g.country_code',
			'dt' => 'country_code',
			'field' => 'country_code',
			'as' => 'country_code',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'g.zipcode',
			'dt' => 'zipcode',
			'field' => 'zipcode',
			'as' => 'zipcode',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'g.latitude',
			'dt' => 'latitude',
			'field' => 'latitude',
			'as' => 'latitude',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'g.longitude',
			'dt' => 'longitude',
			'field' => 'longitude',
			'as' => 'longitude',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'g.map_img_url',
			'dt' => 'map_img_url',
			'field' => 'map_img_url',
			'as' => 'map_img_url',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
	);

	$join_query = "FROM `user_geolocation_info` AS `g`";
	
	$extra_where = "";
	
	if(isset($_GET['user_session_id'])){
		if($extra_where == ""){
			$extra_where = "`user_session_id` = '".$_GET['user_session_id']."'";
		}else{
			$extra_where .= " AND `user_session_id` = '".$_GET['user_session_id']."'";
		}
	}
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );

	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);