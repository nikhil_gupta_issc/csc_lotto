<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user_transaction';

	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'put.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'pl.id',
			'dt'        => 'location_id',
			'field' => 'location_id',
			'as' => 'location_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pl.name',
			'dt'        => 'location',
			'field' => 'location',
			'as' => 'location',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'SUM(put.amount)',
			'dt'        => 'net',
			'field' => 'net',
			'as' => 'net',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		)
	);

	$join_query = "FROM `panel_user_transaction` AS `put` JOIN `panel_location` AS `pl` ON `put`.`location_id`=`pl`.`id`";
	
	$date = new DateTime('now');
	$date_str = $date->format("Y-m-d");
	$extra_where = "CAST(`put`.`transaction_date` AS DATE) = '".$date_str."' AND (put.type_id='9' OR put.type_id='10')";
	
	$group_by = "GROUP BY `put`.`location_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);