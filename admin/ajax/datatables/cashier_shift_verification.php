<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user_shift';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 's.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'cashier_name',
			'field' => 'cashier_name',
			'as' => 'cashier_name',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db'        => 's.id',
			'dt'        => 'shift_id',
			'field' => 'shift_id',
			'as' => 'shift_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 's.shift_start',
			'dt'        => 'shift_start',
			'field' => 'shift_start',
			'as' => 'shift_start',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 's.shift_end',
			'dt'        => 'shift_end',
			'field' => 'shift_end',
			'as' => 'shift_end',
			'formatter' => function( $d, $row ) {
				if($d=="0000-00-00 00:00:00"){
					return "Active";
				}else{
					return date("m/d/Y h:i A", strtotime($d));
				}
			}
		),
		array(
			'db'        => 's.float_amount',
			'dt'        => 'float_amount',
			'field' => 'float_amount',
			'as' => 'float_amount',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.opening',
			'dt'        => 'opening',
			'field' => 'opening',
			'as' => 'opening',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.sales',
			'dt'        => 'sales',
			'field' => 'sales',
			'as' => 'sales',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.payouts',
			'dt'        => 'payouts',
			'field' => 'payouts',
			'as' => 'payouts',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.closing',
			'dt'        => 'closing',
			'field' => 'closing',
			'as' => 'closing',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.expected',
			'dt'        => 'expected',
			'field' => 'expected',
			'as' => 'expected',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.over_short',
			'dt'        => 'over_short',
			'field' => 'over_short',
			'as' => 'over_short',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.is_verified',
			'dt'        => 'is_verified',
			'field' => 'is_verified',
			'as' => 'is_verified',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 's.is_verified',
			'dt'        => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				if($row['is_exact']==1){
					$exact = "Exact";
				}else{
					$exact = "Non-Exact";
				}
				
				if($d==1){
					$ver_date = new DateTime($row['verified_on']);
					$str = $ver_date->format("m/d/Y h:i A");
					return "<span style='color:green'>Marked as ".$exact." by ".$row['ver_firstname']." ".$row['ver_lastname']." (".$row['verified_by'].") on ".$str."</span>";
				}else{
					return "<span style='color:red;'>Needs Verified</span>";
				}
			}
		),
		array(
			'db'        => 's.is_exact',
			'dt'        => 'is_exact',
			'field' => 'is_exact',
			'as' => 'is_exact',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 's.verified_on',
			'dt'        => 'verified_on',
			'field' => 'verified_on',
			'as' => 'verified_on',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 's.verified_by',
			'dt'        => 'verified_by',
			'field' => 'verified_by',
			'as' => 'verified_by',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ver_u.firstname',
			'dt'        => 'ver_firstname',
			'field' => 'ver_firstname',
			'as' => 'ver_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ver_u.lastname',
			'dt'        => 'ver_lastname',
			'field' => 'ver_lastname',
			'as' => 'ver_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);
	
	$join_query = "FROM `panel_user_shift` AS `s` LEFT JOIN `users` AS `u` ON `s`.`user_id`=`u`.`id` LEFT JOIN `users` AS `ver_u` ON `ver_u`.`id`=`s`.`verified_by`";
	
	$extra_where = "s.shift_end!='0000-00-00 00:00:00'";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`shift_start` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `shift_start` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`shift_start` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `shift_start` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);