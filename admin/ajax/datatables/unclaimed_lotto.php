<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_ticket';
	
	// Table's primary key
	$primary_key = 'ticket_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 't.ticket_id',
			'dt' => 'DT_RowId',
			'field' => 'ticket_id',
			'as' => 'ticket_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 't.ticket_number',
			'dt'        => 'ticket_number',
			'field' => 'ticket_number',
			'as' => 'ticket_number',
			'formatter' => function( $d, $row ) {
				return "<a href='/admin/tasks/view_void_ticket/index.php?ticket_number=".$d."'>".$d."</a>";
			}
		),
		array(
			'db'        => 'c.firstname',
			'dt'        => 'cashier',
			'field' => 'cashier',
			'as' => 'cashier',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "Online" : $d." ".$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db'        => 'c.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'c.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'l.name',
			'dt'        => 'location',
			'field' => 'location',
			'as' => 'location',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "Online" : $d;
			}
		),
		array(
			'db'        => 'wp.total_payout',
			'dt'        => 'total_won',
			'field' => 'total_won',
			'as' => 'total_won',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN put.type_id = 12 THEN put.amount END)',
			'dt'        => 'total_unclaimed',
			'field' => 'total_unclaimed',
			'as' => 'total_unclaimed',
			'formatter' => function( $d, $row ) {
				return "".number_format(($row['total_won'] - (abs($d))))." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'purchase_date',
			'dt'        => 'date',
			'field' => 'date',
			'as' => 'date',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				$dt_str = $dt->format("m/d/Y h:i A");
				return $dt_str;
			}
		)
	);


	$join_query = "FROM `lotto_ticket` AS `t` JOIN `winner_payout` as `wp` ON `wp`.`ticket_number`=`t`.`ticket_number` LEFT JOIN `panel_user_transaction` AS `put` ON `put`.`ticket_number`=`t`.`ticket_number` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id`=`l`.`id` LEFT JOIN `users` AS `c` ON `t`.`cashier_id`=`c`.`id`";

	$extra_where = "`wp`.`status_id`!=1";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`purchase_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `purchase_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`purchase_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `purchase_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	$group_by = "GROUP BY `t`.`ticket_number`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
