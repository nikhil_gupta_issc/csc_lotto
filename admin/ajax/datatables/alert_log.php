<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'alerts';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'a.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'a.subject',
			'dt'        => 'subject',
			'field' => 'subject',
			'as' => 'subject',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.message',
			'dt'        => 'message',
			'field' => 'message',
			'as' => 'message',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.is_dismissed',
			'dt'        => 'is_dismissed',
			'field' => 'is_dismissed',
			'as' => 'is_dismissed',
			'formatter' => function( $d, $row ) {
				return $d == 0 ? "Active" : "Dismissed";
			}
		),
		array(
			'db'        => 'a.date_created',
			'dt'        => 'date_created',
			'field' => 'date_created',
			'as' => 'date_created',
			'formatter' => function( $d, $row ) {
				return date( 'm/d/Y h:i A', strtotime($d));
			}
		),
		array(
			'db'        => 'a.date_dismissed',
			'dt'        => 'date_dismissed',
			'field' => 'date_dismissed',
			'as' => 'date_dismissed',
			'formatter' => function( $d, $row ) {
				return $d == "0000-00-00 00:00:00" ? "Active" : date( 'm/d/Y h:i A', strtotime($d));
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user',
			'field' => 'user',
			'as' => 'user',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$d.")";
			}
		)
	);


	$join_query = "FROM `alerts` AS `a` LEFT JOIN `users` as `u` ON `a`.`user_id`=`u`.`id`";

	//$extra_where = "";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`date_created` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `date_created` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`date_created` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `date_created` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);