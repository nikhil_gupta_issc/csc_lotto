<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'customers';
	
	$customer_id = $_GET['customer_id'];
	
	// Table's primary key
	$primary_key = 'customer_id';
	
	//Build subquery where
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($subquery_where == ""){
			$subquery_where = "ct.transaction_date >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($subquery_where == ""){
			$subquery_where = "ct.transaction_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$subquery_where .= " AND ct.transaction_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'c.customer_id',
			'dt' => 'DT_RowId',
			'field' => 'customer_id',
			'as' => 'customer_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.customer_number',
			'dt' => 'customer_number',
			'field' => 'customer_number',
			'as' => 'customer_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'customer_name',
			'field' => 'customer_name',
			'as' => 'customer_name',
			'formatter' => function( $d, $row ) {
				return $d.' '.$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.midname',
			'dt' => 'midname',
			'field' => 'midname',
			'as' => 'midname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.available_balance',
			'dt' => 'available_balance',
			'field' => 'available_balance',
			'as' => 'available_balance',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "0.00000000 ".CURRENCY_FORMAT."" : "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db' => '(SELECT SUM(ct3.amount) FROM customer_transaction AS ct3 WHERE ct3.transaction_type_id = 5 AND ct3.user_id = c.user_id AND ('.str_replace("ct.","ct3.",$subquery_where).'))',
			'dt' => 'withdrawals',
			'field' => 'withdrawals',
			'as' => 'withdrawals',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db' => '(SELECT SUM(ct2.amount) FROM customer_transaction AS ct2 WHERE ct2.transaction_type_id = 6 AND ct2.user_id = c.user_id AND ('.str_replace("ct.","ct2.",$subquery_where).'))',
			'dt' => 'deposits',
			'field' => 'deposits',
			'as' => 'deposits',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db' => 'c.bonus_balance',
			'dt' => 'bonus_balance',
			'field' => 'bonus_balance',
			'as' => 'bonus_balance',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "0.00000000 ".CURRENCY_FORMAT."" : "".number_format($d). " ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db' => 'u.locked',
			'dt' => 'locked',
			'field' => 'locked',
			'as' => 'locked',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'c.is_disabled',
			'dt'        => 'status',
			'field'		=> 'status',
			'as' 		=> 'status',
			'formatter' => function( $d, $row ) {
				$locked = $row["locked"] == 1 ? "<span style='color:orange;'>Locked</span>" : "<span style='color:green;'>Enabled</span>";
				return $d == 1 ? "<span style='color:red;'>Disabled</span>" : $locked;
			}
		)
	);

	$join_query = "FROM `customers` AS `c` LEFT JOIN `customer_transaction` AS `ct` ON `c`.`user_id`=`ct`.`user_id` LEFT JOIN `users` AS `u` ON `c`.`user_id`=`u`.`id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `u`.`id`";
	
	if($customer_id > 0){
		$extra_where = "c.user_id = '".$customer_id."'";
	}
	
	$group_by = " GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
