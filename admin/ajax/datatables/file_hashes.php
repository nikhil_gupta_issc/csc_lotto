<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'file_hashes';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'fh.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'fh.path',
			'dt'        => 'path',
			'field' => 'path',
			'as' => 'path',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'fh.approved_filesize',
			'dt'        => 'size',
			'field' => 'size',
			'as' => 'size',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'fh.approved_modified_date',
			'dt'        => 'modified_on',
			'field' => 'modified_on',
			'as' => 'modified_on',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format('m/d/Y h:i:s A');
			}
		),
		array(
			'db'        => 'fh.approved_hash',
			'dt'        => 'hash',
			'field' => 'hash',
			'as' => 'hash',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'fh.approved_by',
			'dt'        => 'approved_by',
			'field' => 'approved_by',
			'as' => 'approved_by',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'fh.approved_on',
			'dt'        => 'approved_on',
			'field' => 'approved_on',
			'as' => 'approved_on',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format('m/d/Y h:i:s A');
			}
		)
	);


	$join_query = "FROM `file_hashes` AS `fh`";

	//$extra_where = " is_deleted=0";

	//$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);