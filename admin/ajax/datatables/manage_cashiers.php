<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'p.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'p.id',
			'dt' => 'cashier_id',
			'field' => 'cashier_id',
			'as' => 'cashier_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.username',
			'dt' => 'username',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'cashier_name',
			'field' => 'cashier_name',
			'as' => 'cashier_name',
			'formatter' => function( $d, $row ) {
				return $d.' '.$row['lastname']." (".$row["user_id"].")";
			}
		),
		array(
			'db' => 'u.midname',
			'dt' => 'midname',
			'field' => 'midname',
			'as' => 'midname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.date_of_birth',
			'dt' => 'date_of_birth',
			'field' => 'date_of_birth',
			'as' => 'date_of_birth',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.gender',
			'dt' => 'gender',
			'field' => 'gender',
			'as' => 'gender',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.address',
			'dt' => 'address',
			'field' => 'address',
			'as' => 'address',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.address2',
			'dt' => 'address2',
			'field' => 'address2',
			'as' => 'address2',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.city',
			'dt' => 'city',
			'field' => 'city',
			'as' => 'city',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.country',
			'dt' => 'country',
			'field' => 'country',
			'as' => 'country',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.island_id',
			'dt' => 'island_id',
			'field' => 'island_id',
			'as' => 'island_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'i.name',
			'dt' => 'island',
			'field' => 'island',
			'as' => 'island',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'l.name',
			'dt' => 'location',
			'field' => 'location',
			'as' => 'location',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'p.location_id',
			'dt' => 'location_id',
			'field' => 'location_id',
			'as' => 'location_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'p.commission_rate',
			'dt' => 'commission_rate',
			'field' => 'commission_rate',
			'as' => 'commission_rate',
			'formatter' => function( $d, $row ) {
				return ($d*100)."%";
			}
		),
		array(
			'db' => 'u.email',
			'dt' => 'email',
			'field' => 'email',
			'as' => 'email',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.telephone',
			'dt' => 'phones',
			'field' => 'phones',
			'as' => 'phones',
			'formatter' => function( $d, $row ) {
				return $d.'<br>'.$row['cellphone'];
			}
		),
		array(
			'db' => 'ui.telephone',
			'dt' => 'telephone',
			'field' => 'telephone',
			'as' => 'telephone',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.cellphone',
			'dt' => 'cellphone',
			'field' => 'cellphone',
			'as' => 'cellphone',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'p.balance',
			'dt' => 'balance',
			'field' => 'balance',
			'as' => 'balance',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." " . CURRENCY_FORMAT;
			}
		),
		array(
			'db' => 'u.last_login',
			'dt' => 'last_login',
			'field' => 'last_login',
			'as' => 'last_login',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db' => 'u.locked',
			'dt' => 'locked',
			'field' => 'locked',
			'as' => 'locked',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'p.disabled_by',
			'dt' => 'disabled_by',
			'field' => 'disabled_by',
			'as' => 'disabled_by',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'p.is_disabled',
			'dt'        => 'status',
			'field'		=> 'status',
			'as' 		=> 'status',
			'formatter' => function( $d, $row ) {
			
				$row["locked"] == 1 ?
				$l = "<span style='color:orange;'>"."Locked"."</span>" :
				$l = "<span style='color:green;'>"."Enabled"."</span>";	
			
				return $d == 1 ? "<span style='color:red;'>"."Disabled"."</span>" : "$l";
			}
		),
		array(
			'db' => 'u.locked_expiration',
			'dt' => 'status_details',
			'field' => 'status_details',
			'as' => 'status_details',
			'formatter' => function( $d, $row ) {
			
				$row["disabled_by"] != "0" ?
				$l = "Previously disabled by user ".$row["disabled_by"]."." :
				$l = "<i class='fa fa-check-circle-o' style='color:green;'></i>";	
			
				return $row["locked"] == 1 ? "Locked until ".$d : $l;
			}
		),
                array(
			'db' => 'p.virtual_money_offset',
			'dt' => 'virtual_money_offset',
			'field' => 'virtual_money_offset',
			'as' => 'virtual_money_offset',
			'formatter' => function( $d, $row ) {
			       
                                return $d != NULL ? "<input type='text' user_id = '".$row['user_id']."' maxlength='10' class='cashier_vm' value='".$d."'><a title='Save virtual money offset' class='save_vm'><i class='fa fa-save'></i></a><a title='Reset Virtual money offset to default' class='reset_vm'><i class='fa fa-undo'></i></a>" : "<input type='text' user_id = '".$row['user_id']."' maxlength='10' class='cashier_vm' value=''><a title='Save virtual money offset' class='save_vm'><i class='fa fa-save'></i></a><a title='Reset Virtual money offset to default' class='reset_vm'><i class='fa fa-undo'></i></a>";



			}
		),
                array(
			'db' => 'p.location_change',
			'dt' => 'location_change',
			'field' => 'location_change',
			'as' => 'location_change',
			'formatter' => function( $d, $row ) {			       
                                return "<input type='checkbox' user_id='".$row['user_id']."' id='cashier_location_change_".$row['user_id']."' class='cashier_location_change' ".($d == 1 ? 'checked' : '')." >";
			}
		),
                array(
			'db' => 'p.petty_cash_rights',
			'dt' => 'petty_cash_rights',
			'field' => 'petty_cash_rights',
			'as' => 'petty_cash_rights',
			'formatter' => function( $d, $row ) {			       
                                return "<input type='checkbox' user_id='".$row['user_id']."' id='petty_cash_change_".$row['user_id']."' class='petty_cash_change' ".($d == 1 ? 'checked' : '')." >";
			}
		),
                array(
			'db' => 'p.location_change',
			'dt' => 'p_location_change',
			'field' => 'p_location_change',
			'as' => 'p_location_change',
			'formatter' => function( $d, $row ) {			       
                                return $d;
			}
		),
                array(
			'db' => 'p.petty_cash_rights',
			'dt' => 'p_petty_cash_rights',
			'field' => 'p_petty_cash_rights',
			'as' => 'p_petty_cash_rights',
			'formatter' => function( $d, $row ) {			       
                                return $d;
			}
		)		
	);

	$join_query = "FROM `panel_user` AS `p` LEFT JOIN `users` AS `u` ON `p`.`user_id`=`u`.`id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `u`.`id` LEFT JOIN `island` AS `i` ON `i`.`id` = `ui`.`island_id` LEFT JOIN `panel_location` AS `l` ON `l`.`id` = `p`.`location_id`";
	
	//$extra_where = "";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
