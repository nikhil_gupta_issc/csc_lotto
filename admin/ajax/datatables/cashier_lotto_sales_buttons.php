<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$cashier_id = $_POST['cashier_id'];
	$tickett_id = $_POST['ticket_id'];	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'overall_total':
			$q = "SELECT SUM(t.total) AS total FROM `lotto_ticket` AS `t`";
			
			$extra_where = " WHERE t.shift_id != 0";
	
			if($cashier_id != ""){
				$extra_where .= " AND t.cashier_id = '".$cashier_id."'";
			}
			
			if(isset($_POST['filter_date_from']) && $_POST['filter_date_from'] != "undefined"){
				$extra_where .= " AND `purchase_date` >= '".$_POST['filter_date_from']."'";
			}
			
			if(isset($_POST['filter_date_to'])&& $_POST['filter_date_to'] != "undefined"){
				$extra_where .= " AND `purchase_date` <= '".$_POST['filter_date_to']." 23:59:59'";
			}

			$returned = $db->queryOneRow($q.$extra_where);
			$returned['total'] = $returned['total'] == null ? "0.00000000" : number_format($returned['total']);
			
			$response['result'] = $returned;
			$response['query'] = $q.$extra_where;
			break;
		case 'bet_total':

			$q="SELECT t.total AS total FROM `lotto_ticket` AS `t` WHERE t.ticket_number = ".$tickett_id ; 
			$returned = $db->queryOneRow($q);
			$response['result']=$returned;
			$response['query'] = $q;
			break;			
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
