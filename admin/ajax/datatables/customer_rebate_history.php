<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'cron_weekly_totals';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'c.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'c.id',
			'dt' => 'id',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {				
				return $d;
			}
		),
		array(
			'db' => 'c.period',
			'dt' => 'period',
			'field' => 'period',
			'as' => 'period',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.casino_1',
			'dt' => 'casino_chetu',
			'field' => 'casino_chetu',
			'as' => 'casino_chetu',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.casino_2',
			'dt' => 'casino_multislot',
			'field' => 'casino_multislot',
			'as' => 'casino_multislot',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.lotto',
			'dt' => 'lotto',
			'field' => 'lotto',
			'as' => 'lotto',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.rapidball',
			'dt' => 'rapidball',
			'field' => 'rapidball',
			'as' => 'rapidball',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.sports',
			'dt' => 'sports',
			'field' => 'sports',
			'as' => 'sports',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.total',
			'dt' => 'total',
			'field' => 'total',
			'as' => 'total',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.date_sent',
			'dt' => 'date_sent',
			'field' => 'date_sent',
			'as' => 'date_sent',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.month',
			'dt' => 'month',
			'field' => 'month',
			'as' => 'month',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.from_date',
			'dt' => 'from_date',
			'field' => 'from_date',
			'as' => 'from_date',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.to_date',
			'dt' => 'to_date',
			'field' => 'to_date',
			'as' => 'to_date',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)		
	);

	$join_query = "FROM `cron_weekly_totals` AS `c`";
	
	//$extra_where = "";
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "DATE(`from_date`) >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND DATE(`from_date`) >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "DATE(`to_date`) <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND DATE(`to_date`) <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
