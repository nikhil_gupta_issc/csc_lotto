<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_game';
	
	// Table's primary key
	$primaryKey = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'id',
			'dt' => 'DT_RowId',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'number_of_balls',
			'dt'        => 'game_name',
			'formatter' => function( $d, $row ) {
				return $d." Ball";
			}
		),
		array(
			'db'        => 'short_name',
			'dt'        => 'short_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'name',
			'dt'        => 'name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'number_of_balls',
			'dt'        => 'number_of_balls',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'house_id',
			'dt'        => 'house_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'min_bet',
			'dt'        => 'min_bet',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'max_per_ball',
			'dt'        => 'max_per_ball',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'max_bet',
			'dt'        => 'max_bet',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
				array(
			'db'        => 'hh_min_bet',
			'dt'        => 'hh_min_bet',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'hh_max_per_ball',
			'dt'        => 'hh_max_per_ball',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'hh_max_bet',
			'dt'        => 'hh_max_bet',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
				array(
			'db'        => 'web_min_bet',
			'dt'        => 'web_min_bet',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'web_max_per_ball',
			'dt'        => 'web_max_per_ball',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'web_max_bet',
			'dt'        => 'web_max_bet',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'is_disabled',
			'dt'        => 'status',
			'formatter' => function( $d, $row ) {
				return $d == 1 ? "<span style='color:red;'>"."Disabled"."</span>" : "Enabled";
			}
		)
	);
	
	$where = "house_id = '".$_GET['house_id']."'";
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primaryKey, $columns, $where )
	);