<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "panel_user";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'enable':
			$q = "UPDATE $table SET `is_disabled` = 0 WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			break;
		case 'disable':
			$q = "UPDATE $table SET `is_disabled` = 1, `disabled_by` = ".$session->userinfo['id']." WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			break;
/*hardcode*/	case 'unlock':

			$q = "SELECT user_id FROM `panel_user` WHERE id=".$id.";";
			$user_id = $db->queryOneRow($q);

			$q = "UPDATE users SET `locked` = 0, `locked_expiration` = NULL WHERE id=".$user_id["user_id"].";";
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			break;
		case 'delete':
			$q = "DELETE FROM $table WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			if($response['result'] == true){
// TODO: mark user account as deleted only if it is not admin and/or panel user
			}
			break;
		case 'get_user_info':
			$q = "SELECT p.id AS id, p.id AS cashier_id, u.id AS user_id, u.firstname AS firstname, u.lastname AS lastname, u.firstname AS cashier_name, u.midname AS midname, ui.date_of_birth AS date_of_birth, ui.gender AS gender, ui.address AS address, ui.address2 AS address2, ui.city AS city, ui.country AS country, ui.island_id AS island_id, i.name AS island, l.name AS location, p.location_id AS location_id, p.commission_rate AS commission_rate, u.email AS email, ui.telephone AS phones, ui.telephone AS telephone, ui.cellphone AS cellphone, p.balance AS balance, u.last_login AS last_login FROM `users` AS `u` LEFT JOIN `panel_user` AS `p` ON `u`.`id` = `p`.`user_id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `u`.`id` LEFT JOIN `island` AS `i` ON `i`.`id` = `ui`.`island_id` LEFT JOIN `panel_location` AS `l` ON `l`.`id` = `p`.`location_id`
WHERE u.id = '".$_POST['user_id']."'";
			$response['result'] = $db->query($q);
			$response['query'] = $q;
			break;
		case 'update':
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE id=".$_POST['cashier_id'];
			$response['result'] = $db->queryDirect($q);
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
			
			// process user fields
			$table = "users";
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE id=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
			
			// process user_info fields
                        $t_user_info = $db->queryOneRow('SELECT user_id from user_info WHERE user_id = '.$_POST['user_id']);
                        if(!empty($t_user_info['user_id'])){

			$table = "user_info";
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE user_id=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
                        } else {
                            $q = "INSERT INTO `user_info` (
				       `user_id`,
				       `telephone`,
				       `cellphone`,
				       `address`,
				       `address2`,
				       `country`,
				       `city`,
				       `island_id`,
				       `gender`,
				       `date_of_birth`
			) VALUES (				
				'".$_POST['user_id']."',
				'".$_POST['telephone']."',
				'".$_POST['cellphone']."',
				'".$_POST['address']."',
				'".$_POST['address2']."',
				'".$_POST['country']."',
                                '".$_POST['city']."',
                                '".$_POST['island_id']."',
                                '".$_POST['gender']."',
                                '".$_POST['date_of_birth']."'
			);";
                        
			$new_id = $db->queryInsert($q);
                        $response['result'] = true;
                         
                        }
			
			break;
		case 'insert':			
			$q = "INSERT INTO `panel_user` (
				`id`,
				`user_id`,
				`start_date`,
				`commission_rate`,
				`location_id`,
				`payout_scheme_id`,
				`receipt_message_id`,
				`created_by_user_id`,
				`created_on`,
				`shift_id`,
                                `location_change`,
                                `petty_cash_rights`
			) VALUES (
				NULL,
				'".$_POST['user_id']."',
				'".$_POST['start_date']."',
				'".$_POST['commission_rate']."',
				'".$_POST['location_id']."',
				'".$_POST['payout_scheme_id']."',
				'".$_POST['receipt_message_id']."',
				'".$session->userinfo['id']."',
				'".$now."',
				'-1',
                                '".(empty($_POST['location_change']) ? '0' : '1')."',
                                '".(empty($_POST['petty_cash_rights']) ? '0' : '1')."'
			);";
			$new_id = $db->queryInsert($q);
			$response['result'] = true;
			
			if($new_id < 1){
				$response['result'] = false;
				$response['query'] = $q;
			}
			
			break;

                  case 'change_cashier_location_change':

                         if(empty($_POST['value'])){
                             $_POST['value'] = '0';
                         }

                         $q = "UPDATE `panel_user` SET location_change = %i WHERE user_id = %i";
                         $db->queryOneRow($q, array($_POST['value'],$_POST['user_id']));

                         $response['success'] = true;

                         break;

                case 'change_petty_cash_rights':

                         if(empty($_POST['value'])){
                             $_POST['value'] = '0';
                         }

                         $q = "UPDATE `panel_user` SET petty_cash_rights = %i WHERE user_id = %i";
                         $db->queryOneRow($q, array($_POST['value'],$_POST['user_id']));

                         $response['success'] = true;

                         break;

		case 'update_photo':
			$photo_arr = explode("&", $_POST['photo_path']);
			$photo_path = $photo_arr[0];
		
			$q = "UPDATE `$table` SET `profile_picture_path` = '".$photo_path."' WHERE customer_id=$id;";
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['result'] = false;
				$response['query'] = $q;
			}
			
			break;
		case 'add_funds':		
			// error check
			$add_amount = str_replace("$","",$_POST['add_amount']);
			if(!is_numeric($add_amount) || $add_amount <= 0){
				$response['success'] = "false";
				$response['errors'] .= "Invalid Amount<br>";
			}
			$q = "SELECT shift_id FROM `panel_user` WHERE `user_id`=%i";
			$shift = $db->queryOneRow($q, array($_POST['user_id']));
			if($shift['shift_id'] == -1){
				$response['success'] = "false";
				$response['errors'] .= "The cashier must have a shift active.<br>";
			}
			
			if($response['success'] != "false"){
				$core->make_panel_user_transaction($add_amount, 14, "Funds added from the administration panel", "NULL", "-9999", $shift['shift_id']);
				$response['success'] = "true";
			}
			break;
		case 'add_vm' :
 	                // error check
			$add_amount = str_replace("$","",$_POST['add_amount']);
			if(!is_numeric($add_amount) || $add_amount <= 0){
				$response['success'] = "false";
				$response['errors'] .= "Invalid Amount<br>";
			}
			$q = "SELECT shift_id FROM `panel_user` WHERE `user_id`=%i";
			$shift = $db->queryOneRow($q, array($_POST['user_id']));
			if($shift['shift_id'] == -1){
				$response['success'] = "false";
				$response['errors'] .= "The cashier must have a shift active.<br>";
			}
			
			if($response['success'] != "false"){
				$core->make_virtual_money_transaction($add_amount, $_POST['user_id'], $shift['shift_id']);
				$response['success'] = "true";
			}
			break;

                case 'vm_info' :
                        
			$q = "SELECT shift_id,balance FROM `panel_user` WHERE `user_id`=%i";
			$shift = $db->queryOneRow($q, array($_POST['user_id']));
			if($shift['shift_id'] == -1){
			      $response['success'] = "false";
			      $response['errors'] .= "The cashier must have a shift active.<br>";
			}
			
			if($response['success'] != "false"){
			      $query = "SELECT SUM(virtual_amount) as total_virtual_money FROM panel_user_virtual_money WHERE user_id = %i AND panel_user_shift_id = %i";
                              $response['current_balance'] = $shift['balance'];
                              $t_vm_total = $db->queryOneRow($query, array($_POST['user_id'],$shift['shift_id']));
                              $i_vm_total = $t_vm_total['total_virtual_money'];
                              $response['total_vm'] = (!empty($i_vm_total) ? $i_vm_total : '0.00');  

                              $q = "SELECT SUM(amount) FROM `panel_user_transaction` WHERE shift_id=%i GROUP BY type_id";
			      $transactions = $db->query($q, array($shift['shift_id']));

			      $payouts = 0;
			      $sales = 0;
			      foreach($transactions as $trans){
				   if($trans['SUM(amount)'] < 0){
				    	  $payouts += $trans['SUM(amount)'];
				   }else{
					  $sales += $trans['SUM(amount)'];
				   }
			      }
				
			      $q = "SELECT * FROM `panel_user_shift` WHERE id=%i";
			      $shift_info = $db->queryOneRow($q, array($shift['shift_id']));
                              $response['opening_balance'] = (!empty($shift_info["opening"]) ? $shift_info["opening"] : '0.00');
                              $i_expected_balance = $shift_info["opening"]+$sales+$payouts;
			      $response['expected_balance'] = (!empty($i_expected_balance) ? $i_expected_balance : '0.00'); 
			                                     
			      $t_virtual_balance = $core->get_virtual_balance($shift['shift_id'],$_POST['user_id']);
                              $response['current_vm'] = (!empty($t_virtual_balance['virtual_money']) ? $t_virtual_balance['virtual_money'] : '0.00');
                              $response['payouts'] = (!empty($payouts) ? number_format($payouts) : '0.00000000');
                              $response['sales'] = (!empty($sales) ? number_format($sales) : '0.00000000');
                              $i_used_vm = $i_vm_total - $response['current_vm'];
                              $response['used_vm'] = (!empty($i_used_vm) ? $i_used_vm : '0.00');
                              $response['success'] = true;
                      
			 }						
			break;

                case 'add_cashier_vm':
                         
                        // error check
			$add_amount = str_replace("$","",$_POST['add_amount']);
			if(!is_numeric($add_amount) || $add_amount <= 0){
				$response['success'] = "false";
				$response['errors'] .= "Invalid Amount<br>";
			}
                       
                        if($response['success'] != "false"){
                              $q = "UPDATE panel_user SET `virtual_money_offset` = ".$add_amount." WHERE user_id = %i";
			      $shift = $db->queryOneRow($q, array($_POST['user_id']));
                        }
                    
                        $response['success'] = true;

                        break;

                case 'reset_cashier_vm':

                         $q = "UPDATE panel_user SET `virtual_money_offset` = NULL WHERE user_id = %i";
			 $db->queryOneRow($q, array($_POST['user_id']));
                       
                         $response['success'] = true; 

                         break;
               
		case 'update_security':		
			// handle user table changes
			$q = "UPDATE `users` SET `locked` = '".$_POST['locked']."', `google_auth_enabled` = '".$_POST['google_auth_enabled']."', `force_password_change` = '".$_POST['force_password_change']."', `locked_expiration` = '".$_POST['locked_expiration']."' WHERE `id`=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			if($response['result'] != true){
				$response['result'] = false;
				$response['query'] = $q;
				break;
			}
			
			if($_POST['pass'] != ""){
				if($_POST['pass'] == $_POST['conf_pass']){
					// change salt
					$usersalt = $session->generateRandStr(8);
					$database->updateUserField("", "usersalt", $usersalt, $_POST['user_id']);
					$response['result'] = $db->queryDirect($q);
					if($response['result'] != true){
						$response['result'] = false;
						$response['query'] = '$database->updateUserField("", "usersalt", $usersalt, '.$_POST['user_id'].')';
						break;
					}
					
					// change password hash
					$database->updateUserField("", "password", sha1($usersalt.$_POST['pass']), $_POST['user_id']);
					$response['result'] = $db->queryDirect($q);
					if($response['result'] != true){
						$response['result'] = false;
						$response['query'] = '$database->updateUserField("", "password", sha1($usersalt.'.$_POST['pass'].'), '.$_POST['user_id'].')';
						break;
					}
				}else{
					$response['result'] = false;
					$response['query'] = 'Password and confirmation did not match';
					break;
				}
			}
			
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
