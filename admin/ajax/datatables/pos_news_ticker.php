<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'pos_news_ticker';
	
	// Table's primary key
	$primary_key = 'id';

	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'pos.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'pos.content',
			'dt'        => 'content',
			'field' 	=> 'content',
			'as' 		=> 'content',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pos.ticker_type_id',
			'dt'        => 'area',
			'field' 	=> 'area',
			'as' 		=> 'area',
			'formatter' => function( $d, $row ) {
				if($d == 1){
					if($row['location_id'] == -1){
						return "All Locations";
					}else{
						return $row['location'];
					}
				}else{
					if($row['island_id'] == -1){
						return "All Islands";
					}else{
						return $row['island'];
					}
				}
			}
		),
		array(
			'db'        => 'pl.name',
			'dt'        => 'location',
			'field' 	=> 'location',
			'as' 		=> 'location',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'i.name',
			'dt'        => 'island',
			'field' 	=> 'island',
			'as' 		=> 'island',
			'formatter' => function( $d, $row ) {
				return $row['island_id'] == -1 ? "All Islands" : $d;
			}
		),
		array(
			'db'        => 'pos.location_id',
			'dt'        => 'location_id',
			'field' 	=> 'location_id',
			'as' 		=> 'location_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pos.island_id',
			'dt'        => 'island_id',
			'field' 	=> 'island_id',
			'as' 		=> 'island_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pos.is_franchise',
			'dt'        => 'is_franchise',
			'field' 	=> 'is_franchise',
			'as' 		=> 'is_franchise',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pos.is_franchise',
			'dt'        => 'location_type',
			'field' 	=> 'location_type',
			'as' 		=> 'location_type',
			'formatter' => function( $d, $row ) {
				if($d == "-1"){
					return "Any";
				}elseif($d == "0"){
					return "Non-Franchise";
				}else{
					return "Franchise";
				}
			}
		),
		array(
			'db'        => 'ptt.type',
			'dt'        => 'ticker_type',
			'field' 	=> 'ticker_type',
			'as' 		=> 'ticker_type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pos.ticker_type_id',
			'dt'        => 'ticker_type_id',
			'field' 	=> 'ticker_type_id',
			'as' 		=> 'ticker_type_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pos.expiration_date',
			'dt'        => 'expiration_date',
			'field' 	=> 'expiration_date',
			'as' 		=> 'expiration_date',
			'formatter' => function( $d, $row ) {
				return date( 'm/d/Y', strtotime($d));
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' 	=> 'user_id',
			'as'		=> 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' 	=> 'firstname',
			'as'		=> 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' 	=> 'lastname',
			'as'		=> 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pos.added_by',
			'dt'        => 'added_by',
			'field' 	=> 'added_by',
			'as'		=> 'added_by',
			'formatter' => function( $d, $row ) {
				return $row['firstname'].' '.$row['lastname']." (".$row["user_id"].")";
			}
		),
		array(
			'db'        => 'pos.added_on',
			'dt'        => 'added_on',
			'field' 	=> 'added_on',
			'as' 		=> 'added_on',
			'formatter' => function( $d, $row ) {
				return date( 'm/d/Y h:i A', strtotime($d));
			}
		)
	);

	$join_query = "FROM `pos_news_ticker` as pos LEFT JOIN `pos_ticker_type` as `ptt` ON `ptt`.`id`=`pos`.`ticker_type_id` LEFT JOIN users as u ON `pos`.`added_by` = `u`.`id` LEFT JOIN `island` as `i` ON `i`.`id`=`pos`.`island_id` LEFT JOIN `panel_location` AS `pl` ON `pl`.`id`=`pos`.`location_id`";
	
	//$extra_where = "";
	
	//$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);