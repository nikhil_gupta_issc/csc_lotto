<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_ticket';
	
	// Table's primary key
	$primary_key = 'ticket_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 't.ticket_id',
			'dt' => 'DT_RowId',
			'field' => 'ticket_id',
			'as' => 'ticket_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'g.short_name',
			'dt'        => 'game',
			'field' => 'game',
			'as' => 'game',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.bet_amount',
			'dt'        => 'net',
			'field' => 'net',
			'as' => 'net',
			'formatter' => function( $d, $row ) {
				return "".number_format(($row['sales'] - $row['winnings'] - $row['commission'])) . " ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(w.winning_amount)',
			'dt'        => 'winnings',
			'field' => 'winnings',
			'as' => 'winnings',
			'formatter' => function( $d, $row ) {
				return "".number_format($d) . " ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 't.number_of_bets',
			'dt'        => 'number_of_bets',
			'field' => 'number_of_bets',
			'as' => 'number_of_bets',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'SUM(b.bet_amount*t.commission_rate)',
			'dt'        => 'commission',
			'field' => 'commission',
			'as' => 'commission',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(b.bet_amount)',
			'dt'        => 'sales',
			'field' => 'sales',
			'as' => 'sales',
			'formatter' => function( $d, $row ) {
				return "".number_format($d). " ".CURRENCY_FORMAT."";
			}
		)
	);


	$join_query = "FROM `lotto_ticket` AS `t` JOIN `lotto_bet` AS `b` ON `t`.`ticket_id`=`b`.`ticket_id` JOIN `lotto_game` AS `g` ON `g`.`id`=`b`.`game_id` LEFT JOIN `winners` AS `w` ON `w`.`bet_id`=`b`.`bet_id`";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`purchase_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `purchase_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`purchase_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `purchase_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	$group_by = "GROUP BY `g`.`short_name`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
