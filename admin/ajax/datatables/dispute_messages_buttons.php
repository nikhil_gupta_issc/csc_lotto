<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$response = array();
	$message = isset($_POST['message']) ? $_POST['message'] : null;
	$dispute_id = isset($_POST['dispute_id']) ? $_POST['dispute_id'] : null;
	$resolution_details = isset($_POST['resolution_details']) ? $_POST['resolution_details'] : null;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	if($_POST['action'] == 'respond'){
		$response['errors'] = "";
		if($message == null){
			$response['errors'] .= "You must enter a message.<br>";
		}
		if($response['errors'] == ""){
			$q = "SELECT * FROM `disputes` AS `d` LEFT JOIN `users` AS `u` ON `u`.`id`=`d`.`user_id` WHERE `d`.`id`=%i";
			$info = $db->queryOneRow($q, array($dispute_id));
			
			$token = sha1('dispute'.$info['user_id'].$info['created_on']);
			
			$q = "INSERT INTO `dispute_messages` (`dispute_id`,`user_id`,`message`,`created_on`) VALUES (%i, %i, %s, %s)";
			$message_id = $db->queryInsert($q, array($dispute_id, $session->userinfo['id'], $message, $now));
			
			$mailer->send_email($info['email'], "CSCLotto Dispute Response", "There is a new response for your dispute. Click the link below to view/reply to the message.<br><br>".ROOTPATH."/disputes.php?id=".$dispute_id."&token=".$token);
			
			$response['success'] = true;
		}else{
			$response['success'] = false;
		}
	}
	
	if($_POST['action'] == 'mark_resolved'){
		$response['errors'] = "";
		if($resolution_details == null){
			$response['errors'] .= "You must enter a resolution detail.<br>";
		}
		if($response['errors'] == ""){
			$q = "SELECT * FROM `disputes` AS `d` LEFT JOIN `users` AS `u` ON `u`.`id`=`d`.`user_id` WHERE `d`.`id`=%i";
			$info = $db->queryOneRow($q, array($dispute_id));
			
			$token = sha1('dispute'.$info['user_id'].$info['created_on']);
			
			$q = "UPDATE `disputes` SET `resolution` = %s, `is_resolved`=1 WHERE `id`=%i";
			$db->queryDirect($q, array($resolution_details, $dispute_id));
			
			$q = "INSERT INTO `dispute_messages` (`dispute_id`,`user_id`,`message`,`created_on`) VALUES (%i, %i, %s, %s)";
			$message_id = $db->queryInsert($q, array($dispute_id, $session->userinfo['id'], $resolution_details, $now));
			
			$mailer->send_email($info['email'], "CSCLotto Dispute Closed", "Your dispute with CSCLotto has been closed. For all message details, click on the following link.<br><br>".ROOTPATH."/disputes.php?id=".$dispute_id."&token=".$token);
			
			$response['success'] = true;
		}else{
			$response['success'] = false;
		}
	}
	
	echo json_encode($response);