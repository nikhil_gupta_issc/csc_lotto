<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_ticket';
	
	// Table's primary key
	$primary_key = 'ticket_id';

	//Get the cashier and to - from filter dates
	$cashier_id = $_GET['cashier_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 't.ticket_id',
			'dt' => 'DT_RowId',
			'field' => 'ticket_id',
			'as' => 'ticket_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 't.ticket_number',
			'dt'        => 'ticket_number',
			'field' => 'ticket_number',
			'as' => 'ticket_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 't.cashier_id',
			'dt'        => 'cashier_id',
			'field' => 'cashier_id',
			'as' => 'cashier_id',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "Online" : $row['firstname']." ".$row['lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 't.total',
			'dt'        => 'total',
			'field' => 'total',
			'as' => 'total',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." " . CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'l.name',
			'dt'        => 'location_id',
			'field' => 'location_id',
			'as' => 'location_id',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "Online" : $d;
			}
		),
		array(
			'db'        => 't.shift_id',
			'dt'        => 'shift_id',
			'field' => 'shift_id',
			'as' => 'shift_id',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "Online" : $d;
			}
		),
		array(
			'db' => 'put.balance',
			'dt' => 'balance',
			'field' => 'balance',
			'as' => 'balance',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'put.initial_balance',
			'dt' => 'initial_balance',
			'field' => 'initial_balance',
			'as' => 'initial_balance',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 't.purchase_date',
			'dt'        => 'purchase_date',
			'field' => 'purchase_date',
			'as' => 'purchase_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		)
	);


	$join_query = "FROM `lotto_ticket` AS `t` LEFT JOIN `users` AS `u` ON `t`.`cashier_id`=`u`.`id` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id`=`l`.`id` LEFT JOIN `panel_user_transaction` AS `put` ON `t`.`ticket_number`=`put`.`ticket_number`";
	
	$extra_where = "t.shift_id != 0";
	
	if($cashier_id != ""){
		$extra_where .= " AND t.cashier_id = '".$cashier_id."'";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`purchase_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `purchase_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`purchase_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `purchase_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
