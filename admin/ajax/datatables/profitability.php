<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user_transaction';

	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'put.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'pl.id',
			'dt'        => 'location_id',
			'field' => 'location_id',
			'as' => 'location_id',
			'formatter' => function( $d, $row ) {
				return "loc_".$d;
			}
		),
		array(
			'db'        => 'pl.name',
			'dt'        => 'location',
			'field' => 'location',
			'as' => 'location',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN put.type_id = 7 THEN put.amount END)',
			'dt'        => 'lotto_sold',
			'field' => 'lotto_sold',
			'as' => 'lotto_sold',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN put.type_id = 9 THEN put.amount END)',
			'dt'        => 'deposits',
			'field' => 'deposits',
			'as' => 'deposits',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN put.type_id = 10 THEN put.amount END)',
			'dt'        => 'withdrawals',
			'field' => 'withdrawals',
			'as' => 'withdrawals',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN put.type_id = 8 THEN put.amount END)',
			'dt'        => 'voids',
			'field' => 'voids',
			'as' => 'voids',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN put.type_id = 12 THEN put.amount END)',
			'dt'        => 'lotto_payouts',
			'field' => 'lotto_payouts',
			'as' => 'lotto_payouts',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(put.amount)',
			'dt'        => 'net',
			'field' => 'net',
			'as' => 'net',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'pl.id',
			'dt'        => 'transfer',
			'field' => 'transfer',
			'as' => 'transfer',
			'formatter' => function( $d, $row ) {
				return "N/A";
			}
		)
	);

	$join_query = "FROM `panel_user_transaction` AS `put` JOIN `panel_location` AS `pl` ON `put`.`location_id`=`pl`.`id`";
	
	if($_GET['location_id'] != ""){
		$extra_where = "`pl`.`id` = ".$_GET['location_id'];
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`put`.`transaction_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `put`.`transaction_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`put`.`transaction_date` <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND `put`.`transaction_date` <= '".$_GET['filter_date_to']."'";
		}
	}
	
	$group_by = "GROUP BY `put`.`location_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
