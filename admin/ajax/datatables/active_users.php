<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'active_users';
	
	// Table's primary key
	$primary_key = 'username';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'au.username',
			'dt' => 'DT_RowId',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'au.username',
			'dt'        => 'username',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'id',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'u.ip',
			'dt'        => 'ip',
			'field' => 'ip',
			'as' => 'ip',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.last_login',
			'dt'        => 'latest_activity',
			'field' => 'latest_activity',
			'as' => 'latest_activity',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format("m/d/Y h:i A");
			}
		),
		array(
			'db'        => 'u.last_login',
			'dt'        => 'inactive',
			'field' => 'inactive',
			'as' => 'inactive',
			'formatter' => function( $d, $row ) {
				global $core;
				$now = new DateTime();
				$dt = new DateTime($d);
				$interval = $now->diff($dt);
				return $core->format_interval($interval, true);
			}
		)
	);

	$join_query = "FROM `active_users` AS `au` LEFT JOIN `users` AS `u` ON `au`.`username`=`u`.`username`";
	
	//$group_by = "GROUP BY ";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);