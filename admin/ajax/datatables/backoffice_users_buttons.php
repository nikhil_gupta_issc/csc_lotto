<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "back_office_users";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'enable':
			$q = "UPDATE $table SET `is_disabled` = 0 WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			break;
		case 'disable':
			$q = "UPDATE $table SET `is_disabled` = 1 WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			break;
		case 'delete':
			$q = "DELETE FROM $table WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			if($response['result'] != true){
				$response['query'] = $q;
			}
			break;
		case 'update':
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE id=".$_POST['cust_id'];
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
			
			// process user_info fields
			$table = "user_info";
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE user_id=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
			
			break;
		case 'insert':
			$now_dt = new DateTime;
			$now = $now_dt->format("Y-m-d H:i:s");
			$q = "INSERT INTO $table (`id`, `user_id`, `created_by`, `created_on`) VALUES (NULL, '".$id."', '".$sesssion->userinfo['id']."', '".$now."');";
			$new_id = $db->queryInsert($q);
			$response['result'] = true;
			
			if($new_id < 1){
				$response['result'] = false;
				$response['query'] = $q;
			}
			
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);