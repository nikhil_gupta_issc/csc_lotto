<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'vouchers';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'v.id',
			'dt' => 'DT_RowId',
			'field' => 'ticket_id',
			'as' => 'ticket_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'v.code',
			'dt'        => 'code',
			'field' => 'code',
			'as' => 'code',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'v.start',
			'dt'        => 'start_date',
			'field' => 'start_date',
			'as' => 'start_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));;
			}
		),
		array(
			'db'        => 'v.end',
			'dt'        => 'end_date',
			'field' => 'end_date',
			'as' => 'end_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'v.user_id',
			'dt'        => 'user',
			'field' => 'user',
			'as' => 'user',
			'formatter' => function( $d, $row ) {
				return $d == "" ? "Global" : $row['firstname']." ".$row['lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'v.amount',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'v.is_enabled',
			'dt'        => 'is_enabled',
			'field' => 'is_enabled',
			'as' => 'is_enabled',
			'formatter' => function( $d, $row ) {
				return $d == "1" ? "Enabled" : "Disabled";
			}
		)
	);

	$join_query = "FROM `vouchers` AS `v` LEFT JOIN `users` AS `u` ON `u`.`id`=`v`.`user_id`";
	
	$extra_where = "v.is_system = '0'";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
