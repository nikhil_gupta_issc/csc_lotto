<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'pu.id',
			'dt' => 'DT_RowId',
			'field' => 'cashier_id',
			'as' => 'cashier_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'pl.id',
			'dt' => 'location_id',
			'field' => 'location_id',
			'as' => 'location_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'pu.id',
			'dt' => 'cashier_id',
			'field' => 'cashier_id',
			'as' => 'cashier_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'cashier_firstname',
			'field' => 'cashier_firstname',
			'as' => 'cashier_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'cashier_lastname',
			'field' => 'cashier_lastname',
			'as' => 'cashier_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'cashier_fullname',
			'field' => 'cashier_fullname',
			'as' => 'cashier_fullname',
			'formatter' => function( $d, $row ) {
				return $row['cashier_firstname']." ".$row['cashier_lastname'];
			}
		),	
		array(
			'db' => 'u.username',
			'dt' => 'cashier_username',
			'field' => 'cashier_username',
			'as' => 'cashier_username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.username',
			'dt' => 'cashier_username',
			'field' => 'cashier_username',
			'as' => 'cashier_username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.email',
			'dt' => 'cashier_email',
			'field' => 'cashier_email',
			'as' => 'cashier_email',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.last_login',
			'dt' => 'last_login',
			'field' => 'last_login',
			'as' => 'last_login',
			'formatter' => function( $d, $row ) {
				return $d > 0 ? date('m/d/Y h:i A', strtotime($d)) : "Never";
			}
		),
	);

	$join_query = "FROM `panel_user` AS `pu` LEFT JOIN `users` AS `u` ON `pu`.`user_id`=`u`.`id` LEFT JOIN `panel_location` AS `pl` ON `pu`.`location_id` = `pl`.`id`";
	
	if(isset($_GET['location_id'])){
		$extra_where = "`location_id` = ".$_GET['location_id'];
	}
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );

	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);