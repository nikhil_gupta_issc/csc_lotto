<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user_transaction';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Ticket id
	$location_id = $_GET['location_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'put.made_by',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'put.made_by',
			'dt'        => 'cashier_id',
			'field' => 'cashier_id',
			'as' => 'cashier_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'cashier_name',
			'field' => 'cashier_name',
			'as' => 'cashier_name',
			'formatter' => function( $d, $row ) {
				return $d." ".$row['lastname'];
			}
		),
		array(
			'db'        => 'COUNT(put.amount)',
			'dt'        => 'total_transactions',
			'field' => 'total_transactions',
			'as' => 'total_transactions',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'SUM(put.amount)',
			'dt'        => 'net',
			'field' => 'net',
			'as' => 'net',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		)
	);

	$join_query = "FROM `panel_user_transaction` AS `put` JOIN `users` AS `u` ON `u`.`id`=`put`.`made_by`";
	
	$extra_where = "put.location_id = '".$location_id."'";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`put`.`transaction_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `put`.`transaction_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`put`.`transaction_date` <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND `put`.`transaction_date` <= '".$_GET['filter_date_to']."'";
		}
	}
	
	$group_by = "GROUP BY `put`.`made_by`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
