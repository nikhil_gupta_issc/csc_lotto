<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'payout_scheme';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'ps.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'ps.name',
			'dt' => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ps.description',
			'dt' => 'description',
			'field' => 'description',
			'as' => 'description',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ball2.payout_rate',
			'dt' => 'ball2_rate',
			'field' => 'ball2_rate',
			'as' => 'ball2_rate',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "$0.00" : "$".$d;
			}
		),
		array(
			'db' => 'ball3.payout_rate',
			'dt' => 'ball3_rate',
			'field' => 'ball3_rate',
			'as' => 'ball3_rate',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "$0.00" : "$".$d;
			}
		),
		array(
			'db' => 'ball4.payout_rate',
			'dt' => 'ball4_rate',
			'field' => 'ball4_rate',
			'as' => 'ball4_rate',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "$0.00" : "$".$d;
			}
		),
		array(
			'db' => 'ball5.payout_rate',
			'dt' => 'ball5_rate',
			'field' => 'ball5_rate',
			'as' => 'ball5_rate',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "$0.00" : "$".$d;
			}
		),
		array(
			'db' => 'ball6.payout_rate',
			'dt' => 'ball6_rate',
			'field' => 'ball6_rate',
			'as' => 'ball6_rate',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "$0.00" : "$".$d;
			}
		),
		array(
			'db' => 'ps.is_disabled',
			'dt' => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				return $d == 1 ? "<span style='color:red;'>"."Disabled"."</span>" : "Enabled";
			}
		)
	);

	$join_query = "FROM `payout_scheme` AS `ps` LEFT JOIN `payout_scheme_detail` AS `ball2` ON `ball2`.`ball_count`='2' AND `ps`.`id`=`ball2`.`scheme_id` LEFT JOIN `payout_scheme_detail` AS `ball3` ON `ball3`.`ball_count`='3' AND `ps`.`id`=`ball3`.`scheme_id` LEFT JOIN `payout_scheme_detail` AS `ball4` ON `ball4`.`ball_count`='4' AND `ps`.`id`=`ball4`.`scheme_id` LEFT JOIN `payout_scheme_detail` AS `ball5` ON `ball5`.`ball_count`='5' AND `ps`.`id`=`ball5`.`scheme_id` LEFT JOIN `payout_scheme_detail` AS `ball6` ON `ball6`.`ball_count`='6' AND `ps`.`id`=`ball6`.`scheme_id`";
	
	//$extra_where = "";
	
	$group_by = "GROUP BY `ps`.`id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);