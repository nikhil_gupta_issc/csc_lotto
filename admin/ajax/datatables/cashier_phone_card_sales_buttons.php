<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$cashier_id = $_POST['cashier_id'];
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'overall_total':
			$q = "SELECT SUM(put.amount) AS total FROM `phone_cards` AS `pc` LEFT JOIN `users` AS `u` ON `pc`.`panel_user_id`=`u`.`id` LEFT JOIN `panel_user_transaction` AS `put` ON `put`.`id`=`pc`.`transaction_id`";
			
			if($cashier_id != ""){
				$extra_where = " WHERE pc.panel_user_id = '".$cashier_id."'";
			}
			
			if(isset($_POST['filter_date_from']) && $_POST['filter_date_from'] != "undefined"){
				if($extra_where == ""){
					$extra_where = " WHERE `created_on` >= '".$_POST['filter_date_from']."'";
				}else{
					$extra_where .= " AND `created_on` >= '".$_POST['filter_date_from']."'";
				}
			}
			
			if(isset($_POST['filter_date_to'])&& $_POST['filter_date_to'] != "undefined"){
				if($extra_where == ""){
					$extra_where .= " WHERE `created_on` <= '".$_POST['filter_date_to']." 23:59:59'";
				}else{
					$extra_where .= " AND `created_on` <= '".$_POST['filter_date_to']." 23:59:59'";
				}
			}

			$returned = $db->queryOneRow($q.$extra_where);
			$returned['total'] = $returned['total'] == null ? "0.00" : number_format($returned['total']);
			
			$response['result'] = $returned;
			$response['query'] = $q.$extra_where;
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
