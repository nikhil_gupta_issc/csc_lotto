<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'user_sessions';
	
	$customer_id = $_GET['customer_id'];
	
	// Table's primary key
	$primary_key = 'id';
	
	//Build subquery where
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($subquery_where == ""){
			$subquery_where = "ct.transaction_date >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($subquery_where == ""){
			$subquery_where = "ct.transaction_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$subquery_where .= " AND ct.transaction_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'us.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),array(
			'db' => 'us.id',
			'dt' => 'session_id',
			'field' => 'session_id',
			'as' => 'session_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return $d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'customer',
			'field' => 'customer',
			'as' => 'customer',
			'formatter' => function( $d, $row ) {
				return $d.' '.$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.midname',
			'dt' => 'midname',
			'field' => 'midname',
			'as' => 'midname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'us.start',
			'dt' => 'start',
			'field' => 'start',
			'as' => 'start',
			'formatter' => function( $d, $row ) {
				$date = new DateTime($d);
				return $date->format("m/d/Y h:i A");
			}
		),
		array(
			'db' => 'us.end',
			'dt' => 'end',
			'field' => 'end',
			'as' => 'end',
			'formatter' => function( $d, $row ) {
				$date = new DateTime($d);
				return $date->format("m/d/Y h:i A");
			}
		),
		array(
			'db' => 'us.end',
			'dt' => 'duration',
			'field' => 'duration',
			'as' => 'duration',
			'formatter' => function( $d, $row ) {
				global $core;
				$start_dt = new DateTime($row['start']);
				$end_dt = new DateTime($row['end']);
				$interval = $end_dt->diff($start_dt);
				return $core->format_interval($interval);
			}
		),
		array(
			'db' => 'us.termination_reason',
			'dt' => 'termination_reason',
			'field' => 'termination_reason',
			'as' => 'termination_reason',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => '(SELECT SUM(ct1.amount) FROM customer_transaction AS ct1 WHERE ct1.transaction_type_id = 43 AND ct1.user_id = us.user_id AND ct1.user_session_id=us.id)',
			'dt' => 'promo_received',
			'field' => 'promo_received',
			'as' => 'promo_received',
			'formatter' => function( $d, $row ) {
				if($d == null){
					return "$0.00";
				}else{
					return "".number_format($d)." ".CURRENCY_FORMAT;
				}
			}
		),
		array(
			'db' => '(SELECT SUM(rb.bet_amount) FROM rapidballs_bet AS rb WHERE rb.user_session_id=us.id AND is_processed=0)',
			'dt' => 'rapidballs_remaining',
			'field' => 'rapidballs_remaining',
			'as' => 'rapidballs_remaining',
			'formatter' => function( $d, $row ) {
				if($d == null){
					return "$0.00";
				}else{
					return "".number_format($d)." ".CURRENCY_FORMAT;
				}
			}
		),
		array(
			'db' => '(SELECT SUM(lb.bet_amount) FROM lotto_bet AS lb WHERE lb.user_session_id=us.id AND is_processed=0)',
			'dt' => 'lotto_remaining',
			'field' => 'lotto_remaining',
			'as' => 'lotto_remaining',
			'formatter' => function( $d, $row ) {
				if($d == null){
					return "$0.00";
				}else{
					return "".number_format($d)." ".CURRENCY_FORMAT;
				}
			}
		),
		array(
			'db' => '(SELECT SUM(ct1.amount) FROM customer_transaction AS ct1 WHERE ct1.transaction_type_id = 43 AND ct1.user_id = us.user_id AND ct1.user_session_id=us.id)',
			'dt' => 'funds_incomplete',
			'field' => 'funds_incomplete',
			'as' => 'funds_incomplete',
			'formatter' => function( $d, $row ) {
				if($row['lotto_remaining'] == null && $row['rapidballs_remaining'] == null){
					return "$0.00";
				}else{
					return "".number_format($row['lotto_remaining'] + $row['rapidballs_remaining'])." ".CURRENCY_FORMAT;
				}
			}
		),
		array(
			'db' => '(SELECT SUM(ct1.bonus_used) FROM customer_transaction AS ct1 WHERE ct1.bonus_used != 0 AND ct1.user_id = us.user_id AND ct1.user_session_id=us.id)',
			'dt' => 'promo_wagered',
			'field' => 'promo_wagered',
			'as' => 'promo_wagered',
			'formatter' => function( $d, $row ) {
				if($d == null){
					return "$0.00";
				}else{
					return "".number_format($d)." ".CURRENCY_FORMAT;
				}
			}
		),
		array(
			'db' => '(SELECT SUM(ct1.amount) FROM customer_transaction AS ct1 WHERE ct1.amount > 0 AND ct1.transaction_type_id != 5 AND ct1.user_session_id=us.id AND ct1.transaction_type_id != 6 AND ct1.transaction_type_id != 43 AND ct1.user_id = us.user_id)',
			'dt' => 'amount_won',
			'field' => 'amount_won',
			'as' => 'amount_won',
			'formatter' => function( $d, $row ) {
				if($d == null){
					return "$0.00";
				}else{
					return "".number_format($d)." ".CURRENCY_FORMAT;
				}
			}
		),
		array(
			'db' => '(SELECT SUM(ct1.amount) FROM customer_transaction AS ct1 WHERE ct1.amount < 0 AND ct1.transaction_type_id != 5 AND ct1.user_session_id=us.id AND ct1.transaction_type_id != 6 AND ct1.transaction_type_id != 43 AND ct1.user_id = us.user_id)',
			'dt' => 'amount_wagered',
			'field' => 'amount_wagered',
			'as' => 'amount_wagered',
			'formatter' => function( $d, $row ) {
				if($d == null){
					return "$0.00";
				}else{
					return "".number_format($d)." ".CURRENCY_FORMAT;
				}
			}
		),
		array(
			'db' => '(SELECT SUM(ct1.amount) FROM customer_transaction AS ct1 WHERE ct1.transaction_type_id=6 AND ct1.user_session_id=us.id AND ct1.user_id = us.user_id)',
			'dt' => 'deposits',
			'field' => 'deposits',
			'as' => 'deposits',
			'formatter' => function( $d, $row ) {
				if($d == null){
					return "$0.00";
				}else{
					return "".number_format($d)." ".CURRENCY_FORMAT;
				}
			}
		),
		array(
			'db' => '(SELECT SUM(ct1.amount) FROM customer_transaction AS ct1 WHERE ct1.transaction_type_id=5 AND ct1.user_session_id=us.id AND ct1.user_id = us.user_id)',
			'dt' => 'withdrawals',
			'field' => 'withdrawals',
			'as' => 'withdrawals',
			'formatter' => function( $d, $row ) {
				if($d == null){
					return "$0.00";
				}else{
					return "".number_format($d)." ".CURRENCY_FORMAT;
				}
			}
		),
		array(
			'db' => 'us.is_active',
			'dt' => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				if($d == 1){
					return "Active";
				}else{
					return ucwords($row["termination_reason"]);
				}
			}
		)
	);

	$join_query = "FROM `user_sessions` AS `us` LEFT JOIN `users` AS `u` ON `us`.`user_id`=`u`.`id`";
	
	if($customer_id > 0){
		$extra_where = " us.user_id = '".$customer_id."'";
	}else{
		$extra_where = " us.user_id != '-9999'";
	}
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		$extra_where .= " AND us.start >= '".$_GET['filter_date_from']."'";
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		$extra_where .= " AND us.start <= '".$_GET['filter_date_to']." 23:59:59'";
	}
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
