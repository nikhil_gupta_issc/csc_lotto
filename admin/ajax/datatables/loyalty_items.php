<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'loyalty_items';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'li.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'li.name',
			'dt' => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'li.type',
			'dt' => 'type',
			'field' => 'type',
			'as' => 'type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'li.description',
			'dt' => 'description',
			'field' => 'description',
			'as' => 'description',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'li.image_path',
			'dt' => 'image_path',
			'field' => 'image_path',
			'as' => 'image_path',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'li.id',
			'dt' => 'loyalty_item_id',
			'field' => 'loyalty_item_id',
			'as' => 'loyalty_item_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'li.point_cost',
			'dt' => 'point_cost',
			'field' => 'point_cost',
			'as' => 'point_cost',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'li.value',
			'dt' => 'value',
			'field' => 'value',
			'as' => 'value',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." " . CURRENCY_FORMAT;
			}
		),
		array(
			'db' => 'li.is_disabled',
			'dt' => 'is_disabled',
			'field' => 'is_disabled',
			'as' => 'is_disabled',
			'formatter' => function( $d, $row ) {
				if($d == 1){
					return "<span style='color:red;'>Disabled</span>";					
				}else{
					return "<span style='color:green;'>Enabled</span>";
				}
			}
		)
	);

	$join_query = "FROM `loyalty_items` AS `li`";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
