<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'overall_total':
			$q = "SELECT SUM(pu.balance) AS total FROM `panel_user_shift` AS `pus` LEFT JOIN `panel_user` AS `pu` ON `pu`.`user_id`=`pus`.`user_id`";
			
			$extra_where = " WHERE `pus`.`shift_end` = '0000-00-00 00:00:00'";

			$returned = $db->queryOneRow($q.$extra_where);
			$returned['total'] = $returned['total'] == null ? "0.00000000" : number_format($returned['total'],8);
			
			$response['result'] = $returned;
			$response['query'] = $q.$extra_where;
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
