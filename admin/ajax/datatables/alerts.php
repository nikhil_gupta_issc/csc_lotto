<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'alert_rules';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'a.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'a.rule_name',
			'dt'        => 'rule_name',
			'field' => 'rule_name',
			'as' => 'rule_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.table_name',
			'dt'        => 'table_name',
			'field' => 'table_name',
			'as' => 'table_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.column_name',
			'dt'        => 'column_name',
			'field' => 'column_name',
			'as' => 'column_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.item_name',
			'dt'        => 'item_name',
			'field' => 'item_name',
			'as' => 'item_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.comparison',
			'dt'        => 'comparison',
			'field' => 'comparison',
			'as' => 'comparison',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.value',
			'dt'        => 'value',
			'field' => 'value',
			'as' => 'value',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.alert_type',
			'dt'        => 'alert_type',
			'field' => 'alert_type',
			'as' => 'alert_type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.is_disabled',
			'dt'        => 'is_disabled',
			'field' => 'is_disabled',
			'as' => 'is_disabled',
			'formatter' => function( $d, $row ) {
				$d = $d == 1 ? "Disabled" : "Enabled";
				return $d;
			}
		),
		array(
			'db'        => 'a.is_system',
			'dt'        => 'is_system',
			'field' => 'is_system',
			'as' => 'is_system',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'a.function_name',
			'dt'        => 'function_name',
			'field' => 'function_name',
			'as' => 'function_name',
			'formatter' => function( $d, $row ) {
                                $d = !empty($d) ? $d : '-';
				return $d;
			}
		),
		array(
			'db'        => 'a.is_disabled',
			'dt'        => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				if($row['is_system'] == 0){
					$d = $d == 1 ? "<span style='color: red;'>Disabled</span>" : "<span style='color: green;'>Enabled</span>";
				}else{
					$d = "<span style='color: blue;'>Enabled (Locked)</span>";
				}
				return $d;
			}
		)
	);


	$join_query = "FROM `alert_rules` AS `a`";

	//$extra_where = "";

	//$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
