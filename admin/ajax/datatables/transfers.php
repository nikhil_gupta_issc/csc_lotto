<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'pending_transfers';
	
	// Table's primary key
	$primary_key = 'id';

	//Get the cashier and to - from filter dates
	$location_id = $_GET['location_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'pt.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'pl_from.name',
			'dt'        => 'from_location',
			'field' => 'from_location',
			'as' => 'from_location',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pl_to.name',
			'dt'        => 'to_location',
			'field' => 'to_location',
			'as' => 'to_location',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pt.amount',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." " . CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'u_id',
			'field' => 'u_id',
			'as' => 'u_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pt.accepted_on',
			'dt'        => 'accepted_on',
			'field' => 'accepted_on',
			'as' => 'accepted_on',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format("m/d/Y h:i A");
			}
		),
		array(
			'db'        => 'pt.created_on',
			'dt'        => 'created_on',
			'field' => 'created_on',
			'as' => 'created_on',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format("m/d/Y h:i A");
			}
		),
		array(
			'db'        => 'sent_u.firstname',
			'dt'        => 'sent_firstname',
			'field' => 'sent_firstname',
			'as' => 'sent_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'sent_u.lastname',
			'dt'        => 'sent_lastname',
			'field' => 'sent_lastname',
			'as' => 'sent_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'sent_u.id',
			'dt'        => 'sent_by',
			'field' => 'sent_by',
			'as' => 'sent_by',
			'formatter' => function( $d, $row ) {
				return $row['sent_firstname']." ".$row['sent_lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'pt.is_accepted',
			'dt'        => 'accepted',
			'field' => 'accepted',
			'as' => 'accepted',
			'formatter' => function( $d, $row ) {
				if($d == 1){
					$dt = new DateTime($row['accepted_on']);
					return "Accepted by ".$row['firstname']." ".$row['lastname']." (".$row['u_id'].") On ".$dt->format("m/d/Y h:i A");
				}else{
					return "Pending";
				}
			}
		)
	);

	$join_query = "FROM `pending_transfers` AS `pt` JOIN `panel_location` AS `pl_from` ON `pl_from`.`id`=`pt`.`from_location_id` JOIN `panel_location` AS `pl_to` ON `pl_to`.`id`=`pt`.`to_location_id` LEFT JOIN `users` AS `u` ON `pt`.`accepted_by`=`u`.`id` LEFT JOIN `users` AS `sent_u` ON `pt`.`created_by`=`sent_u`.`id`";
	
	if($location_id != ""){
		$extra_where .= "(pt.from_location_id = '".$location_id."' OR pt.to_location_id = '".$location_id."')";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`pt`.`created_on` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `pt`.`created_on` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`pt`.`created_on` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `pt`.`created_on` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "GROUP BY ";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
