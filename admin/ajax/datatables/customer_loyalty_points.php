<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'users';
	
	// Table's primary key
	$primary_key = 'id';
	
	$gold_limit = $db->queryOneRow("SELECT value FROM settings WHERE setting='gold_point_amount'");
	$platinum_limit = $db->queryOneRow("SELECT value FROM settings WHERE setting='platinum_point_amount'");
	$diamond_limit = $db->queryOneRow("SELECT value FROM settings WHERE setting='diamond_point_amount'");

	//Get status selection
	$status = $_GET['status'];
	
	$customer_id = $_GET['customer_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'u.id', 
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'first_name',
			'field' => 'first_name',
			'as' => 'first_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'last_name',
			'field' => 'last_name',
			'as' => 'last_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'c.user_id',
			'dt'        => 'full_name',
			'field' => 'full_name',
			'as' => 'full_name',
			'formatter' => function( $d, $row ) {
				return $row['first_name']." ".$row['last_name']." (".$d.")";
			}
		),
		array(
			'db'        => 'c.loyalty_points',
			'dt'        => 'loyalty_points_achieved',
			'field' => 'loyalty_points_achieved',
			'as' => 'loyalty_points_achieved',
			'formatter' => function( $d, $row ) {
				return number_format(round($d));
			}
		),
		array(
			'db'        => 'SUM(lpr.number_of_points)',
			'dt'        => 'loyalty_points_redeemed',
			'field' => 'loyalty_points_redeemed',
			'as' => 'loyalty_points_redeemed',
			'formatter' => function( $d, $row ) {
				return number_format(round($d));
			}
		),
		array(
			'db'        => 'c.loyalty_points',
			'dt'        => 'loyalty_points',
			'field' => 'loyalty_points',
			'as' => 'loyalty_points',
			'formatter' => function( $d, $row ) {
				return number_format(round($row['loyalty_points_achieved'] - $row['loyalty_points_redeemed']));
			}
		),
		array(
			'db'        => 'c.loyalty_points',
			'dt'        => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				global $core;
				
				return $core->get_loyalty_level($row['user_id']);
			}
		)
	);
	
	$join_query = "FROM `users` AS `u` JOIN `customers` AS `c` ON `c`.`user_id`=`u`.`id` LEFT JOIN `loyalty_points_redemption` AS `lpr` ON `lpr`.`user_id`=`u`.`id`";
	
	$group_by = "GROUP BY `c`.`user_id`";

	if($customer_id > 0){
		$extra_where = " c.user_id = '".$customer_id."'";
	}else{
		$extra_where = " c.user_id != '-9999'";
	}
	
	if($status == "Regular"){
		$extra_where .= " AND c.loyalty_points < ".$gold_limit['value'];
	}elseif($status == "Gold"){
		$extra_where .= " AND c.loyalty_points >= ".$gold_limit['value']." AND c.loyalty_points < ".$platinum_limit['value'];
	}elseif($status == "Platinum"){
		$extra_where .= " AND c.loyalty_points >= ".$platinum_limit['value']." AND c.loyalty_points < ".$diamond_limit['value'];
	}elseif($status == "Diamond"){
		$extra_where .= " AND c.loyalty_points >= ".$diamond_limit['value'];
	}
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
