<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'payout_scheme';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'ps.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'psd.payout_rate',
			'dt'        => 'payout_rate',
			'field' => 'payout_rate',
			'as' => 'payout_rate',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'psd.payout_rate',
			'dt'        => 'max_exposure',
			'field' => 'max_exposure',
			'as' => 'max_exposure',
			'formatter' => function( $d, $row ) {
				return "".number_format($row['payout_rate']*$_GET['limit'])." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'ps.name',
			'dt'        => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	$join_query = "FROM `payout_scheme` AS `ps` JOIN `payout_scheme_detail` AS `psd` ON `ps`.`id`=`psd`.`scheme_id`";
	
	$extra_where = "psd.ball_count = '".$_GET['ball_count']."'";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
