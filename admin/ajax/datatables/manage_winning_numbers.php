<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'winners';
	
	$house_id = $_GET["house_id"];
	$date = $_GET["date"];

	// Table's primary key
	$primary_key = 'winner_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'w.winner_id',
			'dt' => 'DT_RowId',
			'field' => 'winner_id',
			'as' => 'winner_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'w.ticket_number',
			'dt'        => 'ticket_number',
			'field' => 'ticket_number',
			'as' => 'ticket_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.draw_date',
			'dt'        => 'draw_date',
			'field' => 'draw_date',
			'as' => 'draw_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y", strtotime($d));
			}
		),
		array(
			'db'        => 't.purchase_date',
			'dt'        => 'purchase_date',
			'field' => 'purchase_date',
			'as' => 'purchase_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 'w.winning_amount',
			'dt'        => 'winning_amount',
			'field' => 'winning_amount',
			'as' => 'winning_amount',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 'g.name',
			'dt'        => 'game_name',
			'field' => 'game_name',
			'as' => 'game_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.ball_string',
			'dt'        => 'ball_string',
			'field' => 'ball_string',
			'as' => 'ball_string',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.is_boxed',
			'dt'        => 'is_boxed',
			'field' => 'is_boxed',
			'as' => 'is_boxed',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "S" : "B";
			}
		),
		array(
			'db'        => 'l.name',
			'dt'        => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $d == "" ? "Online" : $d;
			}
		)
	);

	$join_query = "FROM `winners` AS `w` LEFT JOIN `lotto_bet` AS `b` ON `b`.`bet_id`=`w`.`bet_id` LEFT JOIN `lotto_ticket` AS `t` ON `t`.`ticket_id`=`b`.`ticket_id` LEFT JOIN `lotto_game` AS `g` ON `b`.`game_id`=`g`.`id` LEFT JOIN `panel_location` AS `l` ON `l`.`id`=`t`.`location_id`";
	
	$extra_where = "b.draw_date = '".$date."' AND g.house_id = '".$house_id."'";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);