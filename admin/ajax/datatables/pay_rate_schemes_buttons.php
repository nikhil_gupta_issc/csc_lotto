<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "payout_scheme";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	switch ($action){
		case 'enable':
			$q = "UPDATE $table SET `is_disabled` = 0 WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			break;
		case 'disable':
			$q = "UPDATE $table SET `is_disabled` = 1 WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			break;
		case 'delete':
			$q = "DELETE FROM $table WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			if($response['result'] == true){
				// delete linked pay rates
				$q = "DELETE FROM ".$table."_detail WHERE scheme_id=".$id;
				$response['result'] = $db->queryDirect($q);
				$response['query'] = $q;
			}
			break;
		case 'update':
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE id=$id";
			$response['result'] = $db->queryDirect($q);

			// process related table
			$table = $table."_detail";
			
			//Make a lookup to decide if the values need inserted or updated
			$q = "SELECT * FROM $table WHERE `scheme_id`=$id";
			$scheme_details = $db->query($q);
			foreach($scheme_details as $detail){
				$scheme_ball_lookup[$detail['ball_count']] = $detail['payout_rate'];
			}
			
			if(isset($scheme_ball_lookup['2'])){
				$q = "UPDATE $table SET `payout_rate` = '".trim($_POST['ball2_rate'], "$")."' WHERE `scheme_id`=$id AND `ball_count` = '2'";
			}else{
				$q = "INSERT INTO $table (`payout_rate`, `ball_count`, `scheme_id`) VALUES ('".trim($_POST['ball2_rate'], "$")."', '2', '$id')";
			}
			if($db->queryDirect($q) != true){
				$response['result'] = false;
				$response['query'] = $q;
			}
			if(isset($scheme_ball_lookup['3'])){
				$q = "UPDATE $table SET `payout_rate` = '".trim($_POST['ball3_rate'], "$")."' WHERE `scheme_id`=$id AND `ball_count` = '3'";
			}else{
				$q = "INSERT INTO $table (`payout_rate`, `ball_count`, `scheme_id`) VALUES ('".trim($_POST['ball3_rate'], "$")."', '3', '$id')";
			}
			if($db->queryDirect($q) != true){
				$response['result'] = false;
				$response['query'] = $q;
			}
			if(isset($scheme_ball_lookup['4'])){
				$q = "UPDATE $table SET `payout_rate` = '".trim($_POST['ball4_rate'], "$")."' WHERE `scheme_id`=$id AND `ball_count` = '4'";
			}else{
				$q = "INSERT INTO $table (`payout_rate`, `ball_count`, `scheme_id`) VALUES ('".trim($_POST['ball4_rate'], "$")."', '4', '$id')";
			}
			if($db->queryDirect($q) != true){
				$response['result'] = false;
				$response['query'] = $q;
			}
			if(isset($scheme_ball_lookup['5'])){
				$q = "UPDATE $table SET `payout_rate` = '".trim($_POST['ball5_rate'], "$")."' WHERE `scheme_id`=$id AND `ball_count` = '5'";
			}else{
				$q = "INSERT INTO $table (`payout_rate`, `ball_count`, `scheme_id`) VALUES ('".trim($_POST['ball5_rate'], "$")."', '5', '$id')";
			}
			if($db->queryDirect($q) != true){
				$response['result'] = false;
				$response['query'] = $q;
			}
			if(isset($scheme_ball_lookup['6'])){
				$q = "UPDATE $table SET `payout_rate` = '".trim($_POST['ball6_rate'], "$")."' WHERE `scheme_id`=$id AND `ball_count` = '6'";
			}else{
				$q = "INSERT INTO $table (`payout_rate`, `ball_count`, `scheme_id`) VALUES ('".trim($_POST['ball6_rate'], "$")."', '6', '$id')";
			}
			if($db->queryDirect($q) != true){
				$response['result'] = false;
				$response['query'] = $q;
			}
			break;
		case 'insert':			
			$q = "INSERT INTO `payout_scheme` (`id`, `name`, `description`, `is_system_only`, `is_disabled`, `is_deleted`) VALUES (NULL, '".$_POST['name']."', '".$_POST['description']."', '0', '0', '0');";
			$new_id = $db->queryInsert($q);
			$response['result'] = true;
			
			if($new_id < 1){
				$response['result'] = false;
				$response['query'] = $q;
			}else{
				// insert was successful, so store payout rates in associated table
				$q = "INSERT INTO `payout_scheme_detail` (`id`, `scheme_id`, `ball_count`, `payout_rate`) VALUES (NULL, '".$new_id."', '2', '".$_POST['ball2_rate']."');";
				if($db->queryInsert($q) < 1){
					$response['result'] = false;
					$response['query'] = $q;
				}
				$q = "INSERT INTO `payout_scheme_detail` (`id`, `scheme_id`, `ball_count`, `payout_rate`) VALUES (NULL, '".$new_id."', '3', '".$_POST['ball3_rate']."');";
				if($db->queryInsert($q) < 1){
					$response['result'] = false;
					$response['query'] = $q;
				}
				$q = "INSERT INTO `payout_scheme_detail` (`id`, `scheme_id`, `ball_count`, `payout_rate`) VALUES (NULL, '".$new_id."', '4', '".$_POST['ball4_rate']."');";
				if($db->queryInsert($q) < 1){
					$response['result'] = false;
					$response['query'] = $q;
				}
				$q = "INSERT INTO `payout_scheme_detail` (`id`, `scheme_id`, `ball_count`, `payout_rate`) VALUES (NULL, '".$new_id."', '5', '".$_POST['ball5_rate']."');";
				if($db->queryInsert($q) < 1){
					$response['result'] = false;
					$response['query'] = $q;
				}
			}
			
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);