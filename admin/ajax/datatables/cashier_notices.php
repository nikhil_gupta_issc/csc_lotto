<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'cashier_payout_notices';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'cpn.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'cpn.rule_name',
			'dt'        => 'rule_name',
			'field' => 'rule_name',
			'as' => 'rule_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'cpn.table_name',
			'dt'        => 'table_name',
			'field' => 'table_name',
			'as' => 'table_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'cpn.column_name',
			'dt'        => 'column_name',
			'field' => 'column_name',
			'as' => 'column_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'cpn.comparison',
			'dt'        => 'comparison',
			'field' => 'comparison',
			'as' => 'comparison',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'cpn.value',
			'dt'        => 'value',
			'field' => 'value',
			'as' => 'value',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'cpn.is_disabled',
			'dt'        => 'is_disabled',
			'field' => 'is_disabled',
			'as' => 'is_disabled',
			'formatter' => function( $d, $row ) {
				$d = $d == 1 ? "Disabled" : "Enabled";
				return $d;
			}
		),
		array(
			'db'        => 'cpn.title',
			'dt'        => 'title',
			'field' => 'title',
			'as' => 'title',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'cpn.message',
			'dt'        => 'message',
			'field' => 'message',
			'as' => 'message',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'cpn.is_disabled',
			'dt'        => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				$d = $d == 1 ? "<span style='color: red;'>Disabled</span>" : "<span style='color: green;'>Enabled</span>";
				return $d;
			}
		)
	);


	$join_query = "FROM `cashier_payout_notices` AS `cpn`";

	//$extra_where = "";

	//$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);