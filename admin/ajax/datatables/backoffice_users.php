<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'back_office_users';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'bo.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $d.' '.$row['midname'].' '.$row['lastname']." (".$row["user_id"].")";
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.midname',
			'dt' => 'midname',
			'field' => 'midname',
			'as' => 'midname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.username',
			'dt' => 'username',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.email',
			'dt' => 'email',
			'field' => 'email',
			'as' => 'email',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.last_login',
			'dt' => 'last_login',
			'field' => 'last_login',
			'as' => 'last_login',
			'formatter' => function( $d, $row ) {
				if($d){
					$last_login = new DateTime($d);
					return $last_login->format('m/d/Y h:i:s A');
				}else{
					return "Never";
				}
			}
		)
	);

	$join_query = "FROM `back_office_users` AS `bo` LEFT JOIN `users` AS `u` ON `bo`.`user_id` = `u`.`id`";
	
	$extra_where = "user_id > 0";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);