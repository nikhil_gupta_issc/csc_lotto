<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'phone_cards';
	
	// Table's primary key
	$primary_key = 'id';

	//Get the cashier and to - from filter dates
	$cashier_id = $_GET['cashier_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'pc.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'pc.created_on',
			'dt'        => 'purchase_date',
			'field' => 'purchase_date',
			'as' => 'purchase_date',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format("m/d/Y h:i A");
			}
		),
		array(
			'db'        => 'pc.pin',
			'dt'        => 'pin',
			'field' => 'pin',
			'as' => 'pin',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'put.amount',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'cashier_id',
			'field' => 'cashier_id',
			'as' => 'cashier_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'cashier',
			'field' => 'cashier',
			'as' => 'cashier',
			'formatter' => function( $d, $row ) {
				return $row["firstname"]." ".$row["lastname"]." (".$d.")";
			}
		)
	);


	$join_query = "FROM `phone_cards` AS `pc` LEFT JOIN `users` AS `u` ON `pc`.`panel_user_id`=`u`.`id` LEFT JOIN `panel_user_transaction` AS `put` ON `put`.`id`=`pc`.`transaction_id`";
	
	if($cashier_id != ""){
		$extra_where .= "pc.panel_user_id = '".$cashier_id."'";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`created_on` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `created_on` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`created_on` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `created_on` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
