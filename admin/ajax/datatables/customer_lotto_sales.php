<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_ticket';
	
	// Table's primary key
	$primary_key = 'ticket_id';
	
	$customer_id = $_GET['customer_id'];

	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 't.ticket_id',
			'dt' => 'DT_RowId',
			'field' => 'ticket_id',
			'as' => 'ticket_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 't.ticket_number',
			'dt'        => 'ticket_number',
			'field' => 'ticket_number',
			'as' => 'ticket_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'c.firstname',
			'dt'        => 'c_firstname',
			'field' => 'c_firstname',
			'as' => 'c_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'c.lastname',
			'dt'        => 'c_lastname',
			'field' => 'c_lastname',
			'as' => 'c_lastname',+
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 't.cashier_id',
			'dt'        => 'cashier',
			'field' => 'cashier',
			'as' => 'cashier',
			'formatter' => function( $d, $row ) {
				return $d == "-9999" ? "Online" : $row['c_firstname']." ".$row['c_lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'l.name',
			'dt'        => 'location',
			'field' => 'location',
			'as' => 'location',
			'formatter' => function( $d, $row ) {
				return $d == "" ? "Online" : $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'u_firstname',
			'field' => 'u_firstname',
			'as' => 'u_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'u_lastname',
			'field' => 'u_lastname',
			'as' => 'u_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 't.user_id',
			'dt'        => 'customer',
			'field' => 'customer',
			'as' => 'customer',
			'formatter' => function( $d, $row ) {
				return $row['u_firstname']." ".$row['u_lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 't.total',
			'dt'        => 'total',
			'field' => 'total',
			'as' => 'total',
			'formatter' => function( $d, $row ) {
				return "".number_format($d,8)." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 't.purchase_date',
			'dt'        => 'purchase_date',
			'field' => 'purchase_date',
			'as' => 'purchase_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		)
	);

	$join_query = "FROM `lotto_ticket` AS `t` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id`=`l`.`id` LEFT JOIN `users` AS `c` ON `c`.`id`=`t`.`cashier_id` LEFT JOIN `users` AS `u` ON `u`.`id`=`t`.`user_id`";
	
	if($customer_id > 0){
		$extra_where = "t.user_id = '".$customer_id."'";
	}else{
		$extra_where = "t.user_id != '-9999'";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where .= " AND t.purchase_date >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND t.purchase_date >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where .= " AND t.purchase_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND t.purchase_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
