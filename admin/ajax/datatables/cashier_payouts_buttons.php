<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$cashier_id = isset($_POST['cashier_id']) ? $_POST['cashier_id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'overall_total':
			$q = "SELECT SUM(put.amount) AS total FROM `panel_user` AS `pu` JOIN `panel_user_transaction` AS `put` ON `pu`.`user_id`=`put`.`made_by`";
			
			$extra_where = " WHERE `put`.`type_id`='12'";
			if($cashier_id != ""){
				$extra_where .= " AND put.made_by = '".$cashier_id."'";
			}

			if(isset($_POST['filter_date_from']) && $_POST['filter_date_from'] != "undefined"){
				$extra_where .= " AND `transaction_date` >= '".$_POST['filter_date_from']."'";
			}
			
			if(isset($_POST['filter_date_to'])&& $_POST['filter_date_to'] != "undefined"){
				$extra_where .= " AND `transaction_date` <= '".$_POST['filter_date_to']." 23:59:59'";
			}

			$returned = $db->queryOneRow($q.$extra_where);
			$returned['total'] = $returned['total'] == null ? "0.00000000" : number_format($returned['total']);
			
			$response['result'] = $returned;
			$response['query'] = $q.$extra_where;
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
