<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "users";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'enable':
			$q = "UPDATE $table SET `is_disabled` = 0 WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			
			// send email to notify user.
			$text_body = "The CSCLotto account linked to this email address has been reactivated and is no longer disabled.\n\n";
			$html_body = "<h2>Account No Longer Disabled</h2>The CSCLotto account linked to this email address has been reactivated and is no longer disabled.<br><br>";
			$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Account Reactivated", $html_body, $text_body);
			break;
		case 'disable':
			$q = "UPDATE $table SET `is_disabled` = 1 WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			$response['query'] = $q;
			
			// send email to notify user.
			$text_body = "The CSCLotto account linked to this email address has been disabled and is no longer usable.\n\n Please contact support with questions concerning this.";
			$html_body = "<h2>Account Disabled</h2>The CSCLotto account linked to this email address has been disabled and is no longer usable.<br><br> Please contact support with questions concerning this.<br><br>";
			$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Account Disabled", $html_body, $text_body);
			break;
		case 'ban':
			$q = "UPDATE $table SET `deleted` = '1' WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			if($response['result'] == true){
				// store who deleted this user
				$q = "UPDATE $table SET `deleted_by` = '".$session->userinfo['id']."' WHERE id=".$id;
				$response['result'] = $db->queryDirect($q);
				
				// update related customer account
				$q = "UPDATE `customers` SET `is_disabled` = '1', `disabled_by` = '".$session->userinfo['id']."' WHERE user_id=".$id;
				$response['result'] = $db->queryDirect($q);
				
				// update related admin account
				//$q = "DELETE FROM `back_office_users` WHERE user_id=".$id;
				//$response['result'] = $db->queryDirect($q);
				
				// update related cashier account
				$q = "UPDATE `panel_user` SET `is_disabled` = '1', `disabled_by` = '".$session->userinfo['id']."' WHERE user_id=".$id;
				$response['result'] = $db->queryDirect($q);
				
				// get username and add to banned users table
				$q = "SELECT `username` FROM $table WHERE id=".$id;
				$u_info = $db->queryOneRow($q);
				$q = "REPLACE INTO `banned_users` (`username`, `timestamp`, `alerted`) VALUES ('".$u_info['username']."', UNIX_TIMESTAMP(), '0')";
				$response['result'] = $db->queryDirect($q);
				
				// add record to the exclusions log
				$q = "INSERT INTO `exclusion_log` (`id`, `exclusion_type_id`, `excluded_on`, `exclusion_end`, `user_id`) VALUES (NULL, '7', '".$now."', NULL, '".$id."');";
				$db->queryInsert($q);
				
				// send email to notify user.
				$text_body = "The CSCLotto account linked to this email address has been banned and is no longer usable.\n\n Please contact support with questions concerning this.";
				$html_body = "<h2>Account Banned</h2>The CSCLotto account linked to this email address has been banned and is no longer usable.<br><br> Please contact support with questions concerning this.<br><br>";
				$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Account Banned", $html_body, $text_body);
			}
			break;
		case 'update':
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE id=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
			
			// process user fields
			$table = "users";
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE id=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			
			if($response['result'] != true){
				$response['query'] = $q;
				break;
			}
			
			break;
		case 'insert':
			// create random password
			$random_password = $database->generateRandStr(8);
		
			$response['result'] = $core->create_user($_POST['firstname'], $_POST['lastname'], $_POST['username'], $random_password, $_POST['email'], true);
			
			if($form->num_errors > 0){
				foreach($form->errors as $error){
					$response['query'] .= "\n<br>".$error;
				}
				$response['result'] = false;
			}
			
			break;
		case 'update_security':		
			// handle user table changes
			$q = "UPDATE `users` SET `userlevel` = '".$_POST['userlevel']."', `locked` = '".$_POST['locked']."', `google_auth_enabled` = '".$_POST['google_auth_enabled']."', `force_password_change` = '".$_POST['force_password_change']."', `locked_expiration` = '".$_POST['locked_expiration']."' WHERE `id`=".$_POST['user_id'];
			$response['result'] = $db->queryDirect($q);
			if($response['result'] != true){
				$response['result'] = false;
				$response['query'] = $q;
				break;
			}
			
			if($_POST['pass'] != ""){
				if($_POST['pass'] == $_POST['conf_pass']){
					// change salt
					$usersalt = $session->generateRandStr(8);
					$database->updateUserField("", "usersalt", $usersalt, $_POST['user_id']);
					$response['result'] = $db->queryDirect($q);
					if($response['result'] != true){
						$response['result'] = false;
						$response['query'] = '$database->updateUserField("", "usersalt", $usersalt, '.$_POST['user_id'].')';
						break;
					}
					
					// change password hash
					$database->updateUserField("", "password", sha1($usersalt.$_POST['pass']), $_POST['user_id']);
					$response['result'] = $db->queryDirect($q);
					if($response['result'] != true){
						$response['result'] = false;
						$response['query'] = '$database->updateUserField("", "password", sha1($usersalt.'.$_POST['pass'].'), '.$_POST['user_id'].')';
						break;
					}
				}else{
					$response['result'] = false;
					$response['query'] = 'Password and confirmation did not match';
					break;
				}
			}
			
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
