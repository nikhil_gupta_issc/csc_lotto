<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'disputes';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'd.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'd.subject',
			'dt'        => 'subject',
			'field' => 'subject',
			'as' => 'subject',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'd.created_on',
			'dt'        => 'created_on',
			'field' => 'created_on',
			'as' => 'created_on',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user',
			'field' => 'user',
			'as' => 'user',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$d.")";
			}
		),
		array(
			'db'        => '(SELECT `user_id` FROM `dispute_messages` WHERE `dispute_messages`.`dispute_id`=`d`.`id` ORDER BY id DESC LIMIT 1)',
			'dt'        => 'last_message_by',
			'field' => 'last_message_by',
			'as' => 'last_message_by',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'd.is_resolved',
			'dt'        => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				if($d == 1){
					return "Closed";
				}else{
					if($row['last_message_by'] == $row['user_id']){
						return "Pending";
					}else{
						return "Awaiting Customer Response";
					}
				}
			}
		)
	);
	
	$join_query = "FROM `disputes` AS `d` LEFT JOIN `dispute_messages` AS `dm` ON `dm`.`dispute_id`=`d`.`id` LEFT JOIN `users` AS `u` ON `u`.`id`=`d`.`user_id`";
	
	$extra_where = "";
	
	if($_GET['is_resolved'] == 0){
		$extra_where = "d.is_resolved = 0";
	}else{
		$extra_where = "d.is_resolved = 1";
	}
	
	$group_by = "GROUP BY `d`.`id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);