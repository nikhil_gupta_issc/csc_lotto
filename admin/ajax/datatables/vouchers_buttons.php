<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$table = "vouchers";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	switch ($action){
		case 'enable':
			$q = "UPDATE `$table` SET `is_enabled` = 1 WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			break;
		case 'disable':
			$q = "UPDATE `$table` SET `is_enabled` = 0 WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			break;
		case 'insert':
			$errors = "";
		
			if(strlen($_POST['code']) > 50){
				$errors .= "The code can only contain 50 or less characters<br>";
			}
			
			if(!is_numeric(trim($_POST['amount'], "$"))){
				$errors .= "The amount must be numeric<br>";
			}
		
			if($errors == ""){
				$q = "INSERT INTO $table (`code`, `start`, `end`, `amount`, `user_id`, `is_enabled`,`redeem_ticket_status`) VALUES ('".strtoupper($_POST['code'])."', '".$_POST['start_date']."', '".$_POST['end_date']."', '".$_POST['amount']."', ".$_POST['user_id'].", '1',".$_POST['redeem_ticket_date'].")";
				$result2 = $db->queryInsert($q);
				if($result2 <= 0){
					echo $q;
					die();
				}
				$result['successful'] = "true";
			}else{
				$result['successful'] = "false";
				$result['errors'] = $errors;
			}
			break;
		default:
		   die("Invalid Action");
	}
	
	echo json_encode($result);
