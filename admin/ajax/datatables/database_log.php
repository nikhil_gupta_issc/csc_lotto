<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'log_database_changes';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'ldc.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'ldc.query',
			'dt'        => 'query',
			'field' => 'query',
			'as' => 'query',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ldc.parameters',
			'dt'        => 'parameters',
			'field' => 'parameters',
			'as' => 'parameters',
			'formatter' => function( $d, $row ) {
				if ($d == NULL){
					$d = "<s>No Encoding</s>";
				}
				return $d;
			}
		),
		array(
			'db'        => 'ldc.executed_by',
			'dt'        => 'executed_by',
			'field' => 'executed_by',
			'as' => 'executed_by',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db'        => 'ldc.executed_on',
			'dt'        => 'executed_on',
			'field' => 'executed_on',
			'as' => 'executed_on',
			'formatter' => function( $d, $row ) {
				return date( 'm/d/Y h:i A', strtotime($d));
			}
		),
		array(
			'db'        => 'ldc.referrer',
			'dt'        => 'referrer',
			'field' => 'referrer',
			'as' => 'referrer',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ldc.values_before',
			'dt'        => 'values_before',
			'field' => 'values_before',
			'as' => 'values_before',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ldc.values_after',
			'dt'        => 'values_after',
			'field' => 'values_after',
			'as' => 'values_after',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ldc.is_success',
			'dt'        => 'is_success',
			'field' => 'is_success',
			'as' => 'is_success',
			'formatter' => function( $d, $row ) {
				if($d == 1){ 
					$d = "<span style='color: green;'>Yes</span>";
				}else{ 
					$d = "<span style='color: red;'>No</span>"; 
				};
				return $d;
			}
		),
	);


	$join_query = "FROM `log_database_changes` AS `ldc` JOIN `users` as `u` ON `ldc`.`executed_by`=`u`.`username`";

	//$extra_where = "";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`executed_on` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `executed_on` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`executed_on` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `executed_on` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
