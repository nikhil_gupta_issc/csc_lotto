<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	switch ($action){
		case 'enable':
			$q = "UPDATE `lotto_game` SET `is_disabled` = 0 WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				die();
			}
			break;
		case 'disable':
			$q = "UPDATE `lotto_game` SET `is_disabled` = 1 WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				die();
			}
			break;
		case 'delete':
			$q = "DELETE FROM `lotto_game` WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				die();
			}
			break;
		case 'update':
			$failed = false;
			
			$q = "UPDATE `lotto_game` SET `house_id` = '".$_POST['house_id']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `number_of_balls` = '".$_POST['number_of_balls']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `short_name` = '".$_POST['short_name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `name` = '".$_POST['name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `min_bet` = '".$_POST['pos_min']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `max_per_ball` = '".$_POST['pos_exp']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `max_bet` = '".$_POST['pos_max']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `hh_min_bet` = '".$_POST['hh_min']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `hh_max_per_ball` = '".$_POST['hh_exp']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `hh_max_bet` = '".$_POST['hh_max']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `web_min_bet` = '".$_POST['web_min']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `web_max_per_ball` = '".$_POST['web_exp']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `web_max_bet` = '".$_POST['web_max']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			
			// create link to xml
			$q = "SELECT * FROM `lotto_game_xml_link` WHERE game_id=".$id;
			$exists = $db->queryOneRow($q);
			if($exists != NULL){
				$q = "UPDATE `lotto_game_xml_link` SET `xml_game_id` = '".$_POST['xml_feed']."' WHERE game_id=".$id;
			}else{
				$q = "INSERT INTO `lotto_game_xml_link` (`game_id`, `xml_game_id`) VALUES ('".$id."', '".$_POST['xml_feed']."')";
			}
			$result = $db->queryDirect($q);
			if($result == false){
				$failed = true;
			}else{
				// log the change in db
				$now_dt = new DateTime();
				$now = $now_dt->format("Y-m-d H:i:s");
				$q = "INSERT INTO `lotto_game_xml_link_changes` (`id`, `game_id`, `xml_game_id`, `changed_by`, `changed_on`) VALUES (NULL, '".$id."', '".$_POST['xml_feed']."', '".$session->userinfo['id']."', '".$now."')";
				$result = $db->queryDirect($q);
			}
			
			if($failed == true){
				die();
			}
			break;
		case 'insert':
			$q = "INSERT INTO `lotto_game` (`id`, `house_id`, `number_of_balls`, `short_name`, `name`, `min_bet`, `max_per_ball`, `max_bet`, `hh_min_bet`, `hh_max_per_ball`, `hh_max_bet`, `web_min_bet`, `web_max_per_ball`, `web_max_bet`, `is_disabled`) VALUES (NULL, '".$_POST['house_id']."', '".$_POST['number_of_balls']."', '".$_POST['short_name']."', '".$_POST['name']."', '".$_POST['pos_min']."', '".$_POST['pos_exp']."', '".$_POST['pos_max']."', '".$_POST['hh_min']."', '".$_POST['hh_exp']."', '".$_POST['hh_max']."', '".$_POST['web_min']."', '".$_POST['web_exp']."', '".$_POST['web_max']."', '0')";
			$game_id = $db->queryInsert($q);
			if($game_id <= 0){
				die();
			}
			
			// create link to xml
			$q = "INSERT INTO `lotto_game_xml_link` (`game_id`, `xml_game_id`) VALUES ('".$game_id."', '".$_POST['xml_feed']."')";
			$result = $db->queryDirect($q);
			if($result == false){
				die();
			}else{
				// log the change in db
				$now_dt = new DateTime();
				$now = $now_dt->format("Y-m-d H:i:s");
				$q = "INSERT INTO `lotto_game_xml_link_changes` (`id`, `game_id`, `xml_game_id`, `changed_by`, `changed_on`) VALUES (NULL, '".$id."', '".$_POST['xml_feed']."', '".$session->userinfo['id']."', '".$now."')";
				$result = $db->queryDirect($q);
			}
			break;
		case 'mass_update':
			$failed = false;

			$q = "UPDATE `lotto_game` SET `web_min_bet` = '".$_POST['web_min']."'";
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `web_max_per_ball` = '".$_POST['web_exp']."'";
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `lotto_game` SET `web_max_bet` = '".$_POST['web_max']."'";
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			
			if($failed == true){
				die();
			}
			break;
		case 'get_xml_games':
			$q = "SELECT * FROM `xml_lotto_game` lg INNER JOIN `xml_lotto_location_game_map` lgm ON lg.id=lgm.game_id INNER JOIN `xml_lotto_location` l ON l.stateprov_id=lgm.stateprov_id WHERE `lg`.`num_digits`='".$_POST['num_balls']."' ORDER BY `l`.`stateprov_name` ASC, `lg`.`name` ASC";
			$result = $db->query($q);
			
			if($_POST['num_balls'] == 2){
				// add 3-ball games to results
				$q = "SELECT * FROM `xml_lotto_game` lg INNER JOIN `xml_lotto_location_game_map` lgm ON lg.id=lgm.game_id INNER JOIN `xml_lotto_location` l ON l.stateprov_id=lgm.stateprov_id WHERE `lg`.`num_digits`='3' ORDER BY `l`.`stateprov_name` ASC, `lg`.`name` ASC";
				$result2 = $db->query($q);
				$result = array_merge($result, $result2);
			}
			
			break;
		case 'get_xml_game_id':
			$q = "SELECT * FROM `lotto_game_xml_link` WHERE `game_id` = '".$id."'";
			$reponse = $db->queryOneRow($q);
			$result = $reponse['xml_game_id'];
			
			break;
		default:
		   die();
	}
	
	echo json_encode($result);