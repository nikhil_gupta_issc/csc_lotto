<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'customer_rebate';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'c.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'c.id',
			'dt' => 'id',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {				
				return $d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.casino_rb_amount',
			'dt' => 'casino_rb_amount',
			'field' => 'casino_rb_amount',
			'as' => 'casino_rb_amount',
			'formatter' => function( $d, $row ) {
				return abs($d);
			}
		),
		array(
			'db' => 'c.lotto_rb_amount',
			'dt' => 'lotto_rb_amount',
			'field' => 'lotto_rb_amount',
			'as' => 'lotto_rb_amount',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.rapidball_rb_amount',
			'dt' => 'rapidball_rb_amount',
			'field' => 'rapidball_rb_amount',
			'as' => 'rapidball_rb_amount',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.sports_rb_amount',
			'dt' => 'sports_rb_amount',
			'field' => 'sports_rb_amount',
			'as' => 'sports_rb_amount',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.casino_loss_percentage',
			'dt' => 'casino_loss_percentage',
			'field' => 'casino_loss_percentage',
			'as' => 'casino_loss_percentage',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.lotto_loss_percentage',
			'dt' => 'lotto_loss_percentage',
			'field' => 'lotto_loss_percentage',
			'as' => 'lotto_loss_percentage',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.rapidball_loss_percentage',
			'dt' => 'rapidball_loss_percentage',
			'field' => 'rapidball_loss_percentage',
			'as' => 'rapidball_loss_percentage',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.sports_loss_percentage',
			'dt' => 'sports_loss_percentage',
			'field' => 'sports_loss_percentage',
			'as' => 'sports_loss_percentage',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
			array(
			'db'        => 'u.username',
			'dt'        => 'username',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),	
		
		array(
			'db' => 'u.firstname',
			'dt' => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$row['user_id'].")";
			}
		),		
		
		array(
			'db' => 'c.total_amount',
			'dt' => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "$0.00" : "$".$d;
			}
		),
		array(
			'db' => 'c.rebate_date',
			'dt' => 'rebate_date',
			'field' => 'rebate_date',
			'as' => 'rebate_date',
			'formatter' => function( $d, $row ) {
				return $d ;
			}
		)
		
		
	);

	$join_query = "FROM `customer_rebate` AS `c` LEFT JOIN `users` AS `u` ON `c`.`user_id`=`u`.`id`";
	
	//$extra_where = "";
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "DATE(`rebate_date`) >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND DATE(`rebate_date`) >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "DATE(`rebate_date`) <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND DATE(`rebate_date`) <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
