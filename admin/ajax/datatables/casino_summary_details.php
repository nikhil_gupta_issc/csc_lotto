<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'customer_transaction';
	
	// Table's primary key
	$primary_key = 'transaction_id';

	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'ct.transaction_id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'SUBSTR(ct.transaction_details, INSTR(ct.transaction_details,"-") + 2, LENGTH(ct.transaction_details))',
			'dt'        => 'game_name',
			'field' => 'game_name',
			'as' => 'game_name',
			'formatter' => function( $d, $row ) {
				return $d." (".$row['game_id'].")";
			}
		),
		array(
			'db'        => 'ct.transaction_record_id',
			'dt'        => 'game_id',
			'field' => 'game_id',
			'as' => 'game_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'CASE WHEN ct.transaction_type_id=2 THEN `amount` * -1 END',
			'dt'        => 'win_amount',
			'field' => 'win_amount',
			'as' => 'win_amount',
			'formatter' => function( $d, $row ) {
				if($d != 0){
					return "".number_format($d) . " ".CURRENCY_FORMAT."";
				}else{
					return "";
				}
			}
		),
		array(
			'db'        => 'CASE WHEN ct.transaction_type_id=1 THEN `amount` * -1 END',
			'dt'        => 'bet_amount',
			'field' => 'bet_amount',
			'as' => 'bet_amount',
			'formatter' => function( $d, $row ) {
				if($d != 0){
					return "".number_format($d) . " ".CURRENCY_FORMAT."";
				}else{
					return "";
				}
			}
		),
		array(
			'db'        => 'ct.vendor_id',
			'dt'        => 'vendor_id',
			'field' => 'vendor_id',
			'as' => 'vendor_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'customer_name',
			'field' => 'customer_name',
			'as' => 'customer_name',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'ct.transaction_date',
			'dt'        => 'transaction_date',
			'field' => 'transaction_date',
			'as' => 'transaction_date',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format('m/d/Y h:i A');
			}
		)
	);

	$join_query = "FROM `customer_transaction` AS `ct` LEFT JOIN `users` AS `u` ON `u`.`id` = `ct`.`user_id`";
	
	$extra_where = "(`ct`.`transaction_type_id`=1 OR `ct`.`transaction_type_id`=2)";
	
	if($_GET['game_id'] != ""){
		$extra_where .= " AND ct.transaction_record_id = '".$_GET['game_id']."'";
	}
	
	if($_GET['vendor_id'] != ""){
		$extra_where .= " AND ct.vendor_id = '".$_GET['vendor_id']."'";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "ct.transaction_date >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND ct.transaction_date >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "ct.transaction_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND ct.transaction_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
