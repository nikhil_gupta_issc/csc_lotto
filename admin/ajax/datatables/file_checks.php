<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'file_hash_checks';
	
	// Table's primary key
	$primary_key = 'id';
	
	//Get the cashier and to - from filter dates
	$status = $_GET['status'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'fhc.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'fhc.datetime_checked',
			'dt'        => 'datetime_checked',
			'field' => 'datetime_checked',
			'as' => 'datetime_checked',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'fhc.file_path',
			'dt'        => 'path',
			'field' => 'path',
			'as' => 'path',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'fhc.size',
			'dt'        => 'size',
			'field' => 'size',
			'as' => 'size',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'fhc.modified_on',
			'dt'        => 'modified_on',
			'field' => 'modified_on',
			'as' => 'modified_on',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format('m/d/Y h:i:s A');
			}
		),
		array(
			'db'        => 'fhc.hash',
			'dt'        => 'hash',
			'field' => 'hash',
			'as' => 'hash',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'fhc.status',
			'dt'        => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ){
				$status = $d == 'approved' ? "<span style='color: green;'>".ucwords($d)."</span>" : "<span style='color: red;'>".ucwords($d)."</span>";
				return $status;
			}
		)
	);


	$join_query = "FROM `file_hash_checks` AS `fhc`";

	$extra_where = "";

	//$group_by = "";
	
	if($status != "" && $status != "undefined"){
		$extra_where .= "`fhc`.`status` = '".$status."'";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`fhc`.`datetime_checked` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `fhc`.`datetime_checked` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`fhc`.`datetime_checked` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `fhc`.`datetime_checked` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);