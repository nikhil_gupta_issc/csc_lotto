<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'adollar';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'a.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'a_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'a.adollar_number',
			'dt'        => 'adollar_number',
			'field' => 'adollar_number',
			'as' => 'adollar_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
	
		array(
			'db'        => 'a.adollar_value',
			'dt'        => 'adollar_value',
			'field' => 'adollar_value',
			'as' => 'adollar_value',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	//$join_query = "FROM `adollar` AS `a` LEFT JOIN `gift_card_redemptions` AS `gcr` ON `gcr`.`card_number`=`g`.`card_number` LEFT JOIN `users` AS `ua` ON `ua`.`id`=`g`.`added_by` LEFT JOIN `users` AS `us` ON `us`.`id`=`g`.`sold_by`";
	
	$join_query="from adollar as a";
	
	//$extra_where = "";
	
	//$group_by = "GROUP BY `a`.`id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
