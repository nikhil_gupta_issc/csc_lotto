<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_location';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'pl.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'pl.id',
			'dt'        => 'id',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'pl.name',
			'dt'        => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $d." (".$row["id"].")";
			}
		),
		array(
			'db'        => 'i.name',
			'dt'        => 'island',
			'field' => 'island',
			'as' => 'island',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	
	$join_query = "FROM `panel_location` AS `pl` LEFT JOIN `island` AS `i` ON `pl`.`island_id`=`i`.`id` JOIN `panel_user_shift` AS `s` ON `s`.`location_id`=`pl`.`id`";
	
	//$extra_where = "s.location_id = '".$_GET['location_id']."'";
	
	if($_GET['franchise'] == "franchise"){
		$extra_where = "pl.commission_rate <> 0";
	}elseif($_GET['franchise'] == "non_franchise"){
		$extra_where = "pl.commission_rate = 0";
	}
	
	if($_GET['location_id'] != "" && $extra_where != ""){
		$extra_where .= " AND pl.id=".$_GET['location_id'];
	}elseif($_GET['location_id'] != "" && $extra_where == ""){
		$extra_where = "pl.id=".$_GET['location_id'];
	}
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`shift_start` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `shift_start` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`shift_start` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `shift_start` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	$group_by = "GROUP BY `pl`.`id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
