<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'customers';
	
	// Table's primary key
	$primary_key = 'customer_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'c.customer_id',
			'dt' => 'DT_RowId',
			'field' => 'customer_id',
			'as' => 'customer_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'c.customer_id',
			'dt' => 'customer_id',
			'field' => 'customer_id',
			'as' => 'customer_id',
			'formatter' => function( $d, $row ) {				
				return $d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.customer_number',
			'dt' => 'customer_number',
			'field' => 'customer_number',
			'as' => 'customer_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.card_number',
			'dt' => 'card_number',
			'field' => 'card_number',
			'as' => 'card_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'customer_name',
			'field' => 'customer_name',
			'as' => 'customer_name',
			'formatter' => function( $d, $row ) {
				return $d.' '.$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.midname',
			'dt' => 'midname',
			'field' => 'midname',
			'as' => 'midname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.date_of_birth',
			'dt' => 'date_of_birth',
			'field' => 'date_of_birth',
			'as' => 'date_of_birth',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.gender',
			'dt' => 'gender',
			'field' => 'gender',
			'as' => 'gender',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.address',
			'dt' => 'address',
			'field' => 'address',
			'as' => 'address',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.address2',
			'dt' => 'address2',
			'field' => 'address2',
			'as' => 'address2',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.city',
			'dt' => 'city',
			'field' => 'city',
			'as' => 'city',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.country',
			'dt' => 'country',
			'field' => 'country',
			'as' => 'country',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.island_id',
			'dt' => 'island_id',
			'field' => 'island_id',
			'as' => 'island_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'i.name',
			'dt' => 'island',
			'field' => 'island',
			'as' => 'island',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.available_balance',
			'dt' => 'available_balance',
			'field' => 'available_balance',
			'as' => 'available_balance',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "$0.00" : "$".$d;
			}
		),
		array(
			'db' => 'c.bonus_balance',
			'dt' => 'bonus_balance',
			'field' => 'bonus_balance',
			'as' => 'bonus_balance',
			'formatter' => function( $d, $row ) {
				return $d <= 0 ? "$0.00" : "$".$d;
			}
		),
		array(
			'db' => 'u.email',
			'dt' => 'email',
			'field' => 'email',
			'as' => 'email',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.telephone',
			'dt' => 'phones',
			'field' => 'phones',
			'as' => 'phones',
			'formatter' => function( $d, $row ) {
				return $d.'<br>'.$row['cellphone'];
			}
		),
		array(
			'db' => 'ui.telephone',
			'dt' => 'telephone',
			'field' => 'telephone',
			'as' => 'telephone',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ui.cellphone',
			'dt' => 'cellphone',
			'field' => 'cellphone',
			'as' => 'cellphone',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'upload_checksum',
			'field' => 'upload_checksum',
			'as' => 'upload_checksum',
			'formatter' => function( $d, $row ) {
				return crc32(md5($d.date("HhHY")));
			}
		),
		array(
			'db' => 'c.drivers_license',
			'dt' => 'drivers_license',
			'field' => 'drivers_license',
			'as' => 'drivers_license',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.voters_card',
			'dt' => 'voters_card',
			'field' => 'voters_card',
			'as' => 'voters_card',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.passport',
			'dt' => 'passport',
			'field' => 'passport',
			'as' => 'passport',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.is_drivers_license_verified',
			'dt' => 'is_drivers_license_verified',
			'field' => 'is_drivers_license_verified',
			'as' => 'is_drivers_license_verified',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.is_voters_card_verified',
			'dt' => 'is_voters_card_verified',
			'field' => 'is_voters_card_verified',
			'as' => 'is_voters_card_verified',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.is_passport_verified',
			'dt' => 'is_passport_verified',
			'field' => 'is_passport_verified',
			'as' => 'is_passport_verified',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.profile_picture_path',
			'dt' => 'profile_picture_path',
			'field' => 'profile_picture_path',
			'as' => 'profile_picture_path',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		
		array(
			'db' => 'c.id_image_path',
			'dt' => 'id_image_path',
			'field' => 'id_image_path',
			'as' => 'id_image_path',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		
		array(
			'db' => 'u.locked',
			'dt' => 'locked',
			'field' => 'locked',
			'as' => 'locked',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'disabler.firstname',
			'dt' => 'dis_firstname',
			'field' => 'dis_firstname',
			'as' => 'dis_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'disabler.lastname',
			'dt' => 'dis_lastname',
			'field' => 'dis_lastname',
			'as' => 'dis_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.profile_picture_path',
			'dt' => 'profile_picture_path',
			'field' => 'profile_picture_path',
			'as' => 'profile_picture_path',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.is_disabled',
			'dt' => 'is_disabled',
			'field' => 'is_disabled',
			'as' => 'is_disabled',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'disabler.id',
			'dt' => 'disabled_by',
			'field' => 'disabled_by',
			'as' => 'disabled_by',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.national_security',
			'dt' => 'national_security',
			'field' => 'national_security',
			'as' => 'national_security',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'c.is_national_security_verified',
			'dt' => 'is_national_security_verified',
			'field' => 'is_national_security_verified',
			'as' => 'is_national_security_verified',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'c.is_disabled',
			'dt'        => 'status',
			'field'		=> 'status',
			'as' 		=> 'status',
			'formatter' => function( $d, $row ) {
				$locked = $row["locked"] == 1 ? "<span style='color:orange;'>Locked</span>" : "<span style='color:green;'>Enabled</span>";
				return $d == 1 ? "<span style='color:red;'>Disabled</span>" : $locked;
			}
		),
		array(
			'db'        => 'c.is_verified',
			'dt'        => 'kyc_verified',
			'field'		=> 'kyc_verified',
			'as' 		=> 'kyc_verified',
			'formatter' => function( $d, $row ) {
				return $d == 0 ? "<span style='color:red;'>Unverified</span>" : "<span style='color:green;'>Verified</span>";
			}
		),
		array(
			'db' => 'u.locked_expiration',
			'dt' => 'status_details',
			'field' => 'status_details',
			'as' => 'status_details',
			'formatter' => function( $d, $row ) {
				
				if($row['is_disabled']==1){
					return "Disabled by ".$row['dis_firstname']." ".$row['dis_lastname']." (".$row["disabled_by"].")";
				}elseif($row["locked"]==1){
					$lock_exp = new DateTime($d);
					$str = $lock_exp->format("m/d/Y h:i A");
					return "Locked until ".$str;
				}else{
					return "";
				}
			}
		)
	);

	$join_query = "FROM `customers` AS `c` LEFT JOIN `users` AS `u` ON `c`.`user_id`=`u`.`id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `u`.`id` LEFT JOIN `island` AS `i` ON `ui`.`island_id`=`i`.`id` LEFT JOIN `users` AS `disabler` ON `disabler`.`id`=`c`.`disabled_by`";
	
	//$extra_where = "";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
