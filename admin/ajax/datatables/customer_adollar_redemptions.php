<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'adollar_redemptions';
	
	// Table's primary key
	$primary_key = 'id';

	//Get the cashier and to - from filter dates
	$customer_id = $_GET['customer_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'vr.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'u_id',
			'field' => 'u_id',
			'as' => 'u_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$row['u_id'].")";
			}
		),
		array(
			'db'        => 'vr.card_number',
			'dt'        => 'code',
			'field' => 'code',
			'as' => 'code',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'vr.amount',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'vr.redeemed_on',
			'dt'        => 'redeemed_on',
			'field' => 'redeemed_on',
			'as' => 'redeemed_on',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format("m/d/Y h:i A");
			}
		)
	);

	$join_query = "FROM `adollar_redemptions` AS `vr` LEFT JOIN `users` AS `u` ON `u`.`id`=`vr`.`redeemed_by`";
	
	//$extra_where = "`ct`.`transaction_type_id`=43";
	
	if($customer_id > 0){
		$extra_where = " u.id = '".$customer_id."'";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`vr`.`redeemed_on` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `vr`.`redeemed_on` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`vr`.`redeemed_on` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `vr`.`redeemed_on` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "GROUP BY ";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
