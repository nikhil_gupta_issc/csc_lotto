<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "active_users";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$username = isset($_POST['username']) ? $_POST['username'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'force_logout':
			$q = "DELETE FROM $table WHERE username=%s";
			$db->queryDirect($q, array($username));  
                        $t_users = array();
                        $t_get_users = file_get_contents(DOMAIN_ROOT.'/cron/json/logged_out_users.json');
                        $t_users = json_decode($t_get_users,true);
                        unset($t_get_users);

                        $file = fopen(DOMAIN_ROOT.'/cron/json/logged_out_users.json',"w+");  
                        $t_users[$username] = 1;                     
			fwrite($file,json_encode($t_users));
			fclose($file); 
                        unset($t_users);                     
			
			// get details about person doing this
			$q = "SELECT * FROM `users` AS `u` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `u`.`id` WHERE `u`.`id`=%i";
			$admin_info = $db->queryOneRow($q, array($session->userinfo['id']));
			
			// Get customer info
			$q = "SELECT * FROM `users` AS `u` LEFT JOIN `customers` AS `c` ON `u`.`id` = `c`.`user_id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `c`.`user_id` WHERE `u`.`username`=%s";
			$cus_info = $db->queryOneRow($q, array($username));
			
			// log as a significant event
			$this->log_sigificant_event('Customer Account Changes', 'Logged Off Customer Account', $admin_info['firstname'].' '.$admin_info['lastname'].' ('.$session->userinfo['id'].') forced logoff for : '.$cus_info['firstname'].' '.$cus_info['lastname'].' ('.$cus_info['id'].')');
			
			$response['result'] = true;
			
			break;
		default:
			$response['result'] = false;
			$response['errors'] = "An unexpected error occurred";
	}
	
	echo json_encode($response);
