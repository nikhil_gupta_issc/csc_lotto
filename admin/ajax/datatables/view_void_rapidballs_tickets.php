<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'rapidballs_ticket';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'rt.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'rt.id',
			'dt' => 'ticket_id',
			'field' => 'ticket_id',
			'as' => 'ticket_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'rt.bet_on',
			'dt'        => 'bet_on',
			'field' => 'bet_on',
			'as' => 'bet_on',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rt.total_amount',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'rbt.description',
			'dt'        => 'bet_type',
			'field' => 'bet_type',
			'as' => 'bet_type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'SUM(rb.payout)',
			'dt'        => 'amount_won',
			'field' => 'amount_won',
			'as' => 'amount_won',
			'formatter' => function( $d, $row ) {
				return "".number_format(abs($d))." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'ct.transaction_date',
			'dt'        => 'purchase_date',
			'field' => 'purchase_date',
			'as' => 'purchase_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i:s A", strtotime($d));
			}
		),
		array(
			'db'        => 'rt.voided_on',
			'dt'        => 'voided_on',
			'field' => 'voided_on',
			'as' => 'voided_on',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i:s A", strtotime($d));
			}
		),
		array(
			'db'        => '(SELECT SUM(`rb2`.`bet_amount`) FROM `rapidballs_bet` AS `rb2` WHERE `rt`.`id`=`rb2`.`ticket_id` AND `rb2`.`is_voided`="1")',
			'dt'        => 'amount_refunded',
			'field' => 'amount_refunded',
			'as' => 'amount_refunded',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'v.firstname',
			'dt'        => 'v_firstname',
			'field' => 'v_firstname',
			'as' => 'v_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'v.lastname',
			'dt'        => 'v_lastname',
			'field' => 'v_lastname',
			'as' => 'v_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rt.is_voided',
			'dt'        => 'is_voided',
			'field' => 'is_voided',
			'as' => 'is_voided',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rt.voided_by',
			'dt'        => 'voided_by_id',
			'field' => 'voided_by_id',
			'as' => 'voided_by_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rt.voided_by',
			'dt'        => 'voided_by',
			'field' => 'voided_by',
			'as' => 'voided_by',
			'formatter' => function( $d, $row ) {
				return $row['v_firstname']." ".$row['v_lastname']." (".$row['voided_by_id'].")";
			}
		),
		array(
			'db'        => 'rt.is_voided',
			'dt'        => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				return $d == 1 ? "<span style='color: red;'>Voided by ".$row['v_firstname']." ".$row['v_lastname']." (".$row['voided_by_id'].") <br>on ".date("m/d/Y", strtotime($row['voided_on']))." at ".date("h:i:s A", strtotime($row['voided_on']))."</span>" : "<span style='color: green;'>Active</span>";
			}
		),
		array(
			'db'        => 'rt.void_reason',
			'dt'        => 'void_reason',
			'field' => 'void_reason',
			'as' => 'void_reason',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	$join_query = "FROM `rapidballs_ticket` AS `rt` LEFT JOIN `rapidballs_bet` AS `rb` ON `rt`.`id`=`rb`.`ticket_id` LEFT JOIN `users` AS `u` ON `u`.`id`=`rt`.`user_id` LEFT JOIN `users` AS `v` ON `v`.`id`=`rt`.`voided_by` LEFT JOIN `rapidballs_bet_type` AS `rbt` ON `rbt`.`id`=`rt`.`bet_type_id` LEFT JOIN `customer_transaction` AS `ct` ON `ct`.`transaction_id`=`rt`.`transaction_id`";
	
	$group_by = "GROUP BY `rt`.`id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
