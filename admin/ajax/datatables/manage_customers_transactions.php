<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'customer_transaction';
	
	// Table's primary key
	$primary_key = 'transaction_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. 
	$columns = array(
		array(
			'db' => 't.transaction_id',
			'dt' => 'DT_RowId',
			'field' => 'transaction_id',
			'as' => 'transaction_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'tt.name',
			'dt' => 'transaction_type',
			'field' => 'transaction_type',
			'as' => 'transaction_type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 't.transaction_details',
			'dt' => 'transaction_details',
			'field' => 'transaction_details',
			'as' => 'transaction_details',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 't.amount',
			'dt' => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 't.transaction_date',
			'dt' => 'transaction_date',
			'field' => 'transaction_date',
			'as' => 'transaction_date',
			'formatter' => function( $d, $row ) {
				return date('m/d/Y h:i:s A' , strtotime($d));
			}
		),
		array(
			'db' => 't.user_id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 't.balance',
			'dt' => 'balance',
			'field' => 'balance',
			'as' => 'balance',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 't.initial_balance',
			'dt' => 'initial_balance',
			'field' => 'initial_balance',
			'as' => 'initial_balance',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'user_fullname',
			'field' => 'user_fullname',
			'as' => 'user_fullname',
			'formatter' => function( $d, $row ) {
				return $row['user_firstname']." ".$row['user_lastname'];
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'user_firstname',
			'field' => 'user_firstname',
			'as' => 'user_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'user_lastname',
			'field' => 'user_lastname',
			'as' => 'user_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
	);

	$join_query = "FROM `customer_transaction` AS `t` LEFT JOIN `users` AS `u` ON `t`.`user_id`=`u`.`id` LEFT JOIN `transaction_types` AS `tt` ON `t`.`transaction_type_id` = `tt`.`id`";
	
	$extra_where = "";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`transaction_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `transaction_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`transaction_date` <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND `transaction_date` <= '".$_GET['filter_date_to']."'";
		}
	}
	
	if(isset($_GET['user_id'])){
		if($extra_where == ""){
			$extra_where = "`user_id` = '".$_GET['user_id']."'";
		}else{
			$extra_where .= " AND `user_id` = '".$_GET['user_id']."'";
		}
	}
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );

	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
