<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user_transaction';

	// Table's primary key
	$primary_key = 'id';
	
	// get the island id
	$location_id = $_GET['location_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'put.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'put.transaction_date',
			'dt'        => 'transaction_date',
			'field' => 'transaction_date',
			'as' => 'transaction_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 'tt.name',
			'dt'        => 'type',
			'field' => 'type',
			'as' => 'type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'customer',
			'field' => 'customer',
			'as' => 'customer',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db'        => 'u2.firstname',
			'dt'        => 'cashier_firstname',
			'field' => 'cashier_firstname',
			'as' => 'cashier_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u2.lastname',
			'dt'        => 'cashier_lastname',
			'field' => 'cashier_lastname',
			'as' => 'cashier_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u2.id',
			'dt'        => 'cashier_id',
			'field' => 'cashier_id',
			'as' => 'cashier_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u2.firstname',
			'dt'        => 'cashier',
			'field' => 'cashier',
			'as' => 'cashier',
			'formatter' => function( $d, $row ) {
				return $row['cashier_firstname']." ".$row['cashier_lastname']." (".$row['cashier_id'].")";
			}
		),
		array(
			'db'        => 'put.amount',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		)
	);

	$join_query = "FROM `panel_user_transaction` AS `put` LEFT JOIN `transaction_types` AS `tt` ON `tt`.`id`=`put`.`type_id` LEFT JOIN `panel_location` AS `pl` ON `put`.`location_id`=`pl`.`id` LEFT JOIN `users` AS `u` ON `u`.`id`=`put`.`customer_id` LEFT JOIN `users` AS `u2` ON `u2`.`id`=`put`.`made_by` LEFT JOIN `lotto_ticket` AS `t` ON `t`.`ticket_number`=`put`.`ticket_number`";
	
	$date = new DateTime('now');
	$date_str = $date->format("Y-m-d");
	$extra_where = "put.location_id='".$location_id."' AND CAST(`put`.`transaction_date` AS DATE) = '".$date_str."' AND (put.type_id='9' OR put.type_id='10')";
	
	//$group_by = "GROUP BY `put`.`made_by`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);