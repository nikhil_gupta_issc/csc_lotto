<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_bet';
	
	// Table's primary key
	$primary_key = 'bet_id';
	
	// Ticket id
	$ticket_number = $_GET['ticket_number'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'b.bet_id',
			'dt' => 'DT_RowId',
			'field' => 'bet_id',
			'as' => 'bet_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'g.short_name',
			'dt'        => 'short_name',
			'field' => 'short_name',
			'as' => 'short_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.ball_string',
			'dt'        => 'ball_string',
			'field' => 'ball_string',
			'as' => 'ball_string',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.is_boxed',
			'dt'        => 'is_boxed',
			'field' => 'is_boxed',
			'as' => 'is_boxed',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "S" : "B";
			}
		),
		array(
			'db'        => 'b.bet_amount',
			'dt'        => 'bet_amount',
			'field' => 'bet_amount',
			'as' => 'bet_amount',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 'psd.payout_rate',
			'dt'        => 'payout_rate',
			'field' => 'payout_rate',
			'as' => 'payout_rate',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.pay_factor',
			'dt'        => 'pay_factor',
			'field' => 'pay_factor',
			'as' => 'pay_factor',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.is_winner',
			'dt'        => 'amount_won',
			'field' => 'amount_won',
			'as' => 'amount_won',
			'formatter' => function( $d, $row ) {
				return "".number_format($row['payout_rate']*$row['pay_factor']) . " ".CURRENCY_FORMAT;
			}
		)
	);

	$join_query = "FROM `lotto_bet` AS `b` LEFT JOIN `lotto_ticket` AS `t` ON `b`.`ticket_id`=`t`.`ticket_id` LEFT JOIN `lotto_game` AS `g` ON `b`.`game_id`=`g`.`id` LEFT JOIN `payout_scheme_detail` AS `psd` ON `psd`.`scheme_id`=`b`.`payout_scheme_id` AND `g`.`number_of_balls`=`psd`.`ball_count`";
	
	$extra_where = "b.is_winner=1 AND t.ticket_number = '".$ticket_number."'";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
