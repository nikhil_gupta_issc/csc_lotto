<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_location';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'loc.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'loc.id',
			'dt' => 'location_id',
			'field' => 'location_id',
			'as' => 'location_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.name',
			'dt' => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.type',
			'dt' => 'type',
			'field' => 'type',
			'as' => 'type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.address',
			'dt' => 'address1',
			'field' => 'address1',
			'as' => 'address1',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.address2',
			'dt' => 'address2',
			'field' => 'address2',
			'as' => 'address2',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.address',
			'dt' => 'address',
			'field' => 'address',
			'as' => 'address',
			'formatter' => function( $d, $row ) {
				return $row['address1']." ".$row['address2'];
			}
		),
		array(
			'db' => 'loc.island_id',
			'dt' => 'island_id',
			'field' => 'island_id',
			'as' => 'island_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'i.name',
			'dt' => 'island',
			'field' => 'island',
			'as' => 'island',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.country',
			'dt' => 'country',
			'field' => 'country',
			'as' => 'country',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.contact_person',
			'dt' => 'contact_person',
			'field' => 'contact_person',
			'as' => 'contact_person',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.contact_email',
			'dt' => 'contact_email',
			'field' => 'contact_email',
			'as' => 'contact_email',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.telephone',
			'dt' => 'telephone',
			'field' => 'telephone',
			'as' => 'telephone',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.payout_scheme_id',
			'dt' => 'pay_scheme_id',
			'field' => 'pay_scheme_id',
			'as' => 'pay_scheme_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'ps.name',
			'dt' => 'payout_scheme',
			'field' => 'payout_scheme',
			'as' => 'payout_scheme',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.receipt_message_id',
			'dt' => 'receipt_message_id',
			'field' => 'receipt_message_id',
			'as' => 'receipt_message_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'm.name',
			'dt' => 'receipt_message',
			'field' => 'receipt_message',
			'as' => 'receipt_message',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.start_date',
			'dt' => 'start_date',
			'field' => 'start_date',
			'as' => 'start_date',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.commission_rate',
			'dt' => 'commission_rate',
			'field' => 'commission_rate',
			'as' => 'commission_rate',
			'formatter' => function( $d, $row ) {
				return ($d*100).'%';
			}
		),
		array(
			'db' => 'loc.cashier_limit',
			'dt' => 'cashier_limit',
			'field' => 'cashier_limit',
			'as' => 'cashier_limit',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.location_limit',
			'dt' => 'location_limit',
			'field' => 'location_limit',
			'as' => 'location_limit',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.enforce_location_limit',
			'dt' => 'enforce_location_limit',
			'field' => 'enforce_location_limit',
			'as' => 'enforce_location_limit',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.ip_address',
			'dt' => 'ip_address',
			'field' => 'ip_address',
			'as' => 'ip_address',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'loc.location_link',
			'dt' => 'location_link',
			'field' => 'location_link',
			'as' => 'location_link',
			'formatter' => function( $d, $row ) {
				return $row['location_link'];
			}
		),
		array(
			'db' => 'loc.location_enabled',
			'dt' => 'location_enabled',
			'field' => 'location_enabled',
			'as' => 'location_enabled',
			'formatter' => function( $d, $row ) {
				return $row['location_enabled'];
			}
		)
	);

	$join_query = "FROM `panel_location` AS `loc` LEFT JOIN `island` AS `i` ON `loc`.`island_id` = `i`.`id` LEFT JOIN `payout_scheme` AS `ps` ON `loc`.`payout_scheme_id` = `ps`.`id` LEFT JOIN `receipt_messages` AS `m` ON `m`.`id` = `loc`.`receipt_message_id`";
	
	$extra_where = "`loc`.`id` > 0";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
