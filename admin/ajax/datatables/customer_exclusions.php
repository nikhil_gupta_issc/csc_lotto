<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'exclusion_log';
	
	$customer_id = $_GET['customer_id'];
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'el.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),array(
			'db' => 'el.id',
			'dt' => 'exclusion_id',
			'field' => 'exclusion_id',
			'as' => 'exclusion_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'customer',
			'field' => 'customer',
			'as' => 'customer',
			'formatter' => function( $d, $row ) {
				return $d.' '.$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'el.excluded_on',
			'dt' => 'excluded_on',
			'field' => 'excluded_on',
			'as' => 'excluded_on',
			'formatter' => function( $d, $row ) {
				$date = new DateTime($d);
				return $date->format("m/d/Y h:i A");
			}
		),
		array(
			'db' => 'el.exclusion_end',
			'dt' => 'exclusion_end',
			'field' => 'exclusion_end',
			'as' => 'exclusion_end',
			'formatter' => function( $d, $row ) {
				$date = new DateTime($d);
				return $date->format("m/d/Y h:i A");
			}
		),
		array(
			'db' => 'et.name',
			'dt' => 'exclusion_type',
			'field' => 'exclusion_type',
			'as' => 'exclusion_type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'el2.count',
			'dt' => 'exclusion_count',
			'field' => 'exclusion_count',
			'as' => 'exclusion_count',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	$join_query = "FROM `exclusion_log` AS `el` LEFT JOIN `users` AS `u` ON `el`.`user_id`=`u`.`id` LEFT JOIN `exclusion_types` AS `et` ON `el`.`exclusion_type_id`=`et`.`id` INNER JOIN (SELECT user_id, COUNT(*) as count FROM `exclusion_log` GROUP BY user_id) AS `el2` ON `el2`.`user_id`=`el`.`user_id`";
	
	if($customer_id > 0){
		$extra_where = "el.user_id = '".$customer_id."'";
	}else{
		$extra_where = "el.user_id != '-9999'";
	}
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		$extra_where .= " AND el.excluded_on >= '".$_GET['filter_date_from']."'";
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		$extra_where .= " AND el.excluded_on <= '".$_GET['filter_date_to']." 23:59:59'";
	}

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
