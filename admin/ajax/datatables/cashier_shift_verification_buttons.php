<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	if($action == NULL && isset($_GET['action'])){
		$action = $_GET['action'];
	}
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	function save_to_csv($data, $filename, $attachment = false){
        if($attachment) {
            // send response headers to the browser
            header( 'Content-Type: text/csv' );
            header( 'Content-Disposition: attachment;filename='.$filename);
            $fp = fopen('php://output', 'w');
        } else {
            $fp = fopen($filename, 'w');
        }

		foreach($data as $row){
            fputcsv($fp, $row);
        }
        
        fclose($fp);
    }
	
	switch ($action){
		case 'export':
			$now_dt = new DateTime();
			$now = $now_dt->format("Y-m-d H:i:s");
		
			$q = "SELECT `put`.`location_id`, `loc`.`name` AS `location_name`, `put`.`made_by`, `u`.`firstname`, `u`.`lastname`, `tt`.`name` as `trans_type`, COUNT(`put`.`amount`) AS `trans_qty`, SUM(`put`.`amount`) AS `total`, `pus`.`shift_start`, `pus`.`shift_end`, `pus`.`opening`, `pus`.`sales`, `pus`.`payouts`, `pus`.`closing`,`pus`.`expected`, `pus`.`over_short` FROM `panel_user_transaction` AS `put` JOIN `panel_location` as `loc` ON `put`.`location_id` = `loc`.`id` JOIN `transaction_types` AS `tt` ON `tt`.`id`=`put`.`type_id` JOIN `panel_user_shift` AS `pus` ON `pus`.`id`=`put`.`shift_id` JOIN `users` AS `u` ON `u`.`id`=`put`.`made_by` GROUP BY `put`.`shift_id`,`put`.`type_id`";
			$query_data = $db->query($q);
			$response['query'] = $q;
			
			$trans_date_dt = new DateTime($query_data[0]['shift_start']);
			$trans_date = $trans_date_dt->format('m/d/Y');
			
			// create quickbooks IIF import file
			$data = array();
			$data[] = array('!TRNS','TRNSTYPE','DATE','ACCNT','AMOUNT','DOCNUM','TOPRINT','NAME','REP','CLASS');
			$data[] = array('!SPL','TRNSTYPE','DATE','ACCNT','AMOUNT','DOCNUM','QNTY','INVITEM','CLASS');
			$data[] = array('!ENDTRNS');
			$data[] = array('TRNS','CASH SALE',$trans_date,'1350 - Deposits In Transit',$query_data[0]['expected'],'366553','N',$query_data[0]['location_name'],$query_data[0]['made_by'].'-'.$query_data[0]['firstname']." ".$query_data[0]['lastname'],$query_data[0]['location_name']);
			
			// pull out relevant data
			foreach($query_data as $trans){
				$trans_date_dt = new DateTime($query_data[0]['shift_start']);
				$trans_date = $trans_date_dt->format('m/d/Y');
				
				switch ($trans['trans_type']){
					case 'Panel Customer Deposit':
						$data[] = array('SPL','CASH SALE',$trans_date,'2311 - Customer Deposits',$trans['total'],'',$trans['trans_qty'],'PCD - Panel Customer Deposit',$trans['location_name']);
						break;
					case 'Panel Customer Withdrawal':
						$data[] = array('SPL','CASH SALE',$trans_date,'2312 - Customer Withdrawals',$trans['total'],'',$trans['trans_qty'],'PCW - PanelCustomerWithdrawal',$trans['location_name']);
						break;
					case 'Sell Lotto Ticket':
						$data[] = array('SPL','CASH SALE',$trans_date,'4111 - Panel Lotto Sales',$trans['total'],'',$trans['trans_qty'],'SLT - Sell Lotto Ticket',$trans['location_name']);
						break;
					case 'Void Lotto Ticket':
						$data[] = array('SPL','CASH SALE',$trans_date,'4111 - Panel Lotto Sales',$trans['total'],'',$trans['trans_qty'],'VLT - Void Lotto Ticket',$trans['location_name']);
						break;
					case 'Panel Pay Lotto Win':
						$data[] = array('SPL','CASH SALE',$trans_date,'4112 - Panel Lotto Payouts',$trans['total'],'',$trans['trans_qty'],'PPLW - Panel Pay Lotto Win',$trans['location_name']);
						break;
					case 'Make Panel Deposit':
						$data[] = array('SPL','CASH SALE',$trans_date,'1640 - Due to Panels',$trans['total'],'',$trans['trans_qty'],'MPD - Make Panel Deposit',$trans['location_name']);
						break;
					case 'Cashier Transfer In':
						$data[] = array('SPL','CASH SALE',$trans_date,'1500 - Location Accounts:1501 · Location Account Transfers',$trans['total'],'',$trans['trans_qty'],'CTI - Cashier Transfer In',$trans['location_name']);
						break;
					default:
						// unknown
				}
			}

			$data[] = array('ENDTRNS');
			
			save_to_csv($data, 'shift_export.iif', true);
			
			die();
			
			break;
		case 'exact_verify':
			$now = new DateTime();
			$now_str = $now->format("Y-m-d H:i:s");
			
			$q = "UPDATE panel_user_shift SET is_exact=1,is_verified=1,verified_by=%i,verified_on=%s WHERE id=%i";
			$db->queryDirect($q, array($session->userinfo['id'], $now_str, $id));
			
			$response['success'] = "true";
			break;
		case 'nonexact_verify':
			$now = new DateTime();
			$now_str = $now->format("Y-m-d H:i:s");
			
			$q = "UPDATE panel_user_shift SET is_exact=0,is_verified=1,verified_by=%i,verified_on=%s WHERE id=%i";
			$db->queryDirect($q, array($session->userinfo['id'], $now_str, $id));
			
			$response['success'] = "true";
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);