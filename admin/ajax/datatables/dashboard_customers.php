<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'customer_transaction';

	// Table's primary key
	$primary_key = 'transaction_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'ct.transaction_id',
			'dt' => 'DT_RowId',
			'field' => 'transaction_id',
			'as' => 'transaction_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'i.id',
			'dt'        => 'island_id',
			'field' => 'island_id',
			'as' => 'island_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'i.name',
			'dt'        => 'island',
			'field' => 'island',
			'as' => 'island',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'customer',
			'field' => 'customer',
			'as' => 'customer',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN ct.amount < 0 THEN ct.amount END)',
			'dt'        => 'sales',
			'field' => 'sales',
			'as' => 'sales',
			'formatter' => function( $d, $row ) {
				return "".number_format(abs($d)) . " ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN ct.amount > 0 THEN ct.amount END)',
			'dt'        => 'winnings',
			'field' => 'winnings',
			'as' => 'winnings',
			'formatter' => function( $d, $row ) {
				return "".number_format($d) . " ".CURRENCY_FORMAT."";
			}
		),
		array(
			'db'        => 'SUM(ct.amount)',
			'dt'        => 'net',
			'field' => 'net',
			'as' => 'net',
			'formatter' => function( $d, $row ) {
				return "<a href='/admin/customer_reporting/customer_activity_detail/index.php?customer=".$row['user_id']."'>".number_format(abs($d))." ".CURRENCY_FORMAT."</a>";
				return "".number_format(abs($d)) . " ".CURRENCY_FORMAT."";
			}
		)
	);

	$join_query = "FROM `customer_transaction` AS `ct` JOIN `users` AS `u` ON `ct`.`user_id`=`u`.`id` JOIN `customers` AS `c` ON `c`.`user_id`=`u`.`id` JOIN `user_info` AS `ui` ON `ui`.`user_id`=`u`.`id` JOIN `island` AS `i` ON `i`.`id`=`ui`.`island_id`";
	
	$date = new DateTime('now');
	$date_str = $date->format("Y-m-d");
	$extra_where = "CAST(`ct`.`transaction_date` AS DATE) = '".$date_str."' AND ct.transaction_type_id<>5 AND ct.transaction_type_id<>6";
	
	$group_by = "GROUP BY `ct`.`user_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
