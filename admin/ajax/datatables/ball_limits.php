<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_bet_limits';
	
	// Table's primary key
	$primary_key = 'id';

	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'bet.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'bet.ball_count',
			'dt'        => 'ball_count',
			'field' 	=> 'ball_count',
			'as' 		=> 'ball_count',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'bet.limit',
			'dt'        => 'bet_limit',
			'field' 	=> 'bet_limit',
			'as' 		=> 'bet_limit',
			'formatter' => function( $d, $row ) {
				return ''.number_format($d).' ' . CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' 	=> 'user_id',
			'as'		=> 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' 	=> 'firstname',
			'as'		=> 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' 	=> 'lastname',
			'as'		=> 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'bet.added_by',
			'dt'        => 'added_by',
			'field' 	=> 'added_by',
			'as'		=> 'added_by',
			'formatter' => function( $d, $row ) {
				return $row['firstname'].' '.$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db'        => 'bet.added_on',
			'dt'        => 'added_on',
			'field' 	=> 'added_on',
			'as' 		=> 'added_on',
			'formatter' => function( $d, $row ) {
				return date( 'Y-m-d h:i A', strtotime($d));
			}
		)
	);

	$join_query = "FROM `lotto_bet_limits` as bet LEFT JOIN users as u ON `bet`.`added_by` = `u`.`id`";
	
	//$extra_where = "";
	
	//$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
