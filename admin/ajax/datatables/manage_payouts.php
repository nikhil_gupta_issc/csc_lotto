<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'winner_payout';
	
	$location_id == isset($_GET["location_id"]) ? $_GET["location_id"] : "";

	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'wp.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
	/*	array(
			'db'        => 'wps.payout_status',
			'dt'        => 'payout_status',
			'field' => 'payout_status',
			'as' => 'payout_status',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		), */
		array(
			'db'        => 'w.ticket_number',
			'dt'        => 'ticket_number',
			'field' => 'ticket_number',
			'as' => 'ticket_number',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		
		array(
			'db'        => 'w.win_date',
			'dt'        => 'win_date',
			'field' => 'win_date',
			'as' => 'win_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y", strtotime($d));
			}
		),
		
		
		array(
			'db'        => 'u.firstname',
			'dt'        => 'cashier_firstname',
			'field' => 'cashier_firstname',
			'as' => 'cashier_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'cashier_lastname',
			'field' => 'cashier_lastname',
			'as' => 'cashier_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'cashier',
			'field' => 'cashier',
			'as' => 'cashier',
			'formatter' => function( $d, $row ) {
				return $d == "" ? "Online" : $row['cashier_firstname']." ".$row['cashier_lastname'];
			}
		),
		
		/*array(
			'db'        => 't.ticket_number',
			'dt'        => 'ticket_number',
			'field' => 'ticket_number',
			'as' => 'ticket_number',
			'formatter' => function( $d, $row ) {
				return "<a href='/admin/tasks/view_void_ticket/index.php?ticket_number=".$d."'>".$d."</a>";
			}
		),
		array(
			'db'        => 't.ticket_number',
			'dt'        => 'ticket_number_raw',
			'field' => 'ticket_number_raw',
			'as' => 'ticket_number_raw',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		), */
		array(
			'db'        => 'wp.total_payout',
			'dt'        => 'total_payout',
			'field' => 'total_payout',
			'as' => 'total_payout',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		)
	);

	//$join_query = "FROM `winner_payout` AS `wp` JOIN `winners` AS `w` ON `wp`.`ticket_number`=`w`.`ticket_number` JOIN `lotto_ticket` AS `t` ON `t`.`ticket_number`=`wp`.`ticket_number` JOIN `winner_payout_status` AS `wps` ON `wps`.`payout_status_id`=`wp`.`status_id` LEFT JOIN `users` AS `u` ON `t`.`cashier_id`=`u`.`id`";
	$join_query="FROM `winner_payout` AS `wp`  JOIN  `winners` AS `w` ON `wp`.`ticket_number` = `w`.`ticket_number`  JOIN  `lotto_ticket` as `lt` ON lt.ticket_number = w.ticket_number JOIN  `users` as `u` ON `lt`.`cashier_id` = `u`.`id`";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`w`.`win_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `w`.`win_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`w`.`win_date` <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND `w`.`win_date` <= '".$_GET['filter_date_to']."'";
		}
	}
	
	$group_by = "GROUP BY `w`.`ticket_number`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
