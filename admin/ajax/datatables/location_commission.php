<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user';
	
	// Table's primary key
	$primary_key = 'id';

	//Get the cashier and to - from filter dates
	$location_id = $_GET['location_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'put.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'SUM(t.commission_amount)',
			'dt'        => 'commission',
			'field' => 'commission',
			'as' => 'commission',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'l.name',
			'dt'        => 'location',
			'field' => 'location',
			'as' => 'location',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'u_id',
			'field' => 'u_id',
			'as' => 'u_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'cashier',
			'field' => 'cashier',
			'as' => 'cashier',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$row['u_id'].")";
			}
		),
		array(
			'db'        => 'CAST(put.transaction_date AS DATE)',
			'dt'        => 'trans_date',
			'field' => 'trans_date',
			'as' => 'trans_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y",strtotime($d));
			}
		),
		array(
			'db'        => 'SUM(t.total)',
			'dt'        => 'sales',
			'field' => 'sales',
			'as' => 'sales',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		)
	);

	$join_query = "FROM `panel_user` AS `pu` JOIN `panel_user_transaction` AS `put` ON `pu`.`user_id`=`put`.`made_by` JOIN `lotto_ticket` AS `t` ON `put`.`ticket_number`=`t`.`ticket_number` JOIN `panel_location` AS `l` ON `t`.`location_id`=`l`.`id` JOIN  `users` AS `u` ON `t`.`cashier_id`=`u`.`id`";
	
	$extra_where = "`put`.`type_id`='7'";
	if($location_id != ""){
		$extra_where .= " AND t.location_id = '".$location_id."'";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`transaction_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `transaction_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`transaction_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `transaction_date` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	$group_by = "GROUP BY `put`.`made_by`, CAST(`put`.`transaction_date` AS DATE)";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
