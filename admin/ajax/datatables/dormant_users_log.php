<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'dormant_log';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'dl.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'u.username',
			'dt'        => 'username',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$d.")";
			}
		),
		array(
			'db'        => 'dl.ip_address',
			'dt'        => 'ip',
			'field' => 'ip',
			'as' => 'ip',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'dl.last_login',
			'dt'        => 'latest_activity',
			'field' => 'latest_activity',
			'as' => 'latest_activity',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format("m/d/Y h:i A");
			}
		),
		array(
			'db'        => 'dl.last_login',
			'dt'        => 'inactive',
			'field' => 'inactive',
			'as' => 'inactive',
			'formatter' => function( $d, $row ) {
				global $core;
				if($row['dormant_end'] == null){
					$now = new DateTime();
				}else{
					$now = new DateTime($row['dormant_end']);
				}
				$dt = new DateTime($d);
				$interval = $now->diff($dt);
				return $core->format_interval($interval, true);
			}
		),
		array(
			'db'        => 'dl.dormant_end',
			'dt'        => 'dormant_end',
			'field' => 'dormant_end',
			'as' => 'dormant_end',
			'formatter' => function( $d, $row ) {
				if($d == null){
					return "Still Dormant";
				}
				$dt = new DateTime($d);
				return $dt->format("m/d/Y h:i A");
			}
		),
		array(
			'db'        => 'dl.balance',
			'dt'        => 'account_balance',
			'field' => 'account_balance',
			'as' => 'account_balance',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ". CURRENCY_FORMAT;
			}
		)
	);

	$join_query = "FROM `dormant_log` AS `dl` LEFT JOIN `users` AS `u` ON `u`.`id`=`dl`.`user_id`";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "dl.last_login >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND dl.last_login >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "dl.last_login <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND dl.last_login <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "GROUP BY ";
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
