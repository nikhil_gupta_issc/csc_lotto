<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'panel_user_shift';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 's.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'cashier_name',
			'field' => 'cashier_name',
			'as' => 'cashier_name',
			'formatter' => function( $d, $row ) {
				return $row['firstname']." ".$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db'        => 's.id',
			'dt'        => 'shift_id',
			'field' => 'shift_id',
			'as' => 'shift_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.id',
			'dt'        => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.firstname',
			'dt'        => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'u.lastname',
			'dt'        => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 's.shift_start',
			'dt'        => 'shift_start',
			'field' => 'shift_start',
			'as' => 'shift_start',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 's.shift_end',
			'dt'        => 'shift_end',
			'field' => 'shift_end',
			'as' => 'shift_end',
			'formatter' => function( $d, $row ) {
				if($d=="0000-00-00 00:00:00"){
					return "Active";
				}else{
					return date("m/d/Y h:i A", strtotime($d));
				}
			}
		),
		array(
			'db'        => 's.float_amount',
			'dt'        => 'float_amount',
			'field' => 'float_amount',
			'as' => 'float_amount',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.opening',
			'dt'        => 'opening',
			'field' => 'opening',
			'as' => 'opening',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.sales',
			'dt'        => 'sales',
			'field' => 'sales',
			'as' => 'sales',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.payouts',
			'dt'        => 'payouts',
			'field' => 'payouts',
			'as' => 'payouts',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.closing',
			'dt'        => 'closing',
			'field' => 'closing',
			'as' => 'closing',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.expected',
			'dt'        => 'expected',
			'field' => 'expected',
			'as' => 'expected',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 's.over_short',
			'dt'        => 'over_short',
			'field' => 'over_short',
			'as' => 'over_short',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		)
	);
	
	$join_query = "FROM `panel_user_shift` AS `s` LEFT JOIN `users` AS `u` ON `s`.`user_id`=`u`.`id`";
	
	$extra_where = "s.location_id = '".$_GET['location_id']."'";
	
	if($_GET['franchise'] == "franchise"){
		$extra_where .= " AND pl.commission_rate <> 0";
	}elseif($_GET['franchise'] == "non_franchise"){
		$extra_where .= " AND pl.commission_rate = 0";
	}
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`shift_start` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `shift_start` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`shift_start` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `shift_start` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);