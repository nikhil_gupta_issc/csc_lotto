<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'customer_transaction';
	
	// Table's primary key
	$primary_key = 'transaction_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'ct.transaction_id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'SUBSTR(ct.transaction_details, INSTR(ct.transaction_details,"-") + 2, LENGTH(ct.transaction_details))',
			'dt'        => 'transaction_details',
			'field' => 'transaction_details',
			'as' => 'transaction_details',
			'formatter' => function( $d, $row ) {
				return $d." (".$row['game_id'].")";
			}
		),
		array(
			'db'        => 'ct.transaction_record_id',
			'dt'        => 'game_id',
			'field' => 'game_id',
			'as' => 'game_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ct.transaction_record_id',
			'dt'        => 'rtp',
			'field' => 'rtp',
			'as' => 'rtp',
			'formatter' => function( $d, $row ) {
				if($row['bets'] != 0){
					return number_format(((-$row['wins'] / $row['bets']) * 100),2)."%";
				}else{
					return "0.00%";
				}
			}
		),
		array(
			'db'        => 'ct.transaction_record_id',
			'dt'        => 'rtp_lifetime',
			'field' => 'rtp_lifetime',
			'as' => 'rtp_lifetime',
			'formatter' => function( $d, $row ) {
				if($row['bets_lifetime'] != 0){
					return number_format(((-$row['wins_lifetime'] / $row['bets_lifetime']) * 100), 2)."%";
				}else{
					return "0.00%";
				}
			}
		),
		array(
			'db'        => 'ct.transaction_record_id',
			'dt'        => 'rtp_theoretical',
			'field' => 'rtp_theoretical',
			'as' => 'rtp_theoretical',
			'formatter' => function( $d, $row ) {
				return "97.00%";
			}
		),
		array(
			'db'        => '(SELECT SUM(`ct2`.`amount`) FROM `customer_transaction` AS `ct2` WHERE `ct2`.`transaction_type_id`=2 AND `ct2`.`transaction_record_id` = `ct`.`transaction_record_id`)',
			'dt'        => 'wins_lifetime',
			'field' => 'wins_lifetime',
			'as' => 'wins_lifetime',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => '(SELECT SUM(`ct2`.`amount`) FROM `customer_transaction` AS `ct2` WHERE `ct2`.`transaction_type_id`=1 AND `ct2`.`transaction_record_id` = `ct`.`transaction_record_id`)',
			'dt'        => 'bets_lifetime',
			'field' => 'bets_lifetime',
			'as' => 'bets_lifetime',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN ct.transaction_type_id=1 THEN `amount` * -1 END)',
			'dt'        => 'bets',
			'field' => 'bets',
			'as' => 'bets',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." " . CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'SUM(CASE WHEN ct.transaction_type_id=2 THEN `amount` * -1 END)',
			'dt'        => 'wins',
			'field' => 'wins',
			'as' => 'wins',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ". CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'SUM(ct.amount * -1)',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." " . CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'COUNT(CASE WHEN ct.transaction_type_id=1 THEN `amount` * -1 END)',
			'dt'        => 'play_count',
			'field' => 'play_count',
			'as' => 'play_count',
			'formatter' => function( $d, $row ) {
				return number_format($d);
			}
		),
		array(
			'db'        => 'COUNT(CASE WHEN ct.transaction_type_id=2 THEN `amount` * -1 END)',
			'dt'        => 'win_count',
			'field' => 'win_count',
			'as' => 'win_count',
			'formatter' => function( $d, $row ) {
				return number_format($d);
			}
		),
		array(
			'db'        => 'ct.vendor_id',
			'dt'        => 'vendor_id',
			'field' => 'vendor_id',
			'as' => 'vendor_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	$join_query = "FROM `customer_transaction` AS `ct`";
	
	$extra_where = "(`ct`.`transaction_type_id`=1 OR `ct`.`transaction_type_id`=2)";
	
	if($_GET['game'] != ""){
		$extra_where .= " AND SUBSTR(`transaction_details`, INSTR(`transaction_details`,'-') + 2, LENGTH(`transaction_details`)) = '".$_GET['game']."'";
	}
	
	if($_GET['vendor_id'] != ""){
		$extra_where .= " AND ct.vendor_id = '".$_GET['vendor_id']."'";
	}

	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "ct.transaction_date >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND ct.transaction_date >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "ct.transaction_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND ct.transaction_date <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}

	$group_by = "GROUP BY SUBSTR(`transaction_details`, INSTR(`transaction_details`,'-') + 2, LENGTH(`transaction_details`))";
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
