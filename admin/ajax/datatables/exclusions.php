<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'users';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'e.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $d.' '.$row['midname'].' '.$row['lastname']." (".$row['user_id'].")";
			}
		),
		array(
			'db' => 'u.midname',
			'dt' => 'midname',
			'field' => 'midname',
			'as' => 'midname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.username',
			'dt' => 'username',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.email',
			'dt' => 'email',
			'field' => 'email',
			'as' => 'email',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'e.excluded_on',
			'dt' => 'start',
			'field' => 'start',
			'as' => 'start',
			'formatter' => function( $d, $row ) {
				if($d){
					$excluded_on = new DateTime($d);
					return $excluded_on->format('m/d/Y h:i:s A');
				}else{
					return "Never";
				}
			}
		),
		array(
			'db' => 'e.exclusion_end',
			'dt' => 'end',
			'field' => 'end',
			'as' => 'end',
			'formatter' => function( $d, $row ) {
				if($d){
					$exclusion_end = new DateTime($d);
					return $exclusion_end->format('m/d/Y h:i:s A');
				}else{
					return "Never";
				}
			}
		),
		array(
			'db' => 'et.id',
			'dt' => 'reason_id',
			'field' => 'reason_id',
			'as' => 'reason_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'et.name',
			'dt' => 'reason',
			'field' => 'reason',
			'as' => 'reason',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'et.type',
			'dt' => 'type',
			'field' => 'type',
			'as' => 'type',
			'formatter' => function( $d, $row ) {
				return ucwords($d);
			}
		),
		array(
			'db' => '(SELECT COUNT(*) FROM `exclusion_log` AS `e2` WHERE `e2`.`user_id` = `e`.`user_id` AND `e2`.`id` <= `e`.`id` GROUP BY `e2`.`user_id`)',
			'dt' => 'total',
			'field' => 'total',
			'as' => 'total',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	$join_query = "FROM `exclusion_log` AS `e` LEFT JOIN `users` AS `u` ON `u`.`id` = `e`.`user_id` LEFT JOIN `exclusion_types` AS `et` ON `et`.`id` = `e`.`exclusion_type_id`";
	
	//$extra_where = "`u`.`deleted` = 1";
	
	$group_by = "GROUP BY `e`.`id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
