<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'log_login_attempts';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'lla.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'lla.message',
			'dt'        => 'message',
			'field' => 'message',
			'as' => 'message',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'lla.user_agent',
			'dt'        => 'device',
			'field' => 'device',
			'as' => 'device',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'lla.ip_address',
			'dt'        => 'ip_address',
			'field' => 'ip_address',
			'as' => 'ip_address',
			'formatter' => function( $d, $row ) {
				return '<a target="_blank" href="http://ipinfo.io/'.$d.'">'.$d.'</a>';
			}
		),
		array(
			'db'        => 'lla.country',
			'dt'        => 'country',
			'field' => 'country',
			'as' => 'country',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'lla.is_successful',
			'dt'        => 'is_successful',
			'field' => 'is_successful',
			'as' => 'is_successful',
			'formatter' => function( $d, $row ) {
				return $d == 0 ? "<span style='color: red;'>Failed</span>" : "Success";
			}
		),
		array(
			'db'        => 'lla.username',
			'dt'        => 'username',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'lla.date_attempted',
			'dt'        => 'date_attempted',
			'field' => 'date_attempted',
			'as' => 'date_attempted',
			'formatter' => function( $d, $row ) {
				return date( 'm/d/Y h:i A', strtotime($d));
			}
		),
	);


	$join_query = "FROM `log_login_attempts` AS `lla`";

	//$extra_where = "";
	
	if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`date_attempted` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `date_attempted` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`date_attempted` <= '".$_GET['filter_date_to']." 23:59:59'";
		}else{
			$extra_where .= " AND `date_attempted` <= '".$_GET['filter_date_to']." 23:59:59'";
		}
	}
	
	//$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);