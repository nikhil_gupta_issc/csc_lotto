<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'users';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'u.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'u.id',
			'dt' => 'user_id',
			'field' => 'user_id',
			'as' => 'user_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'name',
			'field' => 'name',
			'as' => 'name',
			'formatter' => function( $d, $row ) {
				return $d.' '.$row['midname'].' '.$row['lastname'];
			}
		),
		array(
			'db' => 'u.firstname',
			'dt' => 'firstname',
			'field' => 'firstname',
			'as' => 'firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.lastname',
			'dt' => 'lastname',
			'field' => 'lastname',
			'as' => 'lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.midname',
			'dt' => 'midname',
			'field' => 'midname',
			'as' => 'midname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.username',
			'dt' => 'username',
			'field' => 'username',
			'as' => 'username',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.email',
			'dt' => 'email',
			'field' => 'email',
			'as' => 'email',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'u.last_login',
			'dt' => 'last_login',
			'field' => 'last_login',
			'as' => 'last_login',
			'formatter' => function( $d, $row ) {
				if($d){
					$last_login = new DateTime($d);
					return $last_login->format('m/d/Y h:i:s A');
				}else{
					return "Never";
				}
			}
		),		
		array(
			'db' => 'd.id',
			'dt' => 'deleted_by_id',
			'field' => 'deleted_by_id',
			'as' => 'deleted_by_id',
			'formatter' => function( $d, $row ) {
				return $d == 1 ? "<span style='color:red;'>Yes</span>" : "No";
			}
		),	
		array(
			'db' => 'd.firstname',
			'dt' => 'd_firstname',
			'field' => 'd_firstname',
			'as' => 'd_firstname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'd.lastname',
			'dt' => 'd_lastname',
			'field' => 'd_lastname',
			'as' => 'd_lastname',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'd.id',
			'dt' => 'deleted_by',
			'field' => 'deleted_by',
			'as' => 'deleted_by',
			'formatter' => function( $d, $row ) {
				return $row['d_firstname']." ".$row['d_lastname']." (".$row['deleted_by_id'].")";
			}
		),
		array(
			'db' => 'b.timestamp',
			'dt' => 'banned_on',
			'field' => 'banned_on',
			'as' => 'banned_on',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format('m/d/Y h:i A');
			}
		)
	);

	$join_query = "FROM `users` AS `u` LEFT JOIN `users` AS `d` ON `u`.`deleted_by` = `d`.`id` LEFT JOIN `banned_users` AS `b` ON `b`.`username`";
	
	$extra_where = "`u`.`deleted` = 1";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
