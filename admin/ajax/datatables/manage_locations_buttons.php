<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	
	$table = "panel_location";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	
	switch ($action){
		case 'enable':
			$q = "UPDATE `$table` SET `is_disabled` = 0 WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			break;
		case 'disable':
			$q = "UPDATE `$table` SET `is_disabled` = 1 WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			break;
		case 'delete':
			$q = "DELETE FROM `$table` WHERE id=".$id;
			$response['result'] = $db->queryDirect($q);
			break;
		case 'update':
			$key_value_pairs = array();
			foreach($_POST as $key => $value){
				// see if key is a valid column name in db
				$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
				$key_exists = $db->queryOneRow($q);
				if($key_exists['count'] > 0){
					$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
					$num_digits = $key_exists['NUMERIC_PRECISION'];
					$num_places = $key_exists['NUMERIC_SCALE'];
				
					// validate data against data type defined in database
					$data_type = $key_exists['DATA_TYPE'];
					switch ($data_type) {
						case "tinyint":
							settype($value , "integer");
							break;
						case "mediumint":
							settype($value , "integer");
							break;
						case "smallint":
							settype($value , "integer");
							break;
						case "int":
							settype($value , "integer");
							break;
						case "decimal":
							settype($value , "float");
							break;
						case "float":
							settype($value , "float");
							break;
						case "double":
							settype($value , "float");
							break;
						case "real":
							settype($value , "float");
							break;
						case "bit":
							settype($value , "boolean");
							break;
						case "boolean":
							settype($value , "boolean");
							break;
						case "date":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d', strtotime($value));
							}
							break;
						case "datetime":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('Y-m-d H:i:s', strtotime($value));
							}					
							break;
						case "time":
							if(!$value){
								$value = "NULL";
							}else{
								$value = date('H:i:s', strtotime($value));
							}
							break;
						case "timestamp":
							settype($value , "integer");
							break;
						case "year":
							if(!$value){
								$value = "NULL";
							}
							break;				
					}
					
					if($value == "NULL"){
						$key_value_pairs[$i] = " `".$key."` = ".$value;
					}else{
						$key_value_pairs[$i] = " `".$key."` = '".$value."'";
					}
					$i++;
				}
			}
			$value_str = implode(",", $key_value_pairs);
			$q  = "UPDATE $table SET $value_str WHERE id=$id";
			$response['result'] = $db->queryDirect($q);
			break;
		case 'insert':
			$now_dt = new DateTime();
			$now = $now_dt->format("Y-m-d H:i:s");
		
			$q = "INSERT INTO `panel_location` (`id`, `name`, `gl_account_name`, `gl_deposit_to`, `contact_person`, `contact_email`, `telephone`, `address`, `address2`, `island_id`, `country`, `start_date`, `commission_rate`, `payout_scheme_id`, `receipt_message_id`, `cashier_limit`, `location_limit`, `is_3rd_party`, `enforce_location_limit`, `is_deleted`, `is_system`, `created_on`, `created_by`, `ip_address`,`location_link`,`location_enabled`) VALUES (NULL, '".$_POST['name']."', NULL, NULL, '".$_POST['contact_person']."', '".$_POST['contact_email']."', '".$_POST['telephone']."', '".$_POST['address']."', '".$_POST['address2']."', '".$_POST['island_id']."', '".$_POST['country']."', '".$_POST['start_date']."', '".$_POST['commission_rate']."', '".$_POST['payout_scheme_id']."', '".$_POST['receipt_message_id']."', NULL, NULL, '0', '0', '0', '0', '".$now."', '".$session->userinfo['id']."','".$_POST['ip_address']."','".$_POST['location_link']."','".$_POST['location_enabled']."');";
			$response['result'] = $db->queryInsert($q);
			$response['query'] = $q;
			break;
		default:
			$response['result'] = false;
	}
	
	echo json_encode($response);
