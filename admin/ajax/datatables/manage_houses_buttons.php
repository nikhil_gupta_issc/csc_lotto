<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$table = "lotto_house";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$id = isset($_POST['id']) ? $_POST['id'] : NULL;
	$disable_reason = isset($_POST['reason']) ? $_POST['reason'] : NULL;
	$session_id=$session->userinfo['id'];

	switch ($action){
		case 'enable':
			$q = "UPDATE `$table` SET `is_disabled` = 0 WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			break;
		case 'disable':
		 $q = "UPDATE `$table` SET `is_disabled` = 1,house_disable_reason='".$disable_reason."',house_disable_userid='".$session_id."'  WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			break;
		case 'delete':
			$q = "DELETE FROM `$table` WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				die();
			}
			break;
		case 'update':
			$failed = false;
			
			$q = "UPDATE `$table` SET `short_name` = '".$_POST['short_name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `$table` SET `name` = '".$_POST['full_name']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			
			$q = "UPDATE `$table` SET `state_code` = '".$_POST['statecode']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			
			/*  
			$q = "UPDATE `$table` SET `panel_cutoff_time` = '".$_POST['panel_closing']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			$q = "UPDATE `$table` SET `hh_cutoff_time` = '".$_POST['hh_closing']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			*/
			$q = "UPDATE `$table` SET `web_cutoff_time` = '".$_POST['web_closing']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			
			$q = "UPDATE `$table` SET `drawing_time` = '".$_POST['drawing_time']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			
			$q = "UPDATE `$table` SET `plays_on_sunday` = '".$_POST['plays_on_sunday']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			
		 	$q = "UPDATE `$table` SET `is_early_house` = '".$_POST['early_house']."' WHERE id=".$id;
			$result = $db->queryDirect($q);
			if($result == false){
				echo $q;
				$failed = true;
			}
			
			if($failed == true){
				die();
			}
			break;
		case 'insert':
			//$q = "INSERT INTO `$table` (`id`, `short_name`, `name`, `state_code`, `panel_cutoff_time`, `hh_cutoff_time`, `web_cutoff_time`, `drawing_time`) VALUES (NULL, '".$_POST['short_name']."', '".$_POST['full_name']."', NULL, '".$_POST['panel_closing']."', '".$_POST['hh_closing']."', '".$_POST['web_closing']."', '".$_POST['drawing_time']."')";
			 $q = "INSERT INTO `$table` (`id`, `short_name`, `name`, `state_code`, `web_cutoff_time`, `drawing_time`,`is_early_house`,`plays_on_sunday`) VALUES (NULL, '".$_POST['short_name']."', '".$_POST['full_name']."', '".$_POST['statecode']."', '".$_POST['web_closing']."', '".$_POST['drawing_time']."','".$_POST['early_house']."','".$_POST['plays_on_sunday']."')";
			$result = $db->queryInsert($q);
			if($result <= 0){
				echo $q;
				die();
			}
			break;
		default:
		   die("Invalid Action");
	}
	
	echo json_encode($result);
