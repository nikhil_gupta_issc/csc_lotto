<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$q = "SELECT * FROM `cron_daily_totals` ORDER BY `total_date` DESC LIMIT 16";
	$daily_totals = $db->query($q);
	
	foreach($daily_totals as $return_info){
		$date = new DateTime($return_info['total_date']);
		$categories[] = $date->format("m/d/Y");
		$lotto_net[] = $return_info['lotto_bets'] + $return_info['lotto_wins'];
		$casino_net[] = $return_info['casino_bets'] + $return_info['casino_wins'];
		$sports_net[] = $return_info['sports_bets'] + $return_info['sports_wins'];
		$rapidballs_net[] = $return_info['rapidballs_bets'] + $return_info['rapidballs_wins'];
	}

	echo json_encode(array("categories" => $categories, "lotto" => $lotto_net, "casino" => $casino_net, "sports" => $sports_net, "rapidballs" => $rapidballs_net));