<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	if(isset($_POST['user_id'])){
		$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : -1;
		
		if($user_id != -1){
			$checksum = isset($_POST['checksum']) ? $_POST['checksum'] : -1;
			$correct = crc32(md5($user_id.date("HhHY")));
			
			if($correct == $checksum){
				// store upload user id in session so user's uploads can be seen
				$_SESSION['upload_id'] = $user_id;
				//echo 'correct';
			}else{
				// clear session upload user id
				unset($_SESSION['upload_id']);
			}
		}
	}
	
	if(isset($_POST['handler'])){
		if($_POST['handler'] == 'verification'){
			include($_SERVER['DOCUMENT_ROOT'].'/upload_files.php');
		}else{
			include($_SERVER['DOCUMENT_ROOT'].'/admin/upload_user_photos.php');
		}
	}