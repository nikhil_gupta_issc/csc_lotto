<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/core_config.php');

	include_once($_SERVER['DOCUMENT_ROOT'].'/lib/framework/twilio/twilio.php');

	$response = $sms->send_text_message($_POST['cellphone'], $_POST['message']);
	
	echo json_encode($response);