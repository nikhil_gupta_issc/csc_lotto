<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	$game_id = $_POST['game_id'];
	$today_dt = new DateTime();
	$today = $today_dt->format("Y-m-d");
	
	$game_id = $_POST['game_id'];
	
	$picks = array();
	$wagers = array();
	$potential_payouts = array();
	$risks = array();
	$tot_collected = 0;
	
	// get all bets placed for this game

	//$q = "SELECT *, SUM(bet_amount) AS bet_total, SUM(pay_factor) as pf_total FROM `lotto_bet` AS `b` JOIN `lotto_game` g ON b.game_id=g.id LEFT JOIN `lotto_ticket` AS `t` ON `t`.`ticket_id` = `b`.`ticket_id` WHERE g.id='".$game_id."' AND `draw_date` >= '".$today."' AND `t`.`is_voided` = '0' AND `b`.`is_boxed` = '0'  GROUP BY `b`.`ball_string`";
	$q = "SELECT *, SUM(bet_amount) AS bet_total, SUM(pay_factor) as pf_total FROM `lotto_bet` AS `b` JOIN `lotto_game` g ON b.game_id=g.id LEFT JOIN `lotto_ticket` AS `t` ON `t`.`ticket_id` = `b`.`ticket_id` WHERE g.id=%i AND `draw_date` >= %s AND `t`.`is_voided` = '0' AND `b`.`is_boxed` = '0'  GROUP BY `b`.`ball_string`";
	//$straight_bets = $db->query($q);
	$straight_bets = $db->query($q, array($game_id, $today));
	
	if(count($straight_bets) > 0){
		foreach($straight_bets as $bet){
			$ball_count = strlen($bet['ball_string']);
			
			// get payout rate for this game based on payout scheme

			//$q = "SELECT * FROM `payout_scheme_detail` WHERE `scheme_id` = '".$bet['payout_scheme_id']."' AND ball_count = '".$ball_count."'";
			$q = "SELECT * FROM `payout_scheme_detail` WHERE `scheme_id` = %i AND ball_count = %i";
			//$payout_scheme = $db->queryOneRow($q);
			$payout_scheme = $db->queryOneRow($q, array($bet['payout_scheme_id'], $ball_count));

			$payout_rate = $payout_scheme['payout_rate'];
			
			$tot_collected += $bet['bet_total'];
			$picks[$bet['ball_string']] = $bet['ball_string'];
			$wagers[$bet['ball_string']] = $bet['bet_total'] * 1;
			$potential_payouts[$bet['ball_string']] = $payout_rate * $bet['pf_total'];
		}
	}
	
	// get all bets placed for this game

	//$q = "SELECT *, SUM(bet_amount) AS bet_total, SUM(pay_factor) as pf_total FROM `lotto_bet` AS `b` JOIN `lotto_game` g ON b.game_id=g.id LEFT JOIN `lotto_ticket` AS `t` ON `t`.`ticket_id` = `b`.`ticket_id` WHERE g.id='".$game_id."' AND `draw_date` >= '".$today."' AND `t`.`is_voided` = '0' AND `b`.`is_boxed` = '1' GROUP BY `b`.`ball_string`";
	$q = "SELECT *, SUM(bet_amount) AS bet_total, SUM(pay_factor) as pf_total FROM `lotto_bet` AS `b` JOIN `lotto_game` g ON b.game_id=g.id LEFT JOIN `lotto_ticket` AS `t` ON `t`.`ticket_id` = `b`.`ticket_id` WHERE g.id=%i AND `draw_date` >= %s AND `t`.`is_voided` = '0' AND `b`.`is_boxed` = '1' GROUP BY `b`.`ball_string`";
	//$boxed_bets = $db->query($q);
	$boxed_bets = $db->query($q, array($game_id, $today));
	
	if(count($boxed_bets) > 0){		
		foreach($boxed_bets as $bet){
			$ball_count = strlen($bet['ball_string']);
			
			// get payout rate for this game based on payout scheme

			//$q = "SELECT * FROM `payout_scheme_detail` WHERE `scheme_id` = '".$bet['payout_scheme_id']."' AND ball_count = '".$ball_count."'";
			$q = "SELECT * FROM `payout_scheme_detail` WHERE `scheme_id` = %i AND ball_count = %i";
			//$payout_scheme = $db->queryOneRow($q);
			$payout_scheme = $db->queryOneRow($q, array($bet['payout_scheme_id'], $ball_count));

			$payout_rate = $payout_scheme['payout_rate'];
			
			$tot_collected += $bet['bet_total'];
			$picks[$bet['ball_string']] = $bet['ball_string'];
			$wagers[$bet['ball_string']] += $bet['bet_total'] * 1;
			$potential_payouts[$bet['ball_string']] += $payout_rate * $bet['pf_total'];
			
			// if bet is boxed, get all permutations and those potential payouts
			if($bet['is_boxed'] == 1){
				// get potential payouts on boxed bets for permutations of this pick
				$permutations = $core->permutations($bet['ball_string']);
				
				foreach($permutations as $perm){
					if($perm != $bet['ball_string']){
						$wagers[$perm] = $wagers[$perm] > 0 ? $wagers[$perm] : 0;
						$picks[$perm] = $perm;
						$potential_payouts[$perm] += $payout_rate * $bet['pf_total'];
					}
				}
			}
		}
	}

	// calculate risks
	if(count($potential_payouts) > 0){
		foreach($potential_payouts as $bet_on => $payout){
			$risks[$bet_on] = $payout - $tot_collected;
		}
	}
	
	$response = array();
	$response['picks'] = $picks;
	$response['wagers'] = $wagers;
	$response['risks'] = $risks;
	
	// Sort the outer array
	ksort($response); 
	// Sort each inner array
	foreach($response as &$innerArray)
	{
		ksort($innerArray);
	}

	echo json_encode(array("picks" => $core->flatten_array($response['picks']), "wagers" => $core->flatten_array($response['wagers']), "risks" => $core->flatten_array($response['risks'])));
