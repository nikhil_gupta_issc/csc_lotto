
$(document).on('ready',function(){

        $.ajax({
                url: '/admin/ajax/virtual_money.php',
                dataType: 'json',
                data : { action: 'get_default_offset'},
                method: 'POST'
        }).done(function(data){
                if(data.result == true){
                    $('#default_virtual_money').val(data.value);
                }
                console.log(data);
         });

        $('#default_virtual_money').on('blur',function(){
            if($('#default_virtual_money').val() != ''){
                $("#default_vm_modal").modal("show");
            }
              
        });
         
        $('#default_vm_confirm').on('click',function(){
             var offset_value = $('#default_virtual_money').val();
             if(!isNaN(offset_value) && offset_value > 0){
                   $.ajax({
                         url: '/admin/ajax/virtual_money.php',
                         dataType: 'json',
                         data : { action: 'save_default_offset', value: offset_value },
                         method: 'POST'
                    }).done(function(data){
                         $('.close').click();
                         if(data.result == false){
                            bootbox.alert("Error: ".data.error);
                         } else{
                             bootbox.alert("Default virtual money offset has been changed successfully.");
                         }
                   });
             } else{
                 $('.close').click();
                 bootbox.alert("Please provide a valid virtual money amount");
             }
        });

});
