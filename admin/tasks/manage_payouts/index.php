<?php
$page_title = "Manage Payouts";
include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
$start_date = isset($_GET['filter_date_from']) ? "?filter_date_from=".$_GET['filter_date_from'] : "?filter_date_from=".date("Y-m-d", time());
$end_date = isset($_GET['filter_date_to']) ? "&filter_date_to=".$_GET['filter_date_to'] : "&filter_date_to=".date("Y-m-d", time());
$_GET['filter_date_from'] = isset($_GET['filter_date_from']) ? $_GET['filter_date_from'] : date("Y-m-d", time());
$_GET['filter_date_to'] = isset($_GET['filter_date_to']) ? $_GET['filter_date_to'] : date("Y-m-d", time());
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', strtotime($_GET['filter_date_from'])); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', strtotime($_GET['filter_date_to'])); ?>"));

			// variable to store index of currently selected child table
			var selected_child_index = -1;
			var selected_ticket_number = -1;
			var child_table = [];
			
			// function to dynamically create game child datatables
			function get_child_table(table_id){				
				$child_table = '<div class="child_table table-responsive"><table id="child_'+table_id+'" class="display table dt-child-table" cellspacing="0" width="100%">'+
					'<thead>'+
						'<tr>'+
							'<th>Game</th>'+
							'<th>Ball String</th>'+
							'<th>S/B</th>'+
							'<th>Bet Amount</th>'+
							'<th>Amount Won</th>'+
						'</tr>'+
					'</thead>'+
					'<tbody>'+
						'<tr>'+
							'<td colspan="4" class="dataTables_empty">Loading data from server</td>'+
						'</tr>'+
					'</tbody>'+
				'</table></div>';
			
				return $child_table;
			}
			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			// main house level datatable initialization
			var data_table = $('#datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/manage_payouts.php<?php echo $start_date; echo $end_date; ?>",
				"columns": [
					{
						"className":      	'details-control',
						"orderable":      	false,
						"data":           	null,
						"defaultContent": 	'',
						"width":			'20px'
					},
					{ "data": "ticket_number" },
					{ "data": "win_date" },
					{ "data": "total_payout" },
					{ "data": "cashier" },
					<!-- { "data": "payout_status" } !-->
				],
				"order": [[1, 'asc']]
			});
			
			// track last clicked house
			$(document).on('click', '#datatable tbody tr', function () {
				if (typeof $(this).attr('id') != 'undefined'){
					selected_ticket_number = $(this).attr('id').replace("row_", "");
				}
			});

			// Add event listener for opening and closing game child tables
			$(document).on('click', '#datatable tbody td.details-control', function () {			
				var tr = $(this).closest('tr');
				var row = data_table.row( tr );
				
				if(row.child.isShown()){
					// This row is already open - close it
					row.child.hide();
					tr.removeClass('shown');
				}else{
					var row_data = row.data();
					var ticket_id = row_data['DT_RowId'].replace("row_", "");
					
					// is child has not been dynamically created, create it
					if (typeof row.child() == 'undefined'){
						// create this row
						row.child(get_child_table(ticket_id)).show();
						
						// create datatable
						child_table[ticket_id] = $('#child_'+ticket_id).DataTable({
							"processing": true,
							"serverSide": true,
							"deferRender": true,
							//bInfo: false,
							"ajax": "/admin/ajax/datatables/manage_payouts_child_table.php?ticket_number="+row_data['ticket_number'],
							"columns": [
								{ "data": "short_name" },
								{ "data": "ball_string" },
								{ "data": "is_boxed" },
								{ "data": "bet_amount" },
								{ "data": "amount_won" }
							],
							"order": [[0, 'asc']],
							"paging":   	true,
							"searching": 	false,
							"info":     	false
						});
						
						console.log(child_table);
					}else{
						// otherwise, show what was already rendered
						row.child.show();
					}
					
					tr.addClass('shown');
				}
			});
			
			/*
			$(document).on('click', '.dt-child-table tbody tr', function(){					
				// update index of selected child table
				selected_child_index = $(this).closest('.dt-child-table').attr('id').replace("child_", "");
			
				// remove selected class from all rows
				$('.dt-child-table tbody tr').removeClass('selected');
				
				// select row clicked and show action buttons
				$(this).addClass('selected');
				$('.btn-row-action').show();
			});
			*/
			
			$("#start_date").datetimepicker().on('dp.change', function (e) {
				window.location.href = 'index.php?filter_date_from='+$(this).data("DateTimePicker").date().format("YYYY-MM-DD")+'<?php echo $end_date; ?>';
				
				/*
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
					$.each(child_table, function(key, val){
						child_table[key].destroy();
					});
					child_table = [];
				}
				
				// transactions datatable initialization
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					"ajax": "/admin/ajax/datatables/manage_payouts.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
					"columns": [
						{
							"className":      	'details-control',
							"orderable":      	false,
							"data":           	null,
							"defaultContent": 	'',
							"width":			'20px'
						},
						{ "data": "ticket_number" },
						{ "data": "win_date" },
						{ "data": "total_payout" },
						{ "data": "cashier" },
						{ "data": "payout_status" }
					],
					"order": [[1, 'desc']]
				});
				
				data_table.draw(true);
				*/
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				window.location.href = 'index.php<?php echo $start_date; ?>&filter_date_to='+$(this).data("DateTimePicker").date().format("YYYY-MM-DD");

				/*
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
					$.each(child_table, function(key, val){
						child_table[key].destroy();
					});
					child_table = [];
				}
				
				// transactions datatable initialization
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					"ajax": "/admin/ajax/datatables/manage_payouts.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
					"columns": [
						{
							"className":      	'details-control',
							"orderable":      	false,
							"data":           	null,
							"defaultContent": 	'',
							"width":			'20px'
						},
						{ "data": "ticket_number" },
						{ "data": "win_date" },
						{ "data": "total_payout" },
						{ "data": "cashier" },
						{ "data": "payout_status" }
					],
					"order": [[1, 'desc']]
				});
				
				data_table.draw(true);
			*/
			});
		});
		
		function convertTime(time){
			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours < 10) sHours = "0" + sHours;
			if(minutes < 10) sMinutes = "0" + sMinutes;
			return sHours + ":" + sMinutes;
        }
	</script>
	
	<style>
		td.details-control {
			background: url('/lib/assets/datatables/media/images/details_open.png') no-repeat center center;
			cursor: pointer;
		}
		tr.shown td.details-control {
			background: url('/lib/assets/datatables/media/images/details_close.png') no-repeat center center;
		}
		.child_table{
			padding-left: 50px;
		}
	</style>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
					<div class="btn-group pull-right">
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
				
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th></th>
								<th class="text-left">Ticket Number</th>
								<th class="text-left">Win Date</th>
								<th class="text-left">Amount Won</th>
								<th class="text-left">Cashier</th>
							<!--	<th class="text-left">Status</th> !-->
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
