<?php
	$page_title = "Disputes";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			$('#datatable tbody').on('click', 'tr', function(){
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(".btn-row-action").hide();
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(".btn-row-action").show();
					$(this).addClass('selected');
				}
			});
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "subject" },
					{ "data": "created_on" },
					{ "data": "user" },
					{ "data": "status" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/disputes.php?is_resolved="+$('#is_resolved').val()
			});
			
			var messages_data_table;
			
			$(document).on("click", "#view_details_btn", function(){
				$("#view_details_modal").modal("show");
				
				var tr = $(".selected");
				var row = data_table.row( tr );
				var row_data = row.data();
				var dispute_id = row_data['DT_RowId'].replace("row_", "");
				
				if($.fn.dataTable.isDataTable('#dispute_messages_datatable')){
					messages_data_table.destroy();
				}
				
				messages_data_table = $('#dispute_messages_datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					bInfo: false,
					"columns": [
						{ "data": "user" },
						{ "data": "message" },
						{ "data": "created_on" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/dispute_messages.php?dispute_id="+dispute_id
				});
				
				messages_data_table.draw(true);
			});
			
			$(document).on('change', '#is_resolved', function(){				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
					"responsive": true,
					"processing": true,
					"serverSide": true,
					//bInfo: false,
					"columns": [
						{ "data": "subject" },
						{ "data": "created_on" },
						{ "data": "user" },
						{ "data": "status" }
					],
					"order": [[0, 'desc']],
					"ajax": "/admin/ajax/datatables/disputes.php?is_resolved="+$('#is_resolved').val()
				});
				
				data_table.draw(true);
			});
			
			$(document).on('click', '#mark_resolved_btn', function(){
				$("#resolve_modal").modal('show');
			});
			
			$(document).on('click', '#mark_as_resolved', function(){
				var tr = $(".selected");
				var row = data_table.row( tr );
				var row_data = row.data();
				var dispute_id = row_data['DT_RowId'].replace("row_", "");
				
				$(this).prop("disabled", true);
				
				$.ajax({
					url: "/admin/ajax/datatables/dispute_messages_buttons.php",
					data: {
						action : "mark_resolved",
						resolution_details : $("#resolution_details").val(),
						dispute_id : dispute_id
					},
					method: "POST",
					dataType: "json"
				})
				.done(function(data){
					if(data.success == true){
						bootbox.alert("This dispute has been resolved. The customer has been emailed the resolution details.");
						$("#resolve_modal").modal("hide");
						data_table.draw(true);
					}else{
						bootbox.alert(data.errors);
					}
					$('#mark_as_resolved').prop("disabled",false);
				});
			});
			
			$(document).on('click', '#send_response', function(){
				var tr = $(".selected");
				var row = data_table.row( tr );
				var row_data = row.data();
				var dispute_id = row_data['DT_RowId'].replace("row_", "");
				
				$(this).prop("disabled", true);
				
				$.ajax({
					url: "/admin/ajax/datatables/dispute_messages_buttons.php",
					data: {
						action : "respond",
						message : $("#message").val(),
						dispute_id : dispute_id
					},
					method: "POST",
					dataType: "json"
				})
				.done(function(data){
					if(data.success == true){
						bootbox.alert("Your response has been sent to the customer.");
						$("#view_details_modal").modal("hide");
						data_table.draw(true);
					}else{
						bootbox.alert(data.errors);
					}
					$('#send_response').prop("disabled",false);
				});
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Disputes</h3>
					<div class="btn-group pull-right">
						<a id="view_details_btn" href="#" class="btn btn-primary btn-sm btn-row-action">View Details</a>
						<a id="mark_resolved_btn" href="#" class="btn btn-primary btn-sm btn-row-action">Mark as Resolved</a>
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">
					
						<div class="form-group">
							<label for="is_resolved" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">Status</label>
							<div class="col-lg-3 col-md-4 col-sm-5">
								<select type="text" class="form-control" id="is_resolved">
									<option value="0">Pending</option>
									<option value="1">Closed</option>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Subject</th>
								<th class="text-left">Created On</th>
								<th class="text-left">Customer</th>								
								<th class="text-left">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="resolve_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="edit_row_modal_label">Resolve Dispute</h4>
					<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="resolution_details" class="col-sm-3 control-label">Resolution Details</label>
							<div class="col-sm-9">
								<textarea class="form-control" id="resolution_details" placeholder="Resolution Details"></textarea>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-magenta" id="mark_as_resolved">Resolve</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for editting -->
	<div class="modal fade" id="view_details_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="edit_row_modal_label">Dispute Message Detail</h4>
					<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<table id="dispute_messages_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">User</th>
								<th class="text-left">Message</th>
								<th class="text-left">Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="3" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					
					<h3 style="text-align:center;margin-bottom:20px;">Respond to Customer Regarding Dispute</h3>
				
					<form class="form-horizontal">
						<div class="form-group">
							<label for="message" class="col-sm-3 control-label">Message</label>
							<div class="col-sm-9">
								<textarea class="form-control" id="message" placeholder="Message"></textarea>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-magenta" id="send_response">Send Response</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 