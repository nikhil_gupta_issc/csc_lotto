<?php
	$page_title = "View / Void Tickets";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "purchase_date" },
					{ "data": "ticket_number" },
					{ "data": "cashier_id" },
					{ "data": "location_id" },
					{ "data": "user_id" },
					{ "data": "amount" },
					{ "data": "amount_won" },
					{ "data": "is_voided" },
                                        { "data": "status_id" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/view_void_tickets.php?ticket_number=<?php echo $_GET['ticket_number']; ?>"
			});
			
			// temporary transactions datatable initialization
			// definition redefined on row click
			var bet_data_table = $('#bet_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"columns": [
					{ "data": "short_name" },
					{ "data": "draw_time" },
					{ "data": "ball_string" },
					{ "data": "winning_draw" },
					{ "data": "is_boxed" },
					{ "data": "bet_amount" },
					{ "data": "payout" },
					{ "data": "is_processed" }
				],
				"ajax": "/admin/ajax/datatables/bets.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var columns = data_table.row(this).data();
				if(columns['void_status'] == "Void"){
					$('#void_btn').text("Active");
				}else{
					$('#void_btn').text("Void");
				}
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
				
				// show the reprint ticket button
				$('#reprint_ticket').show();
				
				// show or hide edit/delete buttons
				var selected_count = data_table.$('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
			
			$('#void_btn').click(function(){
				var action = $(this).text().toLowerCase();

				if(action == 'void'){
					var columns = data_table.row('.selected').data();
					$("#void_ticket_number").val(columns['ticket_number']);
					$("#void_comment").val(columns['voided_comment']);
					$('#void_ticket_modal').modal("show");
				}else{
					// get database id of selected row
					var id = data_table.$('tr.selected').attr('id').replace("row_", "");

					$.ajax({
						url: "/admin/ajax/datatables/view_void_tickets_buttons.php",
						type: "POST",
						data: {
							action : action,
							voided_comment : '',
							id : id
						},
						dataType: "json",
						success: function(data){
							if(data == true){
								// hide buttons and reload table if successful
								$('.btn-row-action').hide();
								data_table.draw(true);
							}else{
								$('#processed_bets_modal').modal("show");
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							alert("Operation Failed!");
						}
					});
				}
			});

			$('#save_void_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/view_void_tickets_buttons.php",
					type: "POST",
					data: {
						action : 'void',
						voided_comment : $("#void_comment").val(),
						id : id
					},
					dataType: "json",
					success: function(data){
						if(data == true){
							// hide buttons and reload table if successful
							$('.btn-row-action').hide();
							data_table.draw(true);
						}else{
							$('#processed_bets_modal').modal("show");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$(document).on('dblclick', '#datatable tbody tr', function(e){
				// make sure it is still selected so id can be found
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// bind dblclick of row to edit button
				$('#view_details_btn').trigger('click');
			});
			
			$('#reprint_ticket').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");
				
				window.open("/admin/receipts/get_receipt.php?ticket_id="+id,"receipt");
				$("#receipt_modal").modal("show");
			});
			
			$('#view_details_btn').click(function(){
				// get database id of selected row
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				var action = $(this).text().toLowerCase();
				var type = "";
				
				var columns = data_table.row('.selected').data();
				if(columns["cashier_id"] == "Online"){
					type="user";
				}else{
					type="cashier";
				}
				
				$("#purchase_date").text(columns['purchase_date']);
				$("#ticket_number").text(columns['ticket_number']);
				$("#shift").text(columns['shift_id']);
				$("#voided_comment").text(columns['voided_comment']);
				
				$.ajax({
					url: "/admin/ajax/datatables/view_void_tickets_buttons.php",
					type: "POST",
					data: {
						action : action,
						id : id,
						type : type
					},
					dataType: "json",
					success: function(data){
						if(type=="user"){
							$("#user").text(data.name);
							$("#cashier").text(columns['cashier_id']);
							$("#location").text(columns['location_id']);
							$("#status").html(columns['is_voided'].replace("<br>", " "));
						}else{
							$("#user").text(columns['user_id']);
							$("#cashier").text(data.name);
							$("#location").text(data.location);
							$("#status").html(columns['is_voided'].replace("<br>", " "));
						}

						// load transactions datatable
						if($.fn.dataTable.isDataTable('#bet_datatable')){
							bet_data_table.destroy();
						}
						
						bet_data_table = $('#bet_datatable').DataTable({
							"responsive": true,
							"processing": true,
							"serverSide": true,
							//bInfo: false,
							"columns": [
								{ "data": "short_name" },
								{ "data": "draw_time" },
								{ "data": "ball_string" },
								{ "data": "winning_draw" },
								{ "data": "is_boxed" },
								{ "data": "bet_amount" },
								{ "data": "payout" },
								{ "data": "is_processed" }
							],
							"ajax": "/admin/ajax/datatables/bets.php?ticket_id="+id
						});
						
						bet_data_table.draw(true);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
				
				// show modal
				$('#view_details_modal').modal('show');
			});
			
			$('#bet_datatable').on('shown.bs.modal', function() {
				//recalculate the dimensions of datatable
				bet_data_table.columns.adjust().responsive.recalc();
			});
		});
		
		function convertTime(time){
			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours < 10) sHours = "0" + sHours;
			if(minutes < 10) sMinutes = "0" + sMinutes;
			return sHours + ":" + sMinutes;
        }
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Void/View Tickets</h3>
					<div class="btn-group pull-right">
						<a id="view_details_btn" href="#" class="btn btn-primary btn-sm btn-row-action">View Details</a>
						<a id="reprint_ticket" href="#" class="btn btn-primary btn-sm btn-row-action">Reprint Ticket</a>
						<a id="void_btn" class="btn btn-default btn-sm btn-row-action">Void</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Purchase Date</th>
								<th class="text-left">Ticket Number</th>
								<th class="text-left">Cashier</th>
								<th class="text-left">Location</th>
								<th class="text-left">User</th>
								<th class="text-left">Total</th>
								<th class="text-left">Won</th>
								<th class="text-left">Status</th>
                                                                <th class="text-left">Pay Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for voiding details -->
	<div class="modal fade" id="void_ticket_modal" tabindex="-1" role="dialog" aria-labelledby="void_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="void_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="void_row_modal_label">Void Ticket</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							<p class="form-control-static" id="void_ticket_number"></p>
						</div>
					</div>
					<div class="modal-body clearfix">
						<form class="form">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label" for="void_comment">Comments</label>
									<textarea class="form-control" id="void_comment"></textarea>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button id="save_void_btn" type="button" class="btn btn-primary" data-dismiss="modal">Void</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Modal window for ticket details -->
	<div class="modal fade" id="view_details_modal" tabindex="-1" role="dialog" aria-labelledby="edit_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="edit_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="edit_row_modal_label">Ticket Details</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							
						</div>
					</div>
					<div class="modal-body clearfix">
						<form class="form">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="ticket_number" class="control-label">Ticket Number</label>
									<p class="form-control-static" id="ticket_number"></p>
								</div>
								
								<div class="form-group">
									<label for="cashier" class="control-label">Cashier</label>
									<p class="form-control-static" id="cashier"></p>
								</div>
								
								<div class="form-group">
									<label for="location" class="control-label">Location</label>
									<p class="form-control-static" id="location"></p>
								</div>
								
								<div class="form-group">
									<label for="status" class="control-label">Status</label>
									<p class="form-control-static" id="status"></p>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label for="purchase_date" class="control-label">Purchase Date</label>
									<p class="form-control-static" id="purchase_date"></p>
								</div>
								
								<div class="form-group">
									<label for="shift" class="control-label">Shift</label>
									<p class="form-control-static" id="shift"></p>
								</div>
								
								<div class="form-group">
									<label for="user" class="control-label">User</label>
									<p class="form-control-static" id="user"></p>
								</div>
								
								<div class="form-group">
									<label for="voided_comment" class="control-label">Void Comments</label>
									<p class="form-control-static" id="voided_comment"></p>
								</div>
							</div>
						</form>
						<table id="bet_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-left">Game</th>
									<th class="text-left">Draw Date</th>
									<th class="text-left">Ball String</th>
									<th class="text-left">Winning Draw</th>
									<th class="text-left">S/B</th>
									<th class="text-left">Bet Amount</th>
									<th class="text-left">Payout</th>
									<th class="text-left">Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="6" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="processed_bets_modal" tabindex="-1" role="dialog" aria-labelledby="processed_bets_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="processed_bets_modal_label">Processed Bets</h4>
				</div>
				<div class="modal-body">
					This ticket already has processed bets. For security and accounting purposes, you cannot void this ticket.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="receipt_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="receipt_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="receipt_modal_label">Print Preview</h4>
				</div>
				<div class="modal-body clearfix" style="text-align:center;">
					<iframe id="receipt" name="receipt" height="500px" src="" seamless width="90%"></iframe>
					<div class="col-sm-12">
						<div id="receipt_error" class="alert alert-danger" role="alert" style="display:none;">
						
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="cancel_receipt" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
