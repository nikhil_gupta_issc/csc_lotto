<?php
	$page_title = "View / Void Rapidballs Tickets";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<script>
		$(document).ready(function(){
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var data_table = $('#datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				bInfo: false,
				"columns": [
					{ "data": "user_id" },
					{ "data": "bet_type" },
					{ "data": "bet_on" },
					{ "data": "amount" },
					{ "data": "amount_won" },
					{ "data": "purchase_date" },
					{ "data": "status" }
				],
				"order": [[0, 'desc']],
				"ajax": "/admin/ajax/datatables/view_void_rapidballs_tickets.php"
			});
			
			$('#datatable tbody').on('click', 'tr', function(){
				// set name of enable/disable button
				var columns = data_table.row(this).data();
				if(columns['is_voided'] == "0"){
					$('#void_btn').show();
					$('#void_details_btn').hide();
				}else{
					$('#void_btn').hide();
					$('#void_details_btn').show();
				}
				
				// toggle selection css class
				if($(this).hasClass('selected')){
					$(this).removeClass('selected');
				}else{
					data_table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
			});
			
			$(document).on("click", "#void_details_btn", function(){
				var columns = data_table.row(".selected").data();
				$("#voided_by").val(columns['voided_by']);
				$("#voided_on").val(columns['voided_on']);
				$("#reason").val(columns['void_reason']);
				$("#amount_refunded").val(columns['amount_refunded']);
				$("#void_details_modal").modal("show");
			});
			
			$(document).on("click", "#save_void_btn", function(){
				var id = data_table.$('tr.selected').attr('id').replace("row_", "");

				$.ajax({
					url: "/admin/ajax/datatables/view_void_rapidballs_tickets_buttons.php",
					type: "POST",
					data: {
						action : 'void',
						voided_comment : $("#void_comment").val(),
						id : id
					},
					dataType: "json",
					success: function(data){
						if(data == true){
							$('.btn-row-action').hide();
							$('.selected').removeClass('selected');
						}else{
							bootbox.alert(data.errors);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						alert("Operation Failed!");
					}
				});
			});
			
			$('#void_btn').click(function(){
				$("#void_ticket_modal").modal("show");
			});
		});
	</script>

	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Void/View Rapidballs Tickets</h3>
					<div class="btn-group pull-right">
						<a id="void_btn" class="btn btn-default btn-sm btn-row-action">Void</a>
						<a id="void_details_btn" class="btn btn-default btn-sm btn-row-action">Void Details</a>
					</div>
				</div>
				<div class="panel-body">
					<table id="datatable" class="display table dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="text-left">Customer</th>
								<th class="text-left">Bet Type</th>
								<th class="text-left">Bet On</th>
								<th class="text-left">Amount</th>
								<th class="text-left">Won</th>
								<th class="text-left">Purchased On</th>
								<th class="text-left">Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal window for voiding details -->
	<div class="modal fade" id="void_ticket_modal" tabindex="-1" role="dialog" aria-labelledby="void_row_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="void_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="void_row_modal_label">Void Ticket</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							<p class="form-control-static" id="void_ticket_number"></p>
						</div>
					</div>
					<div class="modal-body clearfix">
						<form class="form">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label" for="void_comment">Comments</label>
									<textarea class="form-control" id="void_comment"></textarea>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button id="save_void_btn" type="button" class="btn btn-primary" data-dismiss="modal">Void</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Modal window for void details -->
	<div class="modal fade" id="void_details_modal" tabindex="-1" role="dialog" aria-labelledby="void_details_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="void_row_modal_form" class="form" action="#" method="POST">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title pull-left" id="void_row_modal_label">Void Ticket</h4>
						<div id="modal_row_id" class="pull-right" style="padding-right: 6px;">
							<p class="form-control-static" id="void_ticket_number"></p>
						</div>
					</div>
					<div class="modal-body clearfix">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="voided_by">Voided By:</label>
								<div class="col-sm-9">
									<input type="text" disabled class="form-control" id="voided_by">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="voided_on">Voided On:</label>
								<div class="col-sm-9">
									<input type="text" disabled class="form-control" id="voided_on">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="amount_refunded">Amount Refunded:</label>
								<div class="col-sm-9">
									<input type="text" disabled class="form-control" id="amount_refunded">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="reason">Reason:</label>
								<div class="col-sm-9">
									<textarea disabled class="form-control" id="reason"></textarea>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button id="save_void_btn" type="button" class="btn btn-primary" data-dismiss="modal">Void</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 