<?php
$page_title = "Manage Winning Numbers";
include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
<script>
$(document).ready(function(){
	// default date to today
	$('#winning_date').data("DateTimePicker").clear().date(moment());
	
	// create datatable
	var data_table = $('#datatable').DataTable({
		"processing": true,
		"serverSide": true,
		"deferRender": true,
		bInfo: false,
		"ajax": "/admin/ajax/datatables/manage_winning_numbers.php?house_id="+$("#house").val()+"&date="+$('#winning_date').data("DateTimePicker").date().format("YYYY-MM-DD"),
		"columns": [
			{ "data": "name" },
			{ "data": "game_name" },
			{ "data": "purchase_date" },
			{ "data": "draw_date" },
			{ "data": "ticket_number" },
			{ "data": "ball_string" },
			{ "data": "is_boxed" },
			{ "data": "winning_amount" }
		],
		"order": [[0, 'asc']],
		"paging":   	true,
		"searching": 	true,
		"info":     	true
	});
	
	// track last clicked house
	$('#datatable tbody').on('click', 'tr', function () {
		if (typeof $(this).attr('id') != 'undefined'){
			selected_location_id = $(this).attr('id').replace("row_", "");
		}
	});

	//Initialize the values
	$.ajax({
		url: "/admin/ajax/manage_winning_numbers.php",
		type: "POST",
		data: {
			action : "get_winning_numbers",
			house_id : $("#house").val(),
			win_date : $('#winning_date').data("DateTimePicker").date().format("YYYY-MM-DD")
		},
		dataType: "json",
		success: function(data){
			$("#two_ball").val(data.two_ball);
			$("#three_ball").val(data.three_ball);
			$("#four_ball").val(data.four_ball);
			$("#five_ball").val(data.five_ball);
			$("#six_ball").val(data.six_ball);
			$("#two_ball").prop("disabled", data.two_ball_enabled);
			$("#three_ball").prop("disabled", data.three_ball_enabled);
			$("#four_ball").prop("disabled", data.four_ball_enabled);
			$("#five_ball").prop("disabled", data.five_ball_enabled);
			$("#six_ball").prop("disabled", data.six_ball_enabled);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			bootbox.alert("Operation Failed!");
		}
	});

	$("#winning_date").datetimepicker().on('dp.change', function (e) {
		$.ajax({
			url: "/admin/ajax/manage_winning_numbers.php",
			type: "POST",
			data: {
				action : "get_winning_numbers",
				house_id : $("#house").val(),
				win_date : $('#winning_date').data("DateTimePicker").date().format("YYYY-MM-DD")
			},
			dataType: "json",
			success: function(data){
				$("#two_ball").val(data.two_ball);
				$("#three_ball").val(data.three_ball);
				$("#four_ball").val(data.four_ball);
				$("#five_ball").val(data.five_ball);
				$("#six_ball").val(data.six_ball);
				$("#two_ball").prop("disabled", data.two_ball_enabled);
				$("#three_ball").prop("disabled", data.three_ball_enabled);
				$("#four_ball").prop("disabled", data.four_ball_enabled);
				$("#five_ball").prop("disabled", data.five_ball_enabled);
				$("#six_ball").prop("disabled", data.six_ball_enabled);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				bootbox.alert("Operation Failed!");
			}
		});

		data_table.destroy();

		data_table = $('#datatable').DataTable({
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			//bInfo: false,
			"ajax": "/admin/ajax/datatables/manage_winning_numbers.php?house_id="+$("#house").val()+"&date="+$('#winning_date').data("DateTimePicker").date().format("YYYY-MM-DD"),
			"columns": [
				{ "data": "name" },
				{ "data": "game_name" },
				{ "data": "purchase_date" },
				{ "data": "draw_date" },
				{ "data": "ticket_number" },
				{ "data": "ball_string" },
				{ "data": "is_boxed" },
				{ "data": "winning_amount" }
			],
			"order": [[0, 'asc']],
			"paging":   	true,
			"searching": 	true,
			"info":     	true
		});

		data_table.draw(true);
	});

	$(document).on("click", "#change_numbers", function(){
		$.ajax({
			url: "/admin/ajax/manage_winning_numbers.php",
			type: "POST",
			data: {
				action : "update_winning_numbers",
				house_id : $("#house").val(),
				win_date : $('#winning_date').data("DateTimePicker").date().format("YYYY-MM-DD"),
				two_ball : $("#two_ball").val(),
				three_ball : $("#three_ball").val(),
				four_ball : $("#four_ball").val(),
				five_ball : $("#five_ball").val(),
				six_ball : $("#six_ball").val()
			},
			dataType: "json",
			success: function(data){
				if(data.success==true){
					bootbox.alert("Successfully Changed");
				}else{
					$("#error_reporting").html(data.errors);
					$("#error_reporting").slideDown("fast");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				bootbox.alert("Operation Failed!");
			}
		});
	});

	$(document).on("change", "#house", function(){
		$.ajax({
			url: "/admin/ajax/manage_winning_numbers.php",
			type: "POST",
			data: {
				action : "get_winning_numbers",
				house_id : $(this).val(),
				win_date : $('#winning_date').data("DateTimePicker").date().format("YYYY-MM-DD")
			},
			dataType: "json",
			success: function(data){
				$("#two_ball").val(data.two_ball);
				$("#three_ball").val(data.three_ball);
				$("#four_ball").val(data.four_ball);
				$("#five_ball").val(data.five_ball);
				$("#six_ball").val(data.six_ball);
				$("#two_ball").prop("disabled", data.two_ball_enabled);
				$("#three_ball").prop("disabled", data.three_ball_enabled);
				$("#four_ball").prop("disabled", data.four_ball_enabled);
				$("#five_ball").prop("disabled", data.five_ball_enabled);
				$("#six_ball").prop("disabled", data.six_ball_enabled);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				bootbox.alert("Operation Failed!");
			}
		});

		data_table.destroy();

		data_table = $('#datatable').DataTable({
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			//bInfo: false,
			"ajax": "/admin/ajax/datatables/manage_winning_numbers.php?house_id="+$("#house").val()+"&date="+$('#winning_date').data("DateTimePicker").date().format("YYYY-MM-DD"),
			"columns": [
				{ "data": "name" },
				{ "data": "game_name" },
				{ "data": "purchase_date" },
				{ "data": "draw_date" },
				{ "data": "ticket_number" },
				{ "data": "ball_string" },
				{ "data": "is_boxed" },
				{ "data": "winning_amount" }
			],
			"order": [[0, 'asc']],
			"paging":   	true,
			"searching": 	true,
			"info":     	true
		});

		data_table.draw(true);
	});
});
</script>

<div class="container-fluid">
	<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
	<div class="col-sm-8 col-md-9 col-xl-10">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Manage Winning Numbers</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal">

					<div class="form-group">
						<label for="winning_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">Date</label>
						<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='winning_date'>
							<input type='text' class="form-control" placeholder="Date"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					
					<div class="form-group">
						<label for="house" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3">House</label>
						<div class="col-lg-3 col-md-4 col-sm-5">
							<select type="text" class="form-control" id="house">
								<?php
									$houses = $db->query("SELECT name, id FROM lotto_house WHERE is_disabled=0 ORDER BY name");
									foreach($houses as $house){
										echo "<option value=".$house['id'].">".$house['name']."</option>";
									}
								?>
							</select>
						</div>
						<div class="col-sm-8"></div>
					</div>
					<div class="form-group">
						<label for="two_Ball" class="control-label col-sm-1">2 Ball</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="two_ball"></input>
						</div>
						<div class="col-sm-8"></div>
					</div>
					<div class="form-group">
						<label for="three_Ball" class="control-label col-sm-1">3 Ball</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="three_ball"></input>
						</div>
						<div class="col-sm-8"></div>
					</div>
					
					<div class="form-group">
						<label for="four_Ball" class="control-label col-sm-1">4 Ball</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="four_ball"></input>
						</div>
						<div class="col-sm-8"></div>
					</div>
					
					<div class="form-group">
						<label for="five_ball" class="control-label col-sm-1">5 Ball</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="five_ball"></input>
						</div>
						<div class="col-sm-8"></div>
					</div>

					<!--<div class="form-group">
						<label for="six_ball" class="control-label col-sm-1">6 Ball</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="six_ball"></input>
						</div>
						<div class="col-sm-8"></div>
					</div>-->
					<div class="form-group">
						<div class="col-sm-3 col-sm-offset-1">
							<button type="button" class="btn btn-primary" id="change_numbers">Save</button>
						</div>
						<div class="col-sm-8"></div>
					</div>
					<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
							
					</div>
				</form>
				<table id="datatable" class="display table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="text-left">Location</th>
							<th class="text-left">Game</th>
							<th class="text-left">Purchase Date</th>
							<th class="text-left">Draw Date</th>
							<th class="text-left">Ticket Number</th>
							<th class="text-left">Balls Played</th>
							<th class="text-left">S/B</th>
							<th class="text-left">Winning Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="6" class="dataTables_empty">Loading data from server</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
