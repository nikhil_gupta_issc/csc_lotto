<?php
$page_title = "Manage Shifts";
include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
$franchise = isset($_GET['franchise']) ? $_GET['franchise'] : "";
$location_id = isset($_GET['location_id']) ? $_GET['location_id'] : "";
$end_date = isset($_GET['filter_date_to']) ? "&filter_date_to=".$_GET['filter_date_to'] : "&filter_date_to=".date("Y-m-d", time());
$_GET['filter_date_to'] = isset($_GET['filter_date_to']) ? $_GET['filter_date_to'] : date("Y-m-d", time());
$start_date = isset($_GET['filter_date_from']) ? "?filter_date_from=".$_GET['filter_date_from'] : "?filter_date_from=".date("Y-m-d", time());
$_GET['filter_date_from'] = isset($_GET['filter_date_from']) ? $_GET['filter_date_from'] : date("Y-m-d", time());
?>
	<script>
		$(document).ready(function(){
			$('#start_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', strtotime($_GET['filter_date_from'])); ?>"));
			$('#end_date').data("DateTimePicker").clear().date(moment("<?php echo date('Y-m-d', strtotime($_GET['filter_date_to'])); ?>"));
			
			$("#location").val("<?php echo $location_id; ?>");
			
			// variable to store index of currently selected child table
			var selected_child_index = -1;
			var selected_location_id = -1;
			var child_table = [];
			
			// function to dynamically create game child datatables
			function get_child_table(location_id){				
				$child_table = '<div class="child_table table-responsive"><table id="child_'+location_id+'" class="display table dt-child-table" cellspacing="0" width="100%">'+
					'<thead>'+
						'<tr>'+
							'<th>Cashier</th>'+
							'<th>Shift ID</th>'+
							'<th>Start</th>'+
							'<th>End</th>'+
							'<th>Float</th>'+
							'<th>Opening</th>'+
							'<th>Sales</th>'+
							'<th>Payouts</th>'+
							'<th>Closing</th>'+
							'<th>Expected</th>'+
							'<th>O/S</th>'+
						'</tr>'+
					'</thead>'+
					'<tbody>'+
						'<tr>'+
							'<td colspan="8" class="dataTables_empty">Loading data from server</td>'+
						'</tr>'+
					'</tbody>'+
				'</table></div>';
			
				return $child_table;
			}
			
			// initialize stuff
			$('.btn-row-action').hide();
			$('#edit_row_modal').modal('hide');
			
			var isCollapse = 1;
			$('#collapse_expand').on('click', function() {
				if(isCollapse === 1){
					$('#collapse_expand').html('<img src="/lib/assets/datatables/media/images/details_close.png"></span> <span> Collapse all</span>');
					isCollapse = 0; 
				}else{
					$('#collapse_expand').html('<img src="/lib/assets/datatables/media/images/details_open.png"></span> <span> Expand all</span>');
					isCollapse = 1;
				}
				collapse_expand_rows();
			});

			function collapse_expand_rows(){
				var table_length = $('#datatable tbody tr').length;
				for (var i = 0; i < table_length; i++) {
					var tr = $('.details-control').parents('tr').eq(i);
					var row = data_table.row( tr );
			 
					if ( isCollapse === 1 ) {
						if ( row.child.isShown() ) {
							// This row is already open - close it
							row.child.hide();
							tr.removeClass('shown');
						}
					}else{
						var row_data = row.data();
						var location_id = row_data['DT_RowId'].replace("row_", "");
						
						// is child has not been dynamically created, create it
						if (typeof row.child() == 'undefined'){
							// create this row
							row.child(get_child_table(location_id)).show();
							
							// create datatable
							child_table[location_id] = $('#child_'+location_id).DataTable({
								"processing": true,
								"serverSide": true,
								"deferRender": true,
								//bInfo: false,
								"ajax": "/admin/ajax/datatables/manage_shifts_child_table.php<?php echo $start_date.$end_date; ?>&location_id="+location_id+"&franchise="+$("#franchise").val(),
								"columns": [
									{ "data": "cashier_name" },
									{ "data": "shift_id" },
									{ "data": "shift_start" },
									{ "data": "shift_end" },
									{ "data": "float_amount" },
									{ "data": "opening" },
									{ "data": "sales" },
									{ "data": "payouts" },
									{ "data": "closing" },
									{ "data": "expected" },
									{ "data": "over_short" }
								],
								"order": [[0, 'asc']],
								"paging":   	true,
								"searching": 	true,
								"info":     	false
							});
						}else{
							// otherwise, show what was already rendered
							row.child.show();
						}
						
						tr.addClass('shown');
					}
				}
			}
			
			// main house level datatable initialization
			var data_table = $('#datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				bInfo: false,
				"ajax": "/admin/ajax/datatables/manage_shifts.php<?php echo $start_date.$end_date; ?>&franchise=<?php echo $franchise; ?>&location_id=<?php echo $location_id; ?>",
				"columns": [
					{
						"className":      	'details-control',
						"orderable":      	false,
						"data":           	null,
						"defaultContent": 	'',
						"width":			'20px'
					},
					{ "data": "name" },
					{ "data": "island" }
				],
				"order": [[1, 'asc']]
			});
			
			// track last clicked house
			$('#datatable tbody').on('click', 'tr', function () {
				if (typeof $(this).attr('id') != 'undefined'){
					selected_location_id = $(this).attr('id').replace("row_", "");
				}
			});
			
			$(document).on("change", "#location", function(){
				window.location.href = 'index.php<?php echo $start_date.$end_date; ?>'+"&franchise="+$("#franchise").val()+"&location_id="+$("#location").val();
			});
			
			$(document).on("change", "#franchise", function(){
				window.location.href = 'index.php<?php echo $start_date.$end_date; ?>'+"&franchise="+$("#franchise").val()+"&location_id="+$("#location").val();
			});
			
			$("#start_date").datetimepicker().on('dp.change', function (e) {
				window.location.href = 'index.php?filter_date_from='+$(this).data("DateTimePicker").date().format("YYYY-MM-DD")+'<?php echo $end_date; ?>'+"&franchise="+$("#franchise").val()+"&location_id="+$("#location").val();
			});
			
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				window.location.href = 'index.php<?php echo $start_date; ?>&filter_date_to='+$(this).data("DateTimePicker").date().format("YYYY-MM-DD")+"&franchise="+$("#franchise").val()+"&location_id="+$("#location").val();
			});
			
			// Add event listener for opening and closing game child tables
			$('#datatable tbody').on('click', 'td.details-control', function () {
				var tr = $(this).closest('tr');
				var row = data_table.row( tr );
		 
				if ( row.child.isShown() ) {
					// This row is already open - close it
					row.child.hide();
					tr.removeClass('shown');
				}else{
					var row_data = row.data();
					var location_id = row_data['DT_RowId'].replace("row_", "");
					
					// is child has not been dynamically created, create it
					if (typeof row.child() == 'undefined'){
						// create this row
						row.child(get_child_table(location_id)).show();
						
						// create datatable
						child_table[location_id] = $('#child_'+location_id).DataTable({
							"processing": true,
							"serverSide": true,
							"deferRender": true,
							//bInfo: false,
							"ajax": "/admin/ajax/datatables/manage_shifts_child_table.php<?php echo $start_date.$end_date; ?>&location_id="+location_id+"&franchise="+$("#franchise").val(),
							"columns": [
								{ "data": "cashier_name" },
								{ "data": "shift_id" },
								{ "data": "shift_start" },
								{ "data": "shift_end" },
								{ "data": "float_amount" },
								{ "data": "opening" },
								{ "data": "sales" },
								{ "data": "payouts" },
								{ "data": "closing" },
								{ "data": "expected" },
								{ "data": "over_short" }
							],
							"order": [[0, 'asc']],
							"paging":   	true,
							"searching": 	true,
							"info":     	false
						});
					}else{
						// otherwise, show what was already rendered
						row.child.show();
					}
					
					tr.addClass('shown');
				}
			});
			
			$('#view_z_report_btn').click(function(){
				// get database id of selected row
				var id = $('tr.selected').attr('id').replace("row_", "");
				
				window.open("/admin/receipts/get_receipt.php?shift_id="+id,"receipt");
				$("#receipt_modal").modal("show");
			});
			
			$(document).on('click', '.dt-child-table tbody tr', function(){					
				// update index of selected child table
				selected_child_index = $(this).closest('.dt-child-table').attr('id').replace("child_", "");
			
				// remove selected class from all rows
				$('.dt-child-table tbody tr').removeClass('selected');
				
				// select row clicked and show action buttons
				$(this).addClass('selected');
				$('.btn-row-action').show();
				
				// show or hide edit/delete buttons
				var selected_count = $('tr.selected').length;
				if(selected_count >= 1){
					$('.btn-row-action').show();
				}else{
					$('.btn-row-action').hide();
				}
			});
		});
	</script>
	
	<style>
		td.details-control {
			background: url('/lib/assets/datatables/media/images/details_open.png') no-repeat center center;
			cursor: pointer;
		}
		tr.shown td.details-control {
			background: url('/lib/assets/datatables/media/images/details_close.png') no-repeat center center;
		}
		.child_table{
			padding-left: 50px;
		}
	</style>
	
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
					<div class="btn-group pull-right">
						<a id="view_z_report_btn" href="#" class="btn btn-primary btn-sm btn-row-action">Z Report</a>
					</div>
					
					<button id="collapse_expand" class="btn btn-primary btn-sm pull-left" style="margin-right: 10px; margin-left: 10px;">
						<img src="/lib/assets/datatables/media/images/details_open.png"></span>
						<span>Expand all</span>
					</button>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">

						<div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
						
						<div class="form-group">
							<label for="location" class="control-label col-sm-1">Location Type</label>
							<div class="col-sm-3">
								<select class="form-control" id="franchise">
									<option value="" <?php echo $franchise == "" ? "selected='selected'" : ""; ?>>All</option>
									<option value="franchise" <?php echo $franchise == "franchise" ? "selected='selected'" : ""; ?>>Franchise</option>
									<option value="non_franchise" <?php echo $franchise == "non_franchise" ? "selected='selected'" : ""; ?>>Non-Franchise</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="location" class="control-label col-sm-1">Location</label>
							<div class="col-sm-3">
								<select type="text" class="form-control" id="location">
									<option value="">All Locations</option>
									<?php
										$locations = $db->query("SELECT name, id FROM panel_location WHERE is_deleted=0 ORDER BY name");
										foreach($locations as $location){
											echo "<option value=".$location['id'].">".$location['name']."</option>";
										}
									?>
								</select>
							</div>
							<div class="col-sm-8"></div>
						</div>

						<script type="text/javascript">
							$(function () {
								$("#start_date").on("dp.change", function (e) {
									$('#end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#end_date").on("dp.change", function (e) {
									$('#start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div id="error_reporting" class="alert alert-danger col-sm-4" role="alert" style="display:none;">
								
						</div>
					</form>
					<table id="datatable" class="display table" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th></th>
								<th class="text-left">Location</th>
								<th class="text-left">Island</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="6" class="dataTables_empty">Loading data from server</td>
							</tr>
						</tbody>
					</table>
					<?php
						if(!isset($_GET['filter_date_to'])){
							$date = new DateTime('now');
							$date_str = $date->format("Y-m-d");
						}else{
							$date_str = $_GET['filter_date_to'];
						}
						
						if($_GET['location_id'] != ""){
							$extra_where .= " AND pl.id = ".$_GET['location_id'];
						}
						
						if($franchise == "franchise"){
							$extra_where = " AND pl.commission_rate <> 0";
						}elseif($frachise == "non_franchise"){
							$extra_where = " AND pl.commission_rate = 0";
						}
						
						$q = "SELECT SUM(s.float_amount) AS float_amount, SUM(s.opening) AS opening, SUM(s.sales) AS sales, SUM(s.payouts) AS payouts, SUM(s.closing) AS closing, SUM(s.expected) AS expected, SUM(s.over_short) AS over_short FROM `panel_user_shift` AS `s` JOIN `panel_location` AS `pl` ON `s`.`location_id`=`pl`.`id` WHERE `shift_start` >= '".$_GET['filter_date_from']."' AND `shift_start` <= '".$_GET['filter_date_to']." 23:59:59'".$extra_where;
						$shift_totals = $db->queryOneRow($q);
					?>
					
					<div class="table-responsive">
						<table id="totals" class="table table-striped" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Total Float</th>
									<th>Total Opening</th>
									<th>Total Sales</th>
									<th>Total Payouts</th>
									<th>Total Closing</th>
									<th>Total Expected</th>
									<th>Total O/S</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="float_amount"><?php echo "".number_format($shift_totals['float_amount'])." ".CURRENCY_FORMAT;?></td>
									<td id="opening"><?php echo "".number_format($shift_totals['opening'])." ".CURRENCY_FORMAT;?></td>
									<td id="sales"><?php echo "".number_format($shift_totals['sales'])." ".CURRENCY_FORMAT;?></td>
									<td id="payouts"><?php echo "".number_format($shift_totals['payouts'])." ".CURRENCY_FORMAT;?></td>
									<td id="closing"><?php echo "".number_format($shift_totals['closing'])." ".CURRENCY_FORMAT;?></td>
									<td id="expected"><?php echo "".number_format($shift_totals['expected'])." ".CURRENCY_FORMAT;?></td>
									<td id="over_short"><?php echo "".number_format($shift_totals['over_short'])." ".CURRENCY_FORMAT;?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="receipt_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="receipt_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="receipt_modal_label">Print Preview</h4>
				</div>
				<div class="modal-body clearfix" style="text-align:center;">
					<iframe id="receipt" name="receipt" height="500px" src="" seamless width="90%"></iframe>
					<div class="col-sm-12">
						<div id="receipt_error" class="alert alert-danger" role="alert" style="display:none;">
						
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="cancel_receipt" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<?php
include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
