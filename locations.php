<?php
	$page_title = "Locations";
	
	$top_left_fixed_img = "";
	$top_right_fixed_img = "<img src='/images/cards_rgt.png'>";

	include("header.php");
	include('page_top.php');
	
	
	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}

?>
<style>
	table.styled_tbl, table.styled_tbl2, table.location_tbl2{
		border-collapse:collapse;
		border-bottom:3px solid #620057;
	}
	 
	table.styled_tbl, .styled_tbl_dash{
		width:100%;
	}
	 
	.lotto_tbl{
		float:left;
	}
	 
	table.location_tbl2{
		width:100%;
		margin-top:20px;
	}

	table.styled_tbl tr th, table.styled_tbl2 tr th, .tblheader, .tableheader, table.location_tbl2 tr th, .dash_tbl_hd{ 
		background:#620057;
		color:#fdc217;
		vertical-align:middle;
		padding:6px 10px;
		font-size:15px;	
		text-align:left;
	}

	table.styled_tbl  tr > td, table.styled_tbl2 tr > td, table.location_tbl2 tr td {
		background:#e9c7f1;text-shadow: 1px 1px 2px #FFFFFF;
		padding:5px 10px;
	}

	table.styled_tbl tr > td td{
		background:none;
	}
	 
	table.styled_tbl tr.tblhd td{
		background:#9b078a;
		color:#fff;
		margin-bottom:1px;
		font-weight:bold;
	}

	table.styled_tbl2 tr td{
		padding:15px 9px;
	}

	table.styled_tbl tr td{
		font-weight:bold;
	}

	table.styled_tbl tr.tbltr > td, table.styled_tbl2 tr.tbltr > td, table.location_tbl2 tr.loc_tbl_td_odd > td, .dash_tbl_td_w{
		background:#fff;
	}

	table.styled_tbl2 {
		float:left;
	}

	.td_5{
		font-weight:bold;
		color:#4c0345;
	}
</style>
	<div class="clearfix" style="background:#111111">	
		<div class="row">
			<div class="container">
			   <div class="header_image">
					<img src="/images/locations.jpg" class="img_grow_wide">
					</div>
				</div>
			</div>
			
			<div class="col-md-12" style="color:#CCC; padding:10px;">
				<h2>Contact Us</h2>			
				<p>If you have any questions or complaints feel free to contact us via E-Mail at <a class="text-link" href="mailto:help@csclotto.com">help@csclotto.com</a>. You can also call us during business hours at 1-242-326-5000.</p>
				
				<p>You can also contact us via postal mail using the following address:</p>
				<p>
					<b>CSCLotto</b><br>
					Some Place<br>
					Some Street<br>
					P.O.Box <br>
				</p>
				
				<p>If you want to get in touch with one of our locations directly, please use one of the phone numbers listed below.</p>
			</div>
			
			
	
  <?php
                    $i_island_id = 0;
                    $t_locations = $db->query("SELECT 
    i.id,
    i.name,
    location_link,
    pl.name as location_name,
    pl.telephone
FROM
    island i
        INNER JOIN
    panel_location pl ON pl.island_id = i.id
WHERE
    location_enabled = 1 AND location_link != ''
ORDER BY name");


                 ?>

<div class="col-sm-12" style="margin-bottom: 20px;" >		
<?php
 echo '<div class="col-sm-6">
			<table class="location_tbl2">
				<thead>
					<tr>
						<th class="loc_tbl_hd" colspan="3">New Providence</th>
					</tr>
				</thead>
				<tbody>';
$i_length = count($t_locations);

   for($i=0;$i<$i_length;$i++){
                      if(!empty($i_island_id) && $i_island_id != $t_locations[$i]['id']){

		                 echo '</tbody>
		                         </table>
		                           </div>
		                          <div class="col-sm-6">
		                          <table class="location_tbl2">
				            <thead>
						 <tr>
						     <th class="loc_tbl_hd" colspan="3">'.$t_locations[$i]['name'].'</th>
						 </tr>
					    </thead>
				            <tbody>';
                      } 
if($i%2==0){
					$class = "loc_tbl_td";
					}
					else{
					$class = "loc_tbl_td_odd";
					}
                                echo '<tr class="'.$class.'">
						<td>'.$t_locations[$i]['location_name'].'</td>
						<td>'.$t_locations[$i]['telephone'].'</td>
                                                <td><a target="_blank" href="'.$t_locations[$i]['location_link'].'" ><span class="glyphicon glyphicon-search"> </span></a></td>
					</tr>';                       

                                $i_island_id = $t_locations[$i]['id'];
                    } ?>
</tbody>
		 </table>

						</div>
</div>
</div>
			
			
			
		
<?php
	include("footer.php");
?>
