			<link rel="stylesheet" type="text/css" href="/lib/assets/slick-1.5.7/slick.css"/>
			<link rel="stylesheet" type="text/css" href="/lib/assets/slick-1.5.7/slick-theme.css"/>
			
			<script type="text/javascript" src="/lib/assets/slick-1.5.7/jquery-migrate-1.2.1.min.js"></script>
			<script type="text/javascript" src="/lib/assets/slick-1.5.7/slick.min.js"></script>
			
			<script>
				$(document).ready(function(){				
					// get casino game list
					$.ajax({
						url: "/api/casino/get_casino_games.php",
						type: "POST",
						dataType : "json",
						success: function(response){
							if(response.token != ''){
								$.each(response.games, function(index, game) {
									var tags = "";
									var new_ribbon = "";
									$.each(game.tags, function(index, tag){
										tags += tag+" ";
										if(tag == 'new'){
											new_ribbon = "	<div class='clearfix new_ribbon'></div>";
										}
										if(tag == 'least_played'){
											new_ribbon += "	<div class='clearfix featured_ribbon'></div>";
										}
									});							
									
									$('#casino_games_preview').append(
										"<div id='"+escape(game.LobbyImageAltText.toLowerCase())+"' class='casino_game "+game.GameType.toLowerCase()+" "+tags+"col-xs-6 col-sm-4 col-md-3 col-lg-2' style='padding: 5px;'>"+
										new_ribbon+
										"	<div class='col-sm-12 panel casino_game_inner' style='border: 1px solid purple; background:#000; padding-top: 15px;'>"+
										"		<img onError='hide_game(this);' style='width: 100%;' alt='"+game.LobbyImageAltText+"' src='"+game.LobbyImage+"'>"+
										"		<p style='font-family: Droid Sans;font-size: 13px;color:white;margin-top: 5px;'><b>"+game.LobbyImageAltText+"</b><br><small>Min: "+game.MinBet+", Max: "+game.MaxBet+"</small></p>"+
										"	</div>"+
										"</div>"
									);
								});
								
								$('#casino_games_preview').slick({
									autoplay: true,
									dots: false,
									infinite: true,
									speed: 1000,
									slidesToShow: 6,
									slidesToScroll: 3,
									arrows: false,
									responsive: [
										{
											breakpoint: 900,
											settings: {
												slidesToShow: 4,
												slidesToScroll: 2
											}
										},
										{
											breakpoint: 600,
											settings: {
												slidesToShow: 3,
												slidesToScroll: 2
											}
										},
										{
											breakpoint: 480,
											settings: {
												slidesToShow: 2,
												slidesToScroll: 1
											}
										}
									]
								});
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							//bootbox.alert("Unexpected Error!");
						}
					});
					
					
				});
			</script>
			
			<style>
				.casino_game_type{
					background:#595959;
					background:-moz-linear-gradient(center top,#595959 0%,#1a1a1a 100%);
					-pie-background:linear-gradient(center top,#595959 0%,#1a1a1a 100%);
					background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#595959),color-stop(1,#1a1a1a));
					background-image:-o-linear-gradient(top,#595959 0%,#1a1a1a 100%);
					background:-ms-linear-gradient(center top,#595959 0%,#1a1a1a 100%);
					background:linear-gradient(center top,#595959 0%,#1a1a1a 100%);
					position:relative;
					color:#fff;
					margin:0px;
					overflow:hidden;
					border-bottom: 1px solid #8c8c8c;
					margin-top: -20px;
				}
				
				.casino_game_inner:hover{
					outline: none;
					box-shadow: 0 0 10px #FBB4F0;
				}

				.casino_game_type li{
					display:inline-block;
					position:relative;
					line-height: 30px;
					padding:5px 20px 4px 20px;
					border-right:1px #8c8c8c solid;
					border-bottom:2px transparent solid;
					text-decoration:none;
					text-align:center;
					cursor:pointer;
					-webkit-transform:skew(25deg);
					-moz-transform:skew(25deg);
					-moz-transform:skewX(25deg);
					-ms-transform:skew(25deg);
					-o-transform:skew(25deg);
					transform:skew(25deg);
					outline:none!important;
				}

				.casino_game_type li.active{
					background:-moz-linear-gradient(center top,#212121 50%,#383e28 100%);
					background:-webkit-gradient(linear,left top,left bottom,color-stop(0.5,#212121),color-stop(1,#383e28));
					background:-o-linear-gradient(top,#212121 50%,#383e28 100%);
					background:-ms-linear-gradient(top,#212121 50%,#383e28 100%);
					background:linear-gradient(top,#212121 50%,#383e28 100%);
					border-right: #8c8c8c 1px solid;
					border-bottom: #FBB4F0 2px solid;
					margin-left: -5px;
					padding-right: 25px;
				}

				.casino_game_type li span{
					font-size:12px;
					font-weight:normal;
					color: white;
					display:block;
					text-decoration:none;
					-webkit-transform:skew(-25deg);
					-moz-transform:skew(-25deg);
					-moz-transform:skewX(-25deg);
					-ms-transform:skew(-25deg);
					-o-transform:skew(-25deg);
					transform:skew(-25deg);
					outline:none!important;
					-moz-user-select: none;
					-khtml-user-select: none;
					-webkit-user-select: none;
					-o-user-select: none;
					user-select: none;
				}

				.casino_game_type li.active span, .casino_game_type li span:hover{
					color: #FBB4F0;
					-moz-user-select: none;
					-khtml-user-select: none;
					-webkit-user-select: none;
					-o-user-select: none;
					user-select: none;
				}

				.casino_game_type li.active span{
					font-weight:bold;
					-moz-user-select: none;
					-khtml-user-select: none;
					-webkit-user-select: none;
					-o-user-select: none;
					user-select: none;
				}
				
				.new_ribbon{
					background: url('/images/casino/new_ribbon.png');
					background-size: 80px 80px;
					background-repeat: no-repeat;
					position: absolute;
					top: -2px;
					right: -1px;
					margin: 0;
					width: 80px;
					height: 80px;
					z-index: 999;
				}
				
				.featured_ribbon{
					background: url('/images/casino/featured_ribbon.png');
					background-size: 80px 80px;
					background-repeat: no-repeat;
					position: absolute;
					top: -2px;
					left: -1px;
					margin: 0;
					width: 80px;
					height: 80px;
					z-index: 999;
				}
			</style>
				
			<a href='/casinos.php'>
				<div class='col-md-12' style="height: 220px; overflow: hidden; margin-top: -10px;">
					<div id="casino_games_preview" style="padding: 15px;">
					</div>
				</div>
			</a>
