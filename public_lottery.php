		<div class="clearfix" style="background:#fff">	
			<div class="row">
					<div class="container">
					   <div class="header_image">
							<!--<img src="/images/lottobanner.jpg" class="img_grow_wide">-->
<?php

			$files = scandir('images/lottery_slider_image/');
			$total = count($files);
			$images = array();
			for($x = 0; $x <= $total; $x++){
				if($files[$x] != '.' && $files[$x] != '..' && is_file('images/lottery_slider_image/'.$files[$x])){
					$images[] = $files[$x];
				}
			}
		?>
	
		<div id="carousel-index" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'" class="active"></li>';
						}else{
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'"></li>';
						}
					}
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '
				<div class="item active">

						<img src="/images/lottery_slider_image/'.$images[$x].'" class="carousel-image">

				</div>
							';
						}else{
							echo '
				<div class="item">

						<img src="/images/lottery_slider_image/'.$images[$x].'" class="carousel-image">

				</div>
							';
						}
					}
				?>
						</div>
					</div>
				</div>
		
<script src="/lib/assets/animate_table_change/animator.js"></script>
		<script src="/lib/assets/animate_table_change/rankingTableUpdate.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
var counter_winner = 1;
                             var counter_big_winner = 1;
				function get_recent_winners(){
   var d = new Date();
					var n = d.getTime();

                                        var time = 300 - (parseInt(n) % 300);

                                        if(time >= 295 || counter_winner == 1){
                                        counter_winner++;
					$.ajax({
						url: "/ajax/recent_winners.php",
						method: "POST",
						data:{
							type : 'recent',
							data : 'lottery'
						}
					})
					.done(function(response){
						if(response){
							var old_table = $('#recent_winners_list').html();
							$('#temp_recent_winners_list').html(response);
							var new_table = $('#temp_recent_winners_list').html();
							
							if(old_table != new_table){
								$('#recent_winners_list').rankingTableUpdate('<table class="table" id="recent_winners_list">'+new_table+'</table>', {
									duration: [1000, 0, 700, 0, 500],
									onComplete: function(){
										updating = false;
									},
									animationSettings: {
										up: {
											left: -25,
											backgroundColor: '#CCFFCC'
										},
										down: {
											left: 25,
											backgroundColor: '#8C008C'
										},
										fresh: {
											left: 0,
											backgroundColor: '#CCFFCC'
										},
										drop: {
											left: 0,
											backgroundColor: '#8C008C'
										}
									}
								});
							}
							
						}
					});
}
setTimeout(get_recent_winners, 1000);
				}
				
				function get_big_winners(){
  var d = new Date();
					var n = d.getTime();

                                        var time = 300 - (parseInt(n) % 300);

                                        if(time >= 295 || counter_big_winner == 1){
                                        counter_big_winner++;
					$.ajax({
						url: "/ajax/recent_winners.php",
						method: "POST",
						data:{
							type : 'big',
							data : 'lottery'
						}
					})
					.done(function(response){
						if(response){
							var old_table = $('#big_winners_list').html();
							$('#temp_big_winners_list').html(response);
							var new_table = $('#temp_big_winners_list').html();
							
							if(old_table != new_table){
								$('#big_winners_list').rankingTableUpdate('<table class="table" id="big_winners_list">'+new_table+'</table>', {
									duration: [1000, 0, 700, 0, 500],
									onComplete: function(){
										updating = false;
									},
									animationSettings: {
										up: {
											left: -25,
											backgroundColor: '#CCFFCC'
										},
										down: {
											left: 25,
											backgroundColor: '#8C008C'
										},
										fresh: {
											left: 0,
											backgroundColor: '#CCFFCC'
										},
										drop: {
											left: 0,
											backgroundColor: '#8C008C'
										}
									}
								});
							}
							
						}
					});
} 
setTimeout(get_big_winners, 1000);
				}
			//	get_recent_winners();
			//	get_big_winners();
			});
		</script>		
<script>
	$(document).ready(function(){	
		var data_table = $('#datatable').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/lotto_results.php",
			"columns": [
				{ "data": "draw_date" },
				{ "data": "house_short_name" },
				{ "data": "house_name" },
				{ "data": "ball_2_draw" },
				{ "data": "ball_3_draw" },
				{ "data": "ball_4_draw" },
				{ "data": "ball_5_draw" }
				//{ "data": "ball_6_draw" }
			],
			"order": [[0, 'desc']]
		});
		
		
	});
</script>
	<script>
								$(document).ready(function(){
									$(document).on("blur", "#calc_bet_amount", function(){
										if($(this).val()!=""){
											var formatted_straight = parseFloat($(this).val().replace("$",""),10);
											$(this).val("$"+formatted_straight.toFixed(2));
										}
									});

									$(document).on("click", "#calculate_payout", function(){
										if($("#calc_bet_amount").val()=="$NaN"){
											$("#calc_error").html("<p>Please enter a valid bet amount</p>");
											$("#calc_error").slideDown("fast");
										}else if($("#calc_number_bet").val().length<2 || $("#calc_number_bet").val().length>5){
											$("#calc_error").html("<p>Please enter a 2, 3, 4, or 5 ball number</p>");
											$("#calc_error").slideDown("fast");
										}else{
											$.ajax({
												url: "/ajax/payout_calculator.php",
												data: {ticket_type:$("#calc_ticket_type").val(), sb:$("#calc_sb").val(), number:$("#calc_number_bet").val(), amount:$("#calc_bet_amount").val()},
												method: "POST"
											}).done(function( data ) {
												$("#calc_error").fadeOut("fast");
												$("#calc_win_amount").text(data);
											});
										}
									});
								});
							</script>			

		<div class="bg">
		<!--	<div class="col-md-6 winners_board ">
				<div class="win-bg-plain">
					<h3 class="headings">Latest Lottery Winners</h2>
				</div>

				<table id='temp_recent_winners_list' style='display: none;'></table>
				
				<table class="table" id="recent_winners_list">
					<thead>
						<tr>
							<th class="position anim:id anim:number hidden" />
							<th class="driverName anim:number hidden" />
							<th class="pointsTotal anim:update hidden" />
							<th class="pointsTotal anim:update hidden" />
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div class="col-md-6 winners_board ">
			
				<div class="win-bg-plain">
					<h3 class="headings">Latest Big Lottery Winners</h2>
				</div>
				
				<table id='temp_big_winners_list' style='display: none;'></table>
				
				<table class="table" id="big_winners_list">
					<thead>
						<tr>
							<th class="position anim:id anim:number hidden" />
							<th class="driverName anim:number hidden" />
							<th class="pointsTotal anim:update hidden" />
							<th class="pointsTotal anim:update hidden" />
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>-->
			
			
		</div>
			
			
		<!-- <?php include('inc_public_casino_slider.php');?> -->
			
			
			
		<div class="bg">
			<div class="col-md-9 winners_board" style="padding-left: 0px;">
				<div class="win-bg-plain" style="margin-top: -20px;">
					<h3 class="headings">Winning Numbers</h2>
				</div>
				<table id="datatable" class="display table dt-responsive win-list" cellspacing="0" width="100%" style="color:#FFF">
					<thead>
						<tr>
							<th class="text-left">Date</th>
							<th class="text-left">House</th>
							<th class="text-left">House Name</th>
							<th class="text-left">2 Ball</th>
							<th class="text-left">3 Ball</th>
							<th class="text-left">4 Ball</th>
							<th class="text-left">5 Ball</th>
							<!--<th class="text-left">6 Ball</th>-->
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="6" class="dataTables_empty">Loading data from server</td>
						</tr>
					</tbody>
				</table>
		
			</div>
			<aside>
			<div class="col-md-3" style="background-color: #bf0a0a;">
					<?php
							$lucky_2 = $db->queryOneRow("SELECT value FROM settings WHERE setting='lucky_number_2_ball'");
							$lucky_3 = $db->queryOneRow("SELECT value FROM settings WHERE setting='lucky_number_3_ball'");
							$lucky_4 = $db->queryOneRow("SELECT value FROM settings WHERE setting='lucky_number_4_ball'");
						?>

						<div>
							<h3 class="headings" style="color: #ffffff;">Lucky Numbers</h2>
						</div>
						<div style="margin-below: 20px;">
							<span class="win-score">
								<span class="pr_items"><a style="color: #fff;" href="/lottery.php?tab=2&number=<?php echo $lucky_2['value']; ?>"><?php echo $lucky_2['value']; ?></a></span>|<span class="pr_items"><a style="color: #fff;" href="/lottery.php?tab=2&number=<?php echo $lucky_3['value']; ?>"><?php echo $lucky_3['value']; ?></a></span>|<span class="pr_items"><a style="color: #fff;" href="/lottery.php?tab=2&number=<?php echo $lucky_4['value']; ?>"><?php echo $lucky_4['value']; ?></a></span>
							</span>
						</div>
						<p>&nbsp;</p>
						
				</div>
			
			<div class="col-md-3 calculator">
			
				<div class="win-bg-plain" style="background-color: #000;">
					<h3 class="headings" style="color: #fff;">Calculator</h2>
				</div>
						<div class="table-responsive">
							<table class="table" >
								<tr>
									<td class="center-block" style="border: 1px solid black;">Played</td>
									<td style="border: 1px solid black;">
										<input type="text" class="form-control input-sm center-block" style="background:#fff;border-radius:0px;color:black;" id="calc_number_bet" />
									</td>
								</tr>
								
								<tr >
									<td style="border: 1px solid black;">Bet CSC</td>
									<td style="border: 1px solid black;">
										<input type="text" class="form-control input-sm center-block" style="background:#fff;border-radius:0px;color:black;" id="calc_bet_amount"/>
									</td>
								</tr>
								
								<tr >
									<td style="border: 1px solid black;">S/B</td>
									<td style="border: 1px solid black;">
										<select id="calc_sb" class="select" style="color:black;">
											<option value="S">Straight</option>
											<option value="B">Boxed</option>
										</select>
									</td>
								</tr>
								
								<tr style="padding:0px; background: black;">
									<td style="border: 1px solid black; background: black;">Ticket Type</td>
									<td style="border: 1px solid black; background: black;">
										<select id="calc_ticket_type" class="select" style="color:black;">
											<?php
												$ticket_types = $db->query("SELECT DISTINCT name FROM payout_scheme");
												foreach($ticket_types as $ticket_type){
													echo "<option value='".$ticket_type['name']."'>".$ticket_type['name']."</option>";
												}
											?>
										</select>
									</td>
								</tr>
								<tr style="padding:0px; background: black;">
									<td colspan="2" style="padding:0px; border: 1px solid black; background: black;">
									   <center><h3>You Win:</h3></center>
									   <center><h3 style="font-weight:bold;color:white;background:#bf0a0a;" id="calc_win_amount">0 CSC</h3></center>
									</td>
								</tr>
								<tr style="padding:0px;text-align:center;border: 1px solid black; background: black;">
									<td colspan="2" style="padding:0px; background: black; height: 45px;">
										<button class=".btn-default" style="color:black;padding:5px 30px 5px 30px;" id="calculate_payout">Calculate</button>
									</td>
								</tr>
							</table>
						
						</div>
					</div>
				</aside>
			</div>
				
				
</div>
		
