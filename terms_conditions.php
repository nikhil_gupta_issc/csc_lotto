<?php
	include("header.php");
	include('page_top.php');
	include('config.php');
	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}
?>
	<div class="clearfix" style="background:#fff; color: 000;">	
			<div class="row">
					<div class="container">
					   <div class="header_image">
							<img src="/images/terms_conditions.jpg" class="img_grow_wide">
						</div>
					</div>
				</div>
<!-- <div class="clearfix" style="text-align: left; padding:10px;">
		<p style="color:#CCC;padding:10px;border: 1px solid purple; background: #000;">Our team of industry veterans started with one mission in mind - to provide a trustworthy and unrivaled online experience for every player, no matter their casino experience level. Whether this is your first time playing online, or your one-thousandth, we promise you lightning-fast deposit authorization and customer service by our knowledgeable, friendly agents. So no matter the hour, when you want to play the best casino games available online, CSCLotto is your Casino/Lottery provider.</p>
		</div> -->
	<div class="col-md-12" style="color:#000;">
	<br />
<p>These Terms of Service (“Terms”) apply to your access to, and use of, CSCLotto products and services (“Services”) available through our website (“Website”). CSCLotto owns and operates the Website and provides the Services to you in accordance with the terms and conditions set out below.</p>

<p>We reserve the right to change or modify the terms and conditions contained in these Terms or any policy or guideline of the Website, at any time and in our sole discretion. We will provide notice of these changes by posting the revised Terms to the Website and changing the “Last Revised” date at the bottom of the Terms, or by providing other means of notice as CSCLotto will determine each time in its sole discretion. Any changes or modifications will be effective immediately upon posting the revisions to the Website and will apply to your subsequent use of the Website. Your continued use of this Website will confirm your acceptance of such changes or modifications; therefore, you should review the Terms and applicable policies whenever you use the Website to understand the terms that apply to such use.</p>

<p>The most current version of the Terms can be reviewed by clicking on the “Terms of Service” hypertext link located at the bottom of our web pages. If you do not agree to the Terms in effect when you access or use the Website, you must stop using the Website. We reserve the right to withdraw or amend the Services without notice, and at any time whatsoever.</p>

<h4>Privacy Policy</h4>

<p>Our privacy policy, which sets out how we will use your information, can be found at <a href="/pricacy-policy.php">Privacy Policy</a>. By using this Website, you consent to the processing described therein and warrant that all data provided by you is accurate.</p>

<p><h4>Use of the Website and Services</h4></p>

<p>You agree to access and use the Website and/or the Services in accordance with these Terms and any guidelines set out on our Website. You will not: (a) commit or encourage a criminal offense; (b) transmit or distribute a virus, trojan, worm, logic bomb or any other material which is malicious, technologically harmful, in breach of confidence or in any way offensive or obscene; (c) hack into any aspect of the Services; (d) corrupt data; (e) cause annoyance to other users; (f) infringe upon the rights of any other person’s proprietary rights; (g) send any unsolicited advertising or promotional material, commonly referred to as “spam”; or attempt to affect the performance or functionality of any computer facilities of or accessed through this Website.</p>

<p>Breaching this provision would constitute a criminal offense and CSCLotto will report any such breach to the relevant law enforcement authorities and disclose your identity to them.</p>

<p>CSCLotto will not be liable for any loss or damage caused by a distributed denial-of-service attack, viruses or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of this Website, the Services, or to your downloading of any material posted on it, or on any website linked to it.</p>

<p><h4>Intellectual Property, Software and Content</h4></p>

<p>The intellectual property rights in all software and content (including photographic images) made available to you on or through this Website remains the property of CSCLotto or its licensors and are protected by copyright laws and treaties around the world. All such rights are reserved by CSCLotto and its licensors. You may store, print and display the content supplied solely for your own personal use. You are not permitted to publish, manipulate, distribute or otherwise reproduce, in any format, any of the content or copies of the content supplied to you or which appears on this Website nor may you use any such content in connection with any business or commercial enterprise. Your access to, and use of, our content is at your sole risk.</p>

<p><h4>Linking to this Website</h4></p>

<p>You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it, but you must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists. You must not establish a link from any website that is not owned by you. This Website must not be framed on any other site, nor may you create a link to any part of this Website other than the home page. We reserve the right to withdraw linking permission without notice in our sole discretion.</p>

<p><h4>Third Party Content Disclaimer</h4></p>

<p>In using our Services, you may view content or utilise services provided by third parties, including links to web pages and services of such parties (“Third Party Content”). We do not control, endorse or adopt any Third Party Content and will have no responsibility for Third Party Content including, without limitation, material that may be misleading, incomplete, erroneous, offensive, indecent or otherwise objectionable in your jurisdiction.</p>

<p>Further, your dealings or correspondence with such third parties are solely between you and any such third parties. We are not responsible or liable for any loss or damage of any sort whatsoever incurred as a result of any such dealings and you understand that your use of Third Party Content, and your interactions with third parties, is at your own risk.</p>

<p>Any third party trademarks or trade names featured on this Website are owned by the respective trademark owners. Where a trademark or brand name is referred to it is used solely to describe or identify the products and services and is in no way an assertion that such products or services are endorsed by or connected to CSCLotto.</p>

<p><h4>Disclaimer of Liability</h4></p>

<p>CSCLotto PROVIDES NO GUARANTEE AS TO THE PERFORMANCE OR THE UNINTERRUPTED AVAILABILITY OF THE WEBSITE OR THE SERVICES. THE WEBSITE AND SERVICES ARE PROVIDED ON AN “AS IS,” “AS AVAILABLE” BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. CSCLotto DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT AS TO THE WEBSITE AND THE INFORMATION, CONTENT AND MATERIALS CONTAINED THEREIN. CSCLotto DOES NOT REPRESENT OR WARRANT THAT THE SERVICES OR THE WEBSITE ARE ACCURATE, COMPLETE, RELIABLE, CURRENT OR ERROR-FREE. WHILE CSCLotto ATTEMPTS TO MAKE YOUR ACCESS AND USE OF THE WEBSITE SAFE, CSCLotto CANNOT AND DOES NOT REPRESENT OR WARRANT THAT THE WEBSITE OR ITS SERVER(S) ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS; THEREFORE, YOU SHOULD USE INDUSTRY RECOGNIZED SOFTWARE TO DETECT AND DISINFECT VIRUSES FROM ANY DOWNLOAD.</p>

<p>Some jurisdictions do not allow the disclaimer of implied terms in contracts with consumers, so some or all of the disclaimers in this section may not apply to you.</p>

<p><h4>Indemnity</h4></p>

<p>You agree to indemnify, defend and hold harmless CSCLotto, its directors, officers, employees, consultants, agents, and affiliates, from any and all third party claims, liability, damages and costs (including, but not limited to, legal fees) arising from your use this Website, the Services, or your breach of the Terms of Service.</p>

<p><h4>Variation, Suspension or Termination</h4></p>

<p>CSCLotto shall have the right, in its sole discretion, at any time and without notice, to amend, remove or vary the Services and/or any page of this Website. Further, CSCLotto reserves the right, without notice and in its sole discretion, to terminate or suspend your right to use the Website and/or the Services, and to block or prevent your future access to, and use of, the Website and/or the Services.</p>

<p><h4>Invalidity</h4></p>

<p>If any provision of these Terms shall be declared by any court of competent jurisdiction to be illegal, void or unenforceable, all other provisions of these Terms of Service shall not be affected and shall remain in full force and effect.</p>

<p><h4>Waiver</h4></p>

<p>If you breach these Terms and we take no action, we will still be entitled to use our rights and remedies in any other situation where you breach these Terms of Service.</p>

<p><h4>Entire Agreement</h4></p>

<p>The above Terms of Service constitute the entire agreement between you and us and will supersede any and all prior or contemporaneous agreements between you and CSCLotto.</p>

<p><h4>Contact Us</h4></p>

<p>If you have any questions or concerns, please contact us at <a target="_blank" href="mailto:support@casinocoin.org">support@casinocoin.org</a>.</p>
<br />
<p><b>This Terms of Service has been revised as of November 20, 2018</b></p>


	</div>
</div>
<?php
	
	include("footer.php");
?>
