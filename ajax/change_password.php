<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

$session->editAccount($_POST['curr_password'],$_POST['new_password'],$_POST['repeat_new_password'],$session->userinfo['email']);

if($form->num_errors>0){
	foreach($form->errors as $error){
		echo $error."<br>";
	}
}else{
	$text_body = "We've changed your password for you at your request. If you did not make this change, please contact support.\n\n";
	$html_body = "<h2>Password Changed</h2>We've changed your password for you at your request. If you did not make this change, please contact support.<br><br>";
	$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Your Password Has Been Changed", $html_body, $text_body);
}
