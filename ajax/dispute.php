<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$response = array();
	$subject = isset($_POST['subject']) ? $_POST['subject'] : null;
	$message = isset($_POST['message']) ? $_POST['message'] : null;
	$from_name = isset($_POST['from_name']) ? $_POST['from_name'] : null;
	$from_email = isset($_POST['from_email']) ? $_POST['from_email'] : null;
	$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : null;
	$dispute_id = isset($_POST['dispute_id']) ? $_POST['dispute_id'] : null;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	if($_POST['action'] == 'submit'){
		$response['errors'] = "";
		if($subject == null){
			$response['errors'] .= "You must enter a subject.<br>";
		}
		if($message == null){
			$response['errors'] .= "You must enter a message.<br>";
		}
		if($user_id == -1){
			if($from_email == null){
				$response['errors'] .= "You must enter a your email address.<br>";
			}else{
				// see if we have an account tied to this email address\
				$q = "SELECT * from `users` WHERE `email` = %s";
				$matched_user = $db->queryOneRow($q, array($from_email));
				if($matched_user['id']){
					$user_id = $matched_user['id'];
				}else{
					$response['errors'] .= "The email address you entered does not match any registered user.<br>Please enter the email address you used to create your account.<br>";
				}
			}
		}
		if($response['errors'] == ""){
			$token = sha1('dispute'.$user_id.$now);
			$q = "INSERT INTO `disputes` (`subject`,`created_on`,`is_resolved`,`user_id`,`resolution`,`token`) VALUES (%s, %s, 0, %i, NULL, %s)";
			$id = $db->queryInsert($q, array($subject, $now, $user_id, $token));
			
			$q = "INSERT INTO `dispute_messages` (`dispute_id`,`user_id`,`message`,`created_on`) VALUES (%i, %i, %s, %s)";
			$message_id = $db->queryInsert($q, array($id, $user_id, $message, $now));
			
			$mailer->send_email($from_email, "CSCLotto Dispute Submitted", "Your dispute has been submitted. An CSCLotto representative will respond to your dispute shortly.");
			
			$response['success'] = true;
		}else{
			$response['success'] = false;
		}
	}
	
	if($_POST['action'] == 'send_response'){
		$response['errors'] = "";
		if($message == null){
			$response['errors'] .= "You must enter a message.<br>";
		}
		
		$token = isset($_POST['token']) ? $_POST['token'] : null;
		$q = "SELECT * FROM `disputes` WHERE `id`=%i AND `token`=%s";
		$dispute = $db->queryOneRow($q, array($dispute_id, $token));
		if(empty($dispute)){
			$response['errors'] .= "Invalid dispute.<br>";
		}
		if($response['errors'] == ""){
			$q = "INSERT INTO `dispute_messages` (`dispute_id`,`user_id`,`message`,`created_on`) VALUES (%i, %i, %s, %s)";
			$message_id = $db->queryInsert($q, array($dispute_id, $session->userinfo['id'], $message, $now));
			
			$response['success'] = true;
		}else{
			$response['success'] = false;
		}
	}
	
	echo json_encode($response);
