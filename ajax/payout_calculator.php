<?php
include $_SERVER['DOCUMENT_ROOT']."/config.php";

if(isset($_POST['ticket_type']) && isset($_POST['sb']) && isset($_POST['number']) && isset($_POST['amount'])){
	
	//$payout = $db->query("SELECT * FROM `payout_scheme` ps JOIN `payout_scheme_detail`psd ON ps.id=psd.scheme_id WHERE ps.name='".$_POST['ticket_type']."'");
	$q = "SELECT * FROM `payout_scheme` ps JOIN `payout_scheme_detail`psd ON ps.id=psd.scheme_id WHERE ps.name=%s";
	$payout = $db->query($q, array($_POST['ticket_type']));

	for($x=0;$x<count($payout);$x++){
		if($payout[$x]['ball_count']==strlen($_POST['number'])){
			$_POST['amount'] = str_replace("$", "", $_POST['amount']);
			$is_boxed = $_POST['sb'] == "S" ? 0 : 1;
			
			$projected_payout = $payout[$x]['payout_rate'] * $core->get_pay_factor($_POST['number'],$_POST['amount'], $is_boxed);
			
			echo "$".money_format('%i', $projected_payout);
		}
	}
}
