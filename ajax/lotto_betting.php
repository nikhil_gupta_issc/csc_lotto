<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	date_default_timezone_set('America/New_York');

	//Used to remember that the user would like to auto hide inactive games or vice versa
	if(isset($_POST['action']) && $_POST['action'] == "show_inactive"){
		$q = "UPDATE customers SET show_inactive_games= %i WHERE user_id= %i";
		echo $db->query($q, array($_POST['value'], $session->userinfo['id']));
	}

	//This is used to return a json variable with a list of game_id's that should be inactive
	if(isset($_POST['action']) && $_POST['action'] == "get_inactive_games"){
		$return_array = array();
		
		$date = new datetime('now');
		$date_str = $date->format("H:i:s");
		$house = $db->query("SELECT h.id FROM lotto_house h WHERE web_cutoff_time<='".$date_str."'");
		
		for($x=0;$x<count($house);$x++){
			array_push($return_array, $house[$x]["id"]);
		}
		
		echo implode(",", $return_array);
	}

        //This is used to return a json variable with a list of game_id's that should be inactive
	if(isset($_POST['action']) && $_POST['action'] == "get_active_games"){
		$return_array = array();
		
		$now_dt = new datetime('now');
		$today = $now_dt->format("Y-m-d");
		$houses = $db->query("SELECT id, name, short_name, web_cutoff_time FROM lotto_house");
		
		for($x=0; $x<count($houses); $x++){
		$cut_str = $today." ".$houses[$x]['web_cutoff_time'];
			$cutoff_dt = new DateTime($cut_str);
			
			if($cutoff_dt > $now_dt){
				$houses[$x]['time_remaining'] = $core->format_interval($now_dt->diff($cutoff_dt), false, false, true);
			}else{
				$houses[$x]['time_remaining'] = 'Closed';
			}
		}
		
		echo json_encode($houses);
	}
	
	if(isset($_POST['action']) && $_POST['action'] == "schedule_advance_play"){
		$errors = "";
		
		$balances = $core->check_balance();
		if($balances['available_balance'] < $_POST['total']){
			$errors .= "You do not have sufficient funds!";
		}

		//Start with error handling
		if($_POST['bet_amount'] == ""){
			$errors .= "You must enter a bet amount<br>";
		}elseif(!is_numeric($_POST['bet_amount'])){
			$errors .= "Invalid bet amount<br>";			
		}
		
		if($_POST['num_bets'] == ""){
			$errors .= "You must enter the number of bets<br>";
		}elseif(!is_numeric($_POST['num_bets']) || $_POST['num_bets'] > 7 || $_POST['num_bets'] < 1){
			$errors .= "Invalid number of bets (7 max)<br>";			
		}
		
		if($_POST['pick'] == ""){
			$errors .= "You must enter your pick to bet on<br>";
		}elseif(!is_numeric($_POST['pick'])){
			$errors .= "Invalid pick to bet on<br>";			
		}
		if($errors == ""){
			$now = new DateTime('now');
			$now_str = $now->format("Y-m-d H:i:s");
			
			$advance_play_trans_id = $core->make_customer_transaction(-$_POST['total'], 44, "Advance Play Placeholder");
			
			if(!is_numeric($advance_play_trans_id)){
				$success = "false";
				$errors = $advance_play_trans_id;
				print_r(json_encode(array("success" => $success, "errors" => $errors)));
				die();
			}
			
			// see if pick is different than what is stored in db
			$q = "SELECT * FROM `lotto_bet_favorites` WHERE `id` = ".$_POST['fav_bet_id'];
			$fav_bet_info = $db->queryOneRow($q);
			if($fav_bet_info['pick'] != $_POST['pick']){
				// if pick is different, create new fav bet with the new pick
				$q = "INSERT INTO `lotto_bet_favorites` (`id`, `user_id`, `game_id`, `house_id`, `pick`, `is_boxed`, `name`, `call_in_audio_name`, `call_in_option_number`) VALUES (NULL, '".$fav_bet_info['user_id']."', '".$fav_bet_info['game_id']."', '".$fav_bet_info['house_id']."', '".$_POST['pick']."', '".$fav_bet_info['is_boxed']."', '".$fav_bet_info['name']."', NULL, '1');";
				$fav_bet_id = $db->queryInsert($q);
			}else{
				$fav_bet_id = $_POST['fav_bet_id'];
			}
			
			$q = "INSERT INTO `scheduled_advance_play` (`fav_bet_id`, `transaction_id`,`num_bets`,`start_date`,`bets_remaining`,`bet_amount`) VALUES (%i,%i,%i,%s,%i,%f)";
			$scheduled_id = $db->queryInsert($q, array($fav_bet_id, $advance_play_trans_id, $_POST['num_bets'], $now_str, $_POST['num_bets'], $_POST['bet_amount']));
			
			$core->create_scheduled_ticket($scheduled_id);
			
			$success = "true";
		}else{
			$success = "false";
		}
		
		echo json_encode(array("success" => $success, "errors" => $errors));
	}


        if(isset($_POST['action']) && $_POST['action'] == "get_lotto_payout"){
             
              $t_bets_details = json_decode($_POST['lotto_bets'],true); 
           
              if(!empty($t_bets_details)){
                   $t_array_keys = array_keys($t_bets_details);
                   $i_pick = strlen($t_bets_details[$t_array_keys[0]]['pick']);

                   $q = "SELECT id,is_disabled,house_id FROM lotto_game where house_id IN (".implode(',',$t_array_keys).") and number_of_balls = '".$i_pick."'";
                   unset($t_array_keys);
                   $t_house_details = $db->query($q);
                   $t_house_data = array();
                   if(!empty($t_house_details)){
                       foreach($t_house_details as $t_houses){
                           $t_house_data[$t_houses['house_id']] = $t_houses['is_disabled'];
                       }
                   }

                   unset($t_house_details);

                   $q = "SELECT payout_rate FROM payout_scheme_detail WHERE scheme_id=1 AND ball_count=".$i_pick;
		   $payout_info = $db->queryOneRow($q);
                    
                   $t_bets_result = array();
                   $i = 0;
                   $t_bet_tyes = array("0"=>"S","1"=>"B");
                   foreach($t_bets_details as $i_key=>$t_value){

                         $t_bets_result[$i]['pick'] = $t_value['pick'];
                         //$t_bets_result[$i]['bet_amount'] = $t_value['bet_amount'];
                         $t_bets_result[$i]['game_name'] = $t_value['game_name'];
                         $t_bets_result[$i]['house_id'] = $i_key;
                         $t_bets_result[$i]['house_disabled'] = ((!isset($t_house_data[$i_key])) ? 1 : $t_house_data[$i_key]);

                         if($t_value['bet_type'] == 2){
                               $t_bets_result[$i+1]['pick'] = $t_value['pick'];
                              // $t_bets_result[$i+1]['bet_amount'] = $t_value['bet_amount'];
                               $t_bets_result[$i+1]['game_name'] = $t_value['game_name'];
                               $t_bets_result[$i+1]['house_id'] = $i_key;
                               $t_bets_result[$i+1]['house_disabled'] = ((!isset($t_house_data[$i_key])) ? 1 : $t_house_data[$i_key]);

                               for($j=0;$j<=1;$j++){
                                   $t_bets_result[$i+$j]['bet_amount'] = (($j == 0) ? $t_value['s_amount'] : $t_value['b_amount']);
		                   $pay_factor = $core->get_pay_factor($t_value['pick'], $t_bets_result[$i+$j]['bet_amount'], $j);
				   $t_bets_result[$i+$j]['payout'] = number_format(($payout_info['payout_rate'] * $pay_factor * 1));
		                   $t_bets_result[$i+$j]['bet_type'] = $t_bet_tyes[$j];                                   
                               }
                            $i = $i+2;
		               
                         } else {
                               $t_bets_result[$i]['bet_amount'] = (($t_value['bet_type'] == 0) ? $t_value['s_amount'] : $t_value['b_amount']);
                               $pay_factor = $core->get_pay_factor($t_value['pick'], $t_bets_result[$i]['bet_amount'], $t_value['bet_type']);
                               $t_bets_result[$i]['payout'] = number_format(($payout_info['payout_rate'] * $pay_factor * 1));                               
		               $t_bets_result[$i]['bet_type'] = $t_bet_tyes[$t_value['bet_type']];
                               $i++;
                         }
                   }
                  unset($t_bet_types,$t_bet_details,$i_key,$t_value);
   
              }
              echo json_encode($t_bets_result);
	}
	
	if(isset($_POST['action']) && $_POST['action'] == "add_fav_bet"){
		$date = new datetime('now');
		$date_str = $date->format("H:i:s");

		// determine game name from house and number of digits in pick
		$q = "SELECT * FROM `lotto_game` WHERE `id` = %i";
		$fav_game_info = $db->queryOneRow($q, array($_POST['fav_game_id']));
		$fav_game_name = $fav_game_info['name'];
		
		// create fav bet name
		$fav_name = $fav_game_name." - ".implode("-", str_split($_POST['fav_pick']));
		
		// get current highest favorite call-in number
		$q = "SELECT call_in_option_number FROM `lotto_bet_favorites` WHERE `user_id` = %i ORDER BY `call_in_option_number` DESC LIMIT 1";
		$favorites = $db->queryOneRow($q, array($_SESSION['caller_info']['id']));
		$fav_num = $favorites['call_in_option_number'] + 1;
		
		// save new favorite bet to db
		$q = "INSERT INTO `lotto_bet_favorites` (`id`, `user_id`, `game_id`, `house_id`, `pick`, `is_boxed`, `name`, `call_in_audio_name`, `call_in_option_number`) VALUES (NULL, %i, %i, %i, %i, %i, %s, NULL, %i)";
		$new_id = $db->queryInsert($q, array($session->userinfo['id'], $_POST['fav_game_id'], $fav_game_info['house_id'], $_POST['fav_pick'], $_POST['fav_boxed'], $fav_name, $fav_num));
		
		if($new_id > 0){
			echo json_encode(array("success" => 'true'));
		}else{
			echo json_encode(array("success" => 'false'));
		}
	}
	
	if(isset($_POST['action']) && $_POST['action'] == "remove_fav_bet"){
		$q = "DELETE FROM `lotto_bet_favorites` WHERE `id` = %i";
		$db->queryDirect($q, array($_POST['fav_bet_id']));
		echo json_encode(array("success" => 'true'));
	}
	
	if(isset($_POST['action']) && $_POST['action'] == "get_ball_count"){
		// get current highest favorite call-in number
		$q = "SELECT * FROM `lotto_game` WHERE `id` = %i";
		$game_info = $db->queryOneRow($q, array($_POST['game_id']));
		echo $game_info['number_of_balls'];
	}
	
	if(isset($_POST['action']) && $_POST['action'] == "check_valdiation"){
		$decoded = json_decode($_POST['house_id'],true);
		$total_count=count($decoded);
		for($i=0;$i<=$total_count;$i++)
		{
			 $check="SELECT id,house_id,is_disabled,number_of_balls,name,is_inactive  FROM lotto_game where house_id='".$decoded[$i]."' and number_of_balls = '".strlen($_POST['pick'])."'";
			 $find = $db->queryOneRow($check);
			 $balls_check=$find['id'];
			 if($balls_check!='' && $find['is_disabled']==1  || $balls_check=='')
			 {
				 $arry[$i]['id']=$decoded[$i];
			 }
			
		}	
			 
		$json_product =  json_encode($arry);
		echo($json_product);
		
	}
		
	if(isset($_POST['action']) && $_POST['action'] == "get_payout"){
		 $check="SELECT id,house_id,is_disabled,number_of_balls,name,is_inactive  FROM lotto_game where house_id='".$_POST['house_id']."' and number_of_balls = '".strlen($_POST['pick'])."'";
		 $find = $db->queryOneRow($check);
		 $balls_check=$find['id'];
		 if($balls_check!='' && $find['is_disabled']==1  || $balls_check=='')
		 {
	 		echo "T0`";	
 		 }
		 else
		 {
	 		echo "F0`";
		}	
		$q = "SELECT payout_rate FROM payout_scheme_detail WHERE scheme_id=1 AND ball_count=".strlen($_POST['pick']);
		$payout_info = $db->queryOneRow($q);
		$pay_factor = $core->get_pay_factor($_POST['pick'], $_POST['bet_amount'], $_POST['is_boxed']);
		$total = $payout_info['payout_rate'] * $pay_factor * 1;
		echo number_format($total);
	}

	if(isset($_POST['action']) && $_POST['action'] == "insert"){
		$number_of_bets = count($_POST['data']);
		$date = new datetime('now');
		$date_str = $date->format("Y-m-d H:i:s");
		$error_detected = false;
		
		$limits = $core->get_bet_limits();
                $total = 0;
		
		if($number_of_bets!=0){
			for($x=0;$x<$number_of_bets;$x++){
			
			 	$q = "SELECT *, g.id AS game_id FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id WHERE h.is_disabled=0 AND g.is_disabled = 0 AND h.id=%i AND g.number_of_balls=%i";
				$house = $db->query($q, array($_POST['data'][$x][1], strlen($_POST['data'][$x][0])));
				$_POST['data'][$x][1] = $house[0]['game_id'];
				$web_cutoff_time = new datetime($date->format("Y-m-d")." ".$house[0]["web_cutoff_time"]);
				
				if(isset($house[0])){
					//Validate that the game is allowed to be played at this specific time of the day and on sundays
					if($house[0]["plays_on_sunday"]==0 && $date->format("w")==0){
						//User is trying to play a game that cant be played on sunday, on a sunday
						echo $_POST['data'][$x][2]." is not active on Sundays<br>";
						$error_detected = true;
					}else{
						if($web_cutoff_time <= $date){
							//User is trying to play a game that is past the cut off time.
							echo $_POST['data'][$x][2]." is past the the cut off time for the day<br>";
							$error_detected = true;
						}
					}
					
					//Validate that the amount bet is valid per game played
					$bet_amount = ltrim($_POST['data'][$x][4], '$<span>');
					$bet_amount = rtrim($bet_amount, "</span>");
					if($house[0]["web_min_bet"]>$bet_amount || $house[0]["web_max_bet"]<$bet_amount){
						echo "You can only bet between ".number_format($house[0]["web_min_bet"])." ".CURRENCY_FORMAT." and ".number_format($house[0]["web_max_bet"])." ".CURRENCY_FORMAT." for ".$_POST['data'][$x][2]."<br>";
						$error_detected = true;
					}
							
					$running_total = $core->bet_running_total($house[0]['game_id'], $_POST['data'][$x][0]);
					
					$is_boxed = $_POST['data'][$x][3] == "S" ? 0 : 1;
					$pay_factored_bet = $core->get_pay_factor($_POST['data'][$x][0], $bet_amount, $is_boxed);
					
					if($running_total + $pay_factored_bet > $limits[strlen($_POST['data'][$x][0])]){
						if($running_total==$limits[$house[0]['number_of_balls']]){
							echo $_POST['data'][$x][0]." for ".$_POST['data'][$x][2]." is sold out.<br>";
						}else{
							if($_POST['data'][$x][3] == "S"){
								$bet_limit = $limits[strlen($_POST['data'][$x][0])]-$running_total;
								$type = "straight";
							}else{
								$bet_limit = ($limits[strlen($_POST['data'][$x][0])]-$running_total)*count($core->permutations($_POST['data'][$x][0], true));
								$type = "boxed";
							}
							echo "You can only bet up to ".(number_format($bet_limit))." ".CURRENCY_FORMAT." on ".$type." ".$_POST['data'][$x][0]." for ".$_POST['data'][$x][2]." before it is sold out.<br>";
						}
						$error_detected = true;
					}
				}else{
					echo strlen($_POST['data'][$x][0])." ball game does not exists for ".$_POST['data'][$x][2]."<br>";
					$error_detected = true;
				}
                                if(is_int($_POST['data'][$x][0])){
                                     echo $_POST['data'][$x][0]." is not a valid bet number <br>";
								$error_detected = true;
                                }
                              
                                if(!$error_detected){                                    
                                    $total += $bet_amount;
                                }
			}
                        $_POST['total']  = $total;
			
			//Verify that the user's balance is sufficient before allowing any data to write to the database
			$current_balance = $core->check_balance();
			if(isset($_POST['total']) && $_POST['total']>$current_balance['available_balance']){
				echo "The total exceeds your available balance";
				$error_detected = true;
			}
			
			if(!$error_detected){
				//Generate a ticket number
				$ticket_number = $core->generate_ticket_number();
				$expiration_date = $core->get_expiration_date();
				
				//negate the total since it is a withdraw and make the transaction
				$total = $_POST['total']*-1;
				$trans_id = $core->make_customer_transaction($total, 3, "Purchased Lotto Ticket - ".$ticket_number, $ticket_number);
				
				if(!is_numeric($trans_id)){
					echo $trans_id;
					die();
				}
				
				//Add lotto ticket before adding the transaction so you can get its ID.
				 $q = "INSERT INTO `lotto_ticket` (`ticket_number`,`purchase_date`,`number_of_bets`,`user_id`,`total`,`tender`,`is_voided`,`expiration_date`) VALUES (%s, %s, %i, %i, %f, %f, 0, %s)";
				 $ticket_number;
				$ticket_id = $db->queryInsert($q, array($ticket_number, $date_str, $number_of_bets, $session->userinfo['id'], $_POST['total'], $_POST['total'], $expiration_date));
				
				for($x=0;$x<$number_of_bets;$x++){
					//Get next draw date from the database to store in the bets table
					$q = "SELECT wn.next_draw_date FROM lotto_game_xml_link link JOIN xml_lotto_winning_numbers wn ON link.xml_game_id = wn.game_id WHERE link.game_id=%i ORDER BY wn.next_draw_date DESC LIMIT 1";
					$draw_date = $db->queryOneRow($q, array($_POST['data'][$x][1]));

					//Set up the ball string to be put into the database
					$balls = str_split($_POST['data'][$x][0]);
					$number_of_balls = count($balls);
					//DC sort($balls, SORT_NUMERIC);
					for($y=0;$y<6;$y++){
						if(!isset($balls[$y])){
							array_push($balls, "");
						}
					}
					
					//Make int boolean for boxed
					if($_POST['data'][$x][3]=="B"){
						$boxed=1;
					}else{
						$boxed=0;
					}
					
					//Parse for bet amount
					$bet_amount = ltrim($_POST['data'][$x][4], '$<span>');
					$bet_amount = rtrim($bet_amount, "</span>");
					
					//Calculate the pay factor. Straight will be the bet amount. Boxed is the bet amount divided by the factorial of the number of balls in the bet
					$pay_factor = $core->get_pay_factor($_POST['data'][$x][0], $bet_amount, $boxed);

					$q = "INSERT INTO `lotto_bet` (`ticket_id`,`game_id`,`ball_1`,`ball_2`,`ball_3`,`ball_4`, `ball_5`, `ball_6`, `ball_string`, `is_boxed`, `bet_amount`, `pay_factor`, `is_processed`, `draw_date`, `payout_scheme_id`, `user_session_id`) VALUES (%i,%i,%s,%s,%s,%s,%s,%s,%s,%i,%f,%s,0,%s,1,%i)";
					$db->queryInsert($q, array($ticket_id, $_POST['data'][$x][1], $balls[0], $balls[1], $balls[2], $balls[3], $balls[4], $balls[5], $_POST['data'][$x][0], $boxed, $bet_amount, $pay_factor, $draw_date['next_draw_date'], $_SESSION['user_session_id'])); 
					
				}
			}
		}else{
			echo "You must add bets to purchase";
		}
	}
