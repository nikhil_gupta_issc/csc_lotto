<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'cron_lotto_results';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'clr.draw_date',
			'dt' => 'draw_date',
			'field' => 'draw_date',
			'as' => 'draw_date',
			'formatter' => function( $d, $row ) {
				$draw_dt = new DateTime($d);
				$draw_str = $draw_dt->format("m/d/Y");
				//return $draw_str;
				return $d." ".$row['drawing_time'];
			}
		),
		array(
			'db' => 'lh.drawing_time',
			'dt' => 'drawing_time',
			'field' => 'drawing_time',
			'as' => 'drawing_time',
			'formatter' => function( $d, $row ) {
			
				return $d;
			}
		),
		
		array(
			'db' => 'lh.short_name',
			'dt' => 'house_short_name',
			'field' => 'house_short_name',
			'as' => 'house_short_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		
		
		array(
			'db' => 'lh.name',
			'dt' => 'house_name',
			'field' => 'house_name',
			'as' => 'house_name',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'clr.ball_2_draw',
			'dt' => 'ball_2_draw',
			'field' => 'ball_2_draw',
			'as' => 'ball_2_draw',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'clr.ball_3_draw',
			'dt' => 'ball_3_draw',
			'field' => 'ball_3_draw',
			'as' => 'ball_3_draw',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'clr.ball_4_draw',
			'dt' => 'ball_4_draw',
			'field' => 'ball_4_draw',
			'as' => 'ball_4_draw',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'clr.ball_5_draw',
			'dt' => 'ball_5_draw',
			'field' => 'ball_5_draw',
			'as' => 'ball_5_draw',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'clr.ball_6_draw',
			'dt' => 'ball_6_draw',
			'field' => 'ball_6_draw',
			'as' => 'ball_6_draw',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	$join_query = "FROM `cron_lotto_results` AS `clr` JOIN `lotto_house` AS `lh` ON `clr`.`house_id`=`lh`.`id`";
	
	//$extra_where = "(xlwn.is_override = '0') OR (xlwn.is_override = '1' AND lh.web_cutoff_time <= '".date("H:i:s", time())."' AND xlwn.draw_date <= '".date("Y-m-d", time())."')";

         if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`draw_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `draw_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`draw_date` <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND `draw_date` <= '".$_GET['filter_date_to']."'";
		}
	}

	
	$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	// require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
        $_GET['order'][0]['column'] = 0;
        $_GET['order'][0]['dir'] = 'desc';
	
	//require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
        $_GET['order'][1]['column'] = 1;
        $_GET['order'][1]['dir'] = 'desc';
	
        $_GET['server_order'] = 1;
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
