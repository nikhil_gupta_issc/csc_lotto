<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_bet';
	
	// Table's primary key
	$primary_key = 'bet_id';
	
	// Ticket id
	$ticket_id = $_GET['ticket_id'];
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 't.ticket_id',
			'dt' => 'DT_RowId',
			'field' => 'bet_id',
			'as' => 'bet_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'g.name',
			'dt'        => 'game_id',
			'field' => 'game_id',
			'as' => 'game_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.ball_string',
			'dt'        => 'ball_string',
			'field' => 'ball_string',
			'as' => 'ball_string',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.is_boxed',
			'dt'        => 'is_boxed',
			'field' => 'is_boxed',
			'as' => 'is_boxed',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "S" : "B";
			}
		),
		array(
			'db'        => 'b.is_boxed',
			'dt'        => 'bet_type',
			'field' => 'bet_type',
			'as' => 'bet_type',
			'formatter' => function( $d, $row ) {
				return $d == "0" ? "Straight" : "Boxed";
			}
		),
		array(
			'db'        => 'b.bet_amount',
			'dt'        => 'bet_amount',
			'field' => 'bet_amount',
			'as' => 'bet_amount',
			'formatter' => function( $d, $row ) {
                           
//"Won - ".number_format($row['payout_rate']*$row['pay_factor']) . " ". CURRENCY_FORMAT;

				return "".number_format($d). " CSC";
			}
		),
		array(
			'db'        => 'b.is_processed',
			'dt'        => 'is_processed',
			'field' => 'is_processed',
			'as' => 'is_processed',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'psd.payout_rate',
			'dt'        => 'payout_rate',
			'field' => 'payout_rate',
			'as' => 'payout_rate',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.pay_factor',
			'dt'        => 'pay_factor',
			'field' => 'pay_factor',
			'as' => 'pay_factor',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 't.purchase_date',
			'dt'        => 'purchase_date',
			'field' => 'purchase_date',
			'as' => 'purchase_date',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				return $dt->format('m/d/Y h:iA');
			}
		),
		array(
			'db'        => 'b.is_winner',
			'dt'        => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				if($row['is_processed'] == "0"){
					return "Pending";
				}else{
					if($d == "0"){
						return "Lost";
					}else{
						return "Won - ".number_format($row['payout_rate']*$row['pay_factor']) . " ". CURRENCY_FORMAT;
					}
				}
			}
		),
		array(
			'db'        => 'lp.number_of_points',
			'dt'        => 'loyality_points',
			'field' => 'loyality_points',
			'as' => 'loyality_points',
			'formatter' => function( $d, $row ) {
				return number_format($d/$row['number_of_bets']);
			}
		),
		array(
			'db'        => 'b.bet_amount',
			'dt'        => 'payout',
			'field' => 'payout',
			'as' => 'payout',
			'formatter' => function( $d, $row ) {
				return "".number_format($row['payout_rate'] * $row['pay_factor']) . " ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 't.ticket_number',
			'dt'        => 'ticket_number',
			'field' => 'ticket_number',
			'as' => 'ticket_number',
			'formatter' => function( $d, $row ) {
				return "<a href='/my_account.php?trans_search=".$d."&tab=history'>".$d."</a>";
			}
		),
		array(
			'db'        => 'b.processed_date_time',
			'dt'        => 'draw_time',
			'field' => 'draw_time',
			'as' => 'draw_time',
			'formatter' => function( $d, $row ) {
				if($d != "0000-00-00 00:00:00"){
					$dt = new DateTime($d);
					return $dt->format('m/d/Y h:iA');
				}
			}
		),
		array(
			'db'        => 'b.bet_amount',
			'dt'        => 'payout',
			'field' => 'payout',
			'as' => 'payout',
			'formatter' => function( $d, $row ) {
				return "".number_format($row['payout_rate'] * $row['pay_factor'])." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 't.number_of_bets',
			'dt'        => 'number_of_bets',
			'field' => 'number_of_bets',
			'as' => 'number_of_bets',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'b.winning_draw',
			'dt'        => 'winning_draw',
			'field' => 'winning_draw',
			'as' => 'winning_draw',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	$join_query = "FROM `lotto_bet` AS `b` LEFT JOIN `lotto_ticket` AS `t` ON `b`.`ticket_id`=`t`.`ticket_id` LEFT JOIN `lotto_game` AS `g` ON `b`.`game_id`=`g`.`id` LEFT JOIN `lotto_house` AS `h` ON `g`.`house_id`=`h`.`id` LEFT JOIN `payout_scheme_detail` AS `psd` ON `psd`.`scheme_id`=`b`.`payout_scheme_id` AND `g`.`number_of_balls`=`psd`.`ball_count` LEFT JOIN `customer_transaction` AS `ct` ON `ct`.`ticket_number`=`t`.`ticket_number` LEFT JOIN `loyalty_points` as `lp` ON `ct`.`transaction_id` = `lp`.`transaction_id`";
	
	$extra_where = "t.user_id = '".$session->userinfo['id']."'";
	
       if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`purchase_date` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `purchase_date` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`purchase_date` <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND `purchase_date` <= '".$_GET['filter_date_to']."'";
		}
	}

	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
