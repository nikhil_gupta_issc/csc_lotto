<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'customer_transaction';
	
	// Table's primary key
	$primary_key = 'transaction_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'ct.transaction_id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'ct.transaction_id',
			'dt' => 'id',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ct.transaction_date',
			'dt'        => 'trans_date',
			'field' => 'trans_date',
			'as' => 'trans_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 'ct.transaction_details',
			'dt'        => 'details',
			'field' => 'details',
			'as' => 'details',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'tt.name',
			'dt'        => 'type',
			'field' => 'type',
			'as' => 'type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ct.amount',
			'dt'        => 'amount',
			'field' => 'amount',
			'as' => 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'lp.number_of_points',
			'dt'        => 'loyality_points',
			'field' => 'loyality_points',
			'as' => 'loyality_points',
			'formatter' => function( $d, $row ) {
				return number_format($d,2);
			}
		),
		array(
			'db'        => 'ct.initial_balance',
			'dt'        => 'initial_balance',
			'field' => 'initial_balance',
			'as' => 'initial_balance',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'ct.balance',
			'dt'        => 'balance',
			'field' => 'balance',
			'as' => 'balance',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		),
		array(
			 'db' => 'ct.transaction_type_id',
                        'dt' => 'transaction_type_id',
                        'field' => 'transaction_type_id',
                        'as' => 'transaction_type_id',
                        'formatter' => function( $d, $row ) {
                                return $d;
                        }
			)
	);
	
	$join_query = "FROM `customer_transaction` AS `ct` LEFT JOIN `transaction_types` AS `tt` ON `ct`.`transaction_type_id`=`tt`.`id` LEFT JOIN `loyalty_points` as `lp` ON `ct`.`transaction_id` = `lp`.`transaction_id`";

        if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "trans_date >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND trans_date >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "trans_date <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND trans_date <= '".$_GET['filter_date_to']."'";
		}
	}

	
	$extra_where = "ct.user_id = '".$session->userinfo['id']."'";
        
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
