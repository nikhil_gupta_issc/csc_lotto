<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'lotto_bet';
	
	// Table's primary key
	$primaryKey = 'bet_id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' 		=> 'bet.bet_id',
			'dt' 		=> 'DT_RowId',
			'field'		=> 'bet_id',
			'as'		=> 'bet_id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'game.name',
			'dt'        => 'game_id',
			'field' 	=> 'game_id',
			'as' 		=> 'game_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ticket.purchase_date',
			'dt'        => 'purchase_date',
			'field' 	=> 'purchase_date',
			'as' 		=> 'purchase_date',
			'formatter' => function( $d, $row ) {
				return date( 'm/d/Y h:i A', strtotime($d));
			}
		),
		array(
			'db'        => 'bet.ball_string',
			'dt'        => 'ball_string',
			'field' 	=> 'ball_string',
			'as' 		=> 'ball_string',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'bet.is_boxed',
			'dt'        => 'is_boxed',
			'field'		=> 'is_boxed',
			'as' 		=> 'is_boxed',
			'formatter' => function( $d, $row ) {
				if($d==1){
					return "Yes";
				}else{
					return "No";
				}
			}
		),
		array(
			'db'        => 'bet.bet_amount',
			'dt'        => 'bet_amount',
			'field' 	=> 'bet_amount',
			'as' 		=> 'bet_amount',
			'formatter' => function( $d, $row ) {
				return "$".$d;
			}
		),
		array(
			'db'        => 'bet.is_processed',
			'dt'        => 'is_processed',
			'field' 	=> 'is_processed',
			'as' 		=> 'is_processed',
			'formatter' => function( $d, $row ) {
				if($d==1){
					return "Yes";
				}else{
					return "No";
				}
			}
		)
	);

	$join_query = "FROM `lotto_bet` AS `bet` JOIN `lotto_game` AS `game` ON `bet`.`game_id`=`game`.`id` JOIN `lotto_ticket` AS `ticket` ON `bet`.`ticket_id`=`ticket`.`ticket_id`";
	
	$extra_where = "`ticket`.`user_id`=".$session->userinfo['id'];
	
	$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primaryKey, $columns, $extra_where, $group_by, $join_query )
	);