<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'rapidballs_drawing';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'rb.id',
			'dt' => 'id',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'rb.datetime_drawn',
			'dt' => 'datetime_drawn',
			'field' => 'datetime_drawn',
			'as' => 'datetime_drawn',
			'formatter' => function( $d, $row ) {
				$draw_dt = new DateTime($d);
				$draw_str = $draw_dt->format("Y-m-d h:i A");
				return $draw_str;
			}
		),
		array(
			'db' => 'rb.ball_1',
			'dt' => 'ball_1',
			'field' => 'ball_1',
			'as' => 'ball_1',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rb.ball_2',
			'dt' => 'ball_2',
			'field' => 'ball_2',
			'as' => 'ball_2',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rb.ball_3',
			'dt' => 'ball_3',
			'field' => 'ball_3',
			'as' => 'ball_3',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rb.ball_4',
			'dt' => 'ball_4',
			'field' => 'ball_4',
			'as' => 'ball_4',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rb.ball_5',
			'dt' => 'ball_5',
			'field' => 'ball_5',
			'as' => 'ball_5',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rb.ball_6',
			'dt' => 'ball_6',
			'field' => 'ball_6',
			'as' => 'ball_6',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rb.ball_6',
			'dt' => 'ball_string',
			'field' => 'ball_string',
			'as' => 'ball_string',
			'formatter' => function( $d, $row ) {
				return $row['ball_1']."-".$row['ball_2']."-".$row['ball_3']."-".$row['ball_4']."-".$row['ball_5']."-".$row['ball_6'];
			}
		),
		array(
			'db' => 'rb.ball_6',
			'dt' => 'drawing',
			'field' => 'drawing',
			'as' => 'drawing',
			'formatter' => function( $d, $row ) {
				return '
				<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$row['ball_1'].'"></span>
					<span class="ball ball-sm ball-'.$row['ball_2'].'"></span>
					<span class="ball ball-sm ball-'.$row['ball_3'].'"></span>
					<span class="ball ball-sm ball-'.$row['ball_4'].'"></span>
					<span class="ball ball-sm ball-'.$row['ball_5'].'"></span>
					<span class="ball ball-sm ball-'.$row['ball_6'].'"></span>
				</div>
				';
			}
		),
		array(
			'db' => 'rb.total',
			'dt' => 'total',
			'field' => 'total',
			'as' => 'total',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'rb.lucky',
			'dt' => 'lucky',
			'field' => 'lucky',
			'as' => 'lucky',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'rb.seed_a',
			'dt' => 'seed_a',
			'field' => 'seed_a',
			'as' => 'seed_a',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'rb.seed_b',
			'dt' => 'seed_b',
			'field' => 'seed_b',
			'as' => 'seed_b',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'rb.hash',
			'dt' => 'hash',
			'field' => 'hash',
			'as' => 'hash',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		)
	);

	$join_query = "FROM `rapidballs_drawing` AS rb";
	
	//$extra_where = "(xlwn.is_override = '0') OR (xlwn.is_override = '1' AND lh.web_cutoff_time <= '".date("H:i:s", time())."' AND xlwn.draw_date <= '".date("Y-m-d", time())."')";
	
	$group_by = "";
 if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`datetime_drawn` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `datetime_drawn` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`datetime_drawn` <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND `datetime_drawn` <= '".$_GET['filter_date_to']."'";
		}
	}
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
