<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'rapidballs_ticket';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'rb.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'rb.id',
			'dt'        => 'id',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rt.bet_on',
			'dt'        => 'bet_on',
			'field' => 'bet_on',
			'as' => 'bet_on',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rd.id',
			'dt'        => 'draw_id',
			'field' => 'draw_id',
			'as' => 'draw_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rb.is_processed',
			'dt'        => 'is_processed',
			'field' => 'is_processed',
			'as' => 'is_processed',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db' => 'rd.datetime_drawn',
			'dt' => 'datetime_drawn',
			'field' => 'datetime_drawn',
			'as' => 'datetime_drawn',
			'formatter' => function( $d, $row ) {
				if($row['is_processed'] == 1){
					$draw_dt = new DateTime($d);
					$draw_str = $draw_dt->format("Y-m-d h:i A");
					return $draw_str;
				}else{
					return "Pending";
				}
			}
		),
		array(
			'db' => 'rd.ball_1',
			'dt' => 'ball_1',
			'field' => 'ball_1',
			'as' => 'ball_1',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rd.ball_2',
			'dt' => 'ball_2',
			'field' => 'ball_2',
			'as' => 'ball_2',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rd.ball_3',
			'dt' => 'ball_3',
			'field' => 'ball_3',
			'as' => 'ball_3',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rd.ball_4',
			'dt' => 'ball_4',
			'field' => 'ball_4',
			'as' => 'ball_4',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rd.ball_5',
			'dt' => 'ball_5',
			'field' => 'ball_5',
			'as' => 'ball_5',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rd.ball_6',
			'dt' => 'ball_6',
			'field' => 'ball_6',
			'as' => 'ball_6',
			'formatter' => function( $d, $row ) {
				return '<div class="ball-container" style="text-align: center; width: 90%;">
					<span class="ball ball-sm ball-'.$d.'"></span>
				</div>';
			}
		),
		array(
			'db' => 'rd.id',
			'dt' => 'drawing',
			'field' => 'drawing',
			'as' => 'drawing',
			'formatter' => function( $d, $row ) {
				return '
				<div class="ball-container">
					<span class="ball ball-sm ball-'.$row['ball_1'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-sm ball-'.$row['ball_2'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-sm ball-'.$row['ball_3'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-sm ball-'.$row['ball_4'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-sm ball-'.$row['ball_5'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-sm ball-'.$row['ball_6'].'"></span>
				</div>
				';
			}
		),
		array(
			'db' => 'rd.id',
			'dt' => 'drawing_string',
			'field' => 'drawing_string',
			'as' => 'drawing_string',
			'formatter' => function( $d, $row ) {
				return $row['ball_1']."-".$row['ball_2']."-".$row['ball_3']."-".$row['ball_4']."-".$row['ball_5']."-".$row['ball_6'];
			}
		),
		array(
			'db'        => 'rbt.description',
			'dt'        => 'type',
			'field' => 'type',
			'as' => 'type',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rb.bet_amount',
			'dt'        => 'bet_amount',
			'field' => 'bet_amount',
			'as' => 'bet_amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'lp.number_of_points',
			'dt'        => 'loyality_points',
			'field' => 'loyality_points',
			'as' => 'loyality_points',
			'formatter' => function( $d, $row ) {
				return number_format(($d/$row['number_of_bets']),3);
			}
		),
		array(
			'db'        => 'rb.winner_details',
			'dt'        => 'winner_details',
			'field' => 'winner_details',
			'as' => 'winner_details',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rd.id',
			'dt'        => 'drawing_id',
			'field' => 'drawing_id',
			'as' => 'drawing_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rb.payout',
			'dt'        => 'payout',
			'field' => 'payout',
			'as' => 'payout',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rb.is_voided',
			'dt'        => 'is_voided',
			'field' => 'is_voided',
			'as' => 'is_voided',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rt.number_of_bets',
			'dt'        => 'number_of_bets',
			'field' => 'number_of_bets',
			'as' => 'number_of_bets',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'rb.is_winner',
			'dt'        => 'status',
			'field' => 'status',
			'as' => 'status',
			'formatter' => function( $d, $row ) {
				if($row['is_voided']){
					return "Voided";
				}
				
				if($row['drawing_id']==""){
					return "Pending";
				}else{
					if($d == "0"){
						return "Lost";
					}else{
						return "Won - ".number_format($row['payout'])." ". CURRENCY_FORMAT."(".$row['winner_details'].")";
					}
				}
			}
		)
	);

	 $join_query = "FROM `rapidballs_ticket` AS `rt` JOIN `rapidballs_bet` AS `rb` ON `rt`.`id`=`rb`.`ticket_id` JOIN `rapidballs_bet_type` AS `rbt` ON `rb`.`bet_type_id`=`rbt`.`id` LEFT JOIN `rapidballs_drawing` AS `rd` ON `rd`.`id`=`rb`.`drawing_id`LEFT JOIN `customer_transaction` AS `ct` ON `ct`.`transaction_id`=`rt`.`transaction_id` LEFT JOIN `loyalty_points` as `lp` ON `ct`.`transaction_id` = `lp`.`transaction_id`";
	
	$extra_where = "rt.user_id = '".$session->userinfo['id']."'";

        if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`datetime_drawn` >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND `datetime_drawn` >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "`datetime_drawn` <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND `datetime_drawn` <= '".$_GET['filter_date_to']."'";
		}
	}


	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
