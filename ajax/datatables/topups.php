<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'phone_cards';
	
	// Table's primary key
	$primary_key = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' => 'ct.id',
			'dt' => 'DT_RowId',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db' => 'ct.id',
			'dt' => 'id',
			'field' => 'id',
			'as' => 'id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ct.pin',
			'dt'        => 'pin',
			'field' => 'pin',
			'as' => 'pin',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ct.transaction_id',
			'dt'        => 'transaction_id',
			'field' => 'transaction_id',
			'as' => 'transaction_id',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'ct.created_on',
			'dt'        => 'trans_date',
			'field' => 'trans_date',
			'as' => 'trans_date',
			'formatter' => function( $d, $row ) {
				return date("m/d/Y h:i A", strtotime($d));
			}
		),
		array(
			'db'        => 'ct.response_message',
			'dt'        => 'details',
			'field' => 'details',
			'as' => 'details',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		
		
		
	);
	
	$join_query = "FROM `phone_cards` AS `ct`";

        if(isset($_GET['filter_date_from']) && $_GET['filter_date_from'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "trans_date >= '".$_GET['filter_date_from']."'";
		}else{
			$extra_where .= " AND trans_date >= '".$_GET['filter_date_from']."'";
		}
	}
	
	if(isset($_GET['filter_date_to'])&& $_GET['filter_date_to'] != "undefined"){
		if($extra_where == ""){
			$extra_where = "trans_date <= '".$_GET['filter_date_to']."'";
		}else{
			$extra_where .= " AND trans_date <= '".$_GET['filter_date_to']."'";
		}
	}
	
	//$extra_where = "ct.user_id = '".$session->userinfo['id']."'";
	
	//$group_by = "GROUP BY `c`.`customer_id`";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primary_key, $columns, $extra_where, $group_by, $join_query )
	);
