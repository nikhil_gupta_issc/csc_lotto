<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// SQL server connection information
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASSWORD,
		'db'   => DB_NAME,
		'host' => DB_SERVER
	);
	
	// DB table to use
	$table = 'scheduled_advance_play';
	
	// Table's primary key
	$primaryKey = 'id';
	
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array(
			'db' 		=> 'sap.id',
			'dt' 		=> 'DT_RowId',
			'field'		=> 'id',
			'as'		=> 'id',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array(
			'db'        => 'sap.bet_amount',
			'dt'        => 'amount',
			'field' 	=> 'amount',
			'as' 		=> 'amount',
			'formatter' => function( $d, $row ) {
				return "".number_format($d)." ".CURRENCY_FORMAT;
			}
		),
		array(
			'db'        => 'sap.draw_date',
			'dt'        => 'next_draw_date',
			'field' 	=> 'next_draw_date',
			'as' 		=> 'next_draw_date',
			'formatter' => function( $d, $row ) {
				$dt = new DateTime($d);
				if($dt->format('Y') > 0){
					return $dt->format('m/d/Y');
				}else{
					return "Pending";
				}
			}
		),
		array(
			'db'        => 'sap.bets_remaining',
			'dt'        => 'bets_remaining',
			'field' 	=> 'bets_remaining',
			'as' 		=> 'bets_remaining',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'lg.name',
			'dt'        => 'game',
			'field'		=> 'game',
			'as' 		=> 'game',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'lbf.pick',
			'dt'        => 'pick',
			'field' 	=> 'pick',
			'as' 		=> 'pick',
			'formatter' => function( $d, $row ) {
				return $d;
			}
		),
		array(
			'db'        => 'lbf.is_boxed',
			'dt'        => 'bet_type',
			'field' 	=> 'bet_type',
			'as' 		=> 'bet_type',
			'formatter' => function( $d, $row ) {
				if($d==1){
					return "Boxed";
				}else if($d==2){
					return "Straight & Boxed";
				}
				else
				{
					return "Straight";
				}
			}
		)
	);

	$join_query = "FROM `scheduled_advance_play` AS `sap` JOIN `lotto_bet_favorites` AS `lbf` ON `lbf`.`id`=`sap`.`fav_bet_id` JOIN `lotto_game` AS `lg` ON `lg`.`id`=`lbf`.`game_id`";
	
//original 	$extra_where = "`lbf`.`user_id`=".$session->userinfo['id']." AND `sap`.`bets_remaining`>0";
		
	$extra_where = "`lbf`.`user_id`=".$session->userinfo['id']." ";
	$group_by = "";

	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( $_SERVER['DOCUMENT_ROOT'].'/lib/framework/datatables_class.php' );
	 
	echo json_encode(
		SSP::get_table_data( $_GET, $sql_details, $table, $primaryKey, $columns, $extra_where, $group_by, $join_query )
	);
