<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$response = array();

	function create_random_voucher($amount){
		global $core;
		global $db;
		
		$voucher_info['count'] = 1;
		while($voucher_info['count'] != 0){
			// generate unique code
			$code = "V".round($amount)."-".$core->generate_random_string(6);
			$q = "SELECT COUNT(*) AS count FROM `vouchers` WHERE `code` = '$code'";
			$voucher_info = $db->queryOneRow($q);
		}
		
		return $code;
	}	
	
	if($_POST['action'] == 'redeem'){
		// get item details from id
		$q = "SELECT * FROM `loyalty_items` WHERE `id` = ".$_POST['id'];
		$loyalty = $db->queryOneRow($q);
		
		// see if the user has enough points to redeem
		$q = "SELECT `loyalty_points` FROM `customers` WHERE `user_id` = ".$session->userinfo['id'];
		$point_info = $db->queryOneRow($q);
		$response['user_id'] = $point_info['loyalty_points'];
		if($point_info['loyalty_points'] >= $loyalty['point_cost']){
			if($loyalty['type'] == 'voucher'){
				$code = create_random_voucher($loyalty['value']);
				
				$now_dt = new DateTime();
				$expire_dt = new DateTime();
				$expire_dt = $expire_dt->modify('+1 Year');
				
				$q = "INSERT INTO `vouchers` (`id`, `code`, `start`, `end`, `amount`, `is_enabled`, `is_system`, `user_id`) VALUES (NULL, '".$code."', '".$now_dt->format('Y-m-d H:i:s')."', '".$expire_dt->format('Y-m-d H:i:s')."', '".$loyalty['value']."', '1', '0', '".$session->userinfo['id']."');";
				$voucher_id = $db->queryInsert($q);
				
				$response['voucher'] = $code;
				if($voucher_id > 0){
					$response['success'] = 'true';
					
					// store record of this point redemption
					$q = "INSERT INTO `loyalty_points_redemption` (`id`, `user_id`, `redeemed_date`, `number_of_points`, `item_id`, `item_type`, `details`) VALUES (NULL, '".$session->userinfo['id']."', '".$now_dt->format('Y-m-d H:i:s')."', '".$loyalty['point_cost']."', '".$loyalty['id']."', '".$loyalty['type']."', 'Voucher Code: ".$code."');";
					$id = $db->queryInsert($q);
				}else{
					$response['success'] = 'false';
				}
			}
		}else{
			$response['success'] = 'false';
			$response['error'] = 'You do not have enough points to redeem this item.';
		}
	}
	
	echo json_encode($response);