<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

if(isset($_POST['winning_numbers_list']) && isset($_POST['day'])){
	echo "<ul>";

	//Get all of the data
	if($_POST['day']=="today"){
		$date = new DateTime('now');
	}else{
		$date = new DateTime('now');
		$date->sub(date_interval_create_from_date_string('1 day'));
	}
	$date_str = $date->format("Y-m-d");
	$q = "SELECT lg.number_of_balls,lh.name,lwn.draw_numbers,lh.web_cutoff_time,lwn.is_override FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date=%s ORDER BY lh.name,lg.number_of_balls";
	$data = $db->query($q, array($date_str));

	//Initialize variable used to keep track of current houses and alternating classes
	$current_house = $data[0]['name'];
	$alternator = "even";

	//Build the list
	for($x=0;$x<count($data);$x++){
		if($_POST['day'] == "today" && $data[$x]['web_cutoff_time'] >= date("H:i:s",time())){

		}else{
			if($current_house != $data[$x]['name'] && $x!=0){
				$current_house = $data[$x]['name'];
				echo "</li>";
				echo "<li class='".$alternator."'>";
				if($alternator=="even"){
					$alternator="odd";
				}else{
					$alternator="even";
				}
				echo "<p>".$data[$x]['name']."</p>";
			}elseif($x==0){
				//Initialize the first house for listings
				$current_house = $data[$x]['name'];
				echo '<li class="odd">';
				echo "<p>".$data[0]['name']."</p>";
			}
			if($data[$x]['number_of_balls']==2){
				echo "<span class='ball'>".substr($data[$x]['draw_numbers'],-2,2)."</span>";
			}else{
				echo "<span class='ball'>".$data[$x]['draw_numbers']."</span>";
			}
		}
	}
	
	echo "</li>";
	echo "</ul>";
}