<?php
date_default_timezone_set('America/New_York');
$dt = new DateTime("now");

echo $dt->format("Y-m-d H:i:s a");