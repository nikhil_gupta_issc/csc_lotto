<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

$balance_info = $core->check_balance();

$json = array();

if($session->userinfo['user_info']['hide_balance'] == 1){
	$json['available_money'] = '<a class="balance-show" style="color:white; text-decoration: none;"title="Click to show/hide...">Hidden</a>';
	$json['current_balance'] = '<a class="balance-show" style="color:white; text-decoration: none;"title="Click to show/hide...">Hidden</a>';
	$json['bonus_hold'] = '<a class="balance-show" style="color:white; text-decoration: none;"title="Click to show/hide...">Hidden</a>';
	$json['bonus_balance'] = '<a class="balance-show" style="color:white; text-decoration: none;" title="Click to show/hide...">Hidden</a>';
	$json['total_balance'] = '<a class="balance-show" style="color:white; text-decoration: none;"title="Click to show/hide...">Hidden</a>';
}else{
	$json['available_money'] = '<a class="balance-hide" style="color:white; text-decoration: none;"title="Click to show/hide...">'.number_format($balance_info["available_balance"]).' '.CURRENCY_FORMAT.'</a>';
	$json['current_balance'] = '<a class="balance-hide" style="color:white; text-decoration: none;" title="Click to show/hide...">'.number_format($balance_info["available_balance"]-$balance_info["bonus_balance"]).' '.CURRENCY_FORMAT.'</a>';
	$json['bonus_hold'] = '<a class="balance-hide" style="color:white; text-decoration: none;" title="Click to show/hide...">'.number_format(0).' '.CURRENCY_FORMAT.'</a>';
	$json['bonus_balance'] = '<a class="balance-hide" style="color:white; text-decoration: none;" title="Click to show/hide...">'.number_format($balance_info["bonus_balance"]).' '.CURRENCY_FORMAT.'</a>';
	$json['total_balance'] = '<a id="total_csc_balance" class="balance-hide" style="color:white; text-decoration: none;" title="
	Available Balance : '.number_format($balance_info["available_balance"]-$balance_info["bonus_balance"]).' '.CURRENCY_FORMAT.' 
	Bonus Balance : '.number_format($balance_info["bonus_balance"]).' '.CURRENCY_FORMAT.'
	
	Click to show/hide...">'.number_format($balance_info["available_balance"]).' ' . CURRENCY_FORMAT . '</a>';
}
	
echo json_encode($json);
