<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	date_default_timezone_set('America/New_York');

	$q = "SELECT *, lh.name AS house_name, lh.id AS house_id FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date=CURDATE() ORDER BY lh.name, lg.number_of_balls";
	$data = $db->query($q);

	//Initialize variable used to keep track of current houses and alternating classes
	$current_house = $data[0]['house_name'];
	$alternator = "even";
	
	$house_drawings = array();
	
	//Build the list
	for($x=0; $x<count($data); $x++){
		if($data[$x]['draw_datetime'] != ""){
			$now_dt = new DateTime();
			$draw_dt = new DateTime($data[$x]['draw_datetime']);
			$time_diff = $now_dt->getTimestamp() - $draw_dt->getTimestamp();
			
			if($time_diff <= 9 && $time_diff >= 0){
				if($current_house != $data[$x]['house_name'] && $x != 0){
					$current_house = $data[$x]['house_name'];
					
					// store house name
					$house_drawings[$data[$x]['house_id']] = $data[$x]['house_name'];
				}elseif($x==0){
					//Initialize the first house for listings
					$current_house = $data[$x]['house_name'];
					
					// store house name
					$house_drawings[$data[$x]['house_id']] = "<span style='padding-right: 10px;'>".$data[$x]['house_name']."</span>";
				}
				
				if($data[$x]['number_of_balls']==2){
					$house_drawings[$data[$x]['house_id']] .= "<span class='notif_ball'>".substr($data[$x]['draw_numbers'],-2,2)."</span>";
				}else{
					$house_drawings[$data[$x]['house_id']] .=  "<span class='notif_ball'>".$data[$x]['draw_numbers']."</span>";
				}
			}
		}
	}
	
	
	if(count($house_drawings) > 0){
		echo '
		<style>
			.notif_ball {
				border-radius: 50px;
				background: purple;
				color: white;
				display: inline-block;
				padding: 1px 8px;
				margin: 3px;
			}
			.notification {
			
			 background:#595959 !important;
			background:-moz-linear-gradient(center top,#595959 0%,#1a1a1a 100%) !important;
			-pie-background:linear-gradient(center top,#595959 0%,#1a1a1a 100%) !important;
			background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#595959),color-stop(1,#1a1a1a)) !important;
			background-image:-o-linear-gradient(top,#595959 0%,#1a1a1a 100%) !important;
			background:-ms-linear-gradient(center top,#595959 0%,#1a1a1a 100%) !important;
			background:linear-gradient(center top,#595959 0%,#1a1a1a 100%) !important;
			color : #fff;
			}
		</style>';
		
		foreach($house_drawings as $draw){
			echo '<div class="notification alert alert-info alert-dismissible lotto_notification" role="alert" style="display: none;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><span style="float: left; height: 30px; display: inline-block;"><h3 style="margin: 0; padding: 3px;">Lottery Drawing:</h3></span><span style="height: 30px; display: inline-block;">'.$draw.'</span><a href="#"><h5>Hide these messages.</h5></a></span></div>';
		}
	}
