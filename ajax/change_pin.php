<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	date_default_timezone_set('America/New_York');

	$errors = $core->change_pin($_POST['password'],$_POST['new_pin'],$_POST['repeat_new_pin']);

	if($errors !== true){
		foreach($errors as $error){
			echo $error."<br>";
		}
	}else{
		$text_body = "We've changed your withdrawal PIN for you at your request. If you did not make this change, please contact support.\n\n";
		$html_body = "<h2>Withdrawal PIN Changed</h2>We've changed your withdrawal PIN for you at your request. If you did not make this change, please contact support.<br><br>";
		$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Your Withdrawal PIN Has Been Changed", $html_body, $text_body);
	}
