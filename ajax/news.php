<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	if($_POST['action'] == 'load'){
		$q = "SELECT * FROM `news_updates` WHERE `id` = ".$_POST['id'];
		$news = $db->queryOneRow($q);
		echo json_encode($news);
	}elseif($_POST['action'] == 'delete'){
		$q = "DELETE FROM `news_updates` WHERE `id`=%i;";
		$db->queryDirect($q, array($_POST["id"]));
		echo json_encode(array('success' => 'true'));
	}elseif($_POST['action'] == 'update'){
		$q = "UPDATE `news_updates` SET `title`= %s, `text`= %s, `image` = %s WHERE `id`=%i;";
		$db->queryDirect($q, array($_POST["title"], $_POST["text"], $_POST["image"], $_POST["id"]));
		echo json_encode(array('success' => 'true'));
	}elseif($_POST['action'] == 'insert'){
		$q = "INSERT INTO `news_updates`(`id`, `image`, `title`, `text`, `is_deleted`) VALUES (NULL, %s, %s, %s, '0');";
		$id = $db->queryInsert($q, array($_POST['image'], $_POST['title'], $_POST['text']));
		if($id > 0){
			echo json_encode(array('success' => 'true'));
		}else{
			echo json_encode(array('success' => 'false'));
		}
	}