<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	
	$q = "SELECT * FROM failed_redemption WHERE user_id=%i;";
	$f = $db->queryOneRow($q, array($session->userinfo['id']));

	if ($f == FALSE) { 
		$q = "INSERT INTO failed_redemption (`user_id`) VALUES (%i)";
		$db->queryInsert($q, array($session->userinfo['id']));
	}
    
	if($action=='check_user')
	{
			
		    $query = "select * from users where username = '".strtolower($_POST['username'])."'";
			$user_result=$db->queryOneRow($query);
			if($user_result > 0) // not available
			{
				echo '<div id="Error" style="color:red">Username Already Taken</div>';
			}
			else
			{
				echo '<div id="Success">Username Available</div>';
			}	
			
		
	}
  else
  {
	switch ($action){
		
		
		case 'set_limits':
			//Check last change date and see if it is valid to change
			$q = "SELECT * FROM `customers` WHERE `user_id`=%i";
			$last_changed = $db->queryOneRow($q, array($session->userinfo['id']));
			
			$result['success'] = true;
			
			// loss
			if($last_changed['daily_loss_last_changed'] != null){
				$last_changed_dt = new DateTime($last_changed['daily_loss_last_changed']);
				
				$date = new DateTime();
				$time_between = $date->diff($last_changed_dt);
				$hours = $time_between->h;
				$hours = $hours + ($time_between->days*24);
				
				if($_POST['daily_loss_limit'] != $last_changed['daily_loss_limit']){
					if($hours >= 24 || $last_changed['daily_loss_last_changed'] == null){
						$date = new DateTime();
						$now = $date->format("Y-m-d H:i:s");
						$q = "UPDATE `customers` SET `daily_loss_limit`=%d,`daily_loss_last_changed`=%s WHERE `user_id`=%i";
						$db->queryDirect($q, array($_POST['daily_loss_limit'], $now, $session->userinfo['id']));
						
						// send email to notify user.
						$text_body = "We've set a daily loss limit of $".$_POST['daily_loss_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
						If you did not make this change or it was made in error, please contact support.\n\n";
						$html_body = "<h2>Self Limits Changed</h2>We've set a daily loss limit of $".$_POST['daily_loss_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
						$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
					}else{
						$result['success'] = false;
						$result['hours'] = $hours;
						$result['errors'] .= "You can only change your loss limit once every 24 hours.<br>";
					}
				}
			}else{
				$date = new DateTime();
				$now = $date->format("Y-m-d H:i:s");
				$q = "UPDATE `customers` SET `daily_loss_limit`=%d,`daily_loss_last_changed`=%s WHERE `user_id`=%i";
				$db->queryDirect($q, array($_POST['daily_loss_limit'], $now, $session->userinfo['id']));
				
				// send email to notify user.
				$text_body = "We've set a daily loss limit of $".$_POST['daily_loss_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
				If you did not make this change or it was made in error, please contact support.\n\n";
				$html_body = "<h2>Self Limits Changed</h2>We've set a daily loss limit of $".$_POST['daily_loss_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
				$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
			}
			
			// wagers
			if($last_changed['daily_wager_last_changed'] != null){
				$last_changed_dt = new DateTime($last_changed['daily_wager_last_changed']);
				
				$date = new DateTime();
				$time_between = $date->diff($last_changed_dt);
				$hours = $time_between->h;
				$hours = $hours + ($time_between->days*24);
				
				if($_POST['daily_wager_limit'] != $last_changed['daily_wager_limit']){
					if($hours >= 24 || $last_changed['daily_wager_last_changed'] == null){
						$date = new DateTime();
						$now = $date->format("Y-m-d H:i:s");
						$q = "UPDATE `customers` SET `daily_wager_limit`=%d,`daily_wager_last_changed`=%s WHERE `user_id`=%i";
						$db->queryDirect($q, array($_POST['daily_wager_limit'], $now, $session->userinfo['id']));
						
						// send email to notify user.
						$text_body = "We've set a daily wager limit of $".$_POST['daily_wager_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
						If you did not make this change or it was made in error, please contact support.\n\n";
						$html_body = "<h2>Self Limits Changed</h2>We've set a daily wager limit of $".$_POST['daily_wager_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
						$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
					}else{
						$result['success'] = false;
						$result['hours'] = $hours;
						$result['errors'] .= "You can only change your wager limit once every 24 hours.<br>";
					}
				}
			}else{
				$date = new DateTime();
				$now = $date->format("Y-m-d H:i:s");
				$q = "UPDATE `customers` SET `daily_wager_limit`=%d,`daily_wager_last_changed`=%s WHERE `user_id`=%i";
				$db->queryDirect($q, array($_POST['daily_wager_limit'], $now, $session->userinfo['id']));
				
				// send email to notify user.
				$text_body = "We've set a daily wager limit of $".$_POST['daily_wager_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
				If you did not make this change or it was made in error, please contact support.\n\n";
				$html_body = "<h2>Self Limits Changed</h2>We've set a daily wager limit of $".$_POST['daily_wager_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
				$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
			}

			// deposit
			if($last_changed['daily_deposit_last_changed'] != null){
				$last_changed_dt = new DateTime($last_changed['daily_deposit_last_changed']);
				
				$date = new DateTime();
				$time_between = $date->diff($last_changed_dt);
				$hours = $time_between->h;
				$hours = $hours + ($time_between->days*24);
				
				if($_POST['daily_deposit_limit'] != $last_changed['daily_deposit_limit']){
					if($hours >= 24 || $last_changed['daily_deposit_last_changed'] == null){
						$date = new DateTime();
						$now = $date->format("Y-m-d H:i:s");
						$q = "UPDATE `customers` SET `daily_deposit_limit`=%d,`daily_deposit_last_changed`=%s WHERE `user_id`=%i";
						$db->queryDirect($q, array($_POST['daily_deposit_limit'], $now, $session->userinfo['id']));
						
						// send email to notify user.
						$text_body = "We've set a daily deposit limit of $".$_POST['daily_deposit_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
						If you did not make this change or it was made in error, please contact support.\n\n";
						$html_body = "<h2>Self Limits Changed</h2>We've set a daily deposit limit of $".$_POST['daily_deposit_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
						$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
					}else{
						$result['success'] = false;
						$result['hours'] = $hours;
						$result['errors'] .= "You can only change your deposit limit once every 24 hours.<br>";
					}
				}
			}else{
				$date = new DateTime();
				$now = $date->format("Y-m-d H:i:s");
				$q = "UPDATE `customers` SET `daily_deposit_limit`=%d,`daily_deposit_last_changed`=%s WHERE `user_id`=%i";
				$db->queryDirect($q, array($_POST['daily_deposit_limit'], $now, $session->userinfo['id']));
				
				// send email to notify user.
				$text_body = "We've set a daily deposit limit of $".$_POST['daily_deposit_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
				If you did not make this change or it was made in error, please contact support.\n\n";
				$html_body = "<h2>Self Limits Changed</h2>We've set a daily deposit limit of $".$_POST['daily_deposit_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
				$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
			}
			
			// withdraw
			if($last_changed['daily_withdrawal_last_changed'] != null){
				$last_changed_dt = new DateTime($last_changed['daily_withdrawal_last_changed']);
				
				$date = new DateTime();
				$time_between = $date->diff($last_changed_dt);
				$hours = $time_between->h;
				$hours = $hours + ($time_between->days*24);
				
				if($_POST['daily_withdrawal_limit'] != $last_changed['daily_withdrawal_limit']){
					if($hours >= 24 || $last_changed['daily_withdrawal_last_changed'] == null){
						$date = new DateTime();
						$now = $date->format("Y-m-d H:i:s");
						$q = "UPDATE `customers` SET `daily_withdrawal_limit`=%d,`daily_withdrawal_last_changed`=%s WHERE `user_id`=%i";
						$db->queryDirect($q, array($_POST['daily_withdrawal_limit'], $now, $session->userinfo['id']));
						
						// send email to notify user.
						$text_body = "We've set a daily withdrawal limit of $".$_POST['daily_withdrawal_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
						If you did not make this change or it was made in error, please contact support.\n\n";
						$html_body = "<h2>Self Limits Changed</h2>We've set a daily withdrawal limit of $".$_POST['daily_withdrawal_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
						$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
					}else{
						$result['success'] = false;
						$result['hours'] = $hours;
						$result['errors'] .= "You can only change your withdrawal limit once every 24 hours.<br>";
					}
				}
			}else{
				$date = new DateTime();
				$now = $date->format("Y-m-d H:i:s");
				$q = "UPDATE `customers` SET `daily_withdrawal_limit`=%d,`daily_withdrawal_last_changed`=%s WHERE `user_id`=%i";
				$db->queryDirect($q, array($_POST['daily_withdrawal_limit'], $now, $session->userinfo['id']));
				
				// send email to notify user.
				$text_body = "We've set a daily withdrawal limit of $".$_POST['daily_withdrawal_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
				If you did not make this change or it was made in error, please contact support.\n\n";
				$html_body = "<h2>Self Limits Changed</h2>We've set a daily withdrawal limit of $".$_POST['daily_withdrawal_limit']." for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
				$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
			}
			
			// session
			if($last_changed['daily_session_last_changed'] != null){
				$last_changed_dt = new DateTime($last_changed['daily_session_last_changed']);
				
				$date = new DateTime();
				$time_between = $date->diff($last_changed_dt);
				$hours = $time_between->h;
				$hours = $hours + ($time_between->days*24);
				
				if($_POST['daily_session_limit'] != $last_changed['daily_session_limit']){
					if($hours >= 24 || $last_changed['daily_session_last_changed'] == null){
						$date = new DateTime();
						$now = $date->format("Y-m-d H:i:s");
						$q = "UPDATE `customers` SET `daily_session_limit`=%d,`daily_session_last_changed`=%s WHERE `user_id`=%i";
						$db->queryDirect($q, array($_POST['daily_session_limit'], $now, $session->userinfo['id']));
						
						// send email to notify user.
						$text_body = "We've set a session login limit of ".$_POST['daily_session_limit']." minutes for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
						If you did not make this change or it was made in error, please contact support.\n\n";
						$html_body = "<h2>Self Limits Changed</h2>We've set a session login limit of ".$_POST['daily_session_limit']." minutes for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
						$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
					}else{
						$result['success'] = false;
						$result['hours'] = $hours;
						$result['errors'] .= "You can only change your session limit once every 24 hours.<br>";
					}
				}
			}else{
				$date = new DateTime();
				$now = $date->format("Y-m-d H:i:s");
				$q = "UPDATE `customers` SET `daily_session_limit`=%d,`daily_session_last_changed`=%s WHERE `user_id`=%i";
				$db->queryDirect($q, array($_POST['daily_session_limit'], $now, $session->userinfo['id']));
				
				// send email to notify user.
				$text_body = "We've set a session login limit of ".$_POST['daily_session_limit']." minutes for you at your request.  You must wait 24 hours before making any changes to this limit.\n\n
				If you did not make this change or it was made in error, please contact support.\n\n";
				$html_body = "<h2>Self Limits Changed</h2>We've set a session login limit of ".$_POST['daily_session_limit']." minutes for you at your request.  You must wait 24 hours before making any changes to this limit.<br><br>If you did not make this change or it was made in error, please contact support.<br><br>";
				$mailer->send_email($session->userinfo['email'], "CSCLotto Notification - Self Limits Have Been Changed", $html_body, $text_body);
			}
			
			// handle user table changes
			if($_POST['locked'] == 1){
				//Log this exclusion
				$date = new DateTime();
				$now = $date->format("Y-m-d H:i:s");
				$q = "INSERT INTO `exclusion_log` (`exclusion_type_id`, `excluded_on`, `exclusion_end`, `user_id`) VALUES (2, %s, %s, %i)";
				$db->queryDirect($q, array($now, $_POST['locked_expiration'], $session->userinfo['id']));
				
				//Lock the account.
				$q = "UPDATE `users` SET `locked`=%i, `locked_expiration`=%s WHERE `id`=%i";
				$db->queryDirect($q, array($_POST['locked'], $_POST['locked_expiration'], $session->userinfo['id']));
			}
			
			break;
			case 'redeem_voucher':
			$voucher = $_POST['voucher'];
			$redeemed = false;
			$amount = 0;
			$date = new DateTime('now');
			$date_str = $date->format("Y-m-d H:i:s");
			$date_str1 = $date->format("Y-m-d");

			$q = "SELECT `failed_voucher` FROM failed_redemption WHERE user_id=%i;";
			$f = $db->queryOneRow($q, array($session->userinfo['id']));
			$f['failed_voucher']++;
			$q = "UPDATE failed_redemption SET failed_voucher=%i WHERE user_id=%i;";
			$db->queryDirect($q, array($f['failed_voucher'], $session->userinfo['id']));
			$q = "SELECT * FROM vouchers WHERE  end > %s AND code=%s AND (user_id=%i OR user_id IS NULL) AND is_enabled = 1";
			$active_vouchers = $db->queryOneRow($q, array($date_str, strtoupper($_POST['voucher']), $session->userinfo['id']));
			$is_valid=1;
			if($active_vouchers['redeem_ticket_status']==1)
			{
				$is_valid=0;
				$get_birth_date="select date_of_birth from user_info where user_id='".$session->userinfo['id']."' ";
				$birth_date = $db->queryOneRow($get_birth_date);
				$user_bdate=$birth_date['date_of_birth'];
				if(!empty($user_bdate))
				{
					if($user_bdate=='0000-00-00')
					{
						$is_valid=0;
						$result['message'] = "Please contact support to add your birthday so you can redeem the voucher.";		
					}
					else if(date('m-d') != date('m-d', strtotime($user_bdate)))
					{
						$is_valid=0;
						$result['message'] = "You can only redeem this voucher on your birthday";
					}
					else
					{
						$is_valid=1;
					}
				}
				else
				{
						$is_valid=0;
						$result['message'] = "Please contact support to add your birthday so you can redeem the voucher.";		
				}
					
			}
		
			if($is_valid==1)
			{				
				$q = "SELECT * FROM vouchers WHERE  end > %s AND code=%s AND (user_id=%i OR user_id IS NULL) AND is_enabled = 1";
				$active_vouchers = $db->query($q, array($date_str, strtoupper($_POST['voucher']), $session->userinfo['id']));	
				
				foreach( $active_vouchers as $voucher ){
				$q = "SELECT * FROM voucher_redemptions WHERE voucher_id = %i AND user_id=%i";
				$redeemed_check = $db->queryOneRow($q, array($voucher['id'], $session->userinfo['id']));
				if( $redeemed_check==NULL )
				{
				
					$balance = $core->check_balance();
					$q = "UPDATE customers SET bonus_balance=%f WHERE user_id=%i";
					$db->queryDirect($q, array(($balance['bonus_balance']+$voucher['amount']), $session->userinfo['id']));
				
					$q = "INSERT INTO voucher_redemptions (`voucher_id`, `user_id`, `redeemed_on`) VALUES (%i, %i, %s)";
					$db->queryInsert($q, array($voucher['id'], $session->userinfo['id'], $date_str));
					$core->make_customer_transaction($voucher['amount'], 43, "Voucher ".strtoupper($_POST['voucher'])." redeemed for $".$voucher['amount']);
					
					$amount += $voucher['amount'];

					$q = "UPDATE failed_redemption SET failed_voucher=0, failed_ticket=0 WHERE user_id=%i";
					$db->queryDirect($q, array($session->userinfo['id']));
					$f['failed_voucher'] = 0;

				}else{
					//Lets me know that a voucher is being redeemed by someone who already redeemed it.
					$redeemed = true;
				}
			}
			
			if($f['failed_voucher'] >= 10) {
				// lock account after X bad logins

				$q = "UPDATE `users` SET `locked` = '1' WHERE `id`= %s";
				$db->query($q, array($session->userinfo['id']));

				// increase/set lockout timer
				$locked_minutes = $user_info['locked_minutes'] + 15;

				$q = "UPDATE `users` SET `locked_minutes` = %i WHERE `id`= %s";
				$db->query($q, array($locked_minutes, $session->userinfo['id']));

				// set lockout expiration
				$now_dt = new DateTime();
				$expiration_dt = $now_dt->modify('+'.$locked_minutes.' minutes');
				$expiration = $expiration_dt->format("Y-m-d H:i:s");

				$q = "UPDATE `users` SET `locked_expiration` = %s  WHERE `id`= %i";
				$db->query($q, array($expiration, $session->userinfo['id']));

				$q = "UPDATE `failed_redemption` SET `failed_voucher` = %i, `failed_ticket` = %i WHERE user_id=%i";
				$db->queryDirect($q, array("9","9",$session->userinfo['id']));

				$session->logout();
				die();		
				}
			
				if($amount == 0 && $redeemed == true){
					$result['message'] = "This voucher has already been redeemed.";
				}elseif($amount == 0 && $redeemed == false){
					$result['message'] = "This is an invalid voucher";
				}elseif($amount != 0){
				$result['message'] = "".number_format($amount)." ". CURRENCY_FORMAT. " has been added in your account.";
			}
			
		}
			/* $voucher = $_POST['voucher'];
			
			$get_birth_date="select date_of_birth from user_info where user_id='".$session->userinfo['id']."' ";
			$birth_date = $db->queryOneRow($get_birth_date);
			$user_bdate=$birth_date['date_of_birth'];
			
			$redeem_status = "SELECT user_id,redeem_ticket_status FROM vouchers WHERE code='".strtoupper($voucher)."'";
			$status = $db->queryOneRow($redeem_status);
			$ticket_redeem=$status['redeem_ticket_status'];
			
			$redeemed = false;
			$amount = 0;
			$date = new DateTime('now');
			$date_str = $date->format("Y-m-d H:i:s");
            echo "<br> Current Date".$date_str1 = $date->format("Y-m-d");
			echo "<br>U_bdate".$user_bdate;
			echo  "<br>Redeem Status".$ticket_redeem;
			
			if($ticket_redeem==1)
			{
				//	echo "Redeem 1 is ok<br> ";
					
					if($user_bdate=='0000-00-00')
					{
						//echo "if bdate not available<br>";
						$result['message'] = "Please contact support to add the birthday to redeem voucher.";		
					}
					else
					{
						if($user_bdate==$date_str1)
						{
								//echo "if bdate match<br>";
					
									$q = "SELECT `failed_voucher` FROM failed_redemption WHERE user_id=%i;";
									$f = $db->queryOneRow($q, array($session->userinfo['id']));
									$f['failed_voucher']++;
									
									$q = "UPDATE failed_redemption SET failed_voucher=%i WHERE user_id=%i;";
									$db->queryDirect($q, array($f['failed_voucher'], $session->userinfo['id']));
									
										
									$q = "SELECT * FROM vouchers WHERE  end > %s AND code=%s AND (user_id=%i OR user_id IS NULL) AND is_enabled = 1";
									$active_vouchers = $db->query($q, array($date_str, strtoupper($_POST['voucher']), $session->userinfo['id']));
									
									foreach($active_vouchers as $voucher){
									
									$q = "SELECT * FROM voucher_redemptions WHERE voucher_id = %i AND user_id=%i";
									$redeemed_check = $db->queryOneRow($q, array($voucher['id'], $session->userinfo['id']));
									
									if($redeemed_check == NULL){
									$balance = $core->check_balance();
									
										
									$q = "UPDATE customers SET bonus_balance=%f WHERE user_id=%i";
									$db->queryDirect($q, array(($balance['bonus_balance']+$voucher['amount']), $session->userinfo['id']));

									$q = "INSERT INTO voucher_redemptions (`voucher_id`, `user_id`, `redeemed_on`) VALUES (%i, %i, %s)";
									$db->queryInsert($q, array($voucher['id'], $session->userinfo['id'], $date_str));

									$core->make_customer_transaction($voucher['amount'], 43, "Voucher ".strtoupper($_POST['voucher'])." redeemed for $".$voucher['amount']);
									
									$amount += $voucher['amount'];
									
									$q = "UPDATE failed_redemption SET failed_voucher=0, failed_ticket=0 WHERE user_id=%i";
									$db->queryDirect($q, array($session->userinfo['id']));
									
									$f['failed_voucher'] = 0;

									}
									else{
										//Lets me know that a voucher is being redeemed by someone who already redeemed it.
										$redeemed = true;
									}
								}
							
								if($f['failed_voucher'] >= 10) {
								// lock account after X bad logins

								$q = "UPDATE `users` SET `locked` = '1' WHERE `id`= %s";
								$db->query($q, array($session->userinfo['id']));

								// increase/set lockout timer
								$locked_minutes = $user_info['locked_minutes'] + 15;

								$q = "UPDATE `users` SET `locked_minutes` = %i WHERE `id`= %s";
								$db->query($q, array($locked_minutes, $session->userinfo['id']));

								// set lockout expiration
								$now_dt = new DateTime();
								$expiration_dt = $now_dt->modify('+'.$locked_minutes.' minutes');
								$expiration = $expiration_dt->format("Y-m-d H:i:s");

								$q = "UPDATE `users` SET `locked_expiration` = %s  WHERE `id`= %i";
								$db->query($q, array($expiration, $session->userinfo['id']));

								$q = "UPDATE `failed_redemption` SET `failed_voucher` = %i, `failed_ticket` = %i WHERE user_id=%i";
								$db->queryDirect($q, array("9","9",$session->userinfo['id']));

								$session->logout();
								die();		
							}
					
							if($amount == 0 && $redeemed == true){
								$result['message'] = "This voucher has already been redeemed.";
							}elseif($amount == 0 && $redeemed == false){
								$result['message'] = "This is an invalid voucher";
							}elseif($amount != 0){
							$result['message'] = "".number_format($amount)." ".CURRENCY_FORMAT." has been added in your account. Happy Birthday!!";
							}
						}
						else 
							{
								
								$result['message'] = "You can redeem this voucher only on birthday";
							}
					}
				
				}
			else
			{
					// if Status is NO.... not check Date of Birth...
						$q = "SELECT `failed_voucher` FROM failed_redemption WHERE user_id=%i;";
						$f = $db->queryOneRow($q, array($session->userinfo['id']));
						$f['failed_voucher']++;
						
						$q = "UPDATE failed_redemption SET failed_voucher=%i WHERE user_id=%i;";
						$db->queryDirect($q, array($f['failed_voucher'], $session->userinfo['id']));
						
						$q = "SELECT * FROM vouchers WHERE start <= %s AND end > %s AND code=%s AND (user_id=%i OR user_id IS NULL)";
						$active_vouchers = $db->query($q, array($date_str, $date_str, strtoupper($_POST['voucher']), $session->userinfo['id']));
						
						foreach($active_vouchers as $voucher){

							$q = "SELECT * FROM voucher_redemptions WHERE voucher_id = %i AND user_id=%i";
							$redeemed_check = $db->queryOneRow($q, array($voucher['id'], $session->userinfo['id']));
							
							if($redeemed_check == NULL){
								$balance = $core->check_balance();
								
								$q = "UPDATE customers SET bonus_balance=%f WHERE user_id=%i";
								$db->queryDirect($q, array(($balance['bonus_balance']+$voucher['amount']), $session->userinfo['id']));

								$q = "INSERT INTO voucher_redemptions (`voucher_id`, `user_id`, `redeemed_on`) VALUES (%i, %i, %s)";
								$db->queryInsert($q, array($voucher['id'], $session->userinfo['id'], $date_str));

								$core->make_customer_transaction($voucher['amount'], 43, "Voucher ".strtoupper($_POST['voucher'])." redeemed for $".$voucher['amount']);
								
								$amount += $voucher['amount'];

								$q = "UPDATE failed_redemption SET failed_voucher=0, failed_ticket=0 WHERE user_id=%i";
								$db->queryDirect($q, array($session->userinfo['id']));
								$f['failed_voucher'] = 0;

							}else{
								//Lets me know that a voucher is being redeemed by someone who already redeemed it.
								$redeemed = true;
							}
						}
						
						if($f['failed_voucher'] >= 10) {
							// lock account after X bad logins

							$q = "UPDATE `users` SET `locked` = '1' WHERE `id`= %s";
							$db->query($q, array($session->userinfo['id']));

							// increase/set lockout timer
							$locked_minutes = $user_info['locked_minutes'] + 15;

							$q = "UPDATE `users` SET `locked_minutes` = %i WHERE `id`= %s";
							$db->query($q, array($locked_minutes, $session->userinfo['id']));

							// set lockout expiration
							$now_dt = new DateTime();
							$expiration_dt = $now_dt->modify('+'.$locked_minutes.' minutes');
							$expiration = $expiration_dt->format("Y-m-d H:i:s");

							$q = "UPDATE `users` SET `locked_expiration` = %s  WHERE `id`= %i";
							$db->query($q, array($expiration, $session->userinfo['id']));

							$q = "UPDATE `failed_redemption` SET `failed_voucher` = %i, `failed_ticket` = %i WHERE user_id=%i";
							$db->queryDirect($q, array("9","9",$session->userinfo['id']));

							$session->logout();
							die();		
						}
						
						if($amount == 0 && $redeemed == true){
							$result['message'] = "This voucher has already been redeemed.";
						}elseif($amount == 0 && $redeemed == false){
							$result['message'] = "This is an invalid voucher";
						}elseif($amount != 0){
							$result['message'] = "".number_format($amount)." ".CURRENCY_FORMAT." has been added to your bonus balance.";
						}
			}
			*/
			
			break;
		case 'redeem_gift_card':
			$card_number = isset($_POST['card_number']) ? $_POST['card_number'] : NULL;
			$security_code = isset($_POST['security_code']) ? $_POST['security_code'] : NULL;
			
			$q = "SELECT *, `gc`.`amount` AS `total_amount`, SUM(`gcr`.`amount`) AS `total_used` FROM `gift_cards` AS `gc` LEFT JOIN `gift_card_redemptions` AS `gcr` ON `gcr`.`card_number`=`gc`.`card_number` WHERE `gc`.`card_number`=%s AND `gc`.`security_code`=%s";
			$card_info = $db->queryOneRow($q, array($card_number, $security_code));
			
			$card_info['total_used'] = $card_info['total_used'] == NULL ? 0 : $card_info['total_used'];
			
			if($card_info['total_used'] < $card_info['total_amount']){
				//There is still money on the card that will be appended to the account
				$balance = $card_info['total_amount'] - $card_info['total_used'];
				$core->make_customer_transaction($balance, 49, "Gift Card ".$card_info['card_number']." redeemed for $".$balance);
				
				//Add this in the redemption table
				$now = new DateTime();
				$now_str = $now->format("Y-m-d H:i:s");
				$q = "INSERT INTO `gift_card_redemptions` (`card_number`,`amount`,`redeemed_by`,`redeemed_on`) VALUES (%s,%d,%i,%s)";
				$db->queryInsert($q, array($card_number, $balance, $session->userinfo['id'], $now_str));
				
				$result['amount'] = number_format($balance);
				$result['success'] = true;
			}elseif($card_info['id'] == ""){
				//This card/security code combo does not exist
				$result['errors'] = "Invalid card/security code.";
				$result['success'] = false;
			}else{
				//It is fully used
				$result['errors'] = "This card has already been used.";
				$result['success'] = false;
			}
			
			break;
			
		case 'adoller_number_btn':
			$card_number = isset($_POST['card_number']) ? $_POST['card_number'] : NULL;
			
			
			$q = "SELECT *, `a`.`adollar_value` AS `total_amount`, SUM(`ar`.`amount`) AS `total_used` FROM `adollar` AS `a` LEFT JOIN `adollar_redemptions` AS `ar` ON `ar`.`card_number`=`a`.`adollar_number` WHERE `a`.`adollar_number`=%s";
			$card_info = $db->queryOneRow($q, array($card_number));
			
			
			$card_info['total_used'] = $card_info['total_used'] == NULL ? 0 : $card_info['total_used'];
			
			if($card_info['total_used'] < $card_info['total_amount']){
				//There is still money on the card that will be appended to the account
				$balance = $card_info['total_amount'] - $card_info['total_used'];
				$core->make_customer_transaction($balance, 151, "A Doller ".$card_info['card_number']." redeemed for $".$balance);
				
				//Add this in the redemption table
				$now = new DateTime();
				$now_str = $now->format("Y-m-d H:i:s");
				$q = "INSERT INTO `adollar_redemptions` (`card_number`,`amount`,`redeemed_by`,`redeemed_on`) VALUES ('".$card_number."','".$balance."','".$session->userinfo['id']."','".$now_str."')";
				$db->queryInsert($q);
				
				$result['amount'] = number_format($balance);
				$result['success'] = true;
			}elseif($card_info['id'] == ""){
				//This card/security code combo does not exist
				$result['errors'] = "Invalid card Number.";
				$result['success'] = false;
			}else{
				//It is fully used
				$result['errors'] = "This card has already been used.";
				$result['success'] = false;
			}
			
			break;	
                case 'check_daily_deposit':

                        $i_daily_deposit = $core->remaining_deposit_limit($session->userinfo['id']);

                        if(($i_daily_deposit - $_POST['amount'] < 0) && $i_daily_deposit != -1){
                                $result['message'] = "The transaction could not be completed because it would exceed your daily deposit limit. You can only deposit $".$i_daily_deposit." more.";
				$result['success'] = false;				
			} else {
                               $result['success'] = true;	
                        }
			break;
		case 'update_balance':
			$user_id = $session->userinfo['id'];
			$balance = $_POST['balance'];
			$amount = $_POST['amount'];
		 	$date = new DateTime('now');
			$initial= $_POST['initial_balance'];
        		$transaction_date = $date->format("Y-m-d H:i:s");
		//$transaction_date = new Date();
			$q= "UPDATE `customers` SET `available_balance` = '".$balance."'  WHERE user_id = '".$session->userinfo['id']."'"; 
			$db->query($q);

			$insertQuery = "INSERT INTO customer_transaction (`user_id`,`amount`,`transaction_date`,`ip_address`,  `balance`, `transaction_type_id`,`initial_balance`) VALUES (".$session->userinfo['id'].",".$amount.",'".$transaction_date."','".$_SERVER['REMOTE_ADDR']."','".$balance."', 5,'".$initial."')";
			$transaction_id =$db->queryInsert($insertQuery);	
			$curl = curl_init('http://localhost:3001/csc/executewithdraw/'.$transaction_id);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$curl_response = curl_exec($curl);
			curl_close($curl);
			$json_deposit_info=json_decode($curl_response);
			if($json_deposit_info->result==true){
				$q = "UPDATE `customer_transaction` SET `transaction_details` ='".$json_deposit_info->txid ."' WHERE `transaction_id` = '".$transaction_id."'";
				$db->query($q);
			}else{ 
                                $q = "UPDATE `customers` SET `available_balance` = '".$balance."' WHERE user_id = '".$session->userinfo['id']."'";
				$db->query($q);
			}

			$abc = "select * from `customers` WHERE user_id = '".$session->userinfo['id']."'";
                       	$session->userinfo['customers'] = $db->queryOneRow($abc);
                      	$result = $json_deposit_info;  
		
			break;
		/*case 'update_transaction_details':
			$user_id = $session->userinfo['id'];
			$detail = $_POST['txid'];
			$q = "UPDATE `customer_transaction";
break;*/
		case 'redeem_ticket':
			$ticket_number = $_POST['ticket_number'];
			$q = "SELECT `failed_ticket` FROM failed_redemption WHERE user_id=%i;";
			$f = $db->queryOneRow($q, array($session->userinfo['id']));
			
			$f['failed_ticket']++;

			
			//see if ticket exists
			$q = "SELECT * FROM `lotto_ticket` WHERE ticket_number=%i";
			$ticket = $db->queryOneRow($q, array($ticket_number));
			if(!empty($ticket)){
				// if ticket exists, link to customer's account
				$q = "UPDATE `lotto_ticket` SET `user_id` = '".$session->userinfo['id']."' WHERE ticket_id='".$ticket['ticket_id']."'";
				$db->query($q);

				$q = "UPDATE failed_redemption SET failed_ticket=%i WHERE user_id=%i;";
				$db->queryDirect($q, array("0", $session->userinfo['id']));
			} else {

				$q = "UPDATE failed_redemption SET failed_ticket=%i WHERE user_id=%i;";
				$db->queryDirect($q, array($f['failed_ticket'], $session->userinfo['id']));
				
				if($f['failed_ticket'] >= 10) {
					// lock account after X bad logins

					$q = "UPDATE `users` SET `locked` = '1' WHERE `id`= %s";
					$db->query($q, array($session->userinfo['id']));

					// increase/set lockout timer
					$locked_minutes = $user_info['locked_minutes'] + 15;

					$q = "UPDATE `users` SET `locked_minutes` = %i WHERE `id`= %s";
					$db->query($q, array($locked_minutes, $session->userinfo['id']));

					// set lockout expiration
					$now_dt = new DateTime();
					$expiration_dt = $now_dt->modify('+'.$locked_minutes.' minutes');
					$expiration = $expiration_dt->format("Y-m-d H:i:s");

					$q = "UPDATE `users` SET `locked_expiration` = %s  WHERE `id`= %i";
					$db->query($q, array($expiration, $session->userinfo['id']));

					$q = "UPDATE `failed_redemption` SET `failed_voucher` = %i, `failed_ticket` = %i WHERE user_id=%i";
					$db->queryDirect($q, array("9","9",$session->userinfo['id']));

					$session->logout();
					die();		
				}

			}

			//$ticket_info = $db->queryOneRow("SELECT * FROM `winner_payout` WHERE ticket_number=".$ticket_number);
			$q = "SELECT * FROM `winner_payout` WHERE ticket_number=%i";
			$ticket_info = $db->queryOneRow($q, array($ticket_number));

			if($ticket_info != NULL){
				//initialize the success flag
				$result['success'] = true;

				$expired = $core->is_ticket_expired($_POST['ticket_number']);
				if($expired){
					$return = array("success" => "false", "errors" => "This ticket has expired!");
					echo json_encode($return);
					die();
				}

				//Get partial payouts
				$data = $core->get_partial_payouts($_POST['ticket_number']);

				//Tally up the partial payouts
				$total_paid = 0;
				if($data != null){
					for($x=0; $x<count($data); $x++){
						$total_paid += abs($data[$x]['amount']);
					}
				}

				//The amount that needs paid out after factoring in the rest of the payouts
				$payout_balance = $ticket_info["total_payout"] - $total_paid;
				if($payout_balance <= 0){
					$result['success'] = false;
					$result['errors'] = "This ticket has already been paid out in full.<br>";
				}

				//Mark the ticket as paid.
				if($result['success'] == true){
					//Set status to paid

					//$db->query("UPDATE `winner_payout` SET `status_id`=1 WHERE ticket_number='".$_POST['ticket_number']."'");
					$q = "UPDATE `winner_payout` SET `status_id`=1 WHERE ticket_number=%i";
					$db->query($q, array($_POST['ticket_number']));

					//Make a customer transaction
					$core->make_customer_transaction($payout_balance, 4, "Lotto winner from panel submitted online", $ticket_number);
					
					//This ticket can now be linked to a customer
					$q = "UPDATE `lotto_ticket` SET `user_id`=%i WHERE ticket_number=%i";					
					$db->query($q, array($session->userinfo['id'], $_POST['ticket_number']));

					$result["amount"] = "".number_format($payout_balance)." ".CURRENCY_FORMAT;
				}
			}else{
				$result['success'] = false;
				$result['errors'] = "This is not a winning ticket but it has been linked to your account.<br>";
				break;
			}
			break;
		default:
		   die("Invalid Action");
	}
	
	echo json_encode($result);
}
