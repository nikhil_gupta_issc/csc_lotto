<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");

	//Grab the settings for the number of days previous and minimum amount shown
	//for winning customers
	$q = "SELECT value FROM settings WHERE setting='winning_customers_amount' OR setting='winning_customers_days'";
	$settings = $db->query($q);

	if($_POST['type'] == 'recent'){
		if($_POST['data'] == 'lottery'){
			// get most most recent for lottery only
			// 2 = casino, 4 = lotto, 18 = sports, 47 = Rapidballs
			$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE transaction_type_id = 4 AND amount > 0 ORDER BY transaction_date DESC LIMIT 6";
			$winners = $db->query($q);
		}elseif($_POST['data'] == 'casino'){
			// get most most recent for casino only
			// 2 = casino, 4 = lotto, 18 = sports, 47 = Rapidballs
			$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE transaction_type_id = 2 AND amount > 5 ORDER BY transaction_date DESC LIMIT 6";
			$winners = $db->query($q);
		}elseif($_POST['data'] == 'sportsbook'){
			// get most most recent for sports only
			// 2 = casino, 4 = lotto, 18 = sports, 47 = Rapidballs
			$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE transaction_type_id = 18 AND amount > 5 ORDER BY transaction_date DESC LIMIT 6";
			$winners = $db->query($q);
		}elseif($_POST['data'] == 'rapidballs'){
			// get most most recent for rapidballs only
			// 2 = casino, 4 = lotto, 18 = sports, 47 = Rapidballs
			$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE transaction_type_id = 47 AND amount > 5 ORDER BY transaction_date DESC LIMIT 6";
			$winners = $db->query($q);
		}else{
			// get most most recent wins regardless of size
			$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE (transaction_type_id = 2 OR transaction_type_id = 4 OR transaction_type_id = 18 OR transaction_type_id = 47) AND amount > 0 ORDER BY transaction_date DESC LIMIT 6";
			$winners = $db->query($q);
		}
	}else{
		if($_POST['data'] == 'lottery'){
			// get most most recent for lottery only
			// 2 = casino, 4 = lotto, 18 = sports, 47 = Rapidballs
			$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE transaction_type_id = 4 AND amount > 500 ORDER BY transaction_date DESC, amount DESC LIMIT 6";
			$winners = $db->query($q);
		}elseif($_POST['data'] == 'casino'){
			// get most most recent for casino only
			// 2 = casino, 4 = lotto, 18 = sports, 47 = Rapidballs
			$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE transaction_type_id = 2 AND amount > 500 ORDER BY transaction_date DESC, amount DESC LIMIT 6";
			$winners = $db->query($q);
		}elseif($_POST['data'] == 'sportsbook'){
			// get most most recent for sports only
			// 2 = casino, 4 = lotto, 18 = sports, 47 = Rapidballs
			$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE transaction_type_id = 18 AND amount > 500 ORDER BY transaction_date DESC, amount DESC LIMIT 6";
			$winners = $db->query($q);
		}elseif($_POST['data'] == 'rapidballs'){
			// get most most recent for rapidballs only
			// 2 = casino, 4 = lotto, 18 = sports, 47 = Rapidballs
			$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE transaction_type_id = 47 AND amount > 500 ORDER BY transaction_date DESC, amount DESC LIMIT 6";
			$winners = $db->query($q);
		}else{
		//Get the first 5 records ordered amount descending
		$transaction_date = new DateTime($settings[1]['value']." days ago");
		$trans_date = $transaction_date->format("Y-m-d H:i:s");
		$amount = $settings[0]['value'];
		$q = "SELECT transaction_type_id, amount, firstname, lastname, transaction_details FROM customer_transaction AS ct INNER JOIN users AS u ON u.id=ct.user_id WHERE (transaction_type_id = 2 OR transaction_type_id = 4 OR transaction_type_id = 18 OR transaction_type_id = 47) AND amount > 500 ORDER BY transaction_date DESC, amount DESC LIMIT 6";		
		$winners = $db->query($q, array($trans_date, $amount));
		}
	}
?>
<thead>
	<tr>
		<th class="position anim:id anim:number hidden" />
		<th class="driverName anim:number hidden" />
		<th class="pointsTotal anim:update hidden" />
		<th class="pointsTotal anim:update hidden" />
	</tr>
</thead>
<tbody>
<?php
	//Print the winners
	foreach($winners as $winner){
?>
		<tr class="text-center win-list" style="height: 46px;">
			<td class="col-md-1"><img src="/images/icon/<?php echo $winner['transaction_type_id']; ?>_icon.png" ></td>
			<td class="col-md-2"><?php echo number_format($winner['amount']). " " . CURRENCY_FORMAT; ?></td>
			<td class="col-md-1"><?php echo substr($winner['firstname'], 0, 1).".".substr($winner['lastname'], 0, 1)."."; ?></td>
			<td class="col-md-8"><?php echo $winner['transaction_details'] ?></td>
		</tr>
<?php
	}
?>
</tbody>
