<?php
	include("header.php");
	include('page_top.php');

	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}

?>
	<div class="clearfix" style="background:#fff; color: 000;">	
			<div class="row">
					<div class="container">
					   <div class="header_image">
							<img src="/images/about.jpg" class="img_grow_wide">
						</div>
					</div>
				</div>
<!-- <div class="clearfix" style="text-align: left; padding:10px;">
		<p style="color:#CCC;padding:10px;border: 1px solid purple; background: #000;">Our team of industry veterans started with one mission in mind - to provide a trustworthy and unrivaled online experience for every player, no matter their casino experience level. Whether this is your first time playing online, or your one-thousandth, we promise you lightning-fast deposit authorization and customer service by our knowledgeable, friendly agents. So no matter the hour, when you want to play the best casino games available online, CSCLotto is your Casino/Lottery provider.</p>
		</div> -->
	<div class="col-md-12" style="color:#000;">
		<h2>About CSCLotto</h2>
	
		<p>CSCLotto is a cryptocurrency lottery website that runs on the CasinoCoin test network. The purpose is to showcase the power and speed of the CasinoCoin blockchain and it’s native currency CSC, and act as a use case demo in the CasinoCoin Bankroll Manager app.</p>
		
		<p><b>CSCLotto is not a real money gambling site.</b> CSC wagered on the site cannot be redeemed for any other currency, fiat or crypto, and they have no value outside of CSCLotto. Test CSC can be claimed for free directly from the CasinoCoin Foundation through <a target="_blank" href="http://link.com/">CasinoCoin Testnet Faucet</a>.</p>
		
		<p>We encourage the CasinoCoin community to test the different games and functions on this website and report to us their feedback. This will help us with further developing the website and enhancing it's technology. You can submit your feedback either via <a target="_blank" href="mailto:feedback@csclotto.com">Email</a> or through our <a target="_blank" href="http://CasinoCoin.chat/">Discord Community Chat</a></p>

		<h2>Contact Us</h2>
		
		<p>If you have any questions about CSCLotto or CasinoCoin, feel free to <a target="_blank" href="https://casinocoin.org/contact/">Contact Us</a>.</p>
		
		
		
	</div>
</div>

<?php
	include("footer.php");
?>
