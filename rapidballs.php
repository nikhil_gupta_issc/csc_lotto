<?php
	$page_title = "FastBallz - A new lotto drawing every 5 minutes!";
	$menu_color = "#e97a19";
	$footer_color = "#e97a19";

	include("header.php");
	include('page_top.php');
	
	// get current settings from db
	$settings = array();
	$q = "SELECT * FROM `settings`";
	$settings_info = $db->query($q);
	foreach($settings_info as $setting){
		$settings[$setting['setting']] = $setting['value'];
	}
	
	// see if the game is disabled
	if($settings['rapidballs_enabled'] == 0){
		echo "<div class='well' style='height: 400px; text-align: center; margin:0;'><h2><br><br>We're sorry but this game is currently disabled.<br><br>Please check back later.</h2></div>";
		include("footer.php");
		die();
	}

	if(!$session->logged_in){
		include('public_rapidballs.php');
		include("footer.php");
		die;
	}else{
		include("rapidballs/index.php");
	}
	
	include("footer.php");
?>
