<?php
	$page_title = "CSC Lotto Signup for BRM.";

	$top_left_fixed_img = "";
	$top_right_fixed_img = "<img src='/images/cards_rgt.png'>";

	include("header.php");
	include('page_top.php');
	require_once("lib/framework/db.php");

	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}

?>

<!-- CSC Lotto Signup landing page -->

<div class="clearfix" style="background:#111111">
<p>&nbsp;</p>

   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?></h3>
         </div>
         <div class="panel-body">
         <?php
            if (isset($_GET['accountid']) && isset($_GET['token'])) {
               $_SESSION['csc_accountid'] = $_GET['accountid'];
               $_SESSION['csc_token'] = $_GET['token'];
               // get the BRM user object
               $userurl = "https://brmdev.casinocoin.org/brm/user/".$_SESSION['csc_accountid'];
               $curl = curl_init($userurl);
               curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($curl, CURLOPT_POST, false);
               curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
               curl_setopt($curl, CURLOPT_HTTPHEADER, array('X-API-JWT: '.$_SESSION['csc_token']));
              $curl_response = curl_exec($curl);
              curl_close($curl);
              $_SESSION['csc_user'] = json_decode($curl_response);

			  /**
			  * These lines of code are added by Nikhil G. to avoid registertion with same email
			  */

			  $email = $_SESSION["csc_user"]->Emailaddress;
			  $query = "SELECT * FROM users WHERE email = '" . $email . "'";
			  $rows = $db->query($query);
			  $userExist = false;
			  if(count($rows))
			  {
				  $userExist = true;
				  $userid = $rows[0]['id'];
				  $query = "UPDATE users SET csc_account_id = '" . $_SESSION['csc_accountid'] . "' WHERE id = " . $userid;
				  $db->queryDirect($query);

				  $query = "UPDATE users_csc_accounts SET status = 'INACTIVE' WHERE user_id = " . $userid . " AND csc_account_id = '" . $_SESSION['csc_accountid'] . "'";
				  $db->queryDirect($query);

				  $query = "INSERT INTO `users_csc_accounts` (`user_id`, `csc_account_id`) VALUES(" . $userid . ", '" . $_SESSION['csc_accountid'] . "')";
				  $db->queryInsert($query);
			  }
            }
            if ($session->logged_in){
               // user was already logged in, save account id
               $q = "UPDATE `users` SET `csc_account_id` = %s WHERE `username`= %s";
               $db->query($q, array($_SESSION['csc_accountid'], $session->username));
               // generate and get a destination tag
               $curl = curl_init('http://localhost:3001/csc/depositinfo/'.$session->userinfo['id']);
               curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($curl, CURLOPT_POST, false);
               curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
               $curl_response = curl_exec($curl);
               curl_close($curl);
               $json_deposit_info=json_decode($curl_response);
               // report tag back to brm and finish registration
               $finishurl = "https://brmdev.casinocoin.org/brm/user/".$_SESSION['csc_accountid']."/confirmRegistration";
               $curl = curl_init($finishurl);
               $post_data = json_encode(array('OperatorAccountID' => 'chwQSi2KV8hJEWSGL9cG32dviNp6LFqUKQ','DestinationTag' => $json_deposit_info->tag));
               curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($curl, CURLOPT_POST, true);
               curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data );
               curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'X-API-JWT: '.$_SESSION['csc_token']));
               curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
               $curl_response = curl_exec($curl);
               curl_close($curl);
         ?>
         <p>Hello <?php echo $_SESSION['csc_user']->Firstname.' '.$_SESSION['csc_user']->Lastname?></p>
         <p>You are now signed up with your BRM account id: <?php echo $_SESSION['csc_accountid'] ?>
         <?php
            }
			else if($userExist){
				?>
		        <p>It seems you already have an account. Please login with your email ID.</p>
		        <?php
			}
			else  {
        ?>
        <p>Hello <?php echo $_SESSION['csc_user']->Firstname.' '.$_SESSION['csc_user']->Lastname?></p>
        <p>Please use the header buttons to either signup or login to your existing account.</p>
        <?php
            }
        ?>
        </div>
   </div>
</div>

<?php
	include("footer.php");
?>
