<?php
	$page_title = "Access Denied";
	include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php');
?>
	<div class="container-fluid">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/side_nav.php');?>
		<div class="col-sm-8 col-md-9 col-xl-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $page_title; ?></h3>
				</div>
				<div class="panel-body" style="text-align:center">
					<h3>You do not have permission to view this page</h3>
				</div>
			</div>
		</div>
	</div>
<?php
	include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); 
?>