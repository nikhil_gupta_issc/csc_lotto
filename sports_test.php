<?php
	$page_title = "NFL, NBA, College Bet on your favorite sports team!";
	
	$top_left_fixed_img = '<img src="/images/baseball.png">';
	$top_right_fixed_img = '<img src="/images/football.png">';

	include("header.php");
	include('page_top.php');

	if(!$session->logged_in){
		// show warning
		include('must_login.php');
		die;
	}else{
?>

<script>
	$(document).ready(function(){
		$(document).on("click", "#iframe_load_btn", function(){
			$("#ContentPlaceHolder1_sportsframe").attr("src", $("#iframe_url").val()+"?stoken=<?php echo $session->userid; ?>");
		});
	});
</script>

<style>
	.bg{
		background: black url('/images/sportbg.jpg') no-repeat center top;
	}
	ul.nav.nav-pills.head-menu>li>a{
		color: white !important;
	}
	ul.nav.nav-pills.head-menu>li:hover>a{
		color: hsl(288, 100%, 18%) !important;
	}
	ul.nav.nav-pills.head-menu>li.active:hover>a{
		color: white !important;
	}
</style>

<div id="wrapper" class="container">
	<div class="header_image">
		<img src="/images/sports_header.jpg" class="img_grow_wide">
	</div>
	
	<div class="col-sm-8">
		<form class="form-inline">
			<div class="form-group col-sm-10">
				<label for="iframe_url">URL</label>
				<input type="text" class="form-control" id="iframe_url" placeholder="URL" style="width: 90%;">
			</div>
			<button id="iframe_load_btn" type="button" class="btn btn-default">Load</button>
		</form>
	</div>
	
	<iframe id="ContentPlaceHolder1_sportsframe" src="" title="Sportsbook" width="100%" height="1500px" frameborder="0"></iframe>
</div>

<?php
	}
	
	include("footer.php");
?>