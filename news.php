<?php
	$page_title = "Updates, Promotions, etc.";
	$menu_color = "turquoise";
	$footer_color = "turquoise";
	
	$top_left_fixed_img = "";
	$top_right_fixed_img = "<img src='/images/cards_rgt.png'>";

	include("header.php");
	include('page_top.php');
	
	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}

?>

	<script>
		$(document).ready(function(){
			$('#error').hide();
			$(document).on("click", "#manage_images_btn", function(){
				$('#manage_images_modal').modal("show");
			});

			$(document).on("click", ".edit-news", function(){
				var id_arr = this.id.split("_");
				var id = id_arr[1];
				
				$.ajax({
					url: "/ajax/news.php",
					dataType: 'json',
					data: {
						action: 'load',
						id: id
					},
					type: "POST",
					success: function(data){
						$('#image').val(data.image).trigger('change');
						$('#title').val(data.title);
						$('#text').val(data.text);
						$('#news_id').val(data.id);
						$('#news_delete_btn').show();
						$('#submit_add_edit').html('Save Changes');
						$('#add_news').modal("show");
					}
				});
			});

			$(document).on("click", "#add_btn", function(){
				$('#error').hide();
				$('#image').val("").trigger('change');
				$('#title').val("");
				$('#text').val("");
				$('#news_id').val('-1');
				$('#news_delete_btn').hide();
				$('#submit_add_edit').html('Create Post');
				$('#add_news').modal("show");
			});
			
			$(document).on("click", "#news_delete_btn", function(){
				$.ajax({
					url: "/ajax/news.php",
					dataType: 'json',
					data: {
						action: 'delete',
						id: $('#news_id').val()
					},
					type: "POST",
					success: function(data){
						$('#add_news').modal("hide");
						window.location.reload();
					}
				});
			});
			
			$(document).on("click", "#submit_add_edit", function(){
				var action = 'update';
				if($(this).html() == 'Create Post'){
					var action = 'insert';
				}else if($(this).html() == 'Save Changes'){
					var action = 'update';
				}
				
				if($('#title').val()=='' || $('#text').val()=='')
				{
					$('#error').show();
					$("#submit_add_edit").removeAttr('disabled');
					$('#error').html('Title or Message can not be Blank.');
				}
				else
				{
				
					$.ajax({
						url: "/ajax/news.php",
						dataType: 'json',
						data: {
							action: action,
							id: $('#news_id').val(),
							image: $('#image').val(),
							title: $('#title').val(),
							text: $('#text').val()
						},
						type: "POST",
						success: function(data){
							$('#add_news').modal("hide");
							window.location.reload();
						}
					});
				}
			});
			
			$(document).on("change", "#image", function(){
				if($(this).val() != ""){
					$('#image_preview').attr('src', '/images/news/'+$(this).val());
					$('#image_preview').show();
				}else{
					$('#image_preview').hide();
				}
			});
			
			$('#manage_images_modal').on('hidden.bs.modal', function (){
				$.ajax({
					url: "/ajax/update_news_pic_list.php",
					type: "POST",
					success: function(data){
						$('#image').html(data);
					}
				});
			});
			
			$.ajax({
				url: "/ajax/update_news_pic_list.php",
				type: "POST",
				success: function(data){
					$('#image').html(data);
				}
			});
		});
	</script>

<!-- Blog Start -->
	<div class="clearfix" style="background:#fff;">	
			<div class="row">
					<div class="container">
					   <div class="header_image">
							<img src="/images/news_banner.jpg?v=1" class="img_grow_wide">
						</div>
					</div>
				</div>

	<div class="clearfix" style="background:#fff">	
	<p>&nbsp;</p>
	<?php
		if ($session->userinfo['userlevel'] > 8){
	?>
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?> ~ Admin</h3>
					<div class="btn-group pull-right">
						<a id="add_btn" href="#" class="btn btn-primary btn-sm">Add</a>
					</div>
				</div>
			</div>
	<?php
		}
	?>

	<div class="col-md-12">
		<?php
			$q = "SELECT * FROM `news_updates` WHERE `is_deleted` != 1 order by id DESC";
			$news = $db->query($q);
			foreach($news as $new){
		?>
		<div class="col-sm-6">
			<?php
				if ($session->userinfo['userlevel'] > 8){
			?>
				<div class="btn-group pull-right">
					<a class="btn btn-primary btn-sm edit-news" id="id_<?php echo $new['id']; ?>">Edit</a>
				</div>
			<?php
				}
			?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $new['title']; ?></h3>
				</div>
				<div class="panel-body" style="max-height:500px; overflow:hidden;background-color: #000;
  color: #fff;">
				<?php
					if(strlen($new['image']) > 4){
						echo "<img src='/images/news/".$new['image']."' width='100%'/>";
					}
					echo "<p style='padding-bottom:20px;'>".$new['text']."</p>";
				?>
				</div>
				<div class="btn-group pull-right" style="margin-bottom:5px;">
					<a class="btn btn-primary btn-sm" data-toggle="modal" <?php echo 'data-target="#more_news'.$new['id'].'"';?>>Read More...</a>
				</div>
			</div>
		</div>
		<?php
			}
		?>
	</div>
</div>

<!-- /Blog End -->
<!-- View More Modal -->
<?php
	$q = "SELECT * FROM `news_updates` WHERE `is_deleted` != 1";
	$news = $db->query($q);
	foreach($news as $new){
?>
<div>
	<?php	
		echo "<div class='modal fade' id='more_news".$new['id']."' tabindex='-1' role='dialog' aria-labelledby='settingsModal'>";
	?>
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
				<div class="panel-heading">
					<h3 class="panel-title"><strong><?php echo $new['title']; ?></strong></h3>
				</div>
				<div class="panel-body">
					<?php
						if(strlen($new['image']) > 4){
							echo "<img src='/images/news/".$new['image']."' width='100%'/>";
							echo "<hr>";
						}
						echo "<p style='padding-bottom:20px; white-space: pre-wrap;'>".$new['text']."</p>";
					?>
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	}
?>
<!-- /View More Modal -->

<!-- Add/Edit Modal -->
<div class="modal fade" id="add_news" tabindex="-1" role="dialog" aria-labelledby="slider_pic_modal_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header clearfix">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title pull-left" id="slider_pic_modal_label">Add Post</h4>
			</div>
			<div class="modal-body clearfix">
				<div id="upload_photos" class="col-sm-12">
					<div class="col-sm-3">
						<img id="image_preview" src="" style="width: 100%; display: none; border: 1px solid black;">
					</div>
				
					<div class="col-sm-6">
						<div class="form-group">
							<label for="image">Image</label>
							<select id="image" name="image" class="form-control">

							</select>
						</div>
					</div>
					
					<div class="col-sm-3" style="padding-top: 25px;">
						<button id="manage_images_btn" type="button" class="btn btn-primary">Manage Images</button>
					</div>
					
					<div class="col-sm-12" style="padding-top: 15px;">
						<div class="form-group">
							<label for='title'>Title</label>
							<textarea id="title" type='text' class='form-control' name='title'></textarea>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">		
							<label for='text'>Body</label>
							<textarea id="text" type='text' class='form-control' name='text' rows='6'></textarea>
						</div>
					</div>
					
					<div class="col-sm-12" style="padding-left:45px;">
						<div id="error" class="alert alert-danger" role="alert" style="display:none;"></div>	
					</div>
					
				</div>					
			</div>
			
			<div class="modal-footer">
				<input id="news_id" type='hidden' name='id' value='-1'>
				<button id="news_delete_btn" type="button" class="btn btn-danger pull-left">Delete</button>
				<button id="submit_add_edit" type="submit" class="btn btn-primary">Create Post</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- /Add Modal -->

<!-- Manages Images -->
<div class="modal fade" id="manage_images_modal" tabindex="-1" role="dialog" aria-labelledby="manage_images_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header clearfix">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title pull-left" id="manage_images_label">Manage Images</h4>
			</div>
			<div class="modal-body clearfix">
				<div id="upload_photos" class="col-sm-12">
					<?php include($_SERVER['DOCUMENT_ROOT'].'/upload_news_photos.php'); ?>
				</div>					
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
</div>
<!-- /Manage Images -->

<?php
	include("footer.php");
?>
