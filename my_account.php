<?php
	include("header.php");
	
	// if form submitted, save user changes
	// add data to customer table
	if(isset($_POST)){
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		$key_value_pairs = array();
		$table = "customers";
		foreach($_POST as $key => $value){
			// see if key is a valid column name in db
			$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
			$key_exists = $db->queryOneRow($q);
			if($key_exists['count'] > 0){
				$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
				$num_digits = $key_exists['NUMERIC_PRECISION'];
				$num_places = $key_exists['NUMERIC_SCALE'];
			
				// validate data against data type defined in database
				$data_type = $key_exists['DATA_TYPE'];
				switch ($data_type) {
					case "tinyint":
						settype($value , "integer");
						break;
					case "mediumint":
						settype($value , "integer");
						break;
					case "smallint":
						settype($value , "integer");
						break;
					case "int":
						settype($value , "integer");
						break;
					case "decimal":
						settype($value , "float");
						break;
					case "float":
						settype($value , "float");
						break;
					case "double":
						settype($value , "float");
						break;
					case "real":
						settype($value , "float");
						break;
					case "bit":
						settype($value , "boolean");
						break;
					case "boolean":
						settype($value , "boolean");
						break;
					case "date":
						if(!$value){
							$value = "NULL";
						}else{
							$value = date('Y-m-d', strtotime($value));
						}
						break;
					case "datetime":
						if(!$value){
							$value = "NULL";
						}else{
							$value = date('Y-m-d H:i:s', strtotime($value));
						}					
						break;
					case "time":
						if(!$value){
							$value = "NULL";
						}else{
							$value = date('H:i:s', strtotime($value));
						}
						break;
					case "timestamp":
						settype($value , "integer");
						break;
					case "year":
						if(!$value){
							$value = "NULL";
						}
						break;				
				}
				
				if($value == "NULL"){
					$key_value_pairs[$i] = " `".$key."` = ".$value;
				}else{
					$key_value_pairs[$i] = " `".$key."` = '".$value."'";
				}
				$i++;
			}
		}
		$value_str = implode(",", $key_value_pairs);
		$q  = "UPDATE $table SET $value_str WHERE user_id=".$session->userinfo['id'];
		$result = $db->queryDirect($q);
		
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		$key_value_pairs = array();
		$table = "users";
		foreach($_POST as $key => $value){
			// see if key is a valid column name in db
			$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
			$key_exists = $db->queryOneRow($q);
			if($key_exists['count'] > 0){
				$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
				$num_digits = $key_exists['NUMERIC_PRECISION'];
				$num_places = $key_exists['NUMERIC_SCALE'];
			
				// validate data against data type defined in database
				$data_type = $key_exists['DATA_TYPE'];
				switch ($data_type) {
					case "tinyint":
						settype($value , "integer");
						break;
					case "mediumint":
						settype($value , "integer");
						break;
					case "smallint":
						settype($value , "integer");
						break;
					case "int":
						settype($value , "integer");
						break;
					case "decimal":
						settype($value , "float");
						break;
					case "float":
						settype($value , "float");
						break;
					case "double":
						settype($value , "float");
						break;
					case "real":
						settype($value , "float");
						break;
					case "bit":
						settype($value , "boolean");
						break;
					case "boolean":
						settype($value , "boolean");
						break;
					case "date":
						if(!$value){
							$value = "NULL";
						}else{
							$value = date('Y-m-d', strtotime($value));
						}
						break;
					case "datetime":
						if(!$value){
							$value = "NULL";
						}else{
							$value = date('Y-m-d H:i:s', strtotime($value));
						}					
						break;
					case "time":
						if(!$value){
							$value = "NULL";
						}else{
							$value = date('H:i:s', strtotime($value));
						}
						break;
					case "timestamp":
						settype($value , "integer");
						break;
					case "year":
						if(!$value){
							$value = "NULL";
						}
						break;				
				}
				
				if($value == "NULL"){
					$key_value_pairs[$i] = " `".$key."` = ".$value;
				}else{
					$key_value_pairs[$i] = " `".$key."` = '".$value."'";
				}
				$i++;
			}
		}
		$value_str = implode(",", $key_value_pairs);
		$q  = "UPDATE $table SET $value_str WHERE id=".$session->userinfo['id'];
		$result = $db->queryDirect($q);
		
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		$key_value_pairs = array();
		$table = "user_info";
		foreach($_POST as $key => $value){
			// see if key is a valid column name in db
			$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '$key' and TABLE_NAME = '$table'";
			$key_exists = $db->queryOneRow($q);
			if($key_exists['count'] > 0){
				$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
				$num_digits = $key_exists['NUMERIC_PRECISION'];
				$num_places = $key_exists['NUMERIC_SCALE'];
			
				// validate data against data type defined in database
				$data_type = $key_exists['DATA_TYPE'];
				switch ($data_type) {
					case "tinyint":
						settype($value , "integer");
						break;
					case "mediumint":
						settype($value , "integer");
						break;
					case "smallint":
						settype($value , "integer");
						break;
					case "int":
						settype($value , "integer");
						break;
					case "decimal":
						settype($value , "float");
						break;
					case "float":
						settype($value , "float");
						break;
					case "double":
						settype($value , "float");
						break;
					case "real":
						settype($value , "float");
						break;
					case "bit":
						settype($value , "boolean");
						break;
					case "boolean":
						settype($value , "boolean");
						break;
					case "date":
						if(!$value){
							$value = "NULL";
						}else{
							$value = date('Y-m-d', strtotime($value));
						}
						break;
					case "datetime":
						if(!$value){
							$value = "NULL";
						}else{
							$value = date('Y-m-d H:i:s', strtotime($value));
						}					
						break;
					case "time":
						if(!$value){
							$value = "NULL";
						}else{
							$value = date('H:i:s', strtotime($value));
						}
						break;
					case "timestamp":
						settype($value , "integer");
						break;
					case "year":
						if(!$value){
							$value = "NULL";
						}
						break;				
				}
				
				if($value == "NULL"){
					$key_value_pairs[$i] = " `".$key."` = ".$value;
				}else{
					$key_value_pairs[$i] = " `".$key."` = '".$value."'";
				}
				$i++;
			}
		}
		$value_str = implode(",", $key_value_pairs);
		$q  = "UPDATE $table SET $value_str WHERE user_id=".$session->userinfo['id'];
		$result = $db->queryDirect($q);
		
		// update session variables
		$database->getUserInfo($session->username, true);
	}
	
	include('page_top.php');

	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}
	
	if(!$session->logged_in){
		// show warning
		include('public_lottery.php');
		include("footer.php");
		die;
	}
	
	// set user's id for uploading
	$_SESSION['upload_id'] = $session->userinfo['id'];
	
	$trans_search = isset($_GET['trans_search']) ? $_GET['trans_search'] : "";
?>
<style> 
	.black_tabs li {
	padding: 5px 10px 4px 19px !important;
	}
	</style>
	<link rel="stylesheet" type="text/css" href="/rapidballs/css/balls.css">

	<script>
		$(document).ready(function(){
            
                        var url = window.location.href.split('#');
                        if(url[1] != '' && url[1] != undefined){
                             $("#tab_"+url[1]+"_btn").trigger("click")
                        }


                       var lottery_history = 0;
                        $(document).on('click','#tab_lot_btn',function(){
                            if(lottery_history == 0){
	
				var lot_data_table = $('#lot_datatable').DataTable({
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					"ajax": "/ajax/datatables/bets.php",
					"columns": [
						{ "data": "purchase_date" },
						{ "data": "draw_time" },
						{ "data": "game_id" },
						{ "data": "ticket_number" },
						{ "data": "ball_string" },
						{ "data": "winning_draw" },
						{ "data": "bet_type" },
						{ "data": "bet_amount" },
						{ "data": "loyality_points" },
						{ "data": "payout" },
						{ "data": "status" }
					],
					"order": [[0, "desc"]],
					"dom": 'lfrtp',
                                        "fnInitComplete": function (oSettings, json) {
		                           lottery_history = 1;
		                }
				});
                              }

                        });
			
                         $("#lotto_start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#lotto_start_date').data("DateTimePicker").date()){
					start_date = $('#lotto_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#lotto_end_date').data("DateTimePicker").date()){
					end_date = $('#lotto_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#lot_datatable')){
					lot_data_table.destroy();
				}
				
                               lot_data_table = $('#lot_datatable').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/bets.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
					{ "data": "purchase_date" },
					{ "data": "draw_time" },
					{ "data": "game_id" },
					{ "data": "ticket_number" },
					{ "data": "ball_string" },
					{ "data": "winning_draw" },
					{ "data": "bet_type" },
					{ "data": "bet_amount" },
					{ "data": "loyality_points" },
					{ "data": "payout" },
					{ "data": "status" }
				],
			"order": [[0, 'desc']]
		});

				
				
				lot_data_table.draw(true);
			});
			
			// end date change
			$("#lotto_end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#lotto_start_date').data("DateTimePicker").date()){
					start_date = $('#lotto_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#lotto_end_date').data("DateTimePicker").date()){
					end_date = $('#lotto_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#lot_datatable')){
					lot_data_table.destroy();
				}
				
				lot_data_table = $('#lot_datatable').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/bets.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
					{ "data": "purchase_date" },
					{ "data": "draw_time" },
					{ "data": "game_id" },
					{ "data": "ticket_number" },
					{ "data": "ball_string" },
					{ "data": "winning_draw" },
					{ "data": "bet_type" },
					{ "data": "bet_amount" },
					{ "data": "loyality_points" },
					{ "data": "payout" },
					{ "data": "status" }
				],
			"order": [[0, 'desc']]
		});
			
				
				
				lot_data_table.draw(true);
			});

                       var trans_history = 0;
                       $(document).on('click','#tab_history_btn',function(){
                          if(trans_history == 0){

			var trans_data_table = $('#trans_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"columns": [
					{ "data": "id" },
					{ "data": "trans_date" },
					{ "data": "details","render": function(data, type, row, meta){
            					//	data = "<a href='http://testexplorer.casinocoin.org/tx/" + data + ">" + data + "</a>";
								//console.log(row);	
									if(type === 'display' && row.transaction_type_id ==="5" || row.transaction_type_id ==="6"){
								// && row.type ==='Customer Withdrawal'
								//console.log(row.type);
							data = '<a style="color:#d89191 !important;" target="_blank" href="http://testexplorer.casinocoin.org/tx/' + data + '">' + data + '</a>';
                					//data = "<a href='http://testexplorer.casinocoin.org/tx/" + data + ">" + data + "</a>";
            						}
            					return data;} 
							},
					{ "data": "amount" },
					{ "data": "loyality_points" },
					{ "data": "initial_balance" },
					{ "data": "balance" }
				],
				"oSearch": {"sSearch": "<?php echo $trans_search; ?>"},
				"order": [[0, 'desc']],
				"ajax": "/ajax/datatables/customer_activity_detail.php",
				"dom": 'lfrtp',
                                "fnInitComplete": function (oSettings, json) {
		                           trans_history = 1;
						//console.log(json['data']);
						//$.each(json['data'],function(id,value){
						//console.log(value)
						//});
		                }
			});trans_data_table.draw(true);
	                        }
                      });
			
                 $("#trans_start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#trans_start_date').data("DateTimePicker").date()){
					start_date = $('#trans_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#trans_end_date').data("DateTimePicker").date()){
					end_date = $('#trans_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#trans_datatable')){
					trans_data_table.destroy();
				}
				
                               trans_data_table = $('#trans_datatable').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/customer_activity_detail.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
					{ "data": "id" },
					{ "data": "trans_date" },
					{ "data": "details" },
					{ "data": "amount" },
					{ "data": "loyality_points" },
					{ "data": "initial_balance" },
					{ "data": "balance" }
				],
				"oSearch": {"sSearch": "<?php echo $trans_search; ?>"},
			"order": [[0, 'desc']]
		});

				
				
				trans_data_table.draw(true);
			});
			
			// end date change
			$("#trans_end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#trans_start_date').data("DateTimePicker").date()){
					start_date = $('#trans_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#trans_end_date').data("DateTimePicker").date()){
					end_date = $('#trans_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#trans_datatable')){
					trans_data_table.destroy();
				}
				
				trans_data_table = $('#trans_datatable').DataTable({
					"bInfo" : false,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					"ajax": "/ajax/datatables/customer_activity_detail.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
					"columns": [
							{ "data": "id" },
							{ "data": "trans_date" },
							{ "data": "details" },
							{ "data": "amount" },
							{ "data": "loyality_points" },
							{ "data": "initial_balance" },
							{ "data": "balance" }
						],
						"oSearch": {"sSearch": "<?php echo $trans_search; ?>"},
					"order": [[0, 'desc']]
				});
				
				trans_data_table.draw(true);
			});



			var topup_data_table = $('#topup_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"columns": [
					{ "data": "id" },
					{ "data": "pin" },
					{ "data": "transaction_id" },
					{ "data": "trans_date" },
					{ "data": "details" }
					
				],
				"oSearch": {"sSearch": "<?php echo $trans_search; ?>"},
				"order": [[0, 'desc']],
				"ajax": "/ajax/datatables/topups.php",
				"dom": 'lfrtp'
			});

                       $("#topup_start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#topup_start_date').data("DateTimePicker").date()){
					start_date = $('#topup_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#topup_end_date').data("DateTimePicker").date()){
					end_date = $('#topup_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#topup_datatable')){
					topup_data_table.destroy();
				}
				
                               topup_data_table = $('#topup_datatable').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/topups.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
					{ "data": "id" },
					{ "data": "pin" },
					{ "data": "transaction_id" },
					{ "data": "trans_date" },
					{ "data": "details" }
					
				],
				"oSearch": {"sSearch": "<?php echo $trans_search; ?>"},
			"order": [[0, 'desc']]
		});

				
				
				topup_data_table.draw(true);
			});
			
			// end date change
			$("#topup_end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#topup_start_date').data("DateTimePicker").date()){
					start_date = $('#topup_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#topup_end_date').data("DateTimePicker").date()){
					end_date = $('#topup_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#topup_datatable')){
					topup_data_table.destroy();
				}
				
				topup_data_table = $('#topup_datatable').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/topups.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
					{ "data": "id" },
					{ "data": "pin" },
					{ "data": "transaction_id" },
					{ "data": "trans_date" },
					{ "data": "details" }
					
				],
				"oSearch": {"sSearch": "<?php echo $trans_search; ?>"},
			"order": [[0, 'desc']]
		});
				
				
				
				topup_data_table.draw(true);
			});

                        var rapid_history = 0;
                        $(document).on('click','#tab_rb_btn',function(){
                       if(rapid_history == 0){
			
			var rapidballs_data_table = $('#rapidballs_datatable').DataTable({
				"responsive": true,
				"processing": true,
				"serverSide": true,
				"columns": [
					{ "data": "datetime_drawn" },
					{ "data": "draw_id" },
					{ "data": "drawing" },
					{ "data": "type" },
					{ "data": "bet_on" },
					{ "data": "bet_amount" },
					{ "data": "loyality_points" },
					{ "data": "status" }
				],
				"order": [[0, 'desc']],
				"ajax": "/ajax/datatables/rapidballs_bet.php",
                                 "fnInitComplete": function (oSettings, json) {
		                           rapid_history = 1;
		                }
			});
 }

                        });

                        $("#rapid_start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#rapid_start_date').data("DateTimePicker").date()){
					start_date = $('#rapid_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#rapid_end_date').data("DateTimePicker").date()){
					end_date = $('#rapid_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#rapidballs_datatable')){
					rapidballs_data_table.destroy();
				}
				
                               rapidballs_data_table = $('#rapidballs_datatable').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/rapidballs_bet.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
					{ "data": "datetime_drawn" },
					{ "data": "draw_id" },
					{ "data": "drawing" },
					{ "data": "type" },
					{ "data": "bet_on" },
					{ "data": "bet_amount" },
					{ "data": "loyality_points" },
					{ "data": "status" }
				],
			"order": [[0, 'desc']]
		});

				
				
				rapidballs_data_table.draw(true);
			});
			
			// end date change
			$("#rapid_end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#rapid_start_date').data("DateTimePicker").date()){
					start_date = $('#rapid_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#rapid_end_date').data("DateTimePicker").date()){
					end_date = $('#rapid_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#rapidballs_datatable')){
					rapidballs_data_table.destroy();
				}
				
				rapidballs_data_table = $('#rapidballs_datatable').DataTable({
					"bInfo" : false,
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					"ajax": "/ajax/datatables/rapidballs_bet.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
					"columns": [
							{ "data": "datetime_drawn" },
							{ "data": "draw_id" },
							{ "data": "drawing" },
							{ "data": "type" },
							{ "data": "bet_on" },
							{ "data": "bet_amount" },
							{ "data": "loyality_points" },
							{ "data": "status" }
						],
					"order": [[0, 'desc']]
				});
				
				
				
				rapidballs_data_table.draw(true);
			});

                      			
			$(document).on("change", "#locked", function(){
				if($("#locked").is(":checked")){
					// set expiration to use default lockout time
					var expire_moment = moment().add(<?php echo LOCKED_MINUTES; ?>, 'minutes');;
					$('#locked_expiration').data("DateTimePicker").clear().date(expire_moment);
					
					$("#lock_expire_option").slideDown("fast");
				}else{
					$("#lock_expire_option").slideUp("fast");
				}
			});
			
			$(document).on("change", "#send_email_notifications", function(){
				$.ajax({
					url: "/ajax/my_account.php",
					type: "POST",
					data: {
						action : "save_email_notifications",
						send_email_notifications : +$('#send_email_notifications').is(":checked")
					},
					dataType: "json",
					success: function(data){
						
					}
				});
			});
			
			$(document).on("change", "#losses_limited", function(){
				if($("#losses_limited").is(":checked")){
					if($("#daily_loss_limit").val() == -1){
						$("#daily_loss_limit").val("0");
					}
					$("#loss_limit").slideDown("fast");
				}else{
					$("#loss_limit").slideUp("fast");
					$("#daily_loss_limit").val("-1");
				}
			});
			
			$(document).on("change", "#wagers_limited", function(){
				if($("#wagers_limited").is(":checked")){
					if($("#daily_wager_limit").val() == -1){
						$("#daily_wager_limit").val("0");
					}
					$("#wager_limit").slideDown("fast");
				}else{
					$("#wager_limit").slideUp("fast");
					$("#daily_wager_limit").val("-1");
				}
			});
			
			$(document).on("change", "#deposits_limited", function(){
				if($("#deposits_limited").is(":checked")){
					if($("#daily_deposit_limit").val() == -1){
						$("#daily_deposit_limit").val("0");
					}
					$("#deposit_limit").slideDown("fast");
				}else{
					$("#deposit_limit").slideUp("fast");
					$("#daily_deposit_limit").val("-1");
				}
			});
			
			$(document).on("change", "#withdrawals_limited", function(){
				if($("#withdrawals_limited").is(":checked")){
					if($("#daily_withdrawal_limit").val() == -1){
						$("#daily_withdrawal_limit").val("0");
					}
					$("#withdrawal_limit").slideDown("fast");
				}else{
					$("#withdrawal_limit").slideUp("fast");
					$("#daily_withdrawal_limit").val("-1");
				}
			});
			
			$(document).on("change", "#sessions_limited", function(){
				if($("#sessions_limited").is(":checked")){
					if($("#daily_session_limit").val() == -1){
						$("#daily_session_limit").val("0");
					}
					$("#session_limit").slideDown("fast");
				}else{
					$("#session_limit").slideUp("fast");
					$("#daily_session_limit").val("-1");
				}
			});
			
			$(document).on("change", "#transactions_limited", function(){
				if($("#transactions_limited").is(":checked")){
					if($("#transaction_limit").val() == -1){
						$("#transaction_limit").val("0");
					}
					$("#tx_limit").slideDown("fast");
				}else{
					$("#tx_limit").slideUp("fast");
					$("#transaction_limit").val("-1");
				}
			});
			
			<?php
				if($session->userinfo['customers']['daily_loss_limit'] == -1){
					echo '$("#losses_limited").bootstrapToggle("off");';
				}else{
					echo '$("#losses_limited").bootstrapToggle("on");';
				}
				
				if($session->userinfo['customers']['daily_wager_limit'] == -1){
					echo '$("#wagers_limited").bootstrapToggle("off");';
				}else{
					echo '$("#wagers_limited").bootstrapToggle("on");';
				}
				
				if($session->userinfo['customers']['daily_deposit_limit'] == -1){
					echo '$("#deposits_limited").bootstrapToggle("off");';
				}else{
					echo '$("#deposits_limited").bootstrapToggle("on");';
				}
				
				if($session->userinfo['customers']['daily_withdrawal_limit'] == -1){
					echo '$("#withdrawals_limited").bootstrapToggle("off");';
				}else{
					echo '$("#withdrawals_limited").bootstrapToggle("on");';
				}
				
				if($session->userinfo['customers']['daily_session_limit'] == -1){
					echo '$("#sessions_limited").bootstrapToggle("off");';
				}else{
					echo '$("#sessions_limited").bootstrapToggle("on");';
				}
				
				if($session->userinfo['customers']['transaction_limit'] == -1){
					echo '$("#transactions_limited").bootstrapToggle("off");';
				}else{
					echo '$("#transactions_limited").bootstrapToggle("on");';
				}
				
				if($session->userinfo['send_email_notifications'] == 1){
					echo '$("#send_email_notifications").bootstrapToggle("on");';
				}
			?>
			
			$(document).on("click", "#save_limits", function(){
				// lock account settings
				var locked_expire = "";
				if($('#locked_expiration').data("DateTimePicker").date()){
					locked_expire = $('#locked_expiration').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss');
				}
				
				$.ajax({
					url: "/ajax/my_account.php",
					type: "POST",
					data: {
						action : "set_limits",
						daily_loss_limit :  $("#daily_loss_limit").val(),
						daily_wager_limit :  $("#daily_wager_limit").val(),
						daily_deposit_limit :  $("#daily_deposit_limit").val(),
						daily_withdrawal_limit :  $("#daily_withdrawal_limit").val(),
						daily_session_limit :  $("#daily_session_limit").val(),
						transaction_limit :  $("#transaction_limit").val(),
						locked : +$('#locked').is(":checked"),
						locked_expiration: locked_expire
					},
					dataType: "json",
					success: function(data){
						if(data.success==true){
							bootbox.alert("Limits have been set.");
							$("#set_limits_errors").html("");
							$("#set_limits_errors").slideUp("fast");
							$("#set_limits_modal").modal("hide");
						}else{
							$("#set_limits_errors").html(data.errors);
							$("#set_limits_errors").slideDown("fast");
						}
					}
				});
			});
				
			$(document).on("click", "#redeem_ticket", function(){
				$.ajax({
					url: "/ajax/my_account.php",
					type: "POST",
					data: {
						action : "redeem_ticket",
						ticket_number : $("#ticket").val()
					},
					dataType: "json",
					success: function(data){
						if(data.success==true){
							bootbox.alert("Winner!! "+data.amount+" has been added to your balance");
							$("#redemption_errors").html("");
							$("#redemption_errors").slideUp("fast");
						}else{
							$("#redemption_errors").html(data.errors);
							$("#redemption_errors").slideDown("fast");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Redemption attempts exceeded.  Your account has been locked out.", function(){ 
							window.location.reload();
						});
					}
				});
			});
			
			$(document).on("click", "#redeem_gift_card", function(){
				$.ajax({
					url: "/ajax/my_account.php",
					type: "POST",
					data: {
						action : "redeem_gift_card",
						card_number : $("#gift_card_number").val(),
						security_code : $("#gift_card_security_code").val()
					},
					dataType: "json",
					success: function(data){
						if(data.success==true){
							bootbox.alert("$"+data.amount+" has been added to your balance");
							$("#redemption_errors").html("");
							$("#redemption_errors").slideUp("fast");
						}else{
							$("#redemption_errors").html(data.errors);
							$("#redemption_errors").slideDown("fast");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Redemption attempts exceeded.  Your account has been locked out.", function(){ 
							window.location.reload();
						});
					}
				});
			});
					
			
			$(document).on("click", "#adoller_number_btn", function(){
			
				if($("#adoller_number").val().length ==12){
				//$("#redemption_errors").html("");
				$.ajax({
					url: "/ajax/my_account.php",
					type: "POST",
					data: {
						action : "adoller_number_btn",
						card_number : $("#adoller_number").val()						
					},
					dataType: "json",
					success: function(data){
						if(data.success==true){
							bootbox.alert("$"+data.amount+" has been added to your Bonus balance");
							$("#redemption_errors").html("");
							$("#redemption_errors").slideUp("fast");
						}else{
							$("#redemption_errors").html(data.errors);
							$("#redemption_errors").slideDown("fast");
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Redemption attempts exceeded.  Your account has been locked out.", function(){ 
							window.location.reload();
						});
					}
				});
				}
				else {
				$("#redemption_errors").html("Please Enter 12-Digit Code.");
				$("#redemption_errors").slideDown("fast");
				}
			});
			
			$(document).on("click", "#redeem_voucher", function(){
				$.ajax({
					url: "/ajax/my_account.php",
					type: "POST",
					data: {
						action : "redeem_voucher",
						voucher : $("#voucher").val()
					},
					dataType: "json",
					success: function(data){
						$("#voucher").val("");
						bootbox.alert(data.message);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Redemption attempts exceeded.  Your account has been locked out.", function(){ 
							window.location.reload();
						});
					}
				});
			});


		$(document).on("click","#execute_csc_withdraw",function(){
			$("#execute_csc_withdraw").attr("disabled", true);
			var total_balance = document.getElementById("total_csc_balance").innerHTML.split(" ");//console.log(total_balance);
			var balance = total_balance[0];
			var with_draw = $("#csc_withdraw_amount").val();
					var js_array =<?php echo json_encode($session);?>;
					//	console.log(js_array);
					//	var balance = js_array['userinfo']['customers']['available_balance'];
					console.log(balance);
					var num = parseInt( balance.replace( ',', '' ) );console.log(num);	
						var total = num - with_draw;console.log(with_draw);console.log(total);
				if(with_draw <= num){
                                   $.ajax({
					url:"/ajax/my_account.php",
					type:"POST",
					data:{ action:"update_balance",
						balance: total,
                                                 amount:with_draw,
						initial_balance:num      },
					dataType:"json",
					success:function(data){
						console.log(data);                 
						$.ajax({
                                        		url:"/ajax/balances.php",
                                        		type:"POST",
                                        		data:{ action:"get_current_balances"
                                                 		 },
                                        		dataType:"json",
                                        		success:function(data){
								//console.log(data);
								$('.avaiable_money').html(data.available_money);
                        $('.current_balance').html(data.current_balance);
                        $('.bonus_balance').html(data.bonus_balance);
                        $('.bonus_hold').html(data.bonus_hold);
                        $('.total_balance').html(data.total_balance);
			
                                        		}
                                		});
		if(data.result==true){
if($.fn.dataTable.isDataTable('#trans_datatable')){
$('#trans_datatable').DataTable().destroy();
                                }
trans_history = 0;
			//console.log(data.message);
			$("#csc_withdraw_amount").val("");
			bootbox.alert("withdraw is being processed.");
			 $("#execute_csc_withdraw").attr("disabled", false);
			}else{ $("#csc_withdraw_amount").val("");
				bootbox.alert("withdraw Unsucessfull!Try again.");
				 $("#execute_csc_withdraw").attr("disabled", false);
                               }
		}
				});
				}else{bootbox.alert("Enter the correct amount");
					$("#csc_withdraw_amount").val(""); $("#execute_csc_withdraw").attr("disabled", false);	}
				}); 


       $(document).on("click","#execute_csc_withdraw_all",function(){
		$("#execute_csc_withdraw_all").attr("disabled", true);
		var total_balance_string = document.getElementById("total_csc_balance").innerHTML.split(" ");
                        var total_balance = total_balance_string[0];
	 var num = parseInt( total_balance.replace( ',', '' ) );	
			//console.log(num);console.log(total_balance);
			if(total_balance == 0){bootbox.alert("no balance to withdraw"); $("#execute_csc_withdraw_all").attr("disabled", false);}
			else{	$.ajax({
					url:"/ajax/my_account.php",
					type:"POST",
					data:{action:"update_balance",
						balance:0,
						amount:num,
						initial_balance:num	},
						dataType:"json",
                                        success:function(data){
                                               // console.log(data);
                                                $.ajax({
                                                        url:"/ajax/balances.php",
                                                        type:"POST",
                                                        data:{ action:"get_current_balances"
                                                                 },
                                                        dataType:"json",
                                                        success:function(data){
								//console.log(data);
                                                                $('.avaiable_money').html(data.available_money);
               						        $('.current_balance').html(data.current_balance);
                        					$('.bonus_balance').html(data.bonus_balance);
                        					$('.bonus_hold').html(data.bonus_hold);
                        					$('.total_balance').html(data.total_balance);
                                                        }
                                                });//console.log(data);
              				  if(data.result==true){
                       				 //console.log(data.message);
                        			$("#csc_withdraw_amount").val("");
                        			bootbox.alert("withdraw is being processed."); $("#execute_csc_withdraw_all").attr("disabled", false); if($.fn.dataTable.isDataTable('#trans_datatable')){
$('#trans_datatable').DataTable().destroy();
                                }
trans_history = 0;
//$('#trans_datatable').DataTable().ajax.reload();
                      				 }else{ $("#csc_withdraw_amount").val(""); $("#execute_csc_withdraw_all").attr("disabled", false);
                                		bootbox.alert("withdraw Unsucessfull!Try again.");
                               			}
               				 }			
                                });

					}		
					});
				
			//Get Results Of Previous Ticket
	$(document).on("click", "#results_confirm", function(){
		$(this).prop("disabled", true);
		$("#results_error").hide();
		$("#results_error").html("");
		if($("#results_ticket_number").val().length != 15){
			$("#results_error").append("Invalid Ticket Number<br>");
			$("#results_error").slideDown("fast");
		}
		if($("#results_ticket_number").val().match(/^[0-9]+$/) == null){
			$("#results_error").append("Tickets are numeric only<br>");
			$("#results_error").slideDown("fast");
		}

		//ajax for the ticket information
		if($("#results_error").html() == ""){
			$.ajax({
				url: "panel/ajax/results.php",
				data: {action: "get_results", ticket_number: $("#results_ticket_number").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);
				
				if(info.success == "true"){
					$("#results_table_body").html(info.table);
					$("#results_total").text(Number(info.total_won, 10).toFixed(2));
					$("#results_details").slideDown('fast');
				}
			});
		}
		$(this).prop("disabled", false);
	});
			
			<?php 
				if(isset($_GET['tab'])){
				
					if($_GET['tab'] == 'personal'){
						echo '$("#tab_personal_btn").trigger("click");';
					}elseif($_GET['tab'] == 'ops'){
						echo '$("#tab_ops_btn").trigger("click");';
					}elseif($_GET['tab'] == 'vouchers'){
						echo '$("#tab_vouchers_btn").trigger("click");';
					}elseif($_GET['tab'] == 'rb'){
						echo '$("#tab_rb_btn").trigger("click");';
					}elseif($_GET['tab'] == 'lot'){
						echo '$("#tab_lot_btn").trigger("click");';
					}elseif($_GET['tab'] == 'history'){
						echo '$("#tab_history_btn").trigger("click");';
					}elseif($_GET['tab'] == 'deposite'){
						echo '$("#tab_deposite_btn").trigger("click");';
					}elseif($_GET['tab'] == 'top_up'){
						echo '$("#tab_topup_btn").trigger("click");';
					}elseif($_GET['tab'] == 'ticket_check'){
						echo '$("#tab_ticket_btn").trigger("click");';
					}elseif($_GET['tab'] == 'casinocoin'){
                                                echo '$("#tab_csc_btn").trigger("click");';
                                        }
				}
			?>
		});
	</script>
<style>
#results_table_body td {

  color: #fff !important;
  background-color: hsl(288, 100%, 18%) !important;
  text-align: center !important;
}
td a {
  color: #fff !important;
}

</style>
	<script type="text/javascript" src="/js/my_account.js?v=<?=JS_VERSION?>"></script>
	<script type="text/javascript" src="js/lotto_betting.js?v=<?=JS_VERSION?>"></script>

	<div class="clearfix" style="background:#111111">	
		<div class="row">
				<div class="container">
				   <div class="header_image">
						<img src="/images/myaccount_banner.jpg?v=1" class="img_grow_wide">
					</div>
				</div>
			</div>
			
			<ul class="black_tabs">
				<li class="active"><a id="tab_personal_btn" data-toggle="tab" href="#personal"><span>Personal</span></a></li>
				<li><a id="tab_ops_btn" data-toggle="tab" href="#ops"><span>Operations</span></a></li>
				<!--<li><a id="tab_vouchers_btn" data-toggle="tab" href="#vouchers"><span>Vouchers</span></a></li>-->
                               <?php if(!empty($b_rapidball_settings)){ ?>
				<li><a id="tab_rb_btn" data-toggle="tab" href="#rb"><span>FastBallz History</span></a></li>
                               <?php } ?>
				<!--<li role="presentation"><a href="#ticket_history" aria-controls="ticket_history" role="tab" data-toggle="tab" id="ticket_history_tab"><span>Lotto Purchase History</span></a></li>-->
				<li><a id="tab_history_btn" data-toggle="tab" href="#history"><span>Transaction History</span></a></li>
				<?php
				$settings = array();
				$q = "SELECT * FROM `settings`";
				$settings_info = $db->query($q);
					foreach($settings_info as $setting){
						$settings[$setting['setting']] = $setting['value'];
					}
						if($settings['creditcard_enabled'] == 1){
				
				 ?>
                                <li><a id="tab_deposite_btn" data-toggle="tab" href="#deposite"><span>Deposit</span></a></li>
                                <?php }?>
<!--
                                <li><a id="tab_topup_btn" data-toggle="tab" href="#top_up" ><span>Top ups</span></a></li>
                                <li><a id="tab_ticket_btn" data-toggle="tab" href="#ticket_check" ><span>Ticket Checker</span></a></li>
-->
                               <!-- <li><a id="tab_lot_btn" data-toggle="tab" href="#lot"><span>Lotto History Details</span></a></li>-->
                                <li><a id="tab_csc_btn" data-toggle="tab" href="#casinocoin"><span>Cashier</span></a></li>
			</ul>
				<div class="panel-body">
					<div class="tab-content">
					<div id="personal" class="tab-pane fade in active">
					<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">User Information</h3>
							</div>
							
							<?php 
								// show previously saved pic if its valid
								if($session->userinfo['customers']['profile_picture_path']){
									$picture_path = $session->userinfo['customers']['profile_picture_path']."&auth=".sha1(md5($session->userinfo['email'].$session->userinfo['id'].date('Ymdh')));
								}else{
									$picture_path = "/admin/images/generic-user-purple.png";
								}
							?>
							
							<div class="panel-body">
								<form class="form" method="post" enctype="multipart/form-data">									
									<div class="col-sm-2">
										<div class="thumbnail" style="width: 150px; height: 150px;">
											<div class="thumbnail-caption">
												<h4><b>My Photo</b></h4>
												<p>Photo outdated or not accurate?  <br><br><a href='locations.php'><b>Contact us!</b></a></p>
											</div>
											<img id="user_pic" style="text-align: center; width: 100%; height: 100%;" alt="User Pic" src="<?php echo $picture_path; ?>" readonly="readonly">
										</div>
									</div>

									<div class="col-sm-10">
										<div class="col-sm-12">
											<div class="col-sm-4">
												<div class="form-group">
													<label for="first_name">First Name</label>
													<input type="text" class="form-control" id="first_name" name="firstname" placeholder="First Name" value="<?php echo $session->userinfo['firstname']; ?>" readonly="readonly">
												</div>
											</div> 

											<div class="col-sm-4">
												<div class="form-group">
													<label for="middle_name">Middle Name</label>
													<input type="text" class="form-control" id="middle_name" name="midname" placeholder="Middle Name" value="<?php echo $session->userinfo['midname']; ?>" readonly="readonly">
												</div>
											</div>
											
											<div class="col-sm-4">
												<div class="form-group">
													<label for="last_name">Last Name</label>
													<input type="text" class="form-control" id="last_name" name="lastname" placeholder="Last Name" value="<?php echo $session->userinfo['lastname']; ?>" readonly="readonly">
												</div>
											</div>
										</div>
										
										<?php
											$sel_male = strtolower($session->userinfo['user_info']['gender']) == "male" ? "selected" : "";							
											$sel_female = strtolower($session->userinfo['user_info']['gender']) == "female" ? "selected" : "";
											$sel_none = $sel_male == "" && $sel_female == "" ? "selected" : "";
										?>
										
										<div class="col-sm-12">
											<div class="col-sm-4">
												<div class="form-group">
													<label for="gender">Gender</label>
													<select class="form-control" id='gender' name="gender" disabled>
														<option <?php echo $sel_none; ?> value='Not Specified'></option>
														<option <?php echo $sel_female; ?> value='Female'>Female</option>
														<option <?php echo $sel_male; ?> value='Male'>Male</option>
													</select>
												</div>	
											</div>
										
											<div class="col-sm-4">
												<div class="form-group">
													<label for="date_of_birth">Date of Birth</label>
													<input type="date" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="Birth Date" value="<?php echo $session->userinfo['user_info']['date_of_birth']; ?>" readonly="readonly">
												</div>	
											</div>
											
											<div class="col-sm-4">
												<div class="form-group">
													<label for="nickname">Nickname</label>
													<input type="text" class="form-control" id="nickname" name="nickname" placeholder="Nickname" value="<?php echo $session->userinfo['user_info']['nickname']; ?>">
												</div>
											</div>
										</div>											
									</div>
									
									<div class="col-sm-12">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="address1">Address 1</label>
												<input type="text" class="form-control" id="address" name="address" value="<?php echo $session->userinfo['user_info']['address']; ?>" readonly="readonly" >
											</div>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label for="address2">Address 2</label>
												<input type="text" class="form-control" id="address2" name="address2" value="<?php echo $session->userinfo['user_info']['address2']; ?>" readonly="readonly" >
											</div>
										</div>
									</div>
									
									<div class="col-sm-12">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="city">City </label>
												<input type="text" class="form-control" id="city" name="city" value="<?php echo $session->userinfo['user_info']['city']; ?>" readonly="readonly" >
											</div>
										</div>
										
										<div class="col-sm-4">
											<div class="form-group">
												<label for="island_id">Island:</label>
												<select id="island_id" name="island_id" class="form-control selectpicker" disabled>
													<option value="-1"></option>
													<?php
														$q = "SELECT * FROM island WHERE id>0 ORDER BY name ASC";
														$islands = $db->query($q);
														foreach($islands as $island){
															if($session->userinfo['user_info']['island_id'] ==  $island['id']){
																echo "<option selected value='".$island['id']."'>".$island['name']."</option>";
															}else{
																echo "<option value='".$island['id']."'>".$island['name']."</option>";
															}
														}
													?>
												</select>
											</div>
										</div>
										
										<div class="col-sm-4">
											<div class="form-group">
												<label for="country">Country</label>
												<input type="text" class="form-control" id="country" name="country" value="<?php echo $session->userinfo['user_info']['country']; ?>" readonly="readonly" >
											</div>
										</div>
									</div>
									
									<div class="col-sm-12">
										<div class="col-sm-4">								
											<div class="form-group">
												<label for="email">Email</label>
												<input type="text" class="form-control" id="email" name="email" placeholder="Email Address" value="<?php echo $session->userinfo['email']; ?>" readonly="readonly" >
											</div>	
										</div>
									
										<div class="col-sm-4">								
											<div class="form-group">
												<label for="telephone">Home Phone</label>
												<input type="text" class="form-control" id="telephone" name="telephone" placeholder="Home Phone Number" value="<?php echo $session->userinfo['user_info']['telephone']; ?>" readonly="readonly" />
												&nbsp;<span style="color:red;"id="errmsg"></span>
											</div>	
										</div> 
										
										<div class="col-sm-4">
											<div class="form-group">
												<label for="cellphone">Mobile Phone</label>
												<input type="text" class="form-control" id="cellphone" name="cellphone" placeholder="Cellular Phone Number" value="<?php echo $session->userinfo['user_info']['cellphone']; ?>" readonly="readonly" />
												&nbsp;<span style="color:red;"id="errmsg"></span>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="card_number">Card Number</label>
												<input type="text" class="form-control" id="card_number" name="card_number" placeholder="Card Number" value="<?php echo $session->userinfo['customers']['card_number']; ?>" readonly="readonly" />
												&nbsp;<span style="color:red;"id="errmsg"></span>
											</div>
										</div>
										<?php
										if($session->userinfo['customers']['is_verified'] == 1){
										$verified= "Yes";}else{$verified= "No";
										}
										?>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="card_number">Verified</label>
												<input type="text" class="form-control" id="Verified" name="Verified" placeholder="Verified" value="<?php echo $verified; ?>" readonly="readonly" />
												
												&nbsp;<span style="color:red;"id="errmsg"></span>
											</div>
										</div>
										
										
										
										
									</div>
									
									
									
									<div class="col-sm-12">
										<hr>
										<input type="submit" class="btn btn-magenta pull-right" id="action" name="action" value="Save Changes"></input>
									</div>
								</form>
							</div>
						</div>
						</div>
					</div>
					<div id="ops" class="tab-pane fade">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">User Operations</h3>
								</div>
								<div class="panel-body">
									<div class="col-sm-12" style="text-align: center; min-height: 250px;">
										<button style="margin: 10px; padding: 10px;" class="btn btn-magenta" id="verify_identity">Verify Identity</button>
										<button style="margin: 10px; padding: 10px;" class="btn btn-magenta" id="change_password">Change Password</button>
									<!--	<button style="margin: 10px; padding: 10px;" class="btn btn-magenta" id="change_pin">Change CSCLotto ATM PIN</button>-->
										<button style="margin: 10px; padding: 10px;" class="btn btn-magenta" id="change_2fa"><?php echo $session->userinfo['google_auth_enabled'] == 1 ? "Disable 2FA" : "Enable 2FA"; ?></button>
										<button style="margin: 10px; padding: 10px;" class="btn btn-magenta" id="set_limits">Self Limits</button>
										
										<br><br>
										
										<div class="form-group">
											<div class="form-group">
												<label class="control-label" for="send_email_notifications">Login Attempt Email Notifications: </label>
												<input id="send_email_notifications" type="checkbox" data-toggle="toggle" data-on="Send" data-off="Don't Send" data-width="130">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="vouchers" class="tab-pane fade">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">						
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Vouchers</h3>
								</div>
								<div class="panel-body">
									<div class="col-sm-6">
										<input style="margin-bottom: 5px;" class="form-control" id="voucher" value="<?php echo $_GET['vouchercode']; ?>"></input>
									</div>
									<div class="col-sm-6">
										<button style="margin-bottom: 5px;" class="btn btn-magenta" id="redeem_voucher">Redeem Voucher</button>
									</div>
									<br><br>
									<div class="col-sm-6">
										<input style="margin-bottom: 5px;" class="form-control" id="ticket"></input>
									</div>
									<div class="col-sm-6">
										<button style="margin-bottom: 5px;" class="btn btn-magenta" id="redeem_ticket">Redeem Ticket</button>
									</div>
									<br><br>
									<div class="col-sm-4">
										<input style="margin-bottom: 5px;" class="form-control" id="gift_card_number"></input>
									</div>
									<div class="col-sm-2">
										<input style="margin-bottom: 5px;" class="form-control" id="gift_card_security_code"></input>
									</div>
									<div class="col-sm-6">
										<button style="margin-bottom: 5px;" class="btn btn-magenta" id="redeem_gift_card">Redeem Gift Card</button>
									</div>
									<br><br>
									<div class="col-sm-6">
										<input style="margin-bottom: 5px;" class="form-control" id="adoller_number"></input>
									</div>
									<div class="col-sm-6">
										<button style="margin-bottom: 5px;" class="btn btn-magenta" id="adoller_number_btn">Redeem A Dollar</button>
									</div>
									<br><br>
									<div id="redemption_errors" class="alert alert-danger col-sm-12" style="display:none; margin-bottom: 5px;">
					
									</div>
								</div>
							</div>
						</div>	
					</div>	
					<!-- START CODE FOR LOTTO PURCHASE HISTORY!-->
					
					   <div role="tabpanel" class="tab-pane fade in" id="ticket_history">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Lotto Purchase History</h3>
						</div>
<!-- <div class="form-group">
							<label for="start_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">From Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='start_date'>
								<input type='text' class="form-control" placeholder="From Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label for="end_date" class="control-label col-xl-1 col-lg-2 col-md-2 col-sm-3 pull-left" style="margin-right: 5px;">To Date</label>
							<div style="padding-left: 10px; padding-right: 10px;" class="input-group date dp col-lg-2 col-md-3 col-sm-4" id='end_date'>
								<input type='text' class="form-control" placeholder="To Date"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div> -->

						<script type="text/javascript">
							$(function () {
								$("#ticket_start_date").on("dp.change", function (e) {
									$('#ticket_end_date').data("DateTimePicker").minDate(e.date);
								});
								$("#ticket_end_date").on("dp.change", function (e) {
									$('#ticket_start_date').data("DateTimePicker").maxDate(e.date);
								});
							});
						</script>
						
						<div class="panel-body large-panel-scroll" style="min-height: 250px;overflow-x: scroll; padding-left: 15px; padding-right: 15px;">							
							<table id="ticket_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th class="text-left">Purchase Date</th>
												<th class="text-left">Draw Date</th>
												<th class="text-left">Game</th>
												<th class="text-left">Ticket Number</th>
												<th class="text-left">Pick</th>
												<th class="text-left">Winning Draw</th>
												<th class="text-left">Bet Type</th>
												<th class="text-left">Bet Amount</th>
												
												<th class="text-left">Payout</th>
												<th class="text-left">Status</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="6" class="dataTables_empty">Loading data from server</td>
											</tr>
										</tbody>
									</table>							
						</div>
					</div>
				</div>
					
				<!--	!-->
					
					
					<div id="lot" class="tab-pane fade">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
							<div class="panel panel-default">
								<div class="panel-heading clearfix">
									<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Lotto History Details</h3>
								</div>
								<div class="panel-body" style="padding-left: 15px; padding-right: 15px;">

<form class="form">
<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='lotto_start_date'>
								         <input type='text' class="form-control" placeholder="From Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>

<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='lotto_end_date'>
								         <input type='text' class="form-control" placeholder="To Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>


</form>

									<table id="lot_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th class="text-left">Purchase Date</th>
												<th class="text-left">Draw Date</th>
												<th class="text-left">Game</th>
												<th class="text-left">Ticket Number</th>
												<th class="text-left">Pick</th>
												<th class="text-left">Winning Draw</th>
												<th class="text-left">Bet Type</th>
												<th class="text-left">Bet Amount</th>
												<th class="text-left">Loyalty Points</th>
												<th class="text-left">Payout</th>
												<th class="text-left">Status</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="6" class="dataTables_empty">Loading data from server</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div id="rb" class="tab-pane fade">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">FastBallz History</h3>
								</div>
								<div class="panel-body" style="padding-left: 15px; padding-right: 15px;">
<form class="form">
<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='rapid_start_date'>
								         <input type='text' class="form-control" placeholder="From Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>

<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='rapid_end_date'>
								         <input type='text' class="form-control" placeholder="To Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>


</form>

									<table id="rapidballs_datatable" class="display table dt-responsive" cellspacing="0" width="100%" style="background-color: #2d2d2d; color: #fff;">
										<thead>
											<tr>
												<th class="text-left">Draw Time</th>
												<th class="text-left">Draw #</th>
												<th class="text-left">Drawing</th>
												<th class="text-left">Bet Type</th>
												<th class="text-left">Bet</th>
												<th class="text-left">Amount Bet</th>
												<th class="text-left">Loyalty Points</th>
												<th class="text-left">Status</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="4" class="dataTables_empty">Loading data from server</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div id="history" class="tab-pane fade">
					
						<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Your Transactions</h3>
								</div>
								<div class="panel-body" style="padding-left: 15px; padding-right: 15px;">
<form class="form">
<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='trans_start_date'>
								         <input type='text' class="form-control" placeholder="From Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>

<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='trans_end_date'>
								         <input type='text' class="form-control" placeholder="To Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>


</form>

									<table id="trans_datatable" class="display table dt-responsive" style="background-color: #2d2d2d; color: #fff; width: 100%;">
										<thead>
											<tr>
												<th class="text-left">ID</th>
												<th class="text-left">Date</th>
												<th class="text-left">Details</th>
												<th class="text-left">Amount</th>
												<th class="text-left">Loyalty Points</th>
												<th class="text-left">Initial Balance</th>
												<th class="text-left">Ending Balance</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="4" class="dataTables_empty">Loading data from server</td>
											</tr>
										</tbody>
									
									</table>
								</div>
							</div>
						</div>
					</div>
	<div id="ticket_check" class="tab-pane fade">
	<div class="col-md-12">
		<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Ticket Checker</h3>
				</div>
				<div class="panel-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-4 control-label">Ticket Number:</label>
							<div class="col-sm-4">
								<input type='text' class='form-control' id="results_ticket_number"/>
							</div>
							<div class="col-sm-4">
							<button type="button" class="btn btn-primary" id="results_confirm">Results</button></div>
						</div>
						<div class="col-sm-12">
					<div id="results_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
					</form>
					<div class="col-sm-12" id="results_details" style="display:none;">
					<h3>Results for this Ticket</h3>
					<table class="table table-striped" id="results_table">
						<thead>
							<tr>
								<th>Game</th>
								<th>Number</th>
								<th>Amount Won</th>
								<th>Win Date</th>
							</tr>
						</thead>
						<tbody id="results_table_body">

						</tbody>
						<tfoot>
							<tr>
								<td style="text-align:right;" colspan="2">Total Won</td>
								<td colspan="2">$<span id="results_total">0.00</span></td>
							</tr>
						</tfoot>
					</table>
				</div>
								</div>
							</div>
						</div>

</div>
	<div id="top_up" class="tab-pane fade">
					
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Your TopUps</h3>
								</div>
								<div class="panel-body">
<form class="form">
<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='topup_start_date'>
								         <input type='text' class="form-control" placeholder="From Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>

<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='topup_end_date'>
								         <input type='text' class="form-control" placeholder="To Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>


</form>

									<table id="topup_datatable" class="display table dt-responsive" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th class="text-left">ID</th>
												<th class="text-left">PIN</th>
												<th class="text-left">Transaction ID</th>
												<th class="text-left">Date</th>
												<th class="text-left">Details</th>
												
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="4" class="dataTables_empty">Loading data from server</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

  
<div id="casinocoin" class="tab-pane fade">
    <div class="col-md-6" style="padding-left: 10px; padding-right: 10px;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Deposit CSC</h3>
            </div>
            <div class="panel-body" style="text-align: left;">
				<?php
				   $curl = curl_init('http://localhost:3001/csc/depositinfo/'.$session->userinfo['id']);
				   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				   curl_setopt($curl, CURLOPT_POST, false);
				   curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				   $curl_response = curl_exec($curl);
				   curl_close($curl);
				   $json_deposit_info=json_decode($curl_response);
				   echo '<div class="col-sm-12"  style="text-align: left;">';
					echo 'To Deposit CasinoCoin (CSC) to your balance, open the BankRoll Manager and Deposit to CSCLotto.<br><br>Your Deposit Address and Destination Tag are shown below for reference.<br><br>';
				  echo '</div>';
				 echo '<div class="col-sm-12"  style="text-align: center;">';  
				 echo '<b>Deposit Address:</b> '.$json_deposit_info->depositAccountID.'<br>';

				  echo '<b>Destination Tag:</b> '.$json_deposit_info->tag.'<br>';
				   echo '</div>';
				?>
            </div>
        </div>
    </div>
    <div class="col-md-6" style="padding-left: 10px; padding-right: 10px;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Withdraw CSC</h3>
            </div>
		    <div class="panel-body" style="text-align: center;">
<?php
			  echo '<div class="col-sm-12"  style="text-align: left;">
			  To Withdraw CasinoCoin (CSC) to your BankRoll Manager, enter in the amount you wish to Withdraw and click Withdraw.<br><br>
				</div>';
  
?>
				<div class="col-sm-6">
                  <input style="margin-bottom: 5px;" class="form-control" id="csc_withdraw_amount"></input>
               </div>
              <div class="col-sm-6">
                  <button style="margin-bottom: 5px;" class="btn btn-magenta" id="execute_csc_withdraw">Withdraw</button>
                  <button style="margin-bottom:5px;" class="btn btn-magenta" id ="execute_csc_withdraw_all">Withdraw All</button>
               </div>
	    </div>
	</div>
    </div>
</div>
	
	<div id="deposite" class="tab-pane fade">
					
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Deposit</h3>
								</div>
								<div class="panel-body" style="text-align: center;">
<div class="col-md-12"><p>You can deposit money to your account using Credit Card. Enter the Amount you wish to deposit and klick on “Make Deposit” to proceed. Fill in your details on the next page to finish the deposit.</p></div>
<div class="col-md-12"><p>&nbsp;</p></div>
      <form class="form make_deposit_form" id="make_deposit_form" method="POST" action="https://www3.cenpos.net/webpay/v7/html5/?merchantid=12723080">							
		<div class="col-sm-12">
                       
			<div class="col-sm-3">
				<div class="form-group">
					<label for="amount">Amount</label>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<input type="text" class="form-control" id="deposit_amount" name="amount" required="required" >
					<input type="hidden" value="0" name="field2" >
					<?php if(!empty($_SERVER['SERVER_PORT'])) {?>
					<input type="hidden" value="<?php echo PROTOCOL.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];?>/payment.php" name="urlreturn" >
					<input type="hidden" value="<?php echo PROTOCOL.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];?>/my_account.php" name="urlcancel" >
					<?php
					}
					else{?>
					<input type="hidden" value="<?php echo PROTOCOL.$_SERVER['SERVER_NAME'];?>/payment.php" name="urlreturn" >
					<input type="hidden" value="<?php echo PROTOCOL.$_SERVER['SERVER_NAME'];?>/my_account.php" name="urlcancel" >
					
					<?php }?>
<input type="hidden" value="0" id="is_valid" >
				</div>
			</div>
		</div>
                 <div id="deposite_errors" class="alert alert-danger col-sm-12" style="display:none;">
					
					</div>
                 <div class="col-sm-3"></div>
                
		<div class="col-sm-6">
			<input type="submit" class="btn btn-magenta" id="action" name="action" value="Make Deposit"></input>
		</div>
	</form>
								</div>
							</div>
						</div>
					</div>
				
			</div>
		</div>
	</div>



	<div class="modal fade" id="change_password_modal" tabindex="-1" role="dialog" aria-labelledby="change_password_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="change_password_modal_label">Change Password</h4>
				</div>
				<div class="modal-body clearfix">
					<form class="form-horizontal">
						<div class="form-group">
					    	<label class="col-sm-4 control-label" for="current_password">Current Password:</label>
					    	<div class="col-sm-8">
					    		<input type="password" class="form-control" name="current_password" id="current_password" placeholder="Current Password"></input>
					    	</div>
						</div>
						<div class="form-group">
					    	<label class="col-sm-4 control-label" for="new_password">New Password:</label>
					    	<div class="col-sm-8">
					    		<input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password"></input>
					    	</div>
						</div>
						<div class="form-group">	
					    	<label class="col-sm-4 control-label" for="repeat_new_password">Repeat New Password:</label>
					    	<div class="col-sm-8">
					    		<input type="password" class="form-control" name="repeat_new_password" id="repeat_new_password" placeholder="Repeat New Password"></input>
					    	</div>
						</div>
					</form>
					
					<div id="change_password_errors" class="alert alert-danger col-sm-12" style="display:none;">
					
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="change_password_confirm">Change</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="change_pin_modal" tabindex="-1" role="dialog" aria-labelledby="change_pin_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="change_pin_modal_label">Change CSCLotto ATM PIN</h4>
				</div>
				<div class="modal-body clearfix">
					<form class="form-horizontal">
						<div class="form-group">
					    	<label class="col-sm-4 control-label" for="change_pin_password">Password:</label>
					    	<div class="col-sm-8">
					    		<input type="password" class="form-control" name="change_pin_password" id="change_pin_password" placeholder="Account Password"></input>
					    	</div>
						</div>
						<div class="form-group">
					    	<label class="col-sm-4 control-label" for="new_pin">New Pin:</label>
					    	<div class="col-sm-8">
					    		<input type="password" class="form-control" name="new_pin" id="new_pin" placeholder="New Pin"></input>
					    	</div>
						</div>
						<div class="form-group">
					    	<label class="col-sm-4 control-label" for="repeat_new_pin">Repeat New Pin:</label>
					    	<div class="col-sm-8">
					    		<input type="password" class="form-control" name="repeat_new_pin" id="repeat_new_pin" placeholder="Repeat New Pin"></input>
					    	</div>
						</div>
					</form>
					
					<div id="change_pin_errors" class="alert alert-danger col-sm-12" style="display:none;">
					
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="change_pin_confirm">Change</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="set_limits_modal" tabindex="-1" role="dialog" aria-labelledby="set_limits_modal_label" aria-hidden="true"> 
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="set_limits_modal_label">Self Limits</h4>
				</div>
				<div class="modal-body clearfix">
					<form class="form-horizontal">
						<div class="form-group">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="losses_limited">Losses:</label>
								<div class="col-sm-7">
									<input id="losses_limited" type="checkbox" data-toggle="toggle" data-on="Limited" data-off="No Limits" data-width="130">
								</div>
							</div>
						</div>
					
						<div id="loss_limit" class="form-group">
					    	<label class="col-sm-4 control-label" for="daily_loss_limit">Daily Loss Limit:</label>
					    	<div class="col-sm-4">
					    		<input type="text" class="form-control" name="daily_loss_limit" id="daily_loss_limit" placeholder="0.00" value="<?php echo $session->userinfo['customers']['daily_loss_limit']; ?>"></input>
					    	</div>
						</div>
						
						<div class="form-group">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="wagers_limited">Wagers:</label>
								<div class="col-sm-7">
									<input id="wagers_limited" type="checkbox" data-toggle="toggle" data-on="Limited" data-off="No Limits" data-width="130">
								</div>
							</div>
						</div>
						
						<div id="wager_limit" class="form-group">
					    	<label class="col-sm-4 control-label" for="daily_wager_limit">Daily Wager Limit:</label>
					    	<div class="col-sm-4">
					    		<input type="text" class="form-control" name="daily_wager_limit" id="daily_wager_limit" placeholder="0.00" value="<?php echo $session->userinfo['customers']['daily_wager_limit']; ?>"></input>
					    	</div>
						</div>
						
						<div class="form-group">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="deposits_limited">Deposits:</label>
								<div class="col-sm-7">
									<input id="deposits_limited" type="checkbox" data-toggle="toggle" data-on="Limited" data-off="No Limits" data-width="130">
								</div>
							</div>
						</div>
						
						<div id="deposit_limit" class="form-group">
					    	<label class="col-sm-4 control-label" for="daily_deposit_limit">Daily Deposit Limit:</label>
					    	<div class="col-sm-4">
					    		<input type="text" class="form-control" name="daily_deposit_limit" id="daily_deposit_limit" placeholder="0.00" value="<?php echo $session->userinfo['customers']['daily_deposit_limit']; ?>"></input>
					    	</div>
						</div>
						
						<div class="form-group">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="withdrawals_limited">Withdrawals:</label>
								<div class="col-sm-7">
									<input id="withdrawals_limited" type="checkbox" data-toggle="toggle" data-on="Limited" data-off="No Limits" data-width="130">
								</div>
							</div>
						</div>
						
						<div id="withdrawal_limit"  class="form-group">
					    	<label class="col-sm-4 control-label" for="daily_withdrawal_limit">Daily Withdrawal Limit:</label>
					    	<div class="col-sm-4">
					    		<input type="text" class="form-control" name="daily_withdrawal_limit" id="daily_withdrawal_limit" placeholder="0.00" value="<?php echo $session->userinfo['customers']['daily_withdrawal_limit']; ?>"></input>
					    	</div>
						</div>
						
						<div class="form-group">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="sessions_limited">Sessions:</label>
								<div class="col-sm-7">
									<input id="sessions_limited" type="checkbox" data-toggle="toggle" data-on="Limited" data-off="No Limits" data-width="130">
								</div>
							</div>
						</div>
						
						<div id="session_limit"  class="form-group">
					    	<label class="col-sm-4 control-label" for="daily_session_limit">Daily Session Limit:</label>
					    	<div class="col-sm-4">
					    		<input type="text" class="form-control" name="daily_session_limit" id="daily_session_limit" placeholder="0" value="<?php echo $session->userinfo['customers']['daily_session_limit']; ?>"></input>
					    	</div>
							<label class="col-sm-2 control-label" for="daily_session_limit" style="text-align: left;">minutes</label>
						</div>
						
						<div class="form-group">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="transactions_limited">Transactions:</label>
								<div class="col-sm-7">
									<input id="transactions_limited" type="checkbox" data-toggle="toggle" data-on="Limited" data-off="No Limits" data-width="130">
								</div>
							</div>
						</div>
						
						<div id="tx_limit" class="form-group">
					    	<label class="col-sm-4 control-label" for="transaction_limit">Transaction Limit:</label>
					    	<div class="col-sm-4">
					    		<input type="text" class="form-control" name="transaction_limit" id="transaction_limit" placeholder="0.00" value="<?php echo $session->userinfo['customers']['transaction_limit']; ?>"></input>
					    	</div>
						</div>
						
						<div class="form-group">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="locked">Lock Account:</label>
								<div class="col-sm-7">
									<input id="locked" type="checkbox" data-toggle="toggle" data-on="Locked" data-off="Not Locked" data-width="130">
								</div>
							</div>
						</div>
						
						<div id="lock_expire_option" class="form-group" style="display: none;">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="locked">Account Lock Expiration:</label>
								<div class="col-sm-7">
									<label class="control-label">Note that this change will not take effect until you logout.</label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="locked"></label>
								<div id="lockout_expiration_contain" class="col-sm-7">
									<div class="dtp-inlne" id="locked_expiration"></div>
								</div>
							</div>
						</div>

						<div class="col-sm-12" style="text-align: center;">
							<p><b>Note:</b> To allow remove a limit, set the slider to the "No Limits" setting. <br> Otherwise, a limit will be set. Entering zero will block everything.</p>
						</div>
					</form>
					
					<div id="set_limits_errors" class="alert alert-danger col-sm-12" style="display:none;">
					
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="save_limits">Change</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="receipt_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="receipt_modal_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="receipt_modal_label">Print Preview</h4>
				</div>
				<div class="modal-body clearfix" style="text-align:center;">
					<iframe id="receipt" name="receipt" height="500px" src="" seamless width="90%"></iframe>
					<div class="col-sm-12">
						<div id="receipt_error" class="alert alert-danger" role="alert" style="display:none;">

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="cancel_receipt" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="verify_modal" tabindex="-1" role="dialog" aria-labelledby="change_pin_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="change_pin_modal_label">Verify Identity</h4>
				</div>
				<div class="modal-body clearfix">
			    	<div class="col-sm-12">
						<?php include('upload_files.php'); ?>
			    	</div>
					
					<?php 
						$checked_drivers = $session->userinfo['customers']['is_drivers_license_verified'] == 1 ? "checked" : "";
						$checked_voters = $session->userinfo['customers']['is_voters_card_verified'] == 1 ? "checked" : "";
						$checked_passport = $session->userinfo['customers']['is_passport_verified'] == 1 ? "checked" : "";
						$checked_national_security = $session->userinfo['customers']['is_national_security_verified'] == 1 ? "checked" : "";
					?>
					
					<div class="col-sm-12">
						<label class="col-sm-3 control-label" for="drivers_license">Driver's License</label>
						<div class="col-sm-4">
							<input disabled type="text" id="drivers_license" class="form-control" value="<?php echo $session->userinfo['customers']['drivers_license']; ?>">
						</div>
						<div class="col-sm-5">
							<input disabled <?php echo $checked_drivers; ?> id="verified_drivers_license" type="checkbox" data-toggle="toggle" data-on="Verified" data-off="Not Verified" data-width="120">
						</div>
						<br><br>
					</div>

					<div class="col-sm-12">
						<label class="col-sm-3 control-label" for="voters_card">Voter's Card</label>
						<div class="col-sm-4">
							<input disabled type="text" id="voters_card" class="form-control" value="<?php echo $session->userinfo['customers']['voters_card']; ?>">
						</div>
						<div class="col-sm-5">
							<input disabled <?php echo $checked_voters; ?> id="verified_voters_card" type="checkbox" data-toggle="toggle" data-on="Verified" data-off="Not Verified" data-width="120">
						</div>
						<br><br>
					</div>

					<div class="col-sm-12">
						<label class="col-sm-3 control-label" for="passport">Passport</label>
						<div class="col-sm-4">
							<input disabled type="text" id="passport" class="form-control" value="<?php echo $session->userinfo['customers']['passport']; ?>">
						</div>
						<div class="col-sm-5">
							<input disabled <?php echo $checked_passport; ?> id="verified_passport" type="checkbox" data-toggle="toggle" data-on="Verified" data-off="Not Verified" data-width="120">
						</div>
						<br><br>
					</div>
					
					<div class="col-sm-12">
						<label class="col-sm-3 control-label" for="national_security">National Security</label>
						<div class="col-sm-4">
							<input disabled type="text" id="national_security" class="form-control" value="<?php echo $session->userinfo['customers']['national_security']; ?>">
						</div>
						<div class="col-sm-5">
							<input disabled <?php echo $checked_national_security; ?> id="verified_national_security" type="checkbox" data-toggle="toggle" data-on="Verified" data-off="Not Verified" data-width="120">
						</div>
						<br><br>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	
<?php	



	include("footer.php");
?>
