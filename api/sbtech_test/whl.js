/*global window, $ */
// STATUS
function whl() {
    'use strict';
    this.errorMessages = new Object();
    this.errorMessages[-1] = "Unknown error.";
    this.errorMessages[-100] = "Invalid input parameters.";
    this.errorMessages[-102] = "Username is not entered.";
    this.errorMessages[-104] = "Password is not entered";
    this.errorMessages[-205] = "Username/password is not correct.";
    this.errorMessages[-7] = "User account is locked.";
    this.siteURL = "http://asurewin.testing.sbtech.com";
    this.linkLogin = this.siteURL + "/Login.html";
    this.linkStatus = this.siteURL + "/Status.html";
    this.linkLogout = this.siteURL + "/Logout.html";
    this.linkReg = "/register.html";
    this.linkDeposits = "/deposit.html";
    this.linkWithdrawals = "/withdraw.html";
    this.linkChangePassword = "/changePassword.html";
    this.linkProfile = "/profile.html";
    this.DEF_PANEL_WIDTH = 800;
    this.DEF_PANEL_HEIGHT = 550;
    this.PANEL_NAME = "panel_popup";
}
whl.prototype.status = function (callback) {
    'use strict';
    this.status_callback = callback;
    var that = this;
	that.statusCallback();
    //$.ajax({
        //type: 'POST',
        //url: this.linkStatus,
        //crossDomain: true,
        //dataType: "jsonp",
        //jsonp: false,
        //jsonpCallback: 'jsoncb',
        //data: {},
        //success: function (data)
        //{
            //that.statusCallback(data);
        //}
    //});
}

whl.prototype.statusCallback = function (data) {
    'use strict';
    if (this.status_callback) {
        var result = new Object();
        result.uid = "";
        result.token = "";
        result.status = "real";
        result.balance = "100 $";
        // result.uid = $(data).find("uid").text();
        // result.token = $(data).find("token").text();
        // result.status = $(data).find("status").text();
        this.status_callback(result);
    }
}

// REFRESH
whl.prototype.refreshSession = function (callback) {
    'use strict';
    this.refresh_callback = callback;
    var that = this;
    that.refreshCallback();
    // $.ajax({
    //     type: 'POST',
    //     url: this.linkStatus,
    //     crossDomain: true,
    //     dataType: "jsonp",
    //     jsonp: false,
    //     jsonpCallback: 'jsoncb',
    //     data: {},
    //     success: function (data) {
    //         that.refreshCallback(data);
    //     }
    // });
}
whl.prototype.refreshCallback = function (data)
{
    'use strict';
    if (this.refresh_callback)
    {
        var result = new Object();
        result.token = "";
        result.status = "success";
        result.balance = "100 $";
        this.refresh_callback(result);
        
        // var token = $(data).find("token").text();
        // if (token != null && token.length > 0 && token != "null")
        // {
        //     result.status = "success";
        // } else {
        //     result.status = "failure";
        // }
    }
}