<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/api/sbtech/api.php");
	
	$sports_api = new SPORTS_API();

	if(isset($_GET['auth_token'])){
		$sports_api->validate_token($_GET['auth_token']);
	}else{
		$sports_api->general_error();
	}