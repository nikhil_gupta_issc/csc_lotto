<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/api/sbtech/api.php");
	
	$sports_api = new SPORTS_API();

	switch ($_GET['action']){
		case "generate_token":
			$sports_api->generate_api_key();
			break;
		case "validate_token":
			if(isset($_GET['auth_token'])){
				$sports_api->validate_token($_GET['auth_token']);
			}
			break;
		case "get_customer_balance":
			if(isset($_GET['cust_id'])){
				$sports_api->get_customer_balance($_GET['cust_id']);
			}
			break;
		case "reserve":
			if(isset($_GET['cust_id']) && isset($_GET['reserve_id']) && isset($_GET['amount'])){
				$sports_api->reserve($_GET['cust_id'], $_GET['reserve_id'], $_GET['amount']);
			}
			break;
		case "debit_reserve":
			if(isset($_GET['cust_id']) && isset($_GET['reserve_id']) && isset($_GET['amount']) && isset($_GET['req_id'])){
				$sports_api->debit_reserve($_GET['cust_id'], $_GET['reserve_id'], $_GET['amount'], $_GET['req_id'], $_POST);
			}
			break;
		case "debit_customer":
			if(isset($_GET['cust_id']) && isset($_GET['req_id']) && isset($_GET['amount'])){
				$sports_api->debit_reserve($_GET['cust_id'], $_GET['req_id'], $_GET['amount'], $_POST);
			}
			break;
		case "credit_customer":
			if(isset($_GET['cust_id']) && isset($_GET['req_id']) && isset($_GET['amount'])){
				$sports_api->credit_customer($_GET['cust_id'], $_GET['req_id'], $_GET['amount'], $_POST);
			}
			break;
		case "cancel_reserve":
			if(isset($_GET['cust_id']) && isset($_GET['reserve_id'])){
				$sports_api->cancel_reserve($_GET['cust_id'], $_GET['reserve_id']);
			}
			break;
		case "commit_reserve":
			if(isset($_GET['cust_id']) && isset($_GET['reserve_id'])){
				$sports_api->commit_reserve($_GET['cust_id'], $_GET['reserve_id']);
			}
			break;
	}