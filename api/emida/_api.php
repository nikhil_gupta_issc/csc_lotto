<?php
	error_reporting(E_ERROR);
	ini_set('display_errors', 1);
	ini_set("soap.wsdl_cache_enabled", "0");
	ini_set('soap.wsdl_cache_ttl','0');

	class PHONE_CARD_API {
		//private $wsdl_url = "https://webservice.emida.net/soap/webServices.wsdl";
		private $wsdl_url = "https://support.btcbahamas.com:8443/soap/webServices.wsdl";
		private $rpc_url = "https://support.btcbahamas.com:8443/soap/servlet/rpcrouter";
		private $site_id;
		private $clerk_id;
		private $version = "01";
		private $language_option = "01";
		private $soap;
		private $username;
		private $password;
		
		function __construct(){
			global $db;
			
			$settings = $db->getSettings();
			
			$this->username = $settings["emida_username"];
			$this->password = $settings["emida_password"];
			
			$this->soap = new SOAPClient($this->wsdl_url, 
				array(
					'soap_version' => SOAP_2_3,
					'trace' => 1, 
					'exceptions' => true
				)
			);
		}
		
		public function test_api(){
			// test all methods of class
			$methods = get_class_methods($this);
			foreach($methods as $method){
				if($method != "__construct" && $method != "test_api"){					
					$r = new \ReflectionMethod('PHONE_CARD_API', $method);
					$params = $r->getParameters();
					$test_arr = array();
					foreach ($params as $param){
						// build test param string
						$param_name = $param->getName();
						$test_arr[$param_name] = $param_name;
					}
					
					// execute test
					echo "\n\n<br><br>Testing $method:\n<br>====================================================================================<br><br>\n\n";
					
					$r->invokeArgs( new PHONE_CARD_API(), $test_arr);
				}
			}
		}
		
		public function test_connection(){
			$response = $this->soap->CommTest();
			echo $response;
		}
		
		public function pin_dist_sale($product_id, $account_id, $amount, $invoice_no = ""){
			global $core;
			
			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $this->clerk_id;
			$params['productId'] = $product_id;
			$params['accountId'] = $account_id;
			$params['amount'] = $amount;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('PinDistSale', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}
			
			return $core->xml_to_array($response);
		}
		
		public function login2($username = null, $password = null){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['userName'] = $this->username;
			$params['password'] = $this->password;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('Login2', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			$return = $core->xml_to_array($response);
			$this->site_id = $return["SiteID"];
			$this->clerk_id = $return["ClerkId"];
			
			return $return;
		}
		
		public function transaction_confirm($transaction_id){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['transactionId'] = $transaction_id;
			
			try {
				$response = $this->soap->__soapCall('TransactionConfirm', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function get_product_list($transaction_type_id = "", $carrier_id = "", $category_id = "", $product_id = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['transTypeId'] = $transaction_type_id;
			$params['carrierId'] = $carrier_id;
			$params['categoryId'] = $category_id;
			$params['productId'] = $product_id;
			
			try {
				$response = $this->soap->__soapCall('GetProductList', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			$products = $core->xml_to_array($response);
			$return = array();
			if(!array_key_exists("Description",$products["Product"])){
				foreach($products['Product'] as $product){
					$return[] = $product;
				}
			}else{
				$return[] = $products['Product'];
			}
			return $return;	
		}
		
		public function get_carrier_list($transaction_type_id = "", $carrier_id = "", $category_id = "", $product_id = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['transTypeId'] = $transaction_type_id;
			$params['carrierId'] = $carrier_id;
			$params['categoryId'] = $category_id;
			$params['productId'] = $product_id;
			
			try {
				$response = $this->soap->__soapCall('GetCarrierList', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			$carriers = $core->xml_to_array($response);
			$return = array();
			if(!array_key_exists("Description",$carriers["Carrier"])){
				foreach($carriers['Carrier'] as $carrier){
					$return[] = $carrier;
				}
			}else{
				$return[] = $carriers['Carrier'];
			}
			return $return;
		}
		
		public function get_category_list($transaction_type_id = "", $carrier_id = "", $category_id = "", $product_id = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['transTypeId'] = $transaction_type_id;
			$params['carrierId'] = $carrier_id;
			$params['categoryId'] = $category_id;
			$params['productId'] = $product_id;
			
			try {
				$response = $this->soap->__soapCall('GetCategoryList', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			$categories = $core->xml_to_array($response);
			$return = array();
			if(!array_key_exists("Description",$categories["Category"])){
				foreach($categories['Category'] as $category){
					$return[] = $category;
				}
			}else{
				$return[] = $categories['Category'];
			}
			return $return;
		}
		
		public function get_trans_type_list($transaction_type_id = "", $carrier_id = "", $category_id = "", $product_id = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['transTypeId'] = $transaction_type_id;
			$params['carrierId'] = $carrier_id;
			$params['categoryId'] = $category_id;
			$params['productId'] = $product_id;
			
			try {
				$response = $this->soap->__soapCall('GetTransTypeList', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function pin_return_request($clerk_id, $account_id, $amount, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = 812;
			$params['accountId'] = $account_id;
			$params['amount'] = $amount;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('PINReturnRequest', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function trans_refund($clerk_id, $account_id, $amount, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = 815;
			$params['accountId'] = $account_id;
			$params['amount'] = $amount;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('TransRefund', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function get_account_balance($merchant_id){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['merchantId'] = $merchant_id;
			
			try {
				$response = $this->soap->__soapCall('GetAccountBalance', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function get_bill_payment_balance($clerk_id, $product_id, $account_id, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['accountId'] = $account_id;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('GetBillPaymentBalance', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function bill_payment($clerk_id, $product_id, $account_id, $amount, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['accountId'] = $account_id;
			$params['amount'] = $amount;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('BillPayment', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function gift_card_query_fee($clerk_id, $product_id, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('GiftCardQueryFee', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function gift_card_activate($clerk_id, $product_id, $serial_number, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['serialNumber'] = $serial_number;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('GiftCardActivate', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function gift_card_balance($clerk_id, $product_id, $card_number, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['cardNumber'] = $card_number;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('GiftCardBalance', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function gift_card_deactivate($clerk_id, $product_id, $transaction_id, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['transactionId'] = $transaction_id;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('GiftCardDeactivate', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function stored_value_query_fee($clerk_id, $product_id, $operation, $amount, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['operation'] = $operation;
			$params['amount'] = $amount;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('StoredValueQueryFee', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function stored_value_activate($clerk_id, $product_id, $serial_number, $amount, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['serialNumber'] = $serial_number;
			$params['amount'] = $amount;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('StoredValueActivate', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function stored_value_load($clerk_id, $product_id, $card_number, $amount, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['cardNumber'] = $card_number;
			$params['amount'] = $amount;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('StoredValueLoad', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function stored_value_balance($clerk_id, $product_id, $card_number, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['cardNumber'] = $card_number;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('StoredValueBalance', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function stored_value_refund($clerk_id, $product_id, $transaction_id, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['transactionId'] = $transaction_id;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('StoredValueRefund', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function stored_value_deactivate($clerk_id, $product_id, $transaction_id, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['productId'] = $product_id;
			$params['transactionId'] = $transaction_id;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('StoredValueDeactivate', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function check_transaction_by_invoice_no($clerk_id, $invoice_no){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['invoiceNo'] = $invoice_no;
			
			try {
				$response = $this->soap->__soapCall('CheckTransactionByInvoiceNo', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function get_b_product_list(){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			
			try {
				$response = $this->soap->__soapCall('GetBProductList', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function check_provider_status($product_id){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['productId'] = $product_id;
			
			try {
				$response = $this->soap->__soapCall('CheckProviderStatus', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function get_product_fee($amount){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['amount'] = $amount;
			
			try {
				$response = $this->soap->__soapCall('GetProductFee', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function pin_dist_sale_ussd($phone_number, $clerk_id, $product_id, $account_id, $amount, $invoice_no = ""){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['phoneNumber'] = $phone_number;
			$params['clerkId'] = $this->clerk_id;
			$params['productId'] = $product_id;
			$params['accountId'] = $account_id;
			$params['amount'] = $amount;
			$params['invoiceNo'] = $invoice_no;
			$params['languageOption'] = $this->language_option;
			
			try {
				$response = $this->soap->__soapCall('PinDistSaleUSSD', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function look_up_transaction_by_invoice_no($clerk_id, $invoice_no){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['terminalId'] = $this->site_id;
			$params['clerkId'] = $clerk_id;
			$params['invoiceNo'] = $invoice_no;
			
			try {
				$response = $this->soap->__soapCall('LookUpTransactionByInvoiceNo', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function apply_payment($merchant_id, $amount, $description = "", $ref_number = "", $username, $password){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['merchantId'] = $merchant_id;
			$params['amount'] = $amount;
			$params['description'] = $description;
			$params['refNumber'] = $ref_number;
			$params['userName'] = $username;
			$params['password'] = $password;
			
			try {
				$response = $this->soap->__soapCall('ApplyPayment', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
		
		public function get_merchant_balance($merchant_id, $username, $password){
			global $core;

			$params = array();
			$params['version'] = $this->version;
			$params['merchantId'] = $merchant_id;
			$params['userName'] = $username;
			$params['password'] = $password;
			
			try {
				$response = $this->soap->__soapCall('GetMerchantBalance', 
					$params
				);
			} catch (SoapFault $e ){
				print_r($e);
			}

			return $core->xml_to_array($response);
			echo $response;
		}
	}
?>
