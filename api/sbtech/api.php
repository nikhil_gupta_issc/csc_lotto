<?php
	error_reporting(E_ERROR);
	ini_set('display_errors', 1);

	class SPORTS_API {
		private $url = "http://seamlessapitester.sbtech.com/";

		private function parse_between($string, $start_str, $end_str){
			$string = " ".$string;
			$ini = strpos($string, $start_str);
			if ($ini == 0) return "";
			$ini += strlen($start_str);
			$len = strpos($string, $end_str, $ini) - $ini;
			return substr($string, $ini, $len);
		}
		
		private function parse_after($string, $search_str){
			$end_pos = strlen($string)+1;
			$string = " ".$string;
			$start_pos = strpos($string, $search_str);
			if ($start_pos == 0) return "";
			$start_pos += strlen($search_str);
			$len = $end_pos - $start_pos;
			return substr($string, $start_pos, $len);
		}
		
		private function parse_before($string, $search_str){
			$start_pos = 1;
			$string = " ".$string;
			$end_pos = strpos($string, $search_str);
			if ($end_pos == 0) return "";
			$len = $end_pos - $start_pos;
			return substr($string, $start_pos, $len);
		}		
		
		public function test_connection(){
			return true;
		}

		public function general_error(){
			echo "error_code=-10\r\n";
			echo "error_message=A parameter was expected and not sent.";
		}
		
		public function generate_api_key(){
			global $db;
			
			$ip = $_SERVER['REMOTE_ADDR'];
		
			// create hash using username and salt with timestamp
			$salt = md5(microtime().rand());
			$secret_key = $ip;
			
			// Compute the signature by hashing the salt with the secret key
			$api_key = hash_hmac('ripemd160', $salt, $secret_key);		

			// save to database
			$q = "REPLACE INTO sbtech_api_keys "
					."(api_key, salt, ip_address, user_id) "
				."VALUES ('$api_key', '$salt', '$ip', 1)";
			$db_result = $db->queryDirect($q);
			
			if($db_result){
				echo "token=$api_key\r\n";
			}else{
				echo "success=false\r\n";
				echo "message=Database error executing query: $q\r\n";
			}
		}
		
		public function validate_token($token){
			global $db;
			global $core;
			
			//$ip = $_SERVER['REMOTE_ADDR'];
			
			// see if this token is in the database
			$q = "SELECT * FROM users WHERE userid='$token' LIMIT 1";
			$db_result = $db->queryOneRow($q);
			
			// if we found a match, verify hash match 
			if(!empty($db_result)){
				//$secret_key = $ip;
				//$salt = $db_result['salt'];
				
				// Compute the signature by hashing the salt with the secret key as the key
				//$expected_hash = hash_hmac('ripemd160', $salt, $secret_key);		
			
				//if($expected_hash == $token){
					// success
					echo "error_code=0\r\n";
					echo "error_message=\r\n";

					// Check balance
					$bal = $core->check_balance("customers", $db_result['id']);
					$ending_balance = $bal['available_balance'];
					
					echo "cust_id=".$db_result['id']."\r\n";
					echo "cust_login=".$db_result['username']."\r\n";
					echo "currency_code=USD\r\n";
					echo "city=city\r\n";
					echo "country=country\r\n";
					echo "balance=".$bal['available_balance'];
				//}else{
				//	echo "error_code=-3\r\n";
				//	echo "error_message=Invalid Token\r\n";
				//}
			}else{
				echo "error_code=-3\r\n";
				echo "error_message=Invalid Token\r\n";
			}
		}
		
		public function get_customer_balance($cust_id){
			global $core;
			
			//Check balance
			if($core->remaining_loss_limit() != -1){
				$ending_balance = $core->remaining_loss_limit();
			}else{
				$bal = $core->check_balance("customers", $cust_id);
				$ending_balance = $bal['available_balance'];
			}
			
			echo "error_code=0\r\n";
			echo "error_message=\r\n";
			echo "balance=".$ending_balance;
		}
		
		private function is_valid_callback($subject){
			$identifier_syntax
			  = '/^[$_\p{L}][$_\p{L}\p{Mn}\p{Mc}\p{Nd}\p{Pc}\x{200C}\x{200D}]*+$/u';

			$reserved_words = array('break', 'do', 'instanceof', 'typeof', 'case',
			  'else', 'new', 'var', 'catch', 'finally', 'return', 'void', 'continue', 
			  'for', 'switch', 'while', 'debugger', 'function', 'this', 'with', 
			  'default', 'if', 'throw', 'delete', 'in', 'try', 'class', 'enum', 
			  'extends', 'super', 'const', 'export', 'import', 'implements', 'let', 
			  'private', 'public', 'yield', 'interface', 'package', 'protected', 
			  'static', 'null', 'true', 'false');

			return preg_match($identifier_syntax, $subject)
				&& ! in_array(mb_strtolower($subject, 'UTF-8'), $reserved_words);
		}
		
		public function whl_get_customer_balance($token){
			global $core;
			global $db;
			
			$result = array();
			
			// get customer id from token
			$q = "SELECT * FROM users WHERE userid='$token' LIMIT 1";
			$db_result = $db->queryOneRow($q);
			
			// if we found a match, verify hash match 
			if(!empty($db_result)){
				// Check balance
				$bal = $core->check_balance("customers", $db_result['id']);
				$result['balance'] = $bal['available_balance']." USD";
				$result['uid'] = $db_result['id'];
				$result['token'] = $token;
				$result['message'] = "";
				if($_GET['callback'] == "refresh_cb"){
					$result['status'] = "success";
				}else{
					$result['status'] = "real";
				}
			}else{
				$result['balance'] = "0 USD";
				$result['message'] = "";
				$result['uid'] = "-1";
				$result['token'] = $token;
				if($_GET['callback'] == "refresh_cb"){
					$result['status'] = "failure";
				}else{
					$result['status'] = "anon";
				}
			}
			
			$json = json_encode($result);
			
			if($this->is_valid_callback($_GET['callback']) == true){
				header('Content-Type: text/javascript; charset=utf8');
				header('Access-Control-Allow-Origin: *');
				header('Access-Control-Max-Age: 3628800');
				header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
				echo $_GET['callback']."(".$json.")";
			}else{
				header('status: 400 Bad Request', true, 400);
			}
		}

		public function reserve($cust_id, $reserve_id, $amount){
			global $db;
			global $core;
			
			$error_code = 0;
			$error_message = "\r\n";
			
			//Check for valid customer
			if(!$this->customer_exists($cust_id)){
				$error_code = -2;
				$error_message = "CustomerNotFound";
				$trans_id = 0;
			}
			
			//Make sure that the amount is greater than zero
			if($amount < 0){
				$error_code = -1;
				$error_message = "The reserve amount is invalid";
				$trans_id = 0;
			}
			
			//Make sure the customer has sufficient funds
			$bal = $core->check_balance("customers", $cust_id);
			$ending_balance = $bal['available_balance'];
			
			if($error_code==0){
				if($bal['available_balance'] < $amount){
					$error_code = -4;
					$error_message = "InsufficientFunds";
					$trans_id = 0;
				}
			}
			
			if($error_code==0){
				//Create the reserve in the database
				if($this->reserve_exists($reserve_id)){
					$q = "SELECT * FROM `sports_reserve` WHERE `reserve_id`=%i";
					$res = $db->queryOneRow($q, array($reserve_id));
					$trans_id = $res['trans_id'];
					$amount = $res['amount_total'];
				}else{
					$trans_id = $core->make_customer_transaction(-$amount, 17, "Sports bet system bet", "NULL", $cust_id);
					
					if($trans_id == "Self-defined loss limit will be exceeded, no transaction was made!"){
						$error_code = -4;
						$error_message = "Self-defined loss limit will be exceeded, no transaction was made!";
						$trans_id = 0;
					}else{
						$q = "INSERT INTO `sports_reserve` (`reserve_id`,`user_id`,`status`,`amount_total`,`trans_id`) VALUES (%i,%i,0,%f,%i)";
						$db->queryInsert($q, array($reserve_id, $cust_id, $amount, $trans_id));
					}
				}
			}
			
			echo "error_code=".$error_code."\r\n";
			echo "error_message=".$error_message."\r\n";
			echo "trx_id=".$trans_id."\r\n";
			echo "balance=".$amount;
		}
		
		public function debit_reserve($cust_id, $reserve_id, $amount, $req_id, $xml){
			global $db;
			
			if(!$this->request_exists($req_id)){
				if($this->check_reserve_remaining($reserve_id) >= $amount){
					$error_message = "\r\n";
					
					//Check to see if the reserve exists
					if(!$this->reserve_exists($reserve_id)){
						$error_code = -20;
						$error_message = "Reserve Doesn't Exist";
					}
					
					//Check to see if the reserve is open
					if($this->reserve_open($reserve_id)){
						$error_code = 0;
					}else{
						$error_code = -22;
						$error_code = "The reserve is not open";
					}
					
					if($error_code == 0){
						$q = "SELECT `amount_used` FROM `sports_reserve` WHERE `reserve_id`=%i";
						$res = $db->queryOneRow($q, array($reserve_id));
						$new_amount_used = $res['amount_used'] + $amount;
						
						$q = "UPDATE `sports_reserve` SET `amount_used` = %f WHERE `reserve_id`=%i";
						$db->queryDirect($q, array($new_amount_used, $reserve_id));
					}
					
					//Add the request
					$q = "INSERT INTO `sports_request` (`request_id`,`request_type`,`amount`,`user_id`,`http_body`,`ending_balance`,`trans_id`,`error_code`,`error_message`) VALUES (%i,'Debit Reserve',%f,%s,%s,NULL,%i,%i,%s)";
					$db->queryInsert($q, array($req_id, $amount, $cust_id, $xml, $req_id, $error_code, $error_message));
					
					echo "error_code=".$error_code."\r\n";
					echo "error_message=".$error_message."\r\n";
					echo "trx_id=".$req_id."\r\n";
					echo "balance=".$amount;
				}else{
					//Add the request
					$error_code = -21;
					$error_message = "AvailableBalanceExceeded";
					
					$q = "INSERT INTO `sports_request` (`request_id`,`request_type`,`amount`,`user_id`,`http_body`,`ending_balance`,`trans_id`,`error_code`,`error_message`) VALUES (%i,'Debit Reserve',%f,%s,%s,NULL,%i,%i,%s)";
					$db->queryInsert($q, array($req_id, $amount, $cust_id, $xml, $req_id, $error_code, $error_message));
					
					echo "error_code=".$error_code."\r\n";
					echo "error_message=".$error_message."\r\n";
					echo "trx_id=0\r\n";
					echo "balance=".$amount;
				}
			}else{
				$req = $this->get_request_info($req_id);
				echo "error_code=".$req['error_code']."\r\n";
				echo "error_message=".$req['error_code']."\r\n";
				echo "trx_id=".$req_id."\r\n";
				echo "balance=".$req['amount'];			
			}
		}
		
		public function debit_customer($cust_id, $req_id, $amount, $xml){
			global $db;
			global $core;
			
			if($this->customer_exists($cust_id)){
				//Check to see if the request exists
				if(!$this->request_exists($req_id)){
					//Make customer transaction
					$trans_id = $core->make_customer_transaction(-$amount, 45, "Sports bet system withdrawal", "NULL", $cust_id);
					$trans_id = $trans_id === false ? $req_id : $trans_id;
				
					//Check new balance
					$bal = $core->check_balance("customers", $cust_id);
					$ending_balance = $bal['available_balance'];
					
					if($trans_id == "The user doesn't have enough money to withdrawal!"){
						$error_code = -4;
						$error_message = "InsufficientFunds";
						$trans_id = $req_id;
						
						//Create a request record
						$q = "INSERT INTO `sports_request` (`request_id`,`request_type`,`amount`,`user_id`,`http_body`,`ending_balance`,`trans_id`,`error_code`,`error_message`) VALUES (%i,'Debit Customer',%i,%s,%s,%i,%i,%i,%s)";
						$db->queryInsert($q, array($req_id, $amount, $cust_id, $xml, $ending_balance, $trans_id, $error_code, $error_message));
						
						echo "error_code=".$error_code."\r\n";
						echo "error_message=".$error_message."\r\n";
						echo "balance=".$amount."\r\n";
						echo "trx_id=".$trans_id;
						return;
					}
					
					//Create a request record
					$error_code = 0;
					$error_message = "\r\n";
					$q = "INSERT INTO `sports_request` (`request_id`,`request_type`,`amount`,`user_id`,`http_body`,`ending_balance`,`trans_id`,`error_code`,`error_message`) VALUES (%i,'Debit Customer',%i,%s,%s,%i,%i,%i,%s)";
					$db->queryInsert($q, array($req_id, $amount, $cust_id, $xml, $ending_balance, $trans_id, $error_code, $error_message));
				}else{
					$req = $this->get_request_info($req_id);
					$ending_balance = $req['ending_balance'];
					$trans_id = $req['trans_id'];
					$amount = $req['amount'];
					$error_code = $req['error_code'];
					$error_message = $req['error_message'];
				}
				echo "error_code=".$error_code."\r\n";
				echo "error_message=".$error_message."\r\n";
				echo "balance=".$amount."\r\n";
				echo "trx_id=".$trans_id;
			}else{
				echo "error_code=-2\r\n";
				echo "error_message=CustomerNotFound\r\n";
				echo "balance=0.00\r\n";
				echo "trx_id=0";
			}
		}
		
		public function credit_customer($cust_id, $req_id, $amount, $xml){
			global $db;
			global $core;
			
			if($this->customer_exists($cust_id)){
				//Check to see if the request exists
				if(!$this->request_exists($req_id)){
					//Make customer transaction
					$trans_id = $core->make_customer_transaction($amount, 18, "Sports bet system winner", "NULL", $cust_id);
					
					$trans_id = $trans_id === false ? $req_id : $trans_id;
					
					//Check new balance
					$bal = $core->check_balance("customers", $cust_id);
					$ending_balance = $bal['available_balance'];
					
					//Create a request record
					$q = "INSERT INTO `sports_request` (`request_id`,`request_type`,`amount`,`user_id`,`http_body`,`ending_balance`,`trans_id`) VALUES (%i,'Credit Customer',%f,%s,%s,%i,%i)";
					$db->queryInsert($q, array($req_id, $amount, $cust_id, $xml, $ending_balance, $trans_id));
				}else{
					$req = $this->get_request_info($req_id);
					$ending_balance = $req['ending_balance'];
					$amount = $req['amount'];
					$trans_id = $req['trans_id'];
				}
				echo "error_code=0\r\n";
				echo "error_message=\r\n";
				echo "balance=".$amount."\r\n";
				echo "trx_id=".$trans_id;
			}else{
				echo "error_code=-2\r\n";
				echo "error_message=CustomerNotFound\r\n";
				echo "balance=0.00\r\n";
				echo "trx_id=0";
			}
		}
		
		public function credit_reserve($cust_id, $reserve_id){
			echo "error_code=0\r\n";
			echo "error_message=\r\n";
			echo "balance=0.00";
		}
		
		public function commit_reserve($cust_id, $reserve_id){
			echo "error_code=0\r\n";
			echo "error_message=\r\n";
			echo "balance=0.00\r\n";
			echo "trx_id=123456";
		}
		
		public function cancel_reserve($cust_id, $reserve_id){
			echo "error_code=0\r\n";
			if($this->reserve_exists($reserve_id)){
				echo "error_message=\r\n";
			}else{
				echo "error_message=Reserve was not found\r\n";
			}
			echo "balance=0.00";
		}
		
		///////////////////////////////////////////////////////////
		
		public function check_reserve_remaining($reserve_id){
			global $db;
			
			$q = "SELECT * FROM `sports_reserve` WHERE `reserve_id`=%i";
			$res = $db->queryOneRow($q, array($reserve_id));
			
			return $res['amount_total'] - $res['amount_used'];
		}
		
		public function customer_exists($user_id){
			global $db;
			
			$q = "SELECT * FROM `customers` WHERE `user_id`=%i";
			$cust = $db->query($q, array($user_id));
			
			if($cust == NULL){
				return false;
			}
			
			return true;
		}
		
		public function request_exists($request_id){
			global $db;
			
			//Attempt to retrieve the reserve
			$q = "SELECT * FROM `sports_request` WHERE `request_id`=%i";
			$req = $db->queryOneRow($q, array($request_id));
			
			if($req == NULL){
				return false;
			}
			
			return true;
		}
		
		public function get_request_info($request_id){
			global $db;
			
			//Attempt to retrieve the reserve
			$q = "SELECT * FROM `sports_request` WHERE `request_id`=%i";
			$req = $db->queryOneRow($q, array($request_id));
			
			return $req;
		}
		
		public function reserve_exists($reserve_id){
			global $db;
			
			//Attempt to retrieve the reserve
			$q = "SELECT * FROM `sports_reserve` WHERE `reserve_id`=%i";
			$res = $db->queryOneRow($q, array($reserve_id));
			
			if($res == NULL){
				return false;
			}
			
			return true;
		}
		
		public function reserve_open($reserve_id){
			global $db;
			
			$q = "SELECT * FROM `sports_reserve` WHERE `reserve_id`=%i";
			$res = $db->queryOneRow($q, array($reserve_id));
			
			if($res['status'] == 0){
				return true;
			}else{
				return false;
			}
		}
	}

	if(isset($_GET)){
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		$q = "INSERT INTO `sports_api_request` (`id`, `sent_date`, `response`, `source_ip`, `request_uri`) VALUES (NULL, %s, NULL, %s, %s);";
		$db->queryInsert($q, array($now, $_SERVER['REMOTE_ADDR'], $_SERVER['REQUEST_URI']));
	}
?>