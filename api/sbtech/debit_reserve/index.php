<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/api/sbtech/api.php");
	
	$sports_api = new SPORTS_API();

	if(isset($_GET['cust_id']) && isset($_GET['reserve_id']) && isset($_GET['amount']) && isset($_GET['req_id'])){
		$sports_api->debit_reserve($_GET['cust_id'], $_GET['reserve_id'], $_GET['amount'], $_GET['req_id'], $_POST);
	}else{
		$sports_api->general_error();
	}