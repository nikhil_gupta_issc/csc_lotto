/*global window, $ */
// STATUS

var GPInt = {
    setCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; path=/; " + expires;
    },
    getCookie: function (cname) {
		var vars = [];
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return vars['stoken'];
    }
}

var loginStatus = "empty";
var strUrlToken = getToken();
var strCookieToken = GPInt.getCookie("stoken");
var strSSToken = sessionStorage.getItem("stoken");
var strToken = '';

if(strUrlToken != '' && strUrlToken != undefined){
    strToken = strUrlToken;
    GPInt.setCookie("stoken", strUrlToken, 1);
    sessionStorage.setItem("stoken", strUrlToken);
}else if(strCookieToken != '' && strUrlToken != undefined){
    strToken = strCookieToken;
    sessionStorage.setItem("stoken", strCookieToken);
}else if(strSSToken != '' && strSSToken != undefined){
    strToken = strSSToken;
    GPInt.setCookie("stoken", strSSToken, 1);
}

function whl() {
    'use strict';
  /*  this.errorMessages = new Object();
    this.errorMessages[-1] = "Unknown error.";
    this.errorMessages[-100] = "Invalid input parameters.";
    this.errorMessages[-102] = "Username is not entered.";
    this.errorMessages[-104] = "Password is not entered";
    this.errorMessages[-205] = "Username/password is not correct.";
    this.errorMessages[-7] = "User account is locked.";
    this.DEF_PANEL_WIDTH = 800;
    this.DEF_PANEL_HEIGHT = 550;
    this.PANEL_NAME = "panel_popup"; */
}
whl.prototype.status = function (callback) {
    'use strict';
    this.status_callback = callback;
    var that = this;
    var path = document.location.origin;
	
   $.ajax({
		type: 'GET',
		url: path+'/api/sbtech/index.php',
		crossDomain: true,
		dataType: "jsonp",
		jsonp: false,
		jsonpCallback: 'status_cb',
		data: {
			action: 'whl_get_customer_balance',
			token: strToken,
			callback: 'status_cb'
		},
		success: function (data){
			if(callback){
				callback(data);
			}
		}
	});
}

whl.prototype.statusCallback = function (data){
    'use strict';
	console.log(data);
	function status_cb(data){
		that.status_callback(data);
	}
}

// REFRESH
whl.prototype.refreshSession = function (callback){
    'use strict';
   this.refresh_callback = callback;
    var that = this;
	 var path = document.location.origin;
	
	$.ajax({
		type: 'GET',
		url: path+'/api/sbtech/index.php',
		crossDomain: true,
		dataType: "jsonp",
		jsonp: false,
		jsonpCallback: 'refresh_cb',
		data: {
			action: 'whl_get_customer_balance',
			token: strToken,
			callback: 'refresh_cb'
		},
		success: function (data){
			if(callback){
				callback(data);
			}
		}
	});
}

whl.prototype.refreshCallback = function (data){
    'use strict';
	console.log(data);
	function refresh_cb(data){
        that.refresh_callback(data);
    }
} 

function getToken(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++){
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars['stoken'];
}

function status_cb(data){
	//console.log(data);
}

function refresh_cb(data){
	//console.log(data);
}
