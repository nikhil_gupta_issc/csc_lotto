

<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/api/sbtech/api.php");
	
	$sports_api = new SPORTS_API();

	if(isset($_GET['cust_id']) && isset($_GET['req_id']) && isset($_GET['amount'])){
		$sports_api->debit_customer($_GET['cust_id'], $_GET['req_id'], $_GET['amount'], $_POST);
	}else{
		$sports_api->general_error();
	}