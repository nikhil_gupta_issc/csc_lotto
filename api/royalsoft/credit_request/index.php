<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/api/royalsoft/api.php");
	
	$royalsoft_api = new ROYALSOFT_API();

	if(isset($_GET['customer_id']) && isset($_GET['amount']) && isset($_GET['ticket_id']) && isset($_GET['secret_key']) && isset($_GET['type'])){
		$response = $royalsoft_api->credit_customer($_GET['customer_id'],$_GET['amount'],$_GET['ticket_id'],$_GET['secret_key'],$_GET['type']);
                                 echo json_encode(array('CreditResponse'=>$response));
	}else{
		$response = $royalsoft_api->general_error();
                echo json_encode($response);
	}


