<?php
	error_reporting(E_ERROR);
	ini_set('display_errors', 1);

	class ROYALSOFT_API {
		
                private $s_secret_key = '29653E2E2DDB1DB6B9F4987C36DD7';				
		
		public function test_connection(){
			return true;
		}
              
                public function general_error(){
                       return array("error"=>1,"message"=>"Insufficient Parameters.");			
		}
		
                function authenticate_player($token, $secret_key){
                       global $db;
		       global $core;
			
                       if($this->s_secret_key == $secret_key){
		               $q = "SELECT * FROM users WHERE userid='$token' LIMIT 1";
			       $db_result = $db->queryOneRow($q);
		               $t_result = array();
			
				// if we found a match, verify hash match 
				if(!empty($db_result)){

		                             // Check balance
					     $bal = $core->check_balance("customers", $db_result['id']);

		                             $t_result['IsAllowed'] = true;
		                             $t_result['Message'] = 'Player Authenticated successfully.';
					     $t_result['InternalId'] = $db_result['id'];
		                             $t_result['ID'] = $db_result['id'];
		                             $t_result['Name'] = $db_result['username'];
		                             $t_result['balance'] = $bal['available_balance'];
					
					return $t_result;
				}else{
		                          $t_result['IsAllowed'] = false;
		                          $t_result['Message'] = 'Invalid Token.';
                                          
		                      return $t_result;
		                            
				}
                      } else {
                                           $t_result['IsAllowed'] = false;
		                           $t_result['Message'] = 'Secret Key does not match.';
		                      return $t_result;
                    }
                }			
		
		public function get_customer_balance($cust_id){
			global $core;
			
			//Check balance
			if($core->remaining_loss_limit() != -1){
				$ending_balance = $core->remaining_loss_limit();
			}else{
				$bal = $core->check_balance("customers", $cust_id);
				$ending_balance = $bal['available_balance'];
			}
			$t_result['balance'] = $ending_balance;
		        return $t_result;		
                  }
		
		
		
		
                public function debit_customer($cust_id, $tiecket_id, $amount, $secret_key){
			global $db;
			global $core;
			
                        if($secret_key == $this->s_secret_key){
			if($this->customer_exists($cust_id)){
                                $t_result = array();
				
                                       $starting_bal = $core->check_balance("customers", $cust_id);

					//Make customer transaction
					$trans_id = $core->make_customer_transaction(-$amount, 45, "Sports bet system withdrawal", $tiecket_id, $cust_id);
					
				
					//Check new balance
					$bal = $core->check_balance("customers", $cust_id);
					$ending_balance = $bal['available_balance'];
                                       
					if($starting_bal['available_balance'] == $ending_balance){
						
                                                $t_result['IsSuccess'] = false;
                                                $t_result['message'] = $trans_id;
                                                $t_result['NewBalance'] = $ending_balance;
         

						//Create a request record
						//$q = "INSERT INTO `sports_request` (`request_id`,`request_type`,`amount`,`user_id`,`http_body`,`ending_balance`,`trans_id`,`error_code`,`error_message`) VALUES (%i,'Debit Customer',%i,%s,%s,%i,%i,%i,%s)";
						//$db->queryInsert($q, array($req_id, $amount, $cust_id, $xml, $ending_balance, $trans_id, $error_code, $error_message));
						
						
						return $t_result;
					}
					
                                        $t_result['IsSuccess'] = true;
                                        $t_result['message'] = "Your transaction id is ".$trans_id;
                                        $t_result['NewBalance'] = $ending_balance;

                                        return $t_result;     
					
			}else{
				$t_result['IsSuccess'] = false;
                                $t_result['message'] = "This customer does not exists.";

                              return $t_result;
     
			}
                     } else {
                              $t_result['IsSuccess'] = false;
                              $t_result['message'] = "Player Authetication failed";

                              return $t_result;
     
                      }
		}
       
                public function credit_customer($cust_id, $amount, $ticket_id, $secret_key, $credit_type){
			global $db;
			global $core;
                          if($secret_key == $this->s_secret_key){
			
			if($this->customer_exists($cust_id)){
				       $starting_bal = $core->check_balance("customers", $cust_id);
					//Make customer transaction
					$trans_id = $core->make_customer_transaction($amount, 18, "Sports bet system winner", $ticket_id, $cust_id);
					
					//Check new balance
					$bal = $core->check_balance("customers", $cust_id);
					$ending_balance = $bal['available_balance'];
					if($starting_bal['available_balance'] == $ending_balance){

                                                $t_result['IsSuccess'] = false;
                                                $t_result['message'] = $trans_id;
                                                $t_result['NewBalance'] = $ending_balance;

                                                 return $t_result;
                                            }

                                        $t_result['IsSuccess'] = true;
                                        $t_result['message'] = "Your transaction id is ".$trans_id;
                                        $t_result['NewBalance'] = $ending_balance;

                                        return $t_result;
			}else{
				$t_result['IsSuccess'] = false;
                                $t_result['message'] = "This customer does not exists.";

                              return $t_result;
                        }
     

			}else {
                              $t_result['IsSuccess'] = false;
                              $t_result['message'] = "Player Authetication failed";

                              return $t_result;     
                      }
		}
		
		public function customer_exists($user_id){
			global $db;
			
			$q = "SELECT * FROM `customers` WHERE `user_id`=%i";
			$cust = $db->query($q, array($user_id));
			
			if($cust == NULL){
				return false;
			}
			
			return true;
		}		
		
	}

	if(isset($_GET)){
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		$q = "INSERT INTO `sports_api_request` (`id`, `sent_date`, `response`, `source_ip`, `request_uri`) VALUES (NULL, %s, NULL, %s, %s);";
		$db->queryInsert($q, array($now, $_SERVER['REMOTE_ADDR'], $_SERVER['REQUEST_URI']));
	}
?>
