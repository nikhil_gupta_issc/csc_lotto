<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/api/royalsoft/api.php");
	
	$royalsoft_api = new ROYALSOFT_API();

	if(isset($_GET['token']) && isset($_GET['secret_key'])){
		$response = $royalsoft_api->authenticate_player($_GET['token'],$_GET['secret_key']);
                echo json_encode(array('AuthenticatedPlayer'=>$response));
	}else{
		$response = $royalsoft_api->general_error();
                echo json_encode($response);
	}
