<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/api/royalsoft/api.php");
	
	$royalsoft_api = new ROYALSOFT_API();

	if(isset($_GET['customer_id']) && isset($_GET['ticket_id']) && isset($_GET['amount']) && isset($_GET['secret_key'])){
		$response = $royalsoft_api->debit_customer($_GET['customer_id'],$_GET['ticket_id'],$_GET['amount'],$_GET['secret_key']);
                echo json_encode(array('DebitResponse'=>$response));
	}else{
		$response = $royalsoft_api->general_error();
                echo json_encode($response);
	}


