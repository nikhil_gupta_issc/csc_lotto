<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/api/royalsoft/api.php");
	
	$royalsoft_api = new ROYALSOFT_API();

	switch ($_GET['action']){
		
		case "authenticate":
			if(isset($_GET['token']) && isset($_GET['secret_key'])){
				$response = $royalsoft_api->authenticate_player($_GET['token'],$_GET['secret_key']);
                                echo json_encode(array('AuthenticatedPlayer'=>$response));
			}
			break;
		case "debit_customer": 
			if(isset($_GET['customer_id']) && isset($_GET['ticket_id']) && isset($_GET['amount']) && isset($_GET['secret_key'])){
				$response = $royalsoft_api->debit_customer($_GET['customer_id'],$_GET['ticket_id'],$_GET['amount'],$_GET['secret_key']);
                                 echo json_encode(array('DebitResponse'=>$response));
			}
			break;
		case "credit_customer": 
			if(isset($_GET['customer_id']) && isset($_GET['amount']) && isset($_GET['ticket_id']) && isset($_GET['secret_key']) && isset($_GET['type'])){
				$response = $royalsoft_api->credit_customer($_GET['customer_id'],$_GET['amount'],$_GET['ticket_id'],$_GET['secret_key'],$_GET['type']);
                                 echo json_encode(array('CreditResponse'=>$response));
			}
			break;
		
	}
