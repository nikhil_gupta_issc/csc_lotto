<?php
	/*
		All global constants and configuration settings
	*/

	// turn on/off error handling
	//ini_set('display_errors', 1);
	//ini_set('display_startup_errors', 1);
	//error_reporting(E_ALL|E_STRICT);
	//error_reporting(-1);

	//=========================
	// Global Constants
	//=========================

	// should convert all these to constants
	$conf_appName 			= 'RapidBalls';
	$conf_version 			= 'v2';
	$conf_warnColor			= '#FF5050';
	$conf_goColor			= '#00CC00';
	$conf_template 			= '1';
	$conf_seed				= 'string';
	$conf_datenow			= date('Y-m-d');
	$conf_timenow			= date('Y-m-d H:i:s');
	$conf_timeonly			= date('H:i:s');
	$conf_timenow			= date('Y-m-d H:i:s');
	$conf_date24 			= date('Y-m-d H:i:s', strtotime('-1 day', strtotime($conf_timenow)));
	$conf_dateadd24 		= date('Y-m-d H:i:s', strtotime('+1 day', strtotime($conf_timenow)));
	$statssession_name		= 'VPIPstats';
	$conf_filePath			= 'C:/wamp/stats_files/';
	$conf_dayOfWeek			= date('D');
	$mysqldate 				= date( 'Y-m-d H:i:s');
	
	// Database constants
	define('DB_PHPMYADMIN', 'http://sandboxrb.rapidballs.eu/countdown/phpmyadmin');
	define('DB_TYPE', 'mysql');
	define('DB_HOST', 'localhost');
	define('DB_PORT', '3306');
	define('DB_USER', 'root');
	define('DB_PASSWORD', '978gktjklhhsk39lf29dh782!!"3546kk');
	define('DB_NAME', 'lottomoddb_lotto_leaf');
	define('DB_PCONNECT', false);
	define('DB_INNODB', true);
	
	define('CASINO_TABLE_PREFIX', 'dcoremoddb_casino_');
	
	// set path constants to use for absolute paths
	// ie. http://sandbox2.rapidballs.eu/
	if($_SERVER['SERVER_PORT_SECURE'] == 0){
		// using http
		define('WWW_ROOTPATH', 'http://'.$_SERVER['SERVER_NAME'].'/');
	}else{
		// using https
		define('WWW_ROOTPATH', 'https://'.$_SERVER['SERVER_NAME'].'/');
	}
	// real file path to root
	// ie. C:\inetpub\wwwroot\rapidBalls_development\sandbox2\
	define('WWW_REALPATH', realpath(dirname( __FILE__ )).'\\');
	
?>