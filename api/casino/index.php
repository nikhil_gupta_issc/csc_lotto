<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);

	// API Routing - Flight Framework
	// http://flightphp.com/learn
    
    
	require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	define('CASINO_TABLE_PREFIX', 'casino_');
	
	// set path constants to use for absolute paths
	if($_SERVER['SERVER_PORT_SECURE'] == 0){
		// using http
		define('WWW_ROOTPATH', 'http://'.$_SERVER['SERVER_NAME'].'/');
	}else{
		// using https
		define('WWW_ROOTPATH', 'https://'.$_SERVER['SERVER_NAME'].'/');
	}
	// real file path to root
	define('WWW_REALPATH', realpath(dirname( __FILE__ )).'\\');
	
	require 'flight/Flight.php';
	require 'classes/casino.php';
	
	header("content-type: application/json");
	
	// NOTE:
	//
	// Had to add PUT,PATCH to PHP53_via_FastCGI verbs
	// in IIS Manager - Site - Configuration Editor - ApplicationHost.config - Web Services - Handlers 
	
	// get PUT/PATCH parameters
	$contents = file_get_contents("php://input");
	parse_str($contents, $put_vars);
	
	// merge all arguments into one array
	$param = array_merge($_GET, $_POST, $put_vars);
	
	// Log all hits to the api to a log file
	$time = @date('[Y/m/d h:m:s]');
	$fp = fopen('api_log.txt', 'a');
	fwrite($fp, "=================================================================".PHP_EOL);
	fwrite($fp, "REQUEST: ".PHP_EOL);
	fwrite($fp, "$time FROM: ".$_SERVER['REMOTE_ADDR']." TO: ".$_SERVER['REQUEST_METHOD']." ".$_SERVER['REQUEST_URI'].PHP_EOL);
	$param_dump = json_encode($param);
	fwrite($fp, "PARAMETERS: ".PHP_EOL);
	fwrite($fp, $param_dump.PHP_EOL);
	fclose($fp);
	
	$request = Flight::request();
	
	$ip = $request->ip;
	
	// see if remote user's ip address has been granted an API key
	$q = "SELECT * FROM ".CASINO_TABLE_PREFIX."api_keys WHERE ip_address='".$ip."' LIMIT 1";
	$db_result = $db->queryOneRow($q);
	$api_key = isset($db_result['api_key']) ? $db_result['api_key'] : -1;

	// log API KEY
	//$fp = fopen('api_log.txt', 'a');
	//fwrite($fp, "API_KEY: ".PHP_EOL);
	//fwrite($fp, $api_key.PHP_EOL);
	//fclose($fp);
	
	// validate API key
	if($api_key != -1 && $api_key == $param['WSPassword']){
		// authenticate using API key and create new class instance
		$casino_api = new casino($ip, $api_key);
	}else{
		if($request->url != "/create/api_key" && $request->url != "/help"){
			//$response['success'] = false;
			$response['ErrorCode'] = "Invalid API Key.";
			
			// log response
			$fp = fopen('api_log.txt', 'a');
			$response_dump = json_encode($response);
			fwrite($fp, "RESPONSE: ".PHP_EOL);
			fwrite($fp, $response_dump.PHP_EOL);
			$end = microtime(true);
			$time = number_format(($end - $start), 2);			
			fwrite($fp, "*****************************************************************".PHP_EOL);
			fwrite($fp, "Start: ".$start." || Stop: ".$end." || Execution time : ".$time.PHP_EOL);
			fwrite($fp, "*****************************************************************".PHP_EOL);
			fwrite($fp, "=================================================================".PHP_EOL);
			fclose($fp);
			
			header('HTTP/1.1 400 Invalid API Key', true, 400);
			header("Status: 400 Invalid API Key");
			
			echo json_encode($response);
			die();
		}
	}

	Flight::set('flight.log_errors', true);
	
	Flight::route('GET|POST /create/api_key', function(){
		global $param;
		$start = microtime(true);
		
		$response = array();

		$ip = $_SERVER['REMOTE_ADDR'];
		
		$casino_api = new casino();
		// To prevent unauthorized use of this API, do not leave the create_api_key() function enabled
		// Uncomment the next line to allow remote users to create an API Key.
//		$casino_api->create_api_key($ip);
	
		// log response
		$fp = fopen('api_log.txt', 'a');
		$response_dump = json_encode($response);
		fwrite($fp, "RESPONSE: ".PHP_EOL);
		fwrite($fp, $response_dump.PHP_EOL);
		fwrite($fp, "=================================================================".PHP_EOL);
		fclose($fp);				
		
		header('HTTP/1.1 201 Created', true, 201);
		header("Status: 201 Created");
		
		echo json_encode($response);
		die();
	});
	
	Flight::route('GET|POST /help', function(){	
		$start = microtime(true);
		$request = Flight::request();
		//print_r($request);
		
		// initialize
		$api_key = $request->query->api_key;
		$ip = $request->ip;
		
		// add help responder
		$response = array();
		
		$response['info'][] = $url."/{ACTION}/?api_key={YOUR_API_KEY}&{ADDITIONAL_PARAMETERS}";
		
		$methods = get_class_methods('casino');
		
		foreach($methods as $method){
			if($method != "__construct"){					
				$r = new ReflectionMethod('casino', $method);
				$params = $r->getParameters();
				foreach ($params as $param){
					//$param is an instance of ReflectionParameter
					$response['actions'][$method]['parameters'][] = $param->getName();
					$response['actions'][$method]['is_optional'][] = $param->isOptional();
				}
			}
		}
		echo json_encode($response);
		die();
	});
	
	Flight::route('GET|POST /player/authenticate', function(){
		$start = microtime(true);
		global $casino_api;
		global $param;
		
		$response = array();

		// authenticate and get response
		if(!empty($param['Token'])){
			$response = $casino_api->player_authenticate('', '', $param['Token']);
		}else{
			$response = $casino_api->player_authenticate($param['Login'], $param['Password']);
		}
	
		// log response
		$fp = fopen('api_log.txt', 'a');
		$response_dump = json_encode($response);
		fwrite($fp, "RESPONSE: ".PHP_EOL);
		fwrite($fp, $response_dump.PHP_EOL);
		$end = microtime(true);
		$time = number_format(($end - $start), 2);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "Start: ".$start." || Stop: ".$end." || Authenticate Execution time : ".$time.PHP_EOL);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "=================================================================".PHP_EOL);
		fclose($fp);				
		
		header('HTTP/1.1 201 Created', true, 201);
		header("Status: 201 Created");
		
		echo json_encode($response);
		die();
	});
	
	Flight::route('GET /player/balance_by_id', function(){		
		$start = microtime(true);
		global $casino_api;
		global $param;
		
		$response = array();

		// authenticate and get response
		$response = $casino_api->player_balance_by_id($param['UserID']);
	
		// log response
		$fp = fopen('api_log.txt', 'a');
		$response_dump = json_encode($response);
		fwrite($fp, "RESPONSE: ".PHP_EOL);
		fwrite($fp, $response_dump.PHP_EOL);
		$end = microtime(true);
		$time = number_format(($end - $start), 2);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "Start: ".$start." || Stop: ".$end." || Get Balance by id Execution time : ".$time.PHP_EOL);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "=================================================================".PHP_EOL);
		fclose($fp);				
		
		header('HTTP/1.1 201 Created', true, 201);
		header("Status: 201 Created");
		
		echo json_encode($response);
		die();
	});
	
	Flight::route('GET /player/balance', function(){	
		$start = microtime(true);
		global $casino_api;
		global $param;
		
		$response = array();

		// authenticate and get response
		$response = $casino_api->player_balance($param['Login']);
	
		// log response
		$fp = fopen('api_log.txt', 'a');
		$response_dump = json_encode($response);
		fwrite($fp, "RESPONSE: ".PHP_EOL);
		fwrite($fp, $response_dump.PHP_EOL);
		$end = microtime(true);
		$time = number_format(($end - $start), 2);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "Start: ".$start." || Stop: ".$end." || Get Balance Execution time : ".$time.PHP_EOL);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "=================================================================".PHP_EOL);
		fclose($fp);				
		
		header('HTTP/1.1 201 Created', true, 201);
		header("Status: 201 Created");
		
		echo json_encode($response);
		die();
	});
	
	Flight::route('GET|POST /player/transaction_by_id', function(){	
		$start = microtime(true);
		global $casino_api;
		global $param;
		
		$response = array();

		// Test
		// http://dev.asurewin.com:8090/api/casino/player/transaction?WSPassword=469d89b4b8807606c61809146ccf737681b9d331&Login=234234&GameId=1&TransactionDate=23-11-2015&TotalWagered=1&BalAdj=1&SessionId=234234234234&GameName=frouty&JackpotContribution=1&BonusPlay=1&CasinoGameType=1&CasinoGameID=1&Description=Test
		
		// authenticate and get response
		$response = $casino_api->casino_transaction_by_id($param['UserID'], $param['GameId'], $param['TransactionDate'], $param['TotalWagered'], $param['BalAdj'], $param['SessionId'], $param['GameName'], $param['JackpotContribution'], $param['BonusPlay'], $param['CasinoGameType'], $param['CasinoGameId'], $param['Description']);
	
		// test
		// ?Login=compcentral&GameId=test&TransactionDate=2015-01-01&TotalWagered=20.00&BalAdj=-20.00&SessionId=1&GameName=test_game&JackpotContribution=1&BonusPlay=0&CasinoGameType=3&CasinoGameId=500&Description=test&WSPassword=16fffa13810ec6d58eab0f214873073363453300
		
		// log response
		$fp = fopen('api_log.txt', 'a');
		$response_dump = json_encode($response);
		fwrite($fp, "RESPONSE: ".PHP_EOL);
		fwrite($fp, $response_dump.PHP_EOL);
		$end = microtime(true);
		$time = number_format(($end - $start), 2);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "Start: ".$start." || Stop: ".$end." || Get Transaction by id Execution time : ".$time.PHP_EOL);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "=================================================================".PHP_EOL);
		fclose($fp);				
		
		header('HTTP/1.1 201 Created', true, 201);
		header("Status: 201 Created");
		
		echo json_encode($response);
		die();
	});
	
	Flight::route('GET|POST /player/transaction', function(){
		$start = microtime(true);
		global $casino_api;
		global $param;
		
		$response = array();

		// Test
		// http://dev.asurewin.com:8090/api/casino/player/transaction?WSPassword=469d89b4b8807606c61809146ccf737681b9d331&Login=234234&GameId=1&TransactionDate=23-11-2015&TotalWagered=1&BalAdj=1&SessionId=234234234234&GameName=frouty&JackpotContribution=1&BonusPlay=1&CasinoGameType=1&CasinoGameID=1&Description=Test
		
		// authenticate and get response
		$response = $casino_api->casino_transaction($param['Login'], $param['TransactionDate'], $param['BalAdj'], $param['SessionId'], $param['GameName'], $param['CasinoGameId'], $param['GameId'], $param['TotalWagered'], $param['JackpotContribution'], $param['BonusPlay'], $param['CasinoGameType'], $param['Description'], $param['Wager']);
	
		// test
		// ?Login=compcentral&GameId=test&TransactionDate=2015-01-01&TotalWagered=20.00&BalAdj=-20.00&SessionId=1&GameName=test_game&JackpotContribution=1&BonusPlay=0&CasinoGameType=3&CasinoGameId=500&Description=test&WSPassword=16fffa13810ec6d58eab0f214873073363453300
		
		// log response
		$fp = fopen('api_log.txt', 'a');
		$response_dump = json_encode($response);
		fwrite($fp, "RESPONSE: ".PHP_EOL);
		fwrite($fp, $response_dump.PHP_EOL);
		$end = microtime(true);
		$time = number_format(($end - $start), 2);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "Start: ".$start." || Stop: ".$end." || Get Transaction Execution time : ".$time.PHP_EOL);
		fwrite($fp, "*****************************************************************".PHP_EOL);
		fwrite($fp, "=================================================================".PHP_EOL);
		fclose($fp);				
		
		header('HTTP/1.1 201 Created', true, 201);
		header("Status: 201 Created");
		
		echo json_encode($response);
		die();
	});

	Flight::start();
?>