<?php

	$page_title = "Winning Numbers";
	$menu_color = "olive";
	$footer_color = "olive";
	
	$top_left_fixed_img = "";
	$top_right_fixed_img = "<img src='/images/cards_rgt.png'>";

	include("header.php");
	include('page_top.php');
  
        
?>

<link rel="stylesheet" type="text/css" href="/rapidballs/css/balls.css">

<script>
	$(document).ready(function(){	
             
			var data_table = $('#datatable').DataTable({
				"bInfo" : false,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				"ajax": "/ajax/datatables/lotto_results.php",
				"columns": [
					{ "data": "draw_date" },
					{ "data": "house_short_name" },
					{ "data": "house_name" },
					{ "data": "ball_2_draw" },
					{ "data": "ball_3_draw" },
					{ "data": "ball_4_draw" },
					//{ "data": "ball_5_draw" }
					//{ "data": "ball_6_draw" }
				],
				"order": [[0, 'desc']]
			});
          
<?php if(!empty($t_site_settings[0]['value'])){ ?>
		
		var data_table_rapidballs = $('#datatable_rapidballs').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/rapidballs_results.php",
			"columns": [
				{ "data": "datetime_drawn" },
				{ "data": "id" },
				{ "data": "drawing" },
				{ "data": "total" },
				{ "data": "lucky" }//,
				//{ "data": "seed_a" },
				//{ "data": "seed_b" },
				//{ "data": "hash" }
			],
			"order": [[0, 'desc']]
		});
<?php } ?>

               $("#start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
                                data_table = $('#datatable').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/lotto_results.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
				{ "data": "draw_date" },
				{ "data": "house_short_name" },
				{ "data": "house_name" },
				{ "data": "ball_2_draw" },
				{ "data": "ball_3_draw" },
				{ "data": "ball_4_draw" }
				//{ "data": "ball_5_draw" }
				//{ "data": "ball_6_draw" }
			],
			"order": [[0, 'desc']]
		});

				
				
				data_table.draw(true);
			});
			
			// end date change
			$("#end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#start_date').data("DateTimePicker").date()){
					start_date = $('#start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#end_date').data("DateTimePicker").date()){
					end_date = $('#end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable')){
					data_table.destroy();
				}
				
				data_table = $('#datatable').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/lotto_results.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
				{ "data": "draw_date" },
				{ "data": "house_short_name" },
				{ "data": "house_name" },
				{ "data": "ball_2_draw" },
				{ "data": "ball_3_draw" },
				{ "data": "ball_4_draw" }
				//{ "data": "ball_5_draw" }
				//{ "data": "ball_6_draw" }
			],
			"order": [[0, 'desc']]
		});
				
				
				
				data_table.draw(true);
			});


                  $("#rapid_start_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#rapid_start_date').data("DateTimePicker").date()){
					start_date = $('#rapid_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#rapid_end_date').data("DateTimePicker").date()){
					end_date = $('#rapid_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable_rapidballs')){
					data_table_rapidballs.destroy();
				}
				
                               data_table_rapidballs = $('#datatable_rapidballs').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/rapidballs_results.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
				{ "data": "datetime_drawn" },
				{ "data": "id" },
				{ "data": "drawing" },
				{ "data": "total" },
				{ "data": "lucky" }//,
				//{ "data": "seed_a" },
				//{ "data": "seed_b" },
				//{ "data": "hash" }
			],
			"order": [[0, 'desc']]
		});

				
				
				data_table_rapidballs.draw(true);
			});
			
			// end date change
			$("#rapid_end_date").datetimepicker().on('dp.change', function (e) {
				var start_date, end_date;
				if($('#rapid_start_date').data("DateTimePicker").date()){
					start_date = $('#rapid_start_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				if($('#rapid_end_date').data("DateTimePicker").date()){
					end_date = $('#rapid_end_date').data("DateTimePicker").date().format('YYYY-MM-DD');
				}
				
				// load transactions datatable
				if($.fn.dataTable.isDataTable('#datatable_rapidballs')){
					data_table_rapidballs.destroy();
				}
				
				data_table_rapidballs = $('#datatable_rapidballs').DataTable({
			"bInfo" : false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"ajax": "/ajax/datatables/rapidballs_results.php?filter_date_from="+start_date+"&filter_date_to="+end_date,
			"columns": [
				{ "data": "datetime_drawn" },
				{ "data": "id" },
				{ "data": "drawing" },
				{ "data": "total" },
				{ "data": "lucky" }//,
				//{ "data": "seed_a" },
				//{ "data": "seed_b" },
				//{ "data": "hash" }
			],
			"order": [[0, 'desc']]
		});
				
				
				
				data_table_rapidballs.draw(true);
			});

       

	});
</script>
<script type="text/javascript">
		$(document).ready(function(){
			$('#slider_photo_btn').click(function(){
				$("#slider_pic_modal").modal("show");
			});
		});
	</script>
<?php
			if($session->userlevel >= 9){
				echo '
		<div style="text-align: center;">
			<button class="btn btn-primary" id="slider_photo_btn" style="margin: 5px;">Change Photos</button>
		</div>
				';
			}
		?>
<div class="clearfix" style="background-color: rgba(0,0,0,0);">	
		<div class="header_image">
		<?php
			$files = scandir('images/winning_page_slider/');
			$total = count($files);
			$images = array();
			for($x = 0; $x <= $total; $x++){
				if($files[$x] != '.' && $files[$x] != '..' && is_file('images/winning_page_slider/'.$files[$x])){
					$images[] = $files[$x];
				}
			}
		?>
	
		<div id="carousel-index" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'" class="active"></li>';
						}else{
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'"></li>';
						}
					}
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '
				<div class="item active">

						<img src="/images/winning_page_slider/'.$images[$x].'" class="carousel-image"  width="100%" height="222px">

				</div>
							';
						}else{
							echo '
				<div class="item">

						<img src="/images/winning_page_slider/'.$images[$x].'" class="carousel-image"  width="100%" height="222px">

				</div>
							';
						}
					}
				?>
			</div>
<!--<div class="header_image">
		<img src="/images/winning_hdd.jpg" class="img_grow_wide">
	</div>-->

			
	
	<div style="padding-bottom: 15px;">
	<!--	<div class="win-bg-plain">
					<h3 class="headings">Winning Lottery Numbers</h2>
				</div>-->
<!--<form class="form">
<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='start_date'>
								         <input type='text' class="form-control" placeholder="From Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>

<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='end_date'>
								         <input type='text' class="form-control" placeholder="To Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>


</form>-->
			<!--	<table id="datatable" class="display table dt-responsive win-list" cellspacing="0" width="100%" style="color:#FFF">
			<thead style="color:#FFF">
				<tr>
					<th class="text-left">Date</th>
					<th class="text-left">House</th>
					<th class="text-left">House Name</th>
					<th class="text-left">2 Ball</th>
					<th class="text-left">3 Ball</th>
					<th class="text-left">4 Ball</th>
					<th class="text-left">5 Ball</th> 
					<th class="text-left">6 Ball</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="6" class="dataTables_empty">Loading data from server</td>
				</tr>
			</tbody>
		</table>-->

                <?php if(!empty($b_rapidball_settings)){ ?>
		
		<div class="win-bg-plain">
					<h3 class="headings">FastBallz Numbers</h2>
				</div>

<form class="form">
<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='rapid_start_date'>
								         <input type='text' class="form-control" placeholder="From Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>

<div class="col-sm-6">
  <div class="col-sm-12">
								<div class="form-group">
									
									<div class="input-group date dp" id='rapid_end_date'>
								         <input type='text' class="form-control" placeholder="To Date"/>
								         <span class="input-group-addon">
									    <span class="glyphicon glyphicon-calendar"></span>
								         </span>
							              </div>
								</div>
							</div>
</div>


</form>

				<table id="datatable_rapidballs" class="display table dt-responsive win-list" cellspacing="0" width="100%" style="color:#FFF">
		
			<thead>
				<tr>
					<th class="text-left">Draw Time</th>
					<th class="text-left">Draw #</th>
					<th class="text-center">Drawing</th>
					<th class="text-right">Total</th>
					<th class="text-right">Lucky</th>
					<!--<th class="text-center">Seed A</th>-->
					<!--<th class="text-center">Seed B</th>-->
					<!--<th class="text-center">Hash</th>-->
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="6" class="dataTables_empty">Loading data from server</td>
				</tr>
			</tbody>
		</table>
	</div>
       <?php } ?>

</div>
<!-- Modal for Slider Pic Selection -->
	<div class="modal fade" id="slider_pic_modal" tabindex="-1" role="dialog" aria-labelledby="slider_pic_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="slider_pic_modal_label">Change Slider Photos</h4>

					<div id="customer_name2" class="pull-right" style="padding-right: 6px;">
						
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div id="upload_photos" class="col-sm-12">
						<?php include($_SERVER['DOCUMENT_ROOT'].'/upload_slider_photos.php'); ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include("footer.php");
?>
