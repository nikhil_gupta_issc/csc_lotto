<?php

include('config.php');

function match_columns($column){
		global $db;
		
		$result = array();

		if($column == "sCustomerNo"){
			$column = "customer_number";
			$table = "customers";
		}
		
		$result['column'] = $column;
		$result['table'] = $table;
		
		return $result;
	}

// Read Csv Code

// Store data from csv file into an array
	$r = 0;
	$col_count = 99;
	$headers = array();
	$csv_data = array();
	$handle = fopen("VerfiedCustomers.csv", "r");
	while(($data = fgetcsv($handle, 8000, ",")) !== FALSE) {
			$col_count = count($data);
				for($c=0; $c<$col_count; $c++){
					// get column headers
					if($r == 0){
						$headers[$c] = match_columns($data[$c]);
					}else{
					// get value
					$value = trim($data[$c]);
									
					
					
					
					if(strtolower($value) == 'true'){
					$value = 1;
					}elseif(strtolower($value) == 'false'){
					$value = 0;
					}
											
										
											// see if key is a valid column name in db
											$q = "SELECT DATA_TYPE, COUNT(*) as count FROM information_schema.COLUMNS WHERE COLUMN_NAME = '".$headers[$c]['column']."' and TABLE_NAME = '".$headers[$c]['table']."'";
											$key_exists = $db->queryOneRow($q);
											if($key_exists['count'] > 0){
												$max_len = $key_exists['CHARACTER_MAXIMUM_LENGTH'];
												$num_digits = $key_exists['NUMERIC_PRECISION'];
												$num_places = $key_exists['NUMERIC_SCALE'];
											
												// validate data against data type defined in database
												$data_type = $key_exists['DATA_TYPE'];
												switch ($data_type){
													case "tinyint":
														settype($value , "integer");
														break;
													case "mediumint":
														settype($value , "integer");
														break;
													case "smallint":
														settype($value , "integer");
														break;
													case "int":
														settype($value , "integer");
														break;
													case "decimal":
														settype($value , "float");
														break;
													case "float":
														settype($value , "float");
														break;
													case "double":
														settype($value , "float");
														break;
													case "real":
														settype($value , "float");
														break;
													case "bit":
														settype($value , "boolean");
														break;
													case "boolean":
														settype($value , "boolean");
														break;
													case "date":
														if(!$value){
															$value = "NULL";
														}else{
															$value = date('Y-m-d', strtotime($value));
														}
														break;
													case "datetime":
														if(!$value){
															$value = "NULL";
														}else{
															$value = date('Y-m-d H:i:s', strtotime($value));
														}					
														break;
													case "time":
														if(!$value){
															$value = "NULL";
														}else{
															$value = date('H:i:s', strtotime($value));
														}
														break;
													case "timestamp":
														settype($value , "integer");
														break;
													case "year":
														if(!$value){
															$value = "NULL";
														}
														break;
												}
												
											
												
											}
											
											if($headers[$c]['table']){
												$csv_data[$r][$headers[$c]['table']][$headers[$c]['column']] = $value;
												
											}
										}
									}
									
									if($r > 0){							
									$csv_data[$r]['customers']['customer_number']."<br>";
										
$q = "UPDATE customers SET `is_drivers_license_verified` = '1', `is_voters_card_verified` = '1', `is_verified` = '1' WHERE customer_number = ".$csv_data[$r]['customers']['customer_number'];
//echo $q."<br><br>";
$insert_id = $db->query($q);

									}
									$r++;
								}
?>
