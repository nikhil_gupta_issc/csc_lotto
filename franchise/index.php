<?php
	$page_title = "CSCLotto Cashier Panel";
	include($_SERVER['DOCUMENT_ROOT'].'/franchise/header.php');
	
	// get all buttons
	$q = "SELECT * FROM `sec_buttons`";
	$buttons = $db->query($q);
	foreach($buttons as $button){
		$button_permissions[$button['id']] = 0;
		$jquery_selectors[$button['id']] = $button['jquery_selector'];
	}


        /**------ Get virtual balance --------**/
        $t_virtual_balance = $core->get_virtual_balance(); 
        $i_virtual_balance = number_format($t_virtual_balance['virtual_money']);

	
	// get group membership
	$member_of = $core->user_sec_group_membership($session->userinfo['id']);
	
	if(count($member_of) > 0){
		foreach($member_of as $group_id){
			// button-level security
			$q = "SELECT * FROM `sec_button_permissions` WHERE group_id = '".$group_id."'";
			$button_perms = $db->query($q);
			
			foreach($button_perms as $perm){
				// stack permissions
				if($perm['permission_level_id'] == -1){
					$button_permissions[$perm['button_id']] = -1;
				}
			}
		}
	}
	
	// disable buttons if needed
	foreach($button_permissions as $button_id => $permission){
		if($permission == -1){
			echo '
				<script type="text/javascript">
					$(document).ready(function(){
						$("'.$jquery_selectors[$button_id].'").prop("disabled", true);
					});
				</script>
			';
		}
	}
	?>

<script type="text/javascript" src="/lib/assets/Web-Ticker-master/jquery.webticker.js"></script>
<link rel="stylesheet" href="css/webticker.css?v=<?=CSS_VERSION?>" type="text/css">
<link rel="stylesheet" href="css/global.css?v=<?=CSS_VERSION?>" type="text/css">
<script type="text/javascript" src="js/lotto_betting.js?v=<?=JS_VERSION?>"></script>
<script type="text/javascript" src="js/panel_buttons.php"></script>

<script type="text/javascript">

$(document).on('keydown', function ( e ) {

    if (e.which == 191 || e.which == 111) {
         e.preventDefault();
         $('#straight_toggle').click();
         $('#straight_amount_text').focus();
    } else if(e.which == 106){
         e.preventDefault();
         $('#boxed_toggle').click();
         $('#boxed_amount_text').focus();        
    } else if(e.which == 13){
        e.preventDefault();
        $('#straight_amount_text').blur();
        $('#boxed_amount_text').blur();
        $('#button_add_cart').click();  
        $('#straight_amount_text').val('');
        $('#boxed_amount_text').val('');   
        $('#number_text').val(''); 
        $('#number_text').focus(); 
    } else if(e.which == 107){
        e.preventDefault();
        $('#straight_amount_text').blur();
        $('#boxed_amount_text').blur();
        $('#purchase_bets_button').click();    
        $('#number_text').val(''); 
        $('#number_text').focus();
        $('#straight_amount_text').val('');
        $('#boxed_amount_text').val(''); 
    }

 });


</script>



<script>
	
 /*   DATE : 15t June'16 */
    
    $(document).ready(function(){	
	var pin_enable=$("#pin_enable").val();
	//alert(pin_enable);
	
	if(pin_enable==1)
	{
		 
		 $("#deposit_customer_pin").show();
		 $("#deposit_pin_text").show();
	}
	if(pin_enable==0)
	{
		
		$("#deposit_customer_pin").hide();
		$("#deposit_pin_text").hide();
		
		
	}
	
});	

/* 		END CODE  */
	</script>
<div class="panel_container">
	<div style="height:5%; width:100%;">
		<ul id="news_ticker">
			<?php
				$curr_time = new datetime('now');
				$curr_time_str = $curr_time->format("Y-m-d H:i:s");
				$tickers = $db->query("SELECT * FROM pos_news_ticker WHERE expiration_date>'".$curr_time_str."' AND (location_id='".$session->userinfo['panel_user']['location_id']."' OR location_id='-1' OR `island_id`='-1' OR `island_id`='".$session->userinfo['customers']['island_id']."')");
				$loc_commission = $db->queryOneRow("SELECT commission_rate FROM panel_location WHERE id=".$session->userinfo['panel_user']['location_id']);
				$commission_code = $loc_commission['commission_rate'] == 0 ? 0 : 1;
				for($x=0;$x<count($tickers);$x++){
					if($tickers[$x]['is_franchise'] == $commission_code || $tickers[$x]['is_franchise'] == -1){
						echo "<li>".$tickers[$x]['content']." |</li>";
					}
				}

				if(count($tickers)==0){
					echo "<li>There is no recent news</li>";
				}
			?>
		</ul>
	</div>
	<div class="left_container">
		<div class="button_container">
			<button class="btn btn-primary show_early_house" style="width:50%;float:left;height:40px;">Show Early Games</button>
			<button class="btn btn-primary show_late_house"style="width:50%;float:left;height:40px;">Show Late Games</button>
			<button class="btn btn-default early_house_toggle" data-toggle="button" style="width:50%;float:left;height:40px;">All Early Games</button>
			<button class="btn btn-default late_house_toggle" data-toggle="button" style="width:50%;float:left;height:40px;">All Late Games</button>
			<?php
				//Build house collection
				$q = "SELECT h.is_early_house, h.web_cutoff_time, h.plays_on_sunday, h.id, h.name, h.short_name FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id JOIN lotto_game_xml_link link ON g.id=link.game_id WHERE h.is_disabled=0 GROUP BY h.id ORDER BY h.web_cutoff_time";
				$houses = $db->query($q);
				
				foreach($houses as $house){
					//initialize the datetime for the cuttoff time
					$web_cutoff_time = new datetime($house["web_cutoff_time"]);
					if(!($house["plays_on_sunday"]==0 && $web_cutoff_time->format("w")==0)){
						$early_late_class = $house['is_early_house'] == 1 ? "early_house" : "late_house";
						if($web_cutoff_time > $curr_time){
							echo "<button data-toggle='button' short_house='".$house['short_name']."' house_id='".$house['id']."' class='btn btn-primary house_btn ".$early_late_class."'>".$house['name']."<br>".$web_cutoff_time->format('h:i:s a')."</button>";
						}else{
							echo "<button data-toggle='button' short_house='".$house['short_name']."'  house_id='".$house['id']."' disabled='disabled' class='btn btn-primary house_btn ".$early_late_class."'>".$house['name']."<br>".$web_cutoff_time->format('h:i:s a')."</button>";
						}
					}
				}
			?>
		</div>
		<div class="cashier_controls">
			<div class="main_keypad">
				<input type="text" tabindex="1" placeholder="#123" id="number_text" class="form-control main_key_input"></input>
				<input type="text" tabindex="2" placeholder="S $0.00" id="straight_amount_text" class="form-control main_key_input"></input>
				<input type="text" tabindex="3" placeholder="B $0.00" id="boxed_amount_text" class="form-control main_key_input"></input>
				<button class="btn btn-default main_key" id="main_keypad_1">1</button>
				<button class="btn btn-default main_key" id="main_keypad_2">2</button>
				<button class="btn btn-default main_key" id="main_keypad_3">3</button>
				<button class="btn btn-default main_key" id="main_keypad_4">4</button>
				<button class="btn btn-default main_key" id="main_keypad_5">5</button>
				<button class="btn btn-default main_key" id="main_keypad_6">6</button>
				<button class="btn btn-default main_key" id="main_keypad_7">7</button>
				<button class="btn btn-default main_key" id="main_keypad_8">8</button>
				<button class="btn btn-default main_key" id="main_keypad_9">9</button>
				<button class="btn btn-default main_key" id="main_keypad_decimal">.</button>
				<button class="btn btn-default main_key" id="main_keypad_0">0</button>
				<button class="btn btn-default main_key" id="main_keypad_backspace">BS</button>
				<button class="btn btn-primary main_key" data-toggle="button" id="main_keypad_hundreds">Hundreds</button>
				<button class="btn btn-primary main_key" id="button_clear_cart">Delete<br>Ticket</button>
				<button class="btn btn-primary main_key" data-toggle="button" id="main_keypad_triples">Triples</button>
			</div>
			<div class="toggle_menu">
				<button class="btn btn-primary toggle_key bottom_btn large_text" id="straight_toggle" data-toggle="button">S</button>
				<button class="btn btn-primary toggle_key bottom_btn large_text" id="boxed_toggle" data-toggle="button">B</button>
							</div>
			<div class="amounts_menu">
				<button tabindex="4" class="btn btn-default amounts_key_col_2" id="button_add_cart">Add</button>
				<button class="btn btn-default amounts_key" id="quick_25_keypad">$0.25</button>
				<button class="btn btn-default amounts_key" id="quick_50_keypad">$0.50</button>
				<button class="btn btn-default amounts_key" id="quick_75_keypad">$0.75</button>
				<button class="btn btn-default amounts_key" id="quick_100_keypad">$1.00</button>
				<button class="btn btn-default amounts_key" id="quick_150_keypad">$1.50</button>
				<button class="btn btn-default amounts_key" id="quick_200_keypad">$2.00</button>
				<button class="btn btn-default amounts_key" id="quick_300_keypad">$3.00</button>
				<button class="btn btn-default amounts_key" id="quick_500_keypad">$5.00</button>
				<button class="btn btn-primary amounts_key bottom_btn" id="button_qk3">QP 3</button>
				<button class="btn btn-primary amounts_key bottom_btn" id="button_qk4">QP 4</button>
			</div>
			<div class="panel_functions_menu">
				<label class="btn btn-default panel_functions_key" role='button' id="voids_sub_menu_button">Voids</label>
				<label class="btn btn-default panel_functions_key" role='button' id="tickets_sub_menu_button">Tickets</label>
				<label class="btn btn-default panel_functions_key" role='button' id="winning_numbers_button">Winning Numbers</label>
				<label class="btn btn-default panel_functions_key" role='button' id="scan_sub_menu_button">Scan</label>
				<label class="btn btn-default panel_functions_key" role='button' id="shift_sub_menu_button">Shift Info</label>
			</div>
		</div>
	</div>
	<div class="right_container">
		<div class="shift_information">
			<table class="table table-bordered">
                                <tr>
					<td>Time:</td>
					<td>
						<?php include($_SERVER['DOCUMENT_ROOT'].'/lib/assets/clock/dc_server_clock.php'); ?>
					</td>
				</tr>
				<tr>
					<td>Location:</td>
					<td>
						<?php
							$location = $db->queryOneRow("SELECT * FROM panel_location WHERE id=".$session->userinfo['panel_user']['location_id']);
							echo $location['name'];
                                                        
                                             ?>
					</td>
				</tr>
				<tr>
					<td>Cashier:</td>
					<td><?php echo $session->userinfo['firstname']." ".$session->userinfo['lastname']." (".$session->userinfo['id'].")" ?></td>
				</tr>
				<tr>
					<td>Balance:</td>
					<td bgcolor="#00FF00">$<span id="cashier_balance"><?php echo number_format($session->userinfo['panel_user']['balance']); ?></span></td>
				</tr>
                                 <tr>
					<td>Virtual Money:</td>
					<td >$<span id="virtual_balance"><?php echo $i_virtual_balance;?></span></td>
 
				</tr>
				<tr>
					<td>Shift Start:</td>
					<td>
						<?php
							$shift_start = $db->queryOneRow("SELECT * FROM `panel_user_shift` WHERE id=".$session->userinfo['panel_user']['shift_id']);
							echo date("m/d/Y h:i A", strtotime($shift_start['shift_start']));
						?>
					</td>
				</tr>
			</table>
		</div>
		<div class="bet_list">
			<div class="panel panel-default stretch_panel">
				<div class="panel-heading">
					<h3 class="panel-title">Bet List</h3>
				</div>
				<div class="panel-body" id="scroll_table" style="padding:0px;overflow-y:scroll;height:90%;">
					<div class="table-responsive">
						<table class="table table-bordered" id="bet_table">
							<thead>
								<tr>
									<th>Choice</th>
									<th>Game</th>
									<th>S/B</th>
									<th>Amt</th>
									<th>Del</th>
								<tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="total_container">
				Total: $<span id="total">0.00</span>
			</div>
			<div class="scroll_tender">
				<button class="btn btn-primary scroll_tender_key" id="scroll_up"><i class='fa fa-arrow-up'></i></button>
				<button class="btn btn-primary scroll_tender_key" id="purchase_bets_button"><i class='fa fa-print'></i> Tender</button>
				<button class="btn btn-primary scroll_tender_key" id="scroll_down"><i class='fa fa-arrow-down'></i></button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="sub_menu_modal" tabindex="-1" role="dialog" aria-labelledby="sub_menu_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body clearfix">
				<button class="btn btn-primary sub_menu_button" id="deposit_button" style="display:none;">Customer Account Deposit</button>
				<button class="btn btn-primary sub_menu_button" id="franchise_deposit_button" style="display:none;">Franchise Deposit</button>
				<button class="btn btn-primary sub_menu_button" id="payouts_button" style="display:none;">Pay Ticket</button>
				<button class="btn btn-primary sub_menu_button" id="voids_button" style="display:none;">Void Ticket</button>
				<button class="btn btn-primary sub_menu_button" id="adjust_void_button" style="display:none;">Adjust Voided Ticket</button>
				<button class="btn btn-primary sub_menu_button" id="transfer_out_button" style="display:none;">Transfer Out</button>
				<button class="btn btn-primary sub_menu_button" id="accept_transfer_button" style="display:none;">Accept Transfer</button>
				<button class="btn btn-primary sub_menu_button" id="replay_ticket_button" style="display:none;">Replay Ticket</button>
				<button class="btn btn-primary sub_menu_button" id="find_ticket_button" style="display:none;">Find Ticket</button>
				<!-- <button class="btn btn-primary sub_menu_button" id="event_ticket_button" style="display:none;">Event Tickets</button> -->
				<button class="btn btn-primary sub_menu_button" id="customer_card_button" style="display:none;">Customer Cards</button>
				<button class="btn btn-primary sub_menu_button" id="gift_card_button" style="display:none;">Gift Cards</button>
				<button class="btn btn-primary sub_menu_button" id="results_button" style="display:none;">Results</button>
				<button class="btn btn-primary sub_menu_button" id="close_shift_button" style="display:none;">Close Shift</button>
				<button class="btn btn-primary sub_menu_button" id="sales_report_button" style="display:none;">Sales Report</button>
                                <button class="btn btn-primary sub_menu_button" id="customer_withdrawal_button" style="display:none;">Customer Withdrawal</button>
                                <button class="btn btn-primary sub_menu_button" id="petty_cash_withdrawal_button" style="display:none;">Petty Cash Withdrawal</button>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="clear_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="clear_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="clear_confirmation_modal_label">Confirm Clearing the Cart</h4>
			</div>
			<div class="modal-body clearfix">
				Are you sure you would like to clear all of the bets in the cart?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="clear_confirm">Clear</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="remove_confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="remove_confirmation_modal_label">Confirm Removal</h4>
			</div>
			<div class="modal-body clearfix">
				Are you sure you would like to remove this bet from the cart?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="remove_confirm">Remove</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="invalid_tendered_modal" tabindex="-1" role="dialog" aria-labelledby="invalid_tendered_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="invalid_tendered_modal_label">Invalid</h4>
			</div>
			<div class="modal-body clearfix">
				Invalid Amount Tendered!!
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="invalid_tendered_confirm" data-dismiss="modal">Ok</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="tender_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="tender_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="tender_modal_label">Tendering Information</h4>
			</div>
			<div class="modal-body clearfix">
				<input id="ticket_id" type="hidden"></input>
				<input id="tender_checksum" type="hidden"></input>
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label">Amount Tendered:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="amount_tendered"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Total:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="tender_total"></p>
							<button data-toggle="button" class="btn btn-default" id="exact_amount">Exact Amount Tendered</button>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Change:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="tender_change"></p>
						</div>
					</div>
				</form>
				<div class="col-sm-12">
					<div id="tender_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_transaction" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="finalize_transaction">Finalize Tender</button>
			</div>
		</div>
	</div>
</div>

<!-- Start Button Modals -->
<div class="modal fade" id="deposit_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deposit_modal_label" aria-hidden="true">
		<?php 
					/*	DATE :15th June'16  */
					$q = "SELECT * FROM  settings where setting='atmpins_enabled'";
					$settings_info = $db->queryOneRow($q);
					$pin_enable=$settings_info['value'];  
		?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="deposit_modal_label">Deposit</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div id="deposit_customer_info" class="clearfix" style="display: none;">
						<div class="col-sm-4">
							<div class="thumbnail" style="width: 150px; height: 150px;">
								<img id="deposit_user_pic" style="text-align: center; width: 100%; height: 100%;" alt="User Pic" src="/admin/images/generic-user-purple.png">
							</div>
						</div>
						<div class="col-sm-8">
							<div class="col-sm-3">Name:</div>
							<div id="deposit_customer_name" class="col-sm-9"></div>

							<div class="col-sm-3">DOB:</div>
							<div id="deposit_customer_dob" class="col-sm-9"></div>

							<div class="col-sm-3">Age:</div>
							<div id="deposit_customer_age" class="col-sm-9"></div>
							
							<div class="col-sm-3">Gender:</div>
							<div id="deposit_customer_gender" class="col-sm-9"></div>

							<div class="col-sm-3">Address:</div>
							<div id="deposit_customer_address" class="col-sm-9"></div>
							
							<div class="col-sm-3">Island:</div>
							<div id="deposit_customer_island" class="col-sm-9"></div>
							
							<div class="col-sm-3">Phone:</div>
							<div id="deposit_customer_phone" class="col-sm-9"></div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-4 control-label">Customer Number:</label>
						<div class="col-sm-8">
							<input type='password' class='form-control' id="deposit_customer_number"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" id="deposit_pin_text">CSCLotto ATM PIN:</label>
						<div class="col-sm-8">
							<input type='password' class='form-control' id="deposit_customer_pin"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Amount:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="deposit_customer_amount"/>
						</div>
					</div>
					<input type="hidden" value="<?php echo $pin_enable;?>" name="pin_enable" id="pin_enable"/>
				</form>
				<div class="col-sm-12">
					<div id="deposit_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_transaction" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="deposit_confirm">Deposit</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="remove_confirmation_modal_dep" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_dep_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="remove_confirmation_modal_dep_label"></h4>
				</div>
				<div class="modal-body">
					Do you really want to deposit <span id="dep_amount"></span>?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="dep_remove_confirm">Confirm</button>
				</div>
			</div>
		</div>
	</div>
<!-- ====  FIND TICKET ====  !-->
<div class="modal fade" id="find_ticket_modal" tabindex="-1" role="dialog" aria-labelledby="find_ticket_modal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				    <h4 class="modal-title" id="replay_ticket_modal_label">Find Ticket</h4>
			         </div>
			<div class="modal-body clearfix">
				
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label"> ID :</label>
						<div class="col-sm-10">
							<input type='text' style="width:300px;" class='form-control' id="find_ticket_id"/>
						</div>
					
					</div>
					
					<div class="col-sm-10" style="padding-left:45px;">
					    <div id="ticket_message" class="alert alert-success" role="alert" style="display:none;"></div>
					</div>
					
				</form>
				
			</div>
				<div class="col-sm-12" style="padding-left:45px;">
					<div id="find_ticket_error" class="alert alert-danger" role="alert" style="display:none;"></div>	
				</div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btn_find_ticket">Find Ticket</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
</div>

<!-- =====  END FIND TICKET  =======  !-->

	
	
<div class="modal fade" id="replay_ticket_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="replay_ticket_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="replay_ticket_modal_label">Replay Ticket</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label">Ticket Number:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="replay_ticket_number"/>
						</div>
					</div>
				</form>
				<div class="col-sm-12">
					<div id="replay_ticket_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_replay_ticket" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="replay_ticket_confirm">Replay Ticket</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="phone_card_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="phone_card_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="phone_card_modal_label">Phone Cards</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label">Carrier:</label>
						<div class="col-sm-8">
							<select id="phone_card_carrier" class="form-control">
								<option value=""></option>
								<?php
									require_once($_SERVER['DOCUMENT_ROOT']."/api/emida/api.php");
									$phone_card_api = new PHONE_CARD_API();
									$phone_card_api->login2();
									$carriers = $phone_card_api->get_carrier_list();
									foreach($carriers as $carrier){
										echo "<option value='".$carrier['CarrierId']."'>".$carrier['Description']."</option>";
									}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Products:</label>
						<div class="col-sm-8">
							<select id="phone_card_products" class="form-control">
								<option value=""></option>
							</select>
						</div>
					</div>
				</form>
				<div class="col-sm-12">
					<div id="phone_card_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_replay_ticket" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="phone_card_confirm">Purchase Phone Card</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="transfer_out_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="transfer_out_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="transfer_out_modal_label">Transfer Out</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label">Transfer to Location:</label>
						<div class="col-sm-8">
							<select id="transfer_to_location" class="form-control">
								<?php
									$q = "SELECT * FROM panel_location WHERE is_deleted=0";
									$locations = $db->query($q);
									foreach($locations as $location){
								?>
								<option value="<?php echo $location['id']; ?>"><?php echo $location['name']; ?></option>
								<?php
									}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Amount:</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="transfer_amount"></input>
						</div>
					</div>
				</form>
				<div class="col-sm-12">
					<div id="transfer_out_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_transfer_out" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="transfer_out_confirm">Transfer Out</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="payouts_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deposit_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="payouts_modal_label">Payouts</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label">Ticket Number:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="payout_ticket_number"/>
						</div>
					</div>
				</form>
				<div class="col-sm-12" id="payout_details" style="display:none;">
					<h3>Ticket Information</h3>

					<form class="form-horizontal" style="margin-bottom: 20px;">
						<div class="form-group">
							<label class="col-sm-4 control-label">Number of Wins</label>
							<div class="col-sm-8">
								<p class="form-control-static" id="payouts_number_of_wins"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Total Won</label>
							<div class="col-sm-8">
								<p class="form-control-static">$<span id="payouts_total_won"></span></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Max Payout</label>
							<div class="col-sm-8">
								<p class="form-control-static">$<span id="payouts_max_payout"></span></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Status</label>
							<div class="col-sm-8">
								<p class="form-control-static"><span id="void_status"></span></p>
							</div>
						</div>
						<div id="voided_comment_contain" class="form-group">
							<label class="col-sm-4 control-label">Comment</label>
							<div class="col-sm-8">
								<p class="form-control-static"><span id="voided_comment"></span></p>
							</div>
						</div>
					</form>
					
					<!-- show payout alert messages -->
					<div id="payout_alerts_contain">
						
					</div>

					<h4>Payouts</h4>
					<table class="table table-striped" id="payout_table">
						<thead>
							<tr>
								<th>Type</th>
								<th>Paid By</th>
								<th>Amount Paid</th>
							</tr>
						</thead>
						<tbody id="payouts_table_body">

						</tbody>
						<tfoot>
							<tr>
								<td style="text-align:right;" colspan="2">Total Paid</td>
								<td>$<span id="partial_payout_total">0.00</span></td>
							</tr>
						</tfoot>
					</table>

					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-4 control-label">Payout Amount:</label>
							<div class="col-sm-4">
								<input type='text' class='form-control' id="payout_amount"/>
							</div>
							<div class="col-sm-4">
								<button type="button" class="btn btn-primary" id="payouts_add_payout">Submit Payout</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-12">
					<div id="payouts_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_payouts" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="payouts_confirm">Payouts</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="results_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deposit_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="results_modal_label">Results</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label">Ticket Number:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="results_ticket_number"/>
						</div>
					</div>
				</form>
				<div class="col-sm-12" id="results_details" style="display:none;">
					<h3>Results for this Ticket</h3>
					<table class="table table-striped" id="results_table">
						<thead>
							<tr>
								<th>Game</th>
								<th>Number</th>
								<th>Amount Won</th>
								<th>Win Date</th>
							</tr>
						</thead>
						<tbody id="results_table_body">

						</tbody>
						<tfoot>
							<tr>
								<td style="text-align:right;" colspan="2">Total Won</td>
								<td colspan="2">$<span id="results_total">0.00</span></td>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="col-sm-12">
					<div id="results_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_results" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="results_confirm">Results</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="voids_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deposit_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="voids_modal_label">Voids</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label">Ticket Number:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="voids_ticket_number" autofocus="autofocus"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Reason:</label>
						<div class="col-sm-8">
							<textarea type='text' class='form-control' maxlength="500" id="voids_ticket_reason"></textarea>
						</div>
					</div>
				</form>
				<div class="col-sm-12">
					<div id="voids_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_voids" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="voids_confirm">Void</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="gift_cards_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="gift_cards_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="gift_cards_modal_label">Gift Cards</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label">Card Number:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="gift_card_number"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Amount:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="gift_card_amount"/>
						</div>
					</div>
				</form>
				<div class="col-sm-12">
					<div id="gift_cards_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_voids" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="gift_cards_confirm">Purchase</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="withdrawal_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="withdrawal_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="withdrawal_modal_label">Withdrawal</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div id="withdraw_customer_info" class="clearfix" style="display: none;">
						<div class="col-sm-4">
							<div class="thumbnail" style="width: 150px; height: 150px;">
								<img id="withdraw_user_pic" style="text-align: center; width: 100%; height: 100%;" alt="User Pic" src="/admin/images/generic-user-purple.png">
							</div>
						</div>
						<div class="col-sm-8">
							<div class="col-sm-3">Name:</div>
							<div id="withdraw_customer_name" class="col-sm-9"></div>

							<div class="col-sm-3">DOB:</div>
							<div id="withdraw_customer_dob" class="col-sm-9"></div>

							<div class="col-sm-3">Age:</div>
							<div id="withdraw_customer_age" class="col-sm-9"></div>
							
							<div class="col-sm-3">Gender:</div>
							<div id="withdraw_customer_gender" class="col-sm-9"></div>

							<div class="col-sm-3">Address:</div>
							<div id="withdraw_customer_address" class="col-sm-9"></div>
							
							<div class="col-sm-3">Island:</div>
							<div id="withdraw_customer_island" class="col-sm-9"></div>
							
							<div class="col-sm-3">Phone:</div>
							<div id="withdraw_customer_phone" class="col-sm-9"></div>
						</div>
					</div>
				
					<div class="form-group">
						<label class="col-sm-4 control-label">Customer Number:</label>
						<div class="col-sm-8">
							<input type='password' class='form-control' id="withdrawal_customer_number"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Available Balance:</label>
						<div class="col-sm-8">
							<p class='form-control-static'>$<span id="withdrawal_customer_available">0.00</span></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" id="withdrawal_pin_text">CSCLotto ATM PIN:</label>
						<div class="col-sm-8">
							<input type='password' class='form-control' id="withdrawal_customer_pin"/>
						</div>
					</div>
				<div class="form-group">
						<label class="col-sm-4 control-label">Amount:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="withdrawal_customer_amount"/>
						</div>
					</div> 
				</form>
				<div class="col-sm-12">
					<div id="withdrawal_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_transaction" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="withdrawal_confirm">Withdrawal</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="remove_confirmation_modal_with" tabindex="-1" role="dialog" aria-labelledby="remove_confirmation_modal_with_label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="remove_confirmation_modal_with_label"></h4>
				</div>
				<div class="modal-body">
					Do you really want to withdraw <span id="with_amount"></span>?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="with_remove_confirm">Confirm</button>
				</div>
			</div>
		</div>
	</div>
<div class="modal fade" id="petty_cash_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="petty_cash_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="petty_cash_modal_label">Petty Cash Withdrawal</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 control-label">Amount:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="petty_cash_amount" value="$0.00"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Reason:</label>
						<div class="col-sm-8">
							<textarea class="form-control" id="withdrawal_reason" ></textarea>
						</div>
					</div>					
				</form>
				<div class="col-sm-12">
					<div id="petty_cash_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_transaction" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="petty_cash_confirm">Petty Cash Withdrawal</button>
			</div>
		</div>
	</div>
</div>



<div class="modal fade" id="sales_shift_info_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deposit_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="sales_shift_info_modal_label">Sales & Shift Information</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">

				</form>
				<div class="col-sm-12">
					<div id="sales_shift_info_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_sales_shift_info" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="sales_shift_info_confirm">???</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="products_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deposit_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="products_modal_label">Products</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">

				</form>
				<div class="col-sm-12">
					<div id="products_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_products" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="products_confirm">???</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="franchise_deposit_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deposit_modal_label" aria-hidden="true">
		<?php 
					/*	DATE :15th June'16 
					$q = "SELECT * FROM  settings where setting='atmpins_enabled'";
					$settings_info = $db->queryOneRow($q);
					$pin_enable=$settings_info['value'];  */
		?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="franchise_deposit_modal_label">Franchise Deposit</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					
					<div class="form-group">
						<label class="col-sm-4 control-label">Customer Number:</label>
						<div class="col-sm-8">
							<input type='password' class='form-control' id="deposit_customer_number"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" id="deposit_pin_text">CSCLotto ATM PIN:</label>
						<div class="col-sm-8">
							<input type='password' class='form-control' id="deposit_customer_pin"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Amount:</label>
						<div class="col-sm-8">
							<input type='text' class='form-control' id="deposit_customer_amount"/>
						</div>
					</div>
					<!--	<input type="hidden" value="<?php echo $pin_enable;?>" name="pin_enable" id="pin_enable"/> !-->
				</form>
				<div class="col-sm-12">
					<div id="franchise_deposit_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_franchise_deposit" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="franchise_deposit_confirm">Deposit</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="close_shift_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="close_shift_modal_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="close_shift_modal_label">Close Shift</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">Cashier:</label>
								<div class="col-sm-8">
									<p class='form-control-static'><?php echo $session->userinfo['firstname']." ".$session->userinfo['lastname']." (".$session->userinfo['id'].")" ?></p>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">Shift Sales:</label>
								<div class="col-sm-8">
									<p class='form-control-static'>$<span id="close_shift_sales">0.00</span></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">Shift Start:</label>
								<div class="col-sm-8">
									<p class='form-control-static' id="shift_start">
									<?php
										$shift_start = $db->queryOneRow("SELECT * FROM `panel_user_shift` WHERE id=".$session->userinfo['panel_user']['shift_id']);
										echo $shift_start['shift_start'];
									?>
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">Shift Payouts:</label>
								<div class="col-sm-8">
									<p class='form-control-static'>$<span id="close_shift_payouts">0.00</span></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="col-sm-6"></div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">Petty Cash:</label>
								<div class="col-sm-8">
								
									<p class='form-control-static'>$<span id="close_shift_pettycash">0.00</span></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<!-- Datatable for transaction breakdowns -->
					</div>
					<div class="col-sm-6">
						<div class="col-sm-6">
							<h2>Bills</h2>
							<div class="form-group">
								<label class="col-sm-4 control-label">$1</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="close_shift_one_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$5</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="close_shift_five_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$10</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="close_shift_ten_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$20</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="close_shift_twenty_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$50</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="close_shift_fifty_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$100</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="close_shift_hundred_bills" placeholder="0"></input>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<h2>Coins</h2>
							<div class="col-sm-12">
								<input class="form-control" type="text" id="close_shift_coins" placeholder="$0.00"></input>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
                                              
					  <h2>Balances</h2>
                                <!--          <div class="col-sm-6">
						<div class="form-group">
							<label class="col-sm-4 control-label">Float:</label>
							<div class="col-sm-8 balance_padding">
								<p class="form-control-static">$<span id="close_shift_float">0.00</span></input>
							</div>
						</div> 
                                 </div> !-->
                                          <div class="col-sm-6">
						<div class="form-group">
							<label class="col-sm-8 control-label">Opening Float:</label>
							<div class="col-sm-4 balance_padding">
								<p class="form-control-static">$<span id="close_shift_opening">0.00</span></input>
							</div>
						</div>
                                          </div>
                                          <div class="col-sm-6">
						<div class="form-group">
							<label class="col-sm-4 control-label">Closing:</label>
							<div class="col-sm-8 balance_padding">
								<p class="form-control-static">$<span id="close_shift_closing">0.00</span></input>
							</div>
						</div>
                                         </div>
                                         <div class="col-sm-6">
						<div class="form-group">
							<label class="col-sm-4 control-label">Expected:</label>
							<div class="col-sm-8 balance_padding">
								<p class="form-control-static">$<span id="close_shift_expected">0.00</span></input>
							</div>
						</div>
                                        </div>
                                        <div class="col-sm-6">
						<div class="form-group">
							<label class="col-sm-5 control-label">Over/Short:</label>
							<div class="col-sm-7 balance_padding">
								<p class="form-control-static">$<span id="close_shift_over_short">0.00</span></input>
							</div>
						</div>  
                                        </div>
                                        <div class="col-sm-6">                                              
						<div class="form-group">
							<label class="col-sm-6 control-label no-padding">Bag #:</label>
							<div class="col-sm-6 balance_padding" style="padding:0px;">
								<input type="text" class="form-control" id="close_shift_bag_number"></input>
							</div>
						</div>
                                        </div>
                                        <div class="col-sm-12 no-padding">
                                            
                                                <h2>Virtual Balance</h2>
                                        </div>
                                        <div class="col-sm-6">
                                                <div class="form-group">
							<label class="col-sm-4 control-label">Total:</label>
							<div class="col-sm-8">
								<p class="form-control-static">$<span id="total_virtual_money">0.00</span></input>
							</div>
						</div>
                                        </div>
                                        <div class="col-sm-6">
                                                <div class="form-group">
							<label class="col-sm-4 control-label">Closing:</label>
							<div class="col-sm-8">
								<p class="form-control-static">$<span id="closing_virtual_money">0.00</span></input>
							</div>
						</div>
                                        </div>
					</div>
				</form>
			</div>
			<div class="col-sm-12">
				<div id="shift_close_error" class="alert alert-danger" role="alert" style="display:none;">
						
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_close_shift" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="close_shift_confirm">Close Shift</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="accept_transfer_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="accept_transfer_modal_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="accept_transfer_label">Accept Transfer</h4>
			</div>
			<div class="modal-body clearfix" style="text-align:center;">
				<table id="pending_transfers_datatable" class="display table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="text-left">From</th>
							<th class="text-left">Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="6" class="dataTables_empty">Loading data from server</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_receipt" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="accept_transfer_confirm">Accept Transfer</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="customer_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="customer_modal_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="customer_modal_label">Customer Look-up</h4>
			</div>
			<div class="modal-body clearfix" style="text-align:center;">
				<table id="customers_datatable" class="display table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="text-center">Account Number</th>
							<th class="text-center">Customer Name</th>
						<!--	<th class="text-left">Balance</th>
							<th class="text-left">Email</th>
							<th class="text-left">Phone</th>-->
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="6" class="dataTables_empty">Loading data from server</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_receipt" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="receipt_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="receipt_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="receipt_modal_label">Print Preview</h4>
			</div>
			<div class="modal-body clearfix" style="text-align:center;">
				<iframe id="receipt" name="receipt" height="500px" src="" seamless width="90%"></iframe>
				<div class="col-sm-12">
					<div id="receipt_error" class="alert alert-danger" role="alert" style="display:none;">
					
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div id="winning_date_picker" class="toshow" style="display:none">
					<div class="col-sm-5 pull-left">
						<input type="date" class="form-control" id="date_winning_numbers" name="date_winning_numbers" placeholder="Enter Date">	
					</div>
				</div>
				<div id="location_sales_report_picker_1" class="toshow_location_report" style="display:none">
					<div class="col-sm-4 pull-left">
						<input type="date" class="form-control" id="from_date_location_sales" name="to_date_location_sales" placeholder="Enter Date" value="<?php $date = new DateTime(); echo $date->format('Y-m-d'); ?>">	
					</div>
				</div>
				<div id="location_sales_report_picker_2" class="toshow_location_report" style="display:none">
					<div class="col-sm-4 pull-left">
						<input type="date" class="form-control" id="to_date_location_sales" name="from_date_location_sales" placeholder="Enter Date" value="<?php $date = new DateTime(); echo $date->format('Y-m-d'); ?>">	
					</div>
				</div>
				<button type="button" class="btn btn-primary" id="reprint_receipt">Reprint</button>
				<button type="button" class="btn btn-default" id="cancel_receipt" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="open_shift_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="open_shift_modal_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="open_shift_modal_label">Open Shift</h4>
			</div>
			<div class="modal-body clearfix">
				<form class="form-horizontal">
					<div class="col-sm-6">
						<div class="col-sm-6">
							<h2>Bills</h2>
							<div class="form-group">
								<label class="col-sm-4 control-label">$1</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="open_shift_one_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$5</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="open_shift_five_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$10</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="open_shift_ten_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$20</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="open_shift_twenty_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$50</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="open_shift_fifty_bills" placeholder="0"></input>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">$100</label>
								<div class="col-sm-2">
									<p class="form-control-static">X</p>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="open_shift_hundred_bills" placeholder="0"></input>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<h2>Coins</h2>
							<div class="col-sm-12">
								<input class="form-control" type="text" id="open_shift_coins" placeholder="$0.00"></input>
							</div>
						</div>
						<br>
           
						<div class="col-sm-6">
							<h2>Location</h2>
							<div class="col-sm-12">
								<select type="text" class="form-control" id="open_shift_location">
									<?php
										
																			
										$locations = $db->query("SELECT name, id FROM panel_location WHERE is_deleted=0 ORDER BY name");
										foreach($locations as $location){
											echo "<option ".(($session->userinfo['panel_user']['location_id'] == $location['id']) ? 'selected' : '')."  value=".$location['id'].">".$location['name']."</option>";
										}
									?>
								</select>
							</div>
						</div>
					</div>

					<div class="col-sm-6">
						<h2>Balances</h2>
						<!-- <div class="form-group">
							<label class="col-sm-4 control-label">Float:</label>
							<div class="col-sm-8">
								<input id="open_shift_float" type="text" class="form-control" placeholder="$0.00"></input>
							</div>
						</div> !-->
						<div class="form-group">
							<label class="col-sm-4 control-label">Opening Float:</label>
							<div class="col-sm-8">
								<p class="form-control-static">$<span id="open_shift_opening">0.00</span></input>
							</div>
						</div>
                                                <div class="form-group">
                                         <?php



$i_virtual_money = 0; 
                $t_current_virtual_money = $db->query("SELECT virtual_money FROM panel_user_shift where user_id = ".$session->userinfo['id']." ORDER BY id DESC LIMIT 1");

                if(!empty($t_current_virtual_money) && !empty($t_current_virtual_money[0]['virtual_money']) && $t_current_virtual_money[0]['virtual_money'] != 0.00){
                      $i_virtual_money = $t_current_virtual_money[0]['virtual_money'];

                } else {
                       $t_virtual_money = $db->query("SELECT virtual_money_offset FROM panel_user WHERE user_id = ".$session->userinfo['id']);
		        if(!empty($t_virtual_money) && !empty($t_virtual_money[0]['virtual_money_offset']) && 
	$t_virtual_money[0]['virtual_money_offset'] !=0.00){
		              $i_virtual_money = $t_virtual_money[0]['virtual_money_offset'];
		        } else {                      
		              $i_virtual_money = 0;                     
		       }
                }



                                         ?>
							<label class="col-sm-4 control-label">Virtual Money:</label>
							<div class="col-sm-8">
								<p class="form-control-static">$<span id="virtual_money_balance"><?php echo $i_virtual_money;?></span></input>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-sm-12">
				<div id="shift_open_error" class="alert alert-danger" role="alert" style="display:none;">
						
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="open_shift_confirm">Open Shift</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="winning_num_preview_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="winning_num_preview_modal_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="winning_num_preview_modal_label">Winning Number</h4>
			</div>
			<div class="modal-body clearfix">
			<input type="hidden" id="winning_num_date">
				 <div id="winning_num_body" style="padding-right:120px; padding-left:120px; padding-top:20px;">


                                </div>



			</div>
			
			<div class="modal-footer">
                                <button type="button" class="btn btn-default"  data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="print_winning_num">Print</button>
			</div>
		</div>
	</div>
</div>


<!--- --------------------------------------------- winning number date ------------------------------------------------------->
<div class="modal fade" id="winning_number_date" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Winning Number</h4>
        </div>
        <div class="modal-body">
         <div class="row">
            <div class='col-sm-6' style="text-align:center;padding-top: 7px;">
            <label class="control-label">Select Date </lable>
            </div>
        <div class='col-sm-6'>
            <div class="form-group">
                <div class="input-group date"  >
                    <input type='text' class="form-control" name="datetimepicker1" id='datetimepicker1'>
                    <!--<span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>-->
                </div>
            </div>
        </div>
        <script type="text/javascript">
                $(document).ready(function () {
                 
                $('#datetimepicker1').datetimepicker({
                    showTodayButton: true,
                    format: 'MM/DD/YYYY'
                });
           
            });
        </script>
    </div>
        </div>
        <div class="col-sm-12">
		<div id="win_error" class="alert alert-danger" role="alert" style="display:none;"></div>
	</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="get_win_num">Show Winning Number</button>
        </div>
        
      </div>
      
    </div>
    
  </div>
<!------------------------------------------------winning number date ---------------------------------------------------------->


<div class="modal fade" id="card_keypad_modal" tabindex="-1" role="dialog" aria-labelledby="card_keypad_modal_label" aria-hidden="true">
	<div class="modal-dialog" style="z-index:10;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="card_keypad_modal_label">Customer Card</h4>
			</div>
			<div class="modal-body clearfix">
                               
                               
                               	<form class="form-horizontal">
					<div id="customercard_info" class="clearfix" style="display: none;">
						
						<div class="col-sm-12">
							<div class="col-sm-2">Name:</div>
							<div id="customercard_name" class="col-sm-4"></div>

							<div class="col-sm-2">DOB:</div>
							<div id="customercard_dob" class="col-sm-4"></div>
						</div>
						
						<div class="col-sm-12">
							<div class="col-sm-2">Age:</div>
							<div id="customercard_age" class="col-sm-4"></div>
							
							<div class="col-sm-2">Gender:</div>
							<div id="customercard_gender" class="col-sm-4"></div>
						</div>
						<div class="col-sm-12">
							<div class="col-sm-2">Address:</div>
							<div id="customercard_address" class="col-sm-4"></div>
							
							<div class="col-sm-2">Island:</div>
							<div id="customercard_island" class="col-sm-4"></div>
						</div>	
						<div class="col-sm-12">
							<div class="col-sm-2">Phone:</div>
							<div id="customercard_phone" class="col-sm-6"></div>
						</div>
					</div>
									
						<div class="col-sm-12">&nbsp;</div>			
						<div class="col-sm-12">Customer Number:</div>
						<div class="col-sm-12">
							<input type='password' class='form-control' id="customercard_number"/></div>
							<input type='hidden' name="c_id" class='form-control' id="cid"/>
					
					
				</form>
                               <div class="col-sm-12">
					<div id="customercard_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
                               
                                                              
                               

				<div class="col-sm-9"><input type="text" class="form-control" id="keypad_value"></input></div>
				<div class="col-sm-3"><button id="keypad_clear" class="btn btn-danger keypad_button">Clear</button></div>
				<div class="col-sm-12">
					<div id="keypad_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
				<div class="col-sm-4"><button id="keypad_1" class="btn btn-primary keypad_button">1</button></div>
				<div class="col-sm-4"><button id="keypad_2" class="btn btn-primary keypad_button">2</button></div>
				<div class="col-sm-4"><button id="keypad_3" class="btn btn-primary keypad_button">3</button></div>
				<div class="col-sm-4"><button id="keypad_4" class="btn btn-primary keypad_button">4</button></div>
				<div class="col-sm-4"><button id="keypad_5" class="btn btn-primary keypad_button">5</button></div>
				<div class="col-sm-4"><button id="keypad_6" class="btn btn-primary keypad_button">6</button></div>
				<div class="col-sm-4"><button id="keypad_7" class="btn btn-primary keypad_button">7</button></div>
				<div class="col-sm-4"><button id="keypad_8" class="btn btn-primary keypad_button">8</button></div>
				<div class="col-sm-4"><button id="keypad_9" class="btn btn-primary keypad_button">9</button></div>
				<div class="col-sm-4"><button id="keypad_decimal" class="btn btn-primary keypad_button">.</button></div>
				<div class="col-sm-4"><button id="keypad_0" class="btn btn-primary keypad_button">0</button></div>
				<div class="col-sm-4"><button id="keypad_backspace" class="btn btn-primary keypad_button">BS</button></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="card_keypad_confirm">Add</button>
			</div>
		</div>
	</div>
</div>


<?php
	if($session->userinfo['panel_user']['shift_id'] == "-1"){
		echo "<script>";
		echo "$('#open_shift_modal').modal('show');";
		echo "</script>";
	}
	include($_SERVER['DOCUMENT_ROOT'].'/franchise/footer.php');
?>
