$(document).ready(function(){
	
	//Used to hold the button location while the confirmation window is open
	//when removing a bet.
	var curr_remove_button;
	
	//Define a set that contains all of the numbers that are included in the cart
	var choices_set = {};
	
	//Make the webticker start
	$("#news").slideDown("fast");
	$("#news_ticker").webTicker();

	//Formats money strings.
	function pad (str, max) {
		return str.length < max ? pad("0" + str, max) : str;
	}
	
	//The following will check every 5 seconds to make sure that the game is still active
	/*
	setInterval(function (){
		$.ajax({
			url: "ajax/lotto_betting.php",
			data: {
				action: "get_inactive_games"
			},
			method: "POST"
		})
		.done(function( data ) {
			//Split all of the game ids into an array
			var ids = data.split(",");

			//Check to see if the disabled games are to be disabled or hidden
			for(var y=0;y<ids.length;y++){
				$("button[house_id='"+ids[y]+"']").removeClass("active");
				$("button[house_id='"+ids[y]+"']").attr("disabled", true);
				$("button[house_id='"+ids[y]+"']").fadeOut('slow');
			}
			
			
		});
	}, 5000);
	*/
	
	//The following will check every second to make sure that the game is still active and show countdown
	setInterval(function (){
		$.ajax({
			url: "ajax/lotto_betting.php",
			dataType: 'json',
			data: {
				action: "get_active_games"
			},
			method: "POST"
		})
		.done(function(data){		
			$.each(data, function(index, house){
				$("button[house_id='"+house.id+"']").html(house.name+"<br>"+house.time_remaining);
				if(house.time_remaining == 'Closed'){
					$("button[house_id='"+house.id+"']").removeClass("active");
					$("button[house_id='"+house.id+"']").attr("disabled", true);
					$("button[house_id='"+house.id+"']").delay(1000).fadeOut(1000);
				}
			});
		});
	}, 1000);
	
	//The following will check every second to make sure that the game is still active and show countdown
	setInterval(function (){
		$.ajax({
			url: "ajax/get_cashier_balance.php",
			dataType: 'json',
			method: "POST"
		})
		.done(function(data){
			if(data.success == "false"){
				$('#cashier_balance').text("No Shift Open!");
			}else{
				$('#cashier_balance').html(data.balance);
                                $('#virtual_balance').html(data.virtual_balance);
			}
		});
	}, 5000);

	//Start all of the focuses for the modal
	var active_textbox = null;
	var active_main_textbox = "#number_text";

	//toggle all early or late houses
	$(document).on("click", ".early_house_toggle", function(){
		if($(this).hasClass("active")){
			$(".early_house_toggle").addClass("active");
			$(".late_house_toggle").removeClass("active");
			$(".early_house:enabled").addClass("active");
			$(".late_house").removeClass("active");
		}else{
			$(".early_house_toggle").removeClass("active");
			$(".house_btn").removeClass("active");
		}
	});
	
	$(document).on("click", ".late_house_toggle", function(){
		if($(this).hasClass("active")){
			$(".late_house_toggle").addClass("active");
			$(".early_house_toggle").removeClass("active");
			$(".early_house").removeClass("active");
			$(".late_house:enabled").addClass("active");
		}else{
			$(".late_house_toggle").removeClass("active");
			$(".house_btn").removeClass("active");
		}
	});
	
	$(document).on("click", ".show_late_house", function(){
		$(".early_house").hide();
		$(".late_house").show();
		$.ajax({
			url: "ajax/lotto_betting.php",
			data: {action: "get_inactive_games"},
			method: "POST"
		})
		.done(function( data ) {
			//Split all of the game ids into an array
			var ids = data.split(",");

			//Check to see if the disabled games are to be disabled or hidden
			for(var y=0;y<ids.length;y++){
				$("button[house_id='"+ids[y]+"']").removeClass("active");
				$("button[house_id='"+ids[y]+"']").attr("disabled", true);
			}
		});
	});
	
	$(document).on("click", ".show_early_house", function(){
		$(".late_house").hide();
		$(".early_house").show();
		$.ajax({
			url: "ajax/lotto_betting.php",
			data: {action: "get_inactive_games"},
			method: "POST"
		})
		.done(function( data ) {
			//Split all of the game ids into an array
			var ids = data.split(",");

			//Check to see if the disabled games are to be disabled or hidden
			for(var y=0;y<ids.length;y++){
				$("button[house_id='"+ids[y]+"']").removeClass("active");
				$("button[house_id='"+ids[y]+"']").attr("disabled", true);
			}
		});
	});
	
	//Initially only show the early games when the panel loads.
	$(".show_early_house").trigger("click");
	
	// delay needed to set focus after modal is visible

	$('#tender_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#amount_tendered').focus();
		}, 1000);
	});
	
	$('#deposit_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#deposit_customer_number').focus();
		}, 500);
	});
	
	$('#replay_ticket_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#replay_ticket_number').focus();
		}, 500);
	});
	
	$('#payouts_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#payout_ticket_number').focus();
		}, 500);
	});
	
	$('#results_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#results_ticket_number').focus();
		}, 500);
	});
	
	$('#voids_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#voids_ticket_number').focus();
		}, 500);
	});
	
	$('#withdrawal_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#withdrawal_customer_number').focus();
		}, 500);
	});
	
	$('#close_shift_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#close_shift_bag_number').focus();
		}, 1000);
	});
	
	$('#keypad_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#keypad_value').focus();
		}, 1000);
	});
		
	
	
	// allow enter key to trigger confirm button
	$('#keypad_modal').keypress(function(e){
		if (e.which == '13'){
			$('#keypad_confirm').trigger("click");
		}
	});
	
	$('#number_text').keypress(function(e){
		if (e.which == '13'){
			$('#button_add_cart').trigger("click");
			$(this).focus();
		}
	});
	
	$(document).on("click", "#scroll_up", function(){
		document.getElementById('scroll_table').scrollTop -= 150;
	});
	
	$(document).on("click", "#scroll_down", function(){
		document.getElementById('scroll_table').scrollTop += 150;
	});
	
	$(document).on("click", "#amount_tendered", function(){
		active_textbox = "#amount_tendered";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});
	
	$(document).on("click", "#gift_card_number", function(){
		active_textbox = "#gift_card_number";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});
	
	$(document).on("click", "#open_shift_one_bills", function(){
		active_textbox = "#open_shift_one_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#open_shift_five_bills", function(){
		active_textbox = "#open_shift_five_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#open_shift_ten_bills", function(){
		active_textbox = "#open_shift_ten_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});
	
	$(document).on("click", "#open_shift_twenty_bills", function(){
		active_textbox = "#open_shift_twenty_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#open_shift_fifty_bills", function(){
		active_textbox = "#open_shift_fifty_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#open_shift_hundred_bills", function(){
		active_textbox = "#open_shift_hundred_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#open_shift_coins", function(){
		active_textbox = "#open_shift_coins";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#open_shift_float", function(){
		active_textbox = "#open_shift_float";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#close_shift_one_bills", function(){
		active_textbox = "#close_shift_one_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#close_shift_five_bills", function(){
		active_textbox = "#close_shift_five_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#close_shift_ten_bills", function(){
		active_textbox = "#close_shift_ten_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#close_shift_twenty_bills", function(){
		active_textbox = "#close_shift_twenty_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#close_shift_fifty_bills", function(){
		active_textbox = "#close_shift_fifty_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#close_shift_hundred_bills", function(){
		active_textbox = "#close_shift_hundred_bills";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#close_shift_coins", function(){
		active_textbox = "#close_shift_coins";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#close_shift_bag_number", function(){
		active_textbox = "#close_shift_bag_number";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#straight_amount_text", function(){
		active_main_textbox = "#straight_amount_text";
		$(this).val("");
	});
	
	$(document).on("click", "#number_text", function(){
		active_main_textbox = "#number_text";
		$(this).val("");
	});

	$(document).on("click", "#boxed_amount_text", function(){
		active_main_textbox = "#boxed_amount_text";
		$(this).val("");
	});

	$(document).on("click", "#replay_ticket_number", function(){
		active_textbox = "#replay_ticket_number";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#payout_ticket_number", function(){
		active_textbox = "#payout_ticket_number";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#results_ticket_number", function(){
		active_textbox = "#results_ticket_number";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#voids_ticket_number", function(){
		active_textbox = "#voids_ticket_number";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});
	
	$(document).on("click", "#transfer_amount", function(){
		active_textbox = "#transfer_amount";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#deposit_customer_amount", function(){
		active_textbox = "#deposit_customer_amount";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#withdrawal_customer_amount", function(){
		active_textbox = "#withdrawal_customer_amount";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	$(document).on("click", "#payout_amount", function(){
		active_textbox = "#payout_amount";
		//$('#keypad_value').val($(this).val());
		//$('#keypad_error').hide();
		//$('#keypad_modal').modal("show");
	});

	//Keypad Confirmed
	$(document).on("click", "#keypad_confirm", function(){
		$(active_textbox).val($('#keypad_value').val());
		$(active_textbox).trigger("blur");
		$(active_textbox).trigger("change");
		$("#keypad_error").text("");
		$("#keypad_error").hide();
		if(active_textbox=="#amount_tendered"){
			if(parseFloat($('#keypad_value').val().replace("$",""))<parseFloat($("#tender_total").text().replace("$",""))){
				$("#keypad_error").text("Invalid Tender Amount!");
				$("#keypad_error").slideDown("fast");
				$(active_textbox).val("");
			}else{
				var change = parseFloat($(active_textbox).val().replace("$",""))-parseFloat($("#tender_total").text().replace("$",""));
				$("#tender_change").text("$"+change.toFixed(2));
				$('#keypad_modal').modal("hide");
			}
		}else{
			$('#keypad_modal').modal("hide");
		}
	});

	//Keypad button handlers
	$(document).on("click", "#keypad_1", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"1");
		}
	});

	$(document).on("click", "#keypad_2", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"2");
		}
	});

	$(document).on("click", "#keypad_3", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"3");
		}
	});

	$(document).on("click", "#keypad_4", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"4");
		}
	});

	$(document).on("click", "#keypad_5", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"5");
		}
	});

	$(document).on("click", "#keypad_6", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"6");
		}
	});

	$(document).on("click", "#keypad_7", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"7");
		}
	});

	$(document).on("click", "#keypad_8", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"8");
		}
	});

	$(document).on("click", "#keypad_9", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"9");
		}
	});

	$(document).on("click", "#keypad_0", function(){
		if(check_char_after_decimal($('#keypad_value').val())){
			$('#keypad_value').val($('#keypad_value').val()+"0");
		}
	});

	$(document).on("click", "#keypad_decimal", function(){
		if($('#keypad_value').val().indexOf(".") == -1 && active_textbox != "#number_text"){
			$('#keypad_value').val($('#keypad_value').val()+".");
		}
	});

	$(document).on("click", "#keypad_backspace", function(){
		$('#keypad_value').val($('#keypad_value').val().substring(0,$('#keypad_value').val().length-1));
	});

	$(document).on("click", "#keypad_clear", function(){
		$('#keypad_value').val("");
		$('#keypad_value').focus();
	});
	
	//Keypad button handlers
	$(document).on("click", "#main_keypad_1", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"1");
			}
		}
	});

	$(document).on("click", "#main_keypad_2", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"2");
			}
		}
	});

	$(document).on("click", "#main_keypad_3", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"3");
			}
		}
	});

	$(document).on("click", "#main_keypad_4", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"4");
			}
		}
	});

	$(document).on("click", "#main_keypad_5", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"5");
			}
		}
	});

	$(document).on("click", "#main_keypad_6", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"6");
			}
		}
	});

	$(document).on("click", "#main_keypad_7", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"7");
			}
		}
	});

	$(document).on("click", "#main_keypad_8", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"8");
			}
		}
	});

	$(document).on("click", "#main_keypad_9", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"9");
			}
		}
	});

	$(document).on("click", "#main_keypad_0", function(){
		if($('#main_keypad_triples').hasClass('active') || $('#main_keypad_hundreds').hasClass('active')){
			$(active_main_textbox).val($(this).text());
		}else{
			if(check_char_after_decimal($(active_main_textbox).val().replace("$",""))){
				$(active_main_textbox).val($(active_main_textbox).val()+"0");
			}
		}
	});

	$(document).on("click", "#main_keypad_decimal", function(){
		if($(active_main_textbox).val().indexOf(".") == -1 && active_main_textbox != "#number_text"){
			$(active_main_textbox).val($(active_main_textbox).val()+".");
		}
	});

	$(document).on("click", "#main_keypad_backspace", function(){
		$(active_main_textbox).val($(active_main_textbox).val().substring(0,$(active_main_textbox).val().length-1));
	});

	$(document).on("click", "#main_keypad_clear", function(){
		$(active_main_textbox).val("");
	});
	
	function quick_amount(current){
		var straight;
		if($("#straight_amount_text").val() == ""){
			straight = 0;
		}else{
			straight = parseFloat($("#straight_amount_text").val().replace("$",""));
		}
		
		var boxed;
		if($("#boxed_amount_text").val() == ""){
			boxed = 0;
		}else{
			boxed = parseFloat($("#boxed_amount_text").val().replace("$",""));
		}
		var add_amount = parseFloat($(current).text().replace("$",""));
		
		if($("#straight_toggle").hasClass("active")){
			$("#straight_amount_text").val((add_amount + straight).toFixed(2));
		}
		
		if($("#boxed_toggle").hasClass("active")){
			$("#boxed_amount_text").val((add_amount + boxed).toFixed(2));
		}
	}
	
	$(document).on("click", "#quick_25_keypad", function(){
		quick_amount(this);
	});

	$(document).on("click", "#quick_50_keypad", function(){
		quick_amount(this);
	});
	
	$(document).on("click", "#quick_75_keypad", function(){
		quick_amount(this);
	});

	$(document).on("click", "#quick_100_keypad", function(){
		quick_amount(this);
	});
	
	$(document).on("click", "#quick_150_keypad", function(){
		quick_amount(this);
	});

	$(document).on("click", "#quick_200_keypad", function(){
		quick_amount(this);
	});
	
	$(document).on("click", "#quick_300_keypad", function(){
		quick_amount(this);
	});

	$(document).on("click", "#quick_500_keypad", function(){
		quick_amount(this);
	});
	
	$(document).on("click", "#main_keypad_hundreds", function(){
		$('#main_keypad_triples').removeClass('active');
		
		if($(this).hasClass('active')){
			$(this).text("Back");
			$('#main_keypad_1').text('100');
			$('#main_keypad_2').text('200');
			$('#main_keypad_3').text('300');
			$('#main_keypad_4').text('400');
			$('#main_keypad_5').text('500');
			$('#main_keypad_6').text('600');
			$('#main_keypad_7').text('700');
			$('#main_keypad_8').text('800');
			$('#main_keypad_9').text('900');
			$('#main_keypad_0').text('000');
		}else{
			$(this).text("Hundreds");
			$('#main_keypad_1').text('1');
			$('#main_keypad_2').text('2');
			$('#main_keypad_3').text('3');
			$('#main_keypad_4').text('4');
			$('#main_keypad_5').text('5');
			$('#main_keypad_6').text('6');
			$('#main_keypad_7').text('7');
			$('#main_keypad_8').text('8');
			$('#main_keypad_9').text('9');
			$('#main_keypad_0').text('0');
		}
		
		active_text_box = '#number_text';
	});
	
	$(document).on("click", "#main_keypad_triples", function(){
		$('#main_keypad_hundreds').removeClass('active');
		
		if($(this).hasClass('active')){
			$(this).text("Back");
			$('#main_keypad_1').text('111');
			$('#main_keypad_2').text('222');
			$('#main_keypad_3').text('333');
			$('#main_keypad_4').text('444');
			$('#main_keypad_5').text('555');
			$('#main_keypad_6').text('666');
			$('#main_keypad_7').text('777');
			$('#main_keypad_8').text('888');
			$('#main_keypad_9').text('999');
			$('#main_keypad_0').text('000');
		}else{
			$(this).text("Triples");
			$('#main_keypad_1').text('1');
			$('#main_keypad_2').text('2');
			$('#main_keypad_3').text('3');
			$('#main_keypad_4').text('4');
			$('#main_keypad_5').text('5');
			$('#main_keypad_6').text('6');
			$('#main_keypad_7').text('7');
			$('#main_keypad_8').text('8');
			$('#main_keypad_9').text('9');
			$('#main_keypad_0').text('0');
		}
		
		active_text_box = '#number_text';
	});
	
	//Tell if there is less than 2 characters after the decimal in a string
	function check_char_after_decimal(decimalstring){
		if(decimalstring.length - decimalstring.indexOf(".") >= 3 && decimalstring.indexOf(".") != -1){
			return false;
		}
		return true;
	}

	//Confirm clear cart
	$(document).on("click", "#button_clear_cart", function(){
		$('#clear_confirmation_modal').modal("show");
	});
	
	$(document).on("click", "#clear_confirm", function(){
		//Remove all bets and get rid of the total
		$('#bet_table tbody').html("");
		$('#total').text("0.00");
		
		//Clear out errors
		$('#error_reporting').text("").slideUp("fast");
		
		//Clear counts for choices
		choices_set = {};
		
		//Hide modal
		$('#clear_confirmation_modal').modal("hide");
	});
	
	//Confirm clear cart
	$(document).on("click", "#invalid_tendered_confirm", function(){
		$('#amount_tendered').trigger("focus");
	});

	$(document).on("change", "#straight_amount_text", function(){
		if($(this).val()!=""){
			var formatted_straight = parseFloat($(this).val().replace(",",".").replace("$",""),10);
			
			$(this).val("$"+formatted_straight.toFixed(2));
			$("#straight_toggle").addClass("active");
		}else{
			$("#straight_toggle").removeClass("active");
		}
	});

	$(document).on("change", "#deposit_customer_amount", function(){
		if($(this).val()!=""){
			var formatted_straight = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_straight.toFixed(2));
		}
	});

	$(document).on("blur", "#withdrawal_customer_amount", function(){
		if($(this).val()!=""){
			var formatted_straight = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_straight.toFixed(2));
		}
	});

	$(document).on("blur", "#amount_tendered", function(){
                $("#tender_error").hide();
		if($(this).val()!=""){
			var formatted_tendered = parseFloat($(this).val().replace("$",""),10);
                        var tender_total = parseFloat($('#tender_total').text().replace("$",""),10);
			$(this).val("$"+formatted_tendered.toFixed(2));
                        if(formatted_tendered < tender_total){
                                $("#tender_error").text("Invalid Tender Amount!");
			        $("#tender_error").slideDown("fast");
				$(this).val(""); 
			}else{
				var change = tender_total - formatted_tendered;
				$("#tender_change").text("$"+change.toFixed(2));                                
			}
		}
	});
	
	$(document).on("blur", "#transfer_amount", function(){
		if($(this).val()!=""){
			var formatted_tendered = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_tendered.toFixed(2));
		}
	});
	
	$(document).on("blur", "#boxed_amount_text", function(){
		if($(this).val()!=""){
			var formatted_boxed = parseFloat($(this).val().replace(",",".").replace("$",""),10);
			$(this).val("$"+formatted_boxed.toFixed(2));
			$("#boxed_toggle").addClass("active");
		}else{
			$("#boxed_toggle").removeClass("active");
		}
	});

	$(document).on("blur", "#payout_amount", function(){
		if($(this).val()!=""){
			var formatted_boxed = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_boxed.toFixed(2));
		}
	});

	$(document).on("blur", "#open_shift_coins", function(){
		if($(this).val()!=""){
			var formatted_boxed = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_boxed.toFixed(2));
		}
	});

	$(document).on("blur", "#open_shift_float", function(){
		if($(this).val()!=""){
			var formatted_boxed = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_boxed.toFixed(2));
		}
	});

	$(document).on("blur", "#close_shift_coins", function(){
		if($(this).val()!=""){
			var formatted_boxed = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_boxed.toFixed(2));
		}
	});
	
	//Start Random number generators for quick buttons
	$(document).on("click", '#button_qk3', function(){
		var new_rand = pad(Math.round(Math.random()*999).toString(), 3);
		$('#number_text').val(new_rand);
	});
	
	$(document).on("click", '#button_qk4', function(){
		var new_rand = pad(Math.round(Math.random()*9999).toString(), 4);
		$('#number_text').val(new_rand);
	});
	
    $("#number_text").keydown(function (e) {
        
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
		     // Allow: Ctrl+A, Command+A
		    (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
		     // Allow: home, end, left, right, down, up
		    (e.keyCode >= 35 && e.keyCode <= 40)) {
		         // let it happen, don't do anything
		         return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		    e.preventDefault();
		}
       });
        $("#straight_amount_text,#boxed_amount_text").keydown(function (e) {
               
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13,110,190,188]) !== -1 ||
		     // Allow: Ctrl+A, Command+A
		    (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
		     // Allow: home, end, left, right, down, up
		    (e.keyCode >= 35 && e.keyCode <= 40)) {
		         // let it happen, don't do anything
		         return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		    e.preventDefault();
		}
       });

	$(document).on("click", '#purchase_bets_button', function(){
		//Make the button go into a loading state
		var button_state = $(this).button("loading");
		
		var tbl = $('#bet_table tbody tr').get().map(function(row) {
			return $(row).find('td').get().map(function(cell) {
				return $(cell).html();
			});
		});
		
		$.ajax({
			url: "ajax/lotto_betting.php",
			data: {action: "insert", data: tbl, total: $('#total').text()},
			method: "POST"
		})
		.done(function( data ) {
			if(data.indexOf("SuccessfullyProcessed")==-1){
				bootbox.alert(data);
				button_state.button('reset');
			}else{
				var id = data.split("|");
				$("#ticket_id").val(id[1]);
				$("#tender_checksum").val(id[2]);
				$("#tender_total").text("$"+$('#total').text());
				$("#tender_modal").modal("show");
				button_state.button('reset');
			}
		});
	});

	$(document).on("click", '#cancel_transaction', function(){
		$.ajax({
				url: "ajax/lotto_betting.php",
				data: {action: "cancel", checksum: $("#tender_checksum").val(), ticket_id: $("#ticket_id").val()},
				method: "POST"
			});
	});

	$(document).on("click", "#reprint_receipt", function(){
		//window.open("/franchise/receipts/get_receipt.php?ticket_id="+$("#ticket_id").val(),"receipt");
		document.getElementById('receipt').contentWindow.location.reload();
	});

	$(document).on("click", "#cancel_receipt", function(){
		window.open("/franchise/receipts/get_receipt.php","receipt");
	});
	
	$(document).on("click", "#exact_amount", function(){
		$("#amount_tendered").val($('#tender_total').text());
		$("#tender_change").text("$0.00");
	});

	$(document).on("click", '#finalize_transaction', function(){
		if($("#amount_tendered").val()==""){
			$("#tender_error").text("Amount Tendered Must Be Entered!");
			$("#tender_error").slideDown("fast");
		}else{
			//Make the button go into a loading state
			$('#cancel_transaction').attr('disabled',true);
			var button_state = $(this).button("loading");
			
			var tbl = $('#bet_table tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});
			
			$.ajax({
				url: "ajax/lotto_betting.php",
				data: {action: "finalize", checksum: $("#tender_checksum").val(), ticket_id: $("#ticket_id").val(), total: $('#tender_total').text().replace("$",""), tender: $("#amount_tendered").val()},
				method: "POST"
			})
			.done(function( data ) {
				if(data.trim()!=""){
					bootbox.alert(data);
					$('#tender_modal').modal("hide");
				}else{
					window.open("/franchise/receipts/get_receipt.php?ticket_id="+$("#ticket_id").val(),"receipt");
					//$("#receipt_modal").modal("show");
					$("#cashier_balance").text((Number($("#cashier_balance").text().replace(",",""), 10) + Number($("#total").text().replace("$",""), 10)).formatMoney(2));
                                        $('#virtual_balance').text((Number($("#virtual_balance").text().replace(",",""), 10) - Number($("#total").text().replace("$",""), 10)).formatMoney(2));
					$("#tender_error").hide();
					$('#tender_modal').modal("hide");
					$("#amount_tendered").val("");
					$('#tender_change').text("");
					button_state.button('reset');
					$("#bet_table").children("tbody").html("");
					$("#number_text").val("");
					$("#error_reporting").fadeOut("fast");
					$("#straight_amount_text").val("");
					$("#boxed_amount_text").val("");
					$("#total").text("0.00");
					$.each($(".house_btn.active"), function() {
						$(this).removeClass("active"); 
					});
                                        $.each($(".bottom_btn.active"), function() {
						$(this).removeClass("active"); 
					});
				}
				$('#cancel_transaction').attr('disabled',false);
			});
		}
	});
	
	$(document).on("click", '.remove_bet_button', function(){
		curr_remove_button = this;
		$('#remove_confirmation_modal').modal("show");
		$('#remove_confirm').removeAttr("disabled");
	});
	
	$(document).on("click", '#remove_confirm', function(){
		//Update total
		 $(this).attr("disabled", true);
		var curr_subtotal = parseFloat($(curr_remove_button).parent('td').siblings('.subtotal').children('span').text());
		var curr_total = parseFloat($('#total').text());
		var new_total = curr_total - curr_subtotal;
		$('#total').text(new_total.toFixed(2));
		
		//Hide modal
		$('#remove_confirmation_modal').modal("hide");
		
		//Hide errors
		$('#error_reporting').text("").slideUp("fast");
		
		//Decrement count for the choice number
		choices_set[parseInt($(curr_remove_button).parent('td').parent('tr').children('td:first-child').text())]-=1;
		
		//Remove Row
		$(curr_remove_button).parent('td').parent('tr').fadeOut('fast', function(){
			$(this).remove();
		});
	});
	
	$(document).on("click", '#button_add_cart', function(){
		var found_error = false;
		var errors = "";
		
		//Parse and format the money fields that will be compared and calculated
		var formatted_straight = Number($("#straight_amount_text").val().replace("$",""),10);
		var formatted_boxed = Number($("#boxed_amount_text").val().replace("$",""),10);
		var total = Number($('#total').text());
		
		if($('#number_text').val() == ""){
			errors += 'You must enter a number to bet.<br>';
			found_error = true;
		}
		
		if($("#straight_amount_text").val() == "" && $("#straight_toggle").hasClass("active")){
			errors += "You must enter a straight amount.<br>";
			found_error = true;
		}
		
		if($("#boxed_amount_text").val() == "" && $("#boxed_toggle").hasClass("active")){
			errors += "You must enter a boxed amount.<br>";
			found_error = true;
		}
		
		//boolean used to determine if any houses were checked
		var houses_checked = false;
		
		//Check for all checked boxes
		$.each($(".house_btn.active"), function() {
			//Update the boolean to say there was a box checked
			houses_checked = true;
			
			if(!found_error){
				//Verify the numbers added
				if($("#straight_toggle").hasClass("active")){
					if(!isNaN(formatted_straight)){
						total += formatted_straight;
						$("#bet_table tbody").append("<tr><td>"+$('#number_text').val()+"</td><td style='display: none;'>"+$(this).attr('house_id')+"</td><td>"+$(this).attr("short_house")+"</td><td>S</td><td class='subtotal'>$<span>"+formatted_straight.toFixed(2)+"</span><td><button class='btn btn-danger remove_bet_button'><i class='fa fa-trash'></i></button></td>");
						if(typeof choices_set[$('#number_text').val()] == 'undefined'){
							choices_set[$('#number_text').val()]=1;
						}else{
							choices_set[$('#number_text').val()]+=1;
						}
					}else{
						errors += 'Invalid Straight Amount<br>';
					}
				}
				if($("#boxed_toggle").hasClass("active")){
					if(!isNaN(formatted_boxed)){
						total += formatted_boxed;
						$("#bet_table tbody").append("<tr><td>"+$('#number_text').val()+"</td><td style='display: none;'>"+$(this).attr('house_id')+"</td><td>"+$(this).attr("short_house")+"</td><td>B</td><td class='subtotal'>$<span>"+formatted_boxed.toFixed(2)+"</span><td><button class='btn btn-danger remove_bet_button'><i class='fa fa-trash'></i></button></td>");
						if(typeof choices_set[$('#number_text').val()] == 'undefined'){
							choices_set[$('#number_text').val()]=1;
						}else{
							choices_set[$('#number_text').val()]+=1;
						}
					}else{
						errors += "Invalid Boxed Amount<br>";
					}
				}
			}
			
			//Update the total
			$('#total').text(total.toFixed(2));
		});
		
		if(!houses_checked){
			errors += "Please Choose a House.<br>";
		}
		
		if(errors != ""){
			bootbox.alert(errors);
		}
	});

	//Start the code for handling deposits
	$(document).on("click", "#deposit_button", function(){
		$("#deposit_modal").modal("show");

	});
});
