<?php
	date_default_timezone_set('America/New_York');
	
	// force SSL connection
	//if($_SERVER["HTTPS"] != "on"){
		//header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
		//exit();
	//}
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	// see if we need to logout
	if(isset($_GET['action'])){
		if($_GET['action'] == 'logout'){
			$session->logout();
		}
	}
   	
	// hard code security to prevent access to non-cashier users
	$q = "SELECT * FROM `panel_user` WHERE `user_id` = '".$session->userinfo['id']."'";
	$cashier_info = $db->queryOneRow($q);
	
	if($cashier_info['user_id'] != $session->userinfo['id'] && $session->logged_in){
		echo "<html><head><link rel='stylesheet' type='text/css' href='/css/default.css?v=<?=CSS_VERSION?>' /></head><body class='redirect_body'><div style='text-align:center;'><img src='/images/asw_logo.png' alt='logo'>";
		echo "<h3>You must be a cashier to access this site.<br>";
	?>
		<p>You will be redirected in <span id="counter">5</span> second(s).</p>
		<script type="text/javascript">
		function countdown() {
		    var i = document.getElementById('counter');
		    i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
		</script>
	<?php 
		echo "</div></body></html>";
		$session->logout();
		header( "refresh:5;url='".ROOTPATH."'" ); 
		die();
	}elseif($cashier_info['is_disabled'] == 1){
		// see if account exists but was disabled
		echo "<html><head><link rel='stylesheet' type='text/css' href='/css/default.css?v=<?=CSS_VERSION?>' /></head><body class='redirect_body'><div style='text-align:center;'><img src='/images/asw_logo.png' alt='logo'>";
		echo "<h3>Your cashier account has been disabled.<br>";
	?>
		<p>You will be redirected in <span id="counter">5</span> second(s).</p>
		<script type="text/javascript">
		function countdown() {
		    var i = document.getElementById('counter');
		    i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
		</script>
	<?php 
		echo "</div></body></html>";
		$session->logout();
		header( "refresh:5;url='".ROOTPATH."'" );
		die();
	} else if(!empty($_SESSION['invalid_location'])){
                echo "<html><head><link rel='stylesheet' type='text/css' href='/css/default.css?v=<?=CSS_VERSION?>' /></head><body class='redirect_body'><div style='text-align:center;'><img src='/images/asw_logo.png' alt='logo'>";
		echo "<h3>You are not authorized to access this panel from this Location<br>";
	?>
		<p>You will be redirected in <span id="counter">5</span> second(s).</p>
		<script type="text/javascript">
		function countdown() {
		    var i = document.getElementById('counter');
		    i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
		</script>
	<?php 
		echo "</div></body></html>";
		$session->logout();
		header( "refresh:5;url='".ROOTPATH."'" ); 
		die();

       }
	
	$page_title = "CSCLotto Cashier Panel";
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title><?php if(isset($page_title)){echo $page_title;}else{echo "CSCLotto.com - The Sure Way to Win!";}?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="icon" type="image/x-icon" href="/images/favicon.ico" />
		
		<!-- jQuery -->
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery/jquery-1.11.0.js"></script>
		
		<!-- jQuery UI -->
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery-ui-1.11.4/jquery-ui.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/lib/assets/jquery-ui-1.11.4/jquery-ui.min.css">
		<link rel="stylesheet" type="text/css" href="/lib/assets/jquery-ui-1.11.4/jquery-ui.theme.min.css">
		
		<script type="text/javascript">
			// fix compatibility issues with same class names in jquery ui and bootstrap
			$.widget.bridge('uitooltip', $.ui.tooltip);
			$.widget.bridge('uibutton', $.ui.button);
		</script>

		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap.min.css" media="screen">
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap-buttons.min.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/modern-business.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/tooltips.css" />
		
		<!-- <link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-xl/BootstrapXL.css" /> -->
		
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/jqBootstrapValidation.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/tooltips.js"></script>
		
		<!-- Google Fonts -->
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine|Inconsolata|Droid+Sans" media="screen">
		
		<!-- Font Awesome -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/font-awesome-4.2.0/css/font-awesome.min.css" />
		
		<!-- DataTables -->
		<!-- <link rel="stylesheet" type="text/css" href="/lib/assets/datatables/media/css/jquery.dataTables.css"> -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/datatables/extensions/Plugins/integration/bootstrap/3/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="/lib/assets/datatables/extensions/Responsive/css/dataTables.responsive.css">
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/extensions/Plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
		
		<!-- High Charts -->
		<script src="http://code.highcharts.com/stock/highstock.js"></script>
		<script src="http://code.highcharts.com/stock/modules/exporting.js"></script>
		
		<!-- DataTables Responsive Plugin (have to load it here after main datatables js finishes loading -->
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/extensions/Responsive/js/dataTables.responsive.js"></script>
		
		<!-- AJAX Push -->
                <?php if($session->logged_in){ ?>
		<script src="/lib/framework/push_ajax.js"></script>
                <?php } ?>
		
		<!-- Modernizr: For HTML5/CSS3 Feature Detection -->
		<script src="/lib/assets/modernizr/modernizr.custom_min.js"></script>
		
		<!-- Bootstrap DateTimePicker (http://eonasdan.github.io/bootstrap-datetimepicker/) -->
		<script type="text/javascript" src="/lib/assets/moment/moment.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
		<link rel="stylesheet" href="/lib/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" />
		
		<!-- BootBox Alerts -->
		<script type="text/javascript" src="/lib/assets/bootbox/bootbox.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-select/dist/css/bootstrap-select.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
		<script>
			$(function () {
				// initialize inline datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
				
				// initialize datepickers
				$('.dp').datetimepicker({
					showTodayButton: true,
					format: 'MM/DD/YYYY'
				});
				
				// initialize datetimepickers
				$('.dtp').datetimepicker({
					showTodayButton: true,
					format: 'MM/DD/YYYY h:mm A'
				});
			});
			$(document).ready(function() {
				Modernizr.load({
					test: Modernizr.placeholder,
					nope: '/lib/assets/modernizr/polyfills/Placeholders.min.js'
				});
			});
		</script>
	</head>
	<body>
<?php
	// include 2FA Code
	include($_SERVER['DOCUMENT_ROOT'].'/lib/framework/2fa_modal.php');

	if(!$session->logged_in){
		if(isset($_GET['forgotpass'])){
			include 'forgotpass.php';
		}else{
			include 'login.php';
		}
	}
	
	// Determine the page that is loaded so that it is showed as the selected item
	// in the side navigation bar.

	$accordion_and_link = ltrim(str_replace($_SERVER['DOCUMENT_ROOT']."/admin", "",dirname($_SERVER["SCRIPT_FILENAME"])),"/");
	$accordion_and_link = explode("/", $accordion_and_link);
	if(count($accordion_and_link) == 2){
		$accordion_opened = $accordion_and_link[0];
		$link_opened = $accordion_and_link[1];
	} else {
		$accordion_opened = "";
		$link_opened = "";
	}
?>
	<link rel="stylesheet" type="text/css" href="/franchise/css/global.css?v=<?=CSS_VERSION?>" />
	
<?php
	// force password change
	if($session->userinfo['force_password_change'] != 0){
?>	
		<div id="header_bar">
			<span id="welcome"><h3>Forced Password Change</h3></span>
			<span id="logout">
				<form action='<?php echo ROOTPATH; ?>/lib/framework/login.php' method='post'>
					<input name='logout' class='button' type='submit' action='logout' value='Logout'>
					<input name='referrer' type='hidden' value='/franchise/index.php'>
				</form>
			</span>
			<span id="logged_inas"><?php echo "Logged in as: ".$session->userinfo['firstname']." ".$session->userinfo['lastname']; ?></span>
		</div>
		
		<form action="lib/framework/login.php" method="POST" style="padding-left: 30px;">
			<div class="row">
				<?php
				if($form->num_errors > 0){
				echo "<td><font size=\"2\" color=\"#ff0000\">".$form->num_errors." error(s) found</font></td>";
				}
				?>
			</div>
			<div class="row">
				<div class="col-md-4">Current Password:</div>
				<input type="password" name="curpass" maxlength="30" value="<?php echo $form->value("curpass"); ?>">
				<?php echo $form->error("curpass"); ?>
			</div>
			<div class="row">
				<div class="col-md-4">New Password:</div>
				<input type="password" name="newpass" maxlength="30" value="<?php echo $form->value("newpass"); ?>">
				<?php echo $form->error("newpass"); ?>
				<br>
			</div>
			<div class="row">	
				<div class="col-md-4">Confirm New Password:</div>
				<input type="password" name="conf_newpass" maxlength="30" value="<?php echo $form->value("newpass"); ?>">
				<?php echo $form->error("newpass"); ?>
				<br>
			</div>
			<div class="row">	
				<div class="col-md-4">Email:</div>
				<input style="width: 350px;" type="text" name="email" maxlength="100" value="<?php
				if($form->value("email") == ""){
				echo $session->userinfo['email'];
				}else{
				echo $form->value("email");
				}
				?>">
			</div>
			<div class="row">
				<?php echo $form->error("email"); ?>
				<br>
				<input type="hidden" name="subedit" value="1">
				<button class="btn btn-primary" type="submit">Submit</button>
			</div>
		</form>
<?php
		include("footer.php");
		die();
	}
?>
	<style>
		body{
			margin-top: -40px;
		}
	</style>
