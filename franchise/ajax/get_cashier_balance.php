<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	date_default_timezone_set('America/New_York');

        $t_virtual_balance = $core->get_virtual_balance();

        echo json_encode(array('balance'=>number_format($session->userinfo['panel_user']['balance']),'virtual_balance'=>number_format($t_virtual_balance['virtual_money'])));

	//echo number_format($session->userinfo['panel_user']['balance'], 2);
	
