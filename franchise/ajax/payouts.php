<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

if($session->userinfo['panel_user']['shift_id'] == "-1"){
	echo json_encode(array("success" => "false", "errors" => "You must start a shift!"));
}else{
	if(isset($_POST['action']) && $_POST['action'] == "get_partial_payouts"){
		$q = "SELECT * FROM `winner_payout` WHERE `ticket_number`=%i;";
		$ticket_winning_information = $db->queryOneRow($q, array($_POST['ticket_number']));
		
		// get ticket info to see if it was voided
		$q = "SELECT * FROM `lotto_ticket` AS `t` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id`=`l`.`id` LEFT JOIN `winner_payout` AS `wp` ON `t`.`ticket_number`=`wp`.`ticket_number` LEFT JOIN `users` AS `v` ON `v`.`id`=`t`.`voided_by` WHERE `t`.`ticket_number`=%i;";
		$ticket_info = $db->queryOneRow($q, array($_POST['ticket_number']));
		
		if($ticket_info['is_voided'] == 1){
			$void_status = "<span style='color: red;'>Voided by ".$ticket_info['firstname']." ".$ticket_info['lastname']." (".$ticket_info['voided_by'].") <br>on ".date("m/d/Y", strtotime($ticket_info['voided_on']))." at ".date("h:i:s A", strtotime($ticket_info['voided_on']))."</span>";
		}else{
			$void_status = "<span style='color: green;'>Active</span>";
		}
		
		if($ticket_winning_information != null && $ticket_info['is_voided'] != 1){
			$data = $core->get_partial_payouts($_POST['ticket_number']);

			$table = "";
			$total_paid = 0;
			if($data != null){
				for($x=0; $x<count($data); $x++){
					$table .= "<tr>";
					$table .= "<td>".$data[$x]['type']."</td>";
					if($data[$x]['firstname']==NULL){
						$table .= "<td>Online</td>";
					}else{
						$table .= "<td>".$data[$x]['firstname']." ".$data[$x]['lastname']."</td>";
					}
					$table .= "<td>".number_format(abs($data[$x]['amount']))." " .CURRENCY_FORMAT."</td>";
					$table .= "</tr>";

					$total_paid += abs($data[$x]['amount']);
				}
			}

			$max_payout = round($ticket_winning_information["total_payout"]-$total_paid,2);
			$total_paid = number_format($total_paid);
			$max_payout = number_format($max_payout);
			$ticket_winning_information["total_payout"] = number_format($ticket_winning_information["total_payout"]);
			$expired = $core->is_ticket_expired($_POST['ticket_number']);
			if($expired){
				$max_payout = "Expired";
			}
			
			// see if we need to create any payout notices based on rules in db
			$q = "SELECT * FROM `cashier_payout_notices`";
			$notice_queries = $db->query($q);
			foreach($notice_queries as $n_query){
				$q = "SELECT COUNT(*) AS alert_count FROM `".$n_query['table_name']."` WHERE `".$n_query['column_name']."` ".$n_query['comparison']." '".$n_query['value']."' AND `ticket_number` = '".$_POST['ticket_number']."'";
				$notice_alert = $db->queryOneRow($q);
				if($notice_alert['alert_count'] > 0){
					$notices .= '
						<div class="alert alert-danger payout_alert">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<h4>NOTICE:</h4>
							<b>'.$n_query['title'].'</b> - '.$n_query['message'].'
						</div>
					';
				}
			}
			
			$return = array("success" => "true", "table" => $table, "number_of_wins" => $ticket_winning_information["num_of_wins"], "total_won" => $ticket_winning_information["total_payout"], "max_payout" => $max_payout, "total_paid" => $total_paid, "void_status" => $void_status, "voided_comment" => $ticket_info['voided_comment'], "notices" => $notices);
		}else{
			$return = array("success" => "true", "table" => "", "number_of_wins" => "0", "total_won" => "0.00", "max_payout" => "0.00", "total_paid" => "0.00", "void_status" => $void_status, "voided_comment" => $ticket_info['voided_comment']);
		}

		echo json_encode($return);
	}

	if(isset($_POST['action']) && $_POST['action'] == "add_payout"){

		//$ticket_winning_information = $db->queryOneRow("SELECT * FROM `winner_payout` WHERE `ticket_number`=".$_POST['ticket_number']);
		$q = "SELECT * FROM `winner_payout` WHERE `ticket_number`=%i;";
		$ticket_winning_information = $db->queryOneRow($q, array($_POST['ticket_number']));

		if($ticket_winning_information != null){
			$expired = $core->is_ticket_expired($_POST['ticket_number']);
			if($expired){
				$return = array("success" => "false", "errors" => "This ticket has expired!");
				echo json_encode($return);
				die();
			}

			$data = $core->get_partial_payouts($_POST['ticket_number']);

			$total_paid = 0;
			if($data != null){
				for($x=0; $x<count($data); $x++){
					$total_paid += abs($data[$x]['amount']);
				}
			}

			$_POST['amount'] = str_replace("$","",$_POST['amount']);
			$maxa = round($ticket_winning_information["total_payout"] - $total_paid, 2);
			if($_POST['amount'] > $maxa){
				$return = array("success" => "false", "errors" => "Amount cannot be higher than ".$maxa." (".$_POST['amount'].")");
				echo json_encode($return);
				die();
			}elseif($_POST['amount'] == $maxa){

				//$db->query("UPDATE `winner_payout` SET `status_id`=1 WHERE ticket_number='".$_POST['ticket_number']."'");
				$q = "UPDATE `winner_payout` SET `status_id`=1 WHERE ticket_number=%i;";
				$db->query($q, array($_POST['ticket_number']));

			}else{

				//$db->query("UPDATE `winner_payout` SET `status_id`=3 WHERE ticket_number='".$_POST['ticket_number']."'");
				$q = "UPDATE `winner_payout` SET `status_id`=3 WHERE ticket_number=%i";
				$db->query($q, array($_POST['ticket_number'])); 


			}

			$table = "<tr><td>Panel</td><td>".$session->userinfo['firstname']." ".$session->userinfo['lastname']."</td><td>$".$_POST['amount']."</td></tr>";
			$trans_id = $core->make_panel_user_transaction(-$_POST['amount'], 12, "Paid Out a Lotto Bet on Ticket - ".$_POST['ticket_number'], $_POST['ticket_number']);
			if($trans_id !== false){
				$max_payout = round($ticket_winning_information["total_payout"]-$total_paid-$_POST['amount'],2);
				$max_payout = number_format($max_payout);
				$total_paid = $total_paid+$_POST['amount'];
				$total_paid = number_format($total_paid);
				$return = array("success" => "true", "transaction_id" => $trans_id,"table" => $table, "max_payout" => $max_payout, "total_paid" => $total_paid);
			}else{
				$return = array("success" => "false", "errors" => "The payout exceeds your balance!");
			}
		}else{
			$return = array("success" => "false", "errors" => "This Ticket Is Not A Winner!");
		}

		echo json_encode($return);
	}
}
