<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	date_default_timezone_set('America/New_York');

	//Used to get the balance of the cashier
	if(isset($_POST['action']) && $_POST['action'] == "get_balance"){
		$balance = $core->check_balance("panel_user");
                $i_virtual_balance = $core->get_virtual_balance();
		echo json_encode(array("balance" => $balance['balance'],"virtual_balance" => $i_virtual_balance['virtual_money']));
	}

	//Used to remember that the user would like to auto hide inactive games or vice versa
	if(isset($_POST['action']) && $_POST['action'] == "set_auto_hide"){
		print_r( $db->query("UPDATE customers SET auto_hide_games=".$_POST['value']." WHERE user_id=".$session->userinfo['id'].""));
	}

	//This is used to return a json variable with a list of game_id's that should be inactive
	if(isset($_POST['action']) && $_POST['action'] == "get_inactive_games"){
		$return_array = array();
		
		$date = new datetime('now');
		$date_str = $date->format("H:i:s");
		$house = $db->query("SELECT h.id FROM lotto_house h WHERE web_cutoff_time<='".$date_str."'");
		
		for($x=0; $x<count($house); $x++){
			// change to time count down
			
			array_push($return_array, $house[$x]["id"]);
		}
		
		echo implode(",",$return_array);
	}
	
	//This is used to return a json variable with a list of game_id's that should be inactive
	if(isset($_POST['action']) && $_POST['action'] == "get_active_games"){
		$return_array = array();
		
		$now_dt = new datetime('now');
		$today = $now_dt->format("Y-m-d");
		$houses = $db->query("SELECT id, name, short_name, web_cutoff_time FROM lotto_house");
		
		for($x=0; $x<count($houses); $x++){
		$cut_str = $today." ".$houses[$x]['web_cutoff_time'];
			$cutoff_dt = new DateTime($cut_str);
			
			if($cutoff_dt > $now_dt){
				$houses[$x]['time_remaining'] = $core->format_interval($now_dt->diff($cutoff_dt), false, false, true);
			}else{
				$houses[$x]['time_remaining'] = 'Closed';
			}
		}
		
		echo json_encode($houses);
	}

	if($session->userinfo['panel_user']['shift_id'] == "-1"){
		echo "You must start a shift!";
	}else{
		if(isset($_POST['action']) && $_POST['action'] == "finalize"){
			if($_POST['checksum'] == sha1(md5($_POST['ticket_id']."YOUCANTGUESSME"))){
				$tender = str_replace("$","",$_POST['tender']);
				$change = $tender-$_POST['total'];

				//$ticket_info = $db->queryOneRow("SELECT ticket_number,commission_rate FROM `lotto_ticket` WHERE `ticket_id`=".$_POST["ticket_id"]);
				$q = "SELECT ticket_number,commission_rate FROM `lotto_ticket` WHERE `ticket_id`=%i;";
				$ticket_info = $db->queryOneRow($q, array($_POST["ticket_id"]));

				//$db->query("UPDATE `lotto_ticket` SET commission_amount='".$ticket_info['commission_rate']*$_POST['total']."', `tender`=".$tender.", `total`=".$_POST['total'].", `change`=".$change." WHERE ticket_id=".$_POST['ticket_id']);
				$q = "UPDATE `lotto_ticket` SET commission_amount=%i, `tender`=%i, `total`=%f, `change`=%f WHERE ticket_id=%i;";
				$db->query($q, array($ticket_info['commission_rate']*$_POST['total'], $tender, $_POST['total'], $change, $_POST['ticket_id'])); 

				$core->make_panel_user_transaction($_POST['total'], 7, "Purchased Lotto Ticket - ".$ticket_info['ticket_number'], $ticket_info['ticket_number']);
			}else{
				//Hack Attempted
			}
		}

		if(isset($_POST['action']) && $_POST['action'] == "cancel"){
			if($_POST['checksum'] == sha1(md5($_POST['ticket_id']."YOUCANTGUESSME"))){

				//$db->query("DELETE FROM `lotto_ticket` WHERE ticket_id=".$_POST['ticket_id']);
				$q = "DELETE FROM `lotto_ticket` WHERE ticket_id=%i;";
				$db->query($q, array($_POST['ticket_id'])); 

				//$db->query("DELETE FROM `lotto_bet` WHERE ticket_id=".$_POST['ticket_id']);
				$q = "DELETE FROM `lotto_bet` WHERE ticket_id=%i;";
				$db->query($q, array($_POST['ticket_id']));

			}
		}

		if(isset($_POST['action']) && $_POST['action'] == "insert"){
			$number_of_bets = count($_POST['data']);
			$date = new datetime('now');
			$date_str = $date->format("Y-m-d H:i:s");
			$error_detected = false;
			$t_virtual_balance = $core->get_virtual_balance();
                        			
                        $limits = $core->get_bet_limits();
		
			if($number_of_bets!=0){
				for($x=0;$x<$number_of_bets;$x++){

					//$house = $db->query("SELECT *, g.id AS game_id FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id WHERE h.is_disabled=0 AND g.id=".$_POST['data'][$x][1]." AND g.number_of_balls=".strlen($_POST['data'][$x][0]));
					$q = "SELECT *, g.id AS game_id FROM lotto_house h JOIN lotto_game g ON h.id=g.house_id WHERE h.is_disabled=0 AND h.id=%i AND g.number_of_balls=%i;";
					$house = $db->query($q, array($_POST['data'][$x][1], strlen($_POST['data'][$x][0])));
					$_POST['data'][$x][1] = $house[0]['game_id'];
					$web_cutoff_time = new datetime($date->format("Y-m-d")." ".$house[0]["web_cutoff_time"]);
					
					if(isset($house[0])){
						//Validate that the game is allowed to be played at this specific time of the day and on sundays
						if($house[0]["plays_on_sunday"]==0 && $date->format("w")==0){
							//User is trying to play a game that cant be played on sunday, on a sunday
							echo $_POST['data'][$x][2]." is not active on Sundays<br>";
							$error_detected = true;
						}else{
							if($web_cutoff_time <= $date){
								//User is trying to play a game that is past the cut off time.
								echo $_POST['data'][$x][2]." is past the the cut off time for the day<br>";
								$error_detected = true;
							}
						}
						
						//Validate that the amount bet is valid per game played
						$bet_amount = ltrim($_POST['data'][$x][4], '$<span>');
						$bet_amount = rtrim($bet_amount, "</span>");
						if($house[0]["web_min_bet"]>$bet_amount || $house[0]["web_max_bet"]<$bet_amount){
							echo "You can only bet between ".$house[0]["web_min_bet"]." and ".$house[0]["web_max_bet"]." for ".$_POST['data'][$x][2]."<br>";
							$error_detected = true;
						}
						
						$running_total = $core->bet_running_total($house[0]['game_id'], $_POST['data'][$x][0]);
						
						$is_boxed = $_POST['data'][$x][3] == "S" ? 0 : 1;
						$pay_factored_bet = $core->get_pay_factor($_POST['data'][$x][0], $bet_amount, $is_boxed);
						
						if($running_total + $pay_factored_bet > $limits[strlen($_POST['data'][$x][0])]){
							if($running_total==$limits[$house[0]['number_of_balls']]){
								echo $_POST['data'][$x][0]." for ".$_POST['data'][$x][2]." is sold out.<br>";
							}else{
								if($_POST['data'][$x][3] == "S"){
									$bet_limit = $limits[strlen($_POST['data'][$x][0])]-$running_total;
									$type = "straight";
								}else{
									$bet_limit = ($limits[strlen($_POST['data'][$x][0])]-$running_total)*count($core->permutations($_POST['data'][$x][0], true));
									$type = "boxed";
								}
								echo "You can only bet up to ".(number_format($bet_limit)). " " . CURRENCY_FORMAT." on ".$type." ".$_POST['data'][$x][0]." for ".$_POST['data'][$x][2]." before it is sold out.<br>";
							}
							$error_detected = true;
						}
					}else{
						echo strlen($_POST['data'][$x][0])." ball game does not exists for ".$_POST['data'][$x][2]."<br>";
						$error_detected = true;
					}
				}

				if(!$error_detected){
					//Generate Ticket Number
                                        if(!empty($t_virtual_balance['virtual_money']) && $t_virtual_balance['virtual_money'] > 0 && $t_virtual_balance['virtual_money'] >= $_POST['total']){

					$ticket_number = $core->generate_ticket_number();
					$expiration_date = $core->get_expiration_date();

					//$q = "SELECT commission_rate FROM panel_location WHERE id=".$session->userinfo['panel_user']['location_id'];
					$q = "SELECT commission_rate FROM panel_location WHERE id=%i;";
					//$commission_rate = $db->queryOneRow($q);
					$commission_rate = $db->queryOneRow($q, array($session->userinfo['panel_user']['location_id']));

					//Add lotto ticket before adding the transaction so you can get its ID.

					//$q = "INSERT INTO `lotto_ticket` (`ticket_number`,`commission_rate`,`purchase_date`,`number_of_bets`,`user_id`,`is_voided`,`cashier_id`,`location_id`,`shift_id`,`expiration_date`) VALUES ('".$ticket_number."','".$commission_rate['commission_rate']."','".$date_str."',".$number_of_bets.",-9999,0,".$session->userinfo['id'].",".$session->userinfo['panel_user']['location_id'].",".$session->userinfo['panel_user']['shift_id'].", '".$expiration_date."')";
					$q = "INSERT INTO `lotto_ticket` (`ticket_number`,`commission_rate`,`purchase_date`,`number_of_bets`,`user_id`,`is_voided`,`cashier_id`,`location_id`,`shift_id`,`expiration_date`) VALUES (%i,%f,%s,%i,-9999,0,%i,%i,%i,%s);";
					//$ticket_id = $db->queryInsert($q);
					$ticket_id = $db->queryInsert($q, array($ticket_number, $commission_rate['commission_rate'], $date_str, $number_of_bets, $session->userinfo['id'], $session->userinfo['panel_user']['location_id'], $session->userinfo['panel_user']['shift_id'], $expiration_date));

					echo "SuccessfullyProcessed|".$ticket_id."|".sha1(md5($ticket_id."YOUCANTGUESSME"));
					
					for($x=0;$x<$number_of_bets;$x++){
						//Get next draw date from the database to store in the bets table

						//$draw_date = $db->queryOneRow("SELECT wn.next_draw_date FROM lotto_game_xml_link link JOIN xml_lotto_winning_numbers wn ON link.xml_game_id = wn.game_id WHERE link.game_id=".$_POST['data'][$x][1]." ORDER BY wn.next_draw_date DESC LIMIT 1");
						$q = "SELECT wn.next_draw_date FROM lotto_game_xml_link link JOIN xml_lotto_winning_numbers wn ON link.xml_game_id = wn.game_id WHERE link.game_id=%i ORDER BY wn.next_draw_date DESC LIMIT 1";
						$draw_date = $db->queryOneRow($q, array($_POST['data'][$x][1]));

						//Set up the ball string to be put into the database
						$balls = str_split($_POST['data'][$x][0]);
						$number_of_balls = count($balls);
						sort($balls, SORT_NUMERIC);
						for($y=0;$y<6;$y++){
							if(!isset($balls[$y])){
								array_push($balls, "");
							}
						}
						
						//Make int boolean for boxed
						if($_POST['data'][$x][3]=="B"){
							$boxed=1;
						}else{
							$boxed=0;
						}
						
						//Parse for bet amount
						$bet_amount = ltrim($_POST['data'][$x][4], '$<span>');
						$bet_amount = rtrim($bet_amount, "</span>");

						//Calculate the pay factor. Straight will be the bet amount. Boxed is the bet amount divided by the factorial of the number of balls in the bet
						$pay_factor = $core->get_pay_factor($_POST['data'][$x][0], $bet_amount, $boxed);
						
						//Get the payrate scheme id

						//$q = "SELECT payout_scheme_id FROM `panel_location` WHERE id=".$session->userinfo['panel_user']['location_id'];
						$q = "SELECT payout_scheme_id FROM `panel_location` WHERE id=%i;";
						//$payout_scheme_id = $db->queryOneRow($q);
						$payout_scheme_id = $db->queryOneRow($q, array($session->userinfo['panel_user']['location_id']));
						
						//$db->queryInsert("INSERT INTO `lotto_bet` (`ticket_id`,`game_id`,`ball_1`,`ball_2`,`ball_3`,`ball_4`, `ball_5`, `ball_6`, `ball_string`, `is_boxed`, `bet_amount`, `pay_factor`, `is_processed`, `draw_date`, `payout_scheme_id`) VALUES (".$ticket_id.",".$_POST['data'][$x][1].",'".$balls[0]."','".$balls[1]."','".$balls[2]."','".$balls[3]."','".$balls[4]."','".$balls[5]."','".$_POST['data'][$x][0]."',".$boxed.",".$bet_amount.",".$pay_factor.",0,'".$draw_date['next_draw_date']."', '".$payout_scheme_id['payout_scheme_id']."')");
						$q = "INSERT INTO `lotto_bet` (`ticket_id`,`game_id`,`ball_1`,`ball_2`,`ball_3`,`ball_4`, `ball_5`, `ball_6`, `ball_string`, `is_boxed`, `bet_amount`, `pay_factor`, `is_processed`, `draw_date`, `payout_scheme_id`) VALUES (%i,%i,%s,%s,%s,%s,%s,%s,%i,%i,%f,%i,0,%s,%i);";
						$db->queryInsert($q, array($ticket_id, $_POST['data'][$x][1], $balls[0], $balls[1], $balls[2], $balls[3], $balls[4], $balls[5], $_POST['data'][$x][0], $boxed, $bet_amount, $pay_factor, $draw_date['next_draw_date'], $payout_scheme_id['payout_scheme_id']));

					}
				}  else {
                                     echo "You do not have sufficient balance to purchase tickets. Please contact Supervisor for more funds";
                             }
			}else{
				echo "You must add bets to purchase";
			}
                    }
		}
	}
