<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

require($_SERVER['DOCUMENT_ROOT'].'/lib/assets/GoogleAuthenticator/PHPGangsta/GoogleAuthenticator.php');

$ga = new PHPGangsta_GoogleAuthenticator();

$clock_tolerance = 2;

if($session->userinfo['panel_user']['shift_id'] == "-1"){
	echo json_encode(array("success" => "false", "errors" => "You must start a shift!"));
}else{
	if(isset($_POST['action']) && $_POST['action'] == "customer_withdrawal"){

		//$user = $db->query("SELECT * FROM `customers` c JOIN `users` u ON c.user_id=u.id WHERE customer_number=".$_POST['customer_number']);
		$q = "SELECT * FROM `customers` c LEFT JOIN `users` u ON c.user_id=u.id WHERE c.customer_number=%i";
		$user = $db->query($q, array($_POST['customer_number']));

		$errors = "";

		if($user == NULL){
			$errors .= "Customer number doesn't exist.</br>";
		}
		
		if($user[0]['is_disabled'] == 1){
			$errors .= "Customer is disabled.</br>";
		}

		if($user[0]['google_auth_enabled'] != "1"){
			if(!$core->check_pin($_POST['customer_number'], $_POST['customer_pin'])){
				$errors .= "Pin is incorrect.<br>";
			}
		}else{
			if(!$ga->verifyCode($user[0]['google_auth_secret'], $_POST['customer_pin'], $clock_tolerance)){
				$errors .= "2fa code is invalid.<br>";
			}
		}
		
		if($user[0]['is_verified'] == 0){
			$errors .= "Customer needs to be verified to withdrawal money.<br>";
		}

		$_POST['amount'] = str_replace("$","",$_POST['amount']);
		if($errors == ""){
			$valid = $core->cashier_withdrawal_validation(-$_POST['amount'], $user[0]['user_id']);
			if($valid === true){
				$trans_id = $core->make_panel_user_transaction(-$_POST['amount'], 10, "Withdrawal made to customer number ".$_POST['customer_number'], "NULL",$user[0]['user_id']);
				if($trans_id === false){
					$errors .= "Unexpected Error Occurred. Balances may be off, contact administration<br>";
				}
				$successful_transaction = $core->make_customer_transaction(-$_POST['amount'], 5, "Withdrawal made via cashier with the id of ".$session->userinfo['id'], "NULL", $user[0]["user_id"]);
				if($successful_transaction === false){
					$errors .= "Unexpected Error Occurred. Balances may be off, contact administration<br>";
				}
			}else{
				$errors .= $valid;
			}

			if($errors == ""){
				$results = array("success" => "true", "transaction_id" => $trans_id);
			}else{
				$results = array("success" => "false", "errors" => $errors);
			}
		}else{
			$results = array("success" => "false", "errors" => $errors);
		}

		echo json_encode($results);
	}

	if(isset($_POST['action']) && $_POST['action'] == "customer_balance"){

		//$user = $db->queryOneRow("SELECT *, i.name as island FROM `customers` c JOIN `users` u ON c.user_id=u.id JOIN `island` AS `i` ON `i`.`id` = `c`.`island_id` WHERE customer_number=".$_POST['customer_number']);
		$q = "SELECT *, i.name as island FROM `customers` c LEFT JOIN `users` u ON c.user_id=u.id LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id`=`u`.`id` LEFT JOIN `island` AS `i` ON `i`.`id` = `ui`.`island_id` WHERE customer_number=%i";
		$user = $db->queryOneRow($q, array($_POST['customer_number']));

		if($user == NULL){
			echo json_encode(array("success" => "false", "errors" => "Customer does not exist!<br>"));
			die();
		}
		
		if($user['is_disabled'] == 1){
			echo json_encode(array("success" => "false", "errors" => "Customer is disabled!<br>"));
			die();
		}

		if($user['google_auth_enabled'] == "1"){
			$pin_text = "2FA Code:";
		}else{
			$pin_text = "Customer Pin:";
		}
		
		// Calculate age from DOB
		if($user['date_of_birth'] != 0000-00-00){
		$dob_dt = new DateTime($user['date_of_birth']);
		$now_dt = new DateTime();
		$age_interval = $now_dt->diff($dob_dt);
		$dob = $dob_dt->format("m/d/Y");
		$age = $core->format_interval($age_interval);
		}
		else{ 
		$age ="";
		$dob ="";	}
		// customer data to display
		$customer_info['name'] = $user['firstname']." ".$user['lastname'];
		$customer_info['dob'] = $dob;
		$customer_info['age'] = $age;
		$customer_info['gender'] = $user['gender'];
		$customer_info['address'] = $user['address']." ".$user['address2'].", ".$user['city'];
		$customer_info['island'] = $user['island'];
		$customer_info['phone'] = $user['telephone'].",".$user['cellphone'];
		$customer_info['picture_path'] = $user['profile_picture_path'];
	
		$balance = $core->check_balance("customers", $user["user_id"]);
		$balance = $balance["available_balance"]-$balance["bonus_balance"];
		echo json_encode(array("success" => "true", "balance" => $balance, "pin_text" => $pin_text, "customer_info" => $customer_info));

	}
	
    	if(isset($_POST['action']) && $_POST['action'] == "petty_cash_withdrawal"){
    	
   		//echo json_encode(array("success"=>false));
   		$balance = $core->check_balance(NULL);
   		
		$i_balance = $balance["balance"];
   		if ($_POST['amount'] <= $i_balance) {
   			$trans_id = $core->make_panel_user_transaction(-$_POST['amount'], 25, $_POST['reason']);
                        echo json_encode(array("success" => "true","transaction_id"=>$trans_id, "message" => "Petty cash transaction is successfull."));
   		} else {
   			echo json_encode(array("success" => "false", "errors" => "Withdrawal amount should be less than or equals to your balance"));
   		}
           }

  

}
