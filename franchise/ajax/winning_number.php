<?php
         date_default_timezone_set('America/New_York');

	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php'); // for testing

        global $db;



			if(!empty($_REQUEST['date'])){
				$date_str = date("Y-m-d", strtotime($_REQUEST['date']));
				 $header_dt = date('F jS, Y', strtotime($_REQUEST['date']));	
				

			}else{
			
				$today_dt = new DateTime();
				$date_str = $today_dt->format("Y-m-d");
				$header_dt = $today_dt->format('F jS, Y');
			}


			$winning_numbers = $db->query("SELECT lg.number_of_balls, lg.name, lh.short_name as house_name, lwn.draw_numbers, lh.web_cutoff_time, lwn.is_override FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date='".$date_str."' ORDER BY lg.name ASC");

			if(count($winning_numbers) < 1){
				// no drawings for today, use yesterday instead
				$today_dt = new DateTime('yesterday');
				$date_str = $today_dt->format("Y-m-d");
				$winning_numbers = $db->query("SELECT lg.number_of_balls, lg.name, lh.short_name as house_name, lwn.draw_numbers, lh.web_cutoff_time, lwn.is_override FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date='".$date_str."' ORDER BY lg.name ASC LIMIT 40");
			}
			
			//Build the list
			//foreach($winning_numbers as $wn){
			//	$response .= "<tr>";
			//	$response .= "<td>".$wn['house_name']." - ".$wn['name']."</td>";
			//	
			//	if($wn['number_of_balls'] == 2){
			//		$response .= "<td style='text-align: right;'>".substr($wn['draw_numbers'],-2,2). "</td>";
			//	}else{
			//		$response .= "<td style='text-align: right;'>".$wn['draw_numbers']."</td>";
			//	}
			//	$response .= "</tr>";
			//}

                          $response = ''; 
                          $name = ''; 
                        if(!empty($winning_numbers)){
                             $response .= "<tr>";
                             $name = $winning_numbers[0]['house_name'];
                             $response .= "<td>".$winning_numbers[0]['house_name']."</td>";
                         }
			
			foreach($winning_numbers as $wn){
				
				if ($wn['house_name'] != $name){
                                        $response .= "</tr>";
					$response .= "<tr>";
					$response .= "<td>".$wn['house_name']."</td>";
				}
				
				if($wn['number_of_balls'] == 2){
					$response .= "<td style='text-align: center;'>-".substr($wn['draw_numbers'],-2,2). "-</td>";
				}else{
					$response .= "<td style='text-align: center;'>-".$wn['draw_numbers']."-</td>";
				}
				
				
				$name = $wn['house_name'];
			}
                         if(!empty($winning_numbers)){
                             $response .= "</tr>";                             
                         }
			
			$html = "
				<div id='winning_numbers_header' style='padding-top: 15px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' height='100px' width='200px;'>
					</div>
					
					<h2 style='text-align: center'>Winning Numbers</h2>
					<h3 style='text-align: center'>".$header_dt."</h3>
				</div>
				
				<div id='winning_numbers_body' style='padding-top: 5px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<tbody>
							".$response."
						</tbody>
					</table>
				</div>
			";
echo $html;
			?>
			
