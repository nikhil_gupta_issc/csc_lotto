<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

if($session->userinfo['panel_user']['shift_id'] == "-1"){
	echo json_encode(array("success" => "false", "errors" => "You must start a shift!"));
}else{
	if(isset($_POST['action']) && $_POST['action'] == "get_results"){

		//$ticket_winning_information = $db->queryOneRow("SELECT * FROM `winner_payout` WHERE `ticket_number`=".$_POST['ticket_number']);
		$q = "SELECT * FROM `winner_payout` WHERE `ticket_number`=%i;";
		$ticket_winning_information = $db->queryOneRow($q, array($_POST['ticket_number']));

		if($ticket_winning_information != null){
			$data = $core->get_winning_bets($_POST['ticket_number']);

			$table = "";
			if($data != null){
				for($x=0; $x<count($data); $x++){
					$table .= "<tr>";
					$table .= "<td>".$data[$x]['short_name']."</td>";
					$table .= "<td>".$data[$x]['ball_string']."</td>";
					$table .= "<td>$".$data[$x]['winning_amount']."</td>";
					$table .= "<td>".$data[$x]['win_date']."</td>";
					$table .= "</tr>";
				}
			}

			$return = array("success" => "true", "table" => $table , "total_won" => $ticket_winning_information["total_payout"]);
		}else{
			$return = array("success" => "true", "table" => "<tr><td colspan='4'>Ticket is not a winner.</td></tr>", "total_won" => "0.00");
		}

		echo json_encode($return);
	}
}
