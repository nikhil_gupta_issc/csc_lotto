<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	$response = array();
	$response['errors'] = "";

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$card_number = isset($_POST['card_number']) ? $_POST['card_number'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'get_amount':
			$q = "SELECT * FROM gift_cards WHERE `card_number`=%s AND `is_sold`=0";
			$card_info = $db->queryOneRow($q, array($card_number));
			
			if($card_number == null || $card_info == array()){
				$response['success'] = false;
				$response['errors'] .= "This gift card is not valid.<br>";
			}
			
			if($response['errors'] == ""){
				$response['success'] = true;
				$response['amount'] = "".number_format($card_info['amount']). " " . CURRENCY_FORMAT;
			}
			break;
		case 'purchase_gift_card':
			$q = "SELECT * FROM gift_cards WHERE `card_number`=%s AND `is_sold`=0";
			$card_info = $db->queryOneRow($q, array($card_number));
			
			if($card_number == null || $card_info == array()){
				$response['success'] = false;
				$response['errors'] .= "This gift card is not valid.<br>";
			}else{
				//Create a panel user transaction
				$id = $core->make_panel_user_transaction($card_info['amount'], 48, "Sold Gift Card - ".$card_number);
				
				//Change the gift card record
				$q = "UPDATE `gift_cards` SET `is_sold`=1, `sold_by`=%i, `sold_on`=%s, `transaction_id`=%i WHERE `card_number`=%s";
				$db->queryDirect($q, array($session->userinfo['id'],$now,$id,$card_number));
			}
			
			if($response['errors'] == ""){
				$response['success'] = true;
				$response['gift_card_id'] = $card_info['id'];
			}
			break;
		default:
			$response['success'] = false;
	}
	
	echo json_encode($response);
