<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

if($session->userinfo['panel_user']['shift_id'] == "-1"){
	echo json_encode(array("success" => "false", "errors" => "You must start a shift!"));
}else{
	if(isset($_POST['action']) && $_POST['action'] == "transfer_funds"){

		//Initialize the errors
		$errors = "";
		
		//Make sure the amount is numeric
		$amount = str_replace("$","",$_POST['amount']);
		
		if(!is_numeric($amount)){
			$errors = "The amount number must be numeric!<br>";
		}
		
		if($errors == ""){
			$transaction_id = $core->make_panel_user_transaction(-$amount, 31, "Transfer Out to Location Id - ".$_POST['location_id']);
			
			$date = new DateTime();
			$date_str = $date->format("Y-m-d H:i:s");
			
			$q = "INSERT INTO pending_transfers (`from_location_id`,`to_location_id`,`amount`,`from_transaction_id`,`is_accepted`,`created_on`,`created_by`) VALUES (%d,%d,%d,%d,%d,%s,%i)";
			$db->queryInsert($q, array($session->userinfo['panel_user']['location_id'], $_POST['location_id'], $amount, $transaction_id, 0, $date_str, $session->userinfo['id']));
			
			//Get the new balance
			$balances = $core->check_balance("cashier");

			//Get location name
			$q = "SELECT * FROM `panel_location` WHERE `id`=%i";
			$loc = $db->queryOneRow($q, array($session->userinfo['panel_user']['location_id']));
			
			//Send alerts
			$q = "SELECT * FROM `panel_user` WHERE `location_id`=%i";
			$alert_users = $db->query($q, array($_POST['location_id']));
			foreach($alert_users as $u){
				$core->send_alert("Pending Transfer",$loc["name"]." sent your location $".$amount,$u['user_id']);
			}
			
			$return = array("success" => "true", "balance" => $balances['balance']);
		}else{
			$return = array("success" => "false", "errors" => $amount);
		}
	}
	
	if(isset($_POST['action']) && $_POST['action'] == "accept_transfer"){
		//Get the pending transfer information
		$q = "SELECT * FROM `pending_transfers` WHERE `id`='".$_POST['id']."'";
		$pending_info = $db->queryOneRow($q);
		
		//Make the transaction to accept the money from the transfer
		$core->make_panel_user_transaction($pending_info['amount'], 30, "Tranfer completed from Location Id ".$pending_info['from_location_id']);
		
		//Get the new balance
		$balances = $core->check_balance("cashier");
		
		//Set the pending transfer as accepted so it can't be accepted twice
		$date = new DateTime();
		$date_str = $date->format("Y-m-d H:i:s");
		$q = "UPDATE `pending_transfers` SET `is_accepted`='1', `accepted_on`=%s, `accepted_by`=%i WHERE `id`='".$_POST['id']."'";
		$db->queryDirect($q, array($date_str,$session->userinfo['id']));
		
		$return = array("success" => "true", "balance" => $balances['balance']);
	}
	
	echo json_encode($return);
}
