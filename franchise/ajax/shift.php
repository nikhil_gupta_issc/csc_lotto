<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

if(isset($_POST['action']) && $_POST['action'] == "open_shift"){
	if($session->userinfo['panel_user']['shift_id'] == "-1"){
		if(isset($_POST['location']) && $_POST['location'] == ""){
			echo json_encode(array("success" => "false", "errors" => "You must enter a location!"));
			die();
		} else {
                        $location = $_POST['location'];     
                }

 /*else if(!isset($_POST['location']) || $_POST['location'] == 'undefined'){
                        $t_location =  $db->queryOneRow('SELECT location_id FROM panel_user WHERE user_id = '.$session->userinfo['id']);
                        $location = $t_location['location_id'];    
                } else {
                        $location = $_POST['location'];     
                }*/

                 /*if(!empty($location)){       
                   $t_location =  $db->queryOneRow('SELECT id,ip_address FROM panel_location WHERE id = '.$location.' AND if(ip_address = "",ip_address = "",ip_address="'.$_SERVER['REMOTE_ADDR'].'")');

                   if(empty($t_location['id'])){
                          $_SESSION['invalid_location'] = 1;
                          echo json_encode(array("success" => "false", "errors" => "1"));
                          die();
                   }
                 }*/

		$date = new datetime("now");
		$date_str = $date->format("Y-m-d H:i:s");

		$opening = str_replace(",","",$_POST['opening']);
		$float_amount = $opening;

                $i_virtual_money = 0; 
                $t_current_virtual_money = $db->query("SELECT virtual_money FROM panel_user_shift where user_id = ".$session->userinfo['id']." ORDER BY id DESC LIMIT 1");

                if(!empty($t_current_virtual_money) && !empty($t_current_virtual_money[0]['virtual_money']) && $t_current_virtual_money[0]['virtual_money'] != 0.00){
                      $i_virtual_money = $t_current_virtual_money[0]['virtual_money'];

                } else {
                       $t_virtual_money = $db->query("SELECT virtual_money_offset FROM panel_user WHERE user_id = ".$session->userinfo['id']);
		        if(!empty($t_virtual_money) && !empty($t_virtual_money[0]['virtual_money_offset']) && 
	$t_virtual_money[0]['virtual_money_offset'] !=0.00){
		              $i_virtual_money = $t_virtual_money[0]['virtual_money_offset'];
		        } else {                      
		              $i_virtual_money = 0;                     
		       }
                }
		
		$q = "INSERT INTO `panel_user_shift` (`user_id`,`location_id`,`shift_start`,`float_amount`,`opening`,`virtual_money`) VALUES (%i,%i,%s,%f,%f,%f);";
		$shift_id = $db->queryInsert($q, array($session->userinfo['id'], $location, $date_str, $float_amount, $opening,$i_virtual_money));
              
                if(!empty($i_virtual_money)){
                    $query = "INSERT INTO `panel_user_virtual_money` (`user_id`,`panel_user_shift_id`,`virtual_amount`,`transaction_date`,`status`) VALUES (%i,%i,%f,%s,%i);";
		    $db->queryInsert($query, array($session->userinfo['id'], $shift_id, $i_virtual_money, $date_str, 1));
                }

		$q = "UPDATE `panel_user` SET `shift_id`=%i, `location_id`=%i, `balance`=%f WHERE user_id=%i;";
		$db->query($q, array($shift_id, $location, $opening, $session->userinfo['id']));
		
		$_SESSION['new_shift'] = 1;
		
		echo json_encode(array("success" => "true"));
		die();
	}else{
		echo json_encode(array("success" => "false", "errors" => "You already have a shift started. Please close your current shift to start a new one!"));
		die();
	}
}

if($session->userinfo['panel_user']['shift_id'] == "-1"){
	echo json_encode(array("success" => "false", "errors" => "You must start a shift!"));
	die();
}else{
	if(isset($_POST['action']) && $_POST['action'] == "close_shift"){
		if($_POST['bag_number'] != ""){

			//$shift_info = $db->queryOneRow("SELECT * FROM `panel_user_shift` WHERE id=".$session->userinfo['panel_user']['shift_id']);
			$q = "SELECT * FROM `panel_user_shift` WHERE id=%i";
			$shift_info = $db->queryOneRow($q, array($session->userinfo['panel_user']['shift_id'])); 

			$shift_id = $session->userinfo['panel_user']['shift_id'];
			$closing = str_replace(",","",$_POST['closing']);
			$expected = str_replace(",","",$_POST['expected']);
			$over_short = $closing-$expected;
			$sales = str_replace(",","",$_POST['sales']);
			$payouts = str_replace(",","",$_POST['payouts']);
			$date = new datetime("now");
			$date_str = $date->format("Y-m-d H:i:s");

			$closing_1s = $_POST['closing_1s'] == "" ? "0" : $_POST['closing_1s'];
			$closing_5s = $_POST['closing_5s'] == "" ? "0" : $_POST['closing_5s'];
			$closing_10s = $_POST['closing_10s'] == "" ? "0" : $_POST['closing_10s'];
			$closing_20s = $_POST['closing_20s'] == "" ? "0" : $_POST['closing_20s'];
			$closing_50s = $_POST['closing_50s'] == "" ? "0" : $_POST['closing_50s'];
			$closing_100s = $_POST['closing_100s'] == "" ? "0" : $_POST['closing_100s'];
			$closing_coins = $_POST['closing_coins'] == "" ? "0" : str_replace("$","",$_POST['closing_coins']);
			
			//$db->query("UPDATE `panel_user_shift` SET `shift_end` = '".$date_str."', `bag_number` = '".$_POST["bag_number"]."', `sales` = ".$sales.", `payouts` = ".$payouts.", `closing` = ".$closing.", `expected` = ".$expected.", `over_short` = ".$over_short." WHERE id=".$session->userinfo['panel_user']['shift_id']);
			$q = "UPDATE `panel_user_shift` SET `shift_end` = %s, `bag_number` = %i, `sales` = %f, `payouts` = %f, `closing` = %f, `expected` = %f, `over_short` = %f, `closing_1s` = %i, `closing_5s` = %i, `closing_10s` = %i, `closing_20s` = %i, `closing_50s` = %i, `closing_100s` = %i, `closing_coins` = %d WHERE id=%i;";
			$db->query($q, array($date_str, $_POST["bag_number"], $sales, $payouts, $closing, $expected, $over_short,$closing_1s,$closing_5s,$closing_10s,$closing_20s,$closing_50s,$closing_100s,$closing_coins, $session->userinfo['panel_user']['shift_id']));

			//$db->query("UPDATE `panel_user` SET `shift_id`=-1 WHERE user_id=".$session->userinfo['id']);
			$q = "UPDATE `panel_user` SET `shift_id`=-1 WHERE user_id=%i;";
			$db->query($q, array($session->userinfo['id']));

			echo json_encode(array("success" => "true", "shift_id" => $shift_id));
			die();
		}else{
			echo json_encode(array("success" => "false", "errors" => "You must enter a bag number"));
			die();
		}
	}

	if(isset($_POST['action']) && $_POST['action'] == "shift_info"){

		//$transactions = $db->query("SELECT SUM(amount) FROM `panel_user_transaction` WHERE shift_id=".$session->userinfo['panel_user']['shift_id']." GROUP BY type_id");
		$q = "SELECT SUM(amount),type_id FROM `panel_user_transaction` WHERE shift_id=%i GROUP BY type_id";
		$transactions = $db->query($q, array($session->userinfo['panel_user']['shift_id']));

		$payouts = 0;
		$sales = 0;
		$pettycash = 0;
		foreach($transactions as $trans){
			if($trans['SUM(amount)'] < 0){
				if($trans['type_id'] == 25){
					$pettycash += $trans['SUM(amount)'];
				}
				else{
				$payouts += $trans['SUM(amount)'];
				}
			}else{
				$sales += $trans['SUM(amount)'];
			}
		}
		/*foreach($transactions as $trans){
			if($trans['type_id'] == 25){
				$pettycash += $trans['SUM(amount)'];
			}
		}*/
		//print_r($pettycash);

		//$shift_info = $db->queryOneRow("SELECT * FROM `panel_user_shift` WHERE id=".$session->userinfo['panel_user']['shift_id']);
		$q = "SELECT * FROM `panel_user_shift` WHERE id=%i";
		$shift_info = $db->queryOneRow($q, array($session->userinfo['panel_user']['shift_id']));

		$expected = $shift_info["opening"]+$sales+$payouts+$pettycash;

                $query = "SELECT SUM(virtual_amount) as total_vm FROM panel_user_virtual_money where user_id = %i AND panel_user_shift_id = %i";
		$t_virtual_money = $db->queryOneRow($query, array($session->userinfo['panel_user']['user_id'],$session->userinfo['panel_user']['shift_id']));
                $t_virtual_balance = $core->get_virtual_balance();
                $i_total_vm = (!empty($t_virtual_money['total_vm']) ? $t_virtual_money['total_vm'] : '0.00');
                $i_closing_vm = (!empty($t_virtual_balance['virtual_money']) ? $t_virtual_balance['virtual_money'] : '0.00');

		echo json_encode(array("success" => "true","over_short" => number_format(-$expected),"expected" => number_format($expected),"float" => number_format($shift_info["float_amount"]),"opening" => number_format($shift_info["opening"]),"sales" => number_format($sales), "payouts" => number_format($payouts), "pettycash" =>number_format($pettycash),"total_vm"=>$i_total_vm,"closing_vm"=>$i_closing_vm));
		die();
	}
}
