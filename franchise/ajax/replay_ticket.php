<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

if($session->userinfo['panel_user']['shift_id'] == "-1"){
	echo json_encode(array("success" => "false", "errors" => "You must start a shift!"));
}else{
	if(isset($_POST['action']) && $_POST['action'] == "validate"){
		$errors = "";
		if(count($db->query("SELECT * FROM `lotto_ticket` WHERE ticket_number=".$_POST['ticket_number'])) == 0){
			$errors .= "Ticket Number Does Not Exist";
		}

		if($errors == ""){
			$ticket_number = $core->generate_ticket_number();
			$date = new datetime('now');
			$date_str = $date->format("Y-m-d H:i:s");

			//$ticket_info = $db->query("SELECT * FROM `lotto_ticket` t JOIN `lotto_bet` b ON t.ticket_id=b.ticket_id WHERE ticket_number=".$_POST['ticket_number']);
			$q = "SELECT * FROM `lotto_ticket` t JOIN `lotto_bet` b ON t.ticket_id=b.ticket_id WHERE ticket_number=%i;";			
			$ticket_info = $db->query($q, array($_POST['ticket_number']));

			$expiration_date = $core->get_expiration_date();

			//Insert Ticket

			//$ticket_id = $db->queryInsert("INSERT INTO `lotto_ticket` (ticket_number, purchase_date, number_of_bets, cashier_id, location_id, shift_id, user_id, commission_rate, expiration_date) VALUES (".$ticket_number.", '".$date_str."', ".$ticket_info[0]["number_of_bets"].", ".$session->userinfo["id"].", ".$session->userinfo["panel_user"]["location_id"].", ".$session->userinfo["panel_user"]["shift_id"].", -9999, ".$session->userinfo["panel_user"]["commission_rate"].", '".$expiration_date."')");
			$q = "INSERT INTO `lotto_ticket` (ticket_number, purchase_date, number_of_bets, cashier_id, location_id, shift_id, user_id, commission_rate, expiration_date) VALUES (%i, %s, %i, %i, %i, %i, -9999, %s, %s);";
			$ticket_id = $db->queryInsert($q, array($ticket_number, $date_str, $ticket_info[0]["number_of_bets"], $session->userinfo["id"], $session->userinfo["panel_user"]["location_id"], $session->userinfo["panel_user"]["shift_id"], $session->userinfo["panel_user"]["commission_rate"], $expiration_date));

			if($ticket_id != null){
				//Insert Bets
				foreach($ticket_info as $bet){
					if($bet["ball_3"]==null){
						$bet["ball_3"] = "NULL";
					}
					if($bet["ball_4"]==null){
						$bet["ball_4"] = "NULL";
					}
					if($bet["ball_5"]==null){
						$bet["ball_5"] = "NULL";
					}
					if($bet["ball_6"]==null){
						$bet["ball_6"] = "NULL";
					}
					
					//Get the payrate scheme id

					//$q = "SELECT payout_scheme_id FROM `panel_location` WHERE id=".$session->userinfo['panel_user']['location_id'];
					$q = "SELECT payout_scheme_id FROM `panel_location` WHERE id=%i;";
					//$payout_scheme_id = $db->queryOneRow($q);
					$payout_scheme_id = $db->queryOneRow($q, array($session->userinfo['panel_user']['location_id']));
					
					//$draw_date = $db->queryOneRow("SELECT next_draw_date FROM `lotto_game` g JOIN `lotto_game_xml_link` link ON g.id=link.game_id JOIN `xml_lotto_winning_numbers` xmlw ON link.xml_game_id=xmlw.game_id WHERE link.game_id=".$bet["game_id"]." ORDER BY xmlw.next_draw_date DESC LIMIT 1");
					$q = "SELECT next_draw_date FROM `lotto_game` g JOIN `lotto_game_xml_link` link ON g.id=link.game_id JOIN `xml_lotto_winning_numbers` xmlw ON link.xml_game_id=xmlw.game_id WHERE link.game_id=%i ORDER BY xmlw.next_draw_date DESC LIMIT 1;";
					$draw_date = $db->queryOneRow($q, array($bet["game_id"])); 

					//$db->query("INSERT INTO `lotto_bet` (ticket_id, game_id, ball_1, ball_2, ball_3, ball_4, ball_5, ball_6, ball_string, is_boxed, bet_amount, pay_factor, draw_date, payout_scheme_id) VALUES (".$ticket_id.", ".$bet["game_id"].", ".$bet["ball_1"].", ".$bet["ball_2"].", ".$bet["ball_3"].", ".$bet["ball_4"].", ".$bet["ball_5"].", ".$bet["ball_6"].", ".$bet["ball_string"].", ".$bet["is_boxed"].", ".$bet["bet_amount"].", ".$bet["pay_factor"].", '".$draw_date["next_draw_date"]."', ".$payout_scheme_id["payout_scheme_id"].")");
					$q = "INSERT INTO `lotto_bet` (ticket_id, game_id, ball_1, ball_2, ball_3, ball_4, ball_5, ball_6, ball_string, is_boxed, bet_amount, pay_factor, draw_date, payout_scheme_id) VALUES (%i, %i, %s, %s, %s, %s, %s, %s, %s, %i, %f, %i, %s, %i);";				
					$db->query($q, array($ticket_id, $bet["game_id"], $bet["ball_1"], $bet["ball_2"], $bet["ball_3"], $bet["ball_4"], $bet["ball_5"], $bet["ball_6"], $bet["ball_string"], $bet["is_boxed"], $bet["bet_amount"], $bet["pay_factor"], $draw_date["next_draw_date"], $payout_scheme_id["payout_scheme_id"]));

				}
				echo "SuccessfullyProcessed|".$ticket_id."|".sha1(md5($ticket_id."YOUCANTGUESSME"))."|$".$ticket_info[0]["total"];
			}else{
				echo "An unexpected error occurred<br>";
				die();
			}
		}else{
			echo $errors;
		}
	}
}
