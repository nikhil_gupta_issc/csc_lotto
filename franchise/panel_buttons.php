<?php include_once($_SERVER['DOCUMENT_ROOT'].'/config.php'); ?>

$(document).ready(function(){
        var petty_cash = 0;
	<?php
		if($_SESSION['new_shift'] == 1){
			?>
				window.open("/franchise/receipts/get_receipt.php?open_shift=true&shift_id=<?php echo $session->userinfo['panel_user']['shift_id']; ?>","receipt");
				//$("#receipt_modal").modal("show");
			<?php
			$_SESSION['new_shift'] = 0;
		}
                if(!empty($session->userinfo['panel_user']['petty_cash_rights'])){ ?>
                       petty_cash = 1;
            <?php    }
	?>
	
	$("#news_ticker").slideDown("fast");

	//Get cashier balance
	function get_balance(){
		$.ajax({
			url: "ajax/lotto_betting.php",
			data: {action: "get_balance"},
			method: "POST"
		}).done(function( data ) {
			var info = JSON.parse(data);

			$("#cashier_balance").text(Number(info.balance, 10).formatMoney(2));
		});
	}
	
	//Button Handlers to open modals
	$(document).on("click", "#shift_sub_menu_button", function(){
		$(".sub_menu_button").hide();
		$("#sub_menu_modal").modal("show");
		$("#close_shift_button").show();
		$("#sales_report_button").show();
	});
	$(document).on("click", "#deposit_sub_menu_button", function(){
		$(".sub_menu_button").hide();
		$("#sub_menu_modal").modal("show");
		$("#deposit_button").show();
		$("#franchise_deposit_button").show();
		$("#deposit_customer_info").hide();
	});
	$(document).on("click", "#voids_sub_menu_button", function(){
		$(".sub_menu_button").hide();
		$("#sub_menu_modal").modal("show");
		$("#voids_button").show();
		$("#adjust_void_button").show();
	});
	$(document).on("click", "#kyc_button", function(){
		$("#customer_modal").modal("show");
	});
	$(document).on("click", "#transfers_sub_menu_button", function(){
		$(".sub_menu_button").hide();
		$("#sub_menu_modal").modal("show");
		$("#transfer_out_button").show();
		$("#accept_transfer_button").show();
	});
	$(document).on("click", "#tickets_sub_menu_button", function(){
		$(".sub_menu_button").hide();
		$("#sub_menu_modal").modal("show");
		$("#payouts_button").show();
		$("#replay_ticket_button").show();
		$("#event_ticket_button").show();
	});
	$(document).on("click", "#cards_sub_menu_button", function(){
		$(".sub_menu_button").hide();
		$("#sub_menu_modal").modal("show");
		$("#customer_card_button").show();
		$("#gift_card_button").show();
	});
	$(document).on("click", "#scan_sub_menu_button", function(){
		$(".sub_menu_button").hide();
		$("#sub_menu_modal").modal("show");
		$("#results_button").show();
		$("#replay_ticket_button").show();
	});
	$(document).on("click", "#replay_ticket_button", function(){
		$("#sub_menu_modal").modal("hide");
		$("#replay_ticket_modal").modal("show");
		$("#replay_ticket_error").hide();
		$("#replay_ticket_number").val("");
	});
	$(document).on("click", "#payouts_button", function(){
		$("#sub_menu_modal").modal("hide");
		$("#payouts_number_of_wins").text("0");
		$("#payouts_total_won").text("0.00");
		$("#payouts_max_payout").text("0.00");
		$("#partial_payout_total").text("0.00");
		$("#void_status").text("");
		$("#voided_comment").text("");
		$("#payout_details").hide();
		$("#payout_ticket_number").val("");
		$("#payout_ticket_number").prop("disabled", false);
		$("#payouts_confirm").prop("disabled", false);
		$("#payouts_table_body").html("");
		$("#payouts_modal").modal("show");
		$("#payouts_error").hide();
	});
	$(document).on("click", "#results_button", function(){
		$("#sub_menu_modal").modal("hide");
		$("#results_modal").modal("show");
		$("#results_error").hide();
	});
	$(document).on("click", "#accept_transfer_button", function(){
		$("#sub_menu_modal").modal("hide");
		$("#accept_transfer_modal").modal("show");
	});
	$(document).on("click", "#winning_numbers_button", function(){
	
               	 var today = new Date();
		    var dd = today.getDate();
		    var mm = today.getMonth()+1; //January is 0!

		    var yyyy = today.getFullYear();
		    if(dd < 10){
			dd="0"+dd
		    } 
		    if(mm<10){
			mm="0"+mm
		    } 
		    var today = mm+"/"+dd+"/"+yyyy;
    
                $('#datetimepicker1').val(today);
		$("#winning_number_date").modal("show");
		$("#win_error").hide();
		//window.open("/franchise/receipts/get_receipt.php?winning_numbers=true","receipt");
		$("div.toshow").show();
	});
	
	$(document).on("blur", "#datetimepicker1", function(){
	var d = $.trim($("#datetimepicker1").val()).length;

	if(d != 0){
	$("#win_error").hide();
	$("#win_error").html("");
	
	}
	});
	
	
	$(document).on("click", "#get_win_num", function(){

	var win_date=$("#datetimepicker1").val();
	var d = $.trim($("#datetimepicker1").val()).length;
	
	if(d == 0){
	
	$("#win_error").html("You must Select a Date");
			$("#win_error").slideDown("fast");
	} else {

            $.ajax({
			url: "/franchise/ajax/winning_number.php",
			data: {date: $("#datetimepicker1").val()},
			method: "POST"
		}).done(function( data ) {
		$('#winning_num_date').val(win_date);
		$('#winning_num_body').html(data);
		$('#winning_num_preview_modal').modal('show');
			
		});
	
		//window.open("/franchise/receipts/get_receipt.php?winning_numbers=true&date="+$("#datetimepicker1").val(),"receipt");
		$("div.toshow").show();
		$("#winning_number_date").modal("hide");
		$("#datetimepicker1").val("");
		$("#win_error").html("");
		}
	});
	$(document).on('click','#print_winning_num',function(){
		$('#winning_num_preview_modal').modal('hide');
              window.open("/franchise/receipts/get_receipt.php?winning_numbers=true&date="+$('#winning_num_date').val(),"receipt");
       
        });
	
	$(document).on("change", "#date_winning_numbers", function(){
		window.open("/franchise/receipts/get_receipt.php?winning_numbers=true&date="+$("#date_winning_numbers").val()+"","receipt");
		//$("#receipt_modal").modal("show");
	});
	$(document).on("click", "#sales_report_button", function(){
		window.open("/franchise/receipts/get_receipt.php?location_id=<?php echo $session->userinfo['panel_user']['location_id'] ?>&from="+$("#from_date_location_sales").val()+"&to="+$("#to_date_location_sales").val()+"","receipt");
		//$("#receipt_modal").modal("show");
		$("div.toshow_location_report").show();
	});
	$(document).on("change", "#to_date_location_sales", function(){
		window.open("/franchise/receipts/get_receipt.php?location_id=<?php echo $session->userinfo['panel_user']['location_id'] ?>&from="+$("#from_date_location_sales").val()+"&to="+$("#to_date_location_sales").val()+"","receipt");
		//$("#receipt_modal").modal("show");
	});
	$(document).on("change", "#from_date_location_sales", function(){
		window.open("/franchise/receipts/get_receipt.php?location_id=<?php echo $session->userinfo['panel_user']['location_id'] ?>&from="+$("#from_date_location_sales").val()+"&to="+$("#to_date_location_sales").val()+"","receipt");
		//$("#receipt_modal").modal("show");
	});
	$(document).on("click", "#voids_button", function(){
		$("#sub_menu_modal").modal("hide");
		$("#voids_ticket_number").val("");
		$("#voids_ticket_reason").val("");
		$("#voids_modal").modal("show");
		$("#voids_error").hide();
	});
	$(document).on("click", "#transfer_out_button", function(){
		$("#sub_menu_modal").modal("hide");
		$("#transfer_out_modal").modal("show");
	});
	$(document).on("click", "#deposit_button", function(){
		$("#sub_menu_modal").modal("hide");
		$("#deposit_customer_number").val("");
		$("#deposit_customer_pin").val("");
		$("#deposit_customer_amount").val("");
		$("#deposit_modal").modal("show");
		$("#deposit_error").hide();
	});
	$(document).on("click", "#withdrawal_button", function(){
              if(petty_cash == 0){
		  open_withdrawal_modal();
              } else {
                  $(".sub_menu_button").hide();
                  $("#customer_withdrawal_button").show();
		  $("#petty_cash_withdrawal_button").show();
		  $("#sub_menu_modal").modal("show");
		  $("#withdraw_user_pic").hide();
              }
	});
        $(document).on('click','#customer_withdrawal_button',function(){
                  $("#sub_menu_modal").modal("hide");
                  open_withdrawal_modal();
        });
        $(document).on('click','#petty_cash_withdrawal_button',function(){
                  $("#sub_menu_modal").modal("hide");
		  $("#withdrawal_reason").val("");
		  $("#petty_cash_amount").val("");
		  $("#petty_cash_modal").modal("show");
		  $("#petty_cash_error").hide();                  
        });
        function open_withdrawal_modal(){
                $("#withdrawal_customer_number").val("");
		$("#withdrawal_customer_pin").val("");
		
		$("#withdrawal_customer_available").text("0.00");
		$("#withdrawal_modal").modal("show");
		$("#withdrawal_error").hide();
		$("#withdraw_customer_info").hide();
       }
	$(document).on("click", "#open_shift_button", function(){
		$("#open_shift_modal").modal("show");
		$("#shift_open_error").hide();
	});
	$(document).on("click", "#close_shift_button", function(){
		$("#sub_menu_modal").modal("hide");
		//ajax for the shift information
		$.ajax({
			url: "ajax/shift.php",
			data: {action: "shift_info"},
			method: "POST"
		}).done(function( data ) {
			var info = JSON.parse(data);

			if(info.success == "true"){
				$("#close_shift_sales").text(info.sales);
				$("#close_shift_payouts").text(info.payouts);
				$("#close_shift_pettycash").text(info.pettycash);
				$("#close_shift_opening").text(info.opening);
				$("#close_shift_float").text(info.float);
				$("#close_shift_expected").text(info.expected);
				$("#close_shift_over_short").text(info.over_short);
                                $("#total_virtual_money").text(info.total_vm);
                                $("#closing_virtual_money").text(info.closing_vm);
			}
		});
		$("#close_shift_modal").modal("show");
		$("#shift_close_error").hide();
	});
	$('#close_shift_modal').on('hidden.bs.modal', function () {
		// reload the page so that the logout takes effect
		location.reload();
	})
	$(document).on("click", "#phone_card_button", function(){
		$("#phone_card_carrier").val("");
		$("#phone_card_products").val("");
		$("#phone_card_modal").modal("show");
		$("#phone_card_error").hide();
	});
	$(document).on("click", "#gift_card_button", function(){
		$("#sub_menu_modal").modal("hide");
		$("#gift_card_number").val("");
		$("#gift_card_amount").text("$0.00");
		$("#gift_cards_modal").modal("show");
	});
	$(document).on("click", "#franchise_deposit_button", function(){
		$("#sub_menu_modal").modal("hide");
		$("#franchise_deposit_modal").modal("show");
		$("#franchise_deposit_error").hide();
	});
	
	//Load products on carrier change
	$(document).on("change", "#phone_card_carrier", function(){
		$.ajax({
			url: "ajax/phone_cards.php",
			data: {action: "get_products", carrier_id: $(this).val()},
			method: "POST",
			dataType: "json"
		}).done(function( data ) {
			if(data.success == true){
				$('#phone_card_products').html(data.options);
			}else{
				
			}
		});
	});
	
	//Purchase a phone card
	$(document).on("click", "#phone_card_confirm", function(){
		$(this).prop("disabled", true);
		$.ajax({
			url: "ajax/phone_cards.php",
			data: {action: "purchase_product", carrier_id: $("#phone_card_carrier").val(), product_id: $("#phone_card_products").val(), category_id: $("#phone_card_products").data("category_id")},
			method: "POST",
			dataType: "json"
		}).done(function( data ) {
			if(data.success == true){
				$("#phone_card_confirm").prop("disabled", false);
				$("#phone_card_modal").modal("hide");
				window.open("/franchise/receipts/get_receipt.php?phone_card_id="+data.phone_card_id,"receipt");
				//$("#receipt_modal").modal("show");
			}else{
				
			}
		});
	});
	
	//Populate gift card amount
	$(document).on("change", "#gift_card_number", function(){
		$.ajax({
			url: "ajax/gift_cards.php",
			data: {action: "get_amount", card_number: $(this).val()},
			method: "POST",
			dataType: "json"
		}).done(function( data ) {
			if(data.success == true){
				$("#gift_card_amount").text(data.amount);
				$("#gift_cards_error").html("");
				$("#gift_cards_error").hide();
			}else{
				$("#gift_card_amount").text("$0.00");
				$("#gift_cards_error").html(data.errors);
				$("#gift_cards_error").slideDown("fast");
			}
		});
	});
	
	//Purchase the gift card
	$(document).on("click", "#gift_cards_confirm", function(){
		$(this).prop("disabled", true);
		$.ajax({
			url: "ajax/gift_cards.php",
			data: {action: "purchase_gift_card", card_number: $("#gift_card_number").val()},
			method: "POST",
			dataType: "json"
		}).done(function( data ) {
			if(data.success == true){
				$("#gift_cards_confirm").prop("disabled", false);
				$("#gift_cards_modal").modal("hide");
				window.open("/franchise/receipts/get_receipt.php?gift_card_id="+data.gift_card_id,"receipt");
				//$("#receipt_modal").modal("show");
			}else{
				$("#gift_card_amount").text("$0.00");
				$("#gift_cards_error").html(data.errors);
				$("#gift_cards_error").slideDown("fast");
			}
		});
	});
	
        $(document).on('click','#customer_card_button',function(){
$("#sub_menu_modal").modal("hide");

             $.ajax({
			url: "ajax/datatables/manage_customers.php",
			data: {action: "purchase_gift_card", card_number: $("#gift_card_number").val()},
			method: "POST",
			dataType: "json"
		}).done(function( data ) {
                     console.log(data['data']);
                     var html = '<option value="">SELECT</option>';
                     for(var i = 0; i < data['data'].length; i++){
                        if(data['data'][i].user_id != null){
                         html += '<option value="'+data['data'][i].user_id+'" >'+data['data'][i].customer_name+'('+data['data'][i].customer_number+')</option>';
}
                     }
$('#customer_select').html(html);
$('.selectpicker').selectpicker('refresh');
		});
             $('#card_keypad_value').val();
             $('#card_keypad_modal').modal('show');
        });

        $('#keypad_modal').on('show.bs.modal', function (e) {
		setTimeout(function(){
			// focus on first input element
			$('#keypad_value').focus();
		}, 1000);
	});
		
	$(document).on('click','#card_keypad_confirm',function(){

              $(this).prop("disabled", true);
		$.ajax({
			url: "ajax/customer_card.php",
			data: {action: "add_cardno",user_id: $('#cid').val() ,card_number: $("#keypad_value").val()},
			method: "POST",
			dataType: "json"
		}).done(function( data ) {
			$('#card_keypad_modal').modal('hide');
		});
        });
        
        
        
        
        //Get customer  when making a customer card
	$(document).on("blur", "#customercard_number", function(){
		//ajax for the balance information
		if($("#customercard_number").val() != ""){
			$.ajax({
				url: "ajax/customer_card.php",
				data: {action: "customer_balance", customer_number: $("#customercard_number").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);
				
				if(info.success == "true"){
					
					$("#cid").val(info.customer_info['id']);
					$("#customercard_name").html(info.customer_info['name']);
					$("#customercard_dob").html(info.customer_info['dob']);
					$("#customercard_age").html(info.customer_info['age']);
					$("#customercard_gender").html(info.customer_info['gender']);
					$("#customercard_address").html(info.customer_info['address']);
					$("#customercard_island").html(info.customer_info['island']);
					$("#customercard_phone").html(info.customer_info['phone']);
					$("#pin_text").html(info.pin_text);
					$("#customercard_error").slideUp("fast");
					$("#customercard_info").slideDown("fast");
				}else{
					$("#customercard_error").html(info.errors);
					$("#customercard_error").slideDown("fast");
				}
			});
		}else{
			$("#customercard_error").html("You must enter a customer number!<br>");
			$("#customercard_error").slideDown("fast");
		}
	});
	
        
        
        
        
        
        
        
        
        
        
        
        
        
	
	// allow enter key to trigger confirm button
	$('#keypad_modal').keypress(function(e){
		if (e.which == '13'){
			$('#keypad_confirm').trigger("click");
		}
	});

	//Replay Ticket
	$(document).on("click", "#replay_ticket_confirm", function(){
		$(this).prop("disabled", true);
		$("#replay_ticket_error").hide();
		$("#replay_ticket_error").html("");
		if($("#replay_ticket_number").val().length != 15){
			$("#replay_ticket_error").append("Invalid Ticket Number<br>");
			$("#replay_ticket_error").slideDown("fast");
		}
		if($("#replay_ticket_number").val().match(/^[0-9]+$/) == null){
			$("#replay_ticket_error").append("Tickets are numeric only<br>");
			$("#replay_ticket_error").slideDown("fast");
		}

		//ajax for the ticket information
		if($("#replay_ticket_error").html() == ""){
			$.ajax({
				url: "ajax/replay_ticket.php",
				data: {action: "validate", ticket_number: $("#replay_ticket_number").val()},
				method: "POST"
			}).done(function( data ) {
				if(data.indexOf("SuccessfullyProcessed")==-1){
					$('#replay_ticket_error').html(data).slideDown("fast");
				}else{
					$("#replay_ticket_error").hide();
					var id = data.split("|");
					$("#ticket_id").val(id[1]);
					$("#tender_checksum").val(id[2]);
					$("#tender_total").text(id[3]);
					$("#replay_ticket_modal").modal("hide");
					$("#tender_modal").modal("show");
				}
			});
		}
	});

	//Accept A Transfer
	$(document).on("click", "#accept_transfer_confirm", function(){
		
		$.ajax({
			url: "ajax/transfers.php",
			data: {action: "accept_transfer", id: pending_transfers_data_table.$('tr.selected').attr('id').replace("row_", "")},
			method: "POST"
		}).done(function( data ) {
			var info = JSON.parse(data);

			if(info.success == "true"){
				$("#cashier_balance").text(info.balance);
				pending_transfers_data_table.draw(true);
				bootbox.alert("You have accepted the transfer!");
			}
		});
	});

	//Get Payouts
	$(document).on("click", "#payouts_confirm", function(){
		$(this).prop("disabled", true);
		$("#payouts_error").hide();
		$("#payouts_error").html("");
		if($("#payout_ticket_number").val().length != 15){
			$("#payouts_error").append("Invalid Ticket Number<br>");
			$("#payouts_error").slideDown("fast");
		}
		if($("#payout_ticket_number").val().match(/^[0-9]+$/) == null){
			$("#payouts_error").append("Tickets are numeric only<br>");
			$("#payouts_error").slideDown("fast");
		}

		//ajax for the ticket information
		if($("#payouts_error").html() == ""){
			$("#payout_ticket_number").prop("disabled", true);
			$.ajax({
				url: "ajax/payouts.php",
				data: {action: "get_partial_payouts", ticket_number: $("#payout_ticket_number").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);

				if(info.success == "true"){
					$("#payouts_table_body").html(info.table);
					$("#payouts_number_of_wins").html(info.number_of_wins);
					$("#payouts_total_won").html(info.total_won);
					$("#payouts_max_payout").html(info.max_payout);
					$("#void_status").html(info.void_status);
					if(info.voided_comment != ""){
						$("#voided_comment_contain").slideDown('fast');
					}else{
						$("#voided_comment_contain").slideUp('fast');
					}
					if(info.notices != ""){
						$("#payout_alerts_contain").slideDown('fast');
					}else{
						$("#payout_alerts_contain").slideUp('fast');
					}
					$("#payout_alerts_contain").html(info.notices);
					$("#voided_comment").html(info.voided_comment);
					$("#partial_payout_total").text(info.total_paid);
					$("#payout_details").slideDown('fast');
				}else{
					$(this).prop("disabled", false);
				}
			});
		}else{
			$(this).prop("disabled", false);
		}
	});

	//Error check the values then do ajax.
	$(document).on("click", "#payouts_add_payout", function(){
		$(this).prop("disabled", true);
		$("#payouts_error").hide();
		$("#payouts_error").html("");

		var payout_amount = $("#payout_amount").val().replace("$","").replace(",","");
		if(parseFloat(payout_amount) > parseFloat($("#payouts_max_payout").html().replace(",",""))){
			$("#payouts_error").append("Your Payout Amount Exeeds Max Payout<br>");
			$("#payouts_error").slideDown("fast");
		}

		//ajax for the ticket information
		if($("#payouts_error").html() == ""){
			$("#payout_ticket_number").prop("disabled", true);
			$("#payout_amount").prop("disabled", true);
			$.ajax({
				url: "ajax/payouts.php",
				data: {action: "add_payout", ticket_number: $("#payout_ticket_number").val(), amount: $("#payout_amount").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);

				if(info.success == "true"){
					window.open("/franchise/receipts/get_receipt.php?transaction_id="+info.transaction_id,"receipt");
					//$("#receipt_modal").modal("show");
					get_balance();
					$("#payouts_table_body").append(info.table);
					$("#payouts_max_payout").text(info.max_payout);
					$("#partial_payout_total").text(info.total_paid);
					$("#payout_amount").val("");
				}else{
					$("#payouts_error").html(info.errors);
					$("#payouts_error").slideDown("fast");
				}
			});
		}
		$(this).prop("disabled", false);
		$("#payout_amount").prop("disabled", false);
	});

	//Submit the transfer and add it to the pending transfers table
	$(document).on("click", "#transfer_out_confirm", function(){
		$(this).prop("disabled", true);
		$("#transfer_out_error").hide();
		$("#transfer_out_error").html("");

		//ajax for the ticket information
		if($("#transfer_out_error").html() == ""){
			$.ajax({
				url: "ajax/transfers.php",
				data: {
					action: "transfer_funds", 
					location_id: $("#transfer_to_location").val(),
					amount: $("#transfer_amount").val()
				},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);

				if(info.success == "true"){
					$("#transfer_out_modal").modal("hide");
					$("#cashier_balance").text(info.balance);
					bootbox.alert("Transfer Completed");
				}else{
					$("#transfer_out_error").show();
					$("#transfer_out_error").html(info.errors);
				}
			});
		}
		$(this).prop("disabled", false);
	});
	
	//Get Results Of Previous Ticket
	$(document).on("click", "#results_confirm", function(){
		$(this).prop("disabled", true);
		$("#results_error").hide();
		$("#results_error").html("");
		if($("#results_ticket_number").val().length != 15){
			$("#results_error").append("Invalid Ticket Number<br>");
			$("#results_error").slideDown("fast");
		}
		if($("#results_ticket_number").val().match(/^[0-9]+$/) == null){
			$("#results_error").append("Tickets are numeric only<br>");
			$("#results_error").slideDown("fast");
		}

		//ajax for the ticket information
		if($("#results_error").html() == ""){
			$.ajax({
				url: "ajax/results.php",
				data: {action: "get_results", ticket_number: $("#results_ticket_number").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);

				if(info.success == "true"){
					$("#results_table_body").html(info.table);
					$("#results_total").text(Number(info.total_won, 10).toFixed(2));
					$("#results_details").slideDown('fast');
				}
			});
		}
		$(this).prop("disabled", false);
	});

	//Void Ticket
	$(document).on("click", "#voids_confirm", function(){
		$(this).prop("disabled", true);
		$("#voids_error").hide();
		$("#voids_error").html("");
		if($("#voids_ticket_number").val().length != 15){
			$("#voids_error").append("Invalid Ticket Number<br>");
			$("#voids_error").slideDown("fast");
		}
		if($("#voids_ticket_number").val().match(/^[0-9]+$/) == null){
			$("#voids_error").append("Tickets are numeric only<br>");
			$("#voids_error").slideDown("fast");
		}
		if($("#voids_ticket_reason").val() == null){
			$("#voids_error").append("Must enter a reason<br>");
			$("#voids_error").slideDown("fast");
		}

		//ajax for the ticket information
		if($("#voids_error").html() == ""){
			$.ajax({
				url: "ajax/voids.php",
				data: {action: "void_ticket", ticket_number: $("#voids_ticket_number").val(), reason: $("#voids_ticket_reason").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);

				if(info.success == "true"){
					window.open("/franchise/receipts/get_receipt.php?void=&ticket_number="+$("#voids_ticket_number").val(),"receipt");
					//$("#receipt_modal").modal("show");
					$("#voids_modal").modal("hide");
					$("#cashier_balance").text(info.balance);
				}else{
					$("#voids_error").html(info.errors);
					$("#voids_error").slideDown("fast");
				}
			});
		}

		$(this).prop("disabled", false);
	});


	$(document).on("click", "#deposit_confirm", function(){
	$("#deposit_modal").modal("hide");
	$("#deposit_customer_amount").val("");	
	$("#deposit_amount").modal("show");

	<!-- $("#dep_remove_confirm").prop("disabled",false); !-->
	<!-- ("#dep_amount").text($('#deposit_customer_amount').val());!-->
	});

	$(document).on("click", "#deposit_confirm_amount", function(){
	$("#deposit_amount").modal("hide");
	$("#remove_confirmation_modal_dep").modal("show");
	$("#dep_remove_confirm").prop("disabled",false);
	$("#dep_amount").text($('#deposit_customer_amount').val());
	});
	


	//Make a deposit
	$(document).on("click", "#dep_remove_confirm", function(){

		$(this).prop("disabled", true);
		
		$("#deposit_customer_amount").prop("disabled",true);
		$("#deposit_customer_number").prop("disabled",true);
		$("#deposit_customer_pin").prop("disabled",true);
		$("#deposit_confirm").prop("disabled",true);
		//$("#myModal").modal("show");
		

		$("#deposit_error").hide();
		$("#deposit_error").html("");

		//ajax for the ticket information
		if($("#deposit_error").html() == ""){
			$.ajax({
		
				url: "ajax/deposit.php",
				data: {action: "customer_deposit", amount: $("#deposit_customer_amount").val(), customer_number: $("#deposit_customer_number").val(), customer_pin: $("#deposit_customer_pin").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);
				$("#dep_remove_confirm").prop("disabled", false);
				$("#deposit_confirm").prop("disabled",false);
				if(info.success == "true"){
					window.open("/franchise/receipts/get_receipt.php?transaction_id="+info.transaction_id,"receipt");
					//$("#receipt_modal").modal("show");
					get_balance();
					$("#deposit_modal").modal("hide");
					$("#remove_confirmation_modal_dep").modal("hide");
					$("#dep_remove_confirm").prop("disabled",false);
					$("#deposit_confirm").prop("disabled",false);
					$("#deposit_customer_info").prop("disabled",true);
				}else{
					$("#deposit_error").html(info.errors);
					$("#deposit_error").slideDown("fast");
				}
			});
		}

		
		$("#deposit_customer_amount").prop("disabled",false);
		$("#deposit_customer_number").prop("disabled",false);
		$("#deposit_customer_pin").prop("disabled",false);
	});

	//Get customer balance when making a deposit
	$(document).on("blur", "#deposit_customer_number", function(){
		//ajax for the balance information
		if($("#deposit_customer_number").val() != ""){
			$.ajax({
				url: "ajax/deposit.php",
				data: {action: "customer_balance", customer_number: $("#deposit_customer_number").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);
				
				if(info.success == "true"){
					// show previously saved pic if its valid
					if(info.customer_info['picture_path']){
						$("#deposit_user_pic").attr("src", info.customer_info['picture_path']+"&auth=<?php echo sha1(md5($session->userinfo['email'].$session->userinfo['id'].date('Ymdh'))); ?>");
					}else{
						// $("#deposit_user_pic").attr("src", "/imaages/CSCLotto_chip.png"); //default to logo for CSCLotto employees
						$("#deposit_user_pic").attr("src", "/admin/images/generic-user-purple.png"); //default to generic icon for customers
					}
					$("#deposit_customer_name").html(info.customer_info['name']);
					$("#deposit_customer_dob").html(info.customer_info['dob']);
					$("#deposit_customer_age").html(info.customer_info['age']);
					$("#deposit_customer_gender").html(info.customer_info['gender']);
					$("#deposit_customer_address").html(info.customer_info['address']);
					$("#deposit_customer_island").html(info.customer_info['island']);
					$("#deposit_customer_phone").html(info.customer_info['phone']);
					$("#deposit_pin_text").html(info.pin_text);
					$("#deposit_error").slideUp("fast");
					$("#deposit_customer_info").slideDown("fast");
				}else{
					$("#deposit_error").html(info.errors);
					$("#deposit_error").slideDown("fast");
				}
			});
		}else{
			$("#deposit_error").html("You must enter a customer number!<br>");
			$("#deposit_error").slideDown("fast");
		}
	});
	
	
	
	
	
	
	
	<!-- MODEL FOR WITHDRAWAL OF AMOUNT !-->
	
	$(document).on("click", "#withdrawal_confirm", function(){
	$("#withdrawal_modal").modal("hide");
	$("#withdrawal_customer_amount").val("");	
	$("#withdrawal_amount").modal("show");	
	<!-- $("#with_amount").text($('#withdrawal_customer_amount').val()); !-->
	});

	$(document).on("click", "#withdrawal_confirm_amount", function(){
	$("#withdrawal_amount").modal("hide");		
	$("#remove_confirmation_modal_with").modal("show");
	$("#with_remove_confirm").prop("disabled",false);
	$("#with_amount").text($('#withdrawal_customer_amount').val());
	});
	
	

	//Make a withdrawal
	$(document).on("click", "#with_remove_confirm", function(){
		$(this).prop("disabled", true);
		$("#withdrawal_customer_amount").prop("disabled",true);
		$("#withdrawal_customer_number").prop("disabled",true);
		$("#withdrawal_customer_pin").prop("disabled",true);
		$("#withdrawal_confirm").prop("disabled",true);
		$("#withdrawal_error").hide();
		$("#withdrawal_error").html("");

		//ajax for the ticket information
		if($("#withdrawal_error").html() == ""){
		
			$.ajax({
				url: "ajax/withdrawal.php",
				data: {action: "customer_withdrawal", amount: $("#withdrawal_customer_amount").val(), customer_number: $("#withdrawal_customer_number").val(), customer_pin: $("#withdrawal_customer_pin").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);
				$("#with_remove_confirm").prop("disabled", false);
				$("#withdrawal_confirm").prop("disabled",false);
				if(info.success == "true"){
					window.open("/franchise/receipts/get_receipt.php?transaction_id="+info.transaction_id,"receipt");
					//$("#receipt_modal").modal("show");
					get_balance();
					$("#withdrawal_modal").modal("hide");
					$("#remove_confirmation_modal_with").modal("hide");
					$("#with_remove_confirm").prop("disabled",false);
					$("#withdrawal_confirm").prop("disabled",false);
					$("#withdraw_customer_info").prop("disabled",true);
				}else{
					$("#withdrawal_error").html(info.errors);
					$("#withdrawal_error").slideDown("fast");
				}
			});
		}

		//$(this).prop("disabled", false);
		$("#withdrawal_customer_amount").prop("disabled",false);
		$("#withdrawal_customer_number").prop("disabled",false);
		$("#withdrawal_customer_pin").prop("disabled",false);
	});

	//Petty Cash withdrawal
	$(document).on("click", "#petty_cash_confirm", function(){
		$(this).prop("disabled", true);
		$("#petty_cash_amount").prop("disabled",true);
		$("#withdrawal_reason").prop("disabled",true);
		$("#petty_cash_error").hide();
		$("#petty_cash_error").html("");
                var amount = $("#petty_cash_amount").val();
		if($.isNumeric(amount) && amount > 0){	
                        if($("#withdrawal_reason").val() != ''){	
			$.ajax({
				url: "ajax/withdrawal.php",
				data: {action: "petty_cash_withdrawal", amount: amount, reason: $("#withdrawal_reason").val() },
				method: "POST",
                                dataType: 'json'
			}).done(function( data ) {				

				if(data.success == "true"){
                                        window.open("/franchise/receipts/get_receipt.php?transaction_id="+data.transaction_id,"receipt");
					$("#petty_cash_modal").modal("hide");
					get_balance();	
				}else{
					$("#petty_cash_error").html(data.errors);
					$("#petty_cash_error").slideDown("fast");
				}
			});
                      } else {
                         $("#petty_cash_error").html("Please enter Reason.");
		         $("#petty_cash_error").slideDown("fast");
                    }
		} else {
                     $("#petty_cash_error").html("Please enter valid amount.");
		     $("#petty_cash_error").slideDown("fast");
                }

		$(this).prop("disabled", false);
		$("#petty_cash_amount").prop("disabled",false);
		$("#withdrawal_reason").prop("disabled",false);
		
	});


        //Get customer balance when making a withdrawal
	$(document).on("blur", "#withdrawal_customer_number", function(){
		//ajax for the balance information
		if($("#withdrawal_customer_number").val() != ""){
			$.ajax({
				url: "ajax/withdrawal.php",
				data: {action: "customer_balance", customer_number: $("#withdrawal_customer_number").val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);

				if(info.success == "true"){
					// show previously saved pic if its valid
					if(info.customer_info['picture_path']){
						$("#withdraw_user_pic").attr("src", info.customer_info['picture_path']+"&auth=<?php echo sha1(md5($session->userinfo['email'].$session->userinfo['id'].date('Ymdh'))); ?>");
					}else{
						// $("#withdraw_user_pic").attr("src", "/imaages/CSCLotto_chip.png"); //default to logo for CSCLotto employees
						$("#withdraw_user_pic").attr("src", "/admin/images/generic-user-purple.png"); //default to generic icon for customers
					}
					$("#withdraw_customer_name").html(info.customer_info['name']);
					$("#withdraw_customer_dob").html(info.customer_info['dob']);
					$("#withdraw_customer_age").html(info.customer_info['age']);
					$("#withdraw_customer_gender").html(info.customer_info['gender']);
					$("#withdraw_customer_address").html(info.customer_info['address']);
					$("#withdraw_customer_island").html(info.customer_info['island']);
					$("#withdraw_customer_phone").html(info.customer_info['phone']);
					$("#withdraw_pin_text").html(info.pin_text);
					$("#withdraw_error").slideUp("fast");
					$("#withdraw_customer_info").slideDown("fast");
					
					$("#withdrawal_customer_available").text(info.balance.toFixed(2));
					$("#withdrawal_pin_text").text(info.pin_text);
					$("#withdrawal_error").slideUp("fast");
				}else{
					$("#withdrawal_customer_available").text("0.00");
					$("#withdrawal_error").html(info.errors);
					$("#withdrawal_error").slideDown("fast");
				}
			});
		}else{
			$("#withdrawal_error").html("You must enter a customer number!<br>");
					$("#withdrawal_error").slideDown("fast");
		}
	});

	//Make a open shift
	$(document).on("click", "#open_shift_confirm", function(){
		$(this).prop("disabled", true);
		//ajax for the balance information
		if($("#open_shift_location").val() != ""){
			$.ajax({
				url: "ajax/shift.php",
				data: {action: "open_shift", location: $("#open_shift_location").val(), opening:$("#open_shift_opening").text()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);

				if(info.success == "true" || info.errors == "1"){
					location.reload();
				}else{
                                      
					$("#shift_open_error").html(info.errors);
					$("#shift_open_error").slideDown("fast");
					$(this).prop("disabled", false);
				}
			});
		}else{
			$("#shift_open_error").html("You must enter a valid location!<br>");
			$("#shift_open_error").slideDown('fast');
			$(this).prop("disabled", false);
		}
	});

	//Close a shift
	$(document).on("click", "#close_shift_confirm", function(){
		$(this).prop("disabled", true);
		//ajax for the balance information
		if($("#close_shift_bag_number").val() != ""){
			$.ajax({
				url: "ajax/shift.php",
				data: {
					action: "close_shift", 
					bag_number: $("#close_shift_bag_number").val(), 
					closing:$("#close_shift_closing").text(), 
					expected:$("#close_shift_expected").text(), 
					sales:$("#close_shift_sales").text(), 
					payouts:$("#close_shift_payouts").text(),
					pettycash:$("#close_shift_pettycash").text(),
					closing_1s:$("#close_shift_one_bills").val(),
					closing_5s:$("#close_shift_five_bills").val(),
					closing_10s:$("#close_shift_ten_bills").val(),
					closing_20s:$("#close_shift_twenty_bills").val(),
					closing_50s:$("#close_shift_fifty_bills").val(),
					closing_100s:$("#close_shift_hundred_bills").val(),
					closing_coins:$("#close_shift_coins").val()
				},
				method: "POST"
			}).done(function( data ) {
				$("#shift_close_error").html("");
				$("#shift_close_error").hide();
				var info = JSON.parse(data);

				if(info.success == "true"){
					window.open("/franchise/receipts/get_receipt.php?logout=true&shift_id="+info.shift_id,"receipt");
					//$("#receipt_modal").modal("show");
				}else{
					$("#shift_close_error").html(info.errors);
					$("#shift_close_error").slideDown("fast");
					$(this).prop("disabled", false);
				}
			});
		}else{
			$("#shift_close_error").html("You must enter a bag number!<br>");
			$("#shift_close_error").slideDown("fast");
			$(this).prop("disabled", false);
		}
	});

	//Start the open shift bill amount handlers
	function add_open_bill_values(){
		return (Number($("#open_shift_one_bills").val() * 1, 10) +
				Number($("#open_shift_five_bills").val() * 5, 10) +
				Number($("#open_shift_ten_bills").val() * 10, 10) +
				Number($("#open_shift_twenty_bills").val() * 20, 10) +
				Number($("#open_shift_fifty_bills").val() * 50, 10) +
				Number($("#open_shift_hundred_bills").val() * 100, 10) +
				Number($("#open_shift_coins").val().replace("$",""), 10)).toFixed(2);
	}
	$(document).on("blur", "#open_shift_one_bills", function(){
		$("#open_shift_opening").text(Number(add_open_bill_values(), 10).formatMoney(2));
	});
	$(document).on("blur", "#open_shift_five_bills", function(){
		$("#open_shift_opening").text(Number(add_open_bill_values(), 10).formatMoney(2));
	});
	$(document).on("blur", "#open_shift_ten_bills", function(){
		$("#open_shift_opening").text(Number(add_open_bill_values(), 10).formatMoney(2));
	});
	$(document).on("blur", "#open_shift_twenty_bills", function(){
		$("#open_shift_opening").text(Number(add_open_bill_values(), 10).formatMoney(2));
	});
	$(document).on("blur", "#open_shift_fifty_bills", function(){
		$("#open_shift_opening").text(Number(add_open_bill_values(), 10).formatMoney(2));
	});
	$(document).on("blur", "#open_shift_hundred_bills", function(){
		$("#open_shift_opening").text(Number(add_open_bill_values(), 10).formatMoney(2));
	});
	$(document).on("blur", "#open_shift_coins", function(){
		$("#open_shift_opening").text(Number(add_open_bill_values(), 10).formatMoney(2));
	});


	//Start the open shift bill amount handlers
	function add_close_bill_values(){
				return (Number($("#close_shift_one_bills").val() * 1, 10) +
				Number($("#close_shift_five_bills").val() * 5, 10) +
				Number($("#close_shift_ten_bills").val() * 10, 10) +
				Number($("#close_shift_twenty_bills").val() * 20, 10) +
				Number($("#close_shift_fifty_bills").val() * 50, 10) +
				Number($("#close_shift_hundred_bills").val() * 100, 10) +
				Number($("#close_shift_coins").val().replace("$",""), 10)).toFixed(2);
	}
	$(document).on("blur", "#close_shift_one_bills", function(){
		$("#close_shift_closing").text(Number(add_close_bill_values(), 10).formatMoney(2));
		$("#close_shift_over_short").text((-(Number($("#close_shift_expected").text().replace(",",""), 10) - Number($("#close_shift_closing").text().replace(",",""), 10))).formatMoney(2));
	});
	$(document).on("blur", "#close_shift_five_bills", function(){
		$("#close_shift_closing").text(Number(add_close_bill_values(), 10).formatMoney(2));
		$("#close_shift_over_short").text((-(Number($("#close_shift_expected").text().replace(",",""), 10) - Number($("#close_shift_closing").text().replace(",",""), 10))).formatMoney(2));
	});
	$(document).on("blur", "#close_shift_ten_bills", function(){
		$("#close_shift_closing").text(Number(add_close_bill_values(), 10).formatMoney(2));
		$("#close_shift_over_short").text((-(Number($("#close_shift_expected").text().replace(",",""), 10) - Number($("#close_shift_closing").text().replace(",",""), 10))).formatMoney(2));
	});
	$(document).on("blur", "#close_shift_twenty_bills", function(){
		$("#close_shift_closing").text(Number(add_close_bill_values(), 10).formatMoney(2));
		$("#close_shift_over_short").text((-(Number($("#close_shift_expected").text().replace(",",""), 10) - Number($("#close_shift_closing").text().replace(",",""), 10))).formatMoney(2));
	});
	$(document).on("blur", "#close_shift_fifty_bills", function(){
		$("#close_shift_closing").text(Number(add_close_bill_values(), 10).formatMoney(2));
		$("#close_shift_over_short").text((-(Number($("#close_shift_expected").text().replace(",",""), 10) - Number($("#close_shift_closing").text().replace(",",""), 10))).formatMoney(2));
	});
	$(document).on("blur", "#close_shift_hundred_bills", function(){
		$("#close_shift_closing").text(Number(add_close_bill_values(), 10).formatMoney(2));
		$("#close_shift_over_short").text((-(Number($("#close_shift_expected").text().replace(",",""), 10) - Number($("#close_shift_closing").text().replace(",",""), 10))).formatMoney(2));
	});
	$(document).on("blur", "#close_shift_coins", function(){
		$("#close_shift_closing").text(Number(add_close_bill_values(), 10).formatMoney(2));
		$("#close_shift_over_short").text((-(Number($("#close_shift_expected").text().replace(",",""), 10) - Number($("#close_shift_closing").text().replace(",",""), 10))).formatMoney(2));
	});

	// main datatable initialization
	var customers_data_table = $('#customers_datatable').DataTable({
		"responsive": true,
		"processing": true,
		"serverSide": true,
		"deferRender": true,
		bInfo: false,
		"ajax": "/franchise/ajax/datatables/manage_customers.php",
		"columns": [
			{ "data": "customer_number" },
			{ "data": "customer_name" },
			{ "data": "available_balance" },
			{ "data": "email" },
			{ "data": "phones" }
		],
		"order": [[1, 'asc']]

	});

	// main datatable initialization
	var pending_transfers_data_table = $('#pending_transfers_datatable').DataTable({
		"responsive": true,
		"processing": true,
		"serverSide": true,
		"deferRender": true,
		bInfo: false,
		"ajax": "/franchise/ajax/datatables/pending_transfers.php",
		"columns": [
			{ "data": "from_location" },
			{ "data": "amount" }
		],
		"order": [[0, 'desc']]

	});
	
	$('#pending_transfers_datatable tbody').on('click', 'tr', function(){
		// toggle selection css class
		if($(this).hasClass('selected')){
			$(this).removeClass('selected');
		}else{
			pending_transfers_data_table.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	});

	Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };
});
