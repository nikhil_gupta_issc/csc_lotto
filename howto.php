<?php
	$page_title = "How to";
	$menu_color = "saddlebrown";
	$footer_color = "saddlebrown";
	
	$top_left_fixed_img = "";
	$top_right_fixed_img = "<img src='/images/cards_rgt.png'>";

	include("header.php");
	include('page_top.php');
	
	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}

?>
<style>

.edit
{
	margin-top:-2px;
	padding-top:1px;

}
</style>
<script type="text/javascript" src="/lib/assets/ckeditor/ckeditor.js"></script>
	<script>
		$(document).ready(function(){

			CKEDITOR.replace("text");

			$.fn.modal.Constructor.prototype.enforceFocus = function () {
			    var $modalElement = this.$element;
			    $(document).on('focusin.modal', function (e) {
			        var $parent = $(e.target.parentNode);
			        if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length
			            // add whatever conditions you need here:
			            &&
			            !$parent.hasClass('cke_dialog_ui_input_select') && !$parent.hasClass('cke_dialog_ui_input_text')) {
			            $modalElement.focus()
			        }
			    })
			};
			
			$('#error').hide();
			$("#news_delete_btn").removeAttr('disabled');
			$("#submit_add_edit").removeAttr('disabled');
			
			$(document).on("click", "#manage_images_btn", function(){
				$('#manage_images_modal').modal("show");
			});

			$(document).on("click", ".edit", function(){
				
				var id_arr = this.id.split("_");
				var id = id_arr[1];
				
				$.ajax({
					url: "/ajax/how_to.php",
					dataType: 'json',
					data: {
						action: 'load',
						id: id
					},
					type: "POST",
					success: function(data){
						$('#image').val(data.image).trigger('change');
						$('#title').val(data.title);

						CKEDITOR.instances['text'].setData(data.text)
		                						
						$('#text').val(data.text);
						$('#how_to_id').val(data.id);
						$('#news_delete_btn').show();
						$('#submit_add_edit').html('Save Changes');
						$('#add_news').modal("show");
					}
				});
			});

			$(document).on("click", "#add_btn", function(){
				$('#error').hide();
				$('#image').val("").trigger('change');
				$('#title').val("");

				CKEDITOR.instances['text'].setData('');
				
				$('#text').val("");
				$('#how_to_id').val('-1');
				$('#news_delete_btn').hide();
				$('#submit_add_edit').html('Create Post');
				$('#add_news').modal("show");
				$('#error').hide();
				
			});
			
			$(document).on("click", "#news_delete_btn", function(){
				$("#news_delete_btn").attr('disabled', 'disabled');
				$.ajax({
					url: "/ajax/how_to.php",
					dataType: 'json',
					data: {
						action: 'delete',
						id: $('#how_to_id').val()
					},
					type: "POST",
					success: function(data){
						$('#add_news').modal("hide");
						window.location.reload();
					}
				});
			});
			
			$(document).on("click", "#submit_add_edit", function(){

				var text = CKEDITOR.instances['text'].getData();
				
				$("#submit_add_edit").attr('disabled', 'disabled');
					
				var action = 'update';
				if($(this).html() == 'Create Post'){
					var action = 'insert';
				}else if($(this).html() == 'Save Changes'){
					var action = 'update';
				}
				
				if($('#title').val() == '' || text == '')
				{
					$('#error').show();
					$("#submit_add_edit").removeAttr('disabled');
					$('#error').html('Title or Message can not be Blank.');
				}
				else
				{
					 
					$.ajax({
						url: "/ajax/how_to.php",
						dataType: 'json',
						data: {
							action: action,
							id:$('#how_to_id').val(),
							title: $('#title').val(),
							text: text
						},
						type: "POST",
						success: function(data){
							$('#add_news').modal("hide");
							window.location.reload();
						}
					});
				}
			});
			
			$(document).on("change", "#image", function(){
				if($(this).val() != ""){
					$('#image_preview').attr('src', '/images/news/'+$(this).val());
					$('#image_preview').show();
				}else{
					$('#image_preview').hide();
				}
			});
			
	/*		$('#manage_images_modal').on('hidden.bs.modal', function (){
				$.ajax({
					url: "/ajax/update_news_pic_list.php",
					type: "POST",
					success: function(data){
						$('#image').html(data);
					}
				});
			});
			
			$.ajax({
				url: "/ajax/update_news_pic_list.php",
				type: "POST",
				success: function(data){
					$('#image').html(data);
				}
			});
			*/
			
			//$('.accordion_expand').addClass('collapsed');
			//$('.accordion_expand').attr('aria-expanded','false');
			
		});
		
		
		
		
	</script>



<!-- Blog Start -->
	<div class="clearfix" style="background: #fff; color: #000;">	
			<div class="row">
					<div class="container">
					   <div class="header_image">
							<img src="/images/howto.jpg?v=1" class="img_grow_wide">
						</div>
					</div>
				</div>

	<div class="clearfix" style="background: #fff; color: #000;">	
	<p>&nbsp;</p>
	<?php
		if ($session->userinfo['userlevel'] > 8){
	?>
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;"><?php echo $page_title; ?> ~ Admin</h3>
					<div class="btn-group pull-right">
						<!-- <a id="add_btn" href="#" class="btn-sm add">Add</a> !-->
						<button id="add_btn" style="margin-left: 10px; padding-right: 10px; padding-left: 10px;" value="Add" acton="logout" type="button"  class="btn btn-primary btn-sm pull-right add" name="btn_add">Add</button>
					</div>
				</div>
			</div>
	<?php
		}
	?>
<div class="col-sm-12">
<p style="color:#000;">DESCRIPTION</p>
</div>
<br><br>
<!-- Accordion Start !-->
<div id="accordion" role="tablist" aria-multiselectable="true" style="width:1150px; padding-left:40px;padding-right:50px;" >
<?php
			$q = "SELECT * FROM `how_to` WHERE `is_deleted` != 1 order by id DESC";
			$news = $db->query($q);
			foreach($news as $new){
		?>	 

	<div class="panel panel-default accordion_panel">
			<div class="panel-heading" role="tab" id="headingOne<?php echo $new['id'];?>">
					<h4 class="panel-title" style="color:#ffffff">
						<a class="accordion_expand" data-toggle="collapse" style="text-decoration:none; color:#ffffff" data-parent="#accordion" href="#collapseOne<?php echo  $new['id'];?>" aria-expanded="true" aria-controls="collapseOne<?php echo  $new['id'];?>">
							 <?php echo $new['title']; ?>
						</a>
						<?php
						if ($session->userinfo['userlevel'] > 8){
					?>
						<div class="btn-group pull-right">
			<!-- <a class="btn-sm edit" style="cursor:pointer;" id="id_<?php echo $new['id']; ?>">Edit</a> !-->
					<button style="margin-left: 10px; padding-right: 10px; padding-left: 10px;" value="Edit" action="logout" type="button" id="id_<?php echo $new['id']; ?>" class="btn btn-primary btn-sm pull-right edit" name="btn_edit">Edit</button>
						</div>
					<?php
						}
					?>
					</h4>
			</div>
			
			<div id="collapseOne<?php echo  $new['id'];?>" style="padding-bottom:20px; padding-top: 20px; padding-left:10px; padding:right:10px;color:gray" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne<?php echo $new['id'];?>">
				<?php echo html_entity_decode($new['text']); ?>
			</div>
	</div>

<?php
} ?>
</div>
<!-- Accordion End !-->

<?php /*	<div class="col-md-12">
		<?php
			$q = "SELECT * FROM `news_updates` WHERE `is_deleted` != 1 order by id DESC";
			$news = $db->query($q);
			foreach($news as $new){
		?>
		<div class="col-sm-6">
			<?php
				if ($session->userinfo['userlevel'] > 8){
			?>
				<div class="btn-group pull-right">
					<a class="btn btn-primary btn-sm edit-news" id="id_<?php echo $new['id']; ?>">Edit</a>
				</div>
			<?php
				}
			?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $new['title']; ?></h3>
				</div>
				<div class="panel-body" style="max-height:500px; overflow:hidden;background-color: #000;
  color: #fff;">
				<?php
					if(strlen($new['image']) > 4){
						echo "<img src='/images/news/".$new['image']."' width='100%'/>";
					}
					echo "<p style='padding-bottom:20px;'>".$new['text']."</p>";
				?>
				</div>
				<div class="btn-group pull-right" style="margin-bottom:5px;">
					<a class="btn btn-primary btn-sm" data-toggle="modal" <?php echo 'data-target="#more_news'.$new['id'].'"';?>>Read More...</a>
				</div>
			</div>
		</div>
		<?php
			}
		?>
	</div>
	
		
					<button data-toggle="collapse" data-target="#demo">Collapsible</button>

		
</div>

<!-- /Blog End -->
*/ ?>
<!-- View More Modal -->
<?php
	$q = "SELECT * FROM `how_to` WHERE `is_deleted` != 1";
	$news = $db->query($q);
	foreach($news as $new){
?>
<div>
	<?php	
		echo "<div class='modal fade' id='more_news".$new['id']."' tabindex='-1' role='dialog' aria-labelledby='settingsModal'>";
	?>
	
	 

		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
				<div class="panel-heading">
					<h3 class="panel-title"><strong><?php echo $new['title']; ?></strong></h3>
					<h3 class="panel-title"><strong><?php echo $new['title']; ?></strong></h3>
				</div>
				<div class="panel-body">
				
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	}
?>
<!-- /View More Modal -->

<!-- Add/Edit Modal -->
<div class="modal fade" id="add_news" tabindex="-1" role="dialog" aria-labelledby="slider_pic_modal_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header clearfix">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title pull-left" id="slider_pic_modal_label">Add How to</h4>
			</div>
			<div class="modal-body clearfix">
				<div id="upload_photos" class="col-sm-12">
					<div class="col-sm-3">
						<img id="image_preview" src="" style="width: 100%; display: none; border: 1px solid black;">
					</div>
					
					<div class="col-sm-12" style="padding-top: 15px;">
						<div class="form-group">
							<label for='title'>Title</label>
							<textarea id="title" type='text' class='form-control' name='title'></textarea>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">		
							<label for='text'>Body</label>
							<textarea id="text" type='text' class='form-control' name='text' rows='6'></textarea>
						</div>
					</div>
					
				<div class="col-sm-12" style="padding-left:45px;">
					<div id="error" class="alert alert-danger" role="alert" style="display:none;"></div>	
				</div>
					
				</div>					
			</div>
			
			<div class="modal-footer">
				<input id="how_to_id" type='hidden' name='id' value='-1'>
				<button id="news_delete_btn" type="button" class="btn btn-danger pull-left">Delete</button>
				<button id="submit_add_edit" type="submit" class="btn btn-primary">Create Post</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- /Add Modal -->

<!-- Manages Images -->
<div class="modal fade" id="manage_images_modal" tabindex="-1" role="dialog" aria-labelledby="manage_images_label" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header clearfix">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title pull-left" id="manage_images_label">Manage Images</h4>
			</div>
			<div class="modal-body clearfix">
				<div id="upload_photos" class="col-sm-12">
					<?php include($_SERVER['DOCUMENT_ROOT'].'/upload_news_photos.php'); ?>
				</div>					
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
</div>
<!-- /Manage Images -->

<?php
	include("footer.php");
?>
