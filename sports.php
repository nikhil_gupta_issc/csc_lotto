<?php
	$page_title = "NFL, NBA, College Bet on your favorite sports team!";
	
	$top_left_fixed_img = '<img src="/images/baseball.png">';
	$top_right_fixed_img = '<img src="/images/football.png">';

	include("header.php");
	include('page_top.php');

	if(!$session->logged_in){
		include('public_sports.php');
	
		include("footer.php");
		die;
	}else{
		
		$settings = array();
		$q = "SELECT * FROM `settings`";
		$settings_info = $db->query($q);
		foreach($settings_info as $setting){
			$settings[$setting['setting']] = $setting['value'];
		}
?>

	<style>
		.bg{
			background: black url('/images/sportbg.jpg') no-repeat center top;
		}
		ul.nav.nav-pills.head-menu>li>a{
			color: white !important;
		}
		ul.nav.nav-pills.head-menu>li:hover>a{
			color: hsl(288, 100%, 18%) !important;
		}
		ul.nav.nav-pills.head-menu>li.active:hover>a{
			color: white !important;
		}
	</style>
<script type="text/javascript">
		$(document).ready(function(){
			$('#slider_photo_btn').click(function(){
				$("#slider_pic_modal").modal("show");
			});
		});
	</script>
<?php
			if($session->userlevel >= 9){
				echo '
		<div style="text-align: center;">
			<button class="btn btn-primary" id="slider_photo_btn" style="margin: 5px;">Change Photos</button>
		</div>
				';
			}
		?>
	<div class="row">
				<div class="container">
				<div class="header_image">
		<?php
			$files = scandir('images/sports_slider_image/');
			$total = count($files);
			$images = array();
			for($x = 0; $x <= $total; $x++){
				if($files[$x] != '.' && $files[$x] != '..' && is_file('images/sports_slider_image/'.$files[$x])){
					$images[] = $files[$x];
				}
			}
		?>
	
		<div id="carousel-index" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'" class="active"></li>';
						}else{
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'"></li>';
						}
					}
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '
				<div class="item active">
					<a href="#">
						<img src="/images/sports_slider_image/'.$images[$x].'" class="carousel-image"  width="100%" height="222px">
					</a>
				</div>
							';
						}else{
							echo '
				<div class="item">
					<a href="#">
						<img src="/images/sports_slider_image/'.$images[$x].'" class="carousel-image"  width="100%" height="222px">
					</a>
				</div>
							';
						}
					}
				?>
			</div>
		  
				
			</div>
		</div>
                   <!--<div class="header_image">
						<img src="/images/sports_header.jpg" class="img_grow_wide">
					</div>-->
                </div>
            </div>
	<?php
 	if($settings['sports_enabled'] == 1){
            if($settings['sportbook_api'] == 'sbtech'){
               $url = "http://asurewin.sbtech.com/?stoken=".$session->userid;               
               ?>
               <div class="row">		
                <iframe class="container" id="sbtech_iframe" src="<?php echo $url;?>" title="Sportsbook" width="100%" height="1000px" frameborder="0"></iframe>
	</div>
            <?php
            }             
            elseif($settings['sportbook_api'] == 'royalsoft') {
               $url = "https://asurewin.royalsoftsports.com/Login.aspx?token=".$session->userid;
               ?>
               <div class="row">		
                <iframe class="container" id="sbtech_iframe" src="<?php echo $url;?>" title="Sportsbook" width="100%" height="1000px" frameborder="0"></iframe>
	</div>
               <?php }	
	
	}else{
	?>
	<div class='well' style='height: 400px; text-align: center; margin:0;'>
		<h2><br><br>We're sorry but Sports Book is currently Unavailable.<br><br>Please check back later.</h2>
	</div>
	<?php
	}
	?>
	</div>
<!-- Modal for Slider Pic Selection -->
	<div class="modal fade" id="slider_pic_modal" tabindex="-1" role="dialog" aria-labelledby="slider_pic_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="slider_pic_modal_label">Change Slider Photos</h4>

					<div id="customer_name2" class="pull-right" style="padding-right: 6px;">
						
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div id="upload_photos" class="col-sm-12">
						<?php include($_SERVER['DOCUMENT_ROOT'].'/upload_slider_photos.php'); ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<?php
	}
	
	include("footer.php");
?>
