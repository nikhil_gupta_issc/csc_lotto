<?php
	include("header.php");
	include('page_top.php');
	
	$games = array();
	
	// get payout rates and ball counts
	$q = "SELECT * FROM `payout_scheme_detail` WHERE `scheme_id` = 1 ORDER BY `scheme_id` ASC";
	$payout_info = $db->query($q);
	foreach($payout_info as $payout){
		// pick random example
		$pick = $core->generate_random_integer(0, pow(10, ($payout['ball_count'])) -1);
		$boxed_odds = number_format(round(pow(10, $payout['ball_count']) / count($core->permutations($pick, true))));
		
		$games[$payout['ball_count']]['boxed']['bet_type'] = $payout['ball_count']."-Ball Boxed";
		$games[$payout['ball_count']]['boxed']['odds'] = "1 in ".$boxed_odds;
		$games[$payout['ball_count']]['boxed']['bet'] = $pick;
		$games[$payout['ball_count']]['boxed']['winners'] = implode(" ", $core->permutations($pick, true));
		$games[$payout['ball_count']]['boxed']['win_1'] = number_format($payout['payout_rate'] * $core->get_pay_factor($pick, 1, 1));
		$games[$payout['ball_count']]['boxed']['win_2'] = number_format($payout['payout_rate'] * $core->get_pay_factor($pick, 2, 1));
		
		$games[$payout['ball_count']]['straight']['bet_type'] = $payout['ball_count']."-Ball Straight";
		$games[$payout['ball_count']]['straight']['odds'] = "1 in ".number_format(pow(10, ($payout['ball_count'])));
		$games[$payout['ball_count']]['straight']['bet'] = $pick;
		$games[$payout['ball_count']]['straight']['winners'] = $pick;
		$games[$payout['ball_count']]['straight']['win_1'] = number_format($payout['payout_rate']);
		$games[$payout['ball_count']]['straight']['win_2'] = number_format($payout['payout_rate'] * 2);
	}
?>
	<style>
		.bg{
			background: black url('/images/lotteryh2_03.jpg') no-repeat center top;
		}
		ul.nav.nav-pills.head-menu>li>a{
			color: white !important;
		}
		ul.nav.nav-pills.head-menu>li:hover>a{
			color: hsl(288, 100%, 18%) !important;
		}
		ul.nav.nav-pills.head-menu>li.active:hover>a{
			color: white !important;
		}
	</style>

	<div class="row">
		<div class="container">
		   <div class="header_image">
				<img src="/images/lottobanner.jpg" class="img_grow_wide">
			</div>
		</div>
	</div>

	<div class="clearfix" style="background:#111111; padding:15px;">
		<div class="lotto_payouts">
			<table class="table table-striped table-bordered" id="payout_table">
				<thead>
					<tr>
						<th colspan="6" style="text-align: center;"><a href="/lottery.php"><button class="btn btn-magenta pull-left">&laquo; Back to Lottery</button></a><h3>LOTTERY PAYOUTS</h3><p style="margin-top: -5px;"><a href='?'>Refresh</a> the page to see different examples.</th>
					<tr>
					<tr>
						<th colspan="2" style="text-align: center;">POSSIBLE BETS</th>
						<th colspan="2" style="text-align: center;">EXAMPLES</th>
						<th colspan="2" style="text-align: center;">PAYOUT</th>
					<tr>
					<tr>
						<th style="text-align: center;">Bet Type:</th>
						<th style="text-align: center;">Odds:</th>
						<th style="text-align: center;">If you picked:</th>
						<th style="text-align: center;">You will win if the drawing is:</th>
						<th style="text-align: center;">Bet 1 CSC</th>
						<th style="text-align: center;">Bet 2 CSC</th>
					<tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
		<?php
			foreach($games as $game){
				echo '
					<tr>
						<td>'.$game['boxed']['bet_type'].'</td>
						<td>'.$game['boxed']['odds'].'</td>
						<td>'.$game['boxed']['bet'].'</td>
						<td>'.$game['boxed']['winners'].'</td>
						<td style="text-align: center;">'.$game['boxed']['win_1'].' CSC</td>
						<td style="text-align: center;">'.$game['boxed']['win_2'].' CSC</td>
					</tr>
					<tr>
						<td>'.$game['straight']['bet_type'].'</td>
						<td>'.$game['straight']['odds'].'</td>
						<td>'.$game['straight']['bet'].'</td>
						<td>'.$game['straight']['winners'].'</td>
						<td style="text-align: center;">'.$game['straight']['win_1'].' CSC</td>
						<td style="text-align: center;">'.$game['straight']['win_2'].' CSC</td>
					</tr>
				';
			}
		?>
				</tfoot>
			</table>
		</div>
	</div>
<?php
	include("footer.php");
?>
