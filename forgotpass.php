    <link rel="stylesheet" type="text/css" href="/admin/css/default.css?v=1" />
	<div class="container" style="text-align: center; width: 55%; background-color: #fff; margin-top: 20px;">
		<a href="index.php" border="0"><img src="/images/casino/asw_logo.png" style="margin-top: 20px; margin-bottom: 20px;"></img></a>

		<div class="panel panel-default" style="padding-top: 40px; background-color: #bf0a0a;">
			<div class="panel-body">
				<form action='lib/framework/login.php' method='post' class="form-horizontal">
				<?php
					if(isset($_SESSION['forgotpass'])){
						/**
						* New password was generated for user and sent to user's
						* email address.
						*/
						if($_SESSION['forgotpass']){
							echo "<h2 class='form-signin-heading'>New Password Generated</h2><br>";
							echo "<p>Your new password has been generated and sent to the email associated with your account.</p>";
						}else{
							/**
							* Email could not be sent, therefore password was not
							* edited in the database.
							*/
						
							echo "<h2 class='form-signin-heading'>New Password Failure</h2><br>";
							echo "<p>There was an error sending you the email with the new password, so your password has not been changed.</p><br>";
						}

						echo "<p>Click <a href='index.php'>here</a> to go back to the login page.</p>";
						
						unset($_SESSION['forgotpass']);
					}else{
				?>
					<h2 class="form-group-heading">Reset Password</h2>
					<p>Please enter the e-mail address you used to register your account.</p><br>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-8">
							<input name="email" type="text" class="form-control" id="email" placeholder="Email address" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<button class="btn btn-large btn-primary" type="submit">Reset Password</button>
							<input type="hidden" name="subforgot" value="1">
							<input type='hidden' name='referrer' value='<?php echo str_replace('action=logout', '', $_SERVER['REQUEST_URI']); ?>'>
						</div>
					</div>
				<?php
					}
				?>
				</form>
				<?php
					if($form->num_errors >= 1){
						echo "<div align='center' style='text-align:center; padding: 20px 8px; color:red;'>".$form->error("email")."</div>";
					}
				?>
			</div>
		</div>
    </div> <!-- /container -->

	<?php 
		//include "footer.php";
		die();
	?>
