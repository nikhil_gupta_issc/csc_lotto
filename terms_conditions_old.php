<?php
	include("header.php");
	include('page_top.php');
	include('config.php');
	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}
?>
	<div class="clearfix" style="background:#111111">	
		<div class="row">
			<div class="container">
				<div class="header_image">
				<?php $q = "SELECT image_path FROM pages WHERE id=2";
				      $result = $db->query($q);
				      ?>
					<img src="<?php echo ROOTPATH.'/images/'.$result[0]['image_path'];?>" class="img_grow_wide">
				</div>
			</div>
		</div>

	
		<div class="col-md-12">
			<br>
			<div class="well">
				<?php
				
				$q = "SELECT content FROM pages WHERE id=2";
				$result = $db->query($q);
				
				$string = preg_replace("/&nbsp;/",'',$result[0]['content']);

				$string = html_entity_decode($string);

				echo $string;
				// include("policies/terms.htm"); ?>
			</div>
			<br>
		</div>
	</div>
<?php
	
	include("footer.php");
?>
