<?php
	// force SSL connection
	//if($_SERVER["HTTPS"] != "on"){
		//header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
		//exit();
	//}
	
	require_once("config.php");
	
	// see if we need to logout
	if(isset($_GET['action'])){
		if($_GET['action'] == 'logout'){
			$session->logout();
		}
	}
	
	// hard code security to prevent access to non-customer users
	$q = "SELECT * FROM `customers` WHERE `user_id` = '".$session->userinfo['id']."'";
	$customer_info = $db->queryOneRow($q);


	if($customer_info['user_id'] != $session->userinfo['id'] && $session->logged_in){

		echo "<html><head><link rel='stylesheet' type='text/css' href='/css/default.css' /></head><body class='redirect_body'><div style='text-align:center;'><img src='/images/asw_logo.png?v=<?=IMAGE_VERSION?>' alt='logo'>";
		echo "<h3>You must have an active customer account to access this site.<br>";
		echo "<p>If you think this was done in error, please <a href='/locations.php'>contact us</a>. In the rare case that the issue can't be resolved you are free to contact the <a target='_blank' href='https://www.bahamas.gov.bs/wps/portal/public/gov/government/contacts/agencies/government%20corporations/gaming%20board/'>Bahamian Gaming Board</a>.</p>";
	?>
		<p>You will be redirected in <span id="counter">30</span> second(s).</p>
		<script type="text/javascript">
		function countdown() {
		    var i = document.getElementById('counter');
		    i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
		</script>
	<?php 
		echo "</div></body></html>";
		$session->logout();
		header( "refresh:30;url='".ROOTPATH."/locations.php'" ); 
		die();
	}elseif($customer_info['is_disabled'] == 1){
		// see if account exists but was disabled
		echo "<html><head><link rel='stylesheet' type='text/css' href='/css/default.css' /></head><body class='redirect_body'><div style='text-align:center;'><img src='/images/asw_logo.png?v=<?=IMAGE_VERSION?>' alt='logo'>";
		echo "<h3>Your account has been disabled.<br>";
		?>
		<p>You will be redirected in <span id="counter">30</span> second(s).</p>
		<script type="text/javascript">
		function countdown() {
		    var i = document.getElementById('counter');
		    i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
		</script>
	<?php 
		$session->logout();
		echo "</div></body></html>";
		header( "refresh:30;url='".ROOTPATH."/locations.php'" ); 
		die();
	}
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title><?php if(isset($page_title)){echo $page_title;}else{echo "CSCLotto.com - DING DING DING!";}?></title>
		
		<link rel="icon" type="image/x-icon" href="/images/favicon.ico?v=<?=IMAGE_VERSION?>" />
		
		<meta charset="utf-8"/>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<meta name="description" content="Online Gaming Website - Play 2, 3, 4 Ball and Fruity, Serpents, Luck Pets etc." />
		<meta name="keywords" content="csclotto, casino, lottery, numbers, fruity slots, serpents treasure, pirates revenge, island luck, asue draw, whatfall" />

		<!-- Modernizr: Polyfill Feature Detection -->
		<script src="/lib/assets/modernizr/polyfills/Placeholders.min.js"></script>
		
		<!-- jQuery -->
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery/jquery-1.11.0.js"></script>
		
		<!-- jQuery UI -->
		<script type="text/javascript" language="javascript" src="/lib/assets/jquery-ui-1.11.4/jquery-ui.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/lib/assets/jquery-ui-1.11.4/jquery-ui.min.css">
		<link rel="stylesheet" type="text/css" href="/lib/assets/jquery-ui-1.11.4/jquery-ui.theme.min.css">
		
		<script type="text/javascript">
			// fix compatibility issues with same class names in jquery ui and bootstrap
			$.widget.bridge('uitooltip', $.ui.tooltip);
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		
		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap.min.css" media="screen">
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/bootstrap-buttons.min.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/modern-business.css" />
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap3/css/tooltips.css" />
		<!-- <link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-xl/BootstrapXL.css" /> -->
		
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/jqBootstrapValidation.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap3/js/tooltips.js"></script>
		
		<!-- Bootstrap FormValidation Plugin (http://formvalidation.io/) -->
		<link rel="stylesheet" href="/lib/assets/formvalidation-0.6.2/dist/css/formValidation.min.css">
		<script src="/lib/assets/formvalidation-0.6.2/dist/js/formValidation.min.js"></script>
		<script src="/lib/assets/formvalidation-0.6.2/dist/js/framework/bootstrap.min.js"></script>
		
		<!-- Bootstrap Select Plugin -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-select/dist/css/bootstrap-select.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
		
		<!-- Bootstrap Form Helper Plugin -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-formhelpers/css/bootstrap-formhelpers.min.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-formhelpers/js/bootstrap-formhelpers.min.js"></script>
		
		<!-- Bootstrap Date Range Picker Plugin (http://www.daterangepicker.com/) -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
		
		<!-- Bootstrap Toggle Plugin (http://www.bootstraptoggle.com/) -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/bootstrap-toggle/css/bootstrap-toggle.min.css">
		<script type="text/javascript" src="/lib/assets/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
		
		<!-- DataTables -->
		<!-- <link rel="stylesheet" type="text/css" href="/lib/assets/datatables/media/css/jquery.dataTables.css"> -->
		<link rel="stylesheet" type="text/css" href="/lib/assets/datatables/extensions/Plugins/integration/bootstrap/3/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="/lib/assets/datatables/extensions/Responsive/css/dataTables.responsive.css">
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/media/js/jquery.dataTables.js"></script>
		<script type="type/javascript" language="javascript" src="/lib/assets/datatables/extensions/TableTools/js/dataTables.tableTools.js"></script>
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/extensions/Plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
		
		<!-- High Charts -->
		<script src="/lib/assets/highstock-2.1.5/js/highstock.js"></script>
		<script src="/lib/assets/highstock-2.1.5/js/modules/exporting.js"></script>
		
		<!-- DataTables Responsive Plugin (have to load it here after main datatables js finishes loading -->
		<script type="text/javascript" language="javascript" src="/lib/assets/datatables/extensions/Responsive/js/dataTables.responsive.js"></script>
		
		<!-- Modernizr: For HTML5/CSS3 Feature Detection -->
		<script src="/lib/assets/modernizr/modernizr.custom_min.js"></script>
		
		<!-- Winnings Pullout Menu -->
		<script src="/js/winnings_pullout.js?v=<?=JS_VERSION?>"></script>
		<script src="/lib/assets/mediaplugin/build/mediaelement-and-player.min.js"></script>
		<link rel="stylesheet" href="/lib/assets/mediaplugin/build/mediaelementplayer.css" />
		
		<!-- Bootstrap DateTimePicker (http://eonasdan.github.io/bootstrap-datetimepicker/) -->
		<script type="text/javascript" src="/lib/assets/moment/moment.js"></script>
		<script type="text/javascript" src="/lib/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
		<link rel="stylesheet" href="/lib/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" />
		
		<!-- BootBox Alerts -->
		<script type="text/javascript" src="/lib/assets/bootbox/bootbox.min.js"></script>
		
		<!-- Phone# Digit Validation -->
		<script type="text/javascript" src="/js/phonenumber.js?v=<?=JS_VERSION?>"></script>
		
		<!-- AJAX Push -->                
		<script src="/lib/framework/push_ajax.js"></script>
		
		<!-- Live Chat -->
		<script src="/js/live_chat.js?v=<?=JS_VERSION?>"></script>
		
		<?php
			$q = "SELECT value FROM settings WHERE setting='has_geolocation'";
			$setting = $db->queryOneRow($q);

			if($setting['value']==1){
		?>
			<!-- Security -->
			 <script src="/js/security.php"></script> 
		<?php
			}		
		?>
		
		<!-- Main UI -->
		<link rel="stylesheet" type="text/css" href="/css/default.css" />
		
		<script>
			$(function () {
				// initialize inline datetimepickers
				$('.dtp-inlne').datetimepicker({
					inline: true,
					sideBySide: true
				});
				
				// initialize datepickers
				$('.dp').datetimepicker({
					showTodayButton: true,
					format: 'MM/DD/YYYY'
				});
				
				// initialize datetimepickers
				$('.dtp').datetimepicker({
					showTodayButton: true,
					format: 'MM/DD/YYYY h:mm A'
				});
			});
			$(document).ready(function(){
				//$.fn.bootstrapBtn = $.fn.button.noConflict();
				
				Modernizr.load({
					test: Modernizr.placeholder,
					nope: '/lib/assets/modernizr/polyfills/Placeholders.min.js'
				});
				
				// Enable Bootstrap Select 
				$('.selectpicker').selectpicker();
				
				// thumbnail hover image changer
				$('.thumbnail').hover(
					function(){
						$(this).find('.thumbnail-caption').slideDown(250); //.fadeIn(250)
					},
					function(){
						$(this).find('.thumbnail-caption').slideUp(250); //.fadeOut(250)
					}
				);
			});
			
			
			
			
			
			
			
			//Get customer  when making a customer card
	$(document).on("blur", "#curpass", function(){
		//ajax for the balance information
		if($("#curpass").val() != ""){
			$.ajax({
				url: "ajax/check_pass.php",
				data: {action: "check_pass", curpass: $("#curpass").val() ,email : $('#email').val()},
				method: "POST"
			}).done(function( data ) {
				var info = JSON.parse(data);
				
				if(info.success != "true"){					
					$("#curpass_error").html(info.errors);
					$("#curpass_error").slideDown("fast");	
				}
				else{
				$("#curpass_error").hide();
				
				}
			});
		}else{
			$("#curpass_error").html("You must enter a Current Password!<br>");
			$("#curpass_error").slideDown("fast");
		}
	});
		
		
		$(document).on("blur", "#newpass", function(){
		//ajax for the balance information
		if($("#newpass").val() == ""){			
		$("#curpass_error").html("You must enter a New Password!<br>");
		$("#curpass_error").slideDown("fast");
		}
	});			
		$(document).on("blur", "#conf_newpass", function(){
		//ajax for the balance information
		
		if($("#conf_newpass").val() == ""){			
		$("#curpass_error").html("You must enter a Confirm Password!<br>");
		$("#curpass_error").slideDown("fast");
		}
		else{	
		
		if($("#newpass").val() != $("#conf_newpass").val()){		
		$("#curpass_error").html("Confirm Password did not match with New Password!<br>");
		$("#curpass_error").slideDown("fast");
		}
		}
		
	});
			
			
			
			
		</script>
	</head>
	<body style="background-color: <?php if(isset($bg_color)){echo $bg_color;}else{echo "#fff";}?>">
		<!-- BEGIN LivePerson Monitor. -->
		<script type="text/javascript"> window.lpTag=window.lpTag||{};if(typeof window.lpTag._tagCount==='undefined'){window.lpTag={site:'46386072'||'',section:lpTag.section||'',autoStart:lpTag.autoStart===false?false:true,ovr:lpTag.ovr||{},_v:'1.5.1',_tagCount:1,protocol:location.protocol,events:{bind:function(app,ev,fn){lpTag.defer(function(){lpTag.events.bind(app,ev,fn);},0);},trigger:function(app,ev,json){lpTag.defer(function(){lpTag.events.trigger(app,ev,json);},1);}},defer:function(fn,fnType){if(fnType==0){this._defB=this._defB||[];this._defB.push(fn);}else if(fnType==1){this._defT=this._defT||[];this._defT.push(fn);}else{this._defL=this._defL||[];this._defL.push(fn);}},load:function(src,chr,id){var t=this;setTimeout(function(){t._load(src,chr,id);},0);},_load:function(src,chr,id){var url=src;if(!src){url=this.protocol+'//'+((this.ovr&&this.ovr.domain)?this.ovr.domain:'lptag.liveperson.net')+'/tag/tag.js?site='+this.site;}var s=document.createElement('script');s.setAttribute('charset',chr?chr:'UTF-8');if(id){s.setAttribute('id',id);}s.setAttribute('src',url);document.getElementsByTagName('head').item(0).appendChild(s);},init:function(){this._timing=this._timing||{};this._timing.start=(new Date()).getTime();var that=this;if(window.attachEvent){window.attachEvent('onload',function(){that._domReady('domReady');});}else{window.addEventListener('DOMContentLoaded',function(){that._domReady('contReady');},false);window.addEventListener('load',function(){that._domReady('domReady');},false);}if(typeof(window._lptStop)=='undefined'){this.load();}},start:function(){this.autoStart=true;},_domReady:function(n){if(!this.isDom){this.isDom=true;this.events.trigger('LPT','DOM_READY',{t:n});}this._timing[n]=(new Date()).getTime();},vars:lpTag.vars||[],dbs:lpTag.dbs||[],ctn:lpTag.ctn||[],sdes:lpTag.sdes||[],ev:lpTag.ev||[]};lpTag.init();}else{window.lpTag._tagCount+=1;} </script>
		<!-- END LivePerson Monitor. -->
		
		<?php include_once("google_analytics.php") ?>
	
		<div id="ajax-notifications-contain" class="container" style="text-align: center; position: fixed; top: 20px; left: 40px; width: 35%; z-index:9999;"></div>

<?php
	// include 2FA Code
	include($_SERVER['DOCUMENT_ROOT'].'/lib/framework/2fa_modal.php');

	if(!$session->logged_in){
		if(isset($_GET['forgotpass'])){
			include 'forgotpass.php';
		}
	}

	// force password change
	if($session->userinfo['force_password_change'] != 0){
?>	
	<link rel="stylesheet" type="text/css" href="/admin/css/global.css?v=<?=CSS_VERSION?>" />
	<div class="container" style="text-align: center; width: 55%;">
		<img src="/images/asw_logo.png?v=<?=IMAGE_VERSION?>"></img>
		<br><br>
		<div class="panel panel-default" style="padding-top: 40px; padding-bottom: 40px;">
			<div class="panel-body">	
				<form action="lib/framework/login.php" method="POST" style="padding-left: 30px;">
					<h2 class="form-group-heading">Required Password Change</h2>
					<br>
					<div class="form-group">
						<label for="curpass" class="col-sm-4 control-label" style="text-align:left">Current Password</label>
						<div class="col-sm-6">
							<input id="curpass" name="curpass" type="password" class="form-control" placeholder="Current Password" required>
							<?php echo $form->error("curpass"); ?>
						</div>
					</div>		
					<br><br>
					<div class="form-group">
						<label for="newpass" class="col-sm-4 control-label" style="text-align:left">New Password</label>
						<div class="col-sm-6">
							<input id="newpass" name="newpass" type="password" class="form-control" placeholder="New Password" required>
							<?php echo $form->error("newpass"); ?>
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<label for="conf_newpass" class="col-sm-4 control-label" style="text-align:left">Confirm New Password</label>
						<div class="col-sm-6">
							<input id="conf_newpass" name="conf_newpass" type="password" class="form-control" placeholder="Confirm New Password" required>
							<?php echo $form->error("newpass"); ?>
						</div>
					</div>	
					<br><br><br>
					<input type="hidden" name="subedit" value="1">
					<input type='hidden' name='referrer' value='<?php echo str_replace('action=logout', '', $_SERVER['REQUEST_URI']); ?>'>
					
					<div class="form-group">
						<div class="col-sm-12">
							<button class="btn btn-large btn-primary" type="submit">Submit</button>
						</div>
					</div>
				</form>
				<br>
				<div class="col-sm-12">
					<div id="curpass_error" class="alert alert-danger" role="alert" style="display:none;">
							
					</div>
				</div>
			</div>
		</div>
    </div> <!-- /container -->
<?php
		include("footer.php");
		die();
	}
?>
