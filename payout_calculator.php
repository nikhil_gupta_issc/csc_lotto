<div class="panel panel-default" style="color:#fdc217; margin-bottom: 0px;">
	<div class="panel-body" style="background: #29052b;box-shadow: inset 1px 1px 48px 9px #bd0dab;">
		<div class="col-md-12" style="margin-top: -20px; text-align: center; color: white;">
			<h3>Payout Calculator</h3>
		</div>
		
		<div class="alert alert-danger col-md-12" id="calc_error" style="display:none;" role="alert">
			
		</div>
		
		<div class="col-md-3">
			<h5 style="white-space: nowrap;"># Played</h5>
			<input class="form-control" type="text" id="calc_number_bet">
		</div>
		<div class="col-md-4">
			<h5>S/B</h5>
			<select class="form-control" id="calc_sb">
				<option value="S">Straight</option>
				<option value="B">Boxed</option>
			</select>
		</div>
		<div class="col-md-5">
			<h5 style="white-space: nowrap;">Ticket Type</h5>
			<select class="form-control" id="calc_ticket_type">
				<?php
					$ticket_types = $db->query("SELECT DISTINCT name FROM payout_scheme");
					foreach($ticket_types as $ticket_type){
						echo "<option value='".$ticket_type['name']."'>".$ticket_type['name']."</option>";
					}
				?>
			</select>
		</div>
		
		<div class="col-md-4">
			<h5 style="white-space: nowrap;">Bet Amount</h5>
			<input class="form-control" type="text" id="calc_bet_amount">
		</div>
		<div class="col-md-3" style="padding-top: 35px">
			<button class="btn btn-primary" id="calculate_payout">Calculate</button>
		</div>
		
		<div class="col-md-5" style="text-align: center; color: white;">
			<h5 style="white-space: nowrap;">You Win:</h5>
			<h4 id="calc_win_amount">$0.00</h4>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$(document).on("blur", "#calc_bet_amount", function(){
		if($(this).val()!=""){
			var formatted_straight = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_straight.toFixed(2));
		}
	});

	$(document).on("click", "#calculate_payout", function(){
		if($("#calc_bet_amount").val()=="$NaN"){
			$("#calc_error").html("<p>Please enter a valid bet amount</p>");
			$("#calc_error").slideDown("fast");
		}else if($("#calc_number_bet").val().length<2 || $("#calc_number_bet").val().length>5){
			$("#calc_error").html("<p>Please enter a 2, 3, 4, or 5 ball number</p>");
			$("#calc_error").slideDown("fast");
		}else{
			$.ajax({
				url: "/ajax/payout_calculator.php",
				data: {ticket_type:$("#calc_ticket_type").val(), sb:$("#calc_sb").val(), number:$("#calc_number_bet").val(), amount:$("#calc_bet_amount").val()},
				method: "POST"
			}).done(function( data ) {
				$("#calc_error").fadeOut("fast");
				$("#calc_win_amount").text(data);
			});
		}
	});
});
</script>