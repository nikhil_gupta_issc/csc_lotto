0<?php
	include("config.php");
	include("header.php");	
	include('page_top.php');
	
	$id = isset($_GET['id']) ? $_GET['id'] : null;
	
	$q = "SELECT * FROM `disputes` WHERE `id`=%i";
	$info = $db->queryOneRow($q, array($id));
	
	$permission = false;
	if($session->logged_in){
		if($session->userinfo['id'] == $info['user_id']){
			$permission = true;
		}
	}else{
		$token = isset($_GET['token']) ? $_GET['token'] : null;
		if($info['token'] == $token){
			$permission = true;
		}
	}
	
	if($permission === false){
?>
		<div class="well clearfix">
			<div class="col-md-12" style="margin-bottom:20px;">
				<img class="img_grow_wide" src="/images/banner_account.jpg">
			</div>
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">View Dispute</h3>
					</div>
					<div class="panel-body">
						You are not permitted to view this dispute.  Please <a href='/locations.php'>contact us</a> if you believe you have received this message in error.
					</div>
				</div>
			</div>
	</div>
<?php
	}else{
?>
		<script>
			$(document).ready(function(){
				$(document).on("click", "#send_response", function(){
					$(this).prop("disabled",true);
					$.ajax({
						url: "/ajax/dispute.php",
						data: {
							action : "send_response",
							message : $("#dispute_message").val(),
							dispute_id : $("#dispute_id").val(),
							token : '<?php echo $_GET['token']; ?>'
						},
						method: "POST",
						dataType: "json"
					})
					.done(function(data){
						if(data.success == true){
							$("#dispute_table").children("tbody").append("<tr><td>"+$("#dispute_message").val()+"</td><td>Just Now</td></tr>");
							$("#dispute_message").val("");
							bootbox.alert("Your response was sent successfully.");
						}else{
							bootbox.alert(data.errors);
						}
						$("#send_response").prop("disabled",false);
					});
				});
			});
		</script>

		<div class="well clearfix">
			<div class="col-md-12" style="margin-bottom:20px;">
				<img class="img_grow_wide" src="/images/banner_account.jpg">
			</div>
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Dispute Details</h3>
					</div>
					<div class="panel-body">
						<table class="table table-striped" id="dispute_table">
							<thead>
								<tr>
									<th>Message</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$q = "SELECT * FROM `dispute_messages` WHERE `dispute_id`=%i ORDER BY `id` DESC";
									$messages = $db->query($q, array($id));
									
									foreach($messages as $message){
										$date_dt = new DateTime($message['created_on']);
										$date = $date_dt->format("m/d/Y h:i A");
								?>
									<tr>
										<td><?php echo $message['message']; ?></td>
										<td><?php echo $date; ?></td>
									<tr>
								<?php
									}
								?>
							</tbody>
						</table>
						<form class="form-horizontal">
							<input type="hidden" id="dispute_id" value="<?php echo $_GET['id']; ?>">
							<div class="form-group">
								<label for="dispute" class="col-sm-1 control-label">Message</label>
								<div class="col-sm-11">
									<textarea class="form-control" id="dispute_message" placeholder="Message"></textarea>
								</div>
							</div>
						</form>
						<button class="pull-right btn btn-magenta" id="send_response" type="button">Send Response</button>
					</div>
				</div>
			</div>
		</div>
<?php
	}
	include("footer.php");
?>