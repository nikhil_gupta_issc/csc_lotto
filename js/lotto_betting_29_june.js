$(document).ready(function(){
	
	//Used to hold the button location while the confirmation window is open
	//when removing a bet.
	var curr_remove_button;
	
	//Define a set that contains all of the numbers that are included in the cart
	var choices_set = {};
	
	//Formats money strings.
	function pad (str, max) {
		return str.length < max ? pad("0" + str, max) : str;
	}

	function show_hide_games(){
		$.ajax({
			url: "/ajax/lotto_betting.php",
			data: {action: "get_inactive_games"},
			method: "POST"
		})
		.done(function( data ) {
			//Split all of the game ids into an array
			var ids = data.split(",");

			//Check to see if the disabled games are to be disabled or hidden
			if($("#show_inactive").is(':checked')){
				for(var y=0;y<ids.length;y++){
					if($("td input[id='game_"+ids[y]+"']").hasClass("fav_check") == false){
						$("td input[id='game_"+ids[y]+"']").removeClass();
						$("td input[id='game_"+ids[y]+"']").prop("checked", false);
						$("td input[id='game_"+ids[y]+"']").attr("disabled", true);
						$("td input[id='game_"+ids[y]+"']").parent('td').siblings('td').children('p').addClass('text-muted');
					}
				}
			}else{
				for(var y=0;y<ids.length;y++){
					if($("td input[id='game_"+ids[y]+"']").hasClass("fav_check") == false){
						$("td input[id='game_"+ids[y]+"']").removeClass();
						$("td input[id='game_"+ids[y]+"']").prop("checked", false);
						$("td input[id='game_"+ids[y]+"']").attr("disabled", true);
						$("td input[id='game_"+ids[y]+"']").parent('td').siblings('td').children('p').addClass('text-muted');
						$("td input[id='game_"+ids[y]+"']").parent('td').parent('tr').fadeOut("fast");
					}
				}
			}
		});
	}
	
	//Initialize hiding or disabling the games.
	show_hide_games();
	
	//The following will check every 10 seconds to make sure that the game is still active
	setInterval(function () {
		show_hide_games
	}, 10000);
	
	//Initialize whether the games show or not
	if($("#show_inactive").is(':checked')){
		$("td input[type='checkbox']:disabled").parent('td').parent('tr').fadeIn("fast");
	}else{
		$("td input[type='checkbox']:disabled").parent('td').parent('tr').fadeOut("fast");
	}

	//Handle the toggle being clicked to autohide games
	$(document).on("change", "#show_inactive", function(){		
		if($("#show_inactive").is(':checked')){
			//Remember that the user prefers this setting
			$.ajax({
				url: "/ajax/lotto_betting.php",
				data: {action: "show_inactive", value:"1"},
				method: "POST"
			});
			$("td input[type='checkbox']:disabled").parent('td').parent('tr').fadeIn("fast");
		}else{
			//Remember that the user prefers this setting
			$.ajax({
				url: "/ajax/lotto_betting.php",
				data: {action: "show_inactive", value:"0"},
				method: "POST"
			});
			$("td input[type='checkbox']:disabled").parent('td').parent('tr').fadeOut("fast");
		}
	});
	
	// set pick max length and placeholder based on num balls of game
	$(document).on("change", "#fav_game_id", function(){		
		$.ajax({
			url: "/ajax/lotto_betting.php",
			data: {
				action: "get_ball_count",
				game_id: $('#fav_game_id').val()
			},
			method: "POST"
		})
		.done(function( ball_count ) {
			$('#fav_pick').attr('maxlength', ball_count);
			var placeholder = "1234567";
			$('#fav_pick').attr('placeholder', placeholder.substring(0, ball_count));
		});
	});
	
	$(document).on("click", "#confirm_advance_play", function(){
		$.ajax({
			dataType: "json",
			url: "/ajax/lotto_betting.php",
			data: {
				action: "schedule_advance_play",
				total: $('#advance_total').val(),
				fav_bet_id: $('#favorite_bet').val(),
				pick: $('#bet_string').val(),
				num_bets: $('#number_of_drawings').val(),
				bet_amount: $('#advance_amount').val().replace("$","")
			},
			method: "POST"
		})
		.done(function( response ){
			if(response.success == "false"){
				$("#advance_play_errors").html(response.errors);
				$("#advance_play_errors").slideDown("fast");
			}else{
				$("#advance_play_errors").html("");
				$("#advance_play_errors").slideUp("fast");
				$("#schedule_advance_play_modal").modal("hide");
				bootbox.alert("Advance Play Scheduled!");
				window.location.href = '../lottery.php?tab=scheduled_bets';
			}
		});
	});

	$(document).on("change", "#advance_amount", function(){
		if(isNaN($(this).val().replace("$",""))){
			$("#advance_play_errors").html("Invalid Amount");
		}else{
			if(!isNaN($("#number_of_drawings").val())){
				var num_drawings = Number($("#number_of_drawings").val(),10);
				var amount = Number($(this).val().replace("$",""),10);
				var total = amount * num_drawings;
				$("#advance_total").val(total.toFixed(2));
			}
		}
	});
	
	$(document).on("change", "#number_of_drawings", function(){
		if(isNaN($(this).val())){
			$("#advance_play_errors").html("Invalid Number of Drawings");
		}else{
			if(!isNaN($("#advance_amount").val().replace("$",""))){
				var amount = Number($("#advance_amount").val().replace("$",""),10);
				var num_drawings = Number($(this).val(),10);
				var total = amount * num_drawings;
				$("#advance_total").val(total.toFixed(2));
			}
		}
	});
	
	$(document).on("blur", "#advance_amount", function(){
		if($(this).val()!=""){
			var formatted_straight = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_straight.toFixed(2));
		}
	});
	
	$(document).on("click", "#schedule_advance_play", function(){
		$('#schedule_advance_play_modal').modal("show");
	});
	
	$(document).on("click", "#button_clear_cart", function(){
		$('#clear_confirmation_modal').modal("show");
	});
	
	$(document).on("click", "#add_fav_bet", function(){
		$('#add_fav_bet_modal').modal("show");
	});
	
	$(document).on("click", "#fav_bet_confirm", function(){
		
		$.ajax({
			url: "/ajax/lotto_betting.php",
			dataType : 'json',
			data: {
				action: "add_fav_bet",
				fav_game_id: $('#fav_game_id').val(),
				fav_pick: $('#fav_pick').val(),
				fav_boxed: $('#fav_boxed').val()
			},
			method: "POST"
		})
		.done(function( response ){
			if(response.success == "true"){
				$('#add_fav_bet_modal').modal("hide");
				window.location.href = '../lottery.php?tab=fav_bets';
			}
		});
	});
	
	// remove fav bet
	$(document).on("click", ".remove_fav", function(){
		// get fav bet id
		var fav_id = this.id.replace('remove_fav_', '');
		
		var tr = $(this).closest('tr');
		
		$.ajax({
			url: "/ajax/lotto_betting.php",
			dataType: 'json',
			data: {
				action: "remove_fav_bet",
				fav_bet_id: fav_id
			},
			method: "POST"
		})
		.done(function( response ){
			if(response.success == "true"){
				$('#fav_row_'+fav_id).remove();
			}
		});
	});
	
	$(document).on("click", "#clear_confirm", function(){
		//Remove all bets and get rid of the total
		$('#bet_table tbody').html("");
		$('#total').text("0.00");
		
		//Clear out errors
		$('#error_reporting').text("").slideUp("fast");
		
		//Clear counts for choices
		choices_set = {};
		
		//Hide modal
		$('#clear_confirmation_modal').modal("hide");
	});
	
	$(document).on("blur", "#straight_amount_text", function(){
		if($(this).val()!=""){
			var formatted_straight = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_straight.toFixed(2));
		}
	});
	
	$(document).on("blur", "#boxed_amount_text", function(){
		if($(this).val()!=""){
			var formatted_boxed = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+formatted_boxed.toFixed(2));
		}
	});
	
	$(document).on("blur", "#fav_bet_amount", function(){
		if($(this).val()!=""){
			var fav_bet_amount = parseFloat($(this).val().replace("$",""),10);
			$(this).val("$"+fav_bet_amount.toFixed(2));
		}
	});
	
	//Start the check all boxes functionality
	$(document).on("click", "#all_early_2_check", function(){
		if($(this).prop('checked')){
			$('.early_2').prop('checked', true);
		}else{
			$('.early_2').prop('checked', false);
		}
	});
	
	$(document).on("click", "#all_late_2_check", function(){
		if($(this).prop('checked')){
			$('.late_2').prop('checked', true);
		}else{
			$('.late_2').prop('checked', false);
		}
	});
	
	$(document).on("click", "#all_early_5_check", function(){
		if($(this).prop('checked')){
			$('.early_5').prop('checked', true);
		}else{
			$('.early_5').prop('checked', false);
		}
	});
	
	$(document).on("click", "#all_late_5_check", function(){
		if($(this).prop('checked')){
			$('.late_5').prop('checked', true);
		}else{
			$('.late_5').prop('checked', false);
		}
	});
	
	$(document).on("click", "#all_early_6_check", function(){
		if($(this).prop('checked')){
			$('.early_6').prop('checked', true);
		}else{
			$('.early_6').prop('checked', false);
		}
	});
	
	$(document).on("click", "#all_late_6_check", function(){
		if($(this).prop('checked')){
			$('.late_6').prop('checked', true);
		}else{
			$('.late_6').prop('checked', false);
		}
	});
	
	$(document).on("click", "#all_fav_bets", function(){
		if($(this).prop('checked')){
			$('.fav_check').prop('checked', true);
		}else{
			$('.fav_check').prop('checked', false);
		}
	});	
	
	//Start Button hiding and showing on tab changes
	$(document).on("click", '#2_ball_tab', function(){
		$('#button_qk2').show();
		$('#button_qk5').hide();
		$('#button_qk6').hide();
		$('#button_add_cart').val("2");
		$('#choose_numbers_panel').show();
	});
	
	$(document).on("click", '#5_ball_tab', function(){
		$('#button_qk2').hide();
		$('#button_qk5').show();
		$('#button_qk6').hide();
		$('#button_add_cart').val("5");
		$('#choose_numbers_panel').show();
	});
	
	$(document).on("click", '#6_ball_tab', function(){
		$('#button_qk2').hide();
		$('#button_qk5').hide();
		$('#button_qk6').show();
		$('#button_add_cart').val("6");
		$('#choose_numbers_panel').show();
	});
	
	$(document).on("click", '#fav_bets_tab', function(){
		$('#button_qk2').hide();
		$('#button_qk3').hide();
		$('#button_qk4').hide();
		$('#button_qk5').hide();
		$('#button_qk6').hide();
		$('#choose_numbers_panel').hide();
	});
	
	$(document).on("click", '#scheduled_bets_tab', function(){
		$('#button_qk2').hide();
		$('#button_qk3').hide();
		$('#button_qk4').hide();
		$('#button_qk5').hide();
		$('#button_qk6').hide();
		$('#choose_numbers_panel').hide();
	});
	
	$(document).on("click", '#how_to_play_tab', function(){
		$('#button_qk2').hide();
		$('#button_qk3').hide();
		$('#button_qk4').hide();
		$('#button_qk5').hide();
		$('#button_qk6').hide();
		$('#choose_numbers_panel').hide();
	});
        var is_datatable = 0;
        $(document).on("click", '#lotto_history_tab', function(){
		$('#button_qk2').hide();
		$('#button_qk3').hide();
		$('#button_qk4').hide();
		$('#button_qk5').hide();
		$('#button_qk6').hide();
		$('#choose_numbers_panel').hide();
                
                if(is_datatable == 0){
		        var lot_data_table = $('#lot_datatable').DataTable({
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					"ajax": "/ajax/datatables/bets.php",
					"columns": [
						{ "data": "purchase_date" },
						{ "data": "draw_time" },
						{ "data": "game_id" },
						{ "data": "ticket_number" },
						{ "data": "ball_string" },
						{ "data": "winning_draw" },
						{ "data": "bet_type" },
						{ "data": "bet_amount" },
						{ "data": "loyality_points" },
						{ "data": "payout" },
						{ "data": "status" }
					],
					"order": [[0, "desc"]],
					"dom": 'lfrtp',
		                        "fnInitComplete": function (oSettings, json) {
		                            is_datatable = 1;
		                       }
				});
                 }
	});

        var is_ticket_datatable = 0;
        $(document).on("click", '#ticket_history_tab', function(){
		$('#button_qk2').hide();
		$('#button_qk3').hide();
		$('#button_qk4').hide();
		$('#button_qk5').hide();
		$('#button_qk6').hide();
		$('#choose_numbers_panel').hide();
                
                if(is_ticket_datatable == 0){
		        var ticket_data_table = $('#ticket_datatable').DataTable({
					"processing": true,
					"serverSide": true,
					"deferRender": true,
					"ajax": "/ajax/datatables/ticket_bets.php",
					"columns": [
						{ "data": "purchase_date" },
						{ "data": "draw_time" },
						{ "data": "game_id" },
						{ "data": "ticket_number" },
						{ "data": "ball_string" },
						{ "data": "winning_draw" },
						{ "data": "bet_type" },
						{ "data": "bet_amount" },
						{ "data": "payout" },
						{ "data": "status" }
					],
					"order": [[0, "desc"]],
					"dom": 'lfrtp',
		                        "fnInitComplete": function (oSettings, json) {
		                            is_ticket_datatable = 1;
		                       }
				});
                 }
	});
	
	//Start Random number generators for quick buttons
	$(document).on("click", '#button_qk2', function(){
		var new_rand = pad(Math.round(Math.random()*99).toString(), 2);
		if(typeof choices_set[new_rand] == 'undefined'){
			$('#number_text').val(new_rand);
		}else{
			$(this).trigger("click");
		}
	});
	
	$(document).on("click", '#button_qk3', function(){
		var new_rand = pad(Math.round(Math.random()*999).toString(), 3);
		if(typeof choices_set[new_rand] == 'undefined' || choices_set[new_rand]==0){
			$('#number_text').val(new_rand);
		}else{
			$(this).trigger("click");
		}
	});
	
	$(document).on("click", '#button_qk4', function(){
		var new_rand = pad(Math.round(Math.random()*9999).toString(), 4);
		if(typeof choices_set[new_rand] == 'undefined' || choices_set[new_rand]==0){
			$('#number_text').val(new_rand);
		}else{
			$(this).trigger("click");
		}
	});
	
	$(document).on("click", '#button_qk5', function(){
		var new_rand = pad(Math.round(Math.random()*99999).toString(), 5);
		if(typeof choices_set[new_rand] == 'undefined'){
			$('#number_text').val(new_rand);
		}else{
			$(this).trigger("click");
		}
	});
	
	$(document).on("click", '#button_qk6', function(){
		var new_rand = pad(Math.round(Math.random()*999999).toString(), 6);
		if(typeof choices_set[new_rand] == 'undefined'){
			$('#number_text').val(new_rand);
		}else{
			$(this).trigger("click");
		}
	});
	
	$(document).on("keyup", '#number_text', function(){
		$("#"+$(this).val().length+"_ball_tab").trigger("click");
	});

	$(document).on("click", '#purchase_bets_button', function(){
		//Make the button go into a loading state
		var button_state = $(this).button("loading");
		
		var tbl = $('#bet_table tbody tr').get().map(function(row) {
			return $(row).find('td').get().map(function(cell) {
				if($(cell).html().indexOf("<button") > -1){
					return "";
				}else{
					return $(cell).html();	
				}
			});
		});
		
		$.ajax({
			url: "/ajax/lotto_betting.php",
			data: {
				action: "insert",
				data: tbl
			},
			method: "POST"
		})
		.done(function(data){
			if(data.trim()!=""){
				$('#error_reporting').html(data).slideDown("fast");
				button_state.button('reset');
			}else{
				bootbox.alert("Ticket purchased successfully!");
				$.each($("td input:checked"), function(){
					$(this).prop('checked', false);
				});
				$("#bet_table tbody").html("");
				$("#total").text("0.00");
				$("#number_text").val("");
				$("#straight_amount_text").val("");
				$("boxed_amount_text").val("");
				button_state.button('reset');
			}
		});
	});

        $("#number_text").keydown(function (e) {
               
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
		     // Allow: Ctrl+A, Command+A
		    (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
		     // Allow: home, end, left, right, down, up
		    (e.keyCode >= 35 && e.keyCode <= 40)) {
		         // let it happen, don't do anything
		         return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		    e.preventDefault();
		}
       });
	
	$(document).on("click", '.remove_bet_button', function(){
	
		curr_remove_button = this;
		$('#remove_confirmation_modal').modal("show");
                $('#remove_confirm').removeAttr("disabled");
	});
	
	$(document).on("click", '#remove_confirm', function(){
                $(this).attr("disabled", true);
		//Update total
		var curr_subtotal = parseFloat($(curr_remove_button).parent('td').siblings('.amount').children('span').text());
		//alert(curr_subtotal);
		var curr_total = parseFloat($('#total').text());
		//alert(curr_total);
		var new_total = curr_total - curr_subtotal;
		$('#total').text(new_total.toFixed(2));
		
		//Hide modal
		$('#remove_confirmation_modal').modal("hide");
		
		//Hide errors
		$('#error_reporting').text("").slideUp("fast");
		
		//Decrement count for the choice number
		choices_set[parseInt($(curr_remove_button).parent('td').parent('tr').children('td:first-child').text())]-=1;
		
		//Remove Row
		$(curr_remove_button).parent('td').parent('tr').fadeOut('fast', function(){
			$(this).remove();
		});
                
	});

         $("#straight_amount_text,#boxed_amount_text").keydown(function (e) {
               
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13,110,190]) !== -1 ||
		     // Allow: Ctrl+A, Command+A
		    (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
		     // Allow: home, end, left, right, down, up
		    (e.keyCode >= 35 && e.keyCode <= 40)) {
		         // let it happen, don't do anything
		         return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		    e.preventDefault();
		}
       });
	
	// handle adding fav bets to cart
	$(document).on("click", '#add_fav_to_cart', function(){
		var total = 0;
		
		var fav_bet_amount = Number($("#fav_bet_amount").val().replace("$",""),10);
		
		//Check for all checked boxes
		$.each($("td input:checked"), function(){
		var game_name = $(this).parent("td").siblings().eq(0).text();
			var pick = $(this).parent("td").siblings().eq(1).text();
			var boxed = $(this).parent("td").siblings().eq(2).text() == "Boxed" ? "B" : "S";
            
            if($(this).parent("td").siblings().eq(2).text()=='Straight & Boxed')
            {
				var is_boxed=2;
			}
			else
			{
				var is_boxed = $(this).parent("td").siblings().eq(2).text() == "Boxed" ? "1" : "0";
			}
			
				if(is_boxed==2)
				{
				 for(i=0;i<2;i++)
				 {	
                   var self = $(this);
                        $.ajax({
								url: "/ajax/lotto_betting.php",
								async:false,
								data: {
									action: "get_payout",
									pick: pick,
									bet_amount: fav_bet_amount,
									is_boxed:i
								},
								method: "POST"
							})
							.success(function(payout){
								if(i==0)
								{
									boxed='B';
								}	
								else
								{
									boxed='S';
								}
								$("#bet_table tbody").prepend("<tr style='height:6px;'><td>"+pick+"</td><td style='display: none;'>"+self.attr('id').replace("game_","")+"</td><td>"+game_name+"</td><td>"+boxed+"</td><td class='subtotal'>$<span>"+fav_bet_amount.toFixed(2)+"</span></td><td class='subtotal'>$<span>"+payout+"</span></td><td><button class='btn btn-danger remove_bet_button'>Delete</button></td>");
								if(typeof choices_set[$('#number_text').val()] == 'undefined'){
									choices_set[$('#number_text').val()]=1;
								}else{
									choices_set[$('#number_text').val()]+=1;
								}
							});
						
			//Verify the numbers added
			total += fav_bet_amount;
			
			if(typeof choices_set[$('#number_text').val()] == 'undefined'){
				choices_set[$('#number_text').val()]=1;
			}else{
				choices_set[$('#number_text').val()]+=1;
			}

			//Update the total
			$('#total').text(total.toFixed(2));
			}
		}
			else if(is_boxed==0 || is_boxed==1)
			{
		
				var self = $(this);
                        $.ajax({
								url: "/ajax/lotto_betting.php",
								data: {
									action: "get_payout",
									pick: pick,
									bet_amount: fav_bet_amount,
									is_boxed:is_boxed
								},
								method: "POST"
							})
							.success(function(payout){
								
								$("#bet_table tbody").prepend("<tr style='height:6px;'><td>"+pick+"</td><td style='display: none;'>"+self.attr('id').replace("game_","")+"</td><td>"+game_name+"</td><td>"+boxed+"</td><td class='subtotal'>$<span>"+fav_bet_amount.toFixed(2)+"</span></td><td class='subtotal'>$<span>"+payout+"</span></td><td><button class='btn btn-danger remove_bet_button'>Delete</button></td>");
								if(typeof choices_set[$('#number_text').val()] == 'undefined'){
									choices_set[$('#number_text').val()]=1;
								}else{
									choices_set[$('#number_text').val()]+=1;
								}
							});
						
			//Verify the numbers added
			total += fav_bet_amount;
			
			if(typeof choices_set[$('#number_text').val()] == 'undefined'){
				choices_set[$('#number_text').val()]=1;
			}else{
				choices_set[$('#number_text').val()]+=1;
			}

			//Update the total
			$('#total').text(total.toFixed(2));	
			}
		});
	});
	
	$(document).on("click", '#button_add_cart', function(){
		
		var found_error = false;
		var error_text = "";
		
		//Initialize the error division so it resets on each click.
		$('#add_cart_errors').text("").slideUp("fast");
		
		//Parse and format the money fields that will be compared and calculated
		var formatted_straight = Number($("#straight_amount_text").val().replace("$",""),10);
		var formatted_boxed = Number($("#boxed_amount_text").val().replace("$",""),10);
		var total = Number($('#total').text());
		var boxed=$("#boxed_amount_text").val().replace("$","");
		var straight=$("#straight_amount_text").val().replace("$","");
		
		//Check to make sure the number of characters in the numbers text box is correct
		if(parseInt($(this).val())=='2'){
			if($('#number_text').val().length<2 || $('#number_text').val().length>4){
				error_text = error_text + "Invalid number of balls chosen<br>";
				found_error = true;
			}
		}else if($('#number_text').val().length!=parseInt($(this).val())){
			error_text = error_text + "Invalid number of balls chosen<br>";
			found_error = true;
		}
		
		/* if((formatted_straight == 0) && (formatted_boxed==0)){
			error_text = error_text + "You must choose a bet amount<br>";
			found_error = true;
		}*/
		
		if(straight == '')
		{
			if(boxed == '')
			{
				error_text = error_text + "You must choose a bet amount<br>";
				found_error = true;
			}	
			
		}
		
		if(straight !=''  && (straight == 0 || straight == 0.00) ) 
		{
				error_text = error_text + "Straight amount should be greater than 0<br>";
				found_error = true;
		}
		
		if(boxed !=''  && (boxed == 0 || boxed == 0.00) ) 
		{
				error_text = error_text + "Boxed amount should be greater than 0<br>";
				found_error = true;
		
		}
		
		
		if(found_error == true){
			$('#add_cart_errors').html(error_text).slideDown("fast");
		}else{			
			//boolean used to determine if any houses were checked
			var houses_checked = false;
			
			//Check for all checked boxes
			$.each($("td input:checked"), function() {
				if($(this).attr('ball')==$("#button_add_cart").val()){
					//Update the boolean to say there was a box checked
					houses_checked = true;
					var id = $(this).attr('id').replace("game_","");
					var game_name = $(this).parent("td").next().text();
					
					//Verify the numbers added
					if($("#straight_amount_text").val() != ""){
						if(!isNaN(formatted_straight)){
							total += formatted_straight;
							$.ajax({
								url: "/ajax/lotto_betting.php",
								data: {
									action: "get_payout",
									pick: $('#number_text').val(),
									bet_amount: formatted_straight.toFixed(2),
									is_boxed: 0
								},
								method: "POST"
							})
							.success(function(payout){
							//console.log(formatted_straight);
								$("#bet_table tbody").prepend("<tr><td>"+$('#number_text').val()+"</td><td style='display: none;'>"+id+"</td><td>"+game_name+"</td><td>S</td><td class='subtotal amount'>$<span>"+formatted_straight.toFixed(2)+"</span></td><td class='subtotal'>$<span>"+payout+"</span></td><td><button class='btn btn-danger remove_bet_button'>Delete</button></td>");
								if(typeof choices_set[$('#number_text').val()] == 'undefined'){
									choices_set[$('#number_text').val()]=1;
								}else{
									choices_set[$('#number_text').val()]+=1;
								}
							});
						}else{
							$('#add_cart_errors').append("Invalid Straight Number<br>").slideDown("fast");
						}
					}
					if($("#boxed_amount_text").val() != ""){
						if(!isNaN(formatted_boxed)){
							total += formatted_boxed;
							$.ajax({
								url: "/ajax/lotto_betting.php",
								data: {
									action: "get_payout",
									pick: $('#number_text').val(),
									bet_amount: formatted_boxed.toFixed(2),
									is_boxed: 1
								},
								method: "POST"
							})
							.success(function(payout){
								$("#bet_table tbody").prepend("<tr><td>"+$('#number_text').val()+"</td><td style='display: none;'>"+id+"</td><td>"+game_name+"</td><td>B</td><td class='subtotal amount'>$<span>"+formatted_boxed.toFixed(2)+"</span></td><td class='subtotal'>$<span>"+payout+"</span></td><td><button class='btn btn-danger remove_bet_button'>Delete</button></td>");
								if(typeof choices_set[$('#number_text').val()] == 'undefined'){
									choices_set[$('#number_text').val()]=1;
								}else{
									choices_set[$('#number_text').val()]+=1;
								}
							});
						}else{
							$('#add_cart_errors').append("Invalid Boxed Number<br>").slideDown("fast");
						}
					}
					
					//Update the total
					$('#total').text(total.toFixed(2));
					
					
				}
			});
			
			//If there were no houses checked, throw an error
			if(!houses_checked){
				$('#add_cart_errors').append("You have not selected any houses<br>").slideDown("fast");
			}
		}
	});
	
	/* Start All Triples */
	
	$(document).on("click", '#button_all_triples', function(){
		
		var found_error = false;
		var error_text = "";
		
		//Initialize the error division so it resets on each click.
		$('#add_cart_errors').text("").slideUp("fast");
		
		//Parse and format the money fields that will be compared and calculated
		var formatted_straight = Number($("#straight_amount_text").val().replace("$",""),10);
		var formatted_boxed = Number($("#boxed_amount_text").val().replace("$",""),10);
		var total = Number($('#total').text());
		
		//Check to make sure the number of characters in the numbers text box is correct
		/*if(parseInt($(this).val())=='2'){
			if($('#number_text').val().length<2 || $('#number_text').val().length>4){
				error_text = error_text + "Invalid number of balls chosen<br>";
				found_error = true;
			}
		}else if($('#number_text').val().length!=parseInt($(this).val())){
			error_text = error_text + "Invalid number of balls chosen<br>";
			found_error = true;
		}*/
		
		
		
	/*	if((formatted_straight == 0) && (formatted_boxed==0)){
			error_text = error_text + "You must choose a bet amount<br>";
			found_error = true;
		}*/
		var boxed=$("#boxed_amount_text").val().replace("$","");
		var straight=$("#straight_amount_text").val().replace("$","");
		
		if(straight == '')
		{
			if(boxed == '')
			{
				error_text = error_text + "You must choose a bet amount<br>";
				found_error = true;
			}	
			
		}
		
		if(straight !=''  && (straight == 0 || straight == 0.00) ) 
		{
				error_text = error_text + "Straight amount should be greater than 0<br>";
				found_error = true;
		}
		
		if(boxed !=''  && (boxed == 0 || boxed == 0.00) ) 
		{
				error_text = error_text + "Boxed amount should be greater than 0<br>";
				found_error = true;
		
		}
		
		
		
			
		if(found_error == true){
			$('#add_cart_errors').html(error_text).slideDown("fast");
		}else{
			
			
	
			//boolean used to determine if any houses were checked
			var houses_checked = false;
			
			//Check for all checked boxes
			$.each($("td input:checked"), function() {
				
				
				if($(this).attr('ball')==$("#button_add_cart").val()){
					//Update the boolean to say there was a box checked
					houses_checked = true;
					var id = $(this).attr('id').replace("game_","");
					var game_name = $(this).parent("td").next().text();
					
					//Verify the numbers added
					
					if($("#straight_amount_text").val() != ""){
						
						if(!isNaN(formatted_straight)){
						
						var myArray = [ '000', '111', '222', '333', '444', '555', '666', '777', '888', '999' ];			
						for( i = 0, l = myArray.length; i < l; i++ ) {
						//alert(myArray[i]);
						//$('#number_text').val(myArray[i]);
							
							
							total += formatted_straight;
							$.ajax({
								url: "/ajax/lotto_betting.php",
								async:false,
								data: {
									action: "get_payout",
									pick:myArray[i],
									bet_amount: formatted_straight.toFixed(2),
									is_boxed: 0
								},
								method: "POST"
							})
							.success(function(payout){
							//console.log(formatted_straight);
								$("#bet_table tbody").prepend("<tr><td>"+myArray[i]+"</td><td style='display: none;'>"+id+"</td><td>"+game_name+"</td><td>S</td><td class='subtotal amount'>$<span>"+formatted_straight.toFixed(2)+"</span></td><td class='subtotal'>$<span>"+payout+"</span></td><td><button class='btn btn-danger remove_bet_button'>Delete</button></td>");
								if(typeof choices_set[myArray[i]] == 'undefined'){
									choices_set[myArray[i]]=1;
								}else{
									choices_set[myArray[i]]+=1;
								}
							});
						}
						}else{
							$('#add_cart_errors').append("Invalid Straight Number<br>").slideDown("fast");
						
						}
					}
					if($("#boxed_amount_text").val() != ""){
						if(!isNaN(formatted_boxed)){
							
						var myArray = [ '000', '111', '222', '333', '444', '555', '666', '777', '888', '999' ];			
						for( i = 0, l = myArray.length; i < l; i++ ) {
						//alert(myArray[i]);
						//$('#number_text').val(myArray[i]);	
							
							total += formatted_boxed;
							$.ajax({
								url: "/ajax/lotto_betting.php",
								async:false,
								data: {
									action: "get_payout",
									pick: myArray[i],
									bet_amount: formatted_boxed.toFixed(2),
									is_boxed: 1
								},
								method: "POST"
							})
							.success(function(payout){
								$("#bet_table tbody").prepend("<tr><td>"+myArray[i]+"</td><td style='display: none;'>"+id+"</td><td>"+game_name+"</td><td>B</td><td class='subtotal amount'>$<span>"+formatted_boxed.toFixed(2)+"</span></td><td class='subtotal'>$<span>"+payout+"</span></td><td><button class='btn btn-danger remove_bet_button'>Delete</button></td>");
								if(typeof choices_set[myArray[i]] == 'undefined'){
									choices_set[myArray[i]]=1;
								}else{
									choices_set[myArray[i]]+=1;
								}
							});
						}
						}else{
							$('#add_cart_errors').append("Invalid Boxed Number<br>").slideDown("fast");
						}
					}
					
					//Update the total
					$('#total').text(total.toFixed(2));
				}
					
				
			});
			
			//If there were no houses checked, throw an error
			if(!houses_checked){
				$('#add_cart_errors').append("You have not selected any houses<br>").slideDown("fast");
			}
		
	}
	});
	
	/* End All Triples */
	
	
	
	/* Start All hundread */
	
	$(document).on("click", '#button_add_hundread', function(){
		
		var found_error = false;
		var error_text = "";
		
		//Initialize the error division so it resets on each click.
		$('#add_cart_errors').text("").slideUp("fast");
		
		//Parse and format the money fields that will be compared and calculated
		var formatted_straight = Number($("#straight_amount_text").val().replace("$",""),10);
		var formatted_boxed = Number($("#boxed_amount_text").val().replace("$",""),10);
		var total = Number($('#total').text());
		
		//Check to make sure the number of characters in the numbers text box is correct
		/*if(parseInt($(this).val())=='2'){
			if($('#number_text').val().length<2 || $('#number_text').val().length>4){
				error_text = error_text + "Invalid number of balls chosen<br>";
				found_error = true;
			}
		}else if($('#number_text').val().length!=parseInt($(this).val())){
			error_text = error_text + "Invalid number of balls chosen<br>";
			found_error = true;
		}*/
		
		
		
	/*	if((formatted_straight == 0) && (formatted_boxed==0)){
			error_text = error_text + "You must choose a bet amount<br>";
			found_error = true;
		}*/
		
		
		var boxed=$("#boxed_amount_text").val().replace("$","");
		var straight=$("#straight_amount_text").val().replace("$","");
		
		if(straight == '')
		{
			if(boxed == '')
			{
				error_text = error_text + "You must choose a bet amount<br>";
				found_error = true;
			}	
			
		}
		
		if(straight !=''  && (straight == 0 || straight == 0.00) ) 
		{
				error_text = error_text + "Straight amount should be greater than 0<br>";
				found_error = true;
		}
		
		if(boxed !=''  && (boxed == 0 || boxed == 0.00) ) 
		{
				error_text = error_text + "Boxed amount should be greater than 0<br>";
				found_error = true;
		
		}
		
		
		
		
		
			
		if(found_error == true){
			$('#add_cart_errors').html(error_text).slideDown("fast");
		}else{
			
			
	
			//boolean used to determine if any houses were checked
			var houses_checked = false;
			
			//Check for all checked boxes
			$.each($("td input:checked"), function() {
				
				
				if($(this).attr('ball')==$("#button_add_cart").val()){
					//Update the boolean to say there was a box checked
					houses_checked = true;
					var id = $(this).attr('id').replace("game_","");
					var game_name = $(this).parent("td").next().text();
					
					//Verify the numbers added
					
					if($("#straight_amount_text").val() != ""){
						
						if(!isNaN(formatted_straight)){
						
						var myArray = [ '100', '200', '300', '400', '500', '600', '700', '800', '900' ];			
						for( i = 0, l = myArray.length; i < l; i++ ) {
						//alert(myArray[i]);
						//$('#number_text').val(myArray[i]);
							
							
							total += formatted_straight;
							$.ajax({
								url: "/ajax/lotto_betting.php",
								async:false,
								data: {
									action: "get_payout",
									pick:myArray[i],
									bet_amount: formatted_straight.toFixed(2),
									is_boxed: 0
								},
								method: "POST"
							})
							.success(function(payout){
							//console.log(formatted_straight);
								$("#bet_table tbody").prepend("<tr><td>"+myArray[i]+"</td><td style='display: none;'>"+id+"</td><td>"+game_name+"</td><td>S</td><td class='subtotal amount'>$<span>"+formatted_straight.toFixed(2)+"</span></td><td class='subtotal'>$<span>"+payout+"</span></td><td><button class='btn btn-danger remove_bet_button'>Delete</button></td>");
								if(typeof choices_set[myArray[i]] == 'undefined'){
									choices_set[myArray[i]]=1;
								}else{
									choices_set[myArray[i]]+=1;
								}
							});
						}
						}else{
							$('#add_cart_errors').append("Invalid Straight Number<br>").slideDown("fast");
						
						
						}
					}
					if($("#boxed_amount_text").val() != ""){
						if(!isNaN(formatted_boxed)){
							
						var myArray = [ '100', '200', '300', '400', '500', '600', '700', '800', '900' ];			
						for( i = 0, l = myArray.length; i < l; i++ ) {
						//alert(myArray[i]);
						//$('#number_text').val(myArray[i]);	
							
							total += formatted_boxed;
							$.ajax({
								url: "/ajax/lotto_betting.php",
								async:false,
								data: {
									action: "get_payout",
									pick: myArray[i],
									bet_amount: formatted_boxed.toFixed(2),
									is_boxed: 1
								},
								method: "POST"
							})
							.success(function(payout){
								$("#bet_table tbody").prepend("<tr><td>"+myArray[i]+"</td><td style='display: none;'>"+id+"</td><td>"+game_name+"</td><td>B</td><td class='subtotal amount'>$<span>"+formatted_boxed.toFixed(2)+"</span></td><td class='subtotal'>$<span>"+payout+"</span></td><td><button class='btn btn-danger remove_bet_button'>Delete</button></td>");
								if(typeof choices_set[myArray[i]] == 'undefined'){
									choices_set[myArray[i]]=1;
								}else{
									choices_set[myArray[i]]+=1;
								}
							});
						}
						}else{
							$('#add_cart_errors').append("Invalid Boxed Number<br>").slideDown("fast");
						}
					}
					
					//Update the total
					$('#total').text(total.toFixed(2));
				}
					
				
			});
			
			//If there were no houses checked, throw an error
			if(!houses_checked){
				$('#add_cart_errors').append("You have not selected any houses<br>").slideDown("fast");
			}
		
	}
	});
	
	/* End  All Hundreads */
});
	

