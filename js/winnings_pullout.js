$(document).ready(function(){

	$(document).on("click", "#winning_pullout_button", function(){
		if($("#winnings_pullout").css("left") == "-200px"){
            $("#winnings_pullout").animate({"left": "0px"},"fast");
        }
        else{
            $("#winnings_pullout").animate({"left": "-200px"},"fast");
        }
	});

	$(document).on("click", "#winning_pullout_today", function(){
		$.ajax({
			url: "/ajax/winning_numbers_pullout.php",
			data: {winning_numbers_list: true, day: "today"},
			method: "POST"
		})
		.done(function( data ) {
			$('#winnings_pullout_list_container').html(data);
		});
	});

	//Prepopulate the winnings table
	$('#winning_pullout_today').click();
	
	$(document).on("click", "#winning_pullout_yesterday", function(){
		$.ajax({
			url: "/ajax/winning_numbers_pullout.php",
			data: {winning_numbers_list: true, day: "yesterday"},
			method: "POST"
		})
		.done(function( data ) {
			$('#winnings_pullout_list_container').html(data);
		});
	});
});