<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

	$q = "SELECT value FROM `settings` WHERE `setting`='geolocation_url_id'";
	$id = $db->queryOneRow($q);
?>
	function getLocation(){
		var browser_geo_loc = {};
		// prompt for browser geolocation (most accurate if mobile device)
		if(navigator.geolocation){
			var location_timeout = setTimeout("browserGeoLocationError('timeout')", 10000);

			navigator.geolocation.getCurrentPosition(function(position){
				clearTimeout(location_timeout);
				
				browser_geo_loc = googleMapInfo(position.coords.latitude, position.coords.longitude);
				
				getLocationByIP('<?php echo $_SERVER['REMOTE_ADDR']; ?>', 0, browser_geo_loc);
			}, function(error){
				clearTimeout(location_timeout);
				browserGeoLocationError(error);
			});
		}else{
			// Fallback for no browser geolocation
			browserGeoLocationError();
			
			getLocationByIP('<?php echo $_SERVER['REMOTE_ADDR']; ?>', 0);
		}
	}

	function googleMapInfo(lat, lon){
		var results = {};
		results['latitude'] = lat;
		results['longitude'] = lon;
		results['geo_url'] = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lon+"&sensor=false";
		results['img_url'] = "http://maps.googleapis.com/maps/api/staticmap?center="+lat+","+lon+"&zoom=10&size=480x120&sensor=false";
		
		$.ajax({
			url: results['geo_url'],
			method: "POST",
			dataType: "json"
		})
		.done(function(data){		
			if(data.status = "OK"){
				results['success'] = true;
				results['location'] = {};
				$.each(data.results, function(index, value){
					results['location'][value.types[0]] = value.formatted_address;
				});
			}else{
				results['success'] = false;
			}
		});
		return results;
	}
	
	function browserGeoLocationError(error){
		if(error){
			if(error == 'timeout'){
				$.ajax({
					data: {
						action: 'block',
						action_reason: 'GEOLOCATION ERROR: The request to get user location timed out.'
					},
					dataType: 'json',
					url: "/lib/framework/ajax/geo_location.php",
					method: "POST"
				});
				bootbox.alert("GEOLOCATION ERROR: The request to get user location timed out.", function(){location.reload();});
			}else{
				switch(error.code){
					case error.PERMISSION_DENIED:
						$.ajax({
							data: {
								action: 'block',
								action_reason: 'GEOLOCATION ERROR: User denied the request for Geolocation.'
							},
							dataType: 'json',
							url: "/lib/framework/ajax/geo_location.php",
							method: "POST"
						});
						bootbox.alert("GEOLOCATION ERROR: User denied the request for Geolocation.", function(){location.reload();});
						break;
					case error.POSITION_UNAVAILABLE:
						$.ajax({
							data: {
								action: 'block',
								action_reason: 'GEOLOCATION ERROR: Location information is unavailable.'
							},
							dataType: 'json',
							url: "/lib/framework/ajax/geo_location.php",
							method: "POST"
						});
						bootbox.alert("GEOLOCATION ERROR: Location information is unavailable.", function(){location.reload();});
						break;
					case error.TIMEOUT:
						$.ajax({
							data: {
								action: 'block',
								action_reason: 'GEOLOCATION ERROR: The request to get user location timed out.'
							},
							dataType: 'json',
							url: "/lib/framework/ajax/geo_location.php",
							method: "POST"
						});
						bootbox.alert("GEOLOCATION ERROR: The request to get user location timed out.", function(){location.reload();});
						break;
					case error.UNKNOWN_ERROR:
						$.ajax({
							data: {
								action: 'block',
								action_reason: 'GEOLOCATION ERROR: An unknown error occurred.'
							},
							dataType: 'json',
							url: "/lib/framework/ajax/geo_location.php",
							method: "POST"
						});
						bootbox.alert("GEOLOCATION ERROR: An unknown error occurred.", function(){location.reload();});
						break;
				}
			}
		}else{
			$.ajax({
				data: {
					action: 'block',
					action_reason: 'GEOLOCATION ERROR: Geolocation not supported by this browser.'
				},
				dataType: 'json',
				url: "/lib/framework/ajax/geo_location.php",
				method: "POST"
			});
			bootbox.alert("GEOLOCATION ERROR: Geolocation not supported by this browser.", function(){location.reload();});
		}
	}
	
	/**
	* Calculates the great-circle distance between two points, with the Haversine formula.
	* @param float lat1 Latitude of start point in [deg decimal]
	* @param float lon1 Longitude of start point in [deg decimal]
	* @param float lat2 Latitude of target point in [deg decimal]
	* @param float lon2 Longitude of target point in [deg decimal]
	* @param float R is earth radius in [km]
	* @return float Distance between points in [km] (same as earth radius)
	*/
	function distance_between_coords(lat1, lon1, lat2, lon2) {
		var R = 6371; // Radius of the earth in km
		var dLat = deg2rad(lat2-lat1);
		var dLon = deg2rad(lon2-lon1); 
		var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2); 
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		var Distance = R * c;
		return Distance;
	}
	function deg2rad(deg) {
		return deg * (Math.PI/180)
	}

	var get_ip_loc_attempts = 0;
	function getLocationByIP(ip, source, browser_geo_loc){
		get_ip_loc_attempts++;
		var geo_url = {};
		geo_url[1] = 'http://freegeoip.net/json/'+ip;
		geo_url[2] = 'http://ipinfo.io/'+ip+'/json';

		if(source == 0){
			$.ajax({
				url: geo_url[1],
				method: "GET",
				dataType: "json"
			})
			.done(function(data){
				if(typeof data.country_name != 'undefined'){
					var gmap_info = googleMapInfo(data.latitude, data.longitude);
					
					// compare ip results to browser geolocation data if available
					if(typeof browser_geo_loc != 'undefined'){
						var dist_km = distance_between_coords(browser_geo_loc.latitude, browser_geo_loc.longitude, data.latitude, data.longitude);
						if(dist_km <= 80){
							// send data to ajax geolocation handler
							$.ajax({
								data: {
									city: data.city,
									region: data.region_name,
									country_code: data.country_code,
									country_name: data.country_name,
									zipcode: data.zip_code,
									hostname: '',
									ip: data.ip,
									latitude: data.latitude,
									longitude: data.longitude,
									organization: '',
									map_img_url: gmap_info.img_url
								},
								dataType: 'json',
								url: "/lib/framework/ajax/geo_location.php",
								method: "POST"
							})
							.done(function(response){
								if(response.success == 'false'){
									$.ajax({
										data: {
											action: 'block',
											action_reason: response.error
										},
										dataType: 'json',
										url: "/lib/framework/ajax/geo_location.php",
										method: "POST"
									});
									bootbox.alert(response.error, function(){location.reload();});
								}
							});
						}else{
							// don't allow if more than 80km (~50 miles) apart
							$.ajax({
								data: {
									action: 'block',
									action_reason: 'GEOLOCATION ERROR: Conflicting or inaccurate geolocation information detected.'
								},
								dataType: 'json',
								url: "/lib/framework/ajax/geo_location.php",
								method: "POST"
							});
							bootbox.alert("GEOLOCATION ERROR: Conflicting or inaccurate geolocation information detected.", function(){location.reload();});
						}
					}
				}else{
					if(get_ip_loc_attempts < geo_url.length){
						// try another source
						getLocationByIP(ip, get_ip_loc_attempts, browser_geo_loc);
					}
				}
			});
		}else if(source == 1){
			$.ajax({
				url: geo_url[2],
				method: "GET",
				dataType: "json"
			})
			.done(function(data){				
				if(typeof data.loc != 'undefined'){
					var loc_arr = data.loc.split(",");
					var gmap_info = googleMapInfo(loc_arr[0], loc_arr[1]);
					
					// compare ip results to browser geolocation data if available
					if(typeof browser_geo_loc != 'undefined'){
						var dist_km = distance_between_coords(browser_geo_loc.latitude, browser_geo_loc.longitude,loc_arr[0], loc_arr[1]);
						if(dist_km <= 80){
							// send data to ajax geolocation handler
							$.ajax({
								data: {
									city: data.city,
									region: data.region,
									country_code: data.country,
									country_name: gmap_info.location.country,
									zipcode: data.postal,
									hostname: data.hostname,
									ip: data.ip,
									latitude: loc_arr[0],
									longitude: loc_arr[1],
									organization: data.org,
									map_img_url: gmap_info.img_url
								},
								dataType: 'json',
								url: "/lib/framework/ajax/geo_location.php",
								method: "POST"
							})
							.done(function(response){
								if(response.success == 'false'){
									$.ajax({
										data: {
											action: 'block',
											action_reason: response.error
										},
										dataType: 'json',
										url: "/lib/framework/ajax/geo_location.php",
										method: "POST"
									});
									bootbox.alert(response.error, function(){location.reload();});
								}
							});
						}else{
							// don't allow if more than 80km (~50 miles) apart
							$.ajax({
								data: {
									action: 'block',
									action_reason: 'GEOLOCATION ERROR: Conflicting or inaccurate geolocation information detected.'
								},
								dataType: 'json',
								url: "/lib/framework/ajax/geo_location.php",
								method: "POST"
							});
							bootbox.alert("GEOLOCATION ERROR: Conflicting or inaccurate geolocation information detected.", function(){location.reload();});
						}
					}
				}else{
					if(get_ip_loc_attempts < geo_url.length){
						// try another source
						getLocationByIP(ip, get_ip_loc_attempts, browser_geo_loc);
					}
				}
			});
		}
	}
	
	var curr_page = window.location.href.replace("http://","");
	curr_page = curr_page.replace("https://","");

	if(curr_page != "<?php echo $_SERVER['HTTP_HOST']; ?>/invalid_location.php"){
		getLocation();
	}