$(document).ready(function(){
	$(document).on("click", "#change_password", function(){
		$("#change_password_modal").modal("show");
		$("#change_password_errors").slideUp("fast");
	});

	$(document).on("click", "#change_password_confirm", function(){
		$.ajax({
			url: "/ajax/change_password.php",
			data: {curr_password: $('#current_password').val(), new_password: $('#new_password').val(), repeat_new_password: $('#repeat_new_password').val()},
			method: "POST"
		})
		.done(function( data ) {
			if(data != ""){
				$("#change_password_errors").html(data);
				$("#change_password_errors").slideDown("fast");
			}else{
				$("#change_password_errors").slideUp("fast");
				$("#change_password_modal").modal("hide");
				bootbox.alert("Password changed successfully");
			}
		});
	});
         $(document).on('submit','#make_deposit_form',function(e) { 	    
            if($('#is_valid').val() == 0){
                 // this code prevents form from actually being submitted
		     e.preventDefault();
		     e.returnValue = false;
		      var $form = $(this);

		 $.ajax({ 
		     type: 'POST',
		     url: '/ajax/my_account.php',
                     data: {action: "check_daily_deposit", amount: $('#deposit_amount').val() } ,
                     dataType: 'json',
		     context: $form, // context will be "this" in your handlers
		     success: function(data) { // your success handler
                   
                        if(data.success == false){  
                               $('#deposite_errors').html(data.message);
                               $('#deposite_errors').slideDown('fast');  
                               $('#is_valid').val('0');                             
                        } else {
                               $('#is_valid').val('1');                            
                               $form.unbind().submit();
                        }
		     },
		     error: function() { // your error handler
		     }
		 });
           }
        });

	$(document).on("click", "#change_pin", function(){
		$("#change_pin_modal").modal("show");
		$("#change_pin_errors").slideUp("fast");
	});
	
	$(document).on("click", "#set_limits", function(){
		$("#set_limits_modal").modal("show");
		$("#set_limits_errors").slideUp("fast");
	});

	$(document).on("click", "#change_pin_confirm", function(){
		$.ajax({
			url: "/ajax/change_pin.php",
			data: {
				password: $('#change_pin_password').val(),
				new_pin: $('#new_pin').val(),
				repeat_new_pin: $('#repeat_new_pin').val()
			},
			method: "POST"
		})
		.done(function( data ) {
			if(data != ""){
				$("#change_pin_errors").html(data);
				$("#change_pin_errors").slideDown("fast");
			}else{
				$("#change_pin_errors").slideUp("fast");
				$("#change_pin_modal").modal("hide");
				bootbox.alert("PIN changed successfully");
			}
		});
	});
	
	$(document).on("click", "#verify_identity", function(){
		$("#verify_modal").modal("show");
	});
});
