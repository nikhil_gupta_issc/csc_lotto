$(document).ready(function(){
	//Run update balances on page load
	update_balances();
       
	//The following will check every 10 seconds to make sure that the balance shown
	//in the balances side menu is accurate
	setInterval(update_balances, 30000);

	//Function to update the balance
	function update_balances() {
		$.ajax({
			url: "/ajax/balances.php",
			dataType : 'json',
			data: {action: "get_current_balances"},
			method: "POST"
		})
		.done(function( data ) {
			$('.avaiable_money').html(data.available_money);
			$('.current_balance').html(data.current_balance);
			$('.bonus_balance').html(data.bonus_balance);
			$('.bonus_hold').html(data.bonus_hold);
			$('.total_balance').html(data.total_balance);
		});
	}
});
