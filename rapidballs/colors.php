<script>
	$(document).ready(function(){
		$(document).on("click", ".pick_number_row .ball", function(){
			if(!$(this).hasClass("selected")){
				$("#pick-1").attr("class", $(this).attr("class"));
				$("#pick-1").removeClass("ball-lg");
				$("#pick-1").addClass("ball-sm");
				$("#pick-1").attr("pick", $(this).attr("pick"));
			}
			
			$(".selected").removeClass("ball-lg");
			$(".selected").addClass("ball-sm");
			$(".selected").removeClass("selected");
			$(this).addClass("selected");
		});
		
		$(document).on("click", "#submit-colors", function(){
			$(this).prop("disabled", true);
			var holdThis = this;
			
			$.ajax({
				url: "/rapidballs/ajax/post_bets.php",
				type: "POST",
				data: {
					action : "post_colors",
					pick : $("#pick-1").attr("pick"),
					amount : $("#stake_per_draw").val(),
					number_of_bets : $("#number_of_draws").val()
				},
				dataType: "json",
				success: function(data){
					// hide buttons and reload table if successful
					if(data["results"] == "false"){
						bootbox.alert(data["errors"]);
					}else{
						bootbox.alert("Drawings successfully scheduled!");
						
						// clear selection
						$("#number_of_draws").val("");
						$("#stake_per_draw").val("");
						  $("#total").text("0");
						$("#clear_pick").trigger("click");
					}
					
					$(holdThis).removeProp("disabled");
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
		});
		
		$(document).on("click", "#clear_pick", function(){
			for(var x = 1; x <= 5; x++){
				$("#pick-"+x).attr("class", "");
				$("#pick-"+x).attr("pick", "");
				$("#pick-"+x).addClass("ball");
				$("#pick-"+x).addClass("ball-sm");
				$("#pick-"+x).addClass("ball-grey-color");
			}
			
			$(".selected").removeClass("ball-lg");
			$(".selected").addClass("ball-sm");
			$(".selected").removeClass("selected");
		});
	});
</script>
<?php
 $select_ip = "select count(id) as num from ipaddress where status=1 AND ipaddress='".$_SERVER["REMOTE_ADDR"]."'";
 $ip = $db->query($select_ip);
// if($ip[0]['num'] >0){
 ?>
<div class="clearfix" style="background:#fff">	
<div class="col-sm-12">
	<p>&nbsp;</p>
</div>
<div class="col-sm-1">
	<p>&nbsp;</p>
</div>

<div class="col-sm-10" style="background-color:#fff; border-radius: 13px 13px 13px 13px; -moz-border-radius: 13px 13px 0px 0px; -webkit-border-radius: 13px; -webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px; -webkit-border-bottom-left-radius: 13px;padding:15px;color:#000">
	<div class="col-sm-8">
		<h1>Colors</h1>
		<p>Colors is a fun game; each draw offers 6 balls with a selection of colors (1-9 = Red, 10-19 = Blue, 20-29 = Yellow, 30-39 = Purple, 40-49 = Green). You place a stake and decide exactly how many of one particular color will appear in the draw. If you guess correctly, you will win a payout.</p>
		<p>&nbsp;</p>
		<div class="col-sm-10">
			<div class="pick_number_row clearfix">
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-6-color" pick="0-blue"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-7-color" pick="3-blue"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-8-color" pick="4-blue"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-9-color" pick="5-blue"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-10-color" pick="6-blue"></span>
				</div>
			</div>
			<div class="pick_number_row clearfix">
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-11-color" pick="0-yellow"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-12-color" pick="3-yellow"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-13-color" pick="4-yellow"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-14-color" pick="5-yellow"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-15-color" pick="6-yellow"></span>
				</div>
			</div>
			<div class="pick_number_row clearfix">
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-16-color" pick="0-purple"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-17-color" pick="3-purple"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-18-color" pick="4-purple"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-19-color" pick="5-purple"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-20-color" pick="6-purple"></span>
				</div>
			</div>
			<div class="pick_number_row clearfix">
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-21-color" pick="0-green"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-22-color" pick="3-green"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-23-color" pick="4-green"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-24-color" pick="5-green"></span>
				</div>
				<div class="color-ball-container growing-ball-container">
					<span class="ball ball-sm ball-25-color" pick="6-green"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<h2>Your Pick</h2>
		<div>
			<button id="clear_pick" class="btn btn-primary">Clear</button>
		</div>
		<div id="lotto-pick-balls" class="clearfix">
			<div class="ball-container">
				<span class="ball ball-sm ball-grey-color" id="pick-1" pick=""></span>
			</div>
		</div>
		<div id="lotto-pick-form">
			<form>
				<div class="form-group">
					<label for="number_of_draws">Number of Draws</label>
					<select class="form-control" id="number_of_draws"><option value="1">1 Draw<option value="5">5 Draws<option value="10">10 Draws<option value="50">50 Draws<option value="100">100 Draws<option value="288">288 Draws (1 Day)</select>
				</div>
				<div class="form-group">
					<label for="stake_per_draw">Stake Per Draw</label>
					<select class="form-control" id="stake_per_draw"><option value="$1.00">1 CSC<option value="$2.00">2 CSC<option value="$5.00">5 CSC<option value="$10.00">10 CSC</select>
				</div>
				<h3>Total: <span id="total">1</span> CSC</h3>
				<button type="button" id="submit-colors" class="btn btn-lg btn-primary">Purchase</button>
			</form>
		</div>
	</div>
</div>
<div class="col-sm-1">
	<p>&nbsp;</p>
</div>
</div>


<div class="bg">
		<div class="col-sm-12">
	<p>&nbsp;</p>
</div>	
				<div class="col-sm-1"></div>
				<div class="col-sm-4" style=" background-color:#CCC; border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;">
					<h3>Colors Payouts</h3>
					<table class="table table-striped">
						<tr>
							<td>Match 0 Colors: Win x3.3 Stake</td>
						</tr>
						<tr>
							<td>Match 3 Colors: Win x9.6 Stake</td>
						</tr>
						<tr>
							<td>Match 4 Colors: Win x70 Stake</td>
						</tr>
						<tr>
							<td>Match 5 Colors: Win x1100 Stake</td>
						</tr>
						<tr>
							<td>Match 6 Colors: Win x52000 Stake</td>
						</tr>
					</table>
					<p>Theoretical RTP: 77%</p>
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-5" style="background-color:#000; border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;color:#fff">				
					<?php include('./rapidballs/inc_latestDraw.php'); ?>
					
				</div>
			<div class="col-sm-1"></div>
				
		</div>
		<?php 
//}
 ?>
