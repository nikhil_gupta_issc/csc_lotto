<script>
	$(document).ready(function(){
		$(document).on("click", ".ball", function(){
			$("#pick-1").attr("class", $(this).attr("class"));
			$("#pick-1").attr("pick", $(this).attr("pick"));
			$("#pick-1").removeClass("ball-lg");
			$("#pick-1").addClass("ball-sm");
		});
		
		$(document).on("click", "#submit-lucky", function(){
			$(this).prop("disabled", true);
			var holdThis = this;
			
			$.ajax({
				url: "/rapidballs/ajax/post_bets.php",
				type: "POST",
				data: {
					action : "post_lucky",
					pick : $("#pick-1").attr("pick"),
					amount : $("#stake_per_draw").val(),
					number_of_bets : $("#number_of_draws").val()
				},
				dataType: "json",
				success: function(data){
					// hide buttons and reload table if successful
					if(data["results"] == "false"){
						bootbox.alert(data["errors"]);
					}else{
						bootbox.alert("Drawings successfully scheduled!");
						
						// clear selection
						$("#number_of_draws").val("");
						$("#stake_per_draw").val("");
						$("#total").text("0");
						$("#clear_pick").trigger("click");
					}
					
					$(holdThis).removeProp("disabled");
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
		});
		
		$(document).on("click", "#clear_pick", function(){
			for(var x = 1; x <= 5; x++){
				$("#pick-"+x).attr("class", "");
				$("#pick-"+x).addClass("ball");
				$("#pick-"+x).addClass("ball-xl");
				$("#pick-"+x).addClass("ball-grey-lucky");
			}
		});
	});
</script>

<div class="clearfix" style="background:#fff">	
<div class="col-sm-12">
	<p>&nbsp;</p>
</div>
<div class="col-sm-1">
	<p>&nbsp;</p>
</div>

<div class="col-sm-10" style="background-color:#000; 			border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;color:#fff">				
			
	<div class="col-sm-8">
		<h1>Lucky</h1>
		<p>If you add up all the numbers shown on the 6 balls drawn this will give you a Total amount (11+17+29+33+37+41 = 168). If you add up all the numbers shown on the Total result (1+6+8 = 15) this will give you a Lucky Number. To play you place a stake and select which Lucky number this will be, and if correct, you will win a payout.</p>
		<p>&nbsp;</p>
		<div class="col-sm-10">
			<div class="center">
				<div class="xl-block-container clearfix">
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-1-lucky" pick="1"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-2-lucky" pick="2"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-3-lucky" pick="3"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-4-lucky" pick="4"></span>
					</div>
				</div>
			</div>
			<div class="center">
				<div class="xl-block-container clearfix">
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-5-lucky" pick="5"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-6-lucky" pick="6"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-7-lucky" pick="7"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-8-lucky" pick="8"></span>
					</div>
					
				</div>
			</div>
			<div class="center">
				<div class="xl-block-container clearfix">
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-9-lucky" pick="9"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-10-lucky" pick="10"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-11-lucky" pick="11"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-12-lucky" pick="12"></span>
					</div>
					
				</div>
			</div>
			<div class="center">
				<div class="xl-block-container clearfix">
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-13-lucky" pick="13"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-14-lucky" pick="14"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-15-lucky" pick="15"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-16-lucky" pick="16"></span>
					</div>
				</div>
			</div>
			<div class="center">
				<div class="xl-block-container clearfix">
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-17-lucky" pick="17"></span>
					</div>
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-18-lucky" pick="18"></span>
					</div>
				</div>
			</div>
			<div class="center">
				<div class="xl-block-container clearfix">
					<div class="ball-container lucky-container">
						<span class="ball ball-xl ball-19-lucky" pick="19"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<h2>Your Pick</h2>
		<div>
			<button id="clear_pick" class="btn btn-primary">Clear</button>
		</div>
		<div id="lotto-pick-balls" class="clearfix">
			<div class="ball-container">
				<span class="ball ball-xl ball-grey-lucky" id="pick-1"  pick=""></span>
			</div>
		</div>
		<div id="lotto-pick-form">
			<form>
				<div class="form-group">
					<label for="number_of_draws">Number of Draws</label>
					<select class="form-control" id="number_of_draws"><option value="1">1 Draw<option value="5">5 Draws<option value="10">10 Draws<option value="50">50 Draws<option value="100">100 Draws<option value="288">288 Draws (1 Day)</select>
				</div>
				<div class="form-group">
					<label for="stake_per_draw">Stake Per Draw</label>
					<select class="form-control" id="stake_per_draw"><option value="$1.00">1 CSC<option value="$2.00">2 CSC<option value="$5.00">5 CSC<option value="$10.00">10 CSC</select>
				</div>
				<h3>Total: <span id="total">1</span> CSC</h3>
				<button type="button" class="btn btn-lg btn-primary" id="submit-lucky">Purchase</button>
			</form>
		</div>
	</div>
</div>
<div class="col-sm-1">
	<p>&nbsp;</p>
</div>
</div>


<div class="bg">
		<div class="col-sm-12">
	<p>&nbsp;</p>
</div>	
				<div class="col-sm-1"></div>
				<div class="col-sm-4" style=" background-color:#CCC; 			border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;">
					<h3>Lucky Payouts</h3>
					<table class="table table-striped">
						<tr>
							<td>Number 1:	Win x205 Stake</td>
						</tr>
						<tr>
							<td>Number 2:	Win x56 Stake</td>
						</tr>
						<tr>
							<td>Number 3:	Win x32 Stake</td>
						</tr>
						<tr>
							<td>Number 4:	Win x21 Stake</td>
						</tr>
						<tr>
							<td>Number 5:	Win x16 Stake</td>
						</tr>
					
						<tr>
							<td>Number 6:	Win x13 Stake</td>
						</tr>
						<tr>
							<td>Number 7:	Win x11 Stake</td>
						</tr>
						<tr>
							<td>Number 8:	Win x9 Stake</td>
						</tr>
						<tr>
							<td>Number 9:	Win x8 Stake</td>
						</tr>
						<tr>
							<td>Number 10:	Win x7.5 Stake</td>
						</tr>
					
						<tr>
							<td>Number 11:	Win x8 Stake</td>
						</tr>
						<tr>
							<td>Number 12:	Win x9 Stake</td>
						</tr>
						<tr>
							<td>Number 13:	Win x10.5 Stake</td>
						</tr>
						<tr>
							<td>Number 14:	Win x12.5 Stake</td>
						</tr>
						<tr>
							<td>Number 15:	Win x16 Stake</td>
						</tr>
					
						<tr>
							<td>Number 16:	Win x22 Stake</td>
						</tr>
						<tr>
							<td>Number 17:	Win x32 Stake</td>
						</tr>
						<tr>
							<td>Number 18:	Win x54 Stake</td>
						</tr>
						<tr>
							<td>Number 19:	Win x190 Stake</td>
						</tr>
					</table>
					<p>Theoretical RTP: Ranges From 77-82%</p>
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-5" style="background-color:#000; 			border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;color:#fff">				
					<?php include('./rapidballs/inc_latestDraw.php'); ?>
					
				</div>
			<div class="col-sm-1"></div>
				
		</div>
	
