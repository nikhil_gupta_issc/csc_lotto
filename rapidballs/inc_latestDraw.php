	<script>
		var clock;

		$(document).ready(function() {
			// Grab the current date
			var currentDate = new Date();

			// Calculate the difference in seconds between the future and current date
			var diff;
			start_counter = function(){
				$.ajax({
					url: "/rapidballs/ajax/rapidballs_countdown.php",
					method: "POST"
				})
				.done(function(response){
					console.log(response);
					diff = response;
					
					// Instantiate a coutdown FlipClock
					clock = $('#countdown_flipclock').FlipClock(diff, {
						clockFace: 'MinuteCounter',
						countdown: true,
						autoStart: false,
						callbacks: {
							stop: function() {
								start_counter();
							},
							interval: function () {
								var time = clock.getTime().time;
								// show warning style if near next draw
								if (time < 30) {
									if(!$("#countdown-contain").hasClass("ending-soon")){
										$("#countdown-contain").addClass("ending-soon");
									}
								}else{
									if($("#countdown-contain").hasClass("ending-soon")){
										$("#countdown-contain").removeClass("ending-soon");
									}
								}
								
								if(time >= 295){
									// check for up to 5 seconds after draw is supposed to happen
									update_drawings();
								}
							}
						}
					});
					
					clock.start();
				});
			};
			start_counter();
			
			function update_drawings(){
				// grab most recent drawing
				$.ajax({
					data: {
						size: "lg",
						limit: 1,
						type: 'normal'
					},
					url: "/rapidballs/ajax/rapidballs_recent_drawings.php",
					method: "POST",
					dataType: "json"
				})
				.done(function(response){
					$("#last-drawing").html(response.balls);
					$("#last-total").text(response.total);
					$("#last-lucky").text(response.lucky);
					//$("#last-hash").text(response.hash);
					//$("#last-seed_a").text(response.seed_a);
					//$("#last-seed_b").text(response.seed_b);
					//$("#next-hash").text(response.next_hash);
				});
			
				// update last 6 most recent
				$.ajax({
					data: {
						size: "sm",
						limit: 6,
						type: 'table'
					},
					url: "/rapidballs/ajax/rapidballs_recent_drawings.php",
					method: "POST"
				})
				.done(function(response){
					$("#recent-6").html(response);
				});
			}
			
			//Update the total on select change.
			$(document).on("change", "#number_of_draws", function(){
				var num_draws = parseFloat($(this).val());
					stake_per_draw = parseFloat($("#stake_per_draw").val().replace("$",""));
				           	
				$("#total").text((num_draws * stake_per_draw).toFixed(0));
			});
			
			$(document).on("change", "#stake_per_draw", function(){
				var num_draws = parseFloat($("#number_of_draws").val());
				stake_per_draw = parseFloat($(this).val().replace("$",""));
				
				$("#total").text((num_draws * stake_per_draw).toFixed(0));
			});
			
			// load initial recent drawings
			update_drawings();
		});
	</script>

	<div id="last-drawing-contain" class="col-sm-12" style="text-align:center">
		<h3 style=" margin-top: -10px; margin-bottom: 40px;">Last Drawing:</h3>
		<div id="last-drawing"></div>
		<p>&nbsp;</p>
		<div class="col-sm-6" style="text-align:center">Total: <span id="last-total"></span></div>
		<div class="col-sm-6" style="text-align:center">Lucky: <span id="last-lucky"></span></div>
		<!--<div class="col-sm-12" style="text-align:center">Seed A: <span id="last-seed_a"></span></div> -->
		<!--<div class="col-sm-12" style="text-align:center">Seed B: <span id="last-seed_b"></span></div> -->
		<!--<div class="col-sm-12" style="text-align:center">Hash: <span id="last-hash"></span></div> -->
	</div>
	<p>&nbsp;</p>
	<div id="countdown-contain"  class="col-sm-12" style="text-align:center">
		<div class="col-sm-12" style="text-align:center">
			<h3 style="margin-top: -10px; margin-bottom: 25px;">Next Drawing In:</h3>
			<div id="countdown_flipclock"></div>
		</div>
		<!--<div class="col-sm-12" style="text-align: center">Hash: <span id="next-hash"></span></div>-->
	</div>
