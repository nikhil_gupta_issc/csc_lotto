<?php
	// Include rapidballs class
	require_once($_SERVER['DOCUMENT_ROOT']."/rapidballs/lib/rapidballs.php");
	$rapidballs = new RAPIDBALLS();
?>
	<link rel="stylesheet" type="text/css" href="/rapidballs/css/balls.css?v=<?=CSS_VERSION?>">
	<link rel="stylesheet" type="text/css" href="/rapidballs/css/global.css?v=<?=CSS_VERSION?>">
	<script type="text/javascript" src="/rapidballs/js/global.js?v=<?=JS_VERSION?>"></script>

	<link rel="stylesheet" href="/lib/assets/FlipClock/flipclock.css">
	<script src="/lib/assets/FlipClock/flipclock.min.js"></script>
	
	<script type="text/javascript" src="/rapidballs/lib/assets/simplyscroll/jquery.simplyscroll.js"></script>
	<link rel="stylesheet" href="/rapidballs/lib/assets/simplyscroll/jquery.simplyscroll.css" media="all" type="text/css">
	<script type="text/javascript">
		(function($) {
			$(function() {
				$(".scroller").simplyScroll({
					orientation: 'vertical', 
					customClass: 'auto-scroll',
					auto: true,
					autoMode: 'bounce',
					direction: 'forwards'
				});
			});
		})(jQuery);
	</script>

	<style>
		#countdown-contain{
			border: 2px solid white;
			padding: 15px;
			text-align: center;
			display: inline-block;
		}
		
		#last-drawing-contain{
			border: 2px solid white;
			padding: 15px;
			text-align: center;
			display: inline-block;
		}
		
		.ending-soon{
			animation: blink .5s step-end infinite alternate;
		}
		
		@keyframes blink {
			50% { border: 2px solid red; } 
		}
		
		.auto-scroll .simply-scroll-clip {
			height: 220px;
		}
	</style>
	<div class="clearfix" style="background:#fff;">
		<div class="clearfix">
		   <div class="header_image">
				<img src="/images/rapidballs_banner.jpg?v=<?=IMAGE_VERSION?>" class="img_grow_wide">
			</div>
		</div>
		<ul class="black_tabs">
			<li <?php echo (($_GET['play'] == '') ? 'class="active"' : '');?>><a href="<?php echo $_SERVER['PHP_SELF']; ?>?play="><span>About</span></a></li>
			<li <?php echo (($_GET['play'] == 'lotto') ? 'class="active"' : '');?>><a href="<?php echo $_SERVER['PHP_SELF']; ?>?play=lotto"><span>Lotto</span></a></li>
			<li <?php echo (($_GET['play'] == 'colors') ? 'class="active"' : '');?>><a href="<?php echo $_SERVER['PHP_SELF']; ?>?play=colors"><span>Colors</span></a></li>
			<li <?php echo (($_GET['play'] == 'totals') ? 'class="active"' : '');?>><a href="<?php echo $_SERVER['PHP_SELF']; ?>?play=totals"><span>Totals</span></a></li>
			<li <?php echo (($_GET['play'] == 'reds') ? 'class="active"' : '');?>><a href="<?php echo $_SERVER['PHP_SELF']; ?>?play=reds"><span>Reds</span></a></li>
			<li <?php echo (($_GET['play'] == 'lucky') ? 'class="active"' : '');?>><a href="<?php echo $_SERVER['PHP_SELF']; ?>?play=lucky"><span>Lucky</span></a></li>
			<li <?php echo (($_GET['play'] == 'recent') ? 'class="active"' : '');?>><a href="<?php echo $_SERVER['PHP_SELF']; ?>?play=recent"><span>Recent</span></a></li>
		</ul>
		<?php
			if(isset($_GET['play'])){
				if($_GET['play'] == 'lotto'){
					include($_SERVER['DOCUMENT_ROOT']."/rapidballs/lotto.php");
				}elseif($_GET['play'] == 'colors'){
					include($_SERVER['DOCUMENT_ROOT']."/rapidballs/colors.php");
				}elseif($_GET['play'] == 'lucky'){
					include($_SERVER['DOCUMENT_ROOT']."/rapidballs/lucky.php");
				}elseif($_GET['play'] == 'totals'){
					include($_SERVER['DOCUMENT_ROOT']."/rapidballs/totals.php");
				}elseif($_GET['play'] == 'reds'){
					include($_SERVER['DOCUMENT_ROOT']."/rapidballs/reds.php");
				}elseif($_GET['play'] == 'recent'){
					include($_SERVER['DOCUMENT_ROOT']."/rapidballs/recent.php");
				}else{
					include($_SERVER['DOCUMENT_ROOT']."/rapidballs/about.php");
				}
			}else{
				include($_SERVER['DOCUMENT_ROOT']."/rapidballs/about.php");
			}
		?>
		<!-- <div class="col-sm-12 clearfix" style="background:#fff;">
			<?php
				$q = "SELECT `value` FROM `settings` WHERE `setting` = 'rapidballs_version';";
				$result = $db->queryOneRow($q);
			?>
			<p style="text-align: center; padding-top: 20px; color: white;">RapidBalls v<?php echo $result['value']; ?></p>
		</div> -->
	</div>
