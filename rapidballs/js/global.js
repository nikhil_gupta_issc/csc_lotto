$(document).ready(function(){
	$(document).on("mouseenter", ".growing-ball-container .ball", function(){
		$(this).removeClass("ball-sm");
		$(this).addClass("ball-lg");
	});
	$(document).on("mouseleave", ".growing-ball-container .ball", function(){
		if(!$(this).hasClass("selected")){
			$(this).removeClass("ball-lg");
			$(this).addClass("ball-sm");
		}
	});
	
	$(document).on("change", "#stake_per_draw", function(){
		if(!isNaN($(this).val().replace("$",""))){
			var stake = parseFloat($(this).val().replace("$",""));
			$(this).val("$" + stake.toFixed(2));
		}else{
			$(this).val("");
		}
	});
});