<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/core_config.php');
	date_default_timezone_set('America/New_York');
       
        $site_settings = "SELECT value FROM `settings`  WHERE setting = 'rapidballs_enabled'" ; 
	$t_site_settings = $db->query($site_settings);

        if(!empty($t_site_settings[0]['value'])){

        $q = "SELECT id,datetime_drawn,ball_1,ball_2,ball_3,ball_4,ball_5,ball_6,total,lucky,hash,seed_a,seed_b FROM `rapidballs_drawing` ORDER BY id DESC LIMIT 1";

	$drawings = $db->query($q);
        if($_REQUEST['limit'] == 1 && !empty($drawings)){
            $query = "SELECT is_winner,user_session_id FROM rapidballs_bet WHERE drawing_id = ".$drawings[0]['id'];
            $winner_draw = $db->query($query);
        }

	if($_REQUEST['type'] == 'normal'){
		foreach($drawings as $drawing){
			$response["balls"] = '
				<div class="ball-container">
					<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_1'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_2'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_3'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_4'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_5'].'"></span>
				</div>
				<div class="ball-container">
					<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_6'].'"></span>
				</div>
			';
			$response["total"] = $drawing['total'];
			$response["lucky"] = $drawing['lucky'];
			$response["hash"] = $drawing['hash'];
			$response["seed_a"] = $drawing['seed_a'];
			$response["seed_b"] = $drawing['seed_b'];
			
			// get next pre-determined seeds from db
			$q = "SELECT hash FROM `rapidballs_next_drawing_seeds` WHERE `id`=1";
			$seed_info = $db->queryOneRow($q);
			$response["next_hash"] = $seed_info['hash'];
			
			echo JSON_encode($response);
		}
	}elseif($_REQUEST['type'] == 'notification'){
		foreach($drawings as $drawing){
			$now_dt = new DateTime();
			$draw_dt = new DateTime($drawing['datetime_drawn']);
			$time_diff = $now_dt->getTimestamp() - $draw_dt->getTimestamp();

			if($time_diff <= 5){

                           if(!empty($winner_draw) && $winner_draw[0]['is_winner'] == 1 && $winner_draw[0]['user_session_id'] == $session->userinfo['id']){
                             echo '<div class="winner">
					<link rel="stylesheet" type="text/css" href="/rapidballs/css/balls.css">
					
					<span ><h3 style="margin: 0; padding: 3px; color:white;">Congratulations!! You won the Rapidball Bet.</h3></span><br>
					
					<span style="height: 30px; display: inline-block; letter-spacing: 2px">
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_1'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_2'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_3'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_4'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_5'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_6'].'"></span>
						</div>
					</span>
					
					<a style="color:white;" href="#"><h5 style="color:white;">Hide these messages.</h5></a></div>
				';


                            } else {
				echo '
					<link rel="stylesheet" type="text/css" href="/rapidballs/css/balls.css">
					
					<span style="float: left; height: 30px; display: inline-block;"><h3 style="margin: 0; padding: 3px;color:white;">FastBallz Drawing:</h3></span>
					
					<span style="height: 30px; display: inline-block; letter-spacing: 2px">
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_1'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_2'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_3'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_4'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_5'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_6'].'"></span>
						</div>
					</span>
					
					<a style="color:white;" href="#"><h5 style="color:white;">Hide these messages.</h5></a>
				';
                          } 
			}
		}
	}elseif($_REQUEST['type'] == 'table'){
		echo '<table class="table table-striped">';
		foreach($drawings as $drawing){
			echo '
				<tr style="text-align:center;">
					<td style="text-align: center; padding: 2px;display:inline-block; letter-spacing: 2px">
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_1'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_2'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_3'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_4'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_5'].'"></span>
						</div>
						<div class="ball-container">
							<span class="ball ball-'.$_REQUEST['size'].' ball-'.$drawing['ball_6'].'"></span>
						</div>
					</td>
				</tr>
			';
		}
		echo "</table>";
	}
}
