<?php
	date_default_timezone_set('America/New_York');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$response = array();

	switch ($_POST['action']){
		case 'post_lotto':
			$_POST["amount"] = str_replace("$", "", $_POST["amount"]);
			
			$all_picks = array();
			$pick_1 = explode(" ", $_POST['pick_1']);
			$pick_1 = explode("-", $pick_1[1]);
			$all_picks[] = $pick_1[1];
			$pick_2 = explode(" ", $_POST['pick_2']);
			$pick_2 = explode("-", $pick_2[1]);
			$all_picks[] = $pick_2[1];
			$pick_3 = explode(" ", $_POST['pick_3']);
			$pick_3 = explode("-", $pick_3[1]);
			$all_picks[] = $pick_3[1];
			$pick_4 = explode(" ", $_POST['pick_4']);
			$pick_4 = explode("-", $pick_4[1]);
			$all_picks[] = $pick_4[1];
			$pick_5 = explode(" ", $_POST['pick_5']);
			$pick_5 = explode("-", $pick_5[1]);
			$all_picks[] = $pick_5[1];
			sort($all_picks);
			$pick_string = $all_picks[0]."-".$all_picks[1]."-".$all_picks[2]."-".$all_picks[3]."-".$all_picks[4];
			
			if(!is_numeric($pick_1[1]) || !is_numeric($pick_2[1]) || !is_numeric($pick_3[1]) || !is_numeric($pick_4[1]) || !is_numeric($pick_5[1])){
				$response["results"] = "false";
				$response["errors"] = "You must pick five numbers";
			}
			
			if(!is_numeric($_POST["number_of_bets"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if(!is_numeric($_POST["amount"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if($_POST["amount"] <= 0 || $_POST["number_of_bets"] <= 0){
				$response["results"] = "false";
				$response["errors"] = "Invalid entry.";
			}
			
			if($response["results"] != "false"){
				$trans_id = $core->make_customer_transaction(-($_POST['number_of_bets']*$_POST['amount']),46,"Fastballz Lotto Bet");
				if(!is_numeric($trans_id)){
					$response["results"] = "false";
					$response["errors"] = $trans_id;
				}else{
					$q = "SELECT id FROM `rapidballs_drawing` ORDER BY id DESC LIMIT 1";
					$current_drawing_id = $db->queryOneRow($q);
					
					$total_amount = $_POST['number_of_bets']*$_POST['amount'];
					$q = "INSERT INTO `rapidballs_ticket` (`bet_type_id`, `transaction_id`, `bet_on`, `total_amount`, `number_of_bets`, `user_id`) VALUES(1, %i, %s, %d, %i, %i)";
					$ticket_id = $db->queryInsert($q, array($trans_id, $pick_string, $total_amount, $_POST['number_of_bets'], $session->userinfo["id"]));
					
					for($x=0;$x<$_POST['number_of_bets'];$x++){
						$drawing_id = $current_drawing_id["id"] + $x + 1;
						$q = "INSERT INTO `rapidballs_bet` (`ticket_id`, `bet_type_id`, `bet_amount`, `drawing_id`, `user_session_id`, `transaction_id`) VALUES (%i, 1, %d, %i, %i, %i)";
						$db->queryInsert($q, array($ticket_id, $_POST['amount'], $drawing_id, $_SESSION['user_session_id'], $trans_id));
					}
					$response["results"] = "true";
					$response["errors"] = "";
				}
			}
			break;
		case 'post_colors':
			$_POST["amount"] = str_replace("$", "", $_POST["amount"]);
			
			if($_POST["pick"] == ""){
				$response["results"] = "false";
				$response["errors"] = "You must pick a color.";
			}
			
			if(!is_numeric($_POST["number_of_bets"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if(!is_numeric($_POST["amount"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if($_POST["amount"] <= 0 || $_POST["number_of_bets"] <= 0){
				$response["results"] = "false";
				$response["errors"] = "Invalid entry.";
			}
			
			if($response["results"] != "false"){
				$trans_id = $core->make_customer_transaction(-($_POST['number_of_bets']*$_POST['amount']),46,"Fastballz Colors Bet");
				if(!is_numeric($trans_id)){
					$response["results"] = "false";
					$response["errors"] = $trans_id;
				}else{
					$q = "SELECT id FROM `rapidballs_drawing` ORDER BY id DESC LIMIT 1";
					$current_drawing_id = $db->queryOneRow($q);
					
					$total_amount = $_POST['number_of_bets']*$_POST['amount'];
					$q = "INSERT INTO `rapidballs_ticket` (`bet_type_id`, `transaction_id`, `bet_on`, `total_amount`, `number_of_bets`, `user_id`) VALUES(2, %i, %s, %d, %i, %i)";
					$ticket_id = $db->queryInsert($q, array($trans_id, $_POST["pick"], $total_amount, $_POST['number_of_bets'], $session->userinfo["id"]));
					
					for($x=0;$x<$_POST['number_of_bets'];$x++){
						$drawing_id = $current_drawing_id["id"] + $x + 1;
						$q = "INSERT INTO `rapidballs_bet` (`ticket_id`, `bet_type_id`, `bet_amount`, `drawing_id`, `user_session_id`, `transaction_id`) VALUES (%i, 2, %d, %i, %i, %i)";
						$db->queryInsert($q, array($ticket_id, $_POST['amount'], $drawing_id, $_SESSION['user_session_id'], $trans_id));
					}
					$response["results"] = "true";
					$response["errors"] = "";
				}
			}
			break;
		case 'post_totals':
			$_POST["amount"] = str_replace("$", "", $_POST["amount"]);
			
			if($_POST["pick"] == "" || ($_POST["pick"] != "21-99" && $_POST["pick"] != "100-150" && $_POST["pick"] != "151-200" && $_POST["pick"] != "201-250" && $_POST["pick"] != "251-279")){
				$response["results"] = "false";
				$response["errors"] = "You must pick a total range.";
			}
			
			if(!is_numeric($_POST["number_of_bets"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if(!is_numeric($_POST["amount"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if($_POST["amount"] <= 0 || $_POST["number_of_bets"] <= 0){
				$response["results"] = "false";
				$response["errors"] = "Invalid entry.";
			}
			
			if($response["results"] != "false"){
				$trans_id = $core->make_customer_transaction(-($_POST['number_of_bets']*$_POST['amount']),46,"Fastballz Totals Bet");
				if(!is_numeric($trans_id)){
					$response["results"] = "false";
					$response["errors"] = $trans_id;
				}else{
					$q = "SELECT id FROM `rapidballs_drawing` ORDER BY id DESC LIMIT 1";
					$current_drawing_id = $db->queryOneRow($q);
					
					$total_amount = $_POST['number_of_bets']*$_POST['amount'];
					$q = "INSERT INTO `rapidballs_ticket` (`bet_type_id`, `transaction_id`, `bet_on`, `total_amount`, `number_of_bets`, `user_id`) VALUES(3, %i, %s, %d, %i, %i)";
					$ticket_id = $db->queryInsert($q, array($trans_id, $_POST["pick"], $total_amount, $_POST['number_of_bets'], $session->userinfo["id"]));
					
					for($x=0;$x<$_POST['number_of_bets'];$x++){
						$drawing_id = $current_drawing_id["id"] + $x + 1;
						$q = "INSERT INTO `rapidballs_bet` (`ticket_id`, `bet_type_id`, `bet_amount`, `drawing_id`, `user_session_id`, `transaction_id`) VALUES (%i, 3, %d, %i, %i, %i)";
						$db->queryInsert($q, array($ticket_id, $_POST['amount'], $drawing_id, $_SESSION['user_session_id'], $trans_id));
					}
					$response["results"] = "true";
					$response["errors"] = "";
				}
			}
			break;
		case 'post_reds':
			$_POST["amount"] = str_replace("$", "", $_POST["amount"]);
			
			if($_POST["pick"] == "" || ($_POST["pick"] != "green" && $_POST["pick"] != "purple" && $_POST["pick"] != "blue" && $_POST["pick"] != "yellow")){
				$response["results"] = "false";
				$response["errors"] = "You must pick a color.";
			}
			
			if(!is_numeric($_POST["number_of_bets"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if(!is_numeric($_POST["amount"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if($_POST["amount"] <= 0 || $_POST["number_of_bets"] <= 0){
				$response["results"] = "false";
				$response["errors"] = "Invalid entry.";
			}
			
			if($response["results"] != "false"){
				$trans_id = $core->make_customer_transaction(-($_POST['number_of_bets']*$_POST['amount']),46,"Fastballz Reds Bet");
				if(!is_numeric($trans_id)){
					$response["results"] = "false";
					$response["errors"] = $trans_id;
				}else{
					$q = "SELECT id FROM `rapidballs_drawing` ORDER BY id DESC LIMIT 1";
					$current_drawing_id = $db->queryOneRow($q);
					
					$total_amount = $_POST['number_of_bets']*$_POST['amount'];
					$q = "INSERT INTO `rapidballs_ticket` (`bet_type_id`, `transaction_id`, `bet_on`, `total_amount`, `number_of_bets`, `user_id`) VALUES(4, %i, %s, %d, %i, %i)";
					$ticket_id = $db->queryInsert($q, array($trans_id, $_POST["pick"], $total_amount, $_POST['number_of_bets'], $session->userinfo["id"]));
					
					for($x=0;$x<$_POST['number_of_bets'];$x++){
						$drawing_id = $current_drawing_id["id"] + $x + 1;
						$q = "INSERT INTO `rapidballs_bet` (`ticket_id`, `bet_type_id`, `bet_amount`, `drawing_id`, `user_session_id`, `transaction_id`) VALUES (%i, 4, %d, %i, %i, %i)";
						$db->queryInsert($q, array($ticket_id, $_POST['amount'], $drawing_id, $_SESSION['user_session_id'], $trans_id));
					}
					$response["results"] = "true";
					$response["errors"] = "";
				}
			}
			break;
		case 'post_lucky':
			$_POST["amount"] = str_replace("$", "", $_POST["amount"]);
			
			if($_POST["pick"] == "" || $_POST["pick"] < 1 || $_POST["pick"] > 19 || !is_numeric($_POST["pick"])){
				$response["results"] = "false";
				$response["errors"] = "You must pick a sum.";
			}
			
			if(!is_numeric($_POST["number_of_bets"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if(!is_numeric($_POST["amount"])){
				$response["results"] = "false";
				$response["errors"] = "The number of draws must be numeric.";	
			}
			
			if($_POST["amount"] <= 0 || $_POST["number_of_bets"] <= 0){
				$response["results"] = "false";
				$response["errors"] = "Invalid entry.";
			}
			
			if($response["results"] != "false"){
				$trans_id = $core->make_customer_transaction(-($_POST['number_of_bets']*$_POST['amount']),46,"Fastballz Lucky Bet");
				if(!is_numeric($trans_id)){
					$response["results"] = "false";
					$response["errors"] = $trans_id;
				}else{
					$q = "SELECT id FROM `rapidballs_drawing` ORDER BY id DESC LIMIT 1";
					$current_drawing_id = $db->queryOneRow($q);
					
					$total_amount = $_POST['number_of_bets']*$_POST['amount'];
					$q = "INSERT INTO `rapidballs_ticket` (`bet_type_id`, `transaction_id`, `bet_on`, `total_amount`, `number_of_bets`, `user_id`) VALUES(5, %i, %s, %d, %i, %i)";
					$ticket_id = $db->queryInsert($q, array($trans_id, $_POST["pick"], $total_amount, $_POST['number_of_bets'], $session->userinfo["id"]));
					
					for($x=0;$x<$_POST['number_of_bets'];$x++){
						$drawing_id = $current_drawing_id["id"] + $x + 1;
						$q = "INSERT INTO `rapidballs_bet` (`ticket_id`, `bet_type_id`, `bet_amount`, `drawing_id`, `user_session_id`, `transaction_id`) VALUES (%i, 5, %d, %i, %i, %i)";
						$db->queryInsert($q, array($ticket_id, $_POST['amount'], $drawing_id, $_SESSION['user_session_id'], $trans_id));
					}
					$response["results"] = "true";
					$response["errors"] = "";
				}
			}
			break;
		}
	
	echo json_encode($response);
