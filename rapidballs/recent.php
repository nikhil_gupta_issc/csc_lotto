

<link rel="stylesheet" type="text/css" href="/rapidballs/css/balls.css?v=<?=CSS_VERSION?>">
		<link rel="stylesheet" type="text/css" href="/rapidballs/css/global.css?v=<?=CSS_VERSION?>">
		<script type="text/javascript" src="/rapidballs/js/global.js?v=<?=JS_VERSION?>"></script>

		<script src="/lib/assets/animate_table_change/animator.js"></script>
		<script src="/lib/assets/animate_table_change/rankingTableUpdate.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				function get_recent_winners(){
					$.ajax({
						url: "/ajax/recent_winners.php",
						method: "POST",
						data:{
							type : 'recent',
							data : 'rapidballs'
						}
					})
					.done(function(response){
						if(response){
							var old_table = $('#recent_winners_list').html();
							$('#temp_recent_winners_list').html(response);
							var new_table = $('#temp_recent_winners_list').html();
							
							if(old_table != new_table){
								$('#recent_winners_list').rankingTableUpdate('<table class="table" id="recent_winners_list">'+new_table+'</table>', {
									duration: [1000, 0, 700, 0, 500],
									onComplete: function(){
										updating = false;
									},
									animationSettings: {
										up: {
											left: -25,
											backgroundColor: '#CCFFCC'
										},
										down: {
											left: 25,
											backgroundColor: '#8C008C'
										},
										fresh: {
											left: 0,
											backgroundColor: '#CCFFCC'
										},
										drop: {
											left: 0,
											backgroundColor: '#8C008C'
										}
									}
								});
							}
							setTimeout(get_recent_winners, 5000);
						}
					});
				}
				
				function get_big_winners(){
					$.ajax({
						url: "/ajax/recent_winners.php",
						method: "POST",
						data:{
							type : 'big',
							data : 'rapidballs'
						}
					})
					.done(function(response){
						if(response){
							var old_table = $('#big_winners_list').html();
							$('#temp_big_winners_list').html(response);
							var new_table = $('#temp_big_winners_list').html();
							
							if(old_table != new_table){
								$('#big_winners_list').rankingTableUpdate('<table class="table" id="big_winners_list">'+new_table+'</table>', {
									duration: [1000, 0, 700, 0, 500],
									onComplete: function(){
										updating = false;
									},
									animationSettings: {
										up: {
											left: -25,
											backgroundColor: '#CCFFCC'
										},
										down: {
											left: 25,
											backgroundColor: '#8C008C'
										},
										fresh: {
											left: 0,
											backgroundColor: '#CCFFCC'
										},
										drop: {
											left: 0,
											backgroundColor: '#8C008C'
										}
									}
								});
							}
							setTimeout(get_big_winners, 5000);
						}
					});
				}
				get_recent_winners();
				get_big_winners();
			});
		</script>
		<link rel="stylesheet" href="/lib/assets/FlipClock/flipclock.css">
	<script src="/lib/assets/FlipClock/flipclock.min.js"></script>
	
	<script type="text/javascript" src="/rapidballs/lib/assets/simplyscroll/jquery.simplyscroll.js"></script>
	<link rel="stylesheet" href="/rapidballs/lib/assets/simplyscroll/jquery.simplyscroll.css" media="all" type="text/css">
	<script type="text/javascript">
		(function($) {
			$(function() {
				$(".scroller").simplyScroll({
					orientation: 'vertical', 
					customClass: 'auto-scroll',
					auto: true,
					autoMode: 'bounce',
					direction: 'forwards'
				});
			});
		})(jQuery);

		var clock;

		$(document).ready(function() {
			var data_table_rapidballs = $('#datatable_rapidballs').DataTable({
				"bInfo" : false,
				"processing": true,
				"serverSide": true,
				"deferRender": true,
				"ajax": "/ajax/datatables/rapidballs_results.php",
				"columns": [
					{ "data": "datetime_drawn" },
					{ "data": "id" },
					{ "data": "drawing" },
					{ "data": "total" },
					{ "data": "lucky" }//,
					//{ "data": "seed_a" },
					//{ "data": "seed_b" },
					//{ "data": "hash" }
				],
				"order": [[0, 'desc']]
			});
			
			// Grab the current date
			var currentDate = new Date();

			// Calculate the difference in seconds between the future and current date
			var diff;
			start_counter = function(){
				$.ajax({
					url: "/rapidballs/ajax/rapidballs_countdown.php",
					method: "POST"
				})
				.done(function(response){
					console.log(response);
					diff = response;
					
					// Instantiate a coutdown FlipClock
					clock = $('#countdown_flipclock').FlipClock(diff, {
						clockFace: 'MinuteCounter',
						countdown: true,
						autoStart: false,
						callbacks: {
							stop: function() {
								start_counter();
							},
							interval: function () {
								var time = clock.getTime().time;
								// show warning style if near next draw
								if (time < 30) {
									if(!$("#countdown-contain").hasClass("ending-soon")){
										$("#countdown-contain").addClass("ending-soon");
									}
								}else{
									if($("#countdown-contain").hasClass("ending-soon")){
										$("#countdown-contain").removeClass("ending-soon");
									}
								}
								
								if(time >= 295){
									// check for up to 5 seconds after draw is supposed to happen
									update_drawings();
								}
							}
						}
					});
					
					clock.start();
				});
			};
			start_counter();
			
			function update_drawings(){
				// grab most recent drawing
				$.ajax({
					data: {
						size: "lg",
						limit: 1,
						type: 'normal'
					},
					url: "/rapidballs/ajax/rapidballs_recent_drawings.php",
					method: "POST",
					dataType: "json"
				})
				.done(function(response){
					$("#last-drawing").html(response.balls);
					$("#last-total").text(response.total);
					$("#last-lucky").text(response.lucky);
				});
			
				// update last 6 most recent
				$.ajax({
					data: {
						size: "sm",
						limit: 6,
						type: 'table'
					},
					url: "/rapidballs/ajax/rapidballs_recent_drawings.php",
					method: "POST"
				})
				.done(function(response){
					$("#recent-6").html(response);
				});
			}
			
			//Update the total on select change.
			$(document).on("change", "#number_of_draws", function(){
				var num_draws = parseFloat($(this).val());
					stake_per_draw = parseFloat($("#stake_per_draw").val().replace("$",""));
					
				$("#total").text((num_draws * stake_per_draw).toFixed(2));
			});
			
			$(document).on("change", "#stake_per_draw", function(){
				var num_draws = parseFloat($("#number_of_draws").val());
					stake_per_draw = parseFloat($(this).val().replace("$",""));
					
				$("#total").text((num_draws * stake_per_draw).toFixed(2));
			});
			
			// load initial recent drawings
			update_drawings();
		});
	</script>
		<style>
		#countdown-contain{
			padding: 15px;
			text-align: left;
			display: inline-block;
			
		}
		
		#last-drawing-contain{
			padding: 15px;
			text-align: left;
			display: inline-block;
			
	
		}
		
		.ending-soon{
			animation: blink .5s step-end infinite alternate;
		}
		
		@keyframes blink {
			50% { border: 2px solid red; } 
		}
		
		.auto-scroll .simply-scroll-clip {
			height: 220px;
		}
		table.dataTable tr.even {
		background-color: #2d2d2d;
		
		}
		table.dataTable th {
		background-color: #2d2d2d;
		
		}
		
	</style>

	<br>
	
		<div class="col-sm-12" style="background:#fff; padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 0px;">
			<div class="win-bg-plain">
				<h3 class="headings">Recent Drawings</h2>
			</div>
			<table id="datatable_rapidballs" class="display table dt-responsive win-list" cellspacing="0" width="100%" style="color:#FFF">
				<thead>
					<tr>
						<th class="text-left">Draw Time</th>
						<th class="text-left">Draw #</th>
						<th class="text-center">Drawing</th>
						<th class="text-right">Total</th>
						<th class="text-right">Lucky</th>
						<!--<th class="text-center">Seed A</th>-->
						<!--<th class="text-center">Seed B</th>-->
						<!--<th class="text-center">Hash</th>-->
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="6" class="dataTables_empty">Loading data from server</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="clearfix" style="background:#111111">
			<!--<div class="col-md-6 winners_board ">
				<div class="win-bg-plain">
					<h3 class="headings">Recent RapidBalls Winners</h2>
				</div>

				<table id='temp_recent_winners_list' style='display: none;'></table>
				
				<table class="table" id="recent_winners_list">
					<thead>
						<tr>
							<th class="position anim:id anim:number hidden" />
							<th class="driverName anim:number hidden" />
							<th class="pointsTotal anim:update hidden" />
							<th class="pointsTotal anim:update hidden" />
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div class="col-md-6 winners_board ">
				<div class="win-bg-plain">
					<h3 class="headings">Recent Big RapidBalls Winners</h2>
				</div>
				
				<table id='temp_big_winners_list' style='display: none;'></table>
				
				<table class="table" id="big_winners_list">
					<thead>
						<tr>
							<th class="position anim:id anim:number hidden" />
							<th class="driverName anim:number hidden" />
							<th class="pointsTotal anim:update hidden" />
							<th class="pointsTotal anim:update hidden" />
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>-->
		</div>
