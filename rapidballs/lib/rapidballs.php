<?php
	class RAPIDBALLS{
		private static $RNGseed = 0;
		
		public function generate_seed_a(){
			return random_int(0, PHP_INT_MAX);
		}
		
		public function generate_seed_b($length = 35){
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$randomString = '';
			for($i = 0; $i < $length; $i++){
				$randomString .= $characters[random_int(0, strlen($characters) - 1)];
			}
			return $randomString;
		}
		 
		public function seed($s = 0) {
                                 if($s == 0){
				RAPIDBALLS::$RNGseed = $this->generate_seed_a();
			}else{
				RAPIDBALLS::$RNGseed = $s;
			}
			srand(RAPIDBALLS::$RNGseed);
		}

		public function generate_random_integer($min=0, $max=PHP_INT_MAX, $pad_zeros = true){
			if(RAPIDBALLS::$RNGseed == 0){
				$this->seed();
			}
			$rnd_num = random_int($min, $max);
			if($pad_zeros == true){
				$num_digits = strlen((string)$max);
				$format_str = "%0".$num_digits."d";
				return sprintf($format_str, $rnd_num);
			}else{
				return $rnd_num;
			}
		}
		
		public function drawing_numbers($seed_a, $num_of_balls = 6){
			$this->seed($seed_a);
			$draw_numbers = array();
			for($i = 0; $i < $num_of_balls; $i++) {
				$number = ($this->generate_random_integer(1, 49));
				if(in_array($number, $draw_numbers)){
					$i = $i-1;
				}else{
					array_push($draw_numbers, $number);
				}
			}
			sort($draw_numbers);
			return $draw_numbers;
		}
		
		public function create_notification($user_id, $message){
			global $db;
			
			$now_dt = new DateTime;
			$now = $now_dt->format("Y-m-d H:i:s");
			
			$q = "INSERT INTO `notifications` (`id`, `user_id`, `message`, `is_dismissed`, `date_created`) VALUES (NULL, '".$user_id."', '".$message."', '0', '".$now."');";
			return $db->queryInsert($q);
		}

		public function draw_rapidballs($drawn_numbers = null){
			global $db;

			// get pre-determined seeds from db
			$q = "SELECT * FROM `rapidballs_next_drawing_seeds` WHERE `id`=1";
			$seed_info = $db->queryOneRow($q);
			
			if($drawn_numbers == null){
				$drawn_numbers = $this->drawing_numbers($seed_info['seed_a']);
			}
			$now = new DateTime();

			$total = 0;
			$payout = 0;
			foreach($drawn_numbers as $num){
				$total += $num;
			}
			
			// get sum of digits
			$lucky = 0;
			$digits = str_split($total);
			foreach($digits as $digit){
				$lucky += $digit;
			}
			
			$q = "INSERT INTO `rapidballs_drawing` (`datetime_drawn`, `ball_1`, `ball_2`, `ball_3`, `ball_4`, `ball_5`, `ball_6`, `total`, `lucky`, `seed_a`, `seed_b`, `hash`) VALUES (%s, %i, %i, %i, %i, %i, %i, %i, %i, %s, %s, %s)";
			$db->queryInsert($q, array($now->format("Y-m-d H:i:s"), $drawn_numbers[0], $drawn_numbers[1], $drawn_numbers[2], $drawn_numbers[3], $drawn_numbers[4], $drawn_numbers[5], $total, $lucky, $seed_info['seed_a'], $seed_info['seed_b'], $seed_info['hash']));
			
			// create seeds & hash for next drawing
			$seed_a = $this->generate_seed_a();
			$seed_b = $this->generate_seed_b();
			$hash = hash('sha1', $seed_a.$seed_b);
			$q = "REPLACE INTO `rapidballs_next_drawing_seeds` (`id`, `seed_a`, `seed_b`, `hash`) VALUES ('1', '".$seed_a."', '".$seed_b."', '".$hash."');";
			$db->queryDirect($q);
		}
		
		public function check_for_winners(){
			global $db;
			global $core;
			
			$now_dt = new DateTime();
			$now = $now_dt->format('Y-m-d H:i:s');
			
			// get all drawings that haven't been processed yet
			$q = "SELECT *,`rapidballs_bet`.`id` AS bet_id FROM `rapidballs_drawing` JOIN `rapidballs_bet` ON `rapidballs_bet`.`drawing_id` = `rapidballs_drawing`.`id` JOIN  `rapidballs_ticket` ON `rapidballs_ticket`.`id` = `rapidballs_bet`.`ticket_id` WHERE `rapidballs_bet`.`is_processed` = '0' AND `rapidballs_ticket`.`is_voided` = '0' ORDER BY `rapidballs_drawing`.`id` DESC";
			$unprocessed_drawings = $db->query($q);
			foreach($unprocessed_drawings as $draw){
				$bet_balls = explode('-', $draw['bet_on']);
				$winning_balls = array();
				$winning_balls[] = $draw['ball_1'];
				$winning_balls[] = $draw['ball_2'];
				$winning_balls[] = $draw['ball_3'];
				$winning_balls[] = $draw['ball_4'];
				$winning_balls[] = $draw['ball_5'];
				$winning_balls[] = $draw['ball_6'];
				
				$winning_colors = array();
				$winning_colors[] = $this->ball_color($draw['ball_1']);
				$winning_colors[] = $this->ball_color($draw['ball_2']);
				$winning_colors[] = $this->ball_color($draw['ball_3']);
				$winning_colors[] = $this->ball_color($draw['ball_4']);
				$winning_colors[] = $this->ball_color($draw['ball_5']);
				$winning_colors[] = $this->ball_color($draw['ball_6']);
				
				if($draw['bet_type_id'] == 1){
					// 1 Lotto Payouts

					// see if any of the balls match
					$matched = 0;
					$payout = 0;
					foreach($bet_balls as $bet){
						foreach($winning_balls as $winner){
							if($bet == $winner){
								$matched++;
							}
						}
					}
					
					if($matched == 2){
						//   Match 2 balls: Win 50% of Stake
						$payout = .5 * $draw['bet_amount'];
					}elseif($matched == 3){
						//   Match 3 balls: Win x50 Stake
						$payout = 50 * $draw['bet_amount'];
					}elseif($matched == 4){
						//   Match 4 balls: Win x600 Stake
						$payout = 600 * $draw['bet_amount'];
					}elseif($matched == 5){
						//   Match 5 balls: Win x8000 Stake
						$payout = 8000 * $draw['bet_amount'];
					}
					
					if($payout > 0){
						// payout winners
						$core->make_customer_transaction($payout, 47, "Fastballz Win - Lotto - Matched ".$matched." Balls", "NULL", $draw['user_id'], null, $draw['user_session_id']);
						
						// mark bet as winner and set payout amount
						$q = "UPDATE `rapidballs_bet` SET `is_winner` = '1' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `winner_details` = 'Matched ".$matched." Balls' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `payout` = '".$payout."' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						
						// create notification of this win
						$this->create_notification($draw['user_id'], '<b>Congratulations!</b> Your recent RapidBalls: Lotto bet matched '.$matched.' balls and won '.number_format((float)$payout). ' ' . CURRENCY_FORMAT.'.');
					}
					
					$winner = implode("-", $winning_balls);
					echo "Processing $".$draw['bet_amount']." bet on ".$draw['bet_on'].".<br>Winner: ".$winner.".<br>Matched $matched numbers.<br>Payout: $".$payout."<br><br>";
					
					// mark it as processed
					$q = "UPDATE `rapidballs_bet` SET `is_processed` = '1' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
					$q = "UPDATE `rapidballs_bet` SET `processed_datetime` = '".$now."' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
				}elseif($draw['bet_type_id'] == 2){
					// 2 Colors Payouts
					$bet_arr = explode('-', $draw['bet_on']);
					$bet_count = $bet_arr[0];
					$bet_color = $bet_arr[1];

					// see if any of the balls match
					$matched = 0;
					$payout = 0;
					
					foreach($winning_colors as $winner){
						if($winner == $bet_color){
							$matched++;
						}
					}
					
					if($matched == 0 && $bet_count == 0){
						//   Match 0 Colors: Win x3.3 Stake
						$payout = 3.3 * $draw['bet_amount'];
					}elseif($matched == 3 && $bet_count == 3){
						//   Match 3 Colors: Win x9.6 Stake
						$payout = 9.6 * $draw['bet_amount'];
					}elseif($matched == 4 && $bet_count == 4){
						//   Match 4 Colors: Win x70 Stake
						$payout = 70 * $draw['bet_amount'];
					}elseif($matched == 5 && $bet_count == 5){
						//   Match 5 Colors: Win x1100 Stake
						$payout = 1100 * $draw['bet_amount'];
					}elseif($matched == 6 && $bet_count == 6){
						//   Match 6 Colors: Win x52000 Stake
						$payout = 52000 * $draw['bet_amount'];
					}
					
					if($payout > 0){
						// payout winners
						$core->make_customer_transaction($payout, 47, "Fastballz Win - Colors - Matched ".$matched." ".$bet_color, "NULL", $draw['user_id'], null, $draw['user_session_id']);
						
						// mark bet as winner and set payout amount
						$q = "UPDATE `rapidballs_bet` SET `is_winner` = '1' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `winner_details` = 'Matched ".$matched." Colors' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `payout` = '".$payout."' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						
						// create notification of this win
						$this->create_notification($draw['user_id'], '<b>Congratulations!</b> Your recent RapidBalls: Color bet matched '.$matched.' '.$bet_color.' and won '.number_format((float)$payout). ' ' . CURRENCY_FORMAT.'.');
					}
					
					$win_col = implode("-", $winning_colors);
					echo "Processing $".$draw['bet_amount']." bet on ".$draw['bet_on'].".<br>Winner: ".$win_col.".<br>Matched $matched $bet_color.<br>Payout: $".$payout."<br><br>";
					
					// mark it as processed
					$q = "UPDATE `rapidballs_bet` SET `is_processed` = '1' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
					$q = "UPDATE `rapidballs_bet` SET `processed_datetime` = '".$now."' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
				}elseif($draw['bet_type_id'] == 3){
					// 3 Totals Payouts
					
					$bet_arr = explode('-', $draw['bet_on']);
					$bet_min = $bet_arr[0];
					$bet_max = $bet_arr[1];
					
					// see if any of the balls match
					$total = 0;
					$payout = 0;
					foreach($winning_balls as $win){
						$total += $win;
					}
					
					if($bet_min >= 21 && $bet_max <= 99 && $total >= $bet_min && $total <= $bet_max){
						//   Hit 21-99:		Win x12 Stake
						$payout = 12 * $draw['bet_amount'];
					}elseif($bet_min >= 100 && $bet_max <= 150 && $total >= $bet_min && $total <= $bet_max){
						//   Hit 100-150:	Win x2 Stake
						$payout = 2 * $draw['bet_amount'];
					}elseif($bet_min >= 151 && $bet_max <= 200 && $total >= $bet_min && $total <= $bet_max){
						//   Hit 151-200:	Win x2 Stake
						$payout = 2 * $draw['bet_amount'];
					}elseif($bet_min >= 201 && $bet_max <= 250 && $total >= $bet_min && $total <= $bet_max){
						//   Hit 201-250:	Win x12.5 Stake
						$payout = 12.5 * $draw['bet_amount'];
					}elseif($bet_min >= 251 && $bet_max <= 279 && $total >= $bet_min && $total <= $bet_max){
						//   Hit 251-279:	Win x1800 Stake
						$payout = 1800 * $draw['bet_amount'];
					}
					
					if($payout > 0){
						// payout winners
						$core->make_customer_transaction($payout, 47, "Fastballz Win - Totals - Matched ".$draw['bet_on'], "NULL", $draw['user_id'], null, $draw['user_session_id']);
						
						// mark bet as winner and set payout amount
						$q = "UPDATE `rapidballs_bet` SET `is_winner` = '1' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `winner_details` = 'Matched ".$draw['bet_on']."' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `payout` = '".$payout."' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						
						// create notification of this win
						$this->create_notification($draw['user_id'], '<b>Congratulations!</b> Your recent RapidBalls: Totals bet matched '.$matched.' and won '.number_format((float)$payout). ' ' . CURRENCY_FORMAT.'.');
					}
					
					$win_tot = implode("-", $winning_balls);
					echo "Processing $".$draw['bet_amount']." bet on ".$draw['bet_on'].".<br>Winner: ".$win_tot.".<br>Total $total.<br>Payout: $".$payout."<br><br>";
					
					// mark it as processed
					$q = "UPDATE `rapidballs_bet` SET `is_processed` = '1' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
					$q = "UPDATE `rapidballs_bet` SET `processed_datetime` = '".$now."' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
				}elseif($draw['bet_type_id'] == 4){
					// 4 Reds Payouts

					// see if any of the balls match
					$red_count = 0;
					$bet_count = 0;
					$payout = 0;
					foreach($winning_colors as $winner){
						if($winner == $draw['bet_on']){
							$bet_count++;
						}
						if($winner == 'red'){
							$red_count++;
						}
					}
					
					if($red_count > $bet_count && $bet_count > 0){
						//   Play Blues:	Win x6 Stake
						//   Play Yellows:	Win x6 Stake
						//   Play Purples:	Win x6 Stake
						//   Play Greens:	Win x6 Stake
						$payout = 6 * $draw['bet_amount'];
					}
					
					if($payout > 0){
						// payout winners
						$core->make_customer_transaction($payout, 47, "Fastballz Win - Reds - Matched ".$bet_count." ".$draw['bet_on'], "NULL", $draw['user_id'], null, $draw['user_session_id']);
						
						// mark bet as winner and set payout amount
						$q = "UPDATE `rapidballs_bet` SET `is_winner` = '1' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `winner_details` = 'Matched ".$bet_count." ".$draw['bet_on']."' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `payout` = '".$payout."' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						
						// create notification of this win
						$this->create_notification($draw['user_id'], '<b>Congratulations!</b> Your recent RapidBalls: Reds bet matched '.$bet_count." ".$draw['bet_on'].' and won '.number_format((float)$payout). ' ' . CURRENCY_FORMAT.'.');
					}
					
					$win_col = implode("-", $winning_colors);
					$win_tot = implode("-", $winning_balls);
					echo "Processing $".$draw['bet_amount']." bet on ".$draw['bet_on'].".<br>Winner: ".$win_col.".<br>Reds: ".$red_count.".<br>Matched: ".$bet_count." ".$draw['bet_on'].".<br>Payout: $".$payout."<br><br>";
					
					// mark it as processed
					$q = "UPDATE `rapidballs_bet` SET `is_processed` = '1' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
					$q = "UPDATE `rapidballs_bet` SET `processed_datetime` = '".$now."' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
				}elseif($draw['bet_type_id'] == 5){
					// 5 Lucky
					
					// calculate total
					$total = 0;
					$payout = 0;
					foreach($winning_balls as $win){
						$total += $win;
					}
					
					// get sum of digits
					$sum = 0;
					$digits = str_split($total);
					foreach($digits as $digit){
						$sum += $digit;
					}
					
					if($sum == 1 && $draw['bet_on'] == 1){
						//   Number 1:	Win x205 Stake
						$payout = 205 * $draw['bet_amount'];
					}elseif($sum == 2 && $draw['bet_on'] == 2){
						//   Number 2:	Win x56 Stake
						$payout = 56 * $draw['bet_amount'];
					}elseif($sum == 3 && $draw['bet_on'] == 3){
						//   Number 3:	Win x32 Stake
						$payout = 32 * $draw['bet_amount'];
					}elseif($sum == 4 && $draw['bet_on'] == 4){
						//   Number 4:	Win x21 Stake
						$payout = 21 * $draw['bet_amount'];
					}elseif($sum == 5 && $draw['bet_on'] == 5){
						//   Number 5:	Win x16 Stake
						$payout = 16 * $draw['bet_amount'];
					}elseif($sum == 6 && $draw['bet_on'] == 6){
						//   Number 6:	Win x13 Stake
						$payout = 13 * $draw['bet_amount'];
					}elseif($sum == 7 && $draw['bet_on'] == 7){
						//   Number 7:	Win x11 Stake
						$payout = 11 * $draw['bet_amount'];
					}elseif($sum == 8 && $draw['bet_on'] == 8){
						//   Number 8:	Win x9 Stake
						$payout = 9 * $draw['bet_amount'];
					}elseif($sum == 9 && $draw['bet_on'] == 9){
						//   Number 9:	Win x8 Stake
						$payout = 8 * $draw['bet_amount'];
					}elseif($sum == 10 && $draw['bet_on'] == 10){
						//   Number 10:	Win x7.5 Stake
						$payout = 7.5 * $draw['bet_amount'];
					}elseif($sum == 11 && $draw['bet_on'] == 11){
						//   Number 11:	Win x8 Stake
						$payout = 8 * $draw['bet_amount'];
					}elseif($sum == 12 && $draw['bet_on'] == 12){
						//   Number 12:	Win x9 Stake
						$payout = 9 * $draw['bet_amount'];
					}elseif($sum == 13 && $draw['bet_on'] == 13){
						//   Number 13:	Win x10.5 Stake
						$payout = 10.5 * $draw['bet_amount'];
					}elseif($sum == 14 && $draw['bet_on'] == 14){
						//   Number 14:	Win x12.5 Stake
						$payout = 12.5 * $draw['bet_amount'];
					}elseif($sum == 15 && $draw['bet_on'] == 15){
						//   Number 15:	Win x16 Stake
						$payout = 16 * $draw['bet_amount'];
					}elseif($sum == 16 && $draw['bet_on'] == 16){
						//   Number 16:	Win x22 Stake
						$payout = 22 * $draw['bet_amount'];
					}elseif($sum == 17 && $draw['bet_on'] == 17){
						//   Number 17:	Win x32 Stake
						$payout = 32 * $draw['bet_amount'];
					}elseif($sum == 18 && $draw['bet_on'] == 18){
						//   Number 18:	Win x54 Stake
						$payout = 54 * $draw['bet_amount'];
					}elseif($sum == 19 && $draw['bet_on'] == 19){
						//   Number 19:	Win x190 Stake
						$payout = 190 * $draw['bet_amount'];
					}
					
					if($payout > 0){
						// payout winners
						$core->make_customer_transaction($payout, 47, "Fastballz Win - Lucky - Matched Sum of ".$sum, "NULL", $draw['user_id'], null, $draw['user_session_id']);
						
						// mark bet as winner and set payout amount
						$q = "UPDATE `rapidballs_bet` SET `is_winner` = '1' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `winner_details` = 'Matched Sum of ".$sum."' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						$q = "UPDATE `rapidballs_bet` SET `payout` = '".$payout."' WHERE `id` = '".$draw['bet_id']."'";
						$db->query($q);
						
						// create notification of this win
						$this->create_notification($draw['user_id'], '<b>Congratulations!</b> Your recent RapidBalls: Lucky bet matched '.$sum.' and won '.number_format((float)$payout). ' ' . CURRENCY_FORMAT.'.');
					}
					
					$win_tot = implode("-", $winning_balls);
					echo "Processing $".$draw['bet_amount']." bet on ".$draw['bet_on'].".<br>Winner: ".$win_tot.".<br>Sum: ".$sum.".<br>Payout: $".$payout."<br><br>";
					
					// mark it as processed
					$q = "UPDATE `rapidballs_bet` SET `is_processed` = '1' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
					$q = "UPDATE `rapidballs_bet` SET `processed_datetime` = '".$now."' WHERE `id` = '".$draw['bet_id']."'";
					$db->query($q);
				}
			}
		}
		
		public function ball_color($ball_number){
			if($ball_number >= 1 && $ball_number <= 9){
				return "red";
			}elseif($ball_number >= 10 && $ball_number <= 19){
				return "blue";
			}elseif($ball_number >= 20 && $ball_number <= 29){
				return "yellow";
			}elseif($ball_number >= 30 && $ball_number <= 39){
				return "purple";
			}elseif($ball_number >= 40 && $ball_number <= 49){
				return "green";
			}else{
				return "unknown";
			}
		}
	}
