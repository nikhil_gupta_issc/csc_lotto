<script>
	$(document).ready(function(){
		//Track pick number
		var pick = 1;
		
		$(document).on("click", ".pick_number_row .ball", function(){
			if(!$(this).hasClass("selected")){
				if(pick > 5){
					bootbox.alert("You can not choose any more balls.");
				}else{
					$("#pick-"+pick).attr("class", $(this).attr("class"));
					$("#pick-"+pick).removeClass("ball-lg");
					$("#pick-"+pick).addClass("ball-sm");
					$(this).addClass("selected");
					pick++;	
				}
			}
		});
		
		$(document).on("click", "#submit-lotto", function(){
			$(this).prop("disabled", true);
			var holdThis = this;
			
			$.ajax({
				url: "/rapidballs/ajax/post_bets.php",
				type: "POST",
				data: {
					action : "post_lotto",
					pick_1 : $("#pick-1").attr("class"),
					pick_2 : $("#pick-2").attr("class"),
					pick_3 : $("#pick-3").attr("class"),
					pick_4 : $("#pick-4").attr("class"),
					pick_5 : $("#pick-5").attr("class"),
					amount : $("#stake_per_draw").val(),
					number_of_bets : $("#number_of_draws").val()
				},
				dataType: "json",
				success: function(data){
					// hide buttons and reload table if successful
					if(data["results"] == "false"){
						bootbox.alert(data["errors"]);
					}else{
						bootbox.alert("Drawings successfully scheduled!");
						
						// clear selection
						$("#number_of_draws").val("");
						$("#stake_per_draw").val("");
						  $("#total").text("0");
						$("#clear_pick").trigger("click");
					}
					
					$(holdThis).removeProp("disabled");
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			}); 
		});
		
		$(document).on("click", "#clear_pick", function(){
			for(var x = 1; x <= 5; x++){
				$("#pick-"+x).attr("class", "");
				$("#pick-"+x).addClass("ball");
				$("#pick-"+x).addClass("ball-sm");
				$("#pick-"+x).addClass("ball-"+x+"-grey");
			}
			
			$(".selected").removeClass("ball-lg");
			$(".selected").addClass("ball-sm");
			$(".selected").removeClass("selected");
			
			//Set pick back to 1
			pick = 1;
		});
	});
</script>
<?php
 $select_ip = "select count(id) as num from ipaddress where status=1 AND ipaddress='".$_SERVER["REMOTE_ADDR"]."'";
 $ip = $db->query($select_ip);
// if($ip[0]['num'] >0){
 ?>


<div class="clearfix" style="background:#fff;padding-top:20px; padding-bottom: 10px;">	
<div class="col-sm-12">
</div>
<div class="col-sm-1">
	<p>&nbsp;</p>
</div>

<div class="col-sm-10" style="background-color:#000;border-radius: 13px 13px 13px 13px;-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;-webkit-border-top-right-radius: 13px;-webkit-border-bottom-right-radius: 13px;-webkit-border-bottom-left-radius: 13px;padding:15px;color:#fff;">				
			
	<div class="col-sm-8">
		<h1>Lotto</h1>
		<p>We hold a draw of 6 numbered balls every 5 Minutes from a selection of 49 balls, if you place a stake and correctly guess 3, 4 or 5 of these balls, you will win a payout. We will also pay you 50% of your initial stake if you hit 2 of the balls.</p>

		<h2>Pick Your Numbers</h2>
		<p>&nbsp;</p>
		<div class="col-sm-12">
			<div class="pick_number_row clearfix">
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-1"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-2"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-3"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-4"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-5"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-6"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-7"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-8"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-9"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
				
				</div>
			</div>
			<div class="pick_number_row clearfix">
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-10"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-11"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-12"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-13"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-14"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-15"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-16"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-17"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-18"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-19"></span>
				</div>
			</div>
			<div class="pick_number_row clearfix">
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-20"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-21"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-22"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-23"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-24"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-25"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-26"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-27"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-28"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-29"></span>
				</div>
			</div>
			<div class="pick_number_row clearfix">
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-30"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-31"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-32"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-33"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-34"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-35"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-36"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-37"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-38"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-39"></span>
				</div>
			</div>
			<div class="pick_number_row clearfix">
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-40"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-41"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-42"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-43"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-44"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-45"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-46"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-47"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-48"></span>
				</div>
				<div class="lotto-ball-container growing-ball-container">
					<span class="ball ball-sm ball-49"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<h2>Your Pick</h2>
		<div>
			<button id="clear_pick" class="btn btn-primary">Clear</button>
		</div>
		<div id="lotto-pick-balls" class="clearfix">
			<div class="ball-container">
				<span class="ball ball-sm ball-1-grey" id="pick-1"></span>
			</div>
			<div class="ball-container">
				<span class="ball ball-sm ball-2-grey" id="pick-2"></span>
			</div>
			<div class="ball-container">
				<span class="ball ball-sm ball-3-grey" id="pick-3"></span>
			</div>
			<div class="ball-container">
				<span class="ball ball-sm ball-4-grey" id="pick-4"></span>
			</div>
			<div class="ball-container">
				<span class="ball ball-sm ball-5-grey" id="pick-5"></span>
			</div>
		</div>
		<div id="lotto-pick-form">
			<form>
				<div class="form-group">
					<label for="number_of_draws">Number of Draws</label>
					<select class="form-control" id="number_of_draws"><option value="1">1 Draw<option value="5">5 Draws<option value="10">10 Draws<option value="50">50 Draws<option value="100">100 Draws<option value="288">288 Draws (1 Day)</select>
				</div>
				<div class="form-group">
					<label for="stake_per_draw">Stake Per Draw</label>
					<select class="form-control" id="stake_per_draw"><option value="$1.00">1 CSC<option value="$2.00">2 CSC<option value="$5.00">5 CSC<option value="$10.00">10 CSC</select>
				</div>
				<h3>Total: <span id="total">1</span> CSC</h3>
				<button type="button" id="submit-lotto" class="btn btn-lg btn-primary">Purchase</button>
			</form>
		</div>
	</div>
</div>
<div class="col-sm-1">
	<p>&nbsp;</p>
</div>
</div>


<div class="bg">
		<div class="col-sm-12">
	<p>&nbsp;</p>
</div>	
				<div class="col-sm-1"></div>
				<div class="col-sm-4" style=" background-color:#CCC; 			border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px; -webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;">
					<h3>Lotto Payouts</h3>
					<table class="table table-striped">
						<tr>
							<td>Match 2 balls: Win 50% of Stake</td>
						</tr>
						<tr>
							<td>Match 3 balls: Win x50 Stake</td>
						</tr>
						<tr>
							<td>Match 4 balls: Win x600 Stake</td>
						</tr>
						<tr>
							<td>Match 5 balls: Win x8000 Stake</td>
						</tr>
						
					</table>
					<p>Theoretical RTP: 75%</p>
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-5" style="background-color:#000;border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;-webkit-border-radius: 13px;-webkit-border-top-right-radius: 13px;-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;color:#fff;">				
					<?php include('./rapidballs/inc_latestDraw.php'); ?>
					
				</div>
			<div class="col-sm-1"></div>
				
		</div>
		<?php 
//}
 ?>
