<script>
	$(document).ready(function(){
		$(document).on("click", ".ball", function(){
			$("#pick-1").attr("class", $(this).attr("class"));
			$("#pick-1").removeClass("ball-lg");
			$("#pick-1").addClass("ball-sm");
			$("#pick-1").attr("pick", $(this).attr("pick"));
		});
		
		$(document).on("click", "#submit-totals", function(){
			$(this).prop("disabled", true);
			var holdThis = this;
			
			$.ajax({
				url: "/rapidballs/ajax/post_bets.php",
				type: "POST",
				data: {
					action : "post_totals",
					pick : $("#pick-1").attr("pick"),
					amount : $("#stake_per_draw").val(),
					number_of_bets : $("#number_of_draws").val()
				},
				dataType: "json",
				success: function(data){
					// hide buttons and reload table if successful
					if(data["results"] == "false"){
						bootbox.alert(data["errors"]);
					}else{
						bootbox.alert("Drawings successfully scheduled!");
						
						// clear selection
						$("#number_of_draws").val("");
						$("#stake_per_draw").val("");
						  $("#total").text("0");
						$("#clear_pick").trigger("click");
					}
					
					$(holdThis).removeProp("disabled");
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
		});
		
		$(document).on("click", "#clear_pick", function(){
			for(var x = 1; x <= 5; x++){
				$("#pick-"+x).attr("class", "");
				$("#pick-"+x).attr("pick", "");
				$("#pick-"+x).addClass("ball");
				$("#pick-"+x).addClass("ball-xl");
				$("#pick-"+x).addClass("ball-grey-total");
			}
		});
	});
</script>
<?php
 $select_ip = "select count(id) as num from ipaddress where status=1 AND ipaddress='".$_SERVER["REMOTE_ADDR"]."'";
 $ip = $db->query($select_ip);
// if($ip[0]['num'] >0){
 ?>




<div class="clearfix" style="background:#fff">	
<div class="col-sm-12">
	<p>&nbsp;</p>
</div>
<div class="col-sm-1">
	<p>&nbsp;</p>
</div>

<div class="col-sm-10" style="background-color:#000; border-radius: 13px 13px 13px 13px; -moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px; -webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px; -webkit-border-bottom-left-radius: 13px;padding:15px;color:#fff">
	<div class="col-sm-8">
		<h1>Totals</h1>
		<p>If you add up all the numbers shown on the 6 balls drawn this will give you a Total amount (11+17+29+33+37+41 = 168). To play you place a stake and select which Total range the Total amount will be within, and if correct, you will win a payout.</p>
	<p>&nbsp;</p>
		<div class="col-sm-10">
			<div class="center">
				<div class="xl-block-container clearfix">
					<div class="ball-container total-container">
						<span class="ball ball-xl ball-1-total" pick="21-99"></span>
					</div>
					<div class="ball-container total-container">
						<span class="ball ball-xl ball-2-total" pick="100-150"></span>
					</div>
					<div class="ball-container total-container">
						<span class="ball ball-xl ball-3-total" pick="151-200"></span>
					</div>
				</div>
			</div>
			<div class="center">
				<div class="xl-block-container clearfix">
					<div class="ball-container total-container">
						<span class="ball ball-xl ball-4-total" pick="201-250"></span>
					</div>
					<div class="ball-container total-container">
						<span class="ball ball-xl ball-5-total" pick="251-279"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<h2>Your Pick</h2>
		<div>
			<button id="clear_pick" class="btn btn-primary">Clear</button>
		</div>
		<div id="lotto-pick-balls" class="clearfix">
			<div class="ball-container">
				<span class="ball ball-xl ball-grey-total" id="pick-1"></span>
			</div>
		</div>
		<div id="lotto-pick-form">
			<form>
				<div class="form-group">
					<label for="number_of_draws">Number of Draws</label>
					<select class="form-control" id="number_of_draws"><option value="1">1 Draw<option value="5">5 Draws<option value="10">10 Draws<option value="50">50 Draws<option value="100">100 Draws<option value="288">288 Draws (1 Day)</select>
				</div>
				<div class="form-group">
					<label for="stake_per_draw">Stake Per Draw</label>
					<select class="form-control" id="stake_per_draw"><option value="$1.00">1 CSC<option value="$2.00">2 CSC<option value="$5.00">5 CSC<option value="$10.00">10 CSC</select>
				</div>
				<h3>Total: <span id="total">1</span> CSC</h3>
				<button type="button" class="btn btn-lg btn-primary" id="submit-totals">Purchase</button>
			</form>
		</div>
	</div>
</div>
<div class="col-sm-1">
	<p>&nbsp;</p>
</div>
</div>


<div class="bg">
		<div class="col-sm-12">
	<p>&nbsp;</p>
</div>	
				<div class="col-sm-1"></div>
				<div class="col-sm-4" style=" background-color:#CCC; 			border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;">
					<h3>Totals Payouts</h3>
					<table class="table table-striped">
						<tr>
							<td>Hit 21-99:		Win x12 Stake</td>
						</tr>
						<tr>
							<td>Hit 100-150:	Win x2 Stake</td>
						</tr>
						<tr>
							<td>Hit 151-200:	Win x2 Stake</td>
						</tr>
						<tr>
							<td>Hit 201-250:	Win x12.5 Stake</td>
						</tr>
						<tr>
							<td>Hit 251-279:	Win x1800 Stake</td>
						</tr>
					</table>
					<p>Theoretical RTP: Ranges From 75-89%</p>
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-5" style="background-color:#000; 			border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;color:#fff">				
					<?php include('./rapidballs/inc_latestDraw.php'); ?>
					
				</div>
			<div class="col-sm-1"></div>
				
		</div>
		<?php
// }
 ?>
