<script>
	$(document).ready(function(){
		$(document).on("click", ".ball", function(){
			$("#pick-1").attr("class", $(this).attr("class"));
			$("#pick-1").attr("pick", $(this).attr("pick"));
			$("#pick-1").removeClass("ball-xl");
			$("#pick-1").addClass("ball-reds");
		});
		
		$(document).on("click", "#submit-reds", function(){
			$(this).prop("disabled", true);
			var holdThis = this;
			
			$.ajax({
				url: "/rapidballs/ajax/post_bets.php",
				type: "POST",
				data: {
					action : "post_reds",
					pick : $("#pick-1").attr("pick"),
					amount : $("#stake_per_draw").val(),
					number_of_bets : $("#number_of_draws").val()
				},
				dataType: "json",
				success: function(data){
					// hide buttons and reload table if successful
					if(data["results"] == "false"){
						bootbox.alert(data["errors"]);
					}else{
						bootbox.alert("Drawings successfully scheduled!");
						
						// clear selection
						$("#number_of_draws").val("");
						$("#stake_per_draw").val("");
						  $("#total").text("0");
						$("#clear_pick").trigger("click");
					}
					
					$(holdThis).removeProp("disabled");
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
				}
			});
		});
		
		$(document).on("click", "#clear_pick", function(){
			for(var x = 1; x <= 5; x++){
				$("#pick-"+x).attr("class", "");
				$("#pick-"+x).attr("pick", "");
				$("#pick-"+x).addClass("ball");
				$("#pick-"+x).addClass("ball-reds");
				$("#pick-"+x).addClass("ball-grey-reds");
			}
		});
	});
</script>
<?php
 $select_ip = "select count(id) as num from ipaddress where status=1 AND ipaddress='".$_SERVER["REMOTE_ADDR"]."'";
 $ip = $db->query($select_ip);
 //if($ip[0]['num'] >0){
 ?>
<div class="clearfix" style="background:#fff">	

<div class="col-sm-1">
	<p>&nbsp;</p>
</div>

<div class="col-sm-10" style="margin-top: 20px;background-color:#000; 			border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;color:#fff;">				
			
	<div class="col-sm-8">
		<h1>Reds</h1>
		<p>Reds is the game that allows you to place a stake against a draw having more red balls than another color (Blue, Yellow, Purple, Green) and you win if you guess correctly, providing there is at least 1 of that color in the draw.</p>
		<p>&nbsp;</p>
		<div class="col-sm-10">
			<div class="center">
				<div class="xl-block-container clearfix">
					<div class="ball-container reds-container">
						<span class="ball ball-reds ball-1-reds" pick="blue"></span>
					</div>
					<div class="ball-container reds-container">
						<span class="ball ball-reds ball-2-reds" pick="yellow"></span>
					</div>
					<div class="ball-container reds-container">
						<span class="ball ball-reds ball-3-reds" pick="purple"></span>
					</div>
					<div class="ball-container reds-container">
						<span class="ball ball-reds ball-4-reds" pick="green"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-4 addpadding100">
		<h2>Your Pick</h2>
		<div>
			<button id="clear_pick" class="btn btn-primary">Clear</button>
		</div>
		<div id="lotto-pick-balls" class="clearfix">
			<div class="ball-container">
				<span class="ball ball-reds ball-grey-reds" id="pick-1" pick=""></span>
			</div>
		</div>
		<div id="lotto-pick-form">
			<form>
				<div class="form-group">
					<label for="number_of_draws">Number of Draws</label>
					<select class="form-control" id="number_of_draws"><option value="1">1 Draw<option value="5">5 Draws<option value="10">10 Draws<option value="50">50 Draws<option value="100">100 Draws<option value="288">288 Draws (1 Day)</select>
				</div>
				<div class="form-group">
					<label for="stake_per_draw">Stake Per Draw</label>
					<select class="form-control" id="stake_per_draw"><option value="$1.00">1 CSC<option value="$2.00">2 CSC<option value="$5.00">5 CSC<option value="$10.00">10 CSC</select>
				</div>
				<h3>Total: <span id="total">1</span> CSC</h3>
				<button type="button" class="btn btn-lg btn-primary" id="submit-reds">Purchase</button>
			</form>
		</div>
	</div>
</div>

</div>


<div class="bg">
		<div class="col-sm-12">
	<p>&nbsp;</p>
</div>	
				<div class="col-sm-1"></div>
				<div class="col-sm-4" style=" background-color:#CCC; 			border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;">
					<h3>Reds Payouts</h3>
					<table class="table table-striped">
						<tr>
							<td>Play Blues:	Win x6 Stake</td>
						</tr>
						<tr>
							<td>Play Yellows:	Win x6 Stake</td>
						</tr>
						<tr>
							<td>Play Purples:	Win x6 Stake</td>
						</tr>
						<tr>
							<td>Play Greens:	Win x6 Stake</td>
						</tr>
					</table>
					<p>Theoretical RTP: 85%</p>
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-5 addmargin20" style="background-color:#000; 			border-radius: 13px 13px 13px 13px;	-moz-border-radius: 13px 13px 0px 0px;	-webkit-border-radius: 13px;	-webkit-border-top-right-radius: 13px;	-webkit-border-bottom-right-radius: 13px;	-webkit-border-bottom-left-radius: 13px;padding:15px;color:#fff;">				
					<?php include('./rapidballs/inc_latestDraw.php'); ?>
					
				</div>
			<div class="col-sm-1"></div>
				
		</div>
		<?php 
//} 
?>
