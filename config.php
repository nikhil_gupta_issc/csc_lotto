<?php
	define('DOMAIN_ROOT', '/var/www/html');
	//define('DOMAIN_ROOT', '/var/www/html');

	// Include core config
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/core_config.php");
	}else{
		include(DOMAIN_ROOT.'/core_config.php');
	}

	// Include core class
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/lib/framework/core.php");
	}else{
		include(DOMAIN_ROOT.'/lib/framework/core.php');
	}
	$core = new CORE();

	// Include messaging class
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/lib/framework/message.php");
	}else{
		include(DOMAIN_ROOT.'/lib/framework/message.php');
	}
	//$message = new MESSAGE();

	// Include security handler
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/lib/framework/security.php");
	}
