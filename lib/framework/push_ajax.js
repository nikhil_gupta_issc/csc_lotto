	 var idleState = false;
	 var idleState_flag = true;
   
    function createCookie(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}

	function eraseCookie(name) {
		createCookie(name,"",-1);
	}
	
	function updateTime(cur_date_time){
        /* Simple helper to add a div.
        type is the name of a CSS class (old/new/error).
        msg is the contents of the div */
        $("#current_date_time").replaceWith("<div id='current_date_time'>"+cur_date_time+"</div>");
    }
	
	function display_alert(id, title, message, type, url){
		$.fx.speeds._default = 1000;
        $("#display_alert_"+id).html(message);
		$("#display_alert_"+id).prop('title', title);
        $(function(){
			$("#display_alert_"+id).dialog({
				height: 240,
				width: 400,
				modal: false,
				hide: 'fade',
				buttons: {
					"Close": function() {
						$(this).dialog("close");
						if(url){
							// got to url specified
							parent.window.location=url;
						}
					}
				},
				beforeClose: function() {
					if(url){
						// got to url specified
						parent.window.location=url;
					}
				}
			});
		});
    }

	function display_notifications(){
		// show notifications
		var i = 0;
		$(".notification").each(function (){
			$(this).delay(4000*i).slideDown();
			i++;
		});
		
		// hide alerts
		var hide_delay = 4000;  // starting timeout before first message is hidden
		var hide_next = 4000;   // time in mS to wait before hiding next message
		$(".notification").each(function(index, el) {
			window.setTimeout( function(){
				$(el).addClass('notification-shown');
				$(el).slideUp();  // hide the message
				$(el).remove();
			}, hide_delay + hide_next*index);
		});
    }
	
	function show_recent_rapidballs_drawing(){

		// see if there is a new rapidballs drawing to show
		$.ajax({
			data: {
				size: "sm",
				limit: 1,
				type: 'notification'
			},
			url: "/rapidballs/ajax/rapidballs_recent_drawings.php",
			method: "POST"
		})
		.done(function(response){
			if(response.length > 1){
var substrings = response.split('winner');
				   if(substrings.length-1 > 0){
				       var ajax_cl = 'rapid_winner_notification';
				   } else {
				       var ajax_cl = 'rapid_notification';
				   }
				   
				// insert alert div container into page
				$('#ajax-notifications-contain').prepend('<div id="rapidball-winner" class="notification '+ajax_cl+' alert alert-info alert-dismissible" style="display: none;" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+response+'</div>');
			}
		});
	}
	
	function show_recent_lotto_drawings(){
		// see if there are new lottery drawings to show
		$.ajax({
			data: {
				size: "sm",
				limit: 1,
				type: 'notification'
			},
			url: "/ajax/recent_lotto_drawings.php",
			method: "POST"
		})
		.done(function(response){
			if(response.length > 1){
				// insert alert div container into page
				$('#ajax-notifications-contain').prepend(response);
			}
		});
	}
	
	var run_timer = true;
	var timer;
	var idleTime = 0;
        var url = window.location.pathname.split('/');
    function wait_for_msg(){


    if(idleTime < 6)
    {

        $.ajax({
            type: "GET",
            url: "/lib/framework/push_events.php",
			data: { 
				action: 'get_events',
                                user: url[1]
			},
			
            tryCount : 0,
            retryLimit : 3,
			dataType: 'json',
            async: true,
            cache: false,
            success: function(data){ /* called when request completes */
            
				if(data){
					// hide disconnect alert if successfully got response
					if(readCookie("showingAlert") == "true"){
						bootbox.hideAll();
						eraseCookie("showingAlert");
					}
					
					// update current date / time
					if(data.current_date_time){
						updateTime(data.current_date_time);
					}
					
					if(data.alerts){
						$.each(data.alerts, function(i, alert) {
							if(alert.type == 'dialog'){
								// insert alert div container into page
								$('body').prepend('<div class="dialog_alert" id="display_alert_'+alert.id+'"></div>');
			
                                                       if(alert.function_name == "send_cashier_fund"){
                                                              var msg = '<div class="col-sm-12">'+alert.message+'</div><div class="col-sm-12" style="margin-top: 35px;"><div class="col-sm-6"><input type="text" id="fund_amount" placeholder="Send cashier money" ></div><div class="col-sm-4" style="padding-right: 30px;" ><a onclick="'+alert.function_name+'('+alert.action_id+')" class="send_fund pull-right"><i title="Send Money to cashier" class="fa fa-paper-plane fa-lg"></i></a></div><div class="col-sm-3" ></div></div>';
                                                       } else {
                                                              var msg = alert.message;
                                                       }
								display_alert(alert.id, alert.title, msg,alert.function_name);
							}else if(alert.type == 'alert'){
								if(alert.title == 'Log Off Notice'){
									// cancel future ajax calls
									run_timer = false;
									timer = 0;
								}
								
								if(readCookie("showingAlert") !== "true"){
									bootbox.dialog({
										title: alert.title,
										message: alert.message,
										className: "bootbox-alert-modal",
										buttons: {
											success: {   
												label: "OK",
												className: "btn-primary",
												callback: function() {
													
													eraseCookie("showingAlert");
													window.location.reload(true);
												}
											}
										}
									});

									createCookie("showingAlert", "true", 1);
								}
							}else if(alert.type == 'notification'){
								// insert alert div container into page
								$('#ajax-notifications-contain').prepend('<div id="notifcation_'+alert.id+'" class="notification alert alert-info alert-dismissible" role="alert" style="display:none;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+alert.message+'</div>');
			
							}
						});
					}
					
					//show_recent_rapidballs_drawing();
                                     
                                        if(url[1] != 'panel' && url[1] != 'admin'){ 					
					          show_recent_lotto_drawings();
                                                  show_recent_rapidballs_drawing();
                                        }
				}
				
				if($(".notification-shown").length == 0 && $(".notification").length > 0){
					display_notifications();
				}
				
				if(run_timer){
					timer = setTimeout(
						wait_for_msg, /* Request next message */
						10000 		  /* ..after 5 seconds */
					);
				}
            },
			error: function(data){
				if(data.status == 500){
					console.log("500 Error");
				}else{
					/*if(readCookie("showingAlert") !== "true"){
						bootbox.alert("We're having trouble connecting to the CSCLotto servers. Please check your internet connection. If this problem persists, contact support.", function(){
							eraseCookie("showingAlert");
						});
						
						createCookie("showingAlert", "true", 1);
					}*/
				}
				$.ajax(this);
			}
        });
	}

    };
        function send_cashier_fund(cashier_id){
            
            var amount = $('#fund_amount').val();
            if(!isNaN(amount) && amount > 0){
                $.ajax({					
			  url: "/admin/ajax/datatables/manage_cashiers_buttons.php",
			  type: "POST",
			  data: {
			  	   action : 'add_funds',
				   add_amount : amount,
				   user_id : cashier_id
			  },
			  dataType: "json",
			  success: function(data){
			         	if(data.success == "false"){
						bootbox.alert("Fund cannot be added at the moment. Please try again.");
					}else{ 
                                                $('.ui-dialog-titlebar-close').click();
						bootbox.alert("Fund added successfully");
					}
			   },
			   error: function(XMLHttpRequest, textStatus, errorThrown) { 
					alert("Operation Failed!");
			   }
			});

            } else {
                 bootbox.alert("Please enter valid amount.");
            }
    }

       /*  var idleTime = 0; */
	$(document).on('ready',function(){
	    //Increment the idle time counter every minute.
	    var idleInterval = setInterval(timerIncrement, 10000); // 1 minute
	    //Zero the idle timer on mouse movement.
	    $('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {

                   if(idleTime > 6){
                          idleTime = 0;
                          wait_for_msg(); 
                    } 
                        idleTime = 0;
            });
          wait_for_msg();
          
       });
	

	function timerIncrement() {
	    idleTime = idleTime + 1;
	}
     $(document).on('ready',function(){
          // wait_for_msg();
           /*if(url[1] != 'panel' && url[1] != 'admin'){ 	
                var d = new Date();
		var n = d.getTime();
	
		var rapid_time = 300 - parseFloat((n%300));
		if(rapid_time == 300){
		       show_recent_rapidballs_drawing();
		}
            }*/
     });
       
