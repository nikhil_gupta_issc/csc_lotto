<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	date_default_timezone_set('America/New_York');
	
	function get_events(){
		global $db;
		global $session;
		
		$r = array();
	
		$r['current_date_time'] = date("F j, Y - g:i a");
		
		$a = 0;
	
		if($session->logged_in){
                        $t_get_banned_users = file_get_contents(DOMAIN_ROOT.'/cron/json/banned_users.json');
			$t_banned_users = json_decode($t_get_banned_users,true);
                        unset($t_get_banned_users);
                         //return alert info if alert was not sent yet
			if(!empty($t_banned_users['banned_users'][$session->userinfo['username']])){
				$r['alerts'][$a]['id'] = $a;
				$r['alerts'][$a]['type'] = 'alert';
				$r['alerts'][$a]['title'] = 'Log Off Notice';
				$r['alerts'][$a]['message'] = "Your account has been logged off and locked by an administrator.";
				
				//update database so we know that this user has been alerted
				$q = "UPDATE `banned_users` SET `alerted` = '1' WHERE `username` = '".$session->userinfo['username']."'";
				$db->query($q);
				
				// kill active session
				$session->logout("user was banned");
			}else if(!empty($t_banned_users['disabled_users'][$session->userinfo['id']])){					
				$session->logout("abcd");
                       } else {

				// store user info
				$r['user_id'] = $session->userinfo['id'];
				$r['username'] = $session->userinfo['username'];
				
				// see if user has been inactive too long
                                if(!empty($_GET['user'])){
                                     if($_GET['user'] == 'panel'){
                                           $t_time_out = CASHIER_TIMEOUT;
                                      } else if($_GET['user'] == 'admin') {
                                           $t_time_out = ADMIN_TIMEOUT;
                                      } else {
                                           $t_time_out = USER_TIMEOUT;
                                      }
                                } else {
                                     $t_time_out = USER_TIMEOUT;
                                 }

				$timeout = $session->userinfo['timestamp'] - (time()-$t_time_out*60);
				$r['last_activity'] = $session->userinfo['timestamp'];
				$r['timeout'] = $timeout;
				if($timeout <= 0){
					$r['alerts'][$a]['id'] = $a;
					$r['alerts'][$a]['type'] = 'alert';
					$r['alerts'][$a]['title'] = 'Log Off Notice';
					$r['alerts'][$a]['message'] = TIMEOUT_MESSAGE;
					
					// kill active session
					$session->logout("user was inactive too long");
					
					$a++;
				}else{
					// see if user no longer has an entry in active users table
					// if not, admin has forced thier logout
                                        $t_get_loggedout_users = file_get_contents(DOMAIN_ROOT.'/cron/json/logged_out_users.json');
			                $t_loggedout_users = json_decode($t_get_loggedout_users,true);
                                        if(!empty($t_loggedout_users[$session->userinfo['username']])){

                                                $t_users = array();
						$t_get_users = file_get_contents(DOMAIN_ROOT.'/cron/json/logged_out_users.json');
						$t_users = json_decode($t_get_users,true);
						unset($t_get_users);

						$file = fopen(DOMAIN_ROOT.'/cron/json/logged_out_users.json',"w+");  
						unset($t_users[$session->userinfo['username']]);                     
						fwrite($file,json_encode($t_users));
						fclose($file); 
						unset($t_users);  
                                        
						//$q = "SELECT COUNT(*) as active_count FROM `active_users` WHERE `username` = '".$session->userinfo['username']."'";
						//$active_user = $db->queryOneRow($q);
						//if($active_user['active_count'] == 0){
							$r['alerts'][$a]['id'] = $a;
							$r['alerts'][$a]['type'] = 'alert';
							$r['alerts'][$a]['title'] = 'Log Off Notice';
							$r['alerts'][$a]['message'] = 'An administrator has logged you out.';
						
							// kill active session
							$session->logout("admin forced logout");
						
							$a++;
						//}
                                       }
				}
                                unset($t_banned_users);
				
				// see if user has reached daily self exclusion limit				
				$limit = $session->userinfo['customers']['daily_session_limit'];
				if($limit > 0){
					// get total login time for all sessions today
					$tot_mins = 0;
					$dt = new DateTime();
					$today_start = $dt->format("Y-m-d")." 00:00:00";
					$q = "SELECT * FROM `user_sessions` WHERE (`start` >= '".$today_start."' OR `end` IS NULL) AND `user_id` = '".$session->userinfo['id']."'";
					$sessions = $db->query($q);
					foreach($sessions as $s){
						$start = new DateTime($s['start']);
						$end = new DateTime($s['end']);
						$interval_min = abs($end->getTimestamp() - $start->getTimestamp()) / 60;
						$tot_mins += $interval_min;
					}
					
					$r['session_limit'] = $limit['daily_session_limit'];
					$r['daily_minutes'] = $tot_mins;
					if($tot_mins >= $limit){
						$r['alerts'][$a]['id'] = $a;
						$r['alerts'][$a]['type'] = 'alert';
						$r['alerts'][$a]['title'] = 'Log Off Notice';
						$r['alerts'][$a]['message'] = 'Daily Session Self Exclusion Limit Reached';
						
						// kill active session
						$session->logout("self exclusion daily session limit reached");
						
						$a++;
					}
				}

                                 $t_get_notifications = file_get_contents(DOMAIN_ROOT.'/cron/json/users_notification.json');
			         $t_notifications = json_decode($t_get_notifications,true);
                                 unset($t_get_notifications);

				//$a = 1;
				
				// see if user has an alert

                                if(!empty($t_notifications['alerts'][$session->userinfo['id']]) && $t_notifications['alerts'][$session->userinfo['id']] > 0) {

                                    $q = "SELECT * FROM `alerts` WHERE `user_id` = '".$session->userinfo['id']."' AND `is_dismissed` = '0'";
				    $active_alerts = $db->query($q);
					foreach($active_alerts as $alert){
						$r['alerts'][$a]['id'] = $alert['id'];
						$r['alerts'][$a]['type'] = 'dialog';
						$r['alerts'][$a]['title'] = $alert['subject'];
						$r['alerts'][$a]['message'] = $alert['message'];
		                                $r['alerts'][$a]['function_name'] = $alert['function_name'];
		                                $r['alerts'][$a]['action_id'] = $alert['action_id'];
					
						//update database so we know that this user has been alerted
						$q = "UPDATE `alerts` SET `is_dismissed` = '1' WHERE `id` = '".$alert['id']."'";
						$db->query($q);
					
						$a++;
					}
                                }
 
                                 if(!empty($t_notifications['notification'][$session->userinfo['id']]) && $t_notifications['notification'][$session->userinfo['id']] > 0) {
				
					// see if user has any notifications
				      $q = "SELECT * FROM `notifications` WHERE `user_id` = '".$session->userinfo['id']."' AND `is_dismissed` = 0";
				      $notifications = $db->query($q);
				      foreach($notifications as $notif){
					      $r['alerts'][$a]['id'] = $notif['id'];
					      $r['alerts'][$a]['type'] = 'notification';
					      $r['alerts'][$a]['message'] = $notif['message'];
					
					      $q = "UPDATE `notifications` SET `is_dismissed` = '1' WHERE `id` = '".$notif['id']."'";
					      $db->query($q);
					
					      $a++;
					}
                            }
                            unset( $t_notifications);
			}
		}
		return $r;
	}
	
	if($_GET['action'] == 'get_events'){
		while(!isset($event_data) || !$event_data){
			$event_data = get_events();
			usleep(50000);
		}
		
		echo json_encode($event_data);
	}
?>
