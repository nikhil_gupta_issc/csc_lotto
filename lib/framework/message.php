<?php
if(PHP_SAPI != 'cli'){
	include($_SERVER['DOCUMENT_ROOT'].'/lib/framework/twilio/twilio.php');
}else{
	include('/var/www/html/lib/framework/twilio/twilio.php');
}

class MESSAGE{
    private static $initialized = false;

    function MESSAGE(){
        if(MESSAGE::$initialized === false) {
			MESSAGE::$initialized = true;
        }
    }
	
	public function happy_birthday(){
		global $db, $core, $sms, $mailer;
		
		// get current settings from db
		$settings = $core->get_settings();
		
		// see if current time is after time to send and not already processed today
		$now_dt = new DateTime();
		$now = $now_dt->format('Y-m-d');
		$time2send_dt = new DateTime($settings['birthday_time_to_send']);		
		if(($now_dt > $time2send_dt) && ($settings['birthday_last_processed'] != $now)){
			// see if there are any birthdays today (take into account leap year babies)
			$q = "SELECT * FROM customers AS c LEFT JOIN users AS u ON u.id = c.user_id LEFT JOIN user_info AS ui ON u.id = ui.user_id
					WHERE DATE_FORMAT(date_of_birth,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d')
					OR (
						(
							DATE_FORMAT(NOW(),'%Y') % 4 <> 0
							OR (
									DATE_FORMAT(NOW(),'%Y') % 100 = 0
									AND DATE_FORMAT(NOW(),'%Y') % 400 <> 0
								)
						)
						AND DATE_FORMAT(NOW(),'%m-%d') = '03-01'
						AND DATE_FORMAT(date_of_birth,'%m-%d') = '02-29'
					)";
			$customers = $db->query($q);
			foreach($customers as $customer){
				// replace variable codes with actual values
				$text_message = $this->replace_codes($settings['birthday_text'], $customer);
				$email_message = $this->replace_codes($settings['birthday_email'], $customer);
				
				// send text
				$sms->send_text_message($customer['cellphone'], $text_message);
				
				// send email
				$mailer->send_email($customer['email'], "Happy Birthday From CSCLotto!", $email_message, $email_message);
			}
			
			// store today as last processed date
			$q = "REPLACE INTO `settings` (`setting`, `value`) VALUES ('birthday_last_processed', '".$now."');";
			$result = $db->query($q);
		}		
	}
	
	public function replace_codes($search_text, $customer){
		$message = $search_text;
		
		preg_match_all("/\\{{(.*?)\\}}/", $search_text, $tags, PREG_OFFSET_CAPTURE);
		if(count($tags[0]) > 0){
			foreach($tags[0] as $key=>$tag_arr){
				$tag = strtolower($tag_arr[0]);
				$position = $tag_arr[1];
				
				// clean up tag
				$tag = str_ireplace("{{", "", $tag);
				$tag = str_ireplace("}}", "", $tag);
				
				// add alias for first,last
				if($tag == "first_name"){
					$tag = "firstname";
				}
				if($tag == "last_name"){
					$tag = "lastname";
				}
				
				// perform replacements
				$message = str_replace($tag, $customer[$tag], $message);
			}
		}
		
		return $message;
	}
	
	public function send_admin_message($subject, $message){
		global $db, $core, $sms, $mailer;
		
		// get current settings from db
		$settings = $core->get_settings();

		// get all admins
		$q = "SELECT * FROM `users` AS `u` WHERE `u`.`deleted` = '0' AND `u`.`userlevel` >= '9'";
		$admin_users = $db->query($q);
		foreach($admin_users as $user){				
			// send email
			$mailer->send_email($user['email'], $subject, $message, $message);
			
			// send text
			$sms->send_text_message($user['cellphone'], $message);
		}	
	}
	
	public function send_user_message($user_id, $subject, $message){
		global $db, $core, $sms, $mailer;
		
		// get current settings from db
		$settings = $core->get_settings();

		// get all admins
		$q = "SELECT * FROM `users` AS `u` WHERE `u`.`id` = $user_id";
		$user = $db->queryOneRow($q);
		
		// send email
		$mailer->send_email($user['email'], $subject, $message, $message);
		
		// send text
		$sms->send_text_message($user['cellphone'], $message);
	}
	
	public function process_alert_rules(){
		global $db;
		global $core;

		// see if we need to create any alerts based on admin rules
		$q = "SELECT * FROM `alert_rules` WHERE `is_disabled` = 0";
		$alert_rules = $db->query($q);

		foreach($alert_rules as $rule){
			// get primary key of table_id
			$q = "SHOW KEYS FROM `".$rule['table_name']."` WHERE Key_name = 'PRIMARY';";
			$pk_info = $db->queryOneRow($q);
			
			// 	id, rule_name, table_name, column_name, comparison, value, alert_type
			// build query
			$q = "SELECT * FROM ".$rule['table_name']." WHERE ".$rule['column_name']." ".$rule['comparison']." %s";
			$results = $db->query($q, array($rule['value']));
			foreach($results as $result){
				$table_id = $result[$pk_info['Column_name']];
			
				// see if this actual alert already exists
				$q = "SELECT * FROM `alerts` WHERE `rule_id` = ".$rule['id']." AND `table_id` = ".$table_id." AND `is_dismissed` = 0";
				$existing = $db->queryOneRow($q);
				
				// see if the last dismissed alert is for the same thing
				$q = "SELECT * FROM `alerts` WHERE `rule_id` = ".$rule['id']." AND `table_id` = ".$table_id." AND `is_dismissed` = 1 AND `alert_value` = '".$result[$rule['column_name']]."' ORDER BY date_created DESC LIMIT 1";
				$existing_dismissed = $db->queryOneRow($q);
				
				// send new alert
				if(!$existing && !$existing_dismissed){
					// convert comparisons into words
					if($rule['comparison'] == "="){
						$rule['comparison'] = "equal to";
					}elseif($rule['comparison'] == ">"){
						$rule['comparison'] = "greater than";
					}elseif($rule['comparison'] == "<"){
						$rule['comparison'] = "less than";
					}elseif($rule['comparison'] == ">="){
						$rule['comparison'] = "greater than or equal to";
					}elseif($rule['comparison'] == "<="){
						$rule['comparison'] = "less than or equal to";
					}elseif($rule['comparison'] == "!="){
						$rule['comparison'] = "not equal to";
					}
					
					$subject = $rule['rule_name'];
					$message = str_replace("_", "", ucwords($rule['column_name']))." of ".$result[$rule['column_name']]." is ".$rule['comparison']." limit of ".$rule['value'];
					$action_id = '';
					// determine who/what this alert pertains to
					if($rule['item_name'] == "user_id"){
						// get user's info
						$q = "SELECT firstname, lastname FROM `users` WHERE `id` = ".$result[$rule['item_name']];
						$user_info = $db->queryOneRow($q);
						$message .= " for ".$user_info['firstname']." ".$user_info['lastname']." (".$table_id.")";
                                                $action_id = $result[$rule['item_name']];
                                              
					}elseif($rule['item_name'] == "location_id"){
						// get location info
						$q = "SELECT name FROM `panel_location` WHERE `id` = ".$result[$rule['item_name']];
						$loc_info = $db->queryOneRow($q);
						$message .= " for ".$loc_info['name'];
					}
					
					$core->send_alert_by_group($subject, $message, 3, $rule['id'], $table_id, $result[$rule['column_name']],$rule['function_name'],$action_id);
					//$this->send_admin_message($subject, $message);
				}
			}
		}
	}
}
