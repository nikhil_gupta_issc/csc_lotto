<?php
	require_once("../../config.php");

	set_time_limit(0); //prevent timeout
	
	$time_start = microtime(true);
	
	$filename = "/srv/www/backup.sql";
	
	$q = "SELECT * FROM `settings` WHERE `setting` = 'last_line_imported'";
	$last_line_info = $db->queryOneRow($q);
	
	$start_iteration = $last_line_info['value'] + 1;
	$end_iteration = $start_iteration + 500000 - 1;
	
	$end_line = 500000 - 1;
	
	echo "Processing lines $start_iteration to $end_iteration.<br><br>";
	
	$tables_created = array();
	
	$line_count = 0;
	$iteration = 0;
	$next_line = "";
	$fh = fopen($filename, 'r');
	if($fh){
		while(!feof($fh)){
			if($next_line == ""){
				$line_read = fgets($fh, 8192);
				$iteration++;
			}else{
				$line_read = $next_line;
				$next_line = "";
			}

			if($iteration >= $start_iteration){
				if($iteration == $start_iteration){
					$time_end = microtime(true);
					$tot_time = $time_end - $time_start;
				
					echo "Seek Time: $tot_time seconds.<br><br>";
				}
			
				// see if this is an INSERT statement
				$pos = stripos("  ".$line_read, " INSERT ");
				if($pos > 0){
					$line = $line_read;
					
					// read in next line to see if its another insert or continued data
					$pos2 = 0;
					while($pos2 == 0){
						$next_line = fgets($fh, 8192);
						$iteration++;
						$pos2 = stripos("  ".$next_line, " INSERT ");
						if($pos2 == 0){
							$line .= $next_line;
						}
					}

					// strip out table name
					// ignore dbo. prefix
					$table_name = str_ireplace("dbo.", "", $core->get_string_between($line, "INSERT ", " ("));
					
					// get columns
					$columns = array();
					$column_str = $core->get_string_between($line, "(", ")");
					$columns = explode(",", $column_str);
					
					// get data
					$data = array();
					$data_str = $core->get_string_between_special($line, "(", ")", 2);
					$data = explode(",", $data_str);
					
					if(count($data) > 1){					
						// create data types for each column
						// create table mysql statement
						$create_query = "CREATE TABLE IF NOT EXISTS `".$table_name."` (";
						$insert_query = "INSERT INTO `".$table_name."` (";
						$data_types = array();
						$index_queries = array();
						foreach($columns as $col){
							$col = trim($col);
							
							// convert from camel case to underscore
							$col_arr = explode("_", $core->camel_case_to_underscore($col));
							
							// see if this field should be an index
							if(in_array('id', $col_arr)){
								$index_queries[] = "ALTER TABLE `".$table_name."` ADD KEY `".$col."` (`".$col."`)";
							}
							
							$data_type = trim($col_arr[0]);
							
							switch($data_type){
								case 'i':
									// integer
									$data_types[] = "int(11)";
									break;
								case 's':
									// string
									$data_types[] = "varchar(500)";
									break;
								case 'dt':
									// datetime
									$data_types[] = "datetime";
									break;
								case 'b':
									// boolean
									$data_types[] = "tinyint(1)";
									break;
								case 'dec':
									// decimal
									$data_types[] = "float";
									break;
								case 'f':
									// float
									$data_types[] = "float";
									break;
								case 'l':
									// long
									$data_types[] = "double";
									break;
								default:
									fclose($fh);
									
									// update line count to this bad entry
									$iteration = $iteration - 1;
									$q = "UPDATE `settings` SET `value` = '".$iteration."' WHERE `setting` = 'last_line_imported'";
									$db->queryDirect($q);
									
									die("ERROR - Data type (".$data_type.") not recognized for column: $col!<br><br>$line<br><br>");
							}
							
							if(in_array('id', $col_arr)){
								$create_query .= "
									`$col` ".$data_types[count($data_types)-1]." NOT NULL,";
							}else{
								$create_query .= "
									`$col` ".$data_types[count($data_types)-1]." NULL,";
							}
							$insert_query .= "`".$col."`, ";
						}
						
						// remove last commas
						$create_query = rtrim($create_query, ",");
						$insert_query = rtrim($insert_query, ", ");
						
						// continue queries
						$create_query .= "
							) ENGINE=InnoDB DEFAULT CHARSET=utf8;
						";
						$insert_query .= "
							) VALUES (
						";
						
						if($tables_created[$table_name] != true){
							// create tables if needed						
							echo "<br>".$create_query."<br>";
							$db->queryDirect($create_query);
							$tables_created[$table_name] = true;
							
							// alter table to add indexes
							foreach($index_queries as $index_q){
								echo $index_q."<br>";
								$db->queryDirect($index_q);
							}
						}
						
						// build insert query data
						foreach($data as $value){
							// fix stupid N'
							$value = trim($value);
							$value = str_replace("N'", "'", $value);
							$insert_query .= $value.", ";	
						}
						
						// remove last comma
						$insert_query = rtrim($insert_query, ", ");
						
						// continue queries
						$insert_query .= "
							)
						";
						
						// die on mysql errors
						$res = $db->queryDirect($insert_query);
						if(!$res){
							// update line count to this bad entry
							$iteration = $iteration - 1;
							$q = "UPDATE `settings` SET `value` = '".$iteration."' WHERE `setting` = 'last_line_imported'";
							$db->queryDirect($q);
							
							echo "<br>Original Line: $line";
							echo "<br>".$db->getLastError();
							echo "<br>".$insert_query."<br>";
							fclose($fh);
							die();
						}
					}
					
					$line_count++;
				}
				
				if($line_count >= $end_line){
					echo "<br>DONE<br>";
					
					$time_end = microtime(true);
					$tot_time = $time_end - $time_start;
					
					echo "<br>\n<br>\nScript completed in $tot_time seconds.";
					
					// update line count
					$q = "UPDATE `settings` SET `value` = '".$iteration."' WHERE `setting` = 'last_line_imported'";
					$db->queryDirect($q);
					
					echo "<script>window.location.replace('http://dev.asurewin.com:8090/lib/framework/sql_import.php');</script>";
					
					fclose($fh);
					die();
				}
			}
		}
		
		fclose($fh);
	}
?>