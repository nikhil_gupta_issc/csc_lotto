<?php
	require_once("../../config.php");

	set_time_limit(0); //prevent timeout
	
	$time_start = microtime(true);
	
	$filename = "/srv/www/backup.sql";
	
	$line_count = 0;
	
	$fh = fopen($filename, 'r');
	if($fh){
		while(!feof($fh)){
			$line_read = fgets($fh, 8192);
			$line_count++;
		}
		
		fclose($fh);
	}
	
	$time_end = microtime(true);
	$tot_time = $time_end - $time_start;
	
	echo "<br>Seek Time: $tot_time seconds.<br><br>";
	
	echo $line_count;
?>