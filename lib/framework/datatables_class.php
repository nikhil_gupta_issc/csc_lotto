<?php

/*
 * Helper functions for building a DataTables server-side processing SQL query
 *
 * The static functions in this class are just helper functions to help build
 * the SQL used in the DataTables demo server-side processing scripts. These
 * functions obviously do not represent all that can be done with server-side
 * processing, they are intentionally simple to show how it works. More complex
 * server-side processing operations will likely require a custom script.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 *
 * Customized By emranulhadi@gmail.com | http://emranulhadi.wordpress.com/
 
 https://github.com/emran/ssp/blob/master/ssp.php
 
 */


// REMOVE THIS BLOCK - used for DataTables test environment only!
//$file = $_SERVER['DOCUMENT_ROOT'].'/datatables/mysql.php';
//if ( is_file( $file ) ) {
//    include( $file );
//}


class SSP {
    /**
     * Create the data output array for the DataTables rows
     *
     * @param array $columns Column information array
     * @param array $data    Data from the SQL get
     * @param bool  $isJoin  Determine the query is complex or simple one
     *
     * @return array Formatted data in a row based format
     */

    static function data_output ( $columns, $data, $isJoin = false ){
        $out = array();

        for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
            $row = array();

            for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
                $column = $columns[$j];

                // Is there a formatter?
                if ( isset( $column['formatter'] ) ) {
                    $row[ $column['dt'] ] = ($isJoin) ? $column['formatter']( $data[$i][ $column['field'] ], $data[$i] ) : $column['formatter']( $data[$i][ $column['db'] ], $data[$i] );
                }
                else {
                    $row[ $column['dt'] ] = ($isJoin) ? $data[$i][ $columns[$j]['field'] ] : $data[$i][ $columns[$j]['db'] ];
                }
            }

            $out[] = $row;
        }

        return $out;
    }


    /**
     * Paging
     *
     * Construct the LIMIT clause for server-side processing SQL query
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @return string SQL limit clause
     */
    static function limit ( $request, $columns ){
        $limit = '';
		
		if(!isset($request['export'])){
			if ( isset($request['start']) && $request['length'] != -1 ) {
				$limit = "LIMIT ".intval($request['start']).", ".intval($request['length']);
			}
		}

        return $limit;
    }


    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing SQL query
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @param  bool  $isJoin  Determine the query is complex or simple one
     *
     *  @return string SQL order by clause
     */
    static function order ( $request, $columns, $isJoin = false ){
        $order = '';

        if ( isset($request['order']) && count($request['order']) ) {
            $orderBy = array();
            $dtColumns = SSP::pluck( $columns, 'dt' );

            for ( $i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                if(!empty($request['server_order'])) {
                     $requestColumn['orderable'] = true;
                } else {
                     $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                }
                
                $column = $columns[ $columnIdx ];
                if ( $requestColumn['orderable'] == 'true' ) {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                        'ASC' :
                        'DESC';

                    $orderBy[] = ($isJoin) ? $column['db'].' '.$dir : '`'.$column['db'].'` '.$dir;
                }
            }

            $order = 'ORDER BY '.implode(', ', $orderBy);
        }

        return $order;
    }


    /**
     * Searching / Filtering
     *
     * Construct the WHERE clause for server-side processing SQL query.
     *
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here performance on large
     * databases would be very poor
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @param  array $bindings Array of values for PDO bindings, used in the sql_exec() function
     *  @param  bool  $isJoin  Determine the the query is complex or simple one
     *
     *  @return string SQL where clause
     */
    static function filter ( $request, $columns, &$bindings, $isJoin = false ){
        $columnSearch = array();
        $dtColumns = SSP::pluck( $columns, 'dt' );
		$dbColumns = SSP::pluck( $columns, 'db' );
		
		function get_quoted_strings($text, $strip_quotes = true){
			preg_match_all("/(?:(?:\"(?:\\\\\"|[^\"])+\")|(?:'(?:\\\'|[^'])+'))/is", $text, $matches);
			
			if($strip_quotes == true){
				$new_matches = array();
				foreach($matches as $match){
					$match = str_replace("'", "", $match);
					$match = str_replace('"', "", $match);
					$new_matches[] = $match;
				}
				return $new_matches[0];
			}
			return $matches[0];
		}
		
		// allow option to do forced filter
		if(isset($request['forced_filter'])){
			if(!isset($request['search'])){
				$request['search'] = array();
				$request['search']['value'] = $request['forced_filter'];
			}else{
				$request['search']['value'] = $request['forced_filter'].' '.$request['search']['value'];
			}
		}
		
		/*
		// only allow searching on searchable columns
        if(isset($request['search']) && $request['search']['value'] != ''){
            $str = $request['search']['value'];

            for($i=0, $ien=count($request['columns']); $i<$ien; $i++){
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                $column = $columns[ $columnIdx ];

                if ( $requestColumn['searchable'] == 'true' ) {
                    $binding = SSP::bind( $bindings, '%'.$str.'%', PDO::PARAM_STR );
                    $globalSearch[] = ($isJoin) ? $column['db']." LIKE ".$binding : "`".$column['db']."` LIKE ".$binding;
                }
            }
        }
		*/
		
		$where = '';
		
		// modified filter to allow filtering on all db fields
		if(isset($request['search']) && $request['search']['value'] != ''){
			if((strpos(" ".$request['search']['value'], "'") > 0) || (strpos(" ".$request['search']['value'], '"') > 0)){
				// if there are quotes, make sure there is an even number of quotes
				if((substr_count($request['search']['value'], '"') % 2 == 0) && (substr_count($request['search']['value'], "'") % 2 == 0)){
					// then allow searching for items in quotes as literal terms
					$str_array1 = get_quoted_strings($request['search']['value']);
					
					// see if there is any other data in the the request that is not in quotes
					$remaining = trim(preg_replace("/(?:(?:\"(?:\\\\\"|[^\"])+\")|(?:'(?:\\\'|[^'])+'))/is", "", $request['search']['value']));
					$str_array2 = explode(" ", $remaining);
					if(count($str_array2) > 0){
						// if so, merger it with the quoted terms
						$str_array = array_merge($str_array1, $str_array2);
					}else{
						// otherwise, use just the quoted terms
						$str_array = $str_array1;
					}
				}else{
					// invalid pairing of quotes - do normal split
					$str_array = explode(" ", $request['search']['value']);
				}
			}else{
				// normal split on spaces so each word is a separate search term
				$str_array = explode(" ", $request['search']['value']);
			}
			
			// perform AND operation across all columns for each search term
			foreach($str_array as $str){
				// see if term starts with '-'
				$equality = substr($str, 0, 1) == '-' ? " NOT LIKE " : " LIKE ";
				$and_or_implode = substr($str, 0, 1) == '-' ? " AND " : " OR ";
				$str = ltrim($str, "-");
				
				// process each search term
				$globalSearch = array();
				foreach($dbColumns as $column){
					// compare the search term to all columns
					$binding = SSP::bind( $bindings, '%'.$str.'%', PDO::PARAM_STR );
					$globalSearch[] = ($isJoin) ? $column.$equality.$binding : "`".$column."`".$equality.$binding;
				}
				if(count($globalSearch)){
					$where .= '('.implode($and_or_implode, $globalSearch).')';
				}
				$where .= " AND ";
			}
			
			// remove final AND
			$where = rtrim($where, " AND ");
		}
		
		/*
        // Individual column filtering
        for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
            $requestColumn = $request['columns'][$i];
            $columnIdx = array_search( $requestColumn['data'], $dtColumns );
            $column = $columns[ $columnIdx ];

            $str = $requestColumn['search']['value'];

            if ( $requestColumn['searchable'] == 'true' &&
                $str != '' ) {
                $binding = SSP::bind( $bindings, '%'.$str.'%', PDO::PARAM_STR );
                $columnSearch[] = ($isJoin) ? $column['db']." LIKE ".$binding : "`".$column['db']."` LIKE ".$binding;
            }
        }
		*/

        // Combine the filters into a single string

        if ( count( $columnSearch ) ) {
            $where = $where === '' ?
                implode(' AND ', $columnSearch) :
                $where .' AND '. implode(' AND ', $columnSearch);
        }

        if ( $where !== '' ) {
            $where = 'WHERE '.$where;
        }

        return $where;
    }


    /**
     * Perform the SQL queries needed for an server-side processing requested,
     * utilising the helper functions of this class, limit(), order() and
     * filter() among others. The returned array is ready to be encoded as JSON
     * in response to an SSP request, or can be modified if needed before
     * sending back to the client.
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $sql_details SQL connection details - see sql_connect()
     *  @param  string $table SQL table to query
     *  @param  string $primaryKey Primary key of the table
     *  @param  array $columns Column information array
     *  @param  array $joinQuery Join query String
     *  @param  string $extraWhere Where query String
     *
     *  @return array  Server-side processing response array
     *
     */
    static function get_table_data ( $request, $sql_details, $table, $primaryKey, $columns, $extraWhere = '', $groupBy = '', $joinQuery = NULL ){
        $bindings = array();
        $db = SSP::sql_connect( $sql_details );

        // Build the SQL query string from the request
        $limit = SSP::limit( $request, $columns );
        $order = SSP::order( $request, $columns, $joinQuery );
        $where = SSP::filter( $request, $columns, $bindings, $joinQuery );

        // IF Extra where set then set and prepare query
        if($extraWhere){
			$extraWhere = ($where) ? ' AND '.$extraWhere : ' WHERE '.$extraWhere;
        }

        // Main query to actually get the data
        if($joinQuery){
            $col = SSP::pluck($columns, 'db', $joinQuery);

            $query = "SELECT SQL_CALC_FOUND_ROWS ".implode(", ", $col)."
			 $joinQuery
			 $where
			 $extraWhere
             $groupBy
			 $order
			 $limit";
			$export_query = "SELECT ".implode(", ", $col)."
			 $joinQuery
			 $where
			 $extraWhere
             $groupBy
			 $order";
        }else{
            $query =  "SELECT SQL_CALC_FOUND_ROWS `".implode("`, `", SSP::pluck($columns, 'db'))."`
			 FROM `$table`
			 $where
			 $extraWhere
			 $groupBy
             $order
			 $limit";
			$export_query = "SELECT `".implode("`, `", SSP::pluck($columns, 'db'))."`
			 FROM `$table`
			 $where
			 $extraWhere
             $groupBy
			 $order";
        }
		
		if($_GET['export'] != 'true'){
			// store path in session
			$_SESSION['datatable_export_url'] = $_SERVER['REQUEST_URI']."&export=true";
			
			// clean up sql query for debug output
			$query = str_replace("  ", " ", $query);
			$query = str_replace("\t", "", $query);
			$query = str_replace("\n", "", $query);
			$query = str_replace("\r", "", $query);
			$query = str_replace("  ", " ", $query);
			
			$data = SSP::sql_exec($db, $bindings, $query);
			
			// Data set length after filtering
			$resFilterLength = SSP::sql_exec( $db,
				"SELECT FOUND_ROWS()"
			);
			$recordsFiltered = $resFilterLength[0][0];

			// Total data set length
			$resTotalLength = SSP::sql_exec( $db,
				"SELECT COUNT(`{$primaryKey}`)
				 FROM   `$table`"
			);
			$recordsTotal = $resTotalLength[0][0];
			
			/*
			 * Output
			 */
			return array(
				"draw"            	=> intval( $request['draw'] ),
				"recordsTotal"    	=> intval( $recordsTotal ),
				"recordsFiltered" 	=> intval( $recordsFiltered ),
				"data"            	=> SSP::data_output( $columns, $data, $joinQuery ),
				"query"				=> $query
			);
		}else{
			$data = SSP::sql_exec($db, $bindings, $export_query, true);
			
			$df = fopen("/srv/www/uploads/direct_export.csv", 'w');
			fputcsv($df, array_keys($data[0]));
			foreach($data as $d){
				fputcsv($df, $d);
			}
			fclose($df);
			
			// load csv if ready for download
			$filepath = "/srv/www/uploads/direct_export.csv";
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: inline; filename="export.csv"');
			header('Expires: 0');
			header('Cache-Control: no-cache, must-revalidate');
			header('Pragma: public');
			header('Content-Length: '.filesize($filepath));
			header('Accept-Ranges: bytes');
			readfile($filepath);
		}
    }

    /**
     * Connect to the database
     *
     * @param  array $sql_details SQL server connection details array, with the
     *   properties:
     *     * host - host name
     *     * db   - database name
     *     * user - user name
     *     * pass - user password
     * @return resource Database connection handle
     */
    static function sql_connect ( $sql_details ){
        try {
            $db = @new PDO(
                "mysql:host={$sql_details['host']};dbname={$sql_details['db']}",
                $sql_details['user'],
                $sql_details['pass'],
                array( PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION )
            );
            $db->query("SET NAMES 'utf8'");
        }
        catch (PDOException $e) {
            SSP::fatal(
                "An error occurred while connecting to the database. ".
                "The error reported by the server was: ".$e->getMessage()
            );
        }

        return $db;
    }


    /**
     * Execute an SQL query on the database
     *
     * @param  resource $db  Database handler
     * @param  array    $bindings Array of PDO binding values from bind() to be
     *   used for safely escaping strings. Note that this can be given as the
     *   SQL query string if no bindings are required.
     * @param  string   $sql SQL query to execute.
     * @return array         Result from the query (all rows)
     */
    static function sql_exec ( $db, $bindings, $sql=null, $assoc_only=false ){
        // Argument shifting
        if ( $sql === null ) {
            $sql = $bindings;
        }

        $stmt = $db->prepare( $sql );
        //echo $sql;

        // Bind parameters
        if ( is_array( $bindings ) ) {
            for ( $i=0, $ien=count($bindings) ; $i<$ien ; $i++ ) {
                $binding = $bindings[$i];
                $stmt->bindValue( $binding['key'], $binding['val'], $binding['type'] );
            }
        }

        // Execute
        try {
            $stmt->execute();
        }
        catch (PDOException $e) {
			// clean up sql
			$sql = str_replace("  ", " ", $sql);
			$sql = str_replace("\n", " ", $sql);
			$sql = str_replace("\r", " ", $sql);
			$sql = str_replace("  ", " ", $sql);
            SSP::fatal( "Query: $sql \n\r An SQL error to occur: ".$e->getMessage() );
        }

        // Return all
		if($assoc_only == true){
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}else{
			return $stmt->fetchAll();
		}
    }


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Internal methods
     */

    /**
     * Throw a fatal error.
     *
     * This writes out an error message in a JSON string which DataTables will
     * see and show to the user in the browser.
     *
     * @param  string $msg Message to send to the client
     */
    static function fatal ( $msg ){
        echo json_encode( array(
            "error" => $msg
        ));
		
        exit(0);
    }

    /**
     * Create a PDO binding key which can be used for escaping variables safely
     * when executing a query with sql_exec()
     *
     * @param  array &$a    Array of bindings
     * @param  *      $val  Value to bind
     * @param  int    $type PDO field type
     * @return string       Bound key to be used in the SQL where this parameter
     *   would be used.
     */
    static function bind ( &$a, $val, $type ){
        $key = ':binding_'.count( $a );

        $a[] = array(
            'key' => $key,
            'val' => $val,
            'type' => $type
        );

        return $key;
    }


    /**
     * Pull a particular property from each assoc. array in a numeric array,
     * returning and array of the property values from each item.
     *
     *  @param  array  $a    Array to get data from
     *  @param  string $prop Property to read
     *  @param  bool  $isJoin  Determine the the JOIN/complex query or simple one
     *  @return array        Array of property values
     */
    static function pluck ( $a, $prop, $isJoin = false ){
        $out = array();

        for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
            $out[] = ($isJoin && isset($a[$i]['as'])) ? $a[$i][$prop]. ' AS '.$a[$i]['as'] : $a[$i][$prop];
        }

        return $out;
    }
}
