<?php
	date_default_timezone_set('America/New_York');

	if(PHP_SAPI != 'cli'){
		//include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
		include_once($_SERVER['DOCUMENT_ROOT'].'/lib/assets/twilio-php/Services/Twilio.php');
	}else{
		//include_once('/var/www/html/config.php');
		include_once('/var/www/html/lib/assets/twilio-php/Services/Twilio.php');
	}
	
	class SMS{
		private static $initialized = false;
		private $twilio_api; 
		public $from_phone_number;
		
		function SMS(){
			if(SMS::$initialized === false){
				SMS::$initialized = true;
				
				global $db;
				// get current settings from db
				$settings = array();
				$q = "SELECT * FROM `settings`";
				$settings_info = $db->query($q);
				foreach($settings_info as $setting){
					$settings[$setting['setting']] = $setting['value'];
				}

				$this->twilio_api = new Services_Twilio($settings['twilio_account_sid'], $settings['twilio_auth_token']);
				$this->from_phone_number = $settings['twilio_phone_number'];
			}
		}
	
		public function send_text_message($phone_number, $message_body){
			global $db;

			$from_phone_number = $this->from_phone_number;
			
			// log this to database
			$now_dt = new DateTime();
			$now = $now_dt->format('Y-m-d H:i:s');
			$q = "INSERT INTO `log_sms_messages` (`id`, `twillio_id`, `twillio_response`, `to_phone_number`, `from_phone_number`, `message`, `status`, `sent_on`) VALUES (NULL, '', '', '".$phone_number."', '".$from_phone_number."', '".$message_body."', '', '".$now."')";
			$log_id = $db->queryInsert($q);
			
			$response = array();
			try{
				$message = $this->twilio_api->account->messages->create(array( 
					'To' => $phone_number, 
					'From' => $from_phone_number,
					'Body' => $message_body,
					'StatusCallback' => ROOTPATH.'/lib/framework/twilio/twilio_callback.php?log_id='.$log_id
				));
				
				if($message->sid){
					$response['sid'] = $message->sid;
					$response['status'] = $message->status;
					$response['sid'] = $message->sid;
					$response['num_segments'] = $message->num_segments;
					$response['num_media'] = $message->num_media;
					$response['api_version'] = $message->api_version;
					$response['price'] = $message->price;
					$response['price_unit'] = $message->price_unit;
					$response['error_code'] = $message->error_code;
					$response['error_message'] = $message->error_message;
					$response['uri'] = $message->uri;
				}else{
					$response['status'] = 'failed';
				}
			} catch (Services_Twilio_RestException $e) {
				$response['status'] = 'failed';
				$response['error_message'] = $e->getMessage();
			}
			
			// update log in database
			$now_dt = new DateTime();
			$now = $now_dt->format('Y-m-d H:i:s');
			$q = "UPDATE `log_sms_messages` SET `twillio_id` = '".$message->sid."' WHERE `id`=".$log_id;
			$db->query($q);
			$q = "UPDATE `log_sms_messages` SET `twillio_response` = '".$message."' WHERE `id`=".$log_id;
			$db->query($q);
			$q = "UPDATE `log_sms_messages` SET `status` = '".$response['status']."' WHERE `id`=".$log_id;
			$db->query($q);
			
			return $response;
		}
	}

	$sms = new SMS();