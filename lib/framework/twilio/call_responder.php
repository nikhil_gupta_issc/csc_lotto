<?php
	// Include core config
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/core_config.php");
	}else{
		include(DOMAIN_ROOT.'/core_config.php');
	}

	// Include core class
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/lib/framework/core.php");
	}else{
		include(DOMAIN_ROOT.'/lib/framework/core.php');
	}
	$core = new CORE();
	
	// get current settings from db
	$settings = array();
	$q = "SELECT * FROM `settings`";
	$settings_info = $db->query($q);
	foreach($settings_info as $setting){
		$settings[$setting['setting']] = $setting['value'];
	}
	$twilio_phone_number = $settings['twilio_phone_number'];
	
	 if(!empty($_SERVER['SERVER_PORT'])) {
	 $path = PROTOCOL.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
	 }
	 else{
	 $path = PROTOCOL.$_SERVER['SERVER_NAME'];	 
	 }
	
    header("content-type: text/xml");
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

	switch($_REQUEST['To']){
		case $twilio_phone_number:
			// phone betting system
			if(isset($_REQUEST['pin_entered'])){
				if($_SESSION['caller_info']['pin'] == $_REQUEST['Digits']){
					// PIN was correct, prompt with main menu
					// get salutation
					$mr_ms = strtolower($_SESSION['caller_info']['gender']) == "female" ? "Ms." : "Mr.";
					echo '
						<Response>
							<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php" method="GET">
								<Say voice="alice" language="en-US">
									What can I help you with '.$mr_ms.' '.$_SESSION['caller_info']['lastname'].'..
									For lottery tickets, press 1.
									For sports betting, press 2.
									For your account balance, press 3.
									To deposit money into your account, press 4.
									For customer service, press 0.
								</Say>
							</Gather>
							<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder.php?Digits='.$_REQUEST['Digits'].'&amp;pin_entered=1</Redirect>
						</Response>			
					';
				}else{
					// PIN was wrong, ask for it again
					echo '
						<Response>
							<Gather timeout="15" finishOnKey="#" action="'.$path.'/lib/framework/twilio/call_responder.php?pin_entered=1" method="GET">
								<Say voice="alice" language="en-US">
									I am sorry but the PIN that you entered is not correct. Please try again.
									To confirm your identity, please enter your PIN followed by the pound sign.
								</Say>
							</Gather>							
						</Response>			
					';
				}
			}else{
				// default initial greeting - ask for PIN
				
				// see if we know who is calling us and store info in session
				$from_number = trim(str_replace("+1", "", $_REQUEST['From']));
				$q = "SELECT * FROM `customers` AS `c` LEFT JOIN `user_info` AS `ui` ON `c`.`user_id` = `ui`.`user_id` LEFT JOIN `users` AS `u` ON `c`.`user_id` = `u`.`id` WHERE `ui`.`telephone` = '".$from_number."' OR `ui`.`cellphone` = '".$from_number."'";
				$caller_info = $db->queryOneRow($q);
				
				if($caller_info){
					// store in session so we don't have to look this up again
					$_SESSION['caller_info'] = $caller_info;
				
					echo '
						<Response>
							<Gather timeout="15" finishOnKey="#" action="'.$path.'/lib/framework/twilio/call_responder.php?pin_entered=1" method="GET">
								<Say voice="alice" language="en-US">
									Hello '.$_SESSION['caller_info']['firstname'].'. Thanks for calling A Sure Wins Automated Betting System!
									To confirm your identity, please enter your PIN followed by the pound sign.
								</Say>
							</Gather>
							<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder.php</Redirect>
						</Response>
					';
				}else{
					echo '
						<Response>
							<Say voice="alice" language="en-US">
								Thanks for calling A Sure Wins Automated Betting System.  To use this system you must call from a phone number linked to your account.
								Please login to w w w dot A Sure Win dot com and add '.implode(" ", str_split($_REQUEST['From'])).' to your list of phone numbers.
								After you have done so, please call back.
							</Say>
						</Response>
					';
				}
			}
			break;
		case '':
			// other numbers for other tasks
			echo '
				<Response>
					<Say voice="alice" language="en-US">You are calling a phone number that has not yet been configured.  Good Bye!</Say>
				</Response>
			';
			
			break;
		default:
		   echo '
				<Response>
					<Say voice="alice" language="en-US">We apologize but an error has occurred.  Good Bye!</Say>
				</Response>
			';
	}
?>
