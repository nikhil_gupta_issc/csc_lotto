<?php
	// Include core config
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/core_config.php");
	}else{
		include(DOMAIN_ROOT.'/core_config.php');
	}
	
	// Include twilio sms class
	if(PHP_SAPI != 'cli'){
		include($_SERVER['DOCUMENT_ROOT'].'/lib/framework/twilio/twilio.php');
	}else{
		include('/var/www/html/lib/framework/twilio/twilio.php');
	}
	
	// store conversation message count
    if(!strlen($_SESSION['message_count'])){
        $_SESSION['message_count'] = 0;
    }
    $_SESSION['message_count']++;
    
    if(!empty($_SERVER['SERVER_PORT'])) {
	 $path = PROTOCOL.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
	 }
	 else{
	 $path = PROTOCOL.$_SERVER['SERVER_NAME'];	 
	 }
	
	$response = "";
	$signature = "  -CSCLotto Bet System";
	$logo_image = "'.$path.'/images/asurewin_sms.jpg";
	
	
	
    header("content-type: text/xml");
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	
	switch($_REQUEST['To']){
		case $sms->from_phone_number:
			// text bet system
			switch(strtolower($_REQUEST['Body'])){
				case 'help':
					// twilio responds to help 
					break;
				case 'stop':
					// twilio handles un-subscribing user 
					break;
				case 'start':
					// twilio handles re-subscribing user 
					break;
				case 'commands':
					$response = "Hello. You can text me the following commands: LOTTO BET, LOTTO RESULTS, LOTTO WINNERS, SPORTS BET, SPORTS RESULTS, SPORTS WINNERS, and ACCOUNT BALANCE.";
					break;
				case 'lotto bet':
					$response = "";
					break;
				case 'lotto winners':
					// 1-3 hear recently drawn winning numbers
					$today_dt = new DateTime('now');
					$date_str = $today_dt->format("Y-m-d");
					$data = $db->query("SELECT lg.number_of_balls, lg.name, lh.short_name as house_name, lwn.draw_numbers, lh.web_cutoff_time, lwn.is_override FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date='".$date_str."' ORDER BY lg.name ASC LIMIT 40");
					
					// need to add option to hear more
					
					if(count($data) < 1){
						// no drawings for today, use yesterday instead
						$today_dt = new DateTime('yesterday');
						$date_str = $today_dt->format("Y-m-d");
						$data = $db->query("SELECT lg.number_of_balls, lg.name, lh.short_name as house_name, lwn.draw_numbers, lh.web_cutoff_time, lwn.is_override FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date='".$date_str."' ORDER BY lg.name ASC LIMIT 40");
						
						$response = 'There are no winning numbers yet for today. ';
						$response .= 'Here are the winning lottery numbers for yesterday, '.$today_dt->format('F jS, Y').".\n";
					}else{
						$response = 'Here are the winning lottery numbers for '.$today_dt->format('F jS, Y').".\n";
					}	
					
					//Build the list
					for($x=0; $x<count($data); $x++){
						if($data[$x]['is_override'] == "1" && $data[$x]['web_cutoff_time'] >= date("H:i:s",time())){

						}else{
							$response .= $data[$x]['house_name']." - ".$data[$x]['name'].": ";
							
							if($data[$x]['number_of_balls'] == 2){
								$response .= substr($data[$x]['draw_numbers'],-2,2). "\n";
							}else{
								$response .= $data[$x]['draw_numbers']."\n";
							}
						}
					}
					break;
				default:
				   $response = "Unknown command. Reply with 'COMMANDS' for a list of commands that I understand.";
			}
			break;
		case '':
			
			
			break;				
		default:
		   die("Unknown Recipient");
	}
?>
<Response>
    <Message>
		<Body><?php echo $response.$signature; ?></Body>
		<Media><?php echo $logo_image; ?></Media>
	</Message>
</Response>
