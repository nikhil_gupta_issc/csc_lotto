<?php
	// Include core config
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/core_config.php");
	}else{
		include(DOMAIN_ROOT.'/core_config.php');
	}

	// Include core class
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/lib/framework/core.php");
	}else{
		include(DOMAIN_ROOT.'/lib/framework/core.php');
	}
	$core = new CORE();
	
	
	 if(!empty($_SERVER['SERVER_PORT'])) {
	 $path = PROTOCOL.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
	 }
	 else{
	 $path = PROTOCOL.$_SERVER['SERVER_NAME'];	 
	 }
	
	
	
	// get current settings from db
	$settings = array();
	$q = "SELECT * FROM `settings`";
	$settings_info = $db->query($q);
	foreach($settings_info as $setting){
		$settings[$setting['setting']] = $setting['value'];
	}
	$twilio_phone_number = $settings['twilio_phone_number'];
	
	function convert_number($number){  
		if(($number < 0) || ($number > 999999999)){  
			return "$number";
		}

		$Gn = floor($number / 1000000);  /* Millions (giga) */  
		$number -= $Gn * 1000000;  
		$kn = floor($number / 1000);     /* Thousands (kilo) */  
		$number -= $kn * 1000;  
		$Hn = floor($number / 100);      /* Hundreds (hecto) */  
		$number -= $Hn * 100;  
		$Dn = floor($number / 10);       /* Tens (deca) */  
		$n = $number % 10;               /* Ones */  

		$res = "";  

		if($Gn){  
			$res .= convert_number($Gn)." Million";  
		}  

		if($kn){  
			$res .= (empty($res) ? "" : " ").convert_number($kn)." Thousand";  
		}

		if($Hn){  
			$res .= (empty($res) ? "" : " ").convert_number($Hn)." Hundred";  
		}  

		$ones = array("", "One", "Two", "Three", "Four", "Five", "Six",  
			"Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",  
			"Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen",  
			"Nineteen");  
		$tens = array("", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty",  
			"Seventy", "Eighty", "Ninety");  

		if($Dn || $n){  
			if(!empty($res)){
			   $res .= " ";  
			}  

			if($Dn < 2){  
				$res .= $ones[$Dn * 10 + $n];  
			}else{  
				$res .= $tens[$Dn];  

				if($n){  
					$res .= "-" . $ones[$n];  
				}  
			}  
		}  

		if(empty($res)){  
			$res = "zero";  
		}

		return $res;  
	}
	
	function money_string($number){
		$cents = intval(substr($number, -2));
		$dollars = substr($number, 0, (strlen($number) - 2));
		$dollar_string = convert_number($dollars);  //convert to words (see function above)  
		if($dollars == '1'){
			$dollar_string .= " Dollar";
		}else{
			$dollar_string .= " Dollars";
		}
		return $dollar_string.' and '.$cents.' Cents';  
	}
	
	// Process button pressed on 2nd level menu and present next level options
	
	// Lottery Related
	if($_REQUEST['submenu'] == 'lottery'){
		if($_REQUEST['Digits'] == '9'){
			// 1-9 send back to lotto menu
			header("Location: '.$path.'/lib/framework/twilio/call_responder_actions.php");
			die;
		}elseif($_REQUEST['Digits'] == '1'){
			// 1-1 purchase lotto ticket
			
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=purchase_lotto_menu" method="GET">
						<Say voice="alice" language="en-US">
							To purchase a lottery ticket using your favourite bets, press 1.
							To save a new favourite bet for later use, press 2.
							To place a bet by speaking to a customer service representative, press 3.
							For instructions on how to place a lottery bet using text messages, press 4.
						</Say>
					</Gather>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
				</Response>
			';			
		}elseif($_REQUEST['Digits'] == '2'){
			// 1-2 enter ticket number to get info about it
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Gather timeout="30" finishOnKey="#" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=lotto_ticket_submit" method="GET">
						<Say voice="alice" language="en-US">Please enter your ticket number, followed by the pound key.</Say>
					</Gather>
					<Say voice="alice" language="en-US">We did not receive any input. Goodbye!</Say>
				</Response>
			';
		}elseif($_REQUEST['Digits'] == '3'){
			// 1-3 hear recently drawn winning numbers
			$today_dt = new DateTime('now');
			$date_str = $today_dt->format("Y-m-d");
			$data = $db->query("SELECT lg.number_of_balls, lg.name, lh.short_name as house_name, lwn.draw_numbers, lh.web_cutoff_time, lwn.is_override FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date='".$date_str."' ORDER BY lg.name ASC LIMIT 40");
			
			// need to add option to hear more
			
			if(count($data) < 1){
				// no drawings for today, use yesterday instead
				$today_dt = new DateTime('yesterday');
				$date_str = $today_dt->format("Y-m-d");
				$data = $db->query("SELECT lg.number_of_balls, lg.name, lh.short_name as house_name, lwn.draw_numbers, lh.web_cutoff_time, lwn.is_override FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date='".$date_str."' ORDER BY lg.name ASC LIMIT 40");
				
				$say_date = '<Say voice="alice" language="en-US">There are no winning numbers yet for today. </Say>';
				$say_date .= '<Say voice="alice" language="en-US">Here are the winning lottery numbers for yesterday, '.$today_dt->format('l \t\h\e jS \of F Y').'. </Say>';
			}else{
				$say_date = '<Say voice="alice" language="en-US">Here are the winning lottery numbers for '.$today_dt->format('l \t\h\e jS \of F Y').'. </Say>';
			}	
			
			//Build the list
			for($x=0; $x<count($data); $x++){
				if($data[$x]['is_override'] == "1" && $data[$x]['web_cutoff_time'] >= date("H:i:s",time())){

				}else{
					$say .= "<Say>".$data[$x]['name']." ,,, ";
					
					if($data[$x]['number_of_balls'] == 2){
						$say .= implode(", ", str_split(substr($data[$x]['draw_numbers'],-2,2)))." ,,,,</Say>\n";
					}else{
						$say .= implode(", ", str_split($data[$x]['draw_numbers']))." ,,,,</Say>\n";
					}
				}
			}
			
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					'.$say_date.'
					'.$say.'
					<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php" method="GET">
						<Say voice="alice" language="en-US">Press 1 to return the the previous menu or nothing to hear the winning numbers again.</Say>
					</Gather>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=3&amp;submenu=lottery</Redirect>
				</Response>
			';
		}else{
			// 1-unknown - send back to Lotto menu
			header("Location: '.$path.'/lib/framework/twilio/call_responder_actions.php");
			die;
		}
	}elseif($_REQUEST['submenu'] == 'purchase_lotto_menu'){	
		if($_REQUEST['Digits'] == '1'){
			// 1-1-1 purchase lotto ticket using a saved favorite bet
			$q = "SELECT *, `f`.`name` AS game_name FROM `lotto_bet_favorites` AS `f` LEFT JOIN `lotto_game` AS `g` ON `f`.`game_id` = `g`.`id` WHERE `user_id` = %i ORDER BY `call_in_option_number` ASC";
			$favorites = $db->query($q, array($_SESSION['caller_info']['id']));
			
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			
			if(count($favorites) >= 1){
				foreach($favorites as $favorite){
					if($favorite['call_in_audio_name'] != ''){
						$say .= '<Say voice="alice" language="en-US">Press '.$favorite['call_in_option_number'].' for </Say><Play>'.$favorite['call_in_audio_name'].'</Play>';
					}else{
						$say .= '<Say voice="alice" language="en-US">Press '.$favorite['call_in_option_number'].' for '.$favorite['game_name'].'</Say>';
					}
				}
				
				echo '
				<Response>
					<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=purchase_lotto_fav" method="GET">
						'.$say.'
					</Gather>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
				</Response>
				';
			}else{
				echo '
				<Response>
					<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=purchase_lotto_menu&amp;Digits=2" method="GET">
						<Say voice="alice" language="en-US">
							We could not find any favourite bets linked to your account. If you would like to create one, press 2, now. Otherwise, please wait while we return you to the previous menu.
						</Say>
					</Gather>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
				</Response>
				';
			}
		}elseif($_REQUEST['Digits'] == '2'){
			// 1-1-2 begin saving a new favorite bet
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Gather numDigits="3" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=new_fav_is_boxed&amp;game_id=5" method="GET">
						<Say voice="alice" language="en-US">
							For demo purposes, we will use Early Miami 3 Ball as the selected lottery game. Please use your keypad to enter a 3 ball pick for this game.
						</Say>
					</Gather>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
				</Response>
			';			
		}elseif($_REQUEST['Digits'] == '3'){
			// 1-1-3 place a bet from a live person
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Say voice="alice" language="en-US">Please wait while we connect you to a customer service representative.</Say>
					<Dial>+17246000808</Dial>
					<Say voice="alice" language="en-US">The call failed or the remote party hung up. Good bye.</Say>
				</Response>
			';
		}else{
			// 1-1-unknown - send back to Lotto Purchase menu
			header("Location: '.$path.'/lib/framework/twilio/call_responder_actions.php");
			die;
		}
	}elseif($_REQUEST['submenu'] == 'purchase_lotto_fav'){	
		// 1-1-1-fav pick selected
		$q = "SELECT *, `f`.`name` AS game_name, `f`.`id` AS fav_id FROM `lotto_bet_favorites` AS `f` LEFT JOIN `lotto_game` AS `g` ON `f`.`game_id` = `g`.`id` WHERE `user_id` = %i AND `call_in_option_number` = %i";
		if($favorite = $db->queryOneRow($q, array($_SESSION['caller_info']['id'], $_REQUEST['Digits']))){
			
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Say voice="alice" language="en-US">
						How much would you like to wager on '.$favorite['game_name'].'?
					</Say>
					<Gather finishOnKey="#" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=confirm_fav_bet_wager&amp;fav_id='.$favorite['fav_id'].'" method="GET">
						<Say voice="alice" language="en-US">
							Enter your bet in cents, followed by the pound sign.  For example to bet one dollar, enter 1-0-0.
						</Say>
					</Gather>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
				</Response>
			';
		}else{
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Say voice="alice" language="en-US">
						Invalid selection.
					</Say>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
				</Response>
			';
		}
	}elseif($_REQUEST['submenu'] == 'confirm_fav_bet_wager'){	
		// 1-1-1-fav pick selected
		$q = "SELECT * FROM `lotto_bet_favorites` AS `f` WHERE `f`.`id` = %i";
		$favorite = $db->queryOneRow($q, array($_REQUEST['fav_id']));
		
		header("content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo '
			<Response>
				<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=place_fav_bet&amp;fav_id='.$favorite['id'].'&amp;amount='.$_REQUEST['Digits'].'" method="GET">
					<Say voice="alice" language="en-US">
						Are you sure that you want to place a bet in the amount of '.money_string($_REQUEST['Digits']).' for '.$favorite['name'].'?
						Press any key to confirm or nothing to cancel.
					</Say>
				</Gather>
				<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
			</Response>
		';
	}elseif($_REQUEST['submenu'] == 'place_fav_bet'){	
		// 1-1-1-fav pick selected
		$q = "SELECT * FROM `lotto_bet_favorites` AS `f` WHERE `f`.`id` = %i";
		$favorite = $db->queryOneRow($q, array($_REQUEST['fav_id']));
		
		// convert cents to dollars
		$bet_amount = $_REQUEST['amount'] / 100;
		
		// actually place the bet
		$core->call_in_lotto_bet($favorite['user_id'], $favorite['game_id'], $favorite['pick'], $favorite['is_boxed'], $bet_amount);
		
		header("content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo '
			<Response>
				<Say voice="alice" language="en-US">
					You have successfully placed a bet in the amount of '.money_string($_REQUEST['amount']).' for '.$favorite['name'].'.
					Good Luck! 
					We will now take you back to the previous menu.
				</Say>
				<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
			</Response>
		';
	}elseif($_REQUEST['submenu'] == 'new_fav_is_boxed'){	
		// 1-1-2 is new fav boxed
		header("content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo '
			<Response>
				<Say voice="alice" language="en-US">
					You selected '.implode(", ", str_split($_REQUEST['Digits'])).' for Early Miami 3 Ball.
				</Say>
				<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=confirm_new_fav_pick&amp;game_id=5&amp;pick='.$_REQUEST['Digits'].'" method="GET">
					<Say voice="alice" language="en-US">
						If this is correct, Press 1 for a boxed bet or 0 for straight..  Otherwise, please press nothing and wait to be taken back to the previous menu.
					</Say>
				</Gather>
				<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
			</Response>
		';
	}elseif($_REQUEST['submenu'] == 'confirm_new_fav_pick'){	
		// 1-1-2-confirm saving a new favorite bet
		$say = $_REQUEST['Digits'] == 1 ? "Great.  We will make that a boxed bet." : "Okay. Straight bet it is.";
	
		header("content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo '
			<Response>
				<Say voice="alice" language="en-US">
					'.$say.'
				</Say>
				<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=record_fav_bet_audio&amp;game_id=5&amp;pick='.$_REQUEST['pick'].'&amp;is_boxed='.$_REQUEST['Digits'].'" method="GET">
					<Say voice="alice" language="en-US">
						If this is correct, press any key.  Otherwise, please wait to be taken back to the previous menu
					</Say>
				</Gather>
				<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=2&amp;submenu=lottery</Redirect>
			</Response>
		';	
	}elseif($_REQUEST['submenu'] == 'record_fav_bet_audio'){
		// 1-1-2-record save a new favourite bet audio name
		
		// get current highest favorite number
		$q = "SELECT call_in_option_number FROM `lotto_bet_favorites` WHERE `user_id` = %i ORDER BY `call_in_option_number` DESC LIMIT 1";
		$favorites = $db->queryOneRow($q, array($_SESSION['caller_info']['id']));
		
		$fav_num = $favorites['call_in_option_number'] + 1;
		
		// save new favorite bet to db
		$q = "INSERT INTO `lotto_bet_favorites` (`id`, `user_id`, `game_id`, `pick`, `is_boxed`, `name`, `call_in_audio_name`, `call_in_option_number`) VALUES (NULL, %i, %i, %i, %i, %s, NULL, %i)";
		$db->query($q, array($_SESSION['caller_info']['id'], $_REQUEST['game_id'], $_REQUEST['pick'], $_REQUEST['is_boxed'], 'Early Miami 3 Ball - '.implode("-", str_split($_REQUEST['pick'])), $fav_num));
		
		header("content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo '
			<Response>
				<Say voice="alice" language="en-US">
					Please re-cord a name for this entry after the tone.
				</Say>
				<Record maxLength="30" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=save_fav_bet_audio&amp;fav_num='.$fav_num.'" />
			</Response>
		';
	}elseif($_REQUEST['submenu'] == 'save_fav_bet_audio'){
		// 1-1-2-save Save new favourite bet audio
		
		// save audio url to db
		$q = "UPDATE `lotto_bet_favorites` SET `call_in_audio_name` = %s WHERE `user_id` = %i AND `call_in_option_number` = %i";
		if($db->queryDirect($q, array($_REQUEST['RecordingUrl'], $_SESSION['caller_info']['id'], $_REQUEST['fav_num']))){
			
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Say voice="alice" language="en-US">
						You successfully created a new favorite bet called, 
					</Say>
					<Play>'.$_REQUEST['RecordingUrl'].'</Play>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
				</Response>
			';
		}else{
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Say voice="alice" language="en-US">
						I am sorry but something went wrong.  Please try again later.
					</Say>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1&amp;submenu=lottery</Redirect>
				</Response>
			';
		}
	}elseif($_REQUEST['submenu'] == 'lotto_ticket_submit'){
		// 1-2 confirm ticket number for lookup
		header("content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo '
			<Response>
				<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?lotto_ticket_number='.$_REQUEST['Digits'].'" method="GET">
					<Say voice="alice" language="en-US">Did you enter '.implode(" ", str_split($_REQUEST['Digits'])).'? Press 1 for yes or 2 to retry.</Say>
				</Gather>
				<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=lottery</Redirect>
			</Response>
		';
	}elseif(isset($_REQUEST['lotto_ticket_number'])){
		// 1-2 see if we should send back to retry entering ticket number
		if($_REQUEST['Digits'] == '2'){
			// send back to lotto ticket menu
			header("Location: '.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=lotto_ticket&Digits=2");
			die;
		}
		
		header("content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo '
			<Response>
		';
		
		// lookup ticket
		$q = "SELECT * FROM `lotto_ticket` WHERE `ticket_number`= %i";
		$ticket_info = $db->queryOneRow($q, array($_REQUEST['lotto_ticket_number']));
		
		// generate expiration message
		if($ticket_info['expiration_date']){
			$expiration_dt = new DateTime($ticket_info['expiration_date']);
			$now_dt = new DateTime();
			$interval_dt = $expiration_dt->diff($now_dt);
			$expires_in = $core->format_interval($interval_dt, true);

			$expiration_msg = $now_dt < $expiration_dt ? "This ticket will expire in ".$expires_in." on ".$expiration_dt->format('l jS \of F Y \a\t h:i A')."." : "This ticket expired on ".$expiration_dt->format('l jS \of F Y \a\t h:i A').".";
		
			echo '
				<Say voice="alice" language="en-US">
					'.$expiration_msg.'
				</Say>
			';
		}
		
		echo '
			<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=lottery</Redirect>
			</Response>
		';
	}else{
		// Process button pressed on main menu and present 2nd level options
		
		// send back to main menu
		if($_REQUEST['Digits'] == '9'){
			// send back to main menu
			header("Location: '.$path.'/lib/framework/twilio/call_responder.php");
			die;
		}
		
		if($_REQUEST['Digits'] == '0'){
			// call operator
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Say voice="alice" language="en-US">Please wait while we connect you to a customer service representative.</Say>
					<Dial>+17246000808</Dial>
					<Say voice="alice" language="en-US">The call failed or the remote party hung up. Goodbye.</Say>
				</Response>
			';
		}elseif($_REQUEST['Digits'] == '1'){
			// 1 lottery options
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Gather numDigits="1" action="'.$path.'/lib/framework/twilio/call_responder_actions.php?submenu=lottery" method="GET">
						<Say voice="alice" language="en-US">
							To purchase a lottery ticket, press 1.
							To get information about a previously purchased ticket, press 2.
							To hear todays winning numbers, press 3.
						</Say>					
					</Gather>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder_actions.php?Digits=1</Redirect>
				</Response>
			';
		}elseif($_REQUEST['Digits'] == '2'){
			// 2 sports betting options
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			
		}elseif($_REQUEST['Digits'] == '3'){
			// 3 account balance
			
			$avail_bal = money_string($_SESSION['caller_info']['available_balance']);
			
			header("content-type: text/xml");
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo '
				<Response>
					<Say voice="alice" language="en-US">
						Your current available balance is '.$avail_bal.'
					</Say>
					<Redirect method="GET">'.$path.'/lib/framework/twilio/call_responder.php?pin_entered=1</Redirect>
				</Response>
			';
		}else{
			// unknown - send back to main menu
			header("Location: '.$path.'/lib/framework/twilio/call_responder.php");
			die;
		}
	}
?>
