<?php
	require_once('phpmailer/PHPMailerAutoload.php');

	class Mailer{
	   function send_email($to_address, $subject, $html_body, $text_body = "", $debug = false){
			global $db;
			// get current settings from db
			$settings = array();
			$q = "SELECT * FROM `settings`";
			$settings_info = $db->query($q);
			foreach($settings_info as $setting){
				$settings[$setting['setting']] = $setting['value'];
			}
		   
			// use PHPMailer for sending email
			$mail = new PHPMailer;
			if($debug == true){
				$mail->SMTPDebug = 3;                             // Enable verbose debug output only for debugging
			}
			$mail->isSMTP();                                    // Set mailer to use SMTP
			$mail->Host = $settings['mailer_host'];				// Specify main and backup SMTP servers
			$mail->Username = $settings['mailer_username'];            // SMTP username
			$mail->Password = $settings['mailer_password'];                       // SMTP password
			$mail->Port = $settings['mailer_port'];                                   // TCP port to connect to
			$mail->From = $settings['mailer_from_email'];
			$mail->FromName = $settings['mailer_from_name'];
			$mail->addAddress($to_address);
			$mail->isHTML(true);                                // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $html_body;
			$mail->AltBody = $text_body;

			if(!$mail->send()){
				echo 'Message could not be sent.<br>';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
				
				//log to database
				$q = "INSERT INTO `log_email` (`id`, `to_email`, `from_email`, `message`, `is_success`, `error_details`) VALUES (NULL, '".addslashes($to_address)."', '".addslashes($settings['mailer_from_email'])."', '".addslashes($html_body)."', '0', '".addslashes($mail->ErrorInfo)."');";
				$db->queryInsert($q);
				return false;
			}else{
				$q = "INSERT INTO `log_email` (`id`, `to_email`, `from_email`, `message`, `is_success`, `error_details`) VALUES (NULL, '".$to_address."', '".$settings['mailer_from_email']."', '".$html_body."', '1', NULL);";
				$db->queryInsert($q);
				return true;
			}
		}
		
	   /*
		* sendActivation - Sends an activation e-mail to the newly
		* registered user with a link to activate the account.
		*/
		
		function sendActivation($user, $email, $pass, $token){
			$subject = "CSCLotto - Welcome!";
			$body = $user.",\n\n"
				."Welcome to CSCLotto! A new account has been registered using your email address with the following information:\n\n"
				."Username: ".$user."\n\n"
				."Password: ".$pass."\n\n"
				."Please visit the following link to activate your account: \n"
				.ROOTPATH."/register.php?mode=activate&user=".urlencode($user)."&activatecode=".$token." \n\n";
			$html_body = "<b>Welcome to CSCLotto!</b> A new account has been registered using your email address with the following information:<br><br>"
				."<b>Username: </b>".$user."<br><br>"
				."<b>Password: </b>".$pass."<br><br>"
				."Please visit the following link to activate your account:<br>"
				.ROOTPATH."/register.php?mode=activate&user=".urlencode($user)."&activatecode=".$token." <br><br>";
			return $this->send_email($email, $subject, $html_body, $body);
		}

		/**
		* adminActivation - Sends an activation e-mail to the newly
		* registered user explaining that admin will activate the account.
		*/

		function adminActivation($user, $email, $pass){
			/*
			$subject = "CSCLotto - Welcome!";
			$body = $user.",\n\n"
				."Welcome! You've just registered at ".$config['SITE_NAME']." "
				."with the following username:\n\n"
				."Username: ".$user."\n\n"
				."Your account is currently inactive and will need to be approved by an administrator. "
				."Another e-mail will be sent when this has occured.\n\n"
				."Thank you for registering.\n\n"
				.$config['SITE_NAME'];

			return $this->send_email($email, $subject, $body);
			*/
		}

		/**
		* activateByAdmin - Sends an activation e-mail to the admin
		* to allow him or her to activate the account. E-mail will appear
		* to come FROM the user using the e-mail address he or she registered
		* with.
		*/

		function activateByAdmin($user, $email, $pass, $token){
			/*
			$subject = "CSCLotto - User Account Activation!";
			$body = "Hello Admin,\n\n"
				.$user." has just registered at ".$config['SITE_NAME']
				." with the following details:\n\n"
				."Username: ".$user."\n"
				."E-mail: ".$email."\n\n"
				."You should check this account and if neccessary, activate it. \n\n"
				."Use this link to activate the account.\n\n"
				.$config['WEB_ROOT']."register.php?mode=activate&user=".urlencode($user)."&activatecode=".$token." \n\n"
				."Thanks.\n\n"
				.$config['SITE_NAME'];

			$adminemail = $config['EMAIL_FROM_ADDR'];
			return $this->send_email($email, $subject, $body);
			*/
		}

		/**
		* adminActivated - Sends an e-mail to the user once
		* admin has activated the account.
		*/

		function adminActivated($user, $email){
			/*
			$subject = "CSCLotto - Welcome!";
			$body = $user.",\n\n"
				."Welcome! You've just registered at ".$config['SITE_NAME']." "
				."with the following username:\n\n"
				."Username: ".$user."\n\n"
				."Your account has now been activated by an administrator. "
				."Please click here to login - "
				.$config['WEB_ROOT']."\n\nThank you for registering.\n\n"
				.$config['SITE_NAME'];

			return $this->send_email($email, $subject, $body);
			*/
		}
		
		/**
		* sendWelcome - Sends an activation e-mail to the newly
		* registered user with a link to activate the account.
		*/

		function sendWelcome($user, $email, $pass){
			$subject = "CSCLotto - Welcome!";
			$body = "Welcome! A new account has been registered using your email address.  Your login credentials are as follows.\n\n"
				."<b>Username: </b>".$user."\n"
				."<b>Password: </b>".$pass."\n\n"
				."For security reasons, you may be required to change your password "
				."to something else after signing in for the first time.\n\n";
			$html_body = "Welcome! A new account has been registered using your email address.  Your login credentials are as follows.<br><br>"
				."<b>Username: </b>".$user."<br>"
				."<b>Password: </b>".$pass."<br><br>"
				."For security reasons, you may be required to change your password "
				."to something else after signing in for the first time.<br><br>";
			return $this->send_email($email, $subject, $html_body, $body);
		}
	   
		/**
		* sendNewPass - Sends the newly generated password
		* to the user's email address that was specified at
		* sign-up.
		*/

		function sendNewPass($user, $email, $pass){
			$subject = "CSCLotto - Your New Password!";
			$body = "We've generated a new password for you at your "
				."request. You can use this new password with your "
				."username to log in to CSCLotto.\n\n"
				."Username: ".$user."\n"
				."New Password: ".$pass."\n\n"
				."For security reasons, you will be required to change your password "
				."to something else after signing in for the first time.\n\n";
			$html_body = "We've generated a new password for you at your "
				."request. You can use this new password with your "
				."username to log in to <a href='https://CSCLotto.com/'>CSCLotto</a>.<br><br>"
				."<b>Username: </b>".$user."<br>"
				."<b>New Password: </b>".$pass."<br><br>"
				."For security reasons, you will be required to change your password "
				."to something else after signing in for the first time.<br><br>";
			return $this->send_email($email, $subject, $html_body, $body);
		}
	}

	/* Initialize mailer object */
	$mailer = new Mailer;
?>
