<?php
class CORE{
    private static $initialized = false;
	
	private static $xml_lotto_games = array();

    function CORE(){
        if(CORE::$initialized === false) {
			CORE::$initialized = true;
        }
    }
	
	public function new_customer_number(){
		global $db;
		mt_srand(hexdec(substr(md5(microtime()), -8)) & 0x7fffffff);
		$customer_number = mt_rand(1000000, 99999999);
		$q = "SELECT COUNT(*) as match_count FROM `customers` WHERE customer_number='".$customer_number."'";
		$result = $db->queryOneRow($q);
		if($result['match_count'] == 0){
			return $customer_number;
		}else{
			$this->new_customer_number();
		}
	}
	
	public function new_customer_card_number(){
		global $db;
		mt_srand(hexdec(substr(md5(microtime()), -8)) & 0x7fffffff);
		$card_number = mt_rand(1000000, 99999999);
		$q = "SELECT COUNT(*) as match_count FROM `customers` WHERE card_number='".$card_number."'";
		$result = $db->queryOneRow($q);
		if($result['match_count'] == 0){
			return $card_number;
		}else{
			$this->new_customer_card_number();
		}
	}
	
	/* ------------------  BEGIN ALERT FUNCTIONS  ---------------------*/
	public function send_alert($subject, $message, $user_id, $rule_id="NULL", $table_id="NULL", $alert_value="",$function_name = "NULL",$action_id="NULL"){
		global $db;
		
		$date = new DateTime('now');
		$date_str = $date->format("Y-m-d H:i:s");
		$q = "INSERT INTO `alerts` (`id`, `user_id`, `subject`, `message`, `date_created`, `rule_id`, `table_id`, `alert_value`,`function_name`,`action_id`) VALUES (NULL, $user_id, '".$subject."', '".$message."', '".$date_str."', $rule_id, $table_id, '".$alert_value."','".$function_name."','".$action_id."')";
		return $db->queryInsert($q);
	}
	
	public function send_alert_by_group($subject, $message, $group_id, $rule_id="NULL", $table_id="NULL", $alert_value="",$function_name="NULL",$action_id = "NULL"){
		global $db;
		
		$q = "SELECT system_lookup_query FROM `sec_groups` WHERE `id` = '".$group_id."'";
		$results = $db->queryOneRow($q);
		$members = $db->query($results['system_lookup_query']);
		foreach($members as $member){
			$this->send_alert($subject, $message, $member['user_id'], $rule_id, $table_id, $alert_value,$function_name,$action_id);
		}
	}
	
	public function send_alert_active_accounts($subject, $message){
		global $db;
		
		$q = "SELECT * FROM `active_users` AS `au` JOIN `users` AS `u` ON `u`.`username`=`au`.`username`";
		$members = $db->query($q);
		foreach($members as $member){
			$this->send_alert($subject, $message, $member['id']);
		}
	}
	
	public function log_sigificant_event($event_type, $event, $details){
		global $db;
		
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		
		$q = "INSERT INTO `significant_events` (`id`, `created_on`, `event_type`, `event`, `details`) VALUES (NULL, %s, %s, %s, %s)";
		return $db->queryInsert($q, array($now, $event_type, $event, $details));
	}
	
	/* ------------------  END ALERT FUNCTIONS  ---------------------*/
	
	/* ------------------  BEGIN UTILITY FUNCTIONS  ---------------------*/
	public function print_pre($array){
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}
	
	public function xml_to_array($xml){
		// get xml game list and covert to an array
		$xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xml);
		$xml = simplexml_load_string($xml);
		$json = json_encode($xml);
		return json_decode($json,true);
	}
	
	public function format_interval($interval, $less_than_min = false, $single_term = false, $countdown = false) { 
		$doPlural = function($nb, $str){return $nb>1?$str.'s':$str;}; // adds plurals 
		
		$format = array(); 
		if($countdown == false){
			if($interval->y !== 0) {
				if($countdown == false){
					$format[] = "%y ".$doPlural($interval->y, "year"); 
				}else{
					$format[] = sprintf('%02d', $interval->y);
				}
			}
			if($interval->m !== 0) { 
				if($countdown == false){
					$format[] = "%m ".$doPlural($interval->m, "month"); 
				}else{
					$format[] = sprintf('%02d', $interval->m);
				}
			} 
			if($interval->d !== 0) {  
				if($countdown == false){
					$format[] = "%d ".$doPlural($interval->d, "day"); 
				}else{
					$format[] = sprintf('%02d', $interval->d);
				}
			} 
			if($interval->h !== 0) {  
				if($countdown == false){
					$format[] = "%h ".$doPlural($interval->h, "hour"); 
				}else{
					$format[] = sprintf('%02d', $interval->h);
				}
			} 
			if($interval->i !== 0) {  
				if($countdown == false){
					$format[] = "%i ".$doPlural($interval->i, "minute"); 
				}else{
					$format[] = sprintf('%02d', $interval->i);
				}
			} 
			if($interval->s !== 0) { 
				if(count($format) == 0 && $less_than_min == true){ 
					return "less than a minute"; 
				} else {  
					if($countdown == false){
						$format[] = "%s ".$doPlural($interval->s, "second"); 
					}else{
						$format[] = sprintf('%02d', $interval->s);
					}
				} 
			}
		}else{
			$format[] = sprintf('%02d', $interval->h);
			$format[] = sprintf('%02d', $interval->i);
			$format[] = sprintf('%02d', $interval->s);
		}
		
		// We use the two biggest parts 
		if(count($format) > 1){
			if($countdown == true){
				$format = array_shift($format).":".array_shift($format).":".array_shift($format);
			}elseif($single_term == false){
				$format = array_shift($format)." and ".array_shift($format);
			}else{
				$format = array_shift($format);
			}
		} else { 
			$format = array_pop($format); 
		} 
		
		return $interval->format($format); 
	}
	
	public function get_pay_factor($pick, $bet_amount, $is_boxed){
		if($is_boxed == 0){
			return $bet_amount;
		}else{
			$unique_permutations = $this->permutations($pick, true);
			return $bet_amount / count($unique_permutations);
		}
	}
	
	public function factorial($number){
		if($number==1){
			return 1;
		}else{
			return $number * $this->factorial($number-1);
		}
	}
	
	public function permutations($str, $unique = false){
		// make sure that it is a string
		$str = strval($str);
		
		/* If we only have a single character, return it */
		if (strlen($str) < 2) {
			return array($str);
		}
	 
		/* Initialize the return value */
		$permutations = array();
	 
		/* Copy the string except for the first character */
		$tail = substr($str, 1);
	 
		/* Loop through the permutations of the substring created above */
		foreach ($this->permutations($tail) as $permutation) {
			/* Get the length of the current permutation */
			$length = strlen($permutation);
	 
			/* Loop through the permutation and insert the first character of the original
			string between the two parts and store it in the result array */
			for ($i = 0; $i <= $length; $i++) {
				$permutations[] = substr($permutation, 0, $i) . $str[0] . substr($permutation, $i);
			}
		}
	 
		/* Return the result */
		if($unique == true){
			return array_unique($permutations);
		}else{
			return $permutations;
		}
	}
	
	public function generate_random_integer($min, $max, $pad_zeros = true){
		mt_srand(hexdec(substr(md5(microtime()), -8)) & 0x7fffffff);
		
		// preserve leading zeros
		if($pad_zeros == true){
			$num_digits = strlen((string)$max);
			$format_str = "%0".$num_digits."d";
			return sprintf($format_str, mt_rand($min, $max));
		}else{
			return mt_rand($min, $max);
		}
	}
	
	public function generate_random_string($length, $case_sensitive = false){
		mt_srand(hexdec(substr(md5(microtime()), -8)) & 0x7fffffff);
		// Character List to Pick from
		if($case_sensitive == false){
			$chrList = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}else{
			$chrList = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}

		// Minimum/Maximum times to repeat character List to seed from
		$chrRepeatMin = 1; // Minimum times to repeat the seed string
		$chrRepeatMax = $length; // Maximum times to repeat the seed string

		// Length of Random String returned
		$chrRandomLength = $length;

		// The ONE LINE random command with the above variables.
		return substr(str_shuffle(str_repeat($chrList, mt_rand($chrRepeatMin,$chrRepeatMax))), 1, $chrRandomLength);
	}
	
	public function flatten_array($arr){
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($arr));
		return iterator_to_array($it, false);
	}
	
	public function get_string_between($text, $start_str, $end_str, $instance = 1){
		// append a space to make matching work easier
		$text = " ".$text;
		$loc = 0;
		
		for($i=1; $i<=$instance; $i++){
			$loc++;
			$loc = strpos($text, $start_str, $loc);
		}
		
		if($loc == 0){
			return "";
		}
		$loc += strlen($start_str);
		$len = strpos($text, $end_str, $loc) - $loc;
		return substr($text, $loc, $len);
	}
	
	public function get_string_between_special($text, $start_str, $end_str, $instance = 1){
		// append a space to make matching work easier
		$text = " ".$text." ";
		$start_loc = 0;
		
		for($i=1; $i<=$instance; $i++){
			$start_loc++;
			$start_loc = strpos($text, $start_str, $start_loc);
		}
		
		if($start_loc == 0){
			return "";
		}
		$start_loc += strlen($start_str);
		$end_loc = strrpos($text, $end_str);
		$len = $end_loc - $start_loc;
		return substr($text, $start_loc, $len); 
	}
	
	public function get_quoted_strings($text, $strip_quotes = true){
		preg_match_all("/(?:(?:\"(?:\\\\\"|[^\"])+\")|(?:'(?:\\\'|[^'])+'))/is", $text, $matches);
		
		if($strip_quotes == true){
			$new_matches = array();
			foreach($matches as $match){
				$match = str_replace("'", "", $match);
				$match = str_replace('"', "", $match);
				$new_matches[] = $match;
			}
			return $new_matches[0];
		}
		return $matches[0];
	}
	
	public function camel_case_to_underscore($text, $multiple_caps = false){
		if($multiple_caps == false){
			return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $text));
		}else{
			return strtolower(preg_replace('/(?<!^)([A-Z])/', '_$1', $text));
		}
	}
	
	private $recursive_file_list = null;
	
	public function get_recursive_file_list($root_dir, $file_mask = ""){
		$dir = new DirectoryIterator($root_dir);

		foreach($dir as $file_info) {
			// ignore parent and current directory entries
			if($file_info == '.' || $file_info == '..') continue;

			if($file_info->isDir()){
				$this->get_recursive_file_list("$root_dir/$file_info");
			}else{
				$file_parts = pathinfo($file_info->getFilename());
			
				// see if it's a valid file based on mask
				if($file_mask === ""){
					$this->recursive_file_list[] = str_ireplace($_SERVER['DOCUMENT_ROOT'], "", $root_dir)."/".$file_info->getFilename();
				}elseif(strtolower($file_parts['extension']) == strtolower($file_mask)){
					$this->recursive_file_list[] = str_ireplace($_SERVER['DOCUMENT_ROOT'], "", $root_dir)."/".$file_info->getFilename();
				}
			}
		}
		return $this->recursive_file_list;
	}
	
	/* ------------------  END UTILITY FUNCTIONS  --------------------- */
	
	
	/* ------------------  BEGIN SECURITY FUNCTIONS  ------------------ */
	public function index_pages(){
		global $db;

		$admin_pages = $this->get_recursive_file_list($_SERVER['DOCUMENT_ROOT']."/admin", "php");

		//Create a page list lookup
		$page_list = $db->query("SELECT `id`, `path`, `is_deleted` FROM sec_pages");
		foreach($page_list as $page){
			$page_lookup[$page['path']] = $page['is_deleted'];
		}

		//Loop through skipping all base directory files. If it is not in the look up, add to the database
		foreach($admin_pages as $page){
			$directories = explode("/",$page);
			if(count($directories)>3){
				if(!isset($page_lookup[$page])){
					$page_id = $db->queryInsert("INSERT INTO sec_pages (`name`,`path`,`is_deleted`) VALUE ('','".$page."',0)");
					$action_id = $db->queryInsert("INSERT INTO sec_page_actions (`page_id`,`action_name`) VALUE (".$page_id.",'default')");
					$db->query("INSERT INTO sec_group_permissions (`group_id`,`action_id`,`permission_level_id`) VALUE (-1,".$action_id.",-1)");
				}elseif(isset($page_lookup[$page]) && $page_lookup[$page]==1){
					$db->query("UPDATE `sec_pages` SET is_deleted=0 WHERE path='".$page."'");
				}
				//Mark as processed to do clean up at end.
				$page_lookup[$page] = "processed";
			}
		}

		//Look for unprocessed pages to be soft deleted from the database.
		foreach($page_lookup as $page => $processed){
			if($processed!="processed" && $page != "/admin/index.php"){
				$db->query("UPDATE `sec_pages` SET is_deleted=1 WHERE path='".$page."'");
			}
		}
	}
	
	function check_file_hashes($file = ""){
		global $db;

		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");

		if($file == ""){
			// process all files
			$files = $this->get_recursive_file_list($_SERVER['DOCUMENT_ROOT'], "");
			
			$approved_files = array();
			$q = "SELECT * FROM `file_hashes`";
			$db_files = $db->query($q);
			foreach($db_files as $dbf){
				// default all files to deleted unless found
				$approved_files[$dbf['path']] = "deleted";
			}
		}else{
			// process one specific file
			$files[0] = $file;
		}
		
		$results = array();
		$results['path_count'] = count($files);
		$results['approved_count'] = 0;
		$results['modified_count'] = 0;
		$results['new_count'] = 0;
		$results['deleted_count'] = 0;
		
		// Check each file against approved hashes in db
		foreach($files as $file){
			$file_hash = sha1_file($_SERVER['DOCUMENT_ROOT'].$file);
			$file_size = filesize($_SERVER['DOCUMENT_ROOT'].$file);
			$file_modified_on = date("Y-m-d H:i:s", filemtime($_SERVER['DOCUMENT_ROOT'].$file));
			
			$q = "SELECT *, COUNT(*) as result_count FROM `file_hashes` WHERE path='".$file."'";
			$db_info = $db->queryOneRow($q);
			if($db_info['result_count'] > 0){
				if($file_hash != $db_info['approved_hash'] || $file_size != $db_info['approved_filesize']){
					//echo "File Change Detected: $file<br>";
					//echo "Approved Hash: $file_hash, Current Hash: ".$db_info['approved_hash']."<br>";
					//echo "Approved Size: $file_size, Current Size: ".$db_info['approved_filesize']."<br>";
					//echo "Approved Modification Date: $file_modified_on, Current Modification Date: ".$db_info['approved_modified_date']."<br>";
					$status = "modified";
					$results['modified_count']++;
				}else{
					$status = "approved";
					$results['approved_count']++;
				}
			}else{
				//echo "New File Detected: $file<br>";
				//echo "Hash: ".$file_hash."<br>";
				//echo "Size: ".$file_size."<br>";
				//echo "Modification Date: ".$file_modified_on."<br>";
				$status = "new";
				$results['new_count']++;
			}
			
			$q = "INSERT INTO `file_hash_checks` (`id`, `datetime_checked`, `file_path`, `size`, `hash`, `modified_on`, `status`) VALUES (NULL, '".$now."', '".$file."', '".$file_size."', '".$file_hash."', '".$file_modified_on."', '".$status."');";
			//echo "<br>$q<br>";
			$db->queryInsert($q);
			//echo "<hr>";
			
			$approved_files[$file] = $status;
		}
		
		foreach($approved_files as $file => $status){
			if($status == "deleted"){
				$results['deleted_count']++;
				//echo "File Deleted: $file<br>";
				$q = "INSERT INTO `file_hash_checks` (`id`, `datetime_checked`, `file_path`, `size`, `hash`, `modified_on`, `status`) VALUES (NULL, '".$now."', '".$file."', '".$file_size."', '".$file_hash."', '".$file_modified_on."', '".$status."');";
				//echo "<br>$q<br>";
				$db->queryInsert($q);
				//echo "<hr>";
			}
		}
		
		return $results;
	}
	
	public function hash_approved_files(){
		global $db;

		// empty table
		$db->query("TRUNCATE TABLE `file_hashes`");
		
		$user_id = isset($session->userinfo['id']) ? $session->userinfo['id'] : 'NULL';
		
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		
		$response = array();
		
		$files = $this->get_recursive_file_list($_SERVER['DOCUMENT_ROOT'], "");
		
		$response['file_count'] = count($files);
		$response['insert_count'] = 0;

		// Loop through all files. If it is not in the look up, add to the database
		foreach($files as $file){
			$response[$file]['hash'] = sha1_file($_SERVER['DOCUMENT_ROOT'].$file);
			$response[$file]['size'] = filesize($_SERVER['DOCUMENT_ROOT'].$file);
			$response[$file]['modified_on'] = date("Y-m-d H:i:s", filemtime($_SERVER['DOCUMENT_ROOT'].$file));
			
			$q = "INSERT INTO `file_hashes` (`id`, `path`, `approved_hash`, `approved_modified_date`, `approved_filesize`, `approved_on`, `approved_by`) VALUES (NULL, '".$file."', '".$response[$file]['hash']."', '".$response[$file]['modified_on']."', '".$response[$file]['size']."', '".$now."', ".$user_id.");";
			if($db->queryInsert($q) > 0){
				$response['insert_count']++;
			}
		}
		
		$response['fail_count'] = $response['file_count'] - $response['insert_count'];
		return $response;
	}
	
	public function user_sec_group_membership($user_id = -1){
		global $db, $session;
		$response = array();
		
		// use passed userid or currently logged in user
		if($user_id == -1){
			$user_id = $session->userinfo['id'];
		}else{
			// get passed user info
			$q = "SELECT * FROM `".TBL_USERS."` WHERE `id`='".$user_id."'";
			$user_info = $db->queryOneRow($q);
		}
		
		// get all dynamic security groups
		$q = "SELECT * FROM `sec_groups` WHERE `is_system` = '1'";
		$sec_groups = $db->query($q);
		
		// get dynamic group membership
		foreach($sec_groups as $group){
			if($group['is_system'] == 1){
				// use lookup query to get member user id's
				$q = $group['system_lookup_query'];
				$user_ids = $db->query($q);
				
				if(count($user_ids) > 0){
					foreach($user_ids as $user){
						if($user['user_id'] == $user_id){
							$response[] = $group['id'];
						}
					}
				}
			}
		}
		
		// get static group membership
		$q = "SELECT `sec_group_id` FROM `sec_group_members` WHERE `user_id` = '".$user_id."'";
		$sec_group_ids = $db->query($q);
		
		if(count($sec_group_ids) > 0){
			foreach($sec_group_ids as $group){
				$response[] = $group['sec_group_id'];
			}
		}
		
		return $response;
	}
	
	public function permission_granted($page_path, $user_id = -1){
		global $db, $session;
		
		$response = array();
		$response['path'] = $page_path;
		$response['permission_level'] = 0;  // 0 = page default, 1 = allow, -1 = deny
		
		// determine if page is an ajax call or not - based on path
		$ajax_page = strpos(" ".$page_path, "ajax");
		
		// get page id from database - inserted by cron indexer
		$q = "SELECT * FROM `sec_pages` WHERE `path` = '$page_path'";
		$page_info = $db->queryOneRow($q);
		
		// use passed userid or currently logged in user
		if($user_id == -1){
			// currently logged in user
			if($session->logged_in){
				$response['user_id'] = $session->userinfo['id'];
				$response['username'] = $session->userinfo['username'];
				$response['user_level'] = $session->userlevel;
			}else{
				// Default Page Action Permissions
				$response['permission_level'] = 0;
			}
		}else{			
			// get passed user info
			$q = "SELECT * FROM `".TBL_USERS."` WHERE `id`='".$user_id."'";
			$user_info = $db->queryOneRow($q);
			
			// see if this user is active
			$q = "SELECT * FROM `".TBL_ACTIVE_USERS."` WHERE `username`='".$user_info['username']."'";
			$active_user = $db->queryOneRow($q);
			if($active_user['username'] == $user_info['username']){
				$response['user_id'] = $user_id;
				$response['username'] = $user_info['username'];
				$response['user_level'] = $user_info['userlevel'];
			}else{
				// Default Page Action Permissions
				$response['permission_level'] = 0;
			}
		}
		
		$q = "SELECT `sc`.`default_security` FROM `sec_categories` AS `sc` JOIN `sec_pages` AS `sp` ON `sc`.`id`=`sp`.`category_id` WHERE `sp`.`path`=%s AND `sp`.`is_deleted`=0";
		$default = $db->queryOneRow($q, array($page_path));
		
		if($default == array()){
			if(stripos($page_path, "/admin") !== false){
				// hard code security to prevent access to non-backoffice users
				$q = "SELECT * FROM `back_office_users` WHERE `user_id` = '".$session->userinfo['id']."'";
				$admin_info = $db->queryOneRow($q);

				if(!isset($admin_info['user_id']) || !isset($session->userinfo['id']) || $admin_info['user_id'] != $session->userinfo['id']){
					$response['permission_granted'] = 0;
					$current_permission = 0;
				}else{
					$response['permission_granted'] = 1;
					$current_permission = 1;
				}
			}elseif(stripos($page_path, "/panel") !== false){
				// hard code security to prevent access to non-cashier users
				$q = "SELECT * FROM `panel_user` WHERE `user_id` = '".$session->userinfo['id']."'";
				$cashier_info = $db->queryOneRow($q);

				if(!isset($cashier_info['user_id']) || !isset($session->userinfo['id']) || $cashier_info['user_id'] != $session->userinfo['id']){
					$response['permission_granted'] = 0;
					$current_permission = 0;
				}else{
					$response['permission_granted'] = 1;
					$current_permission = 1;
				}
			}else{
				$response['permission_granted'] = 1;
				$current_permission = 1;
			}
		}else{
			$current_permission = $default['default_security'];
		}
		
		$groups = array();
		
		//If no security is defined for the page, response['permission_granted'] will already be set.
		//Because we don't have all of the categories set up, we default to allow and don't check
		//security.
		if(!isset($response['permission_granted'])){
			if($response['user_level'] >= 9){
				$response['permission_granted'] = 1;
			}else{
				$q = "SELECT * FROM `sec_categories` AS `sc` JOIN `sec_pages` AS `sp` ON `sc`.`id`=`sp`.`category_id` JOIN `sec_group_permissions` AS `sgp` ON `sgp`.`category_id`=`sc`.`id` JOIN `sec_groups` AS `sg` ON `sg`.`id`=`sgp`.`group_id` JOIN `sec_group_members` AS `sgm` ON `sgm`.`sec_group_id`=`sg`.`id` WHERE `sgm`.`user_id`=%i AND `sp`.`path`=%s AND `sg`.`is_deleted`=0 GROUP BY `sg`.`id`, `sc`.`id`";
				$groups_temp = $db->query($q, array($response["user_id"],$page_path));
				$groups = array_merge($groups, $groups_temp);
				
				// get all dynamic security groups
				$q = "SELECT * FROM `sec_groups` WHERE `is_system` = '1'";
				$sec_groups = $db->query($q); 

				// get dynamic group membership
				foreach($sec_groups as $group){
					if($group['is_system'] == 1){
						// use lookup query to get member user id's
						$q = $group['system_lookup_query'];
						$user_ids = $db->query($q);

						if(count($user_ids) > 0){
							foreach($user_ids as $user){
								if($user['user_id'] == $response["user_id"]){
									$q = "SELECT * FROM `sec_categories` AS `sc` JOIN `sec_pages` AS `sp` ON `sc`.`id`=`sp`.`category_id` JOIN `sec_group_permissions` AS `sgp` ON `sgp`.`category_id`=`sc`.`id` AND `sgp`.`group_id`=%i JOIN `sec_groups` AS `sg` ON `sg`.`id`=%i WHERE `sp`.`path`=%s GROUP BY `sg`.`id`, `sc`.`id`";
									$groups_temp = $db->query($q, array($group['id'], $group['id'], $page_path));
									$groups = array_merge($groups, $groups_temp);
								}
							}
						}
					}
				}
				
				foreach($groups as $group){
					//print_r($group);
					
					//If the current permission status is the same as the default security, continue looking for a group that says otherwise
					if($default['default_security'] == $current_permission){
						if($group['permission_level_id'] == -1){
							$current_permission = -1;
						}elseif($group['permission_level_id'] == 1){
							$current_permission = 1;
						}
					}
				}
				
				//Use the current permission to determine whether or not to grant permission
				$response['permission_granted'] = $current_permission == -1 ? 0 : 1;
			}
		}
		
		//$page_integrity = $this->check_file_hashes($page_path);
		if($page_integrity['approved_count'] != 1){
			$response['permission_granted'] = -2;
		}
		
		/*
		$response['groups'] = $this->user_sec_group_membership($response['user_id']);
		
		// Specific User Level 0-9 Permissions
		if($response['user_level'] < 3){
			// deny override for non-activated users
			$response['permission_level'] = -1;
			$response['message'] = "Your account must be activated to access this page!";
		}elseif(($response['user_level'] >= 3) && ($response['user_level'] <= 8)){
			// Check Security Group Permissions
			if($ajax_page){
				$response['ajax'] = 1;
				
				// Specific Page Action Permissions
				$q = "SELECT * FROM `sec_page_actions` as `pa` JOIN `sec_group_permissions` as `grp_per` ON `pa`.`id` = `grp_per`.`action_id` JOIN `sec_groups` as `grp` ON `grp`.`id` = `grp_per`.`group_id` JOIN `sec_group_members` as `gm` ON `gm`.`sec_group_id` = `grp`.`id` WHERE `pa`.`page_id` = '".$page_info['id']."' AND `gm`.`user_id` = '".$session->userinfo['id']."'";
				$user_page_action_permissions = $db->query($q);
			
				// if no specific actions defined
				if(count($user_page_action_permissions) == 0){
					// Default Page Action Permissions
					$response['permission_level'] = 0;
				}else{
					$response['permission_level'] = 0; // default to 0
					foreach($user_page_action_permissions as $perm){
						// see if action matches what was sent in Ajax request action
						if($_POST['action'] == $user_page_action_permissions['action_name']){
							$response['permission_level'] = $perm['permission_level_id'];
						}
					}
				}
			}else{
				$response['ajax'] = 0;
				
				// Default Page Action Permissions
				$response['permission_level'] = 0;
				
				// Stack group level permissions
				if(count($response['groups']) > 0){
					foreach($response['groups'] as $group_id){
						$q = "SELECT * FROM `sec_page_actions` as `pa` JOIN `sec_group_permissions` as `grp_per` ON `pa`.`id` = `grp_per`.`action_id` WHERE `pa`.`page_id` = '".$page_info['id']."' AND `grp_per`.`group_id` = '".$group_id."'";
						$group_page_perm = $db->queryOneRow($q);

						if(empty($group_page_perm['permission_level_id'])){
							$group_page_perm['permission_level_id'] = 0;
						}
						
						$response['group_permissions'][$group_id] = $group_page_perm['permission_level_id'];
						
						if($group_page_perm['permission_level_id'] == -1){
							$response['permission_level'] = -1;
						}elseif($group_page_perm['permission_level_id'] == 1 && $response['permission_level'] = 0 && $response['permission_level'] != -1){
							$response['permission_level'] = 1;
						}
					}
				}
			}	
		}elseif($response['user_level'] >= 9){
			// allow override for super admins
			$response['permission_level'] = 1;
		}
		
		if($response['permission_level'] == 0){
			// Get Default Page Action Permissions (everyone group)
			$q = "SELECT * FROM `sec_page_actions` as `pa` JOIN `sec_group_permissions` as `grp_per` ON `pa`.`id` = `grp_per`.`action_id` WHERE `pa`.`page_id` = '".$page_info['id']."' AND `grp_per`.`group_id` = '-1'";
			$default_page_action_permissions = $db->queryOneRow($q);
			$response['permission_level'] = $default_page_action_permissions['permission_level_id'];
		}
		
		// if using custom permission levels other than -1, 0, 1, add them here
		
		// determine if access is granted or denied
		if($response['permission_level'] > 0){
			$response['permission_granted'] = 'true';
		}elseif($response['permission_level'] < 0){
			$response['permission_granted'] = 'false';
		}else{
			$response['default_permissions'] = 'true';
			
			// nothing defined and default set to 0
			$response['permission_granted'] = 'true';
		}
		
		*/
		
		return $response;
		
	}
	
	function disable_dormant_accounts(){
		global $db;
		
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		$limit = $now_dt->modify("-6 months");
		$limit_str = $limit->format("Y-m-d H:i:s");
		
		$q = "SELECT `u`.`id`,`u`.`last_login`,`u`.`ip`,`c`.`available_balance` FROM `users` AS `u` LEFT JOIN `customers` AS `c` ON `u`.`id`=`c`.`user_id` WHERE `u`.`last_login` <= %s AND `c`.`is_disabled`=0";
		$dormant_users = $db->query($q, array($limit_str));
		
		foreach($dormant_users as $u){
			$q = "SELECT * FROM customer_transaction WHERE user_id=%i ORDER BY transaction_id DESC LIMIT 1";
			$last_trans = $db->queryOneRow($q, array($u['id']));
			
			$q = "INSERT INTO `dormant_log` (`user_id`,`dormant_start`,`ip_address`,`last_login`,`last_transaction_id`,`last_transaction_date`,`balance`) VALUES (%i, %s, %s, %s, %i, %s, %d)";
			echo $q;
			$db->queryInsert($q, array($u['id'],$now,$u['ip'],$u['last_login'],$last_trans['transaction_id'],$last_trans['transaction_date'],$u['available_balance']));
			print_r( array($u['id'],$now,$u['ip'],$u['last_login'],$last_trans['transaction_id'],$last_trans['transaction_date'],$u['available_balance']));
			
			$q = "UPDATE `customers` SET `is_disabled`='1', `disabled_by`='1' WHERE `user_id`=%i";
			$db->queryDirect($q, array($u['id']));
		}
	}
	/* ------------------  END SECURITY FUNCTIONS  ------------------ */

	/* ------------------  BEGIN LOTTO XML FUNCTIONS  ------------------ */
	
    public function get_lotto_xml(){
		global $db;
		
		// get current list of game ids from database
		$q = "SELECT `id` FROM `xml_lotto_game` ORDER BY `id` ASC";
		$this->xml_lotto_games = $this->flatten_array($db->query($q));
		
		$url = "http://65.18.171.45/lotterydata/ksaconsul.com/kdkloas/lottery.xml";
		
		// get last modified date of remote file
		$h = get_headers($url, 1);
		$last_updated_dt = NULL;
		if(!$h || strpos($h[0], '200') !== false){
			$last_updated_dt = new \DateTime($h['Last-Modified']);
		}
		
		echo "XML Last Updated: ".$last_updated_dt->format("Y-m-d h:m:s A")."<br><br>";
		
		$reader = new XMLReader();
		if (!$reader->open($url)) {
			die("Failed to open: $url");
		}
		
		$curr_stateprov_id = "";
		$curr_game_id = "";
		$curr_game_name = "";
		$curr_drawing_data = array();
		
		while($reader->read()){
			if($reader->nodeType == XMLReader::ELEMENT){
				$node_name = $reader->name;
				
				if($node_name == "StateProv"){
					//Get the stateprove_id from the xml file and set current statprov id
					$curr_stateprov_id = $reader->getAttribute("stateprov_id");
				
					//See if the location already exists in the database
					$db_lotto_data = $db->queryOneRow("SELECT * FROM xml_lotto_location WHERE stateprov_id='".$curr_stateprov_id."'");
					
					if(!isset($db_lotto_data['stateprov_id'])){
						//Add location to the database because it doesn't exist
						echo "Add Location, Doesn't exist<br>";
						
						//Read from attributes to get the information for the new database record
						$stateprove_name = $reader->getAttribute("stateprov_name");
						$stateprove_country = $reader->getAttribute("country");
						$query = "INSERT INTO xml_lotto_location (`stateprov_name`, `stateprov_id`, `stateprov_country`) VALUES ('".$stateprove_name."','".$curr_stateprov_id."','".$stateprove_country."')";
						
						//Run the query
						echo $query."<br>";
						$db->queryInsert($query);
					}
				} elseif($node_name == "game"){
					//Since this tag implies you are moving to a new drawing, run the insert for drawing data.
					//This function needs to run before the new curr game id is set.
					$this->insert_drawing_data($curr_drawing_data, $curr_game_id, $curr_game_name);
					
					//Set the current game_id and make sure it exists in the database
					//Get the stateprove_id from the xml file
					$curr_game_id = $reader->getAttribute("game_id");
					$curr_game_name = $reader->getAttribute("game_name");
					
					// add game to database if not already there
					if(!in_array($curr_game_id, $this->xml_lotto_games)){
						$query = "INSERT INTO xml_lotto_game (`id`, `name`, `description`) VALUES ('".$curr_game_id."', '".$curr_game_name."', NULL)";
						echo $query."<br>";
						$db->queryInsert($query);
					}
					
					//Make sure a link is created for the many to many relationship between games and locations
					$query = "INSERT INTO xml_lotto_location_game_map (`stateprov_id`, `game_id`) VALUES ('".$curr_stateprov_id."','".$curr_game_id."')";
					
					//Run Query
					echo $query."<br>";
					$db->queryInsert($query);
				} else {
					$curr_drawing_data[$node_name] = $reader->readInnerXML();
				}
			}
		}
		$reader->close();
	}
	
	private function insert_drawing_data(&$curr_drawing_data, $curr_game_id, $curr_game_name){
		global $db;
		
		//Make sure the curr_drawing_data is set. The first pass through the game tag will call this function with an
		//empty array.
		if(isset($curr_drawing_data)){
			//Build the query for the drawing to be inserted into the database.
			if(isset($curr_drawing_data['lastdraw_numbers']) && $curr_game_id != ""){
				// convert raw numbers to integer value
				// split on commas for powerball or other such drawings
				$draw_numbers_arr = explode(",", $curr_drawing_data['lastdraw_numbers']);
				$draw_numbers = str_replace("-", "", $draw_numbers_arr[0]);
				// make sure number is valid
				if(intval($draw_numbers) == 0){
					echo "<p style='color:red;'>$draw_numbers is not an integer!</p>";
				}
				
				// update lotto game table with number of digits in winning number
				$num_digits = strlen((string)$draw_numbers);
				$query = "UPDATE xml_lotto_game SET `num_digits`='".$num_digits."' WHERE id='".$curr_game_id."'";
				echo $query."<br>";
				$db->query($query);
				
				// insert winning numbers
				$draw_date = new datetime($curr_drawing_data['lastdraw_date']);
				$draw_date_str = $draw_date->format("Y-m-d");
				$now_dt = new DateTime();
				$draw_datetime_str = $now_dt->format("Y-m-d H:i:s");

				//Check to see if this entry has been overridden
				$q = "SELECT is_override, id FROM xml_lotto_winning_numbers WHERE draw_date='".$draw_date_str."' AND game_id='".$curr_game_id."'";
				$override = $db->queryOneRow($q);
				echo $q."<br>";
				if($override['is_override'] == 1){
					$query = "UPDATE xml_lotto_winning_numbers SET draw_numbers_raw='".$curr_drawing_data['lastdraw_numbers']."', original_number='".$draw_numbers."', game_name='".$curr_game_name."' WHERE draw_date='".$draw_date_str."' AND game_id='".$curr_game_id."'";
					echo $query."<br>";
					$db->queryDirect($query);
					$insert_id = $override['id'];
				}else{
					$query = "INSERT INTO xml_lotto_winning_numbers (`draw_date`, `draw_datetime`, `draw_numbers_raw`, `draw_numbers`, `game_id`, `game_name`) VALUES ('".$draw_date_str."','".$draw_datetime_str."','".$curr_drawing_data['lastdraw_numbers']."', '".$draw_numbers."', '".$curr_game_id."', '".$curr_game_name."')";
					echo $query."<br>";
					$insert_id = $db->queryInsert($query);
				}
			}
			//If there is a nextdraw_date set, update the game information record
			if(isset($curr_drawing_data['nextdraw_date'])){
				$next_draw_date = new datetime($curr_drawing_data['nextdraw_date']);
				$next_draw_date_str = $next_draw_date->format("Y-m-d");
				$query = "UPDATE xml_lotto_winning_numbers SET `next_draw_date`='".$next_draw_date_str."' WHERE game_id='".$curr_game_id."' AND draw_date='".$draw_date_str."'";
				
				//Run Query
				echo $query."<br>";
				$db->query($query);
			}
			
			//If there is a jackpot set, update the game information record
			if(isset($curr_drawing_data['jackpot'])){
				$query = "UPDATE xml_lotto_winning_numbers SET `jackpot`=".$curr_drawing_data['jackpot']." WHERE game_id='".$curr_game_id."' AND draw_date='".$draw_date_str."'";
				
				//Run Query
				echo $query."<br>";
				$db->query($query);
			}
			
			//empty the drawing data from the array
			$curr_drawing_data = array();
		}
	}
	
	/* ------------------  END LOTTO XML FUNCTIONS  ------------------ */
	
	/* ------------------  BEGIN OTHER LOTTO FUNCTIONS  ------------------ */
	
	public function alert_unprocessed_bets(){
		global $db;
		
		$date = new DateTime('now');
		$date_str = $date->format("Y-m-d");
		$q = "SELECT * FROM `lotto_bet` AS `b` JOIN `lotto_ticket` AS `t` ON `t`.`ticket_id`=`b`.`ticket_id` JOIN `lotto_game` AS `g` ON `b`.`game_id`=`g`.`id` WHERE `draw_date` < '".$date_str."' AND `is_processed` = '0'";
		$unprocessed_bets = $db->query($q);
		
		foreach($unprocessed_bets as $bet){
			$this->send_alert_by_group('Lottery Bet Never Processed', 'A lottery bet ('.$bet['ball_string'].' on '.$bet['short_name'].') from ticket number '.$bet['ticket_number'].' was never processed!', "3");
			if($bet['user_id'] != "-9999"){
				$this->send_alert('Lottery Bet Never Processed','A lottery bet ('.$bet['ball_string'].' on '.$bet['short_name'].') from ticket number '.$bet['ticket_number'].' was never processed. Please contact support.', $bet['user_id']);
			}
		}
	}
	
	public function generate_ticket_number(){
		global $db;
		
		$ticket_number = $this->generate_random_integer(0,999999999999999,true);
		$result = $db->queryOneRow("SELECT * FROM `lotto_ticket` WHERE ticket_number='".$ticket_number."'");
		if($result == null){
			return $ticket_number;
		}else{
			$this->generate_ticket_number();
		}
	}
	
	public function call_in_lotto_bet($user_id, $game_id, $pick, $is_boxed, $bet_amount){
		$this->place_lotto_bet($user_id, $game_id, $pick, $is_boxed, $bet_amount, -1, -1, 0, 0);
	}
	
	public function place_lotto_bet($user_id, $game_id, $pick, $is_boxed, $bet_amount, $ticket_source_id=0, $cashier_id=-9999, $location_id=0, $shift_id=0, $ticket_id=null, $advance_play_id=NULL){
		global $db;
		
		if(!$ticket_id){
			// create a new ticket for this bet
			$ticket_id = $this->create_blank_lotto_ticket($ticket_source_id, $cashier_id, $location_id, $user_id, $shift_id);
		}
		
		$ticket_info = $db->queryOneRow("SELECT * FROM `lotto_ticket` WHERE `ticket_id` = '".$ticket_id."'");
		$ticket_number = $ticket_info['ticket_number'];

		//negate the total since it is a withdraw and make the transaction
		$total = $bet_amount * -1;
		
		if($advance_play_id == NULL){
			$detail = "Purchased Lotto Ticket - ".$ticket_number;
		}else{
			$detail = "Purchased Advance Play Lotto Ticket - ".$ticket_number;
		}
		
		$this->make_customer_transaction($total, 3, $detail, $ticket_number, $user_id, $advance_play_id);

		//Get next draw date from the database to store in the bets table
		$draw_date = $db->queryOneRow("SELECT wn.next_draw_date FROM lotto_game_xml_link link JOIN xml_lotto_winning_numbers wn ON link.xml_game_id = wn.game_id WHERE link.game_id=".$game_id." ORDER BY wn.next_draw_date DESC LIMIT 1");

		//Set up the ball string to be put into the database
		$balls = str_split($pick);
		$number_of_balls = count($balls);
		sort($balls, SORT_NUMERIC);
		for($y=0; $y<6; $y++){
			if(!isset($balls[$y])){
				array_push($balls, "");
			}
		}
		
		//Calculate the pay factor. Straight will be the bet amount. Boxed is the bet amount divided by the factorial of the number of balls in the bet
		$pay_factor = $this->get_pay_factor($pick, $bet_amount, $is_boxed);
		
		$db->queryInsert("INSERT INTO `lotto_bet` (`ticket_id`,`game_id`,`ball_1`,`ball_2`,`ball_3`,`ball_4`, `ball_5`, `ball_6`, `ball_string`, `is_boxed`, `bet_amount`, `pay_factor`, `is_processed`, `draw_date`, `payout_scheme_id`) VALUES (".$ticket_id.",".$game_id.",'".$balls[0]."','".$balls[1]."','".$balls[2]."','".$balls[3]."','".$balls[4]."','".$balls[5]."','".$pick."',".$is_boxed.",".$bet_amount.",".$pay_factor.",0,'".$draw_date['next_draw_date']."', 1)");
		
		// update lotto ticket
		$db->query("UPDATE `lotto_ticket` SET `number_of_bets` = '1', `total` = '".$bet_amount."', `tender` = '".$bet_amount."' WHERE `ticket_id` = '".$ticket_id."';");
	
		return $ticket_number;
	}
	
	public function create_blank_lotto_ticket($ticket_source_id, $cashier_id, $location_id, $user_id, $shift_id){
		global $db;
		
		$ticket_number = $this->generate_ticket_number();
		$now_dt = new DateTime();
		$now = $now_dt->format('Y-m-d H:i:s');
		$expiration_date = $this->get_expiration_date();
		
		$q = "INSERT INTO `lotto_ticket` (
			`ticket_id`, `ticket_number`, `ticket_source_id`, `purchase_date`, `number_of_bets`, `cashier_id`, `location_id`, `shift_id`, `user_id`, `total`, `tender`, `change`, `commission_rate`, `commission_amount`, `uses_free_bet`, `free_bet_amount`, `is_voided`, `voided_by`, `void_approved_by`, `voided_on`, `ip_address`, `expiration_date`
		) VALUES (
			NULL, '".$ticket_number."', '".$ticket_source_id."', '".$now."', '0', '".$cashier_id."', '".$location_id."', '".$shift_id."', '".$user_id."', '0', '0', '0', NULL, '0', '0', '0', '0', NULL, '0', NULL, '0', '".$expiration_date."'
		);";
		$new_ticket_id = $db->queryInsert($q);
		return $new_ticket_id;
	}

	public function get_partial_payouts($ticket_number){
		global $db;

		$return = array();

		$q = "SELECT * FROM `panel_user_transaction` put JOIN `users` u ON put.made_by=u.id WHERE put.ticket_number=".$ticket_number." AND put.type_id=12";
		$data = $db->query($q);

		foreach($data as $dat){
			$dat["type"] = "Panel";
			array_push($return, $dat);
		}

		$q = "SELECT * FROM `customer_transaction` WHERE ticket_number=".$ticket_number." AND transaction_type_id=4";
		$data = $db->query($q);

		foreach($data as $dat){
			$dat["type"] = "Web";
			array_push($return, $dat);
		}

		return $return;
	}

	public function get_winning_bets($ticket_number = ""){
		global $db;

		if($ticket_number == ""){
			$data = $db->query("SELECT * FROM `lotto_ticket` t JOIN `lotto_bet` b ON t.ticket_id=b.ticket_id JOIN `winners` w ON b.bet_id=w.bet_id WHERE t.user_id=".$session->userinfo["id"]);
		}else{
			$data = $db->query("SELECT * FROM `lotto_ticket` t JOIN `lotto_bet` b ON t.ticket_id=b.ticket_id JOIN `winners` w ON b.bet_id=w.bet_id JOIN `lotto_game` g ON g.id=b.game_id WHERE t.ticket_number=".$ticket_number);
		}
		return $data;
	}

	public function check_lotto_winner(){
		global $db;
		
		//Pull everyone from the database that has a bet placed for that particular game, on that day.
		$q = "SELECT *, bet.game_id as bet_game_id FROM lotto_ticket ticket JOIN lotto_bet bet ON bet.ticket_id=ticket.ticket_id JOIN lotto_game game ON bet.game_id=game.id JOIN lotto_game_xml_link link ON bet.game_id=link.game_id JOIN xml_lotto_winning_numbers winning ON link.xml_game_id=winning.game_id WHERE winning.draw_date=bet.draw_date AND bet.is_processed=0 AND is_voided=0";
		$winning_bets = $db->query($q);
		
		foreach($winning_bets as $bet){
			$valid = true;

			//If it is an override, make sure that the game has closed before processing it
			if($bet['is_override'] == "1"){
				$q = "SELECT web_cutoff_time FROM lotto_game g JOIN lotto_house h ON g.house_id=h.id JOIN lotto_game_xml_link link ON link.game_id=g.id WHERE link.xml_game_id='".$bet['game_id']."' AND g.id='".$bet['bet_game_id']."'";
				echo $q."<br>";
				$cutoff = $db->queryOneRow($q);
				$cutoff_time = new DateTime($bet['draw_date']." ".$cutoff['web_cutoff_time']);
				$now = new DateTime('now');
				if($cutoff_time > $now){
					continue;
				}
			}

			if($bet['ball_3']=="" && strlen($bet['draw_numbers']) != 2){
				//This is a two ball game. The XML winning draw numbers need to substring for the actual winning number
				$winning_balls = substr($bet['draw_numbers'], 1);
			}else{
				$winning_balls = $bet['draw_numbers'];
			}

			if(strlen($winning_balls) != strlen(trim($bet['ball_string']))){
				//Invalid number of balls are in the database for the bet or xml
				$valid = false;
			}

			if(!is_numeric($winning_balls)){
				//xml ball data has invalid characters
				$valid = false;
			}

			if(!is_numeric(trim($bet['ball_string']))){
				//bet ball data has invalid characters
				$valid = false;
			}


			if($bet['is_boxed']==0){
				//Check for a straight bet win
				if($winning_balls == trim($bet['ball_string'])){
					//WINNER!!
					$winning = true;
				}else{
					$winning = false;
				}
			}else{
				//Start checking boxed winnings.
				//Put the winning numbers into an array and sort them.
				$split_winning_balls = str_split($winning_balls);
				sort($split_winning_balls, SORT_NUMERIC);
				
				//Initialize a variable to check if the player won.
				$winning = true;
				//Now that the winning balls are sorted in order and so are the ball fields in the database,
				//we just have to check if all of the balls match.
				for($y=0; $y<count($split_winning_balls); $y++){
					if($split_winning_balls[$y]!=$bet['ball_'.($y+1)]){
						$winning=false;
					}
				}
			}

			if($winning && $valid){
				//Add winning amount to the users account.
				//Create DateTime object of now
				$date = new datetime('now');
				$date_str = $date->format("Y-m-d H:i:s");

				//Calculate the total
				$q = "SELECT payout_rate FROM payout_scheme_detail WHERE scheme_id=".$bet["payout_scheme_id"]." AND ball_count=".strlen($winning_balls);
				$payout = $db->queryOneRow($q);
				$total = $payout['payout_rate']*$bet['pay_factor'];

				//Initialize the payout status id
				$paid_status = 2;

				//Only make a customer transaction if the bet was made online.
				if($bet['user_id'] != "-9999"){
					if($bet['is_boxed'] == 1){
						$boxed = "Boxed";
					}else{
						$boxed = "Straight";
					}
					$textString_dc = "Lottery: ".$boxed." ".$bet['ball_string']." hit on ".$bet['name'];
					$this->make_customer_transaction($total, 4, $textString_dc, $bet['ticket_number'], $bet['user_id'], null, $bet['user_session_id']);
					$this->send_alert("Lotto Winner!", "You have won ".number_format($total)." CSC. Your ".$boxed." ".$bet['ball_string']." hit on ".$bet['name'].".", $bet['user_id']);
					$paid_status = 1;
				}

				$ticket_payout = $db->queryOneRow("SELECT * FROM `winner_payout` WHERE `ticket_number`='".$bet['ticket_number']."'");

				if($ticket_payout==NULL){
					//No bets won on the ticket to date so add a new payout record.
					$q = "INSERT INTO `winner_payout` (`ticket_number`, `total_payout`, `status_id`, `num_of_wins`, `created_on`, `last_updated`) VALUES ('".$bet['ticket_number']."', '".$total."','".$paid_status."','1','".$date_str."','".$date_str."')";
						echo $q."<br>";
					$db->query($q);
				}else{
					//A previous bet hit, update the record to include the new bet
					if($paid_status == 2 && $ticket_payout['status_id'] == 1){
						//This means the ticket is panel cashier driven and was paid so it is only partially paid now
						$paid_status = 3;
					}
					$num_of_wins = $ticket_payout['num_of_wins']+1;
					$total_payout = $ticket_payout['total_payout']+$total;
					$q = "UPDATE `winner_payout` SET `status_id`='".$paid_status."',`total_payout`='".$total_payout."', `num_of_wins`='".$num_of_wins."', `last_updated`='".$date_str."' WHERE id=".$ticket_payout['id'];
					echo $q."<br>";
					$db->query($q);
				}

				//Create a winning bet record
				$q = "INSERT INTO `winners` (`bet_id`,`ticket_number`,`win_date`,`winning_amount`,`is_confirmed`) VALUES ('".$bet['bet_id']."', '".$bet['ticket_number']."', '".$date_str."','".$total."',0)";
				echo $q."<br>";
				$db->queryInsert($q);

				$q = "UPDATE `winner_payout` SET `status_id`='".$paid_status."',`total_payout`='".$total_payout."', `num_of_wins`='".$num_of_wins."', `last_updated`='".$date_str."' WHERE id=".$ticket_payout['id'];
				echo $q."<br>";
				$db->query($q);
			}

			if($valid){
				$this->mark_bet_processed($bet['bet_id'], $winning, $winning_balls);
			}else{
				echo '/'.trim($bet['ball_string']).'/';
			}
		}
	}

	public function get_expiration_date($date = NULL){
		global $db;

		if($date==NULL){
			$date = date("Y-m-d H:i:s", time());
		}

		$q = "SELECT value FROM `settings` WHERE `setting`='ticket_expiration'";
		$ticket_expiration_days = $db->queryOneRow($q);

		return date("Y-m-d H:i:s",strtotime("+".$ticket_expiration_days['value']." days", time($date)));
	}

	public function is_ticket_expired($ticket_number){
		global $db;

		$q = "SELECT expiration_date FROM `lotto_ticket` WHERE ticket_number='".$ticket_number."'";
		$expiration_date = $db->queryOneRow($q);

		if($expiration_date['expiration_date']<date("Y-m-d H:i:s", time())){
			$data = $this->get_partial_payouts($ticket_number);
			if($data==NULL){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function check_unprocessed_tickets(){
		global $db;

		//Get today
		$today = new datetime('now');
		$today_str = $today->format("Y-m-d");

		$data = $db->query("SELECT * FROM lotto_bet bet JOIN lotto_game_xml_link link ON bet.game_id=link.game_id JOIN xml_lotto_winning_numbers winning ON link.xml_game_id=winning.game_id WHERE bet.draw_date<'".$today_str."' AND bet.is_processed=0");
		
		foreach($data as $unprocessed){
			$this->check_lotto_winner($unprocessed['draw_date'], $unprocessed['xml_game_id']);
		}
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}
	
	public function generate_lucky_numbers(){
		global $db;
		
		$two_ball = $this->generate_random_integer(0, 99);
		$three_ball = $this->generate_random_integer(0, 999);
		$four_ball = $this->generate_random_integer(0, 9999);
		$five_ball = $this->generate_random_integer(0, 99999);
		
		$today_dt = new dateTime();
		$today_date = $today_dt->format('Y-m-d');
		
		// store in database
		$q = "UPDATE `settings` SET `value` = '".$two_ball."' WHERE `setting`='lucky_number_2_ball'";
		echo $q."<br>";
		$db->query($q);
		$q = "UPDATE `settings` SET `value` = '".$three_ball."' WHERE `setting`='lucky_number_3_ball'";
		echo $q."<br>";
		$db->query($q);
		$q = "UPDATE `settings` SET `value` = '".$four_ball."' WHERE `setting`='lucky_number_4_ball'";
		echo $q."<br>";
		$db->query($q);
		$q = "UPDATE `settings` SET `value` = '".$five_ball."' WHERE `setting`='lucky_number_5_ball'";
		echo $q."<br>";
		$db->query($q);
		$q = "UPDATE `settings` SET `value` = '".$today_date."' WHERE `setting`='lucky_number_last_updated'";
		echo $q."<br>";
		$db->query($q);
	}
	
	public function get_bet_limits(){
		global $db;
		
		$q = "SELECT * FROM `lotto_bet_limits`";
		$query_limits = $db->query($q);
		
		foreach($query_limits as $query_limit){
			$limits[$query_limit['ball_count']] = $query_limit['limit'];
		}
		
		return $limits;
	}
	
	public function bet_running_total($game_id, $ball_string){
		global $db;
		
		$q = "SELECT SUM(`pay_factor`) AS `total_amount` FROM `lotto_bet` WHERE game_id='".$game_id."' AND ball_string='".$ball_string."' AND is_processed=0 GROUP BY game_id";
		$total = $db->queryOneRow($q);
		
		return $total['total_amount'];
	}
	
	public function archive_lotto_results(){
		global $db;
		
		// get all results that haven't been archived yet
		$q = "SELECT xlwn.id AS xlwn_id, xlwn.draw_date AS draw_date, lh.id AS house_id, lg.id as game_id, lg.number_of_balls AS number_of_balls, xlwn.draw_numbers AS draw_numbers FROM `xml_lotto_winning_numbers` AS `xlwn` INNER JOIN `lotto_game_xml_link` AS `lgxl` ON `lgxl`.`xml_game_id`=`xlwn`.`game_id` INNER JOIN `lotto_game` AS `lg` ON `lgxl`.`game_id`=`lg`.`id` INNER JOIN `lotto_house` AS `lh` ON `lg`.`house_id`=`lh`.`id` WHERE xlwn.is_archived = '0'";
		$lotto_results = $db->query($q);
		
		echo "<br><br>== ARCHIVE LOTTO RESULTS ==<br><br>";
		
		foreach($lotto_results as $result){
			// see if an entry for this house/date exists already and get id
			$q = "SELECT id FROM `cron_lotto_results` WHERE `house_id` = '".$result['house_id']."' AND `draw_date` = '".$result['draw_date']."'";
			$result_info = $db->queryOneRow($q);
			$result_id = $result_info['id'];
			
			if($result_id < 1){
				// attempt to create new entry for this date/house
				$q = "INSERT INTO `cron_lotto_results` (`id`, `draw_date`, `house_id`, `ball_2_draw`, `ball_3_draw`, `ball_4_draw`, `ball_5_draw`, `ball_6_draw`) VALUES (NULL, '".$result['draw_date']."', '".$result['house_id']."', NULL, NULL, NULL, NULL, NULL);";
				$result_id = $db->queryInsert($q);
			}
			
			// update entry in db with each ball amount
			if($result['number_of_balls'] == 2){
				$q = "UPDATE `cron_lotto_results` SET `ball_2_draw` = '".substr($result['draw_numbers'], -2)."' WHERE `id` = '".$result_id."'";
			}elseif($result['number_of_balls'] == 3){
				$q = "UPDATE `cron_lotto_results` SET `ball_3_draw` = '".$result['draw_numbers']."' WHERE `id` = '".$result_id."'";
			}elseif($result['number_of_balls'] == 4){
				$q = "UPDATE `cron_lotto_results` SET `ball_4_draw` = '".$result['draw_numbers']."' WHERE `id` = '".$result_id."'";
			}elseif($result['number_of_balls'] == 5){
				$q = "UPDATE `cron_lotto_results` SET `ball_5_draw` = '".$result['draw_numbers']."' WHERE `id` = '".$result_id."'";
			}elseif($result['number_of_balls'] == 6){
				$q = "UPDATE `cron_lotto_results` SET `ball_6_draw` = '".$result['draw_numbers']."' WHERE `id` = '".$result_id."'";
			}
			echo "$q <br>";
			$db->query($q);
			
			// set archived bit
			$q = "UPDATE `xml_lotto_winning_numbers` SET `is_archived` = '1' WHERE `id` = '".$result['xlwn_id']."'";
			$db->query($q);
		}
	}
	
	public function advance_play_cron_handler(){
		global $db;
		
		$q = "SELECT id FROM `scheduled_advance_play` WHERE `bets_remaining` > 0";
		$advance_play_entries = $db->query($q);
		
		foreach($advance_play_entries as $ape){
			$this->create_scheduled_ticket($ape['id']);
		}
	}
	
	public function create_scheduled_ticket($advance_play_id){
		global $db;
		
		//Check to see if the advanced play exists
		$q = "SELECT * FROM `scheduled_advance_play` AS `sap` JOIN `lotto_bet_favorites` AS `lbf` ON `sap`.`fav_bet_id`=`lbf`.`id` WHERE `sap`.`id`=%i";
		$scheduled_play = $db->queryOneRow($q, array($advance_play_id));
		
		if($scheduled_play == NULL){
			return -1;
		}
		
		$q = "SELECT *, `sap`.`draw_date` AS `advance_draw_date` FROM `scheduled_advance_play` AS `sap` JOIN `advance_play_tickets` AS `apt` ON `sap`.`id`=`apt`.`advance_play_id` WHERE `sap`.`id`=%i ORDER BY `apt`.`draw_date` DESC";
		$tickets = $db->query($q, array($advance_play_id));
		
		//If bets remaining is 0 return and dont make a new ticket
		if($tickets[0]['bets_remaining'] === 0){
			return -1;
		}
		
		if($tickets == NULL){
			//Create the first ticket
			$ticket_number = $this->place_lotto_bet($scheduled_play['user_id'],$scheduled_play['game_id'],$scheduled_play['pick'],$scheduled_play['is_boxed'],$scheduled_play['bet_amount'],0,-9999,0,0,null,$advance_play_id);
			
			//Get new ticket's information
			$q = "SELECT * FROM `lotto_ticket` AS `lt` JOIN `lotto_bet` AS `lb` ON `lt`.`ticket_id`=`lb`.`ticket_id` WHERE `lt`.`ticket_number`=%i";
			$new_bet = $db->queryOneRow($q, array($ticket_number));
			
			//Put a record in the advance play tickets table
			$q = "INSERT INTO `advance_play_tickets` (`advance_play_id`,`ticket_number`,`draw_date`) VALUES (%i,%i,%s)";
			$db->queryInsert($q, array($advance_play_id, $ticket_number, $new_bet['draw_date']));
			
			//Finally update the scheduled advance play table
			$new_bets_remaining = $scheduled_play['bets_remaining'] - 1;
			$q = "UPDATE `scheduled_advance_play` SET `bets_remaining`=%i, `draw_date`=%s WHERE `id`=%i";
			$db->queryDirect($q, array($new_bets_remaining, $new_bet['draw_date'], $advance_play_id));
			
			return $ticket_number;
		}else{
			$last_draw_date = new DateTime($tickets[0]['advance_draw_date']);
			$now = new DateTime(date("Y-m-d"));
				echo $tickets[0]['advance_draw_date'];
				echo "<BR>".$now->format("Y-m-d H:i:s");
			
			if($last_draw_date < $now){
				//Create a ticket
				$ticket_number = $this->place_lotto_bet($scheduled_play['user_id'],$scheduled_play['game_id'],$scheduled_play['pick'],$scheduled_play['is_boxed'],$scheduled_play['bet_amount'],0,-9999,0,0,null,$advance_play_id);
				
				//Get new ticket's information
				$q = "SELECT * FROM `lotto_ticket` AS `lt` JOIN `lotto_bet` AS `lb` ON `lt`.`ticket_id`=`lb`.`ticket_id` WHERE `lt`.`ticket_number`=%i";
				$new_bet = $db->queryOneRow($q, array($ticket_number));
				
				//Put a record in the advance play tickets table
				$q = "INSERT INTO `advance_play_tickets` (`advance_play_id`,`ticket_number`,`draw_date`) VALUES (%i,%i,%i)";
				$db->queryInsert($q, array($advance_play_id, $ticket_number, $new_bet['draw_date']));
				
				//Finally update the scheduled advance play table
				$new_bets_remaining = $scheduled_play['bets_remaining'] - 1;
				$q = "UPDATE `scheduled_advance_play` SET `bets_remaining`=%i, `draw_date`=%s WHERE `id`=%i";
				$db->queryDirect($q, array($new_bets_remaining, $new_bet['draw_date'], $advance_play_id));
				
				return $ticket_number;
			}else{
				//The scheduled bet is not ready for another ticket
				return -1;
			}
		}
	}
	/* ------------------  END OTHER LOTTO FUNCTIONS  ------------------ */

	/* ------------------  BEGIN BALANCE FUNCTIONS  ------------------ */
	public function available_daily_withdrawal($user_id = NULL, $date = NULL){
		global $db;
		global $session;

		if($user_id==NULL){
			$user_id = $session->userinfo['id'];
		}

		if($date==NULL){
			$date = date("Y-m-d",time());
		}

		$q = "SELECT SUM(amount) AS amount FROM `customer_transaction` WHERE transaction_type_id='5' AND CAST(transaction_date AS DATE)='".$date."' AND user_id='".$user_id."' GROUP BY transaction_type_id";
		$results = $db->queryOneRow($q);

		$q = "SELECT value FROM `settings` WHERE setting='max_withdraw_per_day'";
		$max_withdraw = $db->queryOneRow($q);

		if($results==NULL){
			return $max_withdraw['value'];
		}else{
			return $max_withdraw['value'] - abs($results['amount']);
		}
	}

	public function check_balance($table = "customers", $user_id = NULL){
		global $db;
		global $session;

		if($user_id == NULL){
			$user_id = $session->userinfo['id'];
		}
		
		//Get the balances from the customer table of the database
		if($table == "customers"){
			$balances = $db->queryOneRow("SELECT available_balance, bonus_balance FROM customers WHERE user_id=".$user_id);
		}else{
			$balances = $db->queryOneRow("SELECT balance FROM panel_user WHERE user_id=".$user_id);
		}
		return $balances;
	}
	
	public function mark_bet_processed($bet_id, $is_won = false, $winning_draw = ""){
		global $db;
		
		$date_time = new datetime('now');
		$date_time_str = $date_time->format("Y-m-d H:i:s");

		if($is_won){
			$db->query("UPDATE `lotto_bet` SET `is_processed`=1, `is_winner`=1, `processed_date_time`='".$date_time_str."', `winning_draw`='".$winning_draw."' WHERE `bet_id`=".$bet_id);
		}else{
			$db->query("UPDATE `lotto_bet` SET `is_processed`=1, `is_winner`=0, `processed_date_time`='".$date_time_str."', `winning_draw`='".$winning_draw."' WHERE `bet_id`=".$bet_id);
		}
	}

	public function cashier_withdrawal_validation($total, $user_id){
		$errors = "";

		$cashier_balance = $this->check_balance("panel_user");
		$available_balance = $cashier_balance['balance']+$total;
		if($available_balance < 0){
			$errors .= "Cashier doesn't have the funds<br>";
		}

		$customer_balance = $this->check_balance("customers", $user_id);
		$available_balance = $customer_balance['available_balance']+$total-$customer_balance['bonus_balance'];
		if($available_balance < 0){
			$errors .= "Customer doesn't have the funds<br>";
		}
		
		$withdrawal_limit = $this->remaining_withdrawal_limit($user_id);
		//Check to see if this is a withdrawal, if so check daily withdrawal limit
		if(($withdrawal_limit + $total < 0) && $withdrawal_limit != -1){
			return "The transaction could not be completed because it would exceed the users daily withdrawal limit. They can only withdrawal $".$withdrawal_limit." more.";
		}

		$money_allowed = $this->available_daily_withdrawal($user_id);
		if(abs($total)>$money_allowed){
			$errors .= "The total exceeds the daily withdraw limits. They can only withdraw $".$money_allowed." more today.<br>";
		}

		if($errors == ""){
			return true;
		}else{
			return $errors;
		}
	}

	public function make_panel_user_transaction($total, $type, $details, $ticket_number = "NULL",$customer_id = "-9999", $shift_id = "NULL"){
		global $db;
		global $session;

		if($shift_id=="NULL"){
			if($session->userinfo['panel_user']['shift_id'] == "-1"){
				return false;
			}else{
				$shift_id = $session->userinfo['panel_user']['shift_id'];
				$location_id = $session->userinfo['panel_user']['location_id'];
			}
		}else{
			$q = "SELECT location_id, user_id FROM panel_user WHERE shift_id=%i";
			$loc = $db->queryOneRow($q, array($shift_id));
			$location_id = $loc['location_id'];
		}
		
		//Get cashier balances
		if($loc == NULL){
			$balances = $this->check_balance("panel_user");
		}else{
			$balances = $this->check_balance("panel_user", $loc['user_id']);
		}
		$new_available_balance = $balances['balance']+$total;

                if($type == 7){
                    $i_virtual_balance = $this->get_virtual_balance($shift_id,$loc['user_id']);
		    $new_available_balance = $balances['balance']+$total;
                    $i_new_virtual_balance = $i_virtual_balance['virtual_money'] - $total; 
                }

		//Create DateTime object of now
		$date = new datetime('now');
		$date_str = $date->format("Y-m-d H:i:s");
		
		// Get customer info
		$q = "SELECT * FROM `users` AS `u` LEFT JOIN `customers` AS `c` ON `u`.`id` = `c`.`user_id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `c`.`user_id` WHERE `u`.`id`=%i";
		$cus_info = $db->queryOneRow($q, array($customer_id));

		//Check if this is a withdrawal or a deposit transaction
		if($total<0){
			$neg_total = -$total;
			if($neg_total>$balances['balance']){
				//The total exceeds the amount that the user has available
				return false;
			}else{
				// Log large transfers/wagers
				if($neg_total > LARGE_TRANSFER_THRESHOLD){
					// determine transcation type
					if($type == 10){
						// Withdrawal
						$this->log_sigificant_event('Large Transfer', 'Withdrawal', $session->userinfo['firstname'].' '.$session->userinfo['lastname'].' ('.$session->userinfo['id'].') made a Withdrawal in the amount of $'.$neg_total.' for customer: '.$cus_info['firstname'].' '.$cus_info['lastname'].' ('.$customer_id.').');
					}else{
						// Lottery Bet
						$this->log_sigificant_event('Large Wager', 'Lottery Bet', $session->userinfo['firstname'].' '.$session->userinfo['lastname'].' ('.$session->userinfo['id'].') made a Lottery Bet in the amount of $'.$neg_total.' for customer: '.$cus_info['firstname'].' '.$cus_info['lastname'].' ('.$customer_id.').');
					}
				}
				
				//This is a withdrawal
				$new_balance = $balances['balance'] + $total;
				$db->queryDirect("UPDATE panel_user SET balance=".$new_balance." WHERE shift_id=".$shift_id);
				$db->queryDirect("UPDATE panel_location SET balance=".$new_balance." WHERE shift_id=".$shift_id);
				$trans_id = $db->queryInsert("INSERT INTO panel_user_transaction (`type_id`,`made_by`,`location_id`,`shift_id`,`customer_id`,`transaction_details`, `amount`, `transaction_date`, `ticket_number`, `balance`, `initial_balance`) VALUES (".$type.",".$session->userinfo['id'].",".$location_id.",".$shift_id.",".$customer_id.",'".$details."', ".$total.", '".$date_str."', ".$ticket_number.", '".$new_balance."', '".$balances['balance']."')");
				return $trans_id;
			}
		}elseif($total>0){
			// Log large transfers/wagers
			if($total > LARGE_TRANSFER_THRESHOLD){
				// determine transcation type
				if($type == 9){
					// Deposit
					$this->log_sigificant_event('Large Transfer', 'Deposit', $session->userinfo['firstname'].' '.$session->userinfo['lastname'].' ('.$session->userinfo['id'].') made a Deposit in the amount of $'.$total.' for customer: '.$cus_info['firstname'].' '.$cus_info['lastname'].' ('.$customer_id.').');
				}
			}
			if($total > LARGE_WIN_THRESHOLD){
				if($type == 12){
					// Lottery Win Payout
					$this->log_sigificant_event('Large Win', 'Lottery Win - Panel Payout', $session->userinfo['firstname'].' '.$session->userinfo['lastname'].' ('.$session->userinfo['id'].') made a Lottery Payout in the amount of $'.$total.' for customer: '.$cus_info['firstname'].' '.$cus_info['lastname'].' ('.$customer_id.').');
				}
			}
			
			//This is a deposit
			$new_balance = $balances['balance'] + $total;
			$db->queryDirect("UPDATE panel_user SET balance=".$new_balance." WHERE shift_id=".$shift_id);
                        if($type == 7){
                             $db->queryDirect("UPDATE panel_user_shift SET virtual_money =".$i_new_virtual_balance." WHERE id=".$shift_id);
                        }
			$trans_id = $db->queryInsert("INSERT INTO panel_user_transaction (`type_id`,`made_by`,`location_id`,`shift_id`,`customer_id`,`transaction_details`, `amount`, `transaction_date`, `ticket_number`, `balance`, `initial_balance`) VALUES (".$type.",".$session->userinfo['id'].",".$location_id.",".$shift_id.",".$customer_id.",'".$details."', ".$total.", '".$date_str."', ".$ticket_number.", '".$new_balance."', '".$balances['balance']."')");
			return $trans_id;
		}else{
			//Cant happen, error out
			return false;
		}

		return false;
	}
	
	public function make_customer_transaction($total, $type, $details, $ticket_number = "NULL", $user_id = NULL, $advance_play_id = NULL, $session_id = null){
		global $db;
		global $session;

		if($session_id == null){
			$session_id = $_SESSION['user_session_id'];
			if(empty($session_id)){
				$q = "SELECT `id` FROM `user_sessions` WHERE `user_id` = '".$user_id."' ORDER BY `start` DESC LIMIT 1";
				$sess = $db->queryOneRow($q);
				$session_id = $sess['id'];
			}
		}
		
		if($user_id == NULL){
			$user_id = $session->userinfo['id'];
			$made_by = "";
		}else{
			if(PHP_SAPI != 'cli'){
				$made_by = "";
			}else{
				$made_by = $session->userinfo['id'];
			}
			// lookup user's session id
			$q = "SELECT `id` FROM `user_sessions` WHERE `user_id` = '".$user_id."' ORDER BY `start` DESC LIMIT 1";
			$sess = $db->queryOneRow($q);
		}
		
		//Get user balances
		$balances = $this->check_balance("customers", $user_id);
		if($advance_play_id == NULL){
			$new_available_balance = $balances['available_balance']+$total;
		}else{
			//Advance play should not take from the customers available balance because they paid in advance
			//You must deduct from the placeholder transaction before adding the new one so the accounting on
			//the account makes sense.
			//
			//NOTE: We have to do it this way so each transaction created for a new scheduled bet is linked
			//to that specific ticket. This will allow all reporting to act normal for nets for lotto in the
			//admin panel
			
			$new_available_balance = $balances['available_balance'];
			
			//Get advance play information
			$q = "SELECT * FROM `scheduled_advance_play` WHERE `id`=%i";
			$advance_play = $db->queryOneRow($q, array($advance_play_id));
			
			//Get placeholder transaction amount information
			$q = "SELECT amount FROM `customer_transaction` WHERE `transaction_id`=%i";
			$amount = $db->queryOneRow($q, array($advance_play['transaction_id']));
			
			//Calculate new placeholder amount
			$new_amount = $amount['amount']+$advance_play['bet_amount'];
			
			//Update the placeholder to correct the difference in amounts when adding the new transaction.
			$q = "UPDATE `customer_transaction` SET `amount`=%f WHERE `transaction_id`=%i";
			$db->queryDirect($q, array($new_amount, $advance_play['transaction_id']));
		}

		//Create DateTime object of now
		$date = new datetime('now');
		$date_str = $date->format("Y-m-d H:i:s");
		
		// Get customer info
		$q = "SELECT * FROM `users` AS `u` LEFT JOIN `customers` AS `c` ON `u`.`id` = `c`.`user_id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `c`.`user_id` WHERE `u`.`id`=%i";
		$cus_info = $db->queryOneRow($q, array($user_id));

		//Check if this is a withdrawal or a deposit transaction
		if($total<0){
			//This is a withdrawal
			$neg_total = -$total;
			if($neg_total>$balances['available_balance']){
				//The total exceeds the amount that the user has available
				return "The user doesn't have enough money to withdrawal!";
			}else{
				if($type == 5){
					//This is a customer withdrawal. No bonus is allow to be withdrawn.
					$bonus_used = 0;
					$new_bonus_balance = $balances['bonus_balance'];
				}else{
					if($balances['bonus_balance']+$total==$total){
						//There is no bonus balance to remove
						$bonus_used = 0;
						$new_bonus_balance = 0;
					}elseif($balances['bonus_balance']+$total>=0){
						//Bonus balance is higher than or equal to total
						$bonus_used = $total;
						$new_bonus_balance = $balances['bonus_balance']+$total;
					}else{
						//Bonus is not 0 but will not cover the full amount
						$bonus_used = $balances['bonus_balance'];
						$new_bonus_balance = 0;
					}
				}
				
				// Self Limits
				
				//Check to see if amount is larger than self transaction limit
				if($neg_total > $cus_info['transaction_limit'] && $type != 5 && $cus_info['transaction_limit'] != -1 ){
					return "The transaction could not be completed because it is larger than the transaction limit you have set. You can only bet up to $".$cus_info['transaction_limit'].".";
				}
				
				//Check to see if their net for today exceeds their allowed daily loss limit
				$remaining_loss = $this->remaining_loss_limit($user_id);
				if(($remaining_loss - $neg_total < 0) && $remaining_loss != -1 && $type != 5  && $type != 125 && $type != 126){
					return "The transaction could not be completed because it would exceed your daily loss limit. You can only bet $".$remaining_loss." more.";
				}
				
				//Check to see if their net for today exceeds their allowed daily wager limit
				$remaining_wager = $this->remaining_wager_limit($user_id);
				if(($remaining_wager - $neg_total < 0) && $remaining_wager != -1 && $type != 5 && $type != 125 && $type != 126){
					return "The transaction could not be completed because it would exceed your daily wager limit. You can only bet $".$remaining_wager." more.";
				}
				
				//Check to see if this is a withdrawal, if so check daily withdrawal limit
				$remaining_withdrawal = $this->remaining_withdrawal_limit($user_id);
				if(($remaining_withdrawal - $neg_total < 0) && $remaining_withdrawal != -1  && $type != 125 && $type != 126){
					return "The transaction could not be completed because it would exceed your daily withdrawal limit. You can only withdrawal $".$remaining_withdrawal." more.";
				}
				
				// Log large transfers/wagers
				if($total > LARGE_TRANSFER_THRESHOLD){
					// determine transcation type
					if($type == 5){
						// Withdrawal
						$this->log_sigificant_event('Large Transfer', 'Withdrawal', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') made a Withdrawal in the amount of $'.$total.'.');
					}elseif($type == 3){
						// Lottery Bet
						$this->log_sigificant_event('Large Wager', 'Lottery Bet', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') made a Lottery bet in the amount of $'.$total.'.');
					}elseif($type == 1){
						// Casino Bet
						$this->log_sigificant_event('Large Wager', 'Casino Bet', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') made a Casino bet in the amount of $'.$total.'.');
					}elseif($type == 17){
						// Sports Bet
						$this->log_sigificant_event('Large Wager', 'Sports Bet', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') made a Sports bet in the amount of $'.$total.'.');
					}elseif($type == 46){
						// Rapidballs Bet
						$this->log_sigificant_event('Large Wager', 'RapidBalls Bet', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') made a RapidBalls bet in the amount of $'.$total.'.');
					}
				}
				
				$db->queryDirect("UPDATE customers SET available_balance=".$new_available_balance." WHERE user_id=".$user_id);
				$db->queryDirect("UPDATE customers SET bonus_balance=".$new_bonus_balance." WHERE user_id=".$user_id);
				
				$transaction_id = $db->queryInsert("INSERT INTO customer_transaction (`transaction_type_id`,`user_id`, `user_session_id`, `made_by`,`transaction_details`,`amount`,`transaction_date`,`ip_address`,`bonus_used`, `ticket_number`, `balance`, `initial_balance`) VALUES (".$type.",".$user_id.",'".$session_id."','".$made_by."','".$details."',".$total.",'".$date_str."','".$_SERVER['REMOTE_ADDR']."',".$bonus_used.", ".$ticket_number.", '".$new_available_balance."', '".$balances['available_balance']."')");
				
				//Get current number of loyalty points
				$loyalty_points = $db->queryOneRow("SELECT loyalty_points FROM customers WHERE user_id='".$user_id."'");
				
				//Determine Loyalty Points
				if($type == 3){
					//Lottery Bet
					$setting = $db->queryOneRow("SELECT value FROM settings WHERE setting='loyalty_points_lottery'");
					$points_awarded = $neg_total * $setting['value'];
					$running_points = $points_awarded + $loyalty_points['loyalty_points'];
					$db->queryDirect("UPDATE `customers` SET `loyalty_points` = '".$running_points."' WHERE `user_id` = '".$user_id."'");
					$db->queryInsert("INSERT INTO loyalty_points (`user_id`, `transaction_id`, `number_of_points`) VALUES ('".$user_id."', '".$transaction_id."', '".$points_awarded."')");
				}elseif($type == 1){
					//Casino Bet
					$setting = $db->queryOneRow("SELECT value FROM settings WHERE setting='loyalty_points_casino'");
					$points_awarded = $neg_total * $setting['value'];
					$running_points = $points_awarded + $loyalty_points['loyalty_points'];
					$db->queryDirect("UPDATE `customers` SET `loyalty_points` = '".$running_points."' WHERE `user_id` = '".$user_id."'");
					$db->queryInsert("INSERT INTO loyalty_points (`user_id`, `transaction_id`, `number_of_points`) VALUES ('".$user_id."', '".$transaction_id."', '".$points_awarded."')");
				}elseif($type == 17){
					//Sports Bet
					$setting = $db->queryOneRow("SELECT value FROM settings WHERE setting='loyalty_points_sports'");
					$points_awarded = $neg_total * $setting['value'];
					$running_points = $points_awarded + $loyalty_points['loyalty_points'];
					$db->queryDirect("UPDATE `customers` SET `loyalty_points` = '".$running_points."' WHERE `user_id` = '".$user_id."'");
					$db->queryInsert("INSERT INTO loyalty_points (`user_id`, `transaction_id`, `number_of_points`) VALUES ('".$user_id."', '".$transaction_id."', '".$points_awarded."')");
				}elseif($type == 46){
					//Rapidballs Bet
					$setting = $db->queryOneRow("SELECT value FROM settings WHERE setting='loyalty_points_rapidballs'");
					$points_awarded = $neg_total * $setting['value'];
					$running_points = $points_awarded + $loyalty_points['loyalty_points'];
					$db->queryDirect("UPDATE `customers` SET `loyalty_points` = '".$running_points."' WHERE `user_id` = '".$user_id."'");
					$db->queryInsert("INSERT INTO loyalty_points (`user_id`, `transaction_id`, `number_of_points`) VALUES ('".$user_id."', '".$transaction_id."', '".$points_awarded."')");
				}				
				
				return $transaction_id;
			}
		}elseif($total>0){			
			// Check daily deposit limit
			$remaining_deposit = $this->remaining_deposit_limit($user_id);
			if(($remaining_deposit - $total < 0) && $remaining_deposit != -1 && $type == 6){
				return "The transaction could not be completed because it would exceed your daily deposit limit. You can only deposit $".$remaining_deposit." more.";
			}
			
			// Log large transfers/wagers
			if($total > LARGE_TRANSFER_THRESHOLD){
				// determine transcation type
				if($type == 6){
					// Deposit
					$this->log_sigificant_event('Large Transfer', 'Deposit', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') made a Deposit in the amount of $'.$total.'.');
				}
			}
			if($total > LARGE_WIN_THRESHOLD){
				if($type == 4){
					// Lottery Win
					$this->log_sigificant_event('Large Win', 'Lottery Win', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') had a Lottery win in the amount of $'.$total.'.');
				}elseif($type == 2){
					// Casino Win
					$this->log_sigificant_event('Large Win', 'Casino Win', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') had a Casino win in the amount of $'.$total.'.');
				}elseif($type == 18){
					// Sports Win
					$this->log_sigificant_event('Large Win', 'Sports Win', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') had a Sports win in the amount of $'.$total.'.');
				}elseif($type == 47){
					// Rapidballs Win
					$this->log_sigificant_event('Large Win', 'RapidBalls Win', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') had a RapidBalls win in the amount of $'.$total.'.');
				}		
			}
			if($type == 50){
				// Refund for voided RapidBalls Bet
				// this is logged in ajax call from admin panel
			}
			
			// This is a deposit/win 
			$new_available_balance = $balances['available_balance'] + $total;
			$db->queryDirect("UPDATE customers SET available_balance=".$new_available_balance." WHERE user_id=".$user_id);
			
			$trans_id = $db->queryInsert("INSERT INTO customer_transaction (`transaction_type_id`,`user_id`,`user_session_id`,`made_by`,`transaction_details`,`amount`,`transaction_date`,`ip_address`,`bonus_used`, `ticket_number`, `balance`, `initial_balance`) VALUES (".$type.",".$user_id.",'".$session_id."','".$made_by."','".$details."',".$total.",'".$date_str."','".$_SERVER['REMOTE_ADDR']."',0, ".$ticket_number.", '".$new_available_balance."', '".$balances['available_balance']."')");
			return $trans_id;
		}else{
			// This is a transaction from freeplay only
			$trans_id = $db->queryInsert("INSERT INTO customer_transaction (`transaction_type_id`,`user_id`,`user_session_id`,`made_by`,`transaction_details`,`amount`,`transaction_date`,`ip_address`,`bonus_used`, `ticket_number`, `balance`) VALUES (".$type.",".$user_id.",'".$session_id."','".$made_by."','".$details."',".$total.",'".$date_str."','".$_SERVER['REMOTE_ADDR']."',0, ".$ticket_number.", '".$balances['available_balance']."')");
			return $trans_id;
		}

		return false;
	}
	
	public function make_casino_transaction($user_id, $game_id, $transaction_date, $total_wagered, $bal_adj, $session_id, $game_name, $jackpot_contribution, $bonus_play, $casino_game_type, $casino_game_id, $description, $vendor_id){
		global $db;
		global $database;
		
		$bonus_used = 0;
		
		// reformat date
		//$trans_dt = new DateTime($transaction_date);
		$trans_dt = new DateTime();
		$transaction_date = $trans_dt->format('Y-m-d H:i:s');
		$time = time();
		
		// escape special characters
		$game_name = addslashes($game_name);
		$description = addslashes($description);

		// get user info
		$user_info = $db->queryOneRow("SELECT * FROM ".TBL_USERS." WHERE id='$user_id'");
		$username = $user_info['username'];
		
		// lookup user's session id
		$q = "SELECT `id` FROM `user_sessions` WHERE `user_id` = '".$user_id."' ORDER BY `start` DESC LIMIT 1";
		$sess = $db->queryOneRow($q);
		$session_id = $sess['id'];
		
		// update last login and timestamp
		$db->query("UPDATE ".TBL_USERS." SET last_login = '$transaction_date' WHERE id = '$user_id'");
		$db->query("UPDATE ".TBL_USERS." SET timestamp = '$time' WHERE id = '$user_id'");
		$db->query("REPLACE INTO ".TBL_ACTIVE_USERS." VALUES ('$username', '$time')");
		
		//Get user balances
		$balances = $this->check_balance("customers", $user_id);
		$new_available_balance = $balances['available_balance'] + $bal_adj;
		
		$q = "SELECT `transaction_limit` FROM `customers` WHERE `user_id`=%i";
		$cus_info = $db->queryOneRow($q, array($user_id));
		
		//Check if this is a withdrawal or a deposit transaction
		if($bal_adj < 0){
			//Calculate this uses net loss and gain for the day
			$now = new DateTime();
			$now_str = $now->format("Y-m-d");
			$q = "SELECT SUM(amount) AS `net` FROM `customer_transaction` WHERE `user_id`=%i AND CAST(`transaction_date` AS DATE)=%s AND (`transaction_type_id`='1' OR `transaction_type_id`='2' OR `transaction_type_id`='3' OR `transaction_type_id`='4' OR `transaction_type_id`='17' OR `transaction_type_id`='18' OR `transaction_type_id`='46' OR `transaction_type_id`='47') GROUP BY `user_id`";
			$user_net = $db->queryOneRow($q, array($user_id, $now_str));
			
			//Check to see if their net for today exceeds their allowed daily limit
			if($user_net['net'] + $bal_adj < -$cus_info['daily_loss_limit'] && $cus_info['daily_loss_limit'] > 0){
				return "Self-defined loss limit will be exceeded, no transaction was made!";
			}
			
			// This is a withdrawal/bet
			$trans_type_id = 1;
			$neg_total = -$bal_adj;
			if($neg_total > $balances['available_balance']){
				//The total exceeds the amount that the user has available
				return "The user doesn't have enough money to withdrawal!";
			}else{				
				if($balances['bonus_balance']+$bal_adj==$bal_adj){
					//There is no bonus balance to remove
					$bonus_used = 0;
					$new_bonus_balance = 0;
				}elseif($balances['bonus_balance']+$bal_adj>=0){
					//Bonus balance is higher than or equal to total
					$bonus_used = $bal_adj;
					$new_bonus_balance = $balances['bonus_balance']+$bal_adj;
				}else{
					//Bonus is not 0 but will not cover the full amount
					$bonus_used = $balances['bonus_balance'];
					$new_bonus_balance = 0;
				}
				
				//Check to see if amount is larger than their allowed transaction limit
				if(abs($bal_adj) > $cus_info['transaction_limit'] && $cus_info['transaction_limit'] != -1){
					return "The transaction could not be completed because it is larger than the transaction limit you have set. You can only bet up to $".$cus_info['transaction_limit'].".";
				}
				
				// update customer balance
				$db->queryDirect("UPDATE customers SET available_balance=".$new_available_balance." WHERE user_id=".$user_id);
				$db->queryDirect("UPDATE customers SET bonus_balance=".$new_bonus_balance." WHERE user_id=".$user_id);
				
				// store transaction
				$q = "INSERT INTO customer_transaction (
					`user_id`, `user_session_id`, `amount`, `transaction_type_id`, `transaction_record_id`, `made_by`, `manager_id`, `transaction_details`, `transaction_date`, `vendor_id`, `ip_address`,`bonus_used`, `ticket_number`, `balance`, `initial_balance`
				) VALUES (
					'".$user_id."', '".$session_id."', '".$bal_adj."', '".$trans_type_id."', '".$casino_game_id."', '-9999', '-9999', '".$description."', '".$transaction_date."', '".$vendor_id."', '".$_SERVER['REMOTE_ADDR']."', ".$bonus_used.", NULL, '".$new_available_balance."', '".$balances['available_balance']."'
				)";
				$transaction_id = $db->queryInsert($q);
				
				//Get current number of loyalty points
				$loyalty_points = $db->queryOneRow("SELECT loyalty_points FROM customers WHERE user_id='".$user_id."'");
				
				//Determine Loyalty Points
				if($trans_type_id == 1){
					//Casino Bet
					$setting = $db->queryOneRow("SELECT value FROM settings WHERE setting='loyalty_points_casino'");
					$points_awarded = $neg_total * $setting['value'];
					$running_points = $points_awarded + $loyalty_points['loyalty_points'];
					$db->queryDirect("UPDATE `customers` SET `loyalty_points` = '".$running_points."' WHERE `user_id` = '".$user_id."'");
					$db->queryInsert("INSERT INTO loyalty_points (`user_id`, `transaction_id`, `number_of_points`) VALUES ('".$user_id."', '".$transaction_id."', '".$points_awarded."')");
				}
				
				return $transaction_id;
			}
		}elseif($bal_adj > 0){
			$trans_type_id = 2;
			
			// Log large wins
			if($bal_adj > LARGE_WIN_THRESHOLD){
				$this->log_sigificant_event('Large Win', 'Casino Win', $cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.') had a Casino win in the amount of $'.$bal_adj.'.');	
			}
			
			//This is a deposit/win
			$new_available_balance = $balances['available_balance'] + $bal_adj;
			$db->queryDirect("UPDATE customers SET available_balance=".$new_available_balance." WHERE user_id=".$user_id);
			
			// store transaction
			$q = "INSERT INTO customer_transaction (
				`user_id`, `user_session_id`, `amount`, `transaction_type_id`, `transaction_record_id`, `made_by`, `manager_id`, `transaction_details`, `transaction_date`, `vendor_id`, `ip_address`,`bonus_used`, `ticket_number`, `balance`, `initial_balance`
			) VALUES (
				'".$user_id."', '".$session_id."', '".$bal_adj."', '".$trans_type_id."', '".$casino_game_id."', '-9999', '-9999', '".$description."', '".$transaction_date."', '".$vendor_id."', '".$_SERVER['REMOTE_ADDR']."', ".$bonus_used.", NULL, '".$new_available_balance."', '".$balances['available_balance']."'
			)";
			$transaction_id = $db->queryInsert($q);
			return $transaction_id;
		}else{
			$trans_type_id = 2;
			
			//This is a transaction from freeplay only
			
			// store transaction
			$q = "INSERT INTO customer_transaction (
				`user_id`, `user_session_id`, `amount`, `transaction_type_id`, `transaction_record_id`, `made_by`, `manager_id`, `transaction_details`, `transaction_date`, `vendor_id`, `ip_address`,`bonus_used`, `ticket_number`, `balance`, `initial_balance`
			) VALUES (
				'".$user_id."', '".$session_id."', '".$bal_adj."', '".$trans_type_id."', '".$casino_game_id."', '-9999', '-9999', '".$description."', '".$transaction_date."', '".$vendor_id."', '".$_SERVER['REMOTE_ADDR']."', ".$bonus_used.", NULL, '".$new_available_balance."', '".$balances['available_balance']."'
			)";
			$transaction_id = $db->queryInsert($q);
			return $transaction_id;
		}

		return false;
	}
	
	public function remaining_loss_limit($user_id = NULL){
		global $db;
		global $session;
		
		if($user_id == NULL){
			$user_id = $session->userinfo['id'];
		}
		
		//Calculate this uses net loss and gain for the day
		$now = new DateTime();
		$now_str = $now->format("Y-m-d");
		$q = "SELECT SUM(amount) AS `net` FROM `customer_transaction` WHERE `user_id`=%i AND CAST(`transaction_date` AS DATE)=%s AND (`transaction_type_id`='1' OR `transaction_type_id`='2' OR `transaction_type_id`='3' OR `transaction_type_id`='4' OR `transaction_type_id`='17' OR `transaction_type_id`='18' OR `transaction_type_id`='46' OR `transaction_type_id`='47') GROUP BY `user_id`";
		$user_net = $db->queryOneRow($q, array($user_id, $now_str));

		$q = "SELECT `daily_loss_limit` FROM `customers` WHERE `user_id`=%i";
		$limit = $db->queryOneRow($q, array($user_id));
		
		if($limit['daily_loss_limit'] == -1){
			return -1;
		}
		
		if($user_net['net'] + $limit['daily_loss_limit'] <= 0){
			$date = new DateTime();
			$now = $date->format("Y-m-d H:i:s");
			$q = "SELECT * FROM `exclusion_log` WHERE `excluded_on` < %s AND `exclusion_end` > %s AND `exclusion_type_id` = 1";
			$existant_exclusion = $db->query($q, array($now, $now));
			
			if(empty($existant_exclusion)){
				//Log this exclusion
				$lock_until = $date->format("Y-m-d")." 23:59:59";
				$q = "INSERT INTO `exclusion_log` (`exclusion_type_id`, `excluded_on`, `exclusion_end`, `user_id`) VALUES (1, %s, %s, %i)";
				$db->queryInsert($q, array($now, $lock_until, $user_id));
		
				//Lock out the customer until the end of the day.
				$q = "UPDATE `users` SET `locked`=1, `locked_expiration`=%s WHERE `id`=%i";
				$db->queryDirect($q, array($lock_until, $user_id));
				
				$session->logout("Daily Loss Limit Exceeded");
			}
			
			return 0;
		}
		
		return $user_net['net'] + $limit['daily_loss_limit'];
	}
	
	public function remaining_withdrawal_limit($user_id = NULL){
		global $db;
		global $session;
		
		if($user_id == NULL){
			$user_id = $session->userinfo['id'];
		}
		
		//Calculate this uses net loss and gain for the day
		$now = new DateTime();
		$now_str = $now->format("Y-m-d");
		$q = "SELECT SUM(amount) AS `net` FROM `customer_transaction` WHERE `user_id`=%i AND CAST(`transaction_date` AS DATE)=%s AND (`transaction_type_id`='5') GROUP BY `user_id`";
		$user_net = $db->queryOneRow($q, array($user_id, $now_str));

		$q = "SELECT `daily_withdrawal_limit` FROM `customers` WHERE `user_id`=%i";
		$limit = $db->queryOneRow($q, array($user_id));
		
		if($limit['daily_withdrawal_limit'] == -1){
			return -1;
		}
		
		if($user_net['net'] + $limit['daily_withdrawal_limit'] <= 0){
			$date = new DateTime();
			$now = $date->format("Y-m-d H:i:s");
			$q = "SELECT * FROM `exclusion_log` WHERE `excluded_on` < %s AND `exclusion_end` > %s AND `exclusion_type_id` = 5";
			$existant_exclusion = $db->query($q, array($now, $now));
			
			if(empty($existant_exclusion)){
				//Log this exclusion
				$lock_until = $date->format("Y-m-d")." 23:59:59";
				$q = "INSERT INTO `exclusion_log` (`exclusion_type_id`, `excluded_on`, `exclusion_end`, `user_id`) VALUES (5, %s, %s, %i)";
				$db->queryInsert($q, array($now, $lock_until, $user_id));
			}
			return 0;
		}
		
		return $user_net['net'] + $limit['daily_withdrawal_limit'];
	}
	
	public function remaining_deposit_limit($user_id = NULL){
		global $db;
		global $session;
		
		if($user_id == NULL){
			$user_id = $session->userinfo['id'];
		}
		
		//Calculate this uses net loss and gain for the day
		$now = new DateTime();
		$now_str = $now->format("Y-m-d");
		$q = "SELECT SUM(amount) AS `net` FROM `customer_transaction` WHERE `user_id`=%i AND CAST(`transaction_date` AS DATE)=%s AND (`transaction_type_id`='6') GROUP BY `user_id`";
		$user_net = $db->queryOneRow($q, array($user_id, $now_str));

		$q = "SELECT `daily_deposit_limit` FROM `customers` WHERE `user_id`=%i";
		$limit = $db->queryOneRow($q, array($user_id));
		
		if($limit['daily_deposit_limit'] == -1){
			return -1;
		}
		
		if($user_net['net'] - $limit['daily_deposit_limit'] >= 0){
			$date = new DateTime();
			$now = $date->format("Y-m-d H:i:s");
			$q = "SELECT * FROM `exclusion_log` WHERE `excluded_on` < %s AND `exclusion_end` > %s AND `exclusion_type_id` = 6";
			$existant_exclusion = $db->query($q, array($now, $now));
			
			if(empty($existant_exclusion)){
				//Log this exclusion
				$lock_until = $date->format("Y-m-d")." 23:59:59";
				$q = "INSERT INTO `exclusion_log` (`exclusion_type_id`, `excluded_on`, `exclusion_end`, `user_id`) VALUES (6, %s, %s, %i)";
				$db->queryInsert($q, array($now, $lock_until, $user_id));
			}
			return 0;
		}
		
		return abs($user_net['net'] - $limit['daily_deposit_limit']);
	}
	
	public function remaining_wager_limit($user_id = NULL){
		global $db;
		global $session;
		
		if($user_id == NULL){
			$user_id = $session->userinfo['id'];
		}
		
		//Calculate this uses net loss and gain for the day
		$now = new DateTime();
		$now_str = $now->format("Y-m-d");
		$q = "SELECT SUM(amount) AS `net` FROM `customer_transaction` WHERE `user_id`=%i AND CAST(`transaction_date` AS DATE)=%s AND (`transaction_type_id`='1' OR `transaction_type_id`='3' OR `transaction_type_id`='17' OR `transaction_type_id`='46') GROUP BY `user_id`";
		$user_net = $db->queryOneRow($q, array($user_id, $now_str));

		$q = "SELECT `daily_wager_limit` FROM `customers` WHERE `user_id`=%i";
		$limit = $db->queryOneRow($q, array($user_id));
		
		if($limit['daily_wager_limit'] == -1){
			return -1;
		}
		
		if($user_net['net'] + $limit['daily_wager_limit'] <= 0){
			$date = new DateTime();
			$now = $date->format("Y-m-d H:i:s");
			$q = "SELECT * FROM `exclusion_log` WHERE `excluded_on` < %s AND `exclusion_end` > %s AND `exclusion_type_id` = 3";
			$existant_exclusion = $db->query($q, array($now, $now));
			
			if(empty($existant_exclusion)){
				//Log this exclusion
				$lock_until = $date->format("Y-m-d")." 23:59:59";
				$q = "INSERT INTO `exclusion_log` (`exclusion_type_id`, `excluded_on`, `exclusion_end`, `user_id`) VALUES (3, %s, %s, %i)";
				$db->queryInsert($q, array($now, $lock_until, $user_id));
		
				//Lock out the customer until the end of the day.
				$q = "UPDATE `users` SET `locked`=1, `locked_expiration`=%s WHERE `id`=%i";
				$db->queryDirect($q, array($lock_until, $user_id));
				
				$session->logout("Daily Wager Limit Exceeded");
			}
			
			return 0;
		}
		
		return $user_net['net'] + $limit['daily_wager_limit'];
	}

	public function update_daily_totals($date = NULL){
		global $db;

		if($date == NULL){
			$date = date("Y-m-d",time());
		}

		$q = "SELECT * FROM `customer_transaction` WHERE transaction_date>'".$date." 00:00:00' AND transaction_date<'".$date." 23:59:59'";
		$customer_transactions = $db->query($q);

		$q = "SELECT * FROM `panel_user_transaction` AS `put` LEFT JOIN `lotto_ticket` AS `t` ON `put`.`ticket_number`=`t`.`ticket_number` WHERE transaction_date>'".$date." 00:00:00' AND transaction_date<'".$date." 23:59:59'";
		$panel_user_transactions = $db->query($q);

		//initialize the totals
		$sales = 0;
		$withdrawals = 0;
		$deposits = 0;
		$commission = 0;
		$winnings = 0;
		$voids = 0;
		$casino_wins = 0;
		$casino_bets = 0;
		$lotto_wins = 0;
		$lotto_bets = 0;
		$sports_wins = 0;
		$sports_bets = 0;
		$rapidballs_wins = 0;
		$rapidballs_bets = 0;

		$customer = array();
		
		foreach($customer_transactions as $transaction){
			switch ($transaction['transaction_type_id']){
				case "1":
					//Casino Bet
					$sales += abs($transaction['amount']);
					$casino_bets += abs($transaction['amount']);
					$customer_total['casino_bet'] += abs($transaction['amount']);
					$customer[$transaction['user_id']]['casino_bet_amount'] = isset($customer[$transaction['user_id']]['casino_bet_amount']) == true ? $customer[$transaction['user_id']]['casino_bet_amount'] += abs($transaction['amount']) : abs($transaction['amount']);
					$customer[$transaction['user_id']]['casino_bet_number'] = isset($customer[$transaction['user_id']]['casino_bet_number']) == true ? $customer[$transaction['user_id']]['casino_bet_number']+1 : 1;
					break;
				case "2":
					//Casino Win
					$winnings += -$transaction['amount'];
					$casino_wins += -$transaction['amount'];
					$customer_total['casino_win'] += -$transaction['amount'];
					$customer[$transaction['user_id']]['casino_win_amount'] = isset($customer[$transaction['user_id']]['casino_win_amount']) == true ? $customer[$transaction['user_id']]['casino_win_amount'] += -$transaction['amount'] : -$transaction['amount'];
					$customer[$transaction['user_id']]['casino_win_number'] = isset($customer[$transaction['user_id']]['casino_win_number']) == true ? $customer[$transaction['user_id']]['casino_win_number']+1 : 1;
					break;
				case "3":
					//Lotto Purchase
					$sales += abs($transaction['amount']);
					$lotto_bets += abs($transaction['amount']);
					$customer_total['lotto_purchase'] += abs($transaction['amount']);
					$customer[$transaction['user_id']]['lotto_purchase_amount'] = isset($customer[$transaction['user_id']]['lotto_purchase_amount']) == true ? $customer[$transaction['user_id']]['lotto_purchase_amount'] += abs($transaction['amount']) : abs($transaction['amount']);
					$customer[$transaction['user_id']]['lotto_purchase_number'] = $customer[$transaction['user_id']]['lotto_purchase_number'] > 0 ? $customer[$transaction['user_id']]['lotto_purchase_number']+1 : 1;
					break;
				case "4":
					//Lotto Win
					$winnings += -$transaction['amount'];
					$lotto_wins += -$transaction['amount'];
					$customer_total['lotto_win'] += -$transaction['amount'];
					$customer[$transaction['user_id']]['lotto_win_amount'] = isset($customer[$transaction['user_id']]['lotto_win_amount']) == true ? $customer[$transaction['user_id']]['lotto_win_amount'] += -$transaction['amount'] : -$transaction['amount'];
					$customer[$transaction['user_id']]['lotto_win_number'] = isset($customer[$transaction['user_id']]['lotto_win_number']) == true ? $customer[$transaction['user_id']]['lotto_win_number']+1 : 1;
					break;
				case "5":
					//Withdrawal
					$withdrawals += $transaction['amount'];
					$customer_total['withdrawals'] += $transaction['amount'];
					$customer[$transaction['user_id']]['withdrawal_amount'] = isset($customer[$transaction['user_id']]['withdrawal_amount']) == true ? $customer[$transaction['user_id']]['withdrawal_amount'] += $transaction['amount'] : $transaction['amount'];
					$customer[$transaction['user_id']]['withdrawal_number'] = isset($customer[$transaction['user_id']]['withdrawal_number']) == true ? $customer[$transaction['user_id']]['withdrawal_number']+1 : 1;
					break;
				case "6":
					//Deposit
					$deposits += abs($transaction['amount']);
					$customer_total['deposits'] += abs($transaction['amount']);
					$customer[$transaction['user_id']]['deposit_amount'] = isset($customer[$transaction['user_id']]['deposit_amount']) == true ? $customer[$transaction['user_id']]['deposit_amount'] += abs($transaction['amount']) : abs($transaction['amount']);
					$customer[$transaction['user_id']]['deposit_number'] = isset($customer[$transaction['user_id']]['deposit_number']) == true ? $customer[$transaction['user_id']]['deposit_number']+1 : 1;
					break;
				case "17":
					//Sportsbook purchase
					$sales += abs($transaction['amount']);
					$sports_bets += abs($transaction['amount']);
					$customer_total['sportsbook_purchase'] += abs($transaction['amount']);
					$customer[$transaction['user_id']]['sportsbook_bet_amount'] = isset($customer[$transaction['user_id']]['sportsbook_bet_amount']) == true ? $customer[$transaction['user_id']]['sportsbook_bet_amount'] += abs($transaction['amount']) : abs($transaction['amount']);
					$customer[$transaction['user_id']]['sportsbook_bet_number'] = isset($customer[$transaction['user_id']]['sportsbook_bet_number']) == true ? $customer[$transaction['user_id']]['sportsbook_bet_number']+1 : 1;
					break;
				case "18":
					//Sportsbook payout
					$winnings += -$transaction['amount'];
					$sports_wins += -$transaction['amount'];
					$customer_total['sportsbook_payout'] += -$transaction['amount'];
					$customer[$transaction['user_id']]['sportsbook_win_amount'] = isset($customer[$transaction['user_id']]['sportsbook_win_amount']) == true ? $customer[$transaction['user_id']]['sportsbook_win_amount'] += -$transaction['amount'] : -$transaction['amount'];
					$customer[$transaction['user_id']]['sportsbook_win_number'] = isset($customer[$transaction['user_id']]['sportsbook_win_number']) == true ? $customer[$transaction['user_id']]['sportsbook_win_number']+1 : 1;
					break;
				case "46":
					//Rapidballs purchase
					$sales += abs($transaction['amount']);
					$rapidballs_bets += abs($transaction['amount']);
					$customer_total['rapidballs_purchase'] += abs($transaction['amount']);
					$customer[$transaction['user_id']]['rapidballs_bet_amount'] = isset($customer[$transaction['user_id']]['rapidballs_bet_amount']) == true ? $customer[$transaction['user_id']]['rapidballs_bet_amount'] += abs($transaction['amount']) : abs($transaction['amount']);
					$customer[$transaction['user_id']]['rapidballs_bet_number'] = isset($customer[$transaction['user_id']]['rapidballs_bet_number']) == true ? $customer[$transaction['user_id']]['rapidballs_bet_number']+1 : 1;
					break;
				case "47":
					//Rapidballs payout
					$winnings += -$transaction['amount'];
					$rapidballs_wins += -$transaction['amount'];
					$customer_total['rapidballs_payout'] += -$transaction['amount'];
					$customer[$transaction['user_id']]['rapidballs_win_amount'] = isset($customer[$transaction['user_id']]['rapidballs_win_amount']) == true ? $customer[$transaction['user_id']]['rapidballs_win_amount'] += -$transaction['amount'] : -$transaction['amount'];
					$customer[$transaction['user_id']]['rapidballs_win_number'] = isset($customer[$transaction['user_id']]['rapidballs_win_number']) == true ? $customer[$transaction['user_id']]['rapidballs_win_number']+1 : 1;
					break;
				case "50":
					//Rapidballs refund
					$sales += -$transaction['amount'];
					$rapidballs_bets += -$transaction['amount'];
					$customer_total['rapidballs_purchase'] += -$transaction['amount'];
					$customer[$transaction['user_id']]['rapidballs_bet_amount'] = isset($customer[$transaction['user_id']]['rapidballs_bet_amount']) == true ? $customer[$transaction['user_id']]['rapidballs_bet_amount'] += -$transaction['amount'] : -$transaction['amount'];
					$customer[$transaction['user_id']]['rapidballs_bet_number'] = isset($customer[$transaction['user_id']]['rapidballs_bet_number']) == true ? $customer[$transaction['user_id']]['rapidballs_bet_number']-1 : null;
					break;
				case "21":
					//Phone card purchase
					$sales += abs($transaction['amount']);
					break;
				case "22":
					//Phone card payout
					$voids += -$transaction['amount'];
					break;
			}
		}

		foreach($panel_user_transactions as $transaction){
			switch ($transaction['type_id']){
				case "7":
					//Lotto Purchase
					$sales += $transaction['amount'];
					$lotto_bets += $transaction['amount'];
					$commission += ($transaction['amount']*$transaction['commission_rate']);
					break;
				case "12":
					//Lotto Win
					$winnings += $transaction['amount'];
					$lotto_wins += $transaction['amount'];
					break;
				case "8":
					//Void lotto
					$voids += $transaction['amount'];
					break;
				case "19":
					//Phone card purchase
					$sales += $transaction['amount'];
					break;
				case "20":
					//Phone card payout
					$voids += $transaction['amount'];
					break;
			}
		}

		$curr_totals = $this->get_daily_totals($date);

		if($curr_totals == NULL){
			$q = "INSERT INTO `cron_daily_totals` (`total_date`,`sales`,`voids`,`deposits`,`withdrawals`,`commission`,`winnings`, `casino_wins`, `casino_bets`, `lotto_wins`, `lotto_bets`, `sports_wins`, `sports_bets`, `rapidballs_bets`, `rapidballs_wins`) VALUES ('".$date."','".$sales."','".$voids."','".$deposits."','".$withdrawals."','".-$commission."','".$winnings."', '".$casino_wins."', '".$casino_bets."', '".$lotto_wins."', '".$lotto_bets."', '".$sports_wins."', '".$sports_bets."', '".$rapidballs_bets."', '".$rapidballs_wins."')";
			$db->queryInsert($q);
		}else{
			$q = "UPDATE `cron_daily_totals` SET `sales`='".$sales."',`voids`='".$voids."',`deposits`='".$deposits."',`withdrawals`='".$withdrawals."',`commission`='".-$commission."',`winnings`='".$winnings."',`casino_wins`='".$casino_wins."',`casino_bets`='".$casino_bets."',`lotto_wins`='".$lotto_wins."',`lotto_bets`='".$lotto_bets."',`sports_wins`='".$sports_wins."',`sports_bets`='".$sports_bets."',`rapidballs_wins`='".$rapidballs_wins."',`rapidballs_bets`='".$rapidballs_bets."' WHERE `total_date`='".$date."'";			
			$db->queryDirect($q);
		}

		//Look for pending lotto bets
		$q = "SELECT bet_amount, user_id FROM `lotto_bet` AS `lb` JOIN `lotto_ticket` AS `lt` ON `lb`.`ticket_id`=`lt`.`ticket_id` WHERE `lb`.`is_processed`=0";
		$bets = $db->query($q);
		
		foreach($bets as $bet){
			$customer[$bet['user_id']]['lotto_pending_amount'] = isset($customer[$bet['user_id']]['lotto_pending_amount']) == true ? $customer[$bet['user_id']]['lotto_pending_amount'] += abs($bet['bet_amount']) : abs($bet['bet_amount']);
			$customer[$bet['user_id']]['lotto_pending_number'] = isset($customer[$bet['user_id']]['lotto_pending_number']) == true ? $customer[$bet['user_id']]['lotto_pending_number']+1 : 1;
		}
		
		//Look for pending rapidball bets
		$q = "SELECT bet_amount, user_id FROM `rapidballs_bet` AS `rb` JOIN `rapidballs_ticket` AS `rt` ON `rb`.`ticket_id`=`rt`.`id` WHERE `rb`.`is_processed`=0";
		$bets = $db->query($q);
		
		foreach($bets as $bet){
			$customer[$bet['user_id']]['rapidballs_pending_amount'] = isset($customer[$bet['user_id']]['rapidballs_pending_amount']) == true ? $customer[$bet['user_id']]['rapidballs_pending_amount'] += abs($bet['bet_amount']) : abs($bet['bet_amount']);
			$customer[$bet['user_id']]['rapidballs_pending_number'] = isset($customer[$bet['user_id']]['rapidballs_pending_number']) == true ? $customer[$bet['user_id']]['rapidballs_pending_number']+1 : 1;
		}
		
		//Create lookup
		$q = "SELECT user_id FROM `cron_customer_totals` WHERE `total_date`='".$date."'";
		$ids = $db->query($q);
		foreach($ids as $id){
			$user_lookup[$id['user_id']] = "exists";
		}

		foreach($customer as $user_id => $value){
			$casino_bet_amount = isset($value['casino_bet_amount']) == true ? $value['casino_bet_amount'] : 0;
			$casino_bet_number = isset($value['casino_bet_number']) == true ? $value['casino_bet_number'] : 0;
			$casino_win_amount = isset($value['casino_win_amount']) == true ? $value['casino_win_amount'] : 0;
			$casino_win_number = isset($value['casino_win_number']) == true ? $value['casino_win_number'] : 0;
			$lotto_purchase_amount = isset($value['lotto_purchase_amount']) == true ? $value['lotto_purchase_amount'] : 0;
			$lotto_purchase_number = isset($value['lotto_purchase_number']) == true ? $value['lotto_purchase_number'] : 0;
			$lotto_win_amount = isset($value['lotto_win_amount']) == true ? $value['lotto_win_amount'] : 0;
			$lotto_win_number = isset($value['lotto_win_number']) == true ? $value['lotto_win_number'] : 0;
			$lotto_pending_amount = isset($value['lotto_pending_amount']) == true ? $value['lotto_pending_amount'] : 0;
			$lotto_pending_number = isset($value['lotto_pending_number']) == true ? $value['lotto_pending_number'] : 0;
			$withdrawal_amount = isset($value['withdrawal_amount']) == true ? $value['withdrawal_amount'] : 0;
			$withdrawal_number = isset($value['withdrawal_number']) == true ? $value['withdrawal_number'] : 0;
			$deposit_amount = isset($value['deposit_amount']) == true ? $value['deposit_amount'] : 0;
			$deposit_number = isset($value['deposit_number']) == true ? $value['deposit_number'] : 0;
			$sportsbook_bet_amount = isset($value['sportsbook_bet_amount']) == true ? $value['sportsbook_bet_amount'] : 0;
			$sportsbook_bet_number = isset($value['sportsbook_bet_number']) == true ? $value['sportsbook_bet_number'] : 0;
			$sportsbook_win_amount = isset($value['sportsbook_win_amount']) == true ? $value['sportsbook_win_amount'] : 0;
			$sportsbook_win_number = isset($value['sportsbook_win_number']) == true ? $value['sportsbook_win_number'] : 0;
			$rapidballs_bet_amount = isset($value['rapidballs_bet_amount']) == true ? $value['rapidballs_bet_amount'] : 0;
			$rapidballs_bet_number = isset($value['rapidballs_bet_number']) == true ? $value['rapidballs_bet_number'] : 0;
			$rapidballs_win_amount = isset($value['rapidballs_win_amount']) == true ? $value['rapidballs_win_amount'] : 0;
			$rapidballs_win_number = isset($value['rapidballs_win_number']) == true ? $value['rapidballs_win_number'] : 0;
			$rapidballs_pending_amount = isset($value['rapidballs_pending_amount']) == true ? $value['rapidballs_pending_amount'] : 0;
			$rapidballs_pending_number = isset($value['rapidballs_pending_number']) == true ? $value['rapidballs_pending_number'] : 0;

			if($user_lookup[$user_id] != "exists"){
				$q = "INSERT INTO `cron_customer_totals` (`total_date`,`user_id`,`casino_bet_amount`,`casino_bet_number`,`casino_win_amount`,`casino_win_number`,`lotto_purchase_amount`,`lotto_purchase_number`,`lotto_win_amount`,`lotto_win_number`,`lotto_pending_amount`,`lotto_pending_number`,`withdrawal_amount`,`withdrawal_number`,`deposit_amount`,`deposit_number`,`sportsbook_bet_amount`,`sportsbook_bet_number`,`sportsbook_win_amount`,`sportsbook_win_number`,`rapidballs_bet_amount`,`rapidballs_bet_number`,`rapidballs_win_amount`,`rapidballs_win_number`,`rapidballs_pending_amount`,`rapidballs_pending_number`) VALUES ('".$date."','".$user_id."','".$casino_bet_amount."','".$casino_bet_number."','".$casino_win_amount."','".$casino_win_number."','".$lotto_purchase_amount."','".$lotto_purchase_number."','".$lotto_win_amount."','".$lotto_win_number."','".$lotto_pending_amount."','".$lotto_pending_number."','".$withdrawal_amount."','".$withdrawal_number."','".$deposit_amount."','".$deposit_number."','".$sportsbook_bet_amount."','".$sportsbook_bet_number."','".$sportsbook_win_amount."','".$sportsbook_win_number."','".$rapidballs_bet_amount."','".$rapidballs_bet_number."','".$rapidballs_win_amount."','".$rapidballs_win_number."','".$rapidballs_pending_amount."','".$rapidballs_pending_number."')";
				echo $q."<br>";
				$db->queryInsert($q);
			}else{
				$q = "UPDATE `cron_customer_totals` SET `casino_bet_amount`='".$casino_bet_amount."',`casino_bet_number`='".$casino_bet_number."',`casino_win_amount`='".$casino_win_amount."',`casino_win_number`='".$casino_win_number."',`lotto_purchase_amount`='".$lotto_purchase_amount."',`lotto_purchase_number`='".$lotto_purchase_number."',`lotto_win_amount`='".$lotto_win_amount."',`lotto_win_number`='".$lotto_win_number."',`lotto_pending_amount`='".$lotto_pending_amount."',`lotto_pending_number`='".$lotto_pending_number."',`withdrawal_amount`='".$withdrawal_amount."',`withdrawal_number`='".$withdrawal_number."',`deposit_amount`='".$deposit_amount."',`deposit_number`='".$deposit_number."',`sportsbook_bet_amount`='".$sportsbook_bet_amount."',`sportsbook_bet_number`='".$sportsbook_bet_number."',`sportsbook_win_amount`='".$sportsbook_win_amount."',`sportsbook_win_number`='".$sportsbook_win_number."',`rapidballs_bet_amount`='".$rapidballs_bet_amount."',`rapidballs_bet_number`='".$rapidballs_bet_number."',`rapidballs_win_amount`='".$rapidballs_win_amount."',`rapidballs_win_number`='".$rapidballs_win_number."',`rapidballs_pending_amount`='".$rapidballs_pending_amount."',`rapidballs_pending_number`='".$rapidballs_pending_number."' WHERE `total_date`='".$date."' AND `user_id`='".$user_id."'";
				echo $q."<br>";
				$db->queryDirect($q);
			}
			print_r($user_id);
		}
	}

	public function get_daily_totals($date = NULL){
		global $db;

		if($date == NULL){
			$date = date("Y-m-d",time());
		}

		$q = "SELECT * FROM `cron_daily_totals` WHERE total_date='".$date."'";
		$current_totals = $db->queryOneRow($q);

		//Calculate Net
		if($current_totals != NULL){
			$current_totals['net'] = $current_totals['sales']+$current_totals['voids']+$current_totals['desposits']+$current_totals['withdrawals']+$current_totals['commission']+$current_totals['winnings'];
		}

		return $current_totals;
	}

	public function get_customer_totals($start_date,$end_date,$customer_id = ""){
		global $db;

		if($customer_id!=""){
			$customer_id = "AND user_id='".$customer_id."' ";
		}

		$q = "SELECT SUM(casino_bet_amount) AS casino_bet_amount, SUM(casino_bet_number) AS casino_bet_number, SUM(casino_win_amount) AS casino_win_amount, SUM(casino_win_number) AS casino_win_number, SUM(lotto_purchase_amount) AS lotto_purchase_amount, SUM(lotto_purchase_number) AS lotto_purchase_number, SUM(lotto_win_amount) AS lotto_win_amount, SUM(lotto_win_number) AS lotto_win_number, SUM(lotto_pending_amount) AS lotto_pending_amount, SUM(lotto_pending_number) AS lotto_pending_number, SUM(withdrawal_amount) AS withdrawal_amount, SUM(withdrawal_number) AS withdrawal_number, SUM(deposit_amount) AS deposit_amount, SUM(deposit_number) AS deposit_number, SUM(sportsbook_bet_amount) AS sportsbook_bet_amount, SUM(sportsbook_bet_number) AS sportsbook_bet_number, SUM(sportsbook_win_amount) AS sportsbook_win_amount, SUM(sportsbook_win_number) AS sportsbook_win_number, SUM(rapidballs_bet_amount) AS rapidballs_bet_amount, SUM(rapidballs_bet_number) AS rapidballs_bet_number, SUM(rapidballs_win_amount) AS rapidballs_win_amount, SUM(rapidballs_win_number) AS rapidballs_win_number, SUM(rapidballs_pending_amount) AS rapidballs_pending_amount, SUM(rapidballs_pending_number) AS rapidballs_pending_number FROM cron_customer_totals WHERE total_date <= '".$end_date."' AND total_date >= '".$start_date."' ".$customer_id."GROUP BY user_id";
		$sum_info = $db->query($q);

		$totals['casino_bet_amount'] = 0.00;
		$totals['casino_bet_number'] = 0;
		$totals['casino_win_amount'] = 0.00;
		$totals['casino_win_number'] = 0;
		$totals['lotto_purchase_amount'] = 0.00;
		$totals['lotto_purchase_number'] = 0;
		$totals['lotto_win_amount'] = 0.00;
		$totals['lotto_win_number'] = 0;
		$totals['lotto_pending_amount'] = 0.00;
		$totals['lotto_pending_number'] = 0;
		$totals['withdrawal_amount'] = 0.00;
		$totals['withdrawal_number'] = 0;
		$totals['deposit_amount'] = 0.00;
		$totals['deposit_number'] = 0;
		$totals['sportsbook_bet_amount'] = 0.00;
		$totals['sportsbook_bet_number'] = 0;
		$totals['sportsbook_win_amount'] = 0.00;
		$totals['sportsbook_win_number'] = 0;
		$totals['rapidballs_bet_amount'] = 0.00;
		$totals['rapidballs_bet_number'] = 0;
		$totals['rapidballs_win_amount'] = 0.00;
		$totals['rapidballs_win_number'] = 0;
		$totals['rapidballs_pending_amount'] = 0.00;
		$totals['rapidballs_pending_number'] = 0;
		
		foreach($sum_info as $info){
			$totals['casino_bet_amount'] += $info['casino_bet_amount'];
			$totals['casino_bet_number'] += $info['casino_bet_number'];
			$totals['casino_win_amount'] += $info['casino_win_amount'];
			$totals['casino_win_number'] += $info['casino_win_number'];
			$totals['lotto_purchase_amount'] += $info['lotto_purchase_amount'];
			$totals['lotto_purchase_number'] += $info['lotto_purchase_number'];
			$totals['lotto_win_amount'] += $info['lotto_win_amount'];
			$totals['lotto_win_number'] += $info['lotto_win_number'];
			$totals['lotto_pending_amount'] += $info['lotto_pending_amount'];
			$totals['lotto_pending_number'] += $info['lotto_pending_number'];
			$totals['withdrawal_amount'] += $info['withdrawal_amount'];
			$totals['withdrawal_number'] += $info['withdrawal_number'];
			$totals['deposit_amount'] += $info['deposit_amount'];
			$totals['deposit_number'] += $info['deposit_number'];
			$totals['sportsbook_bet_amount'] += $info['sportsbook_bet_amount'];
			$totals['sportsbook_bet_number'] += $info['sportsbook_bet_number'];
			$totals['sportsbook_win_amount'] += $info['sportsbook_win_amount'];
			$totals['sportsbook_win_number'] += $info['sportsbook_win_number'];
			$totals['rapidballs_bet_amount'] += $info['rapidballs_bet_amount'];
			$totals['rapidballs_bet_number'] += $info['rapidballs_bet_number'];
			$totals['rapidballs_win_amount'] += $info['rapidballs_win_amount'];
			$totals['rapidballs_win_number'] += $info['rapidballs_win_number'];
			$totals['rapidballs_pending_amount'] += $info['rapidballs_pending_amount'];
			$totals['rapidballs_pending_number'] += $info['rapidballs_pending_number'];
		}
		
		//If no customer is specified, we need to include bets and payouts from the panel for lotto.
		if($customer_id == ""){
			//Get the total amount bet and paid out.
			$q = "SELECT SUM(amount) AS total, COUNT(amount) AS number FROM panel_user_transaction WHERE transaction_date <= '".$end_date." 23:59:59' AND transaction_date >= '".$start_date." 00:00:00' AND (type_id=7 OR type_id=12) GROUP BY type_id";
			$panel_sum_info = $db->query($q);
			
			foreach($panel_sum_info as $info){
				if($info['total'] > 0){
					$totals['lotto_purchase_amount'] += $info['total'];
					$totals['lotto_purchase_number'] += $info['number'];
				}elseif($info['total'] < 0){
					$totals['lotto_win_amount'] += $info['total'];
					$totals['lotto_win_number'] += $info['number'];
				}
			}
		}

		$totals['casino_net'] = $totals['casino_bet_amount']+$totals['casino_win_amount'];
		$totals['lotto_net'] = $totals['lotto_purchase_amount']+$totals['lotto_win_amount'];
		$totals['withdrawal_deposit_net'] = $totals['withdrawal_amount']+$totals['deposit_amount'];
		$totals['sportsbook_net'] = $totals['sportsbook_bet_amount']+$totals['sportsbook_win_amount'];
		$totals['rapidballs_net'] = $totals['rapidballs_bet_amount']+$totals['rapidballs_win_amount'];
		$totals['overall_net'] = $totals['casino_net']+$totals['lotto_net']+$totals['withdrawal_deposit_net']+$totals['sportsbook_net']+$totals['rapidballs_net'];

		return $totals;
	}
	/* ------------------  END BALANCE FUNCTIONS  ------------------ */

	/* ------------------  START USER ACCOUNT FUNCTIONS  ------------------ */
	public function get_user_loyalty_points($all_time = false, $user_id = NULL){
		global $db;
		global $session;
		
		if($user_id == NULL){
			$user_id = $session->userinfo['id'];
		}
		
		$q = "SELECT loyalty_points FROM customers WHERE user_id='".$user_id."'";
		$loyalty = $db->queryOneRow($q);
		
		if($all_time == false){
			$q = "SELECT SUM(number_of_points) AS number FROM loyalty_points_redemption WHERE user_id='".$user_id."'";
			$redemptions = $db->queryOneRow($q);
			return $loyalty['loyalty_points'] - $redemptions['number'];
		}else{
			return $loyalty['loyalty_points'];			
		}
	}

       public function get_promo_points($all_time = false, $user_id = NULL){
        
                global $db;
		global $session;
		
		if($user_id == NULL){
			$user_id = $session->userinfo['id'];
		}
		
		$q = "SELECT loyalty_points FROM customers WHERE user_id='".$user_id."'";
		$loyalty = $db->queryOneRow($q);
		
		if($all_time == false){
			$q = "SELECT setting,value FROM settings WHERE setting = 'per_loyalty_points' OR setting = 'promo_point'";
			$loyalty_point_offsets = $db->query($q);

                        if(!empty($loyalty_point_offsets)){
                             $per_loyalty_point = $loyalty_point_offsets[0]['value'];
                             $promo_point = $loyalty_point_offsets[1]['value'];
 
                             return (($loyalty['loyalty_points']*$promo_point)/$per_loyalty_point);                           
                        }

			return 0;
		}else{
			return $loyalty['loyalty_points'];			
		}
       }
	
	public function get_loyalty_level($user_id = NULL){
		global $db;
		global $session;
		
		if($user_id == NULL){
			$user_id = $session->userinfo['id'];
		}
		
		$gold_limit = $db->queryOneRow("SELECT value FROM settings WHERE setting='gold_point_amount'");
		$platinum_limit = $db->queryOneRow("SELECT value FROM settings WHERE setting='platinum_point_amount'");
		$diamond_limit = $db->queryOneRow("SELECT value FROM settings WHERE setting='diamond_point_amount'");
		
		$loyalty_points = $this->get_user_loyalty_points(true, $user_id);
		
		if($loyalty_points < $gold_limit['value']){
			return "Regular";
		}elseif($loyalty_points < $platinum_limit['value']){
			return "Gold";
		}elseif($loyalty_points < $diamond_limit['value']){
			return "Platinum";
		}else{
			return "Diamond";
		}
	}
	
	public function create_user($first_name, $last_name, $username, $password, $email, $send_welcome_email = false){
		global $db;
		global $session;
	
		/* Checks if registration is disabled */
		if(ACCOUNT_ACTIVATION == 4){
			return false;
		}

		/* Convert username to all lowercase */
		if(ALL_LOWERCASE){
			$username = strtolower($username);
		}

		/* Registration attempt */
		$retval = $session->register("manual", $first_name, $last_name, $username, $password, $password, $email, $email, $send_welcome_email);
		
		// 0 = success, 1 = error, 2 = failed, 3-5 = success but activation needed
		if($retval != 1 && $retval != 2){
			return true;
		}else{
			return false;
		}
	}
	
	public function loyalty_point_check(){
		global $db;
		
		$now = new datetime('now');
		$now = $now->format("Y-m-d H:i:s");
		
		$month_ago = new datetime('-1 month');
		$month_ago = $month_ago->format("Y-m-d H:i:s");
		
		$points_needed = $db->queryOneRow("SELECT value FROM settings WHERE setting='vip_point_amount'");
		$user_loyalty_points = $db->query("SELECT SUM(number_of_points) AS number_of_points, lp.user_id, is_vip FROM `loyalty_points` lp JOIN `customers` c ON `lp`.`user_id`=`c`.`user_id` JOIN `customer_transaction` ct ON `ct`.`transaction_id`=`lp`.`transaction_id` WHERE `ct`.`transaction_date`<'".$now."' AND `ct`.`transaction_date`>'".$month_ago."' GROUP BY lp.`user_id`");
		
		foreach($user_loyalty_points as $user_loyalty_point){
			if($user_loyalty_point['number_of_points'] >= $points_needed['value'] && $user_loyalty_point['is_vip'] == 0){
				$db->queryDirect("UPDATE customers SET is_vip=1 WHERE user_id='".$user_loyalty_point['user_id']."'");
			}elseif($user_loyalty_point['number_of_points'] < $points_needed['value'] && $user_loyalty_point['is_vip'] == 1){
				$db->queryDirect("UPDATE customers SET is_vip=0 WHERE user_id='".$user_loyalty_point['user_id']."'");				
			}
		}
	}
	
	public function create_customer($user_id, $customer_number, $card_number, $pin, $phone, $cellphone, $address1, $address2, $country, $city, $island_id, $gender, $dob){
		global $db;
		global $session;
	
		// add data to customer table
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		$q = "INSERT INTO `customers` (
				`customer_id`,
				`user_id`,
				`customer_number`,
				`card_number`,
				`pin`,
				`telephone`,
				`cellphone`,
				`address`,
				`address2`,
				`country`,
				`city`,
				`island_id`,
				`gender`,
				`date_of_birth`,
				`needs_to_deposit`,
				`is_verified`,
				`verified_on`,
				`available_balance`,
				`bonus_balance`,
				`created_by`,
				`created_on`,
				`updated_by`,
				`updated_on`
			) VALUES (
				NULL,
				'".$user_id."',
				'".$customer_number."',
				'".$card_number."',
				'".$pin."',
				'".$phone."',
				'".$cellphone."',
				'".$address1."',
				'".$address2."',
				'".$country."',
				'".$city."',
				'".$island_id."',
				'".$gender."',
				'".$dob."',
				'1',
				'0',
				'0',
				'0',
				'0',
				'".$session->userinfo['id']."',
				'".$now."',
				'".$session->userinfo['id']."',
				'".$now."'
			);";
		$customer_id = $db->queryInsert($q);
		
		if($customer_id < 1){
			// couldn't add customer details...
			// undo the user account creation
			$q = "DELETE FROM `users` WHERE `id`=".$_SESSION['user_id'];
			$db->query($q);
			
			// set a form error to display to the user (1 = error, 0 = success)
			$_SESSION['regsuccess'] = 1;
			
			// add data back to POST so form can be repopulated
			$_POST = $_SESSION['value_array'];
		}
	}
	
	public function activate_account($username, $activation_code){
		global $db;
		
		$q = "SELECT * FROM `users` WHERE `username` = '".$username."'";
		$user_info = $db->queryOneRow($q);
		
		if($activation_code == $user_info['actkey']){
			// confirmed, set userlevel
			$q = "UPDATE `users` SET `userlevel` = '3' WHERE `id` = '".$user_info['id']."'";
			if($db->queryDirect($q)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	/* ------------------  END USER ACCOUNT FUNCTIONS  ------------------ */
	
	/* ------------------  BEGIN PROJECT SPECIFIC LOGIN FUNCTIONS  ------------------ */
	public function change_pin($password, $new_pin, $conf_new_pin){
		global $db, $session, $database;
		//initialize error string
		$errors = array();

		//check to see if the current pin entered is correct in the database
		//$curr_pin_valid = $db->queryOneRow("SELECT pin FROM customers WHERE user_id=".$session->userinfo['id']);
		
		// see if they entered thier password correctly
		$return_code = $database->confirmUserPass($session->userinfo['username'], $password);
		if($return_code > 0){
			array_push($errors, "Your password is not correct");
		}

		//Check for matching new pins
		if($new_pin != $conf_new_pin){
			array_push($errors, "The pins do not match");
		}

		//Check for new pin length
		if(strlen($new_pin) < 5){
			array_push($errors, "New pin is too short");
		}

		if(count($errors) == 0){
			$db->query("UPDATE customers SET pin=".$new_pin." WHERE user_id=".$session->userinfo["id"]);
			return true;
		}else{
			return $errors;
		}
	}

	public function check_pin($account_number, $account_pin){
		global $db;

		$user = $db->queryOneRow("SELECT pin FROM `customers` WHERE customer_number=".$account_number);
		if($user['pin'] == $account_pin){
			return true;
		}else{
			return false;
		}
	}
	/* ------------------  END PROJECT SPECIFIC LOGIN FUNCTIONS  --------------------- */
	
	public function get_settings(){
		global $db;
		
		// get current settings from db
		$settings = array();
		$q = "SELECT * FROM `settings`";
		$settings_array = $db->query($q);
		foreach($settings_array as $setting){
			$settings[$setting['setting']] = $setting['value'];
		}
		return $settings;
	}

       /** ------------------- Virtual Money Scripts -------------------------------- **/

            public function get_virtual_balance($shift_id = NULL,$user_id = NULL){
		global $db;
		global $session;

		if($user_id == NULL){
		       $user_id = $session->userinfo['id'];
		}

		if($shift_id == NULL){
		       $shift_id = $session->userinfo['panel_user']['shift_id'];
		}
	
	        $i_vm_balance = $db->queryOneRow("SELECT virtual_money FROM panel_user_shift where user_id = ".$user_id." AND id = ".$shift_id);
		return $i_vm_balance;

	}

        public function make_virtual_money_transaction($total, $user_id, $shift_id = NULL){

                global $db;
		global $session;

		if($shift_id=="NULL"){
			if($session->userinfo['panel_user']['shift_id'] == "-1"){
				return false;
			}else{
				$shift_id = $session->userinfo['panel_user']['shift_id'];
				$location_id = $session->userinfo['panel_user']['location_id'];
			}
		}else{
			$q = "SELECT location_id, user_id FROM panel_user WHERE shift_id=%i";
			$loc = $db->queryOneRow($q, array($shift_id));
			$location_id = $loc['location_id'];
		}
		
		
                $i_virtual_balance = $this->get_virtual_balance($shift_id,$user_id);
                $i_new_virtual_balance = $i_virtual_balance['virtual_money'] + $total; 

		//Create DateTime object of now
		$date = new datetime('now');
		$date_str = $date->format("Y-m-d H:i:s");
		
		// Get customer info
		$q = "SELECT * FROM `users` AS `u` LEFT JOIN `customers` AS `c` ON `u`.`id` = `c`.`user_id` LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id` = `c`.`user_id` WHERE `u`.`id`=%i";
		$cus_info = $db->queryOneRow($q, array($user_id));

		
		if($total>0){		

                        $this->log_sigificant_event('Virtual Money', 'Virtual Money Transfer', $session->userinfo['firstname'].' '.$session->userinfo['lastname'].' ('.$session->userinfo['id'].') made a virtual money transfer of the amount $'.$total.' for cashier: '.$cus_info['firstname'].' '.$cus_info['lastname'].' ('.$user_id.').');
						
			$db->queryDirect("INSERT INTO panel_user_virtual_money (`user_id`,`panel_user_shift_id`,`virtual_amount`,`transaction_date`,`status`) VALUES (".$user_id.",".$shift_id.",".$total.",'".$date_str."',2)");

                        $db->queryDirect("UPDATE panel_user_shift SET virtual_money = ".$i_new_virtual_balance." WHERE id = ".$shift_id);

			//$trans_id = $db->queryInsert("INSERT INTO panel_user_transaction (`type_id`,`made_by`,`location_id`,`shift_id`,`customer_id`,`transaction_details`, `amount`, `transaction_date`, `balance`, `initial_balance`) VALUES (25,".$session->userinfo['id'].",".$location_id.",".$shift_id.",'-9999','".$session->userinfo['firstname']." ".$session->userinfo['lastname']." (".$session->userinfo['id'].") made a virtual money transfer of the amount $".$total." for cashier: ".$cus_info['firstname']." ".$cus_info['lastname']." (".$user_id.").', ".$total.", '".$date_str."', ".$i_new_virtual_balance.", ".$i_virtual_balance['virtual_money'].")");
			return true;
		}else{
			//Cant happen, error out
			return false;
		}

		return false;
       }
      /** ------------------- End Virtual Money Scripts ---------------------------- **/
}
