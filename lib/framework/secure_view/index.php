<?php
	ob_start();
	
	// Include core class
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	}else{
		include(DOMAIN_ROOT.'/config.php');
	}
	
	$root_dir = "/srv/www/uploads/";
	
	if(isset($_GET['r'])){
		$filepath = $_GET['r'];
	}
	
	if(!$session->logged_in){
		// log to db
		$timestamp_dt = new DateTime();
		$timestamp = $timestamp_dt->format("Y-m-d H:i:s");
		
		$q = "INSERT INTO `log_uploads` (`id`, `timestamp`, `ip_address`, `user_id`, `action`, `file_path`, `result`, `details`) VALUES (NULL, %s, %s, %i, 'view', %s, 'fail', 'Unauthorized Access - Attempted to view file without being logged in');";
		$db->queryInsert($q, array($timestamp, $_SERVER['REMOTE_ADDR'], $session->userinfo['id'], $root_dir.$filepath));

		echo "<h1>You must be logged in...</h1>";
		die("<h2>Unauthorized access by ".$_SERVER['REMOTE_ADDR']." has been logged.</h2>");
	}else{
		// see if user should have access to this file
		
		// seperate file/folder and escape slashes
		$path_arr = explode(DIRECTORY_SEPARATOR, $filepath);
		$folder = "";
		$file = "";
		$p_count = 1;
		foreach($path_arr as $p){
			if($p_count <= count($path_arr) - 1){
				$folder .= $p . DIRECTORY_SEPARATOR;
			}
			$p_count++;
			$file = $p;
		}
		
		$filepath = $root_dir.$filepath;

		//$mime_type = system("file -i -b '".$filepath."'");
		
		// compute authorization key and see if it matches
		$challenge = $_GET['auth'];
		$auth = sha1(md5($session->userinfo['email'].$session->userinfo['id'].date('Ymdh')));
		if($auth != $challenge){			
			// give it an extra hour window to prevent timeout if hour just changed
			$date_str = date('Ymdh') - 1;
			$auth = sha1(md5($session->userinfo['email'].$_SESSION['userid'].$date_str));
		}
		
		if($auth === $challenge){
			if(file_exists($filepath)){				
				//header('Content-Type: application/octet-stream');
				header("Content-Type: $mime_type");
				header('Content-Disposition: inline; filename="'.strtolower($_GET['name']).'"');
				header('Expires: 0');
				header('Cache-Control: no-cache, must-revalidate');
				header('Pragma: public');
				header('Content-Length: '.filesize($filepath));
				header('Accept-Ranges: bytes');
				
				if(readfile($filepath)){
					// log to db
					$timestamp_dt = new DateTime();
					$timestamp = $timestamp_dt->format("Y-m-d H:i:s");
					
					$q = "INSERT INTO `log_uploads` (`id`, `timestamp`, `ip_address`, `user_id`, `action`, `file_path`, `result`, `details`) VALUES (NULL, %s, %s, %i, 'view', %s, 'success', '');";
					$db->queryInsert($q, array($timestamp, $_SERVER['REMOTE_ADDR'], $session->userinfo['id'], $filepath));
				}
			}else{
				// log to db
				$timestamp_dt = new DateTime();
				$timestamp = $timestamp_dt->format("Y-m-d H:i:s");
				
				$q = "INSERT INTO `log_uploads` (`id`, `timestamp`, `ip_address`, `user_id`, `action`, `file_path`, `result`, `details`) VALUES (NULL, %s, %s, %i, 'view', %s, 'fail', 'File does not exist');";
				$db->queryInsert($q, array($timestamp, $_SERVER['REMOTE_ADDR'], $session->userinfo['id'], $filepath));
				
				
				die("<h1>File ($filepath) does not exist.</h1>");
			}
		}else{
			// log to db
			$timestamp_dt = new DateTime();
			$timestamp = $timestamp_dt->format("Y-m-d H:i:s");
			
			$q = "INSERT INTO `log_uploads` (`id`, `timestamp`, `ip_address`, `user_id`, `action`, `file_path`, `result`, `details`) VALUES (NULL, %s, %s, %i, 'view', %s, 'fail', %s);";
			$db->queryInsert($q, array($timestamp, $_SERVER['REMOTE_ADDR'], $session->userinfo['id'], $filepath, 'Invalid Token - Expected auth token of '.$auth.' but received challenge of '.$challenge.''));
			
			die("<h1>Unauthorized access by ".$session->userinfo['username']." (".$_SERVER['REMOTE_ADDR'].") has been logged.</h1>");
		}
	}
?>
