<?php
	// Allow/deny access to page based on security settings for user/groups
	$deny_message = "";  	// "" uses global constant - DENY_DEFAULT_MESSAGE
	// log all page loads
	
	$date = new DateTime();
	$datetime = $date->format("Y-m-d H:i:s");
	
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	
	// log page loads
	if((stripos(strtolower($_SERVER['SCRIPT_FILENAME']), "push_events") === false) && (stripos(strtolower($_SERVER['SCRIPT_FILENAME']), "lotto_betting") === false) && (stripos(strtolower($_SERVER['SCRIPT_FILENAME']), "ajax") === false)){
		$q = "INSERT INTO `log_page_loads`(`id`, `url`, `user_id`, `user_session_id`, `username`, `date_accessed`, `ip_address`) 
		VALUES 
		('NULL',%s,%i,%i,%s,%s,%s)";
		$url = str_ireplace($_SERVER['DOCUMENT_ROOT'], "", $_SERVER['SCRIPT_FILENAME']);
		$db->queryInsert($q, array($url, $session->userinfo['id'], $_SESSION['user_session_id'], $session->userinfo['username'], $datetime, $_SERVER['REMOTE_ADDR']));
	}
	
	$permissions = $core->permission_granted($_SERVER['SCRIPT_NAME']);
	
	$ajax_page = stripos(strtolower($_SERVER['SCRIPT_FILENAME']), "ajax") === false ? false : true;
	
	if($permissions['permission_granted'] === 0){
		if($ajax_page == true){
			// return json error
			if($deny_json_override != ""){
				echo $deny_json_override;
				die();
			}else{
				// build response based on config.php constants
				if(DENY_AJAX_RETURN_TYPE == "json"){
					$json_result = array();
					$json_result[DENY_AJAX_JSON_INDEX] = DENY_AJAX_JSON_VALUE;
					if($deny_message == ""){
						$json_result[DENY_AJAX_JSON_MESSAGE_INDEX] = DENY_DEFAULT_MESSAGE.$_SERVER['SCRIPT_NAME'];
					}else{
						$json_result[DENY_AJAX_JSON_MESSAGE_INDEX] = $deny_message.$_SERVER['SCRIPT_NAME'];
					}
					echo json_encode($json_result);
					die();
				}elseif(DENY_AJAX_RETURN_TYPE == "text"){
					echo DENY_DEFAULT_MESSAGE;
					die();
				}
			}
		}else{
			// either redirect to deny override page
			if($deny_html_override_url != ""){
				header("Location: ".$deny_html_override_url);
			}else{
				// build response based on config.php constants
				//header("Location: ".DENY_HTML_PAGE_URL);
				include($_SERVER['DOCUMENT_ROOT'].DENY_HTML_PAGE_URL);
				die();
			}
		}
	}elseif($permissions['permission_granted'] == -2){
		//die('<h2>ERROR - CONTENT NOT GLI APPROVED.</h2>');
	}