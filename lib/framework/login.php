<?php
	/*
	* Login.php
	*
	* This class is used to handle form submissions and
	* route them to the proper class/function 
	*
	*/

	require_once("../../config.php");

	class Process {
		/* Class constructor */
		function Process(){
			global $session;
			
			if(isset($_POST['sublogin'])){
				/* User submitted login form */
				$this->procLogin();
			}elseif(isset($_POST['sessionlog'])){
				$this->forcelogout();
				}			
			elseif(isset($_POST['subjoin'])){
				/* User submitted registration form */
				$this->procRegister();
			}elseif(isset($_POST['subforgot'])){
				/* User submitted forgot password form */
				$this->procForgotPass();
			}elseif(isset($_POST['subedit'])){
				/* User submitted edit account form */
				$this->procEditAccount();
			}elseif($session->logged_in){
				/* The only other reason to get here is to log out */
				$this->procLogout();
			}else{
			/* Should not get here, which means user is viewing this page by mistake */
				$protocol = 'http';
                                if($_SERVER["HTTPS"] == "on"){
                                        $protocol .= "s";
                                }

                                if($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443"){
                                        $pageURL = $protocol . "://".$_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
                                }else{
                                      	$pageURL = $protocol . "://".$_SERVER["SERVER_NAME"];
                                }

                                header("Location: ".$pageURL);
                                exit;

			}
		}
		
		
		
		function forcelogout(){
		
		global $db, $database ,$session ,$form;  //The database connection		
		
			if(!empty($_POST['user_id']) && !empty($_POST['reason'])){
		
			$id = $_POST['user_id'];
			$reason = $_POST['reason'];
			$temp = $session->login_check($_POST['user'], $_POST['pass'],'0');			
			}
			if($temp == 1){
			// end session in db
			$now_dt = new DateTime();
			$now = $now_dt->format('Y-m-d H:i:s');
			
			$q = "UPDATE `users` SET `userid`= '' WHERE `id`=".$id;				
			$limit = $db->queryOneRow($q);
			
			$q = "UPDATE `user_sessions` SET `end` = '".$now."', `is_active` = 0, `termination_reason` = '".$reason."' WHERE `id` = '".$id."';";
			$db->queryInsert($q);
			
			// end any other active sessions for this user_error
			$q = "UPDATE `user_sessions` SET `end` = '".$now."', `is_active` = 0, `termination_reason` = '".$reason."' WHERE `user_id` = '".$id."' AND `is_active` = 1;";
			$db->queryInsert($q);
			
			/*
			* Delete cookies - the time must be in the past,
			* so just negate what you added when creating the
			* cookie.
			*/
			if(isset($_COOKIE['cookname']) && isset($_COOKIE['cookid'])){
				setcookie("cookname", "", time()-60*60*24*COOKIE_EXPIRE, COOKIE_PATH);
				setcookie("cookid",   "", time()-60*60*24*COOKIE_EXPIRE, COOKIE_PATH);
			}

			/* Unset PHP session variables */
			unset($_SESSION['username']);
			unset($_SESSION['userid']);
			unset($_SESSION['flashMessage']);

			/* Reflect fact that user has logged out */
			$session->logged_in = false;

			/*
			* Remove from active users table and add to
			* active guests tables.
			*/
			$database->removeActiveUser($_POST['user']);
			
			$database->addActiveGuest($_SERVER['REMOTE_ADDR'], time());
			
			
			/* Set user level to guest */
			$session->username  = GUEST_NAME;
			$session->userlevel = GUEST_LEVEL;

			$this->procLogin();
		
		}
		else{
			$form->setError("user", "* Invalid Password.<br><br>");
                        //$form->getErrorArray();
			$_SESSION['error_array'] = $form->getErrorArray();
                        $_SESSION['value_array'] = $_POST;

                        header("Location: ".$_POST['referrer']);
		}
		
		}
		/*
		* procLogin - Processes the user submitted login form, if errors
		* are found, the user is redirected to correct the information,
		* if not, the user is effectively logged in to the system.
		*/
		function procLogin(){
	
			global $session, $form, $db, $core;
                  
                        $max_minues = LOCKED_MINUTES;
                        $max_login_attempt = MAX_LOGIN_ATTEMPTS; 
                        if(!empty($_POST['is_cashier'])){
                            $max_minues =  1;
                            $max_login_attempt = 15;
                        } else if(!empty($_POST['is_admin'])){
                             $max_minues =  15;
                             $max_login_attempt = 3;
                        } else {
                              $max_minues =  3;
                              $max_login_attempt = 3;
                        }

			
			// see if this account is banned
			$q = "SELECT * FROM `".TBL_BANNED_USERS."` WHERE `username`=%s";
			$banned_user = $db->queryOneRow($q, array($_POST['user']));
			if($banned_user['username']){
				// user is banned
				$_SESSION['value_array'] = $_POST;
				$msg = "We are sorry but due violations of our <a href='/terms_conditions.php'>Terms and Conditions</a> your account has been banned or deactivated. If you think this was done in error, please <a href='/locations.php'>contact us</a>. In the rare case that the issue can't be resolved you are free to contact the <a target='_blank' href='https://www.bahamas.gov.bs/wps/portal/public/gov/government/contacts/agencies/government%20corporations/gaming%20board/'>Bahamian Gaming Board</a>.";
				$form->setError("user", "* This account has been banned or deactivated.<br><br>".$msg);
				$_SESSION['error_array'] = $form->getErrorArray();
				
				$now_dt = new DateTime('now');
				$date = $now_dt->format("Y-m-d H:i:s");
				
				$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`, `user_agent`) VALUES (%s,%s,'0','account has been banned or deactivated',%s, %s)";
				$db->queryInsert($q, array($_POST['user'], $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
				
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
				die();
			}
			
			// see if this user is already logged in somewhere else
			if(strstr($_POST['user'], '@') !== false) {
				// if logging in with email address
				$q = "SELECT `username`,`id` FROM `".TBL_USERS."` WHERE `email`=%s";
				$user = $db->queryOneRow($q, array($_POST['user']));
				$_POST['user'] = $user['username'];
                                 $_POST['id'] = $user['id'];	
			}
			$q = "SELECT * FROM `".TBL_ACTIVE_USERS."` WHERE `username`=%s";
			$active_user = $db->queryOneRow($q, array($_POST['user']));
			if($active_user['username']){
				// user is already logged in
				$_SESSION['value_array'] = $_POST;
				$form->setError("account", "* This account is already logged in on another device. If you want to 
login now please enter you PIN or password again.");
				
				//echo $form->setUserID($user['id']);
				$uid = $db->queryOneRow("SELECT id FROM `users` where `username`='".$active_user['username']."'");
				
				//$form->setUserID($uid['id']);
				//$form->setUserName($active_user['username']);
				$_SESSION['logout_uid'] = $uid['id'];
				$_SESSION['logout_username'] = $active_user['username'];
				$_SESSION['error_array'] = $form->getErrorArray();
				
				$now_dt = new DateTime('now');
				$date = $now_dt->format("Y-m-d H:i:s");
				
				$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`, `user_agent`) VALUES (%s,%s,'0','attempt to login to an active users account from another device',%s, %s)";
				$db->queryInsert($q, array($_POST['user'], $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
				
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
				die();
			}			

			// see if we should force 2FA since its not enabled based on userlevel (ie. force admins to use 2FA)
			// could also force 2FA for /panel/ and /admin/ path if needed
			// make an exception for the admin user for now
			if($user_info['google_auth_enabled'] == 1){
				if((FORCE_2FA == 1) && ($user_info['userlevel'] >= FORCE_2FA_USER_LEVEL) && ($_POST['user'] != 'admin')){
					$_SESSION['force_google_auth'] = true;
					
					if(isset($_POST['referrer'])){
						header("Location: ".$_POST['referrer']);
						die();
					}else{
						header("Location: ".$session->referrer);
						die();
					}
				}
			}
			
			// get user info to see if this account has 2FA enabled or is locked out or deleted
			//$user_info = $db->queryOneRow("SELECT * FROM `".TBL_USERS."` WHERE `username`='".$_POST['user']."'");
			$q = "SELECT * FROM `".TBL_USERS."` WHERE `username`=%s";
			$user_info = $db->queryOneRow($q, array($_POST['user']));
                        $_POST['id'] = $user_info['id']; 
			
			if($user_info['deleted'] == 1){
				// account was deleted
				$_SESSION['value_array'] = $_POST;
				$msg = "We are sorry but due violations of our <a href='/terms_conditions.php'>Terms and Conditions</a> your account has been banned or deactivated. If you think this was done in error, please <a href='/locations.php'>contact us</a>. In the rare case that the issue can't be resolved you are free to contact the <a target='_blank' href='https://www.bahamas.gov.bs/wps/portal/public/gov/government/contacts/agencies/government%20corporations/gaming%20board/'>Bahamian Gaming Board</a>.";
				$form->setError("user", "* This account has been banned or deactivated.<br><br>".$msg);
				$_SESSION['error_array'] = $form->getErrorArray();
				
				$now_dt = new DateTime('now');
				$date = $now_dt->format("Y-m-d H:i:s");
				
				$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`,`user_agent`) VALUES (%s,%s,'0','account was previously deleted',%s,%s)";
				$db->queryInsert($q, array($_POST['user'], $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
				
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
				die();
			}
			
			if($user_info['locked'] == 1){
				// account is locked out, see if login attempt should be allowed yet
				$now_dt = new DateTime();
				$expiration_dt = new DateTime($user_info['locked_expiration']);
				
				$timeout_dt = $expiration_dt->diff($now_dt);
				//$timeout = $timeout_dt->format('%a days %h hours %i minutes %S seconds');
				$timeout = $core->format_interval($timeout_dt);
				
				if($now_dt > $expiration_dt){
					// reset login attempts and allow login attempt

					//$db->query("UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '0' WHERE `username`='".$_POST['user']."'");
					$q = "UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '0' WHERE `username`= %s";
					$db->query($q, array($_POST['user']));

					$user_info['failed_login_attempts'] = 0;
					// unlock account

					//$db->query("UPDATE `".TBL_USERS."` SET `locked` = '0' WHERE `username`='".$_POST['user']."'");
					$q = "UPDATE `".TBL_USERS."` SET `locked` = '0' WHERE `username`= %s";
					$db->query($q, array($_POST['user']));

					// reset lockout expiration

					//$db->query("UPDATE `".TBL_USERS."` SET `locked_expiration` = NULL WHERE `username`='".$_POST['user']."'");
					$q = "UPDATE `".TBL_USERS."` SET `locked_expiration` = NULL WHERE `username`= %s";
					$db->query($q, array($_POST['user']));

				}else{
					// display error to user and log this
					$_SESSION['value_array'] = $_POST;
					$msg = "If you think this was done in error, please <a href='/locations.php'>contact us</a>. In the rare case that the issue can't be resolved you are free to contact the <a target='_blank' href='https://www.bahamas.gov.bs/wps/portal/public/gov/government/contacts/agencies/government%20corporations/gaming%20board/'>Bahamian Gaming Board</a>.";
					$form->setError("pass", "* Account is locked out.<br><br>You must wait ".$timeout." before you can try again.<br><br>".$msg);
					$_SESSION['error_array'] = $form->getErrorArray();
					
					//Log Attempt
					$now_dt = new DateTime('now');
					$date = $now_dt->format("Y-m-d H:i:s");

					$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`,`user_agent`) VALUES (%s,%s,'0','account was already locked out',%s,%s)";
					$db->queryInsert($q, array($_POST['user'], $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
					
					if(isset($_POST['referrer'])){
						header("Location: ".$_POST['referrer']);
					}else{
						header("Location: ".$session->referrer);
					}
					die();
				}
			}
			
			if($user_info['google_auth_enabled'] == 1){
				// if so, perform an initial password check
				$retval = $session->login_check($_POST['user'], $_POST['pass'], isset($_POST['remember']));
				
				if($retval){
					/* Login check successful - prompt user for 2FA code */
					$_SESSION['value_array'] = $_POST;
					$_SESSION['google_auth_required'] = true;
					$_SESSION['google_auth_secret'] = $user_info['google_auth_secret'];
					
					if(isset($_POST['referrer'])){
						header("Location: ".$_POST['referrer']);
					}else{
						header("Location: ".$session->referrer);
					}
				}else{
					/* Login check failed */					
					$_SESSION['value_array'] = $_POST;
					$_SESSION['error_array'] = $form->getErrorArray();

					// log this and increase failed login attempts
					$num_failed = $user_info['failed_login_attempts'] + 1;

					//$db->query("UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '".$num_failed."' WHERE `username`='".$_POST['user']."'");
					$q = "UPDATE `".TBL_USERS."` SET `failed_login_attempts` = %i WHERE `username`= %s";
					$db->query($q, array($num_failed, $_POST['user']));

					//Log attempt
					$now_dt = new DateTime('now');
					$date = $now_dt->format("Y-m-d H:i:s");

					$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`,`user_agent`) VALUES (%s,%s,'0','bad username or password',%s,%s)";
					$db->queryInsert($q, array($_POST['user'], $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
					
					// lock account after X bad logins
					if($num_failed >= $max_login_attempt){

						//$db->query("UPDATE `".TBL_USERS."` SET `locked` = '1' WHERE `username`='".$_POST['user']."'");
						$q = "UPDATE `".TBL_USERS."` SET `locked` = '1' WHERE `username`= %s";
						$db->query($q, array($_POST['user']));

						// increase/set lockout timer
						$locked_minutes = $user_info['locked_minutes'] + $max_minues;


						//$db->query("UPDATE `".TBL_USERS."` SET `locked_minutes` = '".$locked_minutes."' WHERE `username`='".$_POST['user']."'");
						$q = "UPDATE `".TBL_USERS."` SET `locked_minutes` = %i WHERE `username`= %s";
						$db->query($q, array($locked_minutes, $_POST['user']));

						// set lockout expiration
						$now_dt = new DateTime();
						$expiration_dt = $now_dt->modify('+'.$locked_minutes.' minutes');
						$expiration = $expiration_dt->format("Y-m-d H:i:s");

						//$db->query("UPDATE `".TBL_USERS."` SET `locked_expiration` = '".$expiration."' WHERE `username`='".$_POST['user']."'");						
						$q = "UPDATE `".TBL_USERS."` SET `locked_expiration` = %s  WHERE `username`= %s";
						$db->query($q, array($expiration, $_POST['user']));

					}
					
					if(isset($_POST['referrer'])){
						header("Location: ".$_POST['referrer']);
					}else{
						header("Location: ".$session->referrer);
					}
				}
			}else{				
				// if not, do traditional login
				$retval = $session->login($_POST['user'], $_POST['pass'], isset($_POST['remember']));

                                if($retval && !empty($_POST['is_cashier'])){

                                   $t_ip_address = $db->queryOneRow("SELECT pl.ip_address,pu.location_change FROM panel_user pu INNER JOIN panel_location pl ON pl.id = pu.location_id WHERE pu.user_id = ".$_POST['id']);

                                   if($t_ip_address['location_change'] == 0 && !empty($t_ip_address['ip_address']) && ($t_ip_address['ip_address'] != $_SERVER['REMOTE_ADDR'])){
                                        $_SESSION['invalid_location'] = 1;
                                   }
                                }

				if($retval && empty($_SESSION['invalid_location'])){
					/* Login successful */
					
					//Log attempt
					$now_dt = new DateTime('now');
					$date = $now_dt->format("Y-m-d H:i:s");

					$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`,`user_agent`) VALUES (%s,%s,'1','successful login',%s,%s)";
					$db->queryInsert($q, array($_POST['user'], $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));

					// reset login attempts

					//$db->query("UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '0' WHERE `username`='".$_POST['user']."'");
					$q = "UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '0' WHERE `username`= %s";
					$db->query($q, array($_POST['user']));
					
					// unlock account
					
					//$db->query("UPDATE `".TBL_USERS."` SET `locked` = '0' WHERE `username`='".$_POST['user']."'");
					$q = "UPDATE `".TBL_USERS."` SET `locked` = '0' WHERE `username`= %s";
					$db->query($q, array($_POST['user']));

					// reset lockout expiration

					//$db->query("UPDATE `".TBL_USERS."` SET `locked_expiration` = NULL WHERE `username`='".$_POST['user']."'");
					$q = "UPDATE `".TBL_USERS."` SET `locked_expiration` = NULL WHERE `username`= %s";
					$db->query($q, array($_POST['user']));

					// reset timer

					//$db->query("UPDATE `".TBL_USERS."` SET `locked_minutes` = '0' WHERE `username`='".$_POST['user']."'");
					$q = "UPDATE `".TBL_USERS."` SET `locked_minutes` = '0' WHERE `username`= %s";
					$db->query($q, array($_POST['user']));

					if(isset($_POST['referrer'])){
						header("Location: ".$_POST['referrer']);
					}else{
						header("Location: ".$session->referrer);
					}
				}else{
					/* Login failed */
					$_SESSION['value_array'] = $_POST;
					$_SESSION['error_array'] = $form->getErrorArray();

					//Log attempt
					$now_dt = new DateTime('now');
					$date = $now_dt->format("Y-m-d H:i:s");

					$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`,`user_agent`) VALUES (%s,%s,'0','bad username or password',%s,%s)";
					$db->queryInsert($q, array($_POST['user'], $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
					
					// log this and increase failed login attempts
					$num_failed = $user_info['failed_login_attempts'] + 1;

					//$db->query("UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '".$num_failed."' WHERE `username`='".$_POST['user']."'");
					$q = "UPDATE `".TBL_USERS."` SET `failed_login_attempts` = %i WHERE `username`= %s";
					$db->query($q, array($num_failed, $_POST['user']));

					// lock account after X bad logins
					if($num_failed >= $max_login_attempt){

						//$db->query("UPDATE `".TBL_USERS."` SET `locked` = '1' WHERE `username`='".$_POST['user']."'");
						$q = "UPDATE `".TBL_USERS."` SET `locked` = '1' WHERE `username`= %s";
						$db->query($q, array($_POST['user']));

						// increase/set lockout timer
						$locked_minutes = $user_info['locked_minutes'] + $max_minues;

						//$db->query("UPDATE `".TBL_USERS."` SET `locked_minutes` = '".$locked_minutes."' WHERE `username`='".$_POST['user']."'");
						$q = "UPDATE `".TBL_USERS."` SET `locked_minutes` = %i WHERE `username`= %s";
						$db->query($q, array($locked_minutes, $_POST['user']));

						// set lockout expiration
						$now_dt = new DateTime();
						$expiration_dt = $now_dt->modify('+'.$locked_minutes.' minutes');
						$expiration = $expiration_dt->format("Y-m-d H:i:s");

						//$db->query("UPDATE `".TBL_USERS."` SET `locked_expiration` = '".$expiration."' WHERE `username`='".$_POST['user']."'");		
						$q = "UPDATE `".TBL_USERS."` SET `locked_expiration` = %s WHERE `username`= %s";
						$db->query($q, array($expiration, $_POST['user']));

					}
					
					if(isset($_POST['referrer'])){
						header("Location: ".$_POST['referrer']);
					}else{
						header("Location: ".$session->referrer);
					}
				}
			}
		}

		/*
		* procLogout - Simply attempts to log the user out of the system
		* given that there is no logout form to process.
		*/
		function procLogout(){
			global $database, $session;

			$retval = $session->logout();
			if(isset($_POST['referrer'])){
				header("Location: ".$_POST['referrer']);
			}else{
				$pageURL = 'http';
				if($_SERVER["HTTPS"] == "on"){
					$pageURL .= "s";
				}
				$pageURL .= "://";

				$path_arr = explode("/",$_SERVER["REQUEST_URI"]);

				if($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443"){
					$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"]."/".$path_arr[0];
				}else{
					$pageURL .= $_SERVER["SERVER_NAME"]."/".$path_arr[0];
				}

				header("Location: ".$pageURL);
			}
		}

		/*
		* procRegister - Processes the user submitted registration form,
		* if errors are found, the user is redirected to correct the
		* information, if not, the user is effectively registered with
		* the system and an email is (optionally) sent to the newly
		* created user.
		*/
		function procRegister(){
			global $database, $session, $form;
		
                        if(isset($_POST['csc_accountid'])){
				$_SESSION['csc_accountid'] = $_POST['csc_accountid'];
			}
			if(isset($_POST['csc_token'])){
                                $_SESSION['csc_token'] = $_POST['csc_token'];   
                        }	
			// fix for different form field naming conventions
			if(isset($_POST['password'])){
				$_POST['pass'] = $_POST['password'];
			}
			if(isset($_POST['username'])){
				$_POST['user'] = $_POST['username'];
			}

			$_SESSION['value_array'] = $_POST;

			/* Checks if registration is disabled */
			if(ACCOUNT_ACTIVATION == 4){
				$_SESSION['reguname'] = $_POST['user'];
				$_SESSION['regsuccess'] = 6;
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
			}

			/* Convert username to all lowercase */
			if(ALL_LOWERCASE){
				$_POST['user'] = strtolower($_POST['user']);
			}
			
			/* Hidden form field captcha deisgned to catch out auto-fill spambots */
			//if(!empty($_POST['killbill'])) { $retval = 2; } else {
				/* Registration attempt */
				$retval = $session->register("manual", $_POST['firstname'], $_POST['lastname'], $_POST['user'], $_POST['pass'], $_POST['conf_pass'], $_POST['email'], $_POST['conf_email'], $_POST['send_welcome_email'], $_POST['country']);
			//}
                           
				if($retval == 0){
				/* Registration Successful */		
				$_SESSION['reguname'] = $_POST['user'];
				$_SESSION['regsuccess'] = 0;
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
			}elseif($retval == 3){
				/* E-mail Activation */
				$_SESSION['reguname'] = $_POST['user'];
				$_SESSION['regsuccess'] = 3;
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
			}elseif($retval == 4){
				/* Admin Activation */
				$_SESSION['reguname'] = $_POST['user'];
				$_SESSION['regsuccess'] = 4;
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
			}elseif($retval == 5){
				/* No Activation Needed but E-mail going out */
				$_SESSION['reguname'] = $_POST['user'];
				$_SESSION['regsuccess'] = 5;
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
			}elseif($retval == 1){
				/* Error found with form */
				$_SESSION['value_array'] = $_POST;
				$_SESSION['error_array'] = $form->getErrorArray();
			//	print_r($_SESSION['error_array']);exit;
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
			}elseif($retval == 2){
				/* Registration attempt failed */
				$_SESSION['reguname'] = $_POST['user'];
				$_SESSION['regsuccess'] = 2;
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
			}
                   exit;
		}

		/*
		* procForgotPass - Validates the given username then if
		* everything is fine, a new password is generated and
		* emailed to the address the user gave on sign up.
		*/
		function procForgotPass(){
			global $database, $session, $mailer, $form;

			$subemail = $_POST['email'];

			/* Email error checking */
			$field = "email"; 
			if(!$subemail || strlen($subemail = trim($subemail)) == 0){
				$form->setError($field, "* Email not entered");
			}else{
				/* Check if valid email address using PHPs filter_var */
				if(!filter_var($subemail, FILTER_VALIDATE_EMAIL)){
					$form->setError($field, "* Email address entered is invalid");
				}
				$subemail = stripslashes($subemail);
			}

			if($database->emailTaken($subemail)){
				/* Get username linked to this email address */
				$subuser = $database->getUsernameFromEmail($subemail);
				
				$user_info = $database->getUserInfo($subuser);

				// see if account still needs to be activated
				if($user_info['userlevel'] < 3){
					$form->setError($field, "* The account linked to this email address has not been activated.<br>An activation link has been resent to this email address.");
					$mailer->sendActivation($subuser, $subemail, $pass, $user_info['actkey']);
				}else{
					if($user_info['accountType'] == 'ldap'){
						$form->setError($field, "* Passwords for LDAP accounts cannot be reset using this system.<br>Please contact your email provider or IT department.");
					}
				}
			}else{
				$form->setError($field, "* No account is registered with that email address");
			}			

			/* Errors exist, have user correct them */
			if($form->num_errors > 0){
				$_SESSION['value_array'] = $_POST;
				$_SESSION['error_array'] = $form->getErrorArray();

				/* Generate new password and email it to user */
			}else{
				/* Generate new password */
				$newpass = $session->generateRandStr(8);

				/* Attempt to send the email with new password */
				if($mailer->sendNewPass($subuser,$subemail,$newpass)){
					/* Email sent, update database */
					$usersalt = $session->generateRandStr(8);
					$newpass = sha1($usersalt.$newpass);
					$database->updateUserField($subuser,"password",$newpass);
					$database->updateUserField($subuser,"usersalt",$usersalt);

					// force user to change password on first login
					$database->updateUserField($subuser, "force_password_change", 1);

					$_SESSION['forgotpass'] = true;
				}else{
					/* Email failure, do not change password */
					$_SESSION['forgotpass'] = false;
				}
			}

			if(isset($_POST['referrer'])){
				header("Location: ".$_POST['referrer']."?forgotpass");
			}else{
				header("Location: ".$session->referrer."?forgotpass");
			}
		}

		/*
		* procEditAccount - Attempts to edit the user's account
		* information, including the password, which must be verified
		* before a change is made.
		*/
		function procEditAccount(){
			global $session, $form;
			/* Account edit attempt */
			$retval = $session->editAccount($_POST['curpass'], $_POST['newpass'], $_POST['conf_newpass'], $_POST['email']);

			if($retval){
				/* Account edit successful */
				$_SESSION['useredit'] = true;
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
			}else{
				/* Error found with form */
				$_SESSION['value_array'] = $_POST;
				$_SESSION['error_array'] = $form->getErrorArray();
				if(isset($_POST['referrer'])){
					header("Location: ".$_POST['referrer']);
				}else{
					header("Location: ".$session->referrer);
				}
			}
		}
	};

	/* Initialize process */
	$process = new Process;
?>
