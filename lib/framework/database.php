<?php
	/*
	* Database.php
	* 
	* The Database class is meant to simplify the task of accessing
	* information from the website's database.
	*
	*/

	class MySQLDB {
		public $connection;         // The MySQL database connection
		public $num_active_users;   // Number of active users viewing site
		public $num_active_guests;  // Number of active guests viewing site
		public $num_members;        // Number of signed-up users

		/* Class constructor */
		function MySQLDB(){ 
			/*
			* Only query database to find out number of members
			* when getNumMembers() is called for the first time,
			* until then, default value set.
			*/
			$this->num_members = -1;

			/* Calculate number of users at site */
			$this->calcNumActiveUsers();
				
			/* Calculate number of guests at site */
			$this->calcNumActiveGuests();
		}
		
		
		
		function uname($username){
			global $db;
			$q = "SELECT * FROM `".TBL_USERS."` WHERE username='$username'";
			$rows = $db->query($q);		
			$count = count($rows);
		
			if($count == 0){return 1;}		
		}
		
		function upass($username,$password){
			global $db;
			$q = "SELECT password,usersalt FROM `".TBL_USERS."` WHERE username='$username'";
			$rows = $db->query($q);	
			
			$password = stripslashes($password);
			
			echo $sqlpass = sha1($rows[0]['usersalt'].$password);
		
		
			if($rows[0]['password'] != $sqlpass){return 1;}		
		}

		/*
		* confirmUserPass - Checks whether or not the given username is in the database, 
		* if so it checks if the given password is the same password in the database
		* for that user. If the user doesn't exist or if the passwords don't match up, 
		* it returns an error code (1 or 2). On success it returns 0.
		*/
		function confirmUserPass($username, $password, $ldap_id = 20){
			global $db;
			$orguser = $username;
			$email = "";
			$username = strtolower($username);

			// Determine proper login method to use
			$login_type = "";

			// is this a manual login attempt with username
			$q = "SELECT * FROM `".TBL_USERS."` WHERE username='$orguser' AND accountType='manual'";
			$rows = $db->query($q);
			foreach($rows as $row){
				$login_type = "manual";
			}

			// is this a manual login attempt with email
			$q = "SELECT * FROM `".TBL_USERS."` WHERE email='$orguser' AND accountType='manual'";
			$rows = $db->query($q);
			foreach($rows as $row){
				$email = $orguser;
				$username = $row['username'];
				$login_type = "manual";
			}

			if($login_type == "manual"){
				/* Verify that manual user is in database */
				$q = "SELECT password, userlevel, usersalt FROM `".TBL_USERS."` WHERE username = '$username' AND accountType='manual'";
				$dbarray = $db->queryOneRow($q);

				/* Retrieve password and userlevel from result, strip slashes */
				$dbarray['password'] = stripslashes($dbarray['password']);
				$dbarray['userlevel'] = stripslashes($dbarray['userlevel']);
				$dbarray['usersalt'] = stripslashes($dbarray['usersalt']);
				$password = stripslashes($password);
				$sqlpass = sha1($dbarray['usersalt'].$password);

				/* Validate that password matches and check if userlevel is equal to 1 */
				if(($dbarray['password'] == $sqlpass)&&($dbarray['userlevel'] == 1)){
					return 3; //Indicates account has not been activated
				}

				/* Validate that password matches and check if userlevel is equal to 2 */
				if(($dbarray['password'] == $sqlpass)&&($dbarray['userlevel'] == 2)){
					return 4; //Indicates admin has not activated account
				}

				/* Validate that password is correct */
				if($dbarray['password'] == $sqlpass){
					//Success! Username and password confirmed
					if($orguser != $email){
						return -99; 
					}else{
						// need to let session know to lookup username
						return -98;
					}
				}else{
					return 2; //Indicates password failure
				}
			}

			// get LDAP Connection Variables from DB
			$q = "SELECT * FROM ldap_sources WHERE id=$ldap_id";
			$rows = $db->query($q);
			foreach($rows as $row){
				$ip = $row['ip_address'];
				$ldap_url = 'ldap://'.$ip;
				$ldap_domain = $row['domain'];
				$ldap_binduser = $row['bind_user'];
				$ldap_bindpass = $row['bind_pass'];
				$ldap_basedn = $row['base_dn'];
				$ldap_directory_type = $row['directory_type'];	//need to do something with this later
				//print_r($row); die();

				// NOTE: in Microsoft AD, group membership attribute is "memberOf", while Novell uses "groupMembership" attribute.

				$ldap_conn = ldap_connect($ldap_url);
				ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
				ldap_set_option($ldap_conn, LDAP_OPT_REFERRALS, 0);

				// bind using bind username and password
				$bind = ldap_bind($ldap_conn, "$ldap_binduser", $ldap_bindpass); 

				// convert email address to username
				if($ldap_id == 20){ //allow IU Exchange accounts to only user username instead of full email
					if(strpos($username, "@") !== false){
						$u_arr = explode("@", $username);
						$email = $username;
						$username = $u_arr[0];
					}else{
						$email = $username."@wiu.k12.pa.us";
					}
				}
			}

			if($bind){
				try{	
					$attributes = array("sn", "givenName", "userPrincipalName", "mail", "department", "sAMAccountName", "memberOf");
					$filter = "(&(objectCategory=person)(sAMAccountName=$username))";		
					$ldap_result = ldap_search($ldap_conn, $ldap_basedn, $filter, $attributes);				
					$entries = ldap_get_entries($ldap_conn, $ldap_result);
					//print_r($entries); die();

					if($entries["count"] > 0){
						// User Info
						$fname = addslashes($entries[0]['givenname'][0]);
						$lname = addslashes($entries[0]['sn'][0]);
						$email = addslashes($entries[0]['userprincipalname'][0]);
						// fix for @mail-wciu.local addresses
						if(!$email || strpos(strtolower($email), ".local")){
							$email = $entries[0]['mail'][0];
						}
	
						// alert user if email address is still invalid
						if(!$email || strpos(strtolower($email), ".local")){
							die("<font color='red'>The email address retrieved from LDAP is invalid ($email)!</font>");
						}

						$username = $entries[0]['samaccountname'][0];
						$dn = $entries[0]['dn'];

						// iterrate through all groups and record them
						for($m=0; $m < $numGroups; $m++){
							$groups[$m] = $entries[0]['memberof'][$m];
						}

						// if user was found, attempt to bind with DN and supplied password
						$login = ldap_bind($ldap_conn, "$dn", $password);

						//echo "<br>fname: $fname<br>lname: $lname<br>email: $email<br>user: $username<br>dn: $dn<br>pass: $password<br>";
						//die;

						if($login){
							// Success! Username and password confirmed

							/* Add slashes if necessary (for query) */
							if(!get_magic_quotes_gpc()) {
								$username = addslashes($username);
							}

							$token = $this->generateRandStr(16);
							$usersalt = $this->generateRandStr(8);	
	
							/* Verify that user is in database */
							$q = "SELECT password, userlevel, usersalt FROM ".TBL_USERS." WHERE username = '$email' AND accountType='ldap'";
							$rows = $db->query($q);
							$count = count($rows);

							//echo $q; die;

							if($count < 1){
								// user not in database... register them
								if($this->addNewUser("ldap", $fname, $lname, $email, $password, $email, $token, $usersalt)){

									// need to set district and iu id's based on ldap_id provided							

									/* Check Account activation setting and process accordingly. */

									if(EMAIL_WELCOME && ACCOUNT_ACTIVATION == 1 ){
										$mailer->sendWelcome($subuser,$subemail,$subpass);
									}else{
										/* No Activation Needed and NO E-mail going out */
										$successcode = 0;
									}
								}else{
									return 2;  //Registration attempt failed
								}
								ldap_unbind($ldap_conn);

								return 0; 
							}else{
								// update info in database from what we just obtained via ldap
								$time = time();
								$password = sha1($usersalt.$password);
								$userip = $_SERVER['REMOTE_ADDR'];

								$this->updateUserField($email, "firstname", $fname );
								$this->updateUserField($email, "lastname", $lname );
								$this->updateUserField($email, "password", $password );
								$this->updateUserField($email, "usersalt", $usersalt );
								$this->updateUserField($email, "actKey", $token );
								$this->updateUserField($email, "timestamp", $time );
								$this->updateUserField($email, "ip", $userip );
								$this->updateUserField($email, "ldap_id", $ldap_id );

								ldap_unbind($ldap_conn);
								return 0; 
							}			
							ldap_unbind($ldap_conn);
							return 0; 
						}else{
							// invalid ldap password.. it could be a manual account with same username
							// failed logging in using LDAP bind with provided credentials 
							// either incorrect credentials or manual account

							/* Verify that manual user is in database */
							$q = "SELECT password, userlevel, usersalt FROM ".TBL_USERS." WHERE username = '$orguser' AND accountType='manual'";
							$dbarray = $db->queryOneRow($q);
							$count = count($dbarray);

							if($count < 1){
								// username does not exist
								return 2;
							}

							/* Retrieve password and userlevel from result, strip slashes */
							$dbarray['password'] = stripslashes($dbarray['password']);
							$dbarray['userlevel'] = stripslashes($dbarray['userlevel']);
							$dbarray['usersalt'] = stripslashes($dbarray['usersalt']);
							$password = stripslashes($password);
							$sqlpass = sha1($dbarray['usersalt'].$password);
		
							/* Validate that password matches and check if userlevel is equal to 1 */
							if(($dbarray['password'] == $sqlpass)&&($dbarray['userlevel'] == 1)){
								return 3; //Indicates account has not been activated
							}

							/* Validate that password matches and check if userlevel is equal to 2 */
							if(($dbarray['password'] == $sqlpass)&&($dbarray['userlevel'] == 2)){
								return 4; //Indicates admin has not activated account
							}

							/* Validate that password is correct */
							if($dbarray['password'] == $sqlpass){
								return -99; //Success! Username and password confirmed
							}else{
								return 2; //Indicates password failure
							}

							// invalid username
							return 1;
						}
					}else{
						// failed logging in using LDAP bind with provided credentials 
						// either incorrect credentials or manual account

						/* Verify that manual user is in database */
						$q = "SELECT password, userlevel, usersalt FROM ".TBL_USERS." WHERE username = '$orguser' AND accountType='manual'";
						$dbarray = $db->queryOneRow($q);
						$count = count($dbarray);

						if($count < 1){
							// username does not exist
							return 2;
						}

						/* Retrieve password and userlevel from result, strip slashes */
						$dbarray['password'] = stripslashes($dbarray['password']);
						$dbarray['userlevel'] = stripslashes($dbarray['userlevel']);
						$dbarray['usersalt'] = stripslashes($dbarray['usersalt']);
						$password = stripslashes($password);

						$sqlpass = sha1($dbarray['usersalt'].$password);

						/* Validate that password matches and check if userlevel is equal to 1 */
						if(($dbarray['password'] == $sqlpass)&&($dbarray['userlevel'] == 1)){
							return 3; //Indicates account has not been activated
						}

						/* Validate that password matches and check if userlevel is equal to 2 */
						if(($dbarray['password'] == $sqlpass)&&($dbarray['userlevel'] == 2)){
							return 4; //Indicates admin has not activated account
						}

						/* Validate that password is correct */
						if($dbarray['password'] == $sqlpass){
							return -99; //Success! Username and password confirmed
						}else{
							return 2; //Indicates password failure
						}

						// invalid username
						return 1;
					}
				}catch(Exception $e){
					ldap_unbind($ldap_conn);
					return 1;
				}
			}else{			
				// LDAP bind with bind user credentials failed -- server is probably down or 
				// try using cached LDAP credentials
	
				//need to return a code to show a warning that server is down.

				/* Verify that ldap user is in database */
				$q = "SELECT password, userlevel, usersalt FROM ".TBL_USERS." WHERE username = '$email' AND accountType='ldap' AND ldap_id=$ldap_id";
				$dbarray = $db->queryOneRow($q);
				$count = count($dbarray);

				if($count < 1){
					// couldn't find an ldap account cached, so try manual account

					/* Verify that manual user is in database */
					$q = "SELECT password, userlevel, usersalt FROM ".TBL_USERS." WHERE username = '$orguser' AND accountType='manual'";
					$dbarray = $db->queryOneRow($q);
					$count = count($dbarray);

					if(!$sql || $count < 1){
						// username does not exist
						return 2;
					}

					/* Retrieve password and userlevel from result, strip slashes */
					$dbarray['password'] = stripslashes($dbarray['password']);
					$dbarray['userlevel'] = stripslashes($dbarray['userlevel']);
					$dbarray['usersalt'] = stripslashes($dbarray['usersalt']);
					$password = stripslashes($password);

					$sqlpass = sha1($dbarray['usersalt'].$password);

					/* Validate that password is correct */
					if($dbarray['password'] == $sqlpass){
						return -99; //Success! Username and password confirmed
					}else{
						return 2; //Indicates password failure
					}

					// invalid username
					return 1;
				}

				/* Retrieve password and userlevel from result, strip slashes */
				$dbarray['password'] = stripslashes($dbarray['password']);
				$dbarray['userlevel'] = stripslashes($dbarray['userlevel']);
				$dbarray['usersalt'] = stripslashes($dbarray['usersalt']);
				$password = stripslashes($password);

				$sqlpass = sha1($dbarray['usersalt'].$password);

				/* Validate that password matches and check if userlevel is equal to 1 */
				if(($dbarray['password'] == $sqlpass)&&($dbarray['userlevel'] == 1)){
					//return 3; //Indicates account has not been activated
				}
		
				/* Validate that password matches and check if userlevel is equal to 2 */
				if(($dbarray['password'] == $sqlpass)&&($dbarray['userlevel'] == 2)){
					//return 4; //Indicates admin has not activated account
				}

				/* Validate that password is correct */
				if($dbarray['password'] == $sqlpass){
					return 0; //Success! Username and password confirmed
				}else{
					return 2; //Indicates password failure
				}

				// invalid password
				return 2;	
			}
		}

		/*
		* confirmUserID - Checks whether or not the given username is in the database, 
		* if so it checks if the given userid is the same userid in the database
		* for that user. If the user doesn't exist or if the userids don't match up, 
		* it returns an error code (1 or 2). On success it returns 0.
		*/
		function confirmUserID($username, $userid){
			global $db;
			
			/* Add slashes if necessary (for query) */
			if(!get_magic_quotes_gpc()) {
				$username = addslashes($username);
			}

			/* Verify that user is in database */
			$q = "SELECT userid FROM ".TBL_USERS." WHERE username = '$username'";
			$dbarray = $db->queryOneRow($q);
			$count = count($dbarray);

			if($count < 1){
				return 1; //Indicates username failure
			}

			/* Retrieve userid from result, strip slashes */
			$dbarray['userid'] = stripslashes($dbarray['userid']);
			$userid = stripslashes($userid);

			/* Validate that userid is correct */
			if($userid == $dbarray['userid']){
				return 0; //Success! Username and userid confirmed
			}else{
				return 2; //Indicates userid invalid
			}
		}

		/*
		* usernameTaken - Returns true if the username has been taken by another user, false otherwise.
		*/
		function usernameTaken($username){
			global $db;
			if(!get_magic_quotes_gpc()){
				$username = addslashes($username);
			}
			$result = $db->query("SELECT username FROM ".TBL_USERS." WHERE username = '$username'");
			$count = count($result);
			return ($count > 0);
		}

		/*
		* emailTaken - Returns true if the username has been taken by another user, false otherwise.
		*/
		function emailTaken($email){
			global $db;
			if(!get_magic_quotes_gpc()){
				$email = addslashes($email);
			}
			$result = $db->query("SELECT email FROM ".TBL_USERS." WHERE email = '$email'");
			$count = count($result);    
			return ($count > 0);
		}   

		/*
		* usernameBanned - Returns true if the username has been banned by the administrator.
		*/
		function usernameBanned($username){
			global $db;
			if(!get_magic_quotes_gpc()){
				$username = addslashes($username);
			}
			$result = $db->query("SELECT username FROM ".TBL_BANNED_USERS." WHERE username = '$username'");
			$count = count($result);    
			return ($count > 0);
		}

		/*
		* addNewUser - Inserts the given (username, password, email) info into the database. 
		* Appropriate user level is set. Returns true on success, false otherwise.
		*/
		function addNewUser($accountType, $firstname, $lastname, $username, $password, $email, $token, $usersalt){
			global $db;
			$time = time();

			/* If admin sign up, give admin user level */
			if(strcasecmp($username, ADMIN_NAME) == 0){
				$ulevel = ADMIN_LEVEL;
				/* Which validation is on? */
			}elseif(ACCOUNT_ACTIVATION == 1){
				$ulevel = REGUSER_LEVEL; /* No activation required */
			}elseif(ACCOUNT_ACTIVATION == 2){
				$ulevel = ACT_EMAIL; /* Activation e-mail will be sent */
			}elseif(ACCOUNT_ACTIVATION == 3){
				$ulevel = ADMIN_ACT; /* Admin will activate account */   
			}

			// if LDAP user, set to no activation required
			if($accountType == "ldap"){
				$ulevel = REGUSER_LEVEL; /* No activation required */
			}

			$password = sha1($usersalt.$password);
			$userip = $_SERVER['REMOTE_ADDR'];

			$query = "INSERT INTO `".TBL_USERS."` (`username`, `accountType`, `firstname`, `lastname`, `password`, `usersalt`, `userid`, `userlevel`, `email`, `timestamp`, `actkey`, `ip`, `regdate`) VALUES ('$username', '$accountType', '$firstname', '$lastname', '$password', '$usersalt', '0', '$ulevel', '$email', '$time', '$token', '$userip', '$time');";
			$_SESSION['user_id'] = $db->queryInsert($query);
			
			if($_SESSION['user_id'] > 0){
				return true;
			}else{
				return false;
			}
		}

		/*
		* updateUserField - Updates a field, specified by the field
		* parameter, in the user's row of the database.
		*/
		function updateUserField($username, $field, $value, $user_id = -1){
			global $db;
			if($user_id != -1){
				$result = $db->queryDirect("UPDATE ".TBL_USERS." SET `".$field."` = '$value' WHERE `id` = '$user_id'");
			}else{
				$result = $db->queryDirect("UPDATE ".TBL_USERS." SET `".$field."` = '$value' WHERE `username` = '$username'");
			}
			return $result;
		}

		/*
		* getUserInfo - Returns the result array from a mysql
		* query asking for all information stored regarding
		* the given username. If query fails, NULL is returned.
		*/
		function getUserInfo($username, $force_update = false){
			global $db;
			global $session;
			global $USER_INFO_TABLES;
			
			$dbarray = $db->queryOneRow("SELECT * FROM ".TBL_USERS." WHERE username = '$username'");
			/* Error occurred, return given name by default */
			$result = count($dbarray);
			if(!$dbarray || $result < 1){
				return NULL;
			}
			
			if(!empty($USER_INFO_TABLES)){
				foreach($USER_INFO_TABLES as $user_table){
					$dbarray[$user_table] = $db->queryOneRow("SELECT * FROM ".$user_table." WHERE user_id = '".$dbarray['id']."'");
					/* Error occurred, return given name by default */
					$result = count($dbarray);
					if(!$dbarray || $result < 1){
						return NULL;
					}
				}
			}
			
			if($force_update == true){
				$session->userinfo = $dbarray;
			}
			
			/* Return result array */
			return $dbarray;
		}

		/*
		* generateRandStr - Generates a string made up of randomized
		* letters (lower and upper case) and digits, the length
		* is a specified parameter.
		*/
		function generateRandStr($length){
			$randstr = "";
			for($i=0; $i<$length; $i++){
				$randnum = mt_rand(0,61);
				if($randnum < 10){
					$randstr .= chr($randnum+48);
				}elseif($randnum < 36){
					$randstr .= chr($randnum+55);
				}else{
					$randstr .= chr($randnum+61);
				}
			}
			return $randstr;
		}

		/*
		* checkUserEmailMatch - Checks whether username
		* and email match in forget password form.
		*/
		function checkUserEmailMatch($username, $email){
			global $db;
			$rows = $db->query("SELECT username FROM ".TBL_USERS." WHERE username = '$username' AND email = '$email'");  
			$number_of_rows = count($rows);

			if($number_of_rows < 1){
				return 0;
			}else{
				return 1;
			}
		}

		/*
		* getUsernameFromEmail
		*/
		function getUsernameFromEmail($email){
			global $db;
			$row = $db->query("SELECT username FROM ".TBL_USERS." WHERE email = '$email' LIMIT 0,1");  
			$number_of_rows = count($row);

			if($number_of_rows < 1){
				return false;
			}else{
				return $row[0]['username'];
			}
		}

		/*
		* getNumMembers - Returns the number of signed-up users
		* of the website, banned members not included. The first
		* time the function is called on page load, the database
		* is queried, on subsequent calls, the stored result
		* is returned. This is to improve efficiency, effectively
		* not querying the database when no call is made.
		*/
		function getNumMembers(){
			global $db;
			if($this->num_members < 0){
				$rows = $db->query("SELECT username FROM ".TBL_USERS);
				$this->num_members = count($rows); 
			}
			return $this->num_members;
		}

		/*
		* calcNumActiveUsers - Finds out how many active users
		* are viewing site and sets class variable accordingly.
		*/
		function calcNumActiveUsers(){
			global $db;
			/* Calculate number of USERS at site */
			$rows = $db->query("SELECT * FROM ".TBL_ACTIVE_USERS);
			$this->num_active_users = count($rows);
		}

		/*
		* calcNumActiveGuests - Finds out how many active guests
		* are viewing site and sets class variable accordingly.
		*/
		function calcNumActiveGuests(){
			global $db;
			
			/* Calculate number of GUESTS at site */
			$rows = $db->query("SELECT * FROM ".TBL_ACTIVE_GUESTS);
			$this->num_active_guests = count($rows);    
		}

		/*
		* addActiveUser - Updates username's last active timestamp
		* in the database, and also adds him to the table of
		* active users, or updates timestamp if already there.
		*/
		function addActiveUser($username, $time){
			global $db;
			if((stripos(strtolower($_SERVER['SCRIPT_FILENAME']), "push_events") === false) && (stripos(strtolower($_SERVER['SCRIPT_FILENAME']), "lotto_betting") === false) && (stripos(strtolower($_SERVER['SCRIPT_FILENAME']), "ajax") === false)){
				$user_info = $db->queryOneRow("SELECT * FROM ".TBL_USERS." WHERE username = '$username'");

				$now_dt = new DateTime();
				$now = $now_dt->format("Y-m-d H:i:s");
				
				// log user's ip & user agent string
				$db->query("UPDATE ".TBL_USERS." SET ip = '".$_SERVER['REMOTE_ADDR']."' WHERE username = '$username'");
				
				// track user's devices
				$device_id = $db->queryInsert("INSERT INTO `user_devices` (`user_id`, `ip_address`, `description`, `browser_agent`, `creation`) VALUES ('".$user_info['id']."', '".$_SERVER['REMOTE_ADDR']."', '', '".$_SERVER['HTTP_USER_AGENT']."', '".$now."');");
				if($device_id > 0){
					// new device so add it
					$q = "INSERT INTO `user_session_devices` (`user_session_id`, `user_device_id`) VALUES ('".$_SESSION['user_session_id']."', '".$device_id."');";
					$db->queryInsert($q);
				}
				
				// update last login and timestamp
				$db->query("UPDATE ".TBL_USERS." SET last_login = '$now' WHERE username = '$username'");
				$db->query("UPDATE ".TBL_USERS." SET timestamp = '$time' WHERE username = '$username'");
				$db->query("REPLACE INTO ".TBL_ACTIVE_USERS." VALUES ('$username', '$time')");
				$this->calcNumActiveUsers();
			}
		}

		/* addActiveGuest - Adds guest to active guests table */
		function addActiveGuest($ip, $time){
			global $db;

			if(stripos(strtolower($_SERVER['SCRIPT_FILENAME']), "push_events") === false){
				$db->query("REPLACE INTO ".TBL_ACTIVE_GUESTS." VALUES ('$ip', '$time')");
				$this->calcNumActiveGuests();
			}
		}

		/* These functions are self explanatory, no need for comments */

		/* removeActiveUser */
		function removeActiveUser($username){
			global $db;

			$db->query("DELETE FROM ".TBL_ACTIVE_USERS." WHERE username = '$username'");
			$this->calcNumActiveUsers();
		}

		/* removeActiveGuest */
		function removeActiveGuest($ip){
			global $db;

			$db->query("DELETE FROM ".TBL_ACTIVE_GUESTS." WHERE ip = '$ip'");
			$this->calcNumActiveGuests();
		}

		/* removeInactiveUsers */
		function removeInactiveUsers(){
			global $db;

			$timeout = time()-USER_TIMEOUT*60;
			$db->query("DELETE FROM ".TBL_ACTIVE_USERS." WHERE timestamp < $timeout");
			$this->calcNumActiveUsers();
		}

		/* removeInactiveGuests */
		function removeInactiveGuests(){
			global $db;

			$timeout = time()-GUEST_TIMEOUT*60;
			$db->query("DELETE FROM ".TBL_ACTIVE_GUESTS." WHERE timestamp < $timeout");
			$this->calcNumActiveGuests();
		}
	};

	/* Create database connection */
	$database = new MySQLDB;
?>
