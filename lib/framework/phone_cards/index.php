<?php
	error_reporting(E_ERROR);
	ini_set('display_errors', 1);

	class phone_card {
		public $username = "";
		public $password = "";
		private $url = "http://seamlessapitester.sbtech.com/";

		private function parse_between($string, $start_str, $end_str){
			$string = " ".$string;
			$ini = strpos($string, $start_str);
			if ($ini == 0) return "";
			$ini += strlen($start_str);
			$len = strpos($string, $end_str, $ini) - $ini;
			return substr($string, $ini, $len);
		}
		
		private function parse_after($string, $search_str){
			$end_pos = strlen($string)+1;
			$string = " ".$string;
			$start_pos = strpos($string, $search_str);
			if ($start_pos == 0) return "";
			$start_pos += strlen($search_str);
			$len = $end_pos - $start_pos;
			return substr($string, $start_pos, $len);
		}
		
		private function parse_before($string, $search_str){
			$start_pos = 1;
			$string = " ".$string;
			$end_pos = strpos($string, $search_str);
			if ($end_pos == 0) return "";
			$len = $end_pos - $start_pos;
			return substr($string, $start_pos, $len);
		}		
		
		public function test_connection(){
			return true;
		}

		public function send_sToken(){
			echo "test_token";
		}
		
		public function login(){
			// if not already logged in, do so
			$post_data = 'user=' . $this->username . '&pass=' . $this->password;

			$ch = curl_init($this->urls["login"]);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$result = curl_exec($ch);
			//print_r(curl_getinfo($ch));
			curl_close($ch);
				
			// get cookies
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $m); 
			$temp = $m[1];
			foreach($temp as $temp_cookie){
				// convert to indexed array
				$cook_arr = explode("=",$temp_cookie);
				$this->cookies[$cook_arr[0]] = $cook_arr[1];
				
				// set cookies locally
				//$expire=time()+60*60*24*30;
				//setcookie($cook_arr[0], $cook_arr[1], $expire);
			}
		}
		
		public function logout(){
			if(isset($this->cookies['mcx_key'])){
				$url = $this->urls["logout"]."&sk=".$this->cookies['mcx_key'];
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_HEADER, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

				$data = curl_exec($ch);
				$curl_info = curl_getinfo($ch);
				curl_close($ch);
			}
			$this->cookies = array();
		}
		
		public function add_trade($buy_sell, $currency, $amount, $price){
			$buy_sell = $buy_sell == "buy" ? "buy=1" : "buy=0";
			$url = $this->urls["add_trade"]
					//."&sk=".$this->cookies['mcx_key']
					."&cur=".$currency
					."&amt=".$amount
					."&price=".$price
					."&".$buy_sell
					."&enabled=1";
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				return true;
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}
		
		public function cancel_trade($currency, $order_id){
			$url = $this->urls["cancel_trade"]
					//."&sk=".$this->cookies['mcx_key']
					."&cur=".$currency
					."&id=".$order_id
					."&enabled=1";
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				return true;
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}		
		
		public function get_orders($currency, $order_type){
			$url = $this->get_orders_url()
					//."?sk=".$this->cookies['mcx_key']
					."&cur=".$currency;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);
				
				if($order_type == "rich"){
					return $result["rich"]["r"];
				}elseif($order_type == "buy"){
					return $result["buy"]["o"];
				}elseif($order_type == "sell"){
					return $result["sell"]["o"];
				}elseif($order_type == "history"){
					return $result["history"]["o"];
				}
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}
		
		public function get_orders_url(){
			// will attempt to use "pro orders" page and fall back to "orders" if not available/allowed
			$url = $this->urls["orders_pro"]
					."?sk=".$this->cookies['mcx_key']
					."&cur=WDC"; // just pick a random coin here as a test
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){			
				return $this->urls["orders_pro"];
			}else{
				return $this->urls["orders"];
			}
		}
		
		public function get_user_info(){
			$url = $this->urls["user_info"];
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);
				
				return $result;
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}

		public function get_user_order_log(){
			$url = $this->urls["user_info"];
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);
				
				return $result['cur'];
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}
		
		public function get_last_buy_price($currency){
			$url = $this->urls["user_info"]
					."?sk=".$this->cookies['mcx_key']
					."&cur=".$currency;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);
				
				foreach($result['cur'] as $cur){
					if($cur['tla'] == $currency){
						foreach ($cur['log'] as $log_entry){
							/*
								[d] => Exchanged for 0.001BTC at 0.00068BTC each
								[a] => 1.46764707
							*/
							if(strpos(strtolower($log_entry['d']), "exchanged") !== false){
								if($log_entry['a'] > 0){
									// positive number means we bought this currency
									// now, find the price in the desc. string
									$price = $this->parse_between($log_entry['d'], " at ", "BTC each");
									
									return $price;
								}
							}
						}
					}
				}
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}
		
		public function get_dividends(){
			$payment = array();
		
			$url = $this->urls["user_info"];
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);	
				
				foreach($result['cur'] as $cur){
					$currency = $cur['tla'];
					foreach ($cur['log'] as $log_entry){
						// log feeshare payments
						if(strpos(strtolower(" ".$log_entry['d']), "feeshare payment") !== false){
							if($log_entry['a'] > 0){							
								// find the number of shares, amount, and timestamp
								$qty = $this->parse_between($log_entry['d'], "for having ", "MCX");
								$amt = $log_entry['a'];
								$timestamp = $log_entry['t'];
								
								// get payout for 1 share in each coins value
								$payment['mcxfee'][$timestamp][$currency]['amount'] = $amt / $qty;
							}
						// log interest payouts
						}elseif(strpos(strtolower(" ".$log_entry['d']), "interest payment") !== false){
							if($log_entry['a'] > 0){						
								// find the number of shares, amount, and timestamp
								$bal = $log_entry['b'];
								$amt = $log_entry['a'];
								
								// determine quantity of coin that interest was collected on
								$qty = $bal - $amt;
								//$qty = $mcx->parse_after($log_entry['d'], "deposit of "); // alternate method
								
								$timestamp = $log_entry['t'];
								
								// get interest per 1 coin
								$payment['interest'][$timestamp][$currency]['amount'] = $amt / $qty;
							}
						}
					}
				}
				
				return $payment;
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}

		public function get_last_mcxfee_payment(){
			$payment = array();
			$tot = 0;
		
			$url = $this->urls["user_info"];
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);
				
				foreach($this->currencies as $currency => $currency_name){
					$qty = 0;
					$amt = 0;
					foreach($result['cur'] as $cur){
						if($cur['tla'] == $currency){
							foreach ($cur['log'] as $log_entry){
								/*
									[t] => 1383782400
									[d] => Feeshare payment for having 18MCX
									[a] => 0.00016598
								*/
								if(strpos(strtolower(" ".$log_entry['d']), "feeshare payment") !== false){
									if($log_entry['a'] > 0){							
										// find the number of shares, amount, and timestamp
										$qty = $this->parse_between($log_entry['d'], "for having ", "MCX");
										$amt = $log_entry['a'];
										$timestamp = $log_entry['t'];
											
										// get the exchange rate of this coin during this time period
										if($currency != 'BTC'){
											$exchange_rate = $this->get_exchange_rate($currency, $timestamp);
										}else{
											$exchange_rate = 1;
											$payment['timestamp'] = $timestamp;
										}
										$payment[$currency]['exchange'] = $exchange_rate;
										
										// get payout for 1 share in each coins value
										$payment[$currency]['amount'] = $amt / $qty;
										
										// determine the btc value of this coin's payout
										$payment[$currency]['btc_value'] = $exchange_rate * $payment[$currency]['amount'];
										
										// add to total payout value
										$tot += $payment[$currency]['btc_value'];
										
										break;
									}
								}
							}
						}
					}
				}
				// get price of MCX at this time
				$payment['mcx_price'] = $this->get_exchange_rate('MCX', $payment['timestamp']);
				
				// add total feeshare payout to array
				$payment['pay_total'] = $tot;
				return $payment;
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}
		
		public function get_last_interest_payment($currency){
			// not valid for MCX, so exit
			if($currency == 'MCX'){
				return false;
			}
		
			$payment = array();
			$tot = 0;
		
			$url = $this->urls["user_info"];
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);
				
				$qty = 0;
				$amt = 0;
				foreach($result['cur'] as $cur){
					if($cur['tla'] == $currency){
						foreach ($cur['log'] as $log_entry){
							/*
								[t] => 1383782400
								[d] => Interest Payment on deposit of 0.61007835
								[a] => 0.00000668
								[b] => 0.61008503
							*/
							if(strpos(strtolower(" ".$log_entry['d']), "interest payment") !== false){
								if($log_entry['a'] > 0){						
									// find the number of shares, amount, and timestamp
									$bal = $log_entry['b'];
									$amt = $log_entry['a'];
									
									// determine quantity of coin that interest was collected on
									$qty = $bal - $amt;
									//$qty = $this->parse_after($log_entry['d'], "deposit of "); // alternate method
									
									$timestamp = $log_entry['t'];
									$payment['timestamp'] = $timestamp;
									
									// get the exchange rate of this coin during this time period
									if($currency != 'BTC'){
										$exchange_rate = $this->get_exchange_rate($currency, $timestamp);
									}else{
										$exchange_rate = 1;
									}
									$payment[$currency]['exchange'] = $exchange_rate;
									
									// get interest per 1 coin
									$payment[$currency]['amount'] = $amt / $qty;
									
									// determine the btc value of this coin's payout
									$payment[$currency]['btc_value'] = $exchange_rate * $payment[$currency]['amount'];
									
									break;
								}
							}
						}
					}
				}

				return $payment;
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}		

		public function get_trade_history($currency){
			$url = $this->get_orders_url()
					."?sk=".$this->cookies['mcx_key']
					."&cur=".$currency;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);
				
				return $result['history']['o'];
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}
		
		public function get_exchange_rate($currency, $timestamp){
			$url = $this->get_orders_url()
					."?sk=".$this->cookies['mcx_key']
					."&cur=".$currency;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);
				
				// find exchange rate closest to the timestamp we are looking for
				$time_diff = 99999999999;
				$exchange_rate = 0;
				foreach($result['history']['o'] as $hist){
					if(abs($hist['t'] - $timestamp) < $time_diff){
						$time_diff = abs($hist['t'] - $timestamp);
						$exchange_rate = $hist['p'];
					}
					// if we get within 10 seconds away, exit loop to improve performance
					if($time_diff <= 10000){
						break;
					}
				}
				return $exchange_rate;
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}
		
		public function current_price($currency){
			$url = $this->get_orders_url()
					."?sk=".$this->cookies['mcx_key']
					."&cur=".$currency;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_COOKIE, $this->get_cookie_str());
			curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$data = curl_exec($ch);
			$curl_info = curl_getinfo($ch);
			curl_close($ch);
			
			if($curl_info['http_code'] == 200){
				$xml = new SimpleXMLElement($data);
				$result = json_decode(json_encode($xml), true);
				
				$prices["buy"] = $result["buy"]["o"][0]["p"];
				$prices["sell"] = $result["sell"]["o"][0]["p"];
				
				return $prices;
			}else{
				// error retrieving data
				echo "Error ".$curl_info['http_code']." - Could not connect to: ".$curl_info['url'];
				return false;
			}
		}
	}
?>