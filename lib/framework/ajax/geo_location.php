<?php
	date_default_timezone_set('America/New_York');

	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	$results = array();
	
	if(isset($_POST['action'])){
		if($_POST['action'] = 'block'){
			// kill active session
			$session->logout($_POST['action_reason']);
		}
	}
	
	// record this check in the db
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	$q = "INSERT INTO `user_geolocation_info` (`id`, `user_id`, `user_session_id`, `date_time`, `city`, `region`, `country_code`, `country_name`, `zipcode`, `hostname`, `ip`, `latitude`, `longitude`, `organization`, `map_img_url`) VALUES (NULL, %i, %i, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);";
	$insert_id = $db->queryInsert($q, array($session->userinfo['id'], $_SESSION['user_session_id'], $now, $_POST['city'], $_POST['region'], $_POST['country_code'], $_POST['country_name'], $_POST['zipcode'], $_POST['hostname'], $_POST['ip'], $_POST['latitude'], $_POST['longitude'], $_POST['organization'], $_POST['map_img_url']));
	if($insert_id > 0){
		$results['success'] = 'true';
	}else{
		$results['success'] = 'false';
	}
	
	// see if country should be permitted
	if(empty($_POST['country_check']) &&  strtolower($_POST['country_name']) != 'bahamas'){
		if(strtolower($_POST['country_code']) != 'bs'){
			// kill active session
			$results['error'] = 'GEOLOCATION ERROR: Location ('.$_POST['country_name'].' - '.$_POST['country_code'].') is not permitted';
			$results['success'] = 'false';
		}
	}
	
	echo json_encode($results);
