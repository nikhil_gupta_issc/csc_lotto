<?php
	// Time-based One-Time Password (TOTP) Google Auth 2FA

	date_default_timezone_set('America/New_York');

	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/lib/assets/GoogleAuthenticator/PHPGangsta/GoogleAuthenticator.php');
	
	$ga = new PHPGangsta_GoogleAuthenticator();
	
	$clock_tolerance = 2; // allow time to be off by 1 minute (2*30sec clock timeouts)
	
	if($_POST['action'] == "enable"){
		// validate user's password before allowing them to change 2FA settings
		if(sha1($session->userinfo['usersalt'].$_POST['password']) == $session->userinfo['password']){
			$now_dt = new DateTime();
			$now = $now_dt->format("Y-m-d H:i:s");

			if($ga->verifyCode($_POST['secret'], $_POST['code'], $clock_tolerance)){
				// passed, so store secret key in database
				$q = "UPDATE `users` SET `google_auth_secret` = '".$_POST['secret']."', `google_auth_enabled` = 1 WHERE id=".$session->userinfo['id'];
				if($db->queryDirect($q)){
					$_SESSION['google_auth_secret'] = "VALID";
					$_SESSION['force_google_auth'] = 0;
					unset($_SESSION['value_array']);
					
					echo 'SUCCESS';
				}else{
					echo '<span style="color:red;">ERROR: Could not save 2FA settings to the database</span><br>';
				}
			}else{
				echo '<span style="color:red;">ERROR: Invalid 2FA Code</span><br>';
			}
		}else{
			echo '<span style="color:red;">ERROR: Incorrect Account Password</span><br>';
		}
	}elseif($_POST['action'] == "disable"){
		// validate user's password before allowing them to change 2FA settings
		if(sha1($session->userinfo['usersalt'].$_POST['password']) == $session->userinfo['password']){
			$now_dt = new DateTime();
			$now = $now_dt->format("Y-m-d H:i:s");

			if($ga->verifyCode($session->userinfo['google_auth_secret'], $_POST['code'], $clock_tolerance)){
				// passed, so delete secret key in database
				$q = "UPDATE `users` SET `google_auth_secret` = '', `google_auth_enabled` = 0 WHERE id=".$session->userinfo['id'];
				if($db->queryDirect($q)){
					echo 'SUCCESS';
				}else{
					echo '<span style="color:red;">ERROR: Could not save 2FA settings to the database</span><br>';
				}
			}else{
				echo '<span style="color:red;">ERROR: Invalid 2FA Code</span><br>';
			}
		}else{
			echo '<span style="color:red;">ERROR: Incorrect Account Password</span><br>';
		}
	}elseif($_POST['action'] == "cancel"){
		session_destroy();
		echo 'SUCCESS';
	}elseif($_POST['action'] == "login"){
		// get user info from db
		$user_info = $db->queryOneRow("SELECT * FROM `".TBL_USERS."` WHERE `username`='".$_SESSION['value_array']['user']."'");
		
		// see if account is locked out
		if($user_info['locked'] == 1){
			// account is locked out, see if login attempt should be allowed yet
			$now_dt = new DateTime();
			$expiration_dt = new DateTime($user_info['locked_expiration']);

			$timeout_dt = $expiration_dt->diff($now_dt);
			$timeout = $core->format_interval($timeout_dt);
			
			if($now_dt > $expiration_dt){
				// reset login attempts and allow login attempt
				$db->query("UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '0' WHERE `username`='".$_SESSION['value_array']['user']."'");
				$user_info['failed_login_attempts'] = 0;
				// unlock account
				$db->query("UPDATE `".TBL_USERS."` SET `locked` = '0' WHERE `username`='".$_SESSION['value_array']['user']."'");
				// reset lockout expiration
				$db->query("UPDATE `".TBL_USERS."` SET `locked_expiration` = NULL WHERE `username`='".$_SESSION['value_array']['user']."'");
			}else{
				// display error to user and log this
				die("ERROR: Account is locked out.<br><br>You must wait ".$timeout." before you can try again.");
			}
		}
		
		if($ga->verifyCode($_SESSION['google_auth_secret'], $_POST['code'], $clock_tolerance)){			
			// process login
			$retval = $session->login($_SESSION['value_array']['user'], $_SESSION['value_array']['pass'], isset($_SESSION['value_array']['remember']));

			if($retval){
				// add to login attempts log
				$now_dt = new DateTime('now');
				$date = $now_dt->format("Y-m-d H:i:s");
				$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`,`user_agent`) VALUES ('".$_SESSION['value_array']['user']."','".$date."','1','2FA enabled login successful','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."')";
				$db->queryInsert($q);
				
				// reset login attempts and allow login attempt
				$db->query("UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '0' WHERE `username`='".$_SESSION['value_array']['user']."'");
				$user_info['failed_login_attempts'] = 0;
				// unlock account
				$db->query("UPDATE `".TBL_USERS."` SET `locked` = '0' WHERE `username`='".$_SESSION['value_array']['user']."'");
				// reset lockout expiration
				$db->query("UPDATE `".TBL_USERS."` SET `locked_expiration` = NULL WHERE `username`='".$_SESSION['value_array']['user']."'");
				// reset lockout minutes
				$db->query("UPDATE `".TBL_USERS."` SET `locked_minutes` = '0' WHERE `username`='".$_SESSION['value_array']['user']."'");
				
				$_SESSION['google_auth_secret'] = "VALID";
				$_SESSION['google_auth_required'] = 0;
				unset($_SESSION['value_array']);
				
				echo "SUCCESS";
			}else{
				// add to login attempts log
				$now_dt = new DateTime('now');
				$date = $now_dt->format("Y-m-d H:i:s");
				$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`,`user_agent`) VALUES ('".$_SESSION['value_array']['user']."','".$date."','0','2FA enabled login failed','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."')";
				$db->queryInsert($q);
				
				/* Login failed */
				$_SESSION['error_array'] = $form->getErrorArray();
				foreach($_SESSION['error_array'] as $error_text){
					echo $error_text."<br>";
				}
			}
		}else{			
			// log failed attempt to db
			$num_failed = $user_info['failed_login_attempts'] + 1;
			$db->query("UPDATE `".TBL_USERS."` SET `failed_login_attempts` = '".$num_failed."' WHERE `username`='".$_SESSION['value_array']['user']."'");
			
			// add to login attempts log
			$now_dt = new DateTime('now');
			$date = $now_dt->format("Y-m-d H:i:s");
			$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`,`user_agent`) VALUES ('".$_SESSION['value_array']['user']."','".$date."','0','Incorrect 2FA Code','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."')";
			$db->queryInsert($q);
			
			// lock account after X bad logins
			if($num_failed >= MAX_LOGIN_ATTEMPTS){
				$db->query("UPDATE `".TBL_USERS."` SET `locked` = '1' WHERE `username`='".$_SESSION['value_array']['user']."'");
				
				// increase/set lockout timer
				$locked_minutes = $user_info['locked_minutes'] + LOCKED_MINUTES;
				$db->query("UPDATE `".TBL_USERS."` SET `locked_minutes` = '".$locked_minutes."' WHERE `username`='".$_SESSION['value_array']['user']."'");
				
				// set lockout expiration
				$now_dt = new DateTime();
				$expiration_dt = $now_dt->modify('+'.$locked_minutes.' minutes');
				$expiration = $expiration_dt->format("Y-m-d H:i:s");
				$db->query("UPDATE `".TBL_USERS."` SET `locked_expiration` = '".$expiration."' WHERE `username`='".$_SESSION['value_array']['user']."'");						
			}
			
			$attempts_remaining = MAX_LOGIN_ATTEMPTS - $num_failed;
			
			// see if account is locked out
			if($attempts_remaining == 0){
				// account is locked out, see if login attempt should be allowed yet
				$now_dt = new DateTime();
				$timeout_dt = $expiration_dt->diff($now_dt);
				//$timeout = $timeout_dt->format('%a days %h hours %i minutes %S seconds');
				$timeout = $core->format_interval($timeout_dt);

				// add to login attempts log
				$now_dt = new DateTime('now');
				$date = $now_dt->format("Y-m-d H:i:s");
				$q = "INSERT INTO `log_login_attempts` (`username`,`date_attempted`,`is_successful`,`message`,`ip_address`,`user_agent`) VALUES ('".$_SESSION['value_array']['user']."','".$date."','0','Account was already locked out','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."')";
				$db->queryInsert($q);
				
				// display error to user
				die("ERROR: Account is locked out.<br><br>You must wait ".$timeout." before you can try again.");
			}
			
			echo '<span style="color:red;">ERROR: Invalid 2FA Code<br><br>';
			echo $attempts_remaining.' login attempts remaining.</span><br>';
		}
	}else{
		echo '<span style="color:red;">ERROR: Invalid Procedure Call</span><br>';
	}
?>
