<script type="text/javascript">
	var montharray=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	var serverdate=new Date('<?php print date("F d, Y H:i:s", time())?>');

	function padlength(what){
		var output=(what.toString().length==1)? "0"+what : what;
		return output;
	}

	function displaytime(){
		serverdate.setSeconds(serverdate.getSeconds()+1);
		var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear();
		var hours = serverdate.getHours();
		var ampm = "AM";
		if(hours >= 12){
			hours = hours - 12;
			var ampm = "PM";
		}
		if(hours == 0){
			hours = 12;
		}
		var timestring=hours+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())+" "+ampm;
		document.getElementById("servertime").innerHTML=datestring+" "+timestring;
	}

	window.onload=function(){
		setInterval("displaytime()", 1000);
	}
</script>

<img width="18px" height="18px" src="/images/clockx.png" border="0">&nbsp;<span id="servertime" style="color:black;"></span>
