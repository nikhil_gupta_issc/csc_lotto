	<link href="/lib/assets/clock/css/style.css" rel="stylesheet" />
	<div id="clock_container" class="row">
		<div id="clock" class="light">
			<div id="clock-title">
				OFFICIAL SERVER TIME
			</div>
			<div class="display">
				<div class="weekdays"></div>
				<div class="ampm"></div>
				<div class="alarm"></div>
				<div class="digits"></div>
			</div>
		</div>
	</div>

	<!-- JavaScript Includes -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/moment.min.js"></script>
	<script src="/lib/assets/clock/js/script.js"></script>