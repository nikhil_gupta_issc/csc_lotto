<?php
// Time-based One-Time Password (TOTP) Google Auth 2FA

date_default_timezone_set('America/New_York');

require_once '../PHPGangsta/GoogleAuthenticator.php';

$now_dt = new DateTime();
echo $now_dt->format("Y-m-d h:i:s A");


$ga = new PHPGangsta_GoogleAuthenticator();

$secret = $ga->createSecret();
echo "<br><br>Secret is: ".$secret."\n\n<br><br>";

$qrCodeUrl = $ga->getQRCodeGoogleUrl('ASureWin', $secret);
echo "Google Charts URL for the QR-Code: ".$qrCodeUrl."\n\n<br><br>";

echo "<img src='".$qrCodeUrl."'><br><br>";


$secret = 'UJVBWJ7IMQ3WBSDQ';
$oneCode = $ga->getCode($secret);
echo "Checking Code '$oneCode' and Secret '$secret':\n<br>";

$checkResult = $ga->verifyCode($secret, $oneCode, 2);    // 2 = 2*30sec clock tolerance
if ($checkResult) {
    echo 'OK';
} else {
    echo 'FAILED';
}
?>

<form method='post'>
	<input type='text' name='secret' value='UJVBWJ7IMQ3WBSDQ'>
	<input type='text' name='2fa_code' value='<?php echo $oneCode; ?>'>
	
	<input type='submit'>
</form>