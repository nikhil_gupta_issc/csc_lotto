<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

date_default_timezone_set('America/New_York'); 
//error_reporting(E_ALL | E_STRICT);

if($_GET['handler'] == 'verification'){
	$ver_token = crc32($_GET['handler'].sha1(date("YdY")));
	if($_GET['token'] == $ver_token){
		require('UploadHandler_verification.php');
		$upload_handler = new UploadHandler();
	}else{
		die("Bad token");
	}
}elseif($_GET['handler'] == 'photos'){
	$photo_token = crc32($_GET['handler'].sha1(date("YmdmY")));
	if($_GET['token'] == $photo_token){
		require('UploadHandler_photos.php');
		$upload_handler = new UploadHandler();
	}else{
		die("Bad token");
	}
}elseif($_GET['handler'] == 'slider'){
	require('UploadHandler_slider.php');
	$upload_handler = new UploadHandler();
}elseif($_GET['handler'] == 'news'){
	require('UploadHandler_news.php');
	$upload_handler = new UploadHandler();
}elseif($_GET['handler'] == 'loyalty'){
	require('UploadHandler_loyalty.php');
	$upload_handler = new UploadHandler();
}