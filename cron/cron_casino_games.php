<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	date_default_timezone_set('America/New_York');		

	        
        $settings = array();
	$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_api_chetu' ";
	$settings_info = $db->queryOneRow($q);
	if(!empty($settings_info)){
               $settings['chetu'] = $settings_info['value'];
        }

	$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_api_multislot'";
	$settings_info = $db->queryOneRow($q);
	if(!empty($settings_info)){
               $settings['multislot'] = $settings_info['value'];
        }         
        

	$q = "SELECT value FROM `settings` WHERE setting = 'casino_enabled'";
	$settings_info = $db->queryOneRow($q);
        if(!empty($settings_info)){
              $settings['casino_enabled'] = $settings_info['value'];
        }
        $t_all_games = array();
			
	if(!empty($settings['casino_enabled']) && !empty($settings['multislot'])){       
		
	// get newest casino games
	$q = "SELECT `transaction_record_id` FROM `customer_transaction` WHERE `vendor_id`=2 AND `transaction_id` IS NOT NULL GROUP BY `transaction_record_id` ORDER BY MIN(transaction_date) DESC LIMIT 8";
	$newest_games = $db->query($q);

        if(!empty($newest_games)){
              foreach($newest_games as $key=>$new_game){
                  $t_all_games[2]['new'][$key] = $new_game['transaction_record_id'];

              }
        }	
	
	// get most played casino games
	$q = "SELECT `transaction_record_id` FROM `customer_transaction` WHERE `vendor_id`=2 AND `transaction_id` IS NOT NULL GROUP BY `transaction_record_id` ORDER BY COUNT(transaction_record_id) DESC LIMIT 8";
	$most_played_games = $db->query($q);
       
        if(!empty($newest_games)){
              foreach($most_played_games as $key=>$new_game){
                  $t_all_games[2]['most_played'][$key] = $new_game['transaction_record_id'];

              }
        }
		
	// get most recently played casino games
	$q = "SELECT `transaction_record_id` FROM `customer_transaction` WHERE `vendor_id`=2 GROUP BY `transaction_record_id` ORDER BY MAX(transaction_date) DESC LIMIT 8";
	$recently_played_games = $db->query($q);
	if(!empty($recently_played_games)){
              foreach($recently_played_games as $key=>$new_game){
                  $t_all_games[2]['recent_played'][$key] = $new_game['transaction_record_id'];

              }
        }
	
	// get least played casino games
	$last_week = new DateTime();
	$last_week = $last_week->modify("-1 week");
	$last_week_str = $last_week->format("Y-m-d H:i:s");
	$q = "SELECT `transaction_record_id` FROM `customer_transaction` WHERE `transaction_date`>'".$last_week_str."' AND `vendor_id`=2 AND `transaction_id` IS NOT NULL GROUP BY `transaction_record_id` ORDER BY COUNT(transaction_record_id) ASC LIMIT 8";
	$least_played_games = $db->query($q);
	if(!empty($least_played_games)){
              foreach($least_played_games as $key=>$new_game){
                  $t_all_games[2]['least_played'][$key] = $new_game['transaction_record_id'];

              }
        }
      
   }
  
	/* CASINO 2 - CasinoSolutions - END */
	
	/* CASINO 1 - Chetu - START */

       if(!empty($settings['casino_enabled']) && !empty($settings['multislot'])){
// get newest casino games
	$q = "SELECT `transaction_record_id` FROM `customer_transaction` WHERE `vendor_id`=1 AND `transaction_id` IS NOT NULL GROUP BY `transaction_record_id` ORDER BY MIN(transaction_date) DESC LIMIT 8";
	$newest_games = $db->query($q);

        if(!empty($newest_games)){
              foreach($newest_games as $key=>$new_game){
                  $t_all_games[1]['new'][$key] = $new_game['transaction_record_id'];

              }
        }	
	
	// get most played casino games
	$q = "SELECT `transaction_record_id` FROM `customer_transaction` WHERE `vendor_id`=1 AND `transaction_id` IS NOT NULL GROUP BY `transaction_record_id` ORDER BY COUNT(transaction_record_id) DESC LIMIT 8";
	$most_played_games = $db->query($q);
       
        if(!empty($newest_games)){
              foreach($most_played_games as $key=>$new_game){
                  $t_all_games[1]['most_played'][$key] = $new_game['transaction_record_id'];

              }
        }
		
	// get most recently played casino games
	$q = "SELECT `transaction_record_id` FROM `customer_transaction` WHERE `vendor_id`=1 GROUP BY `transaction_record_id` ORDER BY MAX(transaction_date) DESC LIMIT 8";
	$recently_played_games = $db->query($q);
	if(!empty($recently_played_games)){
              foreach($recently_played_games as $key=>$new_game){
                  $t_all_games[1]['recent_played'][$key] = $new_game['transaction_record_id'];

              }
        }
	
	// get least played casino games
	$last_week = new DateTime();
	$last_week = $last_week->modify("-1 week");
	$last_week_str = $last_week->format("Y-m-d H:i:s");
	$q = "SELECT `transaction_record_id` FROM `customer_transaction` WHERE `transaction_date`>'".$last_week_str."' AND `vendor_id`=1 AND `transaction_id` IS NOT NULL GROUP BY `transaction_record_id` ORDER BY COUNT(transaction_record_id) ASC LIMIT 8";
	$least_played_games = $db->query($q);
	if(!empty($least_played_games)){
              foreach($least_played_games as $key=>$new_game){
                  $t_all_games[1]['least_played'][$key] = $new_game['transaction_record_id'];

              }
        }
 }
	

	$file = fopen(DOMAIN_ROOT.'/cron/json/casino_games.json',"w");
	fwrite($file,json_encode($t_all_games));
	fclose($file);
