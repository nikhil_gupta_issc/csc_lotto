<?php

include('/var/www/html/config.php');	
date_default_timezone_set('America/New_York');

$q = "SELECT count(id) as alert_count, user_id FROM alerts WHERE is_dismissed = 0 GROUP BY user_id";
$t_alert_counts = $db->query($q);

$t_alerts = array();
foreach($t_alert_counts as $t_alert_count){  
    $t_alerts['alerts'][$t_alert_count['user_id']] = $t_alert_count['alert_count'];
}

$q = "SELECT user_id, count(id) as notification_count FROM `notifications` WHERE `is_dismissed` = 0 GROUP BY user_id";
$t_alert_notifications = $db->query($q);

foreach($t_alert_notifications as $t_alert_notification){  
    $t_alerts['notification'][$t_alert_notification['user_id']] = $t_alert_notification['notification_count'];
}

$file = fopen(DOMAIN_ROOT.'/cron/json/users_notification.json',"w");
fwrite($file,json_encode($t_alerts));
fclose($file);

?>
