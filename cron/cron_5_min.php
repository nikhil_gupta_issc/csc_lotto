<?php
	include('/var/www/html/config.php');

	// Include rapidballs class
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/rapidballs/lib/rapidballs.php");
	}else{
		include(DOMAIN_ROOT.'/rapidballs/lib/rapidballs.php');
	}
	$rapidballs = new RAPIDBALLS();

	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$now2 = $now_dt->format('m/d/y h:i:s A');

	// validate current date/time and results of last cron run
	$q = "SELECT * FROM `log_cron` WHERE `filename` = 'cron_5_min' AND `function` = 'all' ORDER BY `start` DESC LIMIT 1";
	$last_run = $db->queryOneRow($q);
	
	// log cron start
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_5_min', 'all', '0')";
	$cron_5_log_id = $db->queryInsert($q);
	
	$last_start = new DateTime($last_run['start']);
	$expected_dt = new DateTime($last_run['start']);
	$expected_dt = $expected_dt->modify('+5 minutes');
	$expected = $expected_dt->format('m/d/y h:i:s A');
	$elapsed_interval = $now_dt->diff($last_start);
	$elapsed_seconds = $now_dt->getTimestamp() - $last_start->getTimestamp();
	$off_by_interval = $now_dt->diff($expected_dt);
	echo "Last Run - ".$last_run['start']."<br>";
	echo "This Run - ".$now."<br>";
	echo "Expected - ".$expected."<br>";
	echo "Time since last run: ".$core->format_interval($elapsed_interval)." (".$elapsed_seconds." seconds)<br>";
	$message = "";
	if($elapsed_seconds > 330){
		// more than 5 min 30 seconds since last run
		$subject = "Server Date/Time Change Detected";
		$message = "Expected server date/time was off by ".$core->format_interval($off_by_interval)."<br><br>Expected: $expected<br>Actual: $now2".".";
		$group_id = 4;
		$core->send_alert_by_group($subject, $message, $group_id);
		$core->log_sigificant_event('Scheduled Tasks / Server Time', '5-Minute Cron Did Not Run at Expected Time', $message);
	}elseif(!empty($off_by_seconds) && $off_by_seconds < 0){
		// date changed to time before last date
		$subject = "Server Date/Time Change Detected";
		$message = "Expected server date/time was off by ".$core->format_interval($off_by_interval)."<br><br>Expected: $expected<br>Actual: $now2".".";
		$group_id = 4;
		$core->send_alert_by_group($subject, $message, $group_id);
		$core->log_sigificant_event('Server Date/Time Change', 'Serve Clock Set Back In Time', $message);
	}
	echo $message;
	
	// see if last run processed completely
	if($last_run['success'] != '1'){
		$core->log_sigificant_event('Scheduled Tasks / Server Time', '5-Minute Cron Did Not Complete Successfully', 'One or more scheduled tasks did not complete successfully during the previous 5-Minute cron run.');
	}

	// Draw for RapidBalls
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_5_min', 'draw_rapidballs', '0')";
	$cron_log_id = $db->queryInsert($q);
	$rapidballs->draw_rapidballs();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	// RapidBalls Winners
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_5_min', 'rapidballs_winners', '0')";
	$cron_log_id = $db->queryInsert($q);
	$rapidballs->check_for_winners();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

        //Get banned user and disabled users
        $q = "SELECT * FROM `banned_users`";
	$banned_users = $db->query($q);

	$t_users = array();
	foreach($banned_users as $banned_user){
	  
	    $t_users['banned_users'][$banned_user['username']]['timestamp'] = $banned_user[$banned_user['timestamp']];
	    $t_users['banned_users'][$banned_user['username']]['alerted'] =  $banned_user[$banned_user['alerted']];

	}

	$q = "SELECT `user_id` FROM `customers` WHERE `is_disabled`= 1";
	$t_disabled_customers = $db->query($q);

	foreach($t_disabled_customers as $t_disabled_customer){  
	    $t_users['disabled_users'][$t_disabled_customer['user_id']] = 1;
	}

	$file = fopen(DOMAIN_ROOT.'/cron/json/banned_users.json',"w");
	fwrite($file,json_encode($t_users));
	fclose($file);

	// log cron end
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_5_log_id."'";
	$db->query($q);
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_5_log_id."'";
	$db->query($q);

	echo "<br><br>DONE<br><br>";
