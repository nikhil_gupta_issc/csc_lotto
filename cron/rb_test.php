<?php
	include('/var/www/html/config.php');

	// Include rapidballs class
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/rapidballs/lib/rapidballs.php");
	}else{
		include(DOMAIN_ROOT.'/rapidballs/lib/rapidballs.php');
	}
	$rapidballs = new RAPIDBALLS();
	
	if(isset($_GET['force'])){
		$draw_numbers = explode("-", $_GET['force']);
		sort($draw_numbers);
		$rapidballs->draw_rapidballs($draw_numbers);
		
		echo "Forced Rapidballs Drawing of: ".$_GET['force']."<br>";
	}else{
		$num_draws = isset($_GET['draws']) ? $_GET['draws'] : 1;

		for($i=1; $i<=$num_draws; $i++){
			echo "RadidBalls Drawing #$i<br>";
			$rapidballs->draw_rapidballs();
		}	
	}
	
	echo "<br>DONE<br><br>";