<?php
	include('/var/www/html/config.php');
	
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$now2 = $now_dt->format('m/d/y h:i:s A');

	// validate current date/time and results of last cron run
	$q = "SELECT * FROM `log_cron` WHERE `filename` = 'cron_weekly' AND `function` = 'all' ORDER BY `start` DESC LIMIT 1";
	$last_run = $db->queryOneRow($q);
	
	// log cron start
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_weekly', 'all', '0')";
	$cron_daily_log_id = $db->queryInsert($q);
	
	$last_start = new DateTime($last_run['start']);
	$expected_dt = new DateTime($last_run['start']);
	$expected_dt = $expected_dt->modify('+1 week');
	$expected = $expected_dt->format('m/d/y h:i:s A');
	$elapsed_interval = $now_dt->diff($last_start);
	$elapsed_seconds = $now_dt->getTimestamp() - $last_start->getTimestamp();
	$off_by_interval = $now_dt->diff($expected_dt);
	echo "Last Run - ".$last_run['start']."<br>";
	echo "This Run - ".$now."<br>";
	echo "Expected - ".$expected."<br>";
	echo "Time since last run: ".$core->format_interval($elapsed_interval)." (".$elapsed_seconds." seconds)<br>";
	
	
	$last_week = new DateTime();
	$last_week = $last_week->modify("-1 week");
	$last_week_str = $last_week->format("Y-m-d H:i:s");	
	
	$last_d = new DateTime();
	$last_day = $last_d->modify("-1 day");
	$previous_date = $last_day->format("Y-m-d H:i:s");
	
	

	$message = "";
	if($elapsed_seconds > 90000){
		// more than 1 day (86400) + 1 hour (3600) since last run
		$subject = "Server Date/Time Change Detected";
		$message = "Expected server date/time was off by ".$core->format_interval($off_by_interval)."<br><br>Expected: $expected<br>Actual: $now2".".";
		$group_id = 4;
		$core->send_alert_by_group($subject, $message, $group_id);
		$core->log_sigificant_event('Scheduled Tasks / Server Time', 'Weekly Cron Did Not Run at Expected Time', $message);
	}elseif($off_by_seconds < 0){
		// date changed to time before last date
		$subject = "Server Date/Time Change Detected";
		$message = "Expected server date/time was off by ".$core->format_interval($off_by_interval)."<br><br>Expected: $expected<br>Actual: $now2".".";
		$group_id = 4;
		$core->send_alert_by_group($subject, $message, $group_id);
		$core->log_sigificant_event('Server Date/Time Change', 'Serve Clock Set Back In Time', $message);
	}
	echo $message;
	
	// see if last run processed completely
	if($last_run['success'] != '1'){
		$core->log_sigificant_event('Scheduled Tasks / Server Time', 'Weekly Cron Did Not Complete Successfully', 'One or more scheduled tasks did not complete successfully during the previous Weekly cron run.');
	}

	//REBATE CALCULATION CODE STARTS
		$sumArray = array();
		//get current settings from db of loss caclulation on/off
		$settings = array();
		$q = "SELECT value FROM `settings` WHERE `setting` = 'loss_calculation_enabled'";
		$settings_info = $db->queryOneRow($q);
		if(!empty($settings_info)){
           		$loss_calculation_enabled = $settings_info['value'];
       		}
       		
       		if($loss_calculation_enabled == 1){      			
        		       		
        		$type_id = array();
        		// get current settings from db of lotto loss on/off
			$settings = array();
			$q = "SELECT value FROM `settings` WHERE `setting` = 'lotto_loss_enabled'";
			$settings_info = $db->queryOneRow($q);
			if(!empty($settings_info)){
			   $lotto_loss_enabled = $settings_info['value'];			   
			}
			if($lotto_loss_enabled == 1){		
				//array_push($type_id,'3','4');
				// get current settings of lotto loss percentage from db
				$enable = array();
				$q = "SELECT value FROM `settings` WHERE `setting` = 'lotto_loss_percentage'";
				$enable_info = $db->queryOneRow($q);
				if(!empty($enable_info)){
				   $lotto_loss_percentage = $enable_info['value'];
				}
				
				if($lotto_loss_percentage > 0){
        			//get all customer from last one week 
        			$q = "SELECT SUM(lb.bet_amount) as loss, lt.user_id FROM lotto_bet as lb INNER JOIN lotto_ticket as lt ON lb.ticket_id = lt.ticket_id AND lt.user_id != - 9999 AND lb.processed_date_time <= '2016-06-12 23:59:59' AND lb.processed_date_time >= '2016-06-10 00:00:00' AND lb.is_processed = 1 AND lb.is_winner = 0 GROUP BY lt.user_id";
        			
        			$lotto_user = $db->query($q);
        			if(!empty($lotto_user)){
        					foreach($lotto_user as $lotto_value){
        						$sumArray[$lotto_value['user_id']]['lotto'] =$lotto_value['loss'];	
        					}
        				}       			
        			}								
			}
			
			// get current settings of casino loss on/off from db
			$enable = array();
			$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_loss_enabled'";
			$enable_info = $db->queryOneRow($q);
			if(!empty($enable_info)){
			   $casino_loss_enabled = $enable_info['value'];			   
			}			
			if($casino_loss_enabled == 1){
				array_push($type_id,'1','2');
				// get current settings of casino loss percentage from db
				$enable = array();
				$q = "SELECT value FROM `settings` WHERE `setting` = 'casino_loss_percentage'";
				$enable_info = $db->queryOneRow($q);
				if(!empty($enable_info)){
				   $casino_loss_percentage = $enable_info['value'];
				}
			}
			
			// get current settings of rapidball loss on/off from db
			$enable = array();
			$q = "SELECT value FROM `settings` WHERE `setting` = 'rapidball_loss_enabled'";
			$enable_info = $db->queryOneRow($q);
			if(!empty($enable_info)){
			   $rapidball_loss_enabled = $enable_info['value'];			    
			}			
			if($rapidball_loss_enabled == 1){
				//array_push($type_id,'46','47');	
				// get current settings of rapidball loss percentage from db
				$enable = array();
				$q = "SELECT value FROM `settings` WHERE `setting` = 'rapidball_loss_percentage'";
				$enable_info = $db->queryOneRow($q);
				if(!empty($enable_info)){
				   $rapidball_loss_percentage = $enable_info['value'];
				}
				
				if($rapidball_loss_percentage > 0){
        			//get all customer from last one week 
        			$q = "SELECT SUM(rb.bet_amount) as loss, rt.user_id FROM rapidballs_ticket rt INNER JOIN rapidballs_bet rb ON rb.ticket_id = rt.id AND is_processed = 1 AND is_winner = 0 AND rb.processed_datetime <= '2016-06-12 23:59:59' AND rb.processed_datetime >= '2016-06-10 00:00:00' GROUP BY rt.user_id";        		
        			$rb_user = $db->query($q);  
        				if(!empty($rb_user)){
        					foreach($rb_user as $rb_value){
        						$sumArray[$rb_value['user_id']]['rb'] =$rb_value['loss'];	
        					}
        				}      			
        			}
        						
			}
			
			// get current settings from db
			$enable = array();
			$q = "SELECT value FROM `settings` WHERE `setting` = 'sports_loss_enabled'";
			$enable_info = $db->queryOneRow($q);
			if(!empty($enable_info)){
			   $sports_loss_enabled = $enable_info['value'];			   
			}
			if($sports_loss_enabled == 1){
				array_push($type_id,'17','45','18');
				// get current settings of sports loss percentage from db
				$enable = array();
				$q = "SELECT value FROM `settings` WHERE `setting` = 'sports_loss_percentage'";
				$enable_info = $db->queryOneRow($q);
				if(!empty($enable_info)){
				   $sports_loss_percentage = $enable_info['value'];
				}
			}			
			$types = implode(",",$type_id);	
			
		
			 $q = "SELECT  ct.user_id, ct.amount , ct.transaction_type_id FROM customer_transaction as ct where
    				ct.transaction_type_id IN ($types) and ct.transaction_date <= '2016-06-12 23:59:59' AND ct.transaction_date >= '2016-06-10 00:00:00'";
        		$customer_transaction = $db->query($q);	        		
        		
					
			
			foreach ($customer_transaction as $k=>$subArray) {
				//$sumArray[$subArray['user_id']][$subArray['transaction_type_id']] +=$subArray['amount'];		
  				if($subArray['transaction_type_id'] == 1 || $subArray['transaction_type_id'] == 2){
  					$sumArray[$subArray['user_id']]['casino'] +=$subArray['amount'];
  				}
  				elseif($subArray['transaction_type_id'] == 17 || $subArray['transaction_type_id'] == 45 || $subArray['transaction_type_id'] == 18){
  					$sumArray[$subArray['user_id']]['sports'] +=$subArray['amount'];
  				}
			}
			echo "<pre>";
        		print_r($sumArray);
        		
        		$casino_rb_amount_total =0;
        		$lotto_rb_amount_total =0;
        		$rapidball_rb_amount_total = 0;
        		$sports_rb_amount_total =0;
        		$cron_total =0;
        		foreach($sumArray as $key=>$calculation){  
				$casino_rb_amount = 0;
				$lotto_rb_amount = 0;
				$rapidball_rb_amount =0;
				$sports_rb_amount =0;
				$amount = 0; 		      		
        			
        			if(!empty($calculation['casino']) && $calculation['casino']<0){
        				$casino_rb_amount = round((($casino_loss_percentage * $calculation['casino'])/100),2);
        				$casino_rb_amount_total += $casino_rb_amount;
        			}
        			if(!empty($calculation['lotto'])){
        				$lotto_rb_amount = round((($lotto_loss_percentage * $calculation['lotto'])/100),2);
        				$lotto_rb_amount_total += $lotto_rb_amount;
        			}
        			if(!empty($calculation['rb'])){
        				$rapidball_rb_amount = round((($rapidball_loss_percentage * $calculation['rb'])/100),2);
        				$rapidball_rb_amount_total +=$rapidball_rb_amount;
        			}
        			if(!empty($calculation['sports']) && $calculation['sports']){
        				$sports_rb_amount = round((($sports_loss_percentage * $calculation['sports'])/100),2);
        				$sports_rb_amount_total +=$sports_rb_amount;
        			}
        			
        			$amount = round((abs($casino_rb_amount)+abs($lotto_rb_amount)+abs($rapidball_rb_amount)+abs($sports_rb_amount)),2); 
        			
        			
        			
        			//die;
        			
        			if($amount >= 0.01){
        			$id = $core->make_customer_transaction($amount, 150, "Customer Rebate of $".$amount,"NULL",$key);
        			
  				$notification = $core->send_alert('Rebate Account','Your Account has been credited with Rebate Amount of $'.$amount,$key);
  				$rebate_date = $now_dt->format("Y-m-d H:i:s");
  				$q = "INSERT INTO customer_rebate (`user_id`,`casino_rb_amount`,`lotto_rb_amount`,`rapidball_rb_amount`,  `sports_rb_amount`,`casino_loss_percentage`,`lotto_loss_percentage`,`rapidball_loss_percentage`,`sports_loss_percentage`,`total_amount`,  `rebate_date`,`transaction_id`) VALUES ('".$key."','".$casino_rb_amount."','".$lotto_rb_amount."','".$rapidball_rb_amount."','".$sports_rb_amount."','".$casino_loss_percentage."','".$lotto_loss_percentage."','".$rapidball_loss_percentage."','".$sports_loss_percentage."','".$amount."','".$rebate_date."','".$id."')";
  				$r = $db->queryInsert($q);
  				}
        		}
        		$cron_total = abs($casino_rb_amount_total)+abs($lotto_rb_amount_total)+abs($rapidball_rb_amount_total)+abs($sports_rb_amount_total); 
        		$period = '2016-06-10 00:00:00'." ".'2016-06-12 00:00:00';
        		$q = "INSERT INTO cron_weekly_totals(`period`,`casino_1`,`casino_2`,`lotto`,`rapidball`,`sports`,`total`,`date_sent`,`month`,`from_date`,`to_date`) VALUES ('".$period."','".abs($casino_rb_amount_total)."','','".abs($lotto_rb_amount_total)."','".abs($rapidball_rb_amount_total)."','".abs($sports_rb_amount_total)."','".$cron_total."','".date('Y-m-d H:i:s')."','".date('m')."','2016-06-10 00:00:00','2016-06-12 00:00:00')";
        		$result = $db->queryInsert($q);        		        		
		}
	//REBATE CALCULATION CODE ENDS	
	// log cron end
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_daily_log_id."'";
	$db->query($q);
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_daily_log_id."'";
	$db->query($q);
