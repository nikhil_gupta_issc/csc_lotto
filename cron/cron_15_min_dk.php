<?php
	include('/var/www/html/config.php');

	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$now2 = $now_dt->format('m/d/y h:i:s A');

	// validate current date/time and results of last cron run
	$q = "SELECT * FROM `log_cron` WHERE `filename` = 'cron_15_min' AND `function` = 'all' ORDER BY `start` DESC LIMIT 1";
	$last_run = $db->queryOneRow($q);
	
	// log cron start
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_15_min', 'all', '0')";
	$cron_15_log_id = $db->queryInsert($q);
	
	$last_start = new DateTime($last_run['start']);
	$expected_dt = new DateTime($last_run['start']);
	$expected_dt = $expected_dt->modify('+15 minutes');
	$expected = $expected_dt->format('m/d/y h:i:s A');
	$elapsed_interval = $now_dt->diff($last_start);
	$elapsed_seconds = $now_dt->getTimestamp() - $last_start->getTimestamp();
	$off_by_interval = $now_dt->diff($expected_dt);
	echo "Last Run - ".$last_run['start']."<br>";
	echo "This Run - ".$now."<br>";
	echo "Expected - ".$expected."<br>";
	echo "Time since last run: ".$core->format_interval($elapsed_interval)." (".$elapsed_seconds." seconds)<br>";
	$message = "";
	if($elapsed_seconds > 930){
		// more than 15 min 30 seconds since last run
		$subject = "Server Date/Time Change Detected";
		$message = "Expected server date/time was off by ".$core->format_interval($off_by_interval)."<br><br>Expected: $expected<br>Actual: $now2".".";
		$group_id = 4;
		$core->send_alert_by_group($subject, $message, $group_id);
		$core->log_sigificant_event('Scheduled Tasks / Server Time', '15-Minute Cron Did Not Run at Expected Time', $message);
	}elseif($off_by_seconds < 0){
		// date changed to time before last date
		$subject = "Server Date/Time Change Detected";
		$message = "Expected server date/time was off by ".$core->format_interval($off_by_interval)."<br><br>Expected: $expected<br>Actual: $now2".".";
		$group_id = 4;
		$core->send_alert_by_group($subject, $message, $group_id);
		$core->log_sigificant_event('Server Date/Time Change', 'Serve Clock Set Back In Time', $message);
	}
	echo $message;
	
	// see if last run processed completely
	if($last_run['success'] != '1'){
		$core->log_sigificant_event('Scheduled Tasks / Server Time', '15-Minute Cron Did Not Complete Successfully', 'One or more scheduled tasks did not complete successfully during the previous 15-Minute cron run.');
	}

	// Pull in new lotto drawing results from xml feed
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_15_min', 'get_lotto_xml', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->get_lotto_xml();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	// Check for winning lotto tickets
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_15_min', 'check_lotto_winner', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->check_lotto_winner();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	// Update running daily totals
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_15_min', 'update_daily_totals', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->update_daily_totals();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	
	// Archive lotto results
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_15_min', 'archive_lotto_results', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->archive_lotto_results();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);
	
	// Instantiate messaging class
	$message = new MESSAGE();

	// log cron end
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_15_log_id."'";
	$db->query($q);
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_15_log_id."'";
	$db->query($q);

	echo "<br><br>DONE<br><br>";
