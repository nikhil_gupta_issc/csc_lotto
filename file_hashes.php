<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	//$core->hash_approved_files();
	//$core->check_file_hashes();
	//die();
	
	function get_recursive_file_list($root_dir, &$file_list){
		$dir = new DirectoryIterator($root_dir);

		foreach($dir as $entity){
			// ignore parent and current directory entries
			if($entity == '.' || $entity == '..'){
				continue;
			}

			if($entity->isDir()){
				get_recursive_file_list("$root_dir/$entity", $file_list);
			}else{
				add_file("$root_dir/$entity", $file_list);
			}
		}
	}
	
	function add_file($filename, &$array){
		$i = count($array);
		$array[$i]['filename'] = $filename;
		$array[$i]['hash'] = sha1_file($filename);
	}
	
	$file_list = array();
	get_recursive_file_list("/srv", $file_list);
	
	$fp = fopen('files.csv', 'w');

	foreach($file_list as $fields){
		fputcsv($fp, $fields);
	}
	fclose($fp);
	
	echo "<a href='files.csv'>files.csv</a>";