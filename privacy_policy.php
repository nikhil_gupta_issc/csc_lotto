<?php
	include("header.php");
	include('page_top.php');

	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}
?>
	<div class="clearfix" style="background:#fff; color: 000;">	
			<div class="row">
					<div class="container">
					   <div class="header_image">
							<img src="/images/privacy_policy.jpg" class="img_grow_wide">
						</div>
					</div>
				</div>
<!-- <div class="clearfix" style="text-align: left; padding:10px;">
		<p style="color:#CCC;padding:10px;border: 1px solid purple; background: #000;">Our team of industry veterans started with one mission in mind - to provide a trustworthy and unrivaled online experience for every player, no matter their casino experience level. Whether this is your first time playing online, or your one-thousandth, we promise you lightning-fast deposit authorization and customer service by our knowledgeable, friendly agents. So no matter the hour, when you want to play the best casino games available online, CSCLotto is your Casino/Lottery provider.</p>
		</div> -->
	<div class="col-md-12" style="color:#000;">
	<br />
<p>This Privacy Policy sets out the ways in which CSCLotto may use your personal data if you’re an CSCLotto user or visitor to our Sites (which include CSCLotto.com, and any subdomain(s) attached to it, hereinafter “Sites”). “We“, “us“ or “our“ means the CSCLotto entity with which you are interacting. “You“ and “your“ means you, the person engaging with us, registering for our services or visiting our Sites.</p>

<p>By subscribing to our services, visiting our Sites or otherwise interacting with us, you consent to our processing of your personal data in accordance with this Privacy Policy.<p>

<p><h4>What types of personal data do we collect on our Sites</h4></p>

<p>We collect certain personal data about visitors and users of our Sites.</p>

<p>The most common types of personal data we collect include things like: usernames, personal names, email addresses, IP addresses, other contact details, survey responses, blogs, photos, payment information, transactional details, support queries, forum comments, blog comments, content you direct us to make available on our Sites (such as descriptions), and web analytics data.</p>

<p><h4>How we collect personal data on our Sites</h4></p>

<p>We collect personal data directly when you provide it to us, automatically as you navigate through the Sites, or through other people when you use services associated with the Sites.</p>

<p>We collect your personal data when you provide it to us when you subscribe to a newsletter, email list, submit feedback, enter a contest, fill out a survey, complete a form, purchase a product, download software, or send us an email.</p>

<p><h4>Personal data we collect about you from third parties</h4></p>

<p>Although we generally collect personal data directly from you, on occasion, we also collect certain categories of personal data about you from other sources including: (1) third party service providers (like Google, Facebook), which may provide personal data about you when you link, connect, or login to your account with the third party provider and they send us personal data such as your registration and profile from that service. The personal data varies and is controlled by each such service provider or as authorized by you via your privacy settings at each such service provider; and (2) other third party sources or partners, whereby we receive additional personal data about you (to the extent permitted by applicable law), such as demographic data or fraud detection information, and combine it with personal data we already have about you. For example, fraud warnings from service providers such as identity verification services. We also receive personal data about you and your activities on and off the Sites through partnerships, or about your experiences and interactions from our partner ad networks.</p>

<p><h4>How we use your personal data</h4></p>

<p>We will use your personal data where necessary for purposes which are in our, or third parties, legitimate interests. These legitimate interests include: (1) operating the Sites; (2) providing you with services described on the Sites; (3) responding to support requests, and helping facilitate the resolution of any disputes; (4) updating you with operational news and information about our Sites and services, i.e. to notify you about changes to our Sites, website disruptions or security updates; (5) carrying out technical analysis to determine how to improve the Sites and services we provide; (6) managing our relationship with you, i.e. by responding to your comments or queries submitted to us on the Sites or asking for your feedback or whether you want to participate in a survey; (7) managing our legal and operational affairs; (8) training CSCLotto staff about how to best serve our user community; (improving our products and services; (9) providing general administrative and performance functions and activities.</p>

<p>Where you give us consent we will use your personal data to: (1) provide you with marketing information about products and services which we feel may be of interest to you; and (2) to customize our services and Sites, such as advertising that appears on the Sites, where this involves the use of cookies or similar technologies, in order to provide a more personalized experience for our users and visitors.</p>

<p>We will also handle your personal data: (1) as required by law; and (2) to respond to legitimate judiciary and law enforcement authorities, or regulatory requests to abide by anti-money laundering (“AML”) regulations.</p>

<p><h4>Who has access to your personal data</h4><p>

<p>We will disclose personal data to the following recipients: (1) subcontractors and service providers who assist us in connection with the ways we use personal data (as set out above), in particular, security and fraud prevention services, payment processing services, and identification verification services. Our subcontractors and services providers may also transfer and access such personal data from other countries in which they have operations; (2) our professional advisors, including lawyers, accountants, and financial advisors; (3) regulators and government authorities in connection with our compliance procedures and obligations; (4) third party requests relating to criminal investigations or alleged or suspected illegal activity; (5) third party requests, in order to enforce or defend our rights, or to address financial or reputational risks; (6) intellectual property rights holders in relation to an allegation of intellectual property infringement or any other infringement; and (7) other recipients where we are authorized or required by law to provide your personal data.</p>

<p><h4>Where we transfer and store your personal data</h4></p>

<p>We are based in the US, with servers in the US, so your data will be processed in the US, in accordance with applicable data protection regulations. Some of the recipients mentioned above, to whom we may disclose your personal information, may be located outside of the US. In order to protect your personal data we will strive to work with subcontractors and service providers who we believe maintain an adequate level of data security compliance.</p>

<p><h4>How we secure your personal data</h4></p>

<p>We store your personal data on secure servers that are managed by us and our service providers, and occasionally on hard copy files that are kept in a secure location in the US. Personal data that we store or transmit is protected by security and access controls, including username and password authentication, two-factor authentication, and data encryption where appropriate.</p>

<p><h4>How you can access your personal data</h4></p>

<p>You may request to access and receive a copy of your personal data and request corrections of any errors in such personal data by contacting us at <a target="_blank" href="mailto:support@casinocoin.org">support@casinocoin.org</a>.</p>

<p><h4>Marketing communications</h4></p>

<p>We will send you marketing communications by email about products and services that we feel may be of interest to you, subject to your “opt-in” consent upon receiving any email communication from us or your registering for such email communications on our Sites. You can “opt-out” of any such email communications if you would prefer not to receive them in the future by using the “unsubscribe” option provided at the bottom of every email communication.</p>

<p><h4>Cookies and web analytics</h4></p>

<p>You may choose whether to accept cookies by modifying your browser preferences to accept all cookies, be notified when a cookie is set, or to reject all cookies. If you choose to reject cookies parts of our Sites may not work properly during your use of our Sites.</p>

<p>When you visit our Sites, certain anonymous information is collected which does not reveal your identity. Occasionally, we will use third party advertising companies to serve ads based on prior visits to our Sites. For example, if you visit our Sites, you may later see an ad for our products and services when you visit a different website.</p>

<p>If you’re logged into your account on any of our Sites the following details may be recorded: (1) your IP address or proxy server IP address; (2) the domain name you requested; (3) the name of your internet service provider depending on the configuration of your ISP connection; (4) the date and time of your visit to the Site(s); (5) the length of your session; (6) the pages which you have accessed; (7) the number of times you access our Sites within any month; (7) the file URL you look at and information relating to it; (8) the website which referred you to our Sites; and (9) the operating system which your computer uses.</p>

<p><h4>Personal data of minors</h4></p>

<p>We do not knowingly provide services to or collect personal data from persons under 18 years of age. Our Sites are not directed or intended for children under the age of majority. If you are under 18 years of age, you should not register or provide personal data or information on our Sites. If you are the parent or guardian of a person under the age of 18 whom you believe has disclosed personal data to us, please immediately contact us at <a target="_blank" href="mailto:support@casinocoin.org">support@casinocoin.org</a> so that we may delete and remove such minor’s personal data from our systems.</p>

<p><h4>Software you download from our Sites</h4></p>

<p>Some of the downloadable software offered on our Sites, will collect, with your consent, your personal data including: (1) name and address details; (2) IP address or your proxy server IP address; (3) email address and phone number; (4) credit card details and billing information; (5) utility bills and proof of residence; and (6) other type of information when required.</p>

<p>The personal data we collect will be shared with our partners in order to be able to offer you our service(s), specifically this information will be transmitted for the purposes of: (1) know your customer (“KYC”) verification checks; (2) identity verifications; (3) payment verifications; and (4) marketing and promotions.</p>

<p>We will be collecting personal data that is necessary for us to provide the service(s) we offer on our Sites and we will periodically be purging all personal data that we no longer need for business or legal purposes.</p>

<p><h4>Retention and deletion of personal data</h4></p>

<p>We retain your personal data for as long as is necessary to provide the services to you and our other users, and to comply with our legal obligations. If you no longer want us to use your personal data or to provide you with the services, you can request that we erase your personal data. If you request the erasure of your personal data we will retain personal data from deleted accounts as necessary for our legitimate business interests, to comply with the law, prevent fraud, assist with investigations, enforce the terms of service and take other actions permitted by law. The personal data we retain will be handled in accordance with this Privacy Policy.</p>

<p><h4>Changes to the Privacy Policy</h4></p>

<p>We will need to update this Privacy Policy from time to time in order to make sure it stays current with the latest legal requirements and changes to our privacy by design practices.</p>

<p>We will notify you about updates and a copy of the latest version of this Privacy Policy will always be available on each of our Sites.</p>

<p><h4>If you’re a user or visitor in the European Economic Area these rights also apply to you:</h4></p>

<p>For the purposes of applicable EU data protection law (including the “GDPR”), we are a “data controller” and “data processor” of your personal data.</p>

<p><h4>How to port your personal data</h4></p>

<p>You are entitled to ask us to port your personal data (i.e. to transfer in a structured, commonly used and machine-readable format, to you), to erase or restrict processing of your personal data. You also have rights to object to some processing that is based on our legitimate interests, such as profiling that we perform for the purposes of direct marketing, and, where we have asked for your consent to process your data, to withdraw this consent as more fully described below.</p>

<p>These rights are limited in some situations if we can demonstrate that we have a legal requirement to process your personal data. In some circumstances, this means that we may retain some of your personal data even if you withdraw your consent.</p>

<p>Where we require your personal data to comply with legal or contractual obligations, then provision of such personal data is mandatory: if such personal data is not provided, then we will not be able to manage our contractual relationship with you, or to meet obligations placed on us. In all other cases, provision of requested personal data is optional.</p>

<p>If you have unresolved issues you have the right to complain to the data protection authorities. The relevant data protection authority will be the data protection authority of the country: (1) of your habitual residence; (2) of your place of work; or (3) in which you consider the alleged data protection violation has occurred.</p>

<p><h4>How to contact CSCLotto</h4><p>

<p>If you have any questions about our Privacy Policy or the way in which we have been handling your personal data, please contact us at <a target="_blank" href="mailto:support@casinocoin.org">support@casinocoin.org</a>.
<br /><br />
<p><b>This Privacy Policy has been revised as of November 20, 2018</b></p>


		
	</div>
</div>
<?php
	
	include("footer.php");
?>
