<?php
	$menu_color = "slategray";
	$footer_color = "slategray";
	
	include("header.php");
	include('page_top.php');

	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}
?>
	<div class="clearfix" style="background:#fff; color: #000;">	
		<div class="row">
			<div class="container">
			   <div class="header_image">
					<img src="/images/responsible.jpg?v=1" class="img_grow_wide">
				</div>
			</div>
		</div>
		
		<div class="col-md-12" style="color: #000;">
			<h2>The Responsible Way to Play</h2>
			<p>CSCLotto is not a real money gaming site, and operates purely for entertainment purposes. However, we want you to enjoy our gaming platform in a manner that would not result in excess play creating a negative impact on your personal life. We realise that gambling habits can lead to an addiction, even if no money is at risk. Our platform should be used as a means of entertainment and enjoyed in moderate doses.</p>
			<p>Even though we’re not a real money gaming website, CSCLotto still takes age restriction very seriously. No person under the age of 18 years is allowed to open an account with CSCLotto. This verification of age is a critical part of our onboarding process and any persons found to be under the age of 18 will be forbidden from play.</p>
			<p>Finally if you have developed a problem it is very important to seek help and advice.</p>
		</div>
	
		
		
		<div class="col-md-12" style="color: #000;">
			<h3>Setting Limits:</h3>
			<p>CSCLotto provides the ability for all patrons to set a limit on their gaming activity using TestCSC. A limit is set as follows:</p>
			<ol>
				<li>The daily amount you may deposit</li>
				<li>The maximum daily amount you can lose in any gaming transaction</li>
				<li>The daily amount you can wager</li>
				<li>The daily amount you can withdrawal</li>
				<li>The daily session limit in minutes</li>
				<li>The maximum amount for each transaction and the ability to prevent all betting</li>
				<li>Self-exclusion to prevent login and playing in a specific period of time</li>
			</ol>
			<p><i>To <i>set limits</i>, go to <a style="color: #bf0a0a !important;" href='/my_account.php'>My Account</a> and click on the <b>Operations</b> tab.</i></p>

			<h3>Self-Exclusion:</h3>
			<p>CSCLotto takes the consequence associated with compulsive gambling very seriously and thus has put in place procedures available to customers wanting to be excluded from CSCLotto. Request for self-exclusion can be done at any time.</p>
			<p><i>To set a <i>self-exclusion</i>, go to <a style="color: #bf0a0a !important;" href='/my_account.php'>My Account</a> and click on the <b>Operations</b> tab.  </i></p>
			
			<h3>Account Security:</h3>
			
			<p>CSCLotto strongly advises that you keep your login data safe to ensure that no unauthorized persons can access to your account.</p>
			
			<p>We care a lot about your user account security, therefore we’ve added several features that help you to ensure your account is not compromised. In order to protect you the best way we can, please read the following information carefully.</p>
			
			<ol>
				<li>
					<h4><b>Password</b></h4>
					<p>Choose a secure and difficult to guess password. We strongly suggest using a <a target="_blank" style="color: #bf0a0a !important;" href="https://en.wikipedia.org/wiki/Password_manager">password manager</a> to generate a strong and secure one. Your name or the name of your pet is unfortunately too simple.</p>
					<p>Secure password should:</p>
					<ul>
						<li>be at least 8 characters</li>
						<li>contain at least one lowercase letter (a-z)</li>
						<li>contain at least one uppercase letter (A-Z)</li>
						<li>contain at least one number (0-9)</li>
						<li>contain at least one special character (such as ~!@#$%^&*)</li>
					</ul>
				</li>
				<li>
					<h4><b>Two Factor Authentication (2FA)</b></h4>
					<p>Our system supports mobile 2FA. We strongly suggest you use it. Learn more about 2FA by clicking <a style="color: #bf0a0a !important;" target="_blank" href="https://en.wikipedia.org/wiki/Two-factor_authentication#Mobile_phone_two-factor_authentication">here</a>.</p>
				</li>
				<li>
					<h4><b>Last Login</b></h4>
					<p>Every time you login, you see the date and the time of your last login at the upper area of your browser. Please pay attention to this information and check it regularly. Please contact us immediately if this information varies from your actual last login.</p>
				</li>
				<li>
					<h4><b>IP Address</b></h4>
					<p>We log your IP address for security reasons only.</p>
				</li>
				<li>
					<h4><b>Email Notifications</b></h4>
					<p>By default we notify you of every succeeded or failed login on your account. If you don’t want this notification you have to deactivate it in your <a style="color: #bf0a0a !important;" href='/my_account.php'>My Account</a> settings.</p>
				</li>
				<li>
					<h4><b>Account History and Transactions</b></h4>
					<p>To keep track on what is happening while you are logged in, we have a very detailed transaction history that can be found on your <a style="color: #bf0a0a !important;" href='/my_account.php'>My Account</a> page. It will log all your activities for Lotto, FastBallz, Sportsbook and Casinos. In addition, you can see all your deposits, withdrawals, voucher redemptions or loyalty point redemptions. </p>
				</li>
			</ol>
			
			<h3>Return to Player:</h3>
			
			<p>Return to Player (RTP) is the percentage of winnings awarded to total stakes played, calculated over a theoretical lifetime of games played. The RTP percentage will vary widely in either direction due to the statistical variance in the large number of theoretical games played.</p>
			
			<h4></h4>
			
			<br><br>
		</div>
	</div>
<?php
	include("footer.php");
?>