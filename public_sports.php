		<div class="clearfix" style="background:#111111">	
			<div class="row">
					<div class="container">
					   <div class="header_image">
							<!--<img src="/images/sports_header.jpg" class="img_grow_wide">-->
<?php
			$files = scandir('images/sports_slider_image/');
			$total = count($files);
			$images = array();
			for($x = 0; $x <= $total; $x++){
				if($files[$x] != '.' && $files[$x] != '..' && is_file('images/sports_slider_image/'.$files[$x])){
					$images[] = $files[$x];
				}
			}
		?>
	
		<div id="carousel-index" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'" class="active"></li>';
						}else{
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'"></li>';
						}
					}
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '
				<div class="item active">
					<a href="#">
						<img src="/images/sports_slider_image/'.$images[$x].'" class="carousel-image"  width="100%" height="222px">
					</a>
				</div>
							';
						}else{
							echo '
				<div class="item">
					<a href="#">
						<img src="/images/sports_slider_image/'.$images[$x].'" class="carousel-image"  width="100%" height="222px">
					</a>
				</div>
							';
						}
					}
				?>
						</div>
					</div>
				</div>
			
			
		
		
		<script src="/lib/assets/animate_table_change/animator.js"></script>
		<script src="/lib/assets/animate_table_change/rankingTableUpdate.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				function get_recent_winners(){
					$.ajax({
						url: "/ajax/recent_winners.php",
						method: "POST",
						data:{
							type : 'recent',
							data : 'sportsbook'
						}
					})
					.done(function(response){
						if(response){
							var old_table = $('#recent_winners_list').html();
							$('#temp_recent_winners_list').html(response);
							var new_table = $('#temp_recent_winners_list').html();
							
							if(old_table != new_table){
								$('#recent_winners_list').rankingTableUpdate('<table class="table" id="recent_winners_list">'+new_table+'</table>', {
									duration: [1000, 0, 700, 0, 500],
									onComplete: function(){
										updating = false;
									},
									animationSettings: {
										up: {
											left: -25,
											backgroundColor: '#CCFFCC'
										},
										down: {
											left: 25,
											backgroundColor: '#8C008C'
										},
										fresh: {
											left: 0,
											backgroundColor: '#CCFFCC'
										},
										drop: {
											left: 0,
											backgroundColor: '#8C008C'
										}
									}
								});
							}
							setTimeout(get_recent_winners, 5000);
						}
					});
				}
				
				function get_big_winners(){
					$.ajax({
						url: "/ajax/recent_winners.php",
						method: "POST",
						data:{
							type : 'big',
							data : 'sportsbook'
						}
					})
					.done(function(response){
						if(response){
							var old_table = $('#big_winners_list').html();
							$('#temp_big_winners_list').html(response);
							var new_table = $('#temp_big_winners_list').html();
							
							if(old_table != new_table){
								$('#big_winners_list').rankingTableUpdate('<table class="table" id="big_winners_list">'+new_table+'</table>', {
									duration: [1000, 0, 700, 0, 500],
									onComplete: function(){
										updating = false;
									},
									animationSettings: {
										up: {
											left: -25,
											backgroundColor: '#CCFFCC'
										},
										down: {
											left: 25,
											backgroundColor: '#8C008C'
										},
										fresh: {
											left: 0,
											backgroundColor: '#CCFFCC'
										},
										drop: {
											left: 0,
											backgroundColor: '#8C008C'
										}
									}
								});
							}
							setTimeout(get_big_winners, 5000);
						}
					});
				}
			//	get_recent_winners();
			//	get_big_winners();
			});
		</script>
<?php
$settings = array();
		$q = "SELECT * FROM `settings`";
		$settings_info = $db->query($q);
		foreach($settings_info as $setting){
			$settings[$setting['setting']] = $setting['value'];
		}
if($settings['sports_enabled'] == 1){
?>
		<div class="bg">
		<!--	<div class="col-md-6 winners_board ">
				<div class="win-bg-plain">
					<h3 class="headings">Latest Sportsbook Winners</h2>
				</div>

				<table id='temp_recent_winners_list' style='display: none;'></table>
				
				<table class="table" id="recent_winners_list">
					<thead>
						<tr>
							<th class="position anim:id anim:number hidden" />
							<th class="driverName anim:number hidden" />
							<th class="pointsTotal anim:update hidden" />
							<th class="pointsTotal anim:update hidden" />
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div class="col-md-6 winners_board ">
			
				<div class="win-bg-plain">
					<h3 class="headings">Latest Big Sportsbook Winners</h2>
				</div>
				
				<table id='temp_big_winners_list' style='display: none;'></table>
				
				<table class="table" id="big_winners_list">
					<thead>
						<tr>
							<th class="position anim:id anim:number hidden" />
							<th class="driverName anim:number hidden" />
							<th class="pointsTotal anim:update hidden" />
							<th class="pointsTotal anim:update hidden" />
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>-->
			
			
		</div>
		<!-- <?php include('inc_public_casino_slider.php');?> -->
    		<div class="col-md-12" style="height: 220px; overflow: hidden; margin-top: 3px; margin-bottom: 18px; width:99%;"><img src="/images/home_slider.gif"></div>
			
	</div>
	<?php } else {echo "<div class='well' style='height: 400px; text-align: center; margin:0;'><h2><br><br>We're sorry but Sports Book is currently Unavailable.<br><br>Please check back later.</h2></div>";}?>
