    <link rel="stylesheet" type="text/css" href="/admin/css/global.css?v=<?=CSS_VERSION?>" />
    <div class="container" style="text-align: center; width: 350px;">
		<img src="/images/asw_logo.png"></img>
		<br><br>
		<div class="panel panel-default">
			<div class="panel-body">
<?php $s_account_error = $form->error("account");
       if(empty($s_account_error)){ ?>
				<form autocomplete="off" action="<?php echo ROOTPATH; ?>/lib/framework/login.php" method='post' class="form-horizontal">
					<h2 class="form-group-heading">Cashier Panel Login</h2>
					
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">User</label>
						<div class="col-sm-9">
							<input name="user" type="text" class="form-control" id="email" placeholder="Email address or username">
							
						</div>
					</div>
					
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Password</label>
						<div class="col-sm-9">
							<input name="pass" type="password" class="form-control" id="password" placeholder="Password">
                                                        <input name="is_cashier" type="hidden" value="1" >
                                               </div>
					</div>
			
					<div class="form-group">
						<div class="col-sm-12">
							
							<span style="float: right;">
								<a href="?forgotpass">Forgot Password</a>
							</span>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-12">
							<button class="btn btn-large btn-primary" type="submit">Sign in</button>
							<input type='hidden' name='sublogin' value='1'>
							<input type='hidden' name='referrer' value='<?php echo str_replace('action=logout', '', $_SERVER['REQUEST_URI']); ?>'>
						</div>
					</div>
				</form>
				<?php }
                                   if(!empty($s_account_error)) { ?>
                                <form action="/lib/framework/login.php" method="POST" autocomplete="off" class="form-horizontal" >
                                    <h2 class="form-group-heading">Admin Login</h2> 
                                    <div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">User</label>
						<div class="col-sm-9">
					 <input name="user" type="text" class="form-control" value="<?php echo $form->getUserName(); ?>"  readonly >
                                         <input type='hidden' name='is_admin' value='1'>
                                         <input type='hidden' name='sessionlog' value='1'>
			                 <input type='hidden' name='user_id' value="<?php echo $form->getUserID(); ?>">	
			                 <input type='hidden' name='reason' value="<?php echo 'forced logout'; ?>">	
			                 <input type='hidden' name='user' value="<?php echo $form->getUserName(); ?>">	
			                 <input type='hidden' name='referrer' value="<?php echo str_replace('action=logout', '',$_SERVER['REQUEST_URI']); ?>">			
						</div>
					</div>
					
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Password</label>
						<div class="col-sm-9">
							<input name="pass" type="password" class="form-control" id="password" placeholder="Password">
						</div>
					</div>
                                       <div class="form-group">
						<div class="col-sm-12">
							<button class="btn btn-large btn-primary" type="submit">Logout other session</button>
							
						</div>
					</div>
 
                             
                               </form>
				<?php
					} if($form->num_errors >= 1){
						echo "<div class='admin_error' align='center' style='text-align:center; padding: 10px 8px; color:red;'>".(!empty($s_account_error) ? $form->error("account").'<br>' : '').$form->error("user")."<br>".$form->error("pass")."</div>";
					}
				?>
			</div>
		</div>
    </div> <!-- /container -->
    
	<br><br>
	
<?php
	include("footer.php");
	die();
?>
