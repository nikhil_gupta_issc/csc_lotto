<?php
	include($_SERVER['DOCUMENT_ROOT'].'/panel/receipts/receipts.php');

	if(isset($_GET['void'])){
		$receipts->print_void_ticket($_GET['ticket_number']);
	}elseif(isset($_GET['ticket_number'])){
		$receipts->print_ticket($_GET['ticket_number']);
	}elseif(isset($_GET['ticket_id'])){
		$receipts->print_ticket_by_id($_GET['ticket_id']);
	}elseif(isset($_GET['transaction_id'])){
		$receipts->print_transaction($_GET['transaction_id']);
	}elseif(isset($_GET['open_shift'])){
		$receipts->print_open_shift_report($_GET['shift_id']);
	}elseif(isset($_GET['shift_id'])){
		$receipts->print_shift_report($_GET['shift_id']);
	}elseif(isset($_GET['winning_numbers'])){
		$receipts->print_winning_numbers();
	}elseif(isset($_GET['location_id'])){
		$receipts->location_sales_report($_GET['from'], $_GET['to'], $_GET['location_id']);
	}elseif(isset($_GET['phone_card_id'])){
		$receipts->phone_card($_GET['phone_card_id']);
	}elseif(isset($_GET['gift_card_id'])){
		$receipts->gift_card($_GET['gift_card_id']);
	}elseif(isset($_GET['customer_transaction'])){
		$receipts->print_customer_deposit($_GET['customer_transaction']);
	}elseif(isset($_GET['petty_cash'])){
		$receipts->print_petty_cash($_GET['petty_cash']);
	}



	if(isset($_GET['logout'])){
		// need to logout after printing complete
		$session->logout();
	}
