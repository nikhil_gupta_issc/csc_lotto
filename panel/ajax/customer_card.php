<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");

       
	if(isset($_POST['action']) && $_POST['action'] == "customer_balance"){

		//$user = $db->queryOneRow("SELECT *, i.name as island FROM `customers` c JOIN `users` u ON c.user_id=u.id JOIN `island` AS `i` ON `i`.`id` = `c`.`island_id` WHERE customer_number=".$_POST['customer_number']);
		$q = "SELECT *, i.name as island FROM `customers` c LEFT JOIN `users` u ON c.user_id=u.id LEFT JOIN `user_info` AS `ui` ON `ui`.`user_id`=`u`.`id` LEFT JOIN `island` AS `i` ON `i`.`id` = `ui`.`island_id` WHERE customer_number=%i;";
		$user = $db->queryOneRow($q, array($_POST['customer_number']));
	
		if($user == NULL){
			echo json_encode(array("success" => "false", "errors" => "Customer does not exist!<br>"));
			die();
		}
		
		if($user['is_disabled'] == 1){
			echo json_encode(array("success" => "false", "errors" => "Customer is disabled!<br>"));
			die();
		}

		if($user['google_auth_enabled'] == "1"){
			$pin_text = "2FA Code:";
		}else{
			$pin_text = "Customer Pin:";
		}
		
		// Calculate age from DOB
		if($user['date_of_birth'] != 0000-00-00){
		
		$dob_dt = new DateTime($user['date_of_birth']);
		$now_dt = new DateTime();
		$age_interval = $now_dt->diff($dob_dt);		
		$dob = $dob_dt->format("m/d/Y");
		$age = $core->format_interval($age_interval);
		}
		else{ 
		$age ="";
		$dob ="";	}
		
		// customer data to display
		$customer_info['id'] = $user['user_id'];
		$customer_info['name'] = $user['firstname']." ".$user['lastname'];
		$customer_info['dob'] = $dob;
		$customer_info['age'] = $age;
		$customer_info['gender'] = $user['gender'];
		$customer_info['address'] = $user['address']." ".$user['address2'].", ".$user['city'];
		$customer_info['island'] = $user['island'];
		$customer_info['phone'] = $user['telephone'].",".$user['cellphone'];
		$customer_info['picture_path'] = $user['profile_picture_path'];
		
		echo json_encode(array("success" => "true", "pin_text" => $pin_text, "customer_info" => $customer_info));
	}
	else{
	
	$q = "UPDATE customers SET card_number = %s WHERE user_id = %i";
	$db->query($q, array($_POST['card_number'],$_POST['user_id']));

	echo json_encode(array("result"=>true));
	}
	
