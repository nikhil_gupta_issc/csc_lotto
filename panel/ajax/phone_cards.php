<?php
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/api/emida/api.php");
	
	$phone_card_api = new PHONE_CARD_API();
	$phone_card_api->login2();
	
	$response = array();

	$action = isset($_POST['action']) ? $_POST['action'] : NULL;
	$carrier_id = isset($_POST['carrier_id']) ? $_POST['carrier_id'] : NULL;
	$product_id = isset($_POST['product_id']) ? $_POST['product_id'] : NULL;
	$category_id = isset($_POST['category_id']) ? $_POST['category_id'] : NULL;
	$product_amount = isset($_POST['product_amount']) ? $_POST['product_amount'] : NULL;
	
	$now_dt = new DateTime();
	$now = $now_dt->format("Y-m-d H:i:s");
	
	switch ($action){
		case 'get_products':
			$response['options'] = "";
		
			$categories = $phone_card_api->get_category_list("",$carrier_id);
			foreach($categories as $category){
				$products = $phone_card_api->get_product_list("", $carrier_id, $category['CategoryId']);
				foreach($products as $product){
					if($product["TransTypeId"] == "2"){
						$response['options'] .= "<option data-category_id='".$category['CategoryId']."' value='".$product['ProductId']."'>".$category['Description']." - ".$product['ShortDescription']."</option>";
					}
				}
			}

			$response['success'] = true;
			break;
		case 'purchase_product':
			//Get the purchase amount
			$products = $phone_card_api->get_product_list("", $carrier_id, "", $product_id);
		
			//Buy the phone card
			$phone_card_info = $phone_card_api->pin_dist_sale($product_id, "", $products[0]["Amount"]);
			
			//Create a panel user transaction
			$id = $core->make_panel_user_transaction($products[0]["Amount"], 19, "Sold Phone Card - ".$phone_card_info["Pin"]);
			
			//Add the phone card information to the phone card table
			$q = "INSERT INTO `phone_cards` (`panel_user_id`, `pin`, `emida_transaction_id`, `created_on`, `transaction_id`, `control_number`, `response_message`) VALUES (%i, %s, %i, %s, %i, %s, %s)";
			$phone_card_id = $db->queryInsert($q, array($session->userinfo["id"], $phone_card_info["Pin"], $phone_card_info["TransactionId"], $now, $id, $phone_card_info["ControlNo"], $phone_card_info["ResponseMessage"]));
			
			$response['phone_card_id'] = $phone_card_id;
			$response['success'] = true;
			break;
		default:
			$response['success'] = false;
	}
	
	echo json_encode($response);
