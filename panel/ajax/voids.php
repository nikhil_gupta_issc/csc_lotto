<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
date_default_timezone_set('America/New_York');

if($session->userinfo['panel_user']['shift_id'] == "-1"){
	echo json_encode(array("success" => "false", "errors" => "You must start a shift!"));
}else{
	if(isset($_POST['action']) && $_POST['action'] == "void_ticket"){
		$date = new datetime('now');
		$date_str = $date->format("Y-m-d H:i:s");

		//$ticket_info = $db->query("SELECT * FROM `lotto_ticket` t JOIN `lotto_bet` b ON t.ticket_id=b.ticket_id WHERE t.ticket_number=".$_POST['ticket_number']);
		$q = "SELECT * FROM `lotto_ticket` t JOIN `lotto_bet` b ON t.ticket_id=b.ticket_id WHERE t.ticket_number=%i;"; 
		$ticket_info = $db->query($q, array($_POST['ticket_number'])); 

		foreach($ticket_info as $bet){
			if($bet['is_processed'] == 1){
				$return = array("success" => "false", "errors" => "Ticket Already Has Processed Bets");
				echo json_encode($return);
				die();
			}
		}

		if($ticket_info != array()){
			if($ticket_info[0]["cashier_id"] != $session->userinfo['id']){
				$return = array("success" => "false", "errors" => "You can only void a ticket that was sold by you.");
			}elseif($ticket_info[0]["is_voided"] == "1"){
				$return = array("success" => "false", "errors" => "This ticket is already voided.");
			}else{
				//$db->queryDirect("UPDATE `lotto_ticket` SET is_voided=1, voided_by=".$session->userinfo['id'].", void_approved_by=".$session->userinfo['id'].", voided_on='".$date_str."' WHERE ticket_number=".$_POST['ticket_number']);
				$q = "UPDATE `lotto_ticket` SET is_voided=1, voided_by=%i, void_approved_by=%i, voided_on=%s, voided_comment=%s WHERE ticket_number=%i;";
				$db->queryDirect($q, array($session->userinfo['id'], $session->userinfo['id'], $date_str, $_POST['reason'], $_POST['ticket_number']));

				//$db->queryDirect("UPDATE `panel_user_transaction` SET is_voided=1, voided_by=".$session->userinfo['id'].", voided_on='".$date_str."' WHERE ticket_number=".$_POST['ticket_number']);
				$q = "UPDATE `panel_user_transaction` SET is_voided=1, voided_by=%i, voided_on=%s WHERE ticket_number=%i;";
				$db->queryDirect($q, array($session->userinfo['id'], $date_str, $_POST['ticket_number']));				

				//$amount = $db->queryOneRow("SELECT amount FROM `panel_user_transaction` WHERE ticket_number='".$_POST['ticket_number']."' AND type_id='7'");
				$q = "SELECT amount FROM `panel_user_transaction` WHERE ticket_number=%i AND type_id='7';";
				$amount = $db->queryOneRow($q, array($_POST['ticket_number']));

				$trans_id = $core->make_panel_user_transaction(-$amount['amount'], 8, "Voided ticket number ".$_POST['ticket_number'], $_POST['ticket_number']);
				$curr_balance = $core->check_balance("panel_user");

				$return = array("success" => "true", "transaction_id" => $trans_id, "balance" => number_format($curr_balance['balance']) );
			}
		}else{
			$return = array("success" => "false", "errors" => "Ticket Does Not Exist");
		}

		echo json_encode($return);
	}
}
