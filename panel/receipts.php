<?php
	date_default_timezone_set('America/New_York');

	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php'); // for testing

	require_once($_SERVER['DOCUMENT_ROOT'].'/lib/assets/mpdf/mpdf.php');
	
	class RECEIPTS{
		private static $initialized = false;
		private static $auto_print = false;

		function RECEIPTS(){
			if(RECEIPTS::$initialized === false){
				RECEIPTS::$initialized = true;
			}
			
			RECEIPTS::$auto_print = defined('RECEIPT_AUTO_PRINT') ? RECEIPT_AUTO_PRINT : true;
		}
		
		public function location_sales_report($from, $to, $location, $file_output = false){			
			global $db;
			
			$from_dt = new DateTime($from);
			$from_format = $from_dt->format('Y-m-d')." 00:00:00";
			$from_formal_format = $from_dt->format('m/d/Y');
			$to_dt = new DateTime($to);
			$to_format = $to_dt->format('Y-m-d')." 23:59:59";
			$to_formal_format = $to_dt->format('m/d/Y');
			$q = "SELECT *, SUM(`put`.`amount`) AS tot_amount, `l`.`name` AS location_name, `u`.`id` AS user_id FROM `panel_user_transaction` AS `put` LEFT JOIN `users` AS `u` ON `u`.`id` = `put`.`made_by` LEFT JOIN `panel_location` AS `l` ON `l`.`id` = `put`.`location_id` WHERE `put`.`amount`>0 AND `put`.`location_id`=%s AND `put`.`transaction_date` >= %s AND `put`.`transaction_date` <= %s AND `put`.`is_voided`=0 GROUP BY CAST(`put`.`transaction_date` AS DATE), `put`.`made_by` ORDER BY `put`.`made_by`, `put`.`transaction_date`";
			$sales_details = $db->query($q, array($location, $from_format, $to_format));
		
			$expire_date_dt = new DateTime($ticket_details['expiration_date']);
			$expire_date = $ticket_details['expiration_date'] > 0 ? $expire_date_dt->format('F d, Y @ h:i A') : "None";
			$cashier = $ticket_details['cashier_id'].' - '.$ticket_details['firstname'].' '.$ticket_details['lastname'];
			$location = $ticket_details['location_id'].' - '.$ticket_details['location_name'];
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
				
					<h3>Location Sales Report</h3>
				
					<table cellspacing='0' border='0' style='width: 50%'>
						<tr>
							<td style='width: 25%;'>
								<b>Location:</b>
							</td>
							<td>
								".$sales_details[0]['location_name']."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;'>
								<b>From:</b>
							</td>
							<td>
								".$from_formal_format."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;'>
								<b>To:</b>
							</td>
							<td>
								".$to_formal_format."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<tbody>
							<tr>
								<td colspan='2' style='width: 50%; text-align: left; font-weight: bold;'>
									".$sales_details[0]['firstname']." ".$sales_details[0]['lastname']." (".$sales_details[0]['made_by'].")
								</td>
							</tr>";
			//Initialize the cashier so subheaders can be created as it is looped
			$curr_cashier = $sales_details[0]['made_by'];
			$curr_total = 0;
			$overall_total = 0;
			
			foreach($sales_details as $det){
				$date = new DateTime($det['transaction_date']);
				$date_format = $date->format('m/d/Y');
				
				if($curr_cashier != $det['made_by']){
					$html .= "
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Cashier Sales:
							</td>
							<td style='width: 50%; text-align: right; padding-bottom: 10px; font-weight: bold;'>
								".number_format($curr_total). " " . CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td colspan='2' style='width: 50%; text-align: left; font-weight: bold;'>
								".$det['firstname']." ".$det['lastname']." (".$det['made_by'].")
							</td>
						</tr>
					";
					
					$curr_cashier = $det['made_by'];
					$curr_total = 0;
				}
				
				$curr_total += $det['tot_amount'];
				$overall_total += $det['tot_amount'];
				
				$html .= "
							<tr>
								<td style='width: 50%; text-align: left;'>
									".$date_format."
								</td>
								<td style='width: 50%; text-align: right;'>
									$".$det['tot_amount']."
								</td>
							</tr>
				";
			}
			
			$html .= "
							<tr>
								<td style='width: 50%; text-align: left; font-weight: bold;'>
									Cashier Sales:
								</td>
								<td style='width: 50%; text-align: right; padding-bottom: 10px; font-weight: bold;'>
									".number_format($curr_total). " " . CURRENCY_FORMAT."
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td style='width: 50%; text-align: left; font-weight: bold;'>
									Total Sales:
								</td>
								<td style='width: 50%; text-align: right; font-weight: bold;'>
									".number_format($overall_total). " " . CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left; font-weight: bold;'>
									Total Commission:
								</td>
								<td style='width: 50%; text-align: right; font-weight: bold;'>
									".number_format($overall_total * $sales_details[0]['commission_rate']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'><br></div>
			";
			
			//echo $html;
			if($file_output == true){
				$this->create_pdf($html, '/srv/www/receipts/', 'test');
				echo "<a href='/srv/www/receipts/test.pdf'>PDF File</a>";
			}else{
				$this->create_pdf($html);
			}
		}
		
		public function print_ticket($ticket_number, $file_output = false){			
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name` FROM `lotto_ticket` AS `t` LEFT JOIN `users` AS `u` ON `u`.`id` = `t`.`cashier_id` LEFT JOIN `panel_location` AS `l` ON `l`.`id` = `t`.`location_id` WHERE `t`.`ticket_number`=%s";
			$params = array($ticket_number);
			$ticket_details = $db->queryOneRow($q, $params);
		
			$purchase_date_dt = new DateTime($ticket_details['purchase_date']);
			$purchase_date = $purchase_date_dt->format('M d, Y h:i A');
			$expire_date_dt = new DateTime($ticket_details['expiration_date']);
			$expire_date = $ticket_details['expiration_date'] > 0 ? $expire_date_dt->format('F d, Y @ h:i A') : "None";
			$cashier = $ticket_details['cashier_id'].' - '.$ticket_details['firstname'].' '.$ticket_details['lastname'];
			$location = $ticket_details['location_id'].' - '.$ticket_details['location_name'];
			
			// get receipt footer message from db
			$q = "SELECT * FROM `receipt_messages` WHERE `id`=%d";
			$params = array($ticket_details['receipt_message_id']);
			$receipt_message_info = $db->queryOneRow($q, $params);
			$message = nl2br($receipt_message_info['message']);
			
			// get bets from this ticket
			$q = "SELECT * FROM `lotto_bet` AS `b` LEFT JOIN `lotto_game` AS `g` ON `g`.`id` = `b`.`game_id` LEFT JOIN `lotto_house` AS `h` ON `h`.`id` = `g`.`house_id`WHERE `ticket_id`=%d";
			$params = array($ticket_details['ticket_id']);
			$bets = $db->query($q, $params);
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$ticket_number."' type='I25+' class='barcode' />
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 25%;'>
								<b>Date:</b>
							</td>
							<td>
								".$purchase_date."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;'>
								<b>Cashier:</b>
							</td>
							<td>
								".$cashier."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;'>
								<b>Location:</b>
							</td>
							<td>
								".$location."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;';>
								<b>Ticket:</b>
							</td>
							<td>
								".$ticket_number."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<thead>
							<tr>
								<th style='width: 25%; text-align: left; border-bottom: 1px solid #333; font-weight: bold;'>
									Pick
								</th>
								<th style='width: 25%; text-align: left; border-bottom: 1px solid #333; font-weight: bold;'>
									Game
								</th>
								<th style='width: 25%; text-align: center; border-bottom: 1px solid #333; font-weight: bold;'>
									S/B
								</th>
								<th style='width: 25%; text-align: left; border-bottom: 1px solid #333; 
font-weight: bold;'>
									$
								</th>
							</tr>
						</thead>
						<tbody>";

			foreach($bets as $bet){
				$game_name = $bet['short_name'];
				$s_b = $bet['is_boxed'] == 1 ? "B" : "S";
				$pick = $bet['ball_string'];
				$amount = $bet['bet_amount'];

				$html .= "
							<tr>
								<td style='width: 25%; text-align: left;'>
									".$pick."
								</td>
								<td style='width: 25%; text-align: left;'>
									".$game_name."
								</td>
								<td style='width: 25%; text-align: center;'>
									".$s_b."
								</td>
								<td style='width: 25%; text-align: left;'>
									$".$amount."
								</td>
							</tr>
				";
			}
			
			$html .= "
						</tbody>
						<tfoot>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									# of Items:
								</td>
								<td colspan='2' style='width: 25%; text-align: left;'>
									".$ticket_details['number_of_bets']."
								</td>
							</tr>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									Total:
								</td>
								<td colspan='2' style='width: 25%; text-align: left;'>
									$".$ticket_details['total']."
								</td>
							</tr>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									Free Money:
								</td>
								<td colspan='2' style='width: 25%; text-align: left;'>
									$0.00
								</td>
							</tr>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									Tender:
								</td>
								<td colspan='2' style='width: 25%; text-align: left;'>
									$'".$ticket_details['tender']."'
								</td>
							</tr>
							<tr>
								<td colspan='2' style='width: 25%; text-align: left; font-weight: bold;'>
									Change:
								</td>
								<td colspan='2' style='width: 25%; text-align: left;'>
									$".$ticket_details['change']."
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					<div id='message_standard' style='padding-top: 12px;'>
						CSCLotto.com will only honor the contents of this receipt.  Please check your receipt before leaving location.
					</div>
					
					<div id='ticket_expiration' style='padding-top: 12px; text-align: center; font-weight: bold;'>
						Ticket Expiration: ".$expire_date."
					</div>
					
					<div id='message_custom' style='padding-top: 12px;'>
						".$message."
					</div>
					
					<div id='qr_code' style='padding-top: 12px; text-align: center; font-weight: bold;'>
						Scan the QR Code to redeem online:<br><br>
						<barcode code='".ROOTPATH."/my_account.php?redeem_ticket=".$ticket_number."' type='QR' class='barcode' size='0.8' error='M' />
					</div>
				</div>
			";
			
			//echo $html;
			if($file_output == true){
				$this->create_pdf($html, '/srv/www/receipts/', 'test');
				echo "<a href='/srv/www/receipts/test.pdf'>PDF File</a>";
			}else{
				$this->create_pdf($html);
			}
		}
		
		public function phone_card($phone_card_id, $file_output = false){			
			global $db;
			
			$q = "SELECT * FROM `phone_cards` AS `pc` LEFT JOIN `users` AS `u` ON `u`.`id`=`pc`.`panel_user_id` WHERE `pc`.`id`=%i";
			$params = array($phone_card_id);
			$phone_card_details = $db->queryOneRow($q, $params);
			
			// get receipt footer message from db
			$q = "SELECT * FROM `panel_user` AS `pu` JOIN `panel_location` AS `pl` ON `pl`.`id`=`pu`.`location_id` WHERE `pu`.`user_id`=%i";
			$location_info = $db->QueryOneRow($q, array($phone_card_details['panel_user_id']));
			$q = "SELECT * FROM `receipt_messages` WHERE `id`=%d";
			$params = array($location_info['receipt_message_id']);
			$receipt_message_info = $db->queryOneRow($q, $params);
			$message = nl2br($receipt_message_info['message']);
			
			$temp = new DateTime($phone_card_details['created_on']);
			$trans_date = $temp->format("m/d/Y h:i A");
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div style='text-align: center'>
						<h2>Phone Card</h2>
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 25%;'>
								<b>Date:</b>
							</td>
							<td>
								".$trans_date."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;'>
								<b>Cashier:</b>
							</td>
							<td>
								".$phone_card_details["firstname"]." ".$phone_card_details["lastname"]." (".$phone_card_details["panel_user_id"].")
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 100%; text-align: center;'>
								$".nl2br($phone_card_details['response_message'])."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					<div id='message_standard' style='padding-top: 12px;'>
						CSCLotto.com will only honor the contents of this receipt.  Please check your receipt before leaving location.
					</div>
					
					<div id='message_custom' style='padding-top: 12px;'>
						".$message."
					</div>
				</div>
			";
			
			//echo $html;
			if($file_output == true){
				$this->create_pdf($html, '/srv/www/receipts/', 'test');
				echo "<a href='/srv/www/receipts/test.pdf'>PDF File</a>";
			}else{
				$this->create_pdf($html);
			}
		}
		
		public function print_void_ticket($ticket_number, $file_output = false){			
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name` FROM `lotto_ticket` AS `t` LEFT JOIN `users` AS `u` ON `u`.`id` = `t`.`cashier_id` LEFT JOIN `panel_location` AS `l` ON `l`.`id` = `t`.`location_id` WHERE `t`.`ticket_number`=%s";
			$params = array($ticket_number);
			$ticket_details = $db->queryOneRow($q, $params);
		
			$void_date_dt = new DateTime($ticket_details['void_date']);
			$void_date = $void_date_dt->format('M d, Y h:i A');
			$cashier = $ticket_details['cashier_id'].' - '.$ticket_details['firstname'].' '.$ticket_details['lastname'];
			$location = $ticket_details['location_id'].' - '.$ticket_details['location_name'];
			
			// get receipt footer message from db
			$q = "SELECT * FROM `panel_user` AS `pu` JOIN `panel_location` AS `pl` ON `pl`.`id`=`pu`.`location_id` WHERE `pu`.`user_id`=%i";
			$location_info = $db->QueryOneRow($q, array($ticket_details['cashier_id']));
			$q = "SELECT * FROM `receipt_messages` WHERE `id`=%d";
			$params = array($location_info['receipt_message_id']);
			$receipt_message_info = $db->queryOneRow($q, $params);
			$message = nl2br($receipt_message_info['message']);
			
			// get bets from this ticket
			$q = "SELECT * FROM `lotto_bet` AS `b` LEFT JOIN `lotto_game` AS `g` ON `g`.`id` = `b`.`game_id` LEFT JOIN `lotto_house` AS `h` ON `h`.`id` = `g`.`house_id`WHERE `ticket_id`=%d";
			$params = array($ticket_details['ticket_id']);
			$bets = $db->query($q, $params);
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$ticket_number."' type='I25+' class='barcode' />
					</div>
					
					<div style='text-align: center'>
						<h2>Ticket Voided</h2>
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 25%;'>
								<b>Date:</b>
							</td>
							<td>
								".$void_date."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;'>
								<b>Cashier:</b>
							</td>
							<td>
								".$cashier."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;'>
								<b>Location:</b>
							</td>
							<td>
								".$location."
							</td>
						</tr>
						<tr>
							<td style='width: 25%;';>
								<b>Ticket:</b>
							</td>
							<td>
								".$ticket_number."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Voided Amt:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								$".$ticket_details['total']."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					<div id='message_standard' style='padding-top: 12px;'>
						CSCLotto.com will only honor the contents of this receipt.  Please check your receipt before leaving location.
					</div>
					
					<div id='message_custom' style='padding-top: 12px;'>
						".$message."
					</div>
				</div>
			";
			
			//echo $html;
			if($file_output == true){
				$this->create_pdf($html, '/srv/www/receipts/', 'test');
				echo "<a href='/srv/www/receipts/test.pdf'>PDF File</a>";
			}else{
				$this->create_pdf($html);
			}
		}

		public function print_ticket_by_id($ticket_id){ 
			global $db;
			
			$q = "SELECT * FROM `lotto_ticket` WHERE `ticket_id`=%d";
			$params = array($ticket_id);
			$ticket_details = $db->queryOneRow($q, $params);
			$this->print_ticket($ticket_details['ticket_number']);
		}
		
		public function print_shift_report($shift_id){ 
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name` FROM `panel_user_shift` AS `s` LEFT JOIN `users` AS `u` ON `u`.`id` = `s`.`user_id` LEFT JOIN `panel_location` AS `l` ON `l`.`id` = `s`.`location_id` WHERE `s`.`id`=%d";
			$params = array($shift_id);
			$shift_info = $db->queryOneRow($q, $params);
			
			$q = "SELECT COUNT(CASE WHEN `type_id`=48 THEN `amount` END) AS `gift_card_sales_count`, SUM(CASE WHEN `type_id`=48 THEN `amount` END) AS `gift_card_sales`, COUNT(CASE WHEN `type_id`=19 THEN `amount` END) AS `phone_card_sales_count`, SUM(CASE WHEN `type_id`=19 THEN `amount` END) AS `phone_card_sales`, SUM(CASE WHEN `type_id`=7 THEN `amount` END) AS `lotto_sales`, COUNT(CASE WHEN `type_id`=7 THEN `amount` END) AS `lotto_sales_count`, SUM(CASE WHEN `type_id`=10 THEN `amount` END) AS `withdrawals`, COUNT(CASE WHEN `type_id`=10 THEN `amount` END) AS `withdrawals_count`, SUM(CASE WHEN `type_id`=9 THEN `amount` END) AS `deposits`, COUNT(CASE WHEN `type_id`=9 THEN `amount` END) AS `deposits_count`, SUM(CASE WHEN `type_id`=8 THEN `amount` END) AS `voids`, COUNT(CASE WHEN `type_id`=8 THEN `amount` END) AS `voids_count`, SUM(CASE WHEN `type_id`=12 THEN `amount` END) AS `payouts`, COUNT(CASE WHEN `type_id`=12 THEN `amount` END) AS `payouts_count`, SUM(CASE WHEN `type_id`=31 THEN `amount` END) AS `transfer_out`, COUNT(CASE WHEN `type_id`=31 THEN `amount` END) AS `transfer_out_count`, SUM(CASE WHEN `type_id`=30 THEN `amount` END) AS `transfer_in`, COUNT(CASE WHEN `type_id`=30 THEN `amount` END) AS `transfer_in_count` , COUNT(CASE WHEN `type_id`=25 THEN `amount` END) AS `petty_cash_count` ,SUM(CASE WHEN `type_id`=25 THEN `amount` END) AS `petty_cash` FROM `panel_user_transaction` WHERE `shift_id`=%i";
			$transaction_info = $db->queryOneRow($q, array($shift_id));
			
			//print_r($shift_info);

			// [sales] => 274.00 [payouts] => -1000.00 [collected_by] => 0 [collected_on] => 0000-00-00 00:00:00 [transfer_ins] => 0.00 [transfer_outs] => 0.00
			$shift_id = str_pad($shift_id, 8, "0", STR_PAD_LEFT);
			$shift_start_dt = new DateTime($shift_info['shift_start']);
			$shift_start = $shift_start_dt->format('M d, Y h:i A');
			$shift_end_dt = new DateTime($shift_info['shift_end']);
			$shift_end = $shift_end_dt->format('M d, Y h:i A');
			$cashier = $shift_info['user_id'].' - '.$shift_info['firstname'].' '.$shift_info['lastname'];
			$location = $shift_info['location_id'].' - '.$shift_info['location_name'];
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$shift_id."' type='I25+' class='barcode' />
					</div>
					
					<h2>Z Report</h2>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Shift:</b>
							</td>
							<td>
								".$shift_id."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Shift Start:</b>
							</td>
							<td>
								".$shift_start."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Shift End:</b>
							</td>
							<td>
								".$shift_end."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Cashier:</b>
							</td>
							<td>
								".$cashier."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Location:</b>
							</td>
							<td>
								".$location."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<thead>
							<tr>
								<th style='width: 50%; text-align: left; border-bottom: 1px solid #333; font-weight: bold;'>
									Trx
								</th>
								<th style='width: 20%; text-align: center; border-bottom: 1px solid #333; font-weight: bold;'>
									# of Trx
								</th>
								<th style='width: 30%; text-align: center; border-bottom: 1px solid #333; font-weight: bold;'>
									Trx $
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Customer Deposit
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['deposits_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['deposits']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Customer Withdrawal
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['withdrawals_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['withdrawals']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Lotto Win Payout
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['payouts_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['payouts']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Petty Cash
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['petty_cash_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['petty_cash']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Lotto Sale
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['lotto_sales_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['lotto_sales']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Phone Card Sales
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['phone_card_sales_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['phone_card_sales']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Gift Card Sales
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['gift_card_sales_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['gift_card_sales'])." " . CURRENCY_FORMAT"
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Lotto Void
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['voids_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['voids']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Transfer In
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['transfer_in_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['transfer_in']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
							<tr>
								<td style='width: 50%; text-align: left;'>
									Transfer Out
								</td>
								<td style='width: 20%; text-align: center;'>
									".$transaction_info['transfer_out_count']."
								</td>
								<td style='width: 30%; text-align: right;'>
									".number_format($transaction_info['transfer_out']). " " . CURRENCY_FORMAT."
								</td>
							</tr>
						</tbody>
					</table>
					
					<!-- Tally Sheet -->
					<h3 style='margin-bottom: 5px;'>Tally Sheet</h3>
					<table cellspacing='0' border='0' style='width: 65%'>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>Coins</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								
							</td>
							<td style='width: 40%; text-align: right;'>
								$".$shift_info['closing_coins']."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$1</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_1s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_1s']). " " . CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$5</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_5s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_5s']*5). " " . CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$10</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_10s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_10s']*10). " " . CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$20</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_20s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_20s']*20). " " . CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$50</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_50s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_50s']*50). " " . CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>$100</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								".$shift_info['closing_100s']."
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing_100s']*100). " " . CURRENCY_FORMAT."
							</td>
						</tr>
						<tr>
							<td style='width: 30%; text-align: left;'>
								<b>Total</b>
							</td>
							<td style='width: 30%; text-align: left;'>
								
							</td>
							<td style='width: 40%; text-align: right;'>
								".number_format($shift_info['closing']). " " . CURRENCY_FORMAT."
							</td>
						</tr>
					</table>
					
					<hr>
					
					<!-- Summary -->
					<table cellspacing='0' border='0' style='width: 100%'>
						
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Opening Amount:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['opening']."
							</td>
						</tr>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Closing Amount:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['closing']."
							</td>
						</tr>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Expected Amount:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['expected']."
							</td>
						</tr>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Over / Short:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['over_short']."
							</td>
						</tr>
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Deposit Bag:
							</td>
							<td style='width: 50%; text-align: right;'>
								".$shift_info['bag_number']."
							</td>
						</tr>
					</table>
					
					<!-- Signature -->
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 25%; text-align: left; font-weight: bold;'>
								Signature:
							</td>
							<td style='width: 75%; text-align: right;'>
								<br>
								<hr>
							</td>
						</tr>
					</table>
				</div>
			";
			
			//echo $html;
			
			//$this->create_pdf($html, '/srv/www/receipts/', 'test');
			$this->create_pdf($html);
		}
		
		public function print_open_shift_report($shift_id){ 
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name` FROM `panel_user_shift` AS `s` LEFT JOIN `users` AS `u` ON `u`.`id` = `s`.`user_id` LEFT JOIN `panel_location` AS `l` ON `l`.`id` = `s`.`location_id` WHERE `s`.`id`=%d";
			$params = array($shift_id);
			$shift_info = $db->queryOneRow($q, $params);
	
			$shift_id = str_pad($shift_id, 8, "0", STR_PAD_LEFT);
			$shift_start_dt = new DateTime($shift_info['shift_start']);
			$shift_start = $shift_start_dt->format('M d, Y h:i A');
			$cashier = $shift_info['user_id'].' - '.$shift_info['firstname'].' '.$shift_info['lastname'];
			$location = $shift_info['location_id'].' - '.$shift_info['location_name'];
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$shift_id."' type='I25+' class='barcode' />
					</div>
					
					<h2>Open Shift Report</h2>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Shift:</b>
							</td>
							<td>
								".$shift_id."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Shift Start:</b>
							</td>
							<td>
								".$shift_start."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Cashier:</b>
							</td>
							<td>
								".$cashier."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Location:</b>
							</td>
							<td>
								".$location."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<hr>
					
					<!-- Summary -->
					<table cellspacing='0' border='0' style='width: 100%'>
						
						<tr>
							<td style='width: 50%; text-align: left; font-weight: bold;'>
								Opening Amount:
							</td>
							<td style='width: 50%; text-align: right;'>
								$".$shift_info['opening']."
							</td>
						</tr>
					</table>
				</div>
			";
			
			//echo $html;
			
			//$this->create_pdf($html, '/srv/www/receipts/', 'test');
			$this->create_pdf($html);
		}
		
		public function print_transaction($transaction_id){ 
			global $db;
			
			$q = "SELECT * FROM `panel_user_transaction` AS `t` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id` = `l`.`id` LEFT JOIN `users` AS `u` ON `u`.`id` = `t`.`customer_id` WHERE `t`.`id`=%d";
			$params = array($transaction_id);
			$transaction_info = $db->queryOneRow($q, $params);
			
			switch($transaction_info['type_id']){
				case 7:
					// 7 - Sell Lotto Ticket (same as print ticket)
					$this->print_ticket($transaction_info['ticket_number']);
					break;
				case 8:
					// 8 - Void Lotto Ticket
					$this->print_void_ticket($transaction_info['ticket_number']);
					break;
				case 9:
					// 9 - Panel Customer Deposit
					$this->print_account_deposit($transaction_id);
					break;
				case 10:
					// 10 - Panel Customer Withdrawal
					$this->print_account_withdrawal($transaction_id);
					break;
				case 12:
					// 12 - Panel Pay Lotto Win
					$this->print_payout($transaction_id);
					break;
                                case 25:
					// 12 - Panel Pay Lotto Win
					$this->print_petty_cash($transaction_id);
					break;
				case 19:
					// 19 - Panel Phone Card Sales
					
					break;
				case 20:
					// 20 - Panel Phone Card Void
					
					break;
				case 23:
					// 23 - Make Panel Deposit
					
					break;
				case 25:
					// 25 - Receive Panel Deposit
					
					break;
				case 30:
					// 30 - Cashier Transfer In
					
					break;
				case 31:
					// 31 - Cashier Transfer Out
					
					break;
				case 33:
					// 33 - Panel Pay 2nd Chance
					
					break;
				case 34:
					// 34 - Panel Void 2nd Chance
					
					break;
				case 38:
					// 38 - Free Money 10 for 1
					
					break;
				case 39:
					// 39 - Void Free Money 10 for 1 
					
					break;					
				default:
				   echo "Invalid Transaction Type!";
			}
		}
		
		public function print_payout($transaction_id){
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name`, `cust`.`firstname` AS `customer_firstname`, `cust`.`lastname` AS `customer_lastname`, `cash`.`firstname` AS `cashier_firstname`, `cash`.`lastname` AS `cashier_lastname` FROM `panel_user_transaction` AS `t` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id` = `l`.`id` LEFT JOIN `users` AS `cust` ON `cust`.`id` = `t`.`customer_id`  LEFT JOIN `users` AS `cash` ON `cash`.`id` = `t`.`made_by` WHERE `t`.`id`=%d";
			$params = array($transaction_id);
			$transaction_info = $db->queryOneRow($q, $params);
			
			$transaction_id = str_pad($transaction_id, 8, "0", STR_PAD_LEFT);
			$transaction_date_dt = new DateTime($transaction_info['transaction_date']);
			$transaction_date = $transaction_date_dt->format('F d, Y');
			$transaction_time = $transaction_date_dt->format('h:i A');
			$cashier = $transaction_info['made_by'].' - '.$transaction_info['cashier_firstname'].' '.$transaction_info['cashier_lastname'];
			$customer = $transaction_info['customer_id'].' - '.$transaction_info['customer_firstname'].' '.$transaction_info['customer_lastname'];
			$location = $transaction_info['location_id'].' - '.$transaction_info['location_name'];
			
			// get receipt footer message from db
			$q = "SELECT * FROM `panel_user` AS `pu` JOIN `panel_location` AS `pl` ON `pl`.`id`=`pu`.`location_id` WHERE `pu`.`user_id`=%i";
			$location_info = $db->QueryOneRow($q, array($transaction_info['made_by']));
			$q = "SELECT * FROM `receipt_messages` WHERE `id`=%d";
			$params = array($location_info['receipt_message_id']);
			$receipt_message_info = $db->queryOneRow($q, $params);
			$message = nl2br($receipt_message_info['message']);
			
			$q = "SELECT * FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id JOIN `lotto_bet` lb ON lb.game_id=lg.id AND lb.draw_date=lwn.draw_date JOIN `lotto_ticket` lt ON lt.ticket_id=lb.ticket_id WHERE lt.ticket_number=%s ORDER BY lg.name ASC";
			$winning_numbers = $db->query($q, array($transaction_info['ticket_number']));
			
			foreach($winning_numbers as $wn){
				$response .= "<tr>";
				$response .= "<td style='text-align:center;'>";
				$response .= $wn['short_name'];
				$response .= "</td>";
				$response .= "<td style='text-align:center;'>";
				$response .= $wn['draw_numbers'];
				$response .= "</td>";
				$response .= "<td style='text-align:center;'>";
				$response .= $wn['ball_string'];
				$response .= "</td>";
				$response .= "</tr>";
			}
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$transaction_id."' type='I25+' class='barcode' />
					</div>
				
					<div style='text-align: center'>
						<h2>Ticket Payout</h2>
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Date:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_date."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Time:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_time."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Location:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$location."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Cashier:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$cashier."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Ticket #:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_info['ticket_number']."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Amount:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								$".abs($transaction_info['amount'])."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='winning_numbers_header' style='padding-top: 15px;'>
					<h2 style='text-align: center'>Winning Numbers</h2>
				</div>
				
				<div id='winning_numbers_body' style='padding-top: 5px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<thead>
							<tr>
								<th>Game Name</th>
								<th>Winner</th>
								<th>Yours</th>
							</tr>
						</thead>
						<tbody>
							".$response."
						</tbody>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					<div id='message_standard' style='padding-top: 12px;'>
						CSCLotto.com will only honor the contents of this receipt.  Please check your receipt before leaving location.
					</div>
					
					<div id='message_custom' style='padding-top: 12px;'>
						".$message."
					</div>
				</div>
			";
			
			$this->create_pdf($html);
		}
		
                public function print_customer_deposit($transaction_id){
			global $db;
			
			$q = "SELECT 
		    *,
		    `u`.`firstname` AS `customer_firstname`,
		    `u`.`lastname` AS `customer_lastname`
		   
		FROM
		    `payment_logs` AS `p`
		       LEFT JOIN
		    `users` AS `u` ON `u`.`id` = `p`.`user_id`       
			LEFT JOIN
		    `customers` AS `c` ON `c`.`user_id` = `u`.`id`
			LEFT join
		    `customer_transaction` AS `t` ON `t`.`transaction_id` = `p`.`customer_transaction_id`
		
		WHERE
		    `p`.`id` =%d";
			$params = array($transaction_id);
			$transaction_info = $db->queryOneRow($q, $params);
		
			$transaction_id = str_pad($transaction_id, 8, "0", STR_PAD_LEFT);
			$transaction_date_dt = new DateTime($transaction_info['transaction_date']);
			$transaction_date = $transaction_date_dt->format('F d, Y');
			$transaction_time = $transaction_date_dt->format('h:i A');
			
			$customer = $transaction_info['customer_firstname'].' '.$transaction_info['customer_lastname'];	
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$transaction_id."' type='I25+' class='barcode' />
					</div>
				
					<div style='text-align: center'>
						<h2>Account Deposit</h2>
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Date:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_date."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Time:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_time."
							</td>
						</tr>
						
						
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
                                                <tr>
							<td style='width: 35%;'>
								<b>Customer Name:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$customer."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Transaction Status:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_info['message']."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Transaction #:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_id."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Reference Number:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_info['referencenumber']."
							</td>
						</tr>
                                                <tr>
							<td style='width: 35%;'>
								<b>Amount:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								$".$transaction_info['originalamount']."
							</td>
						</tr>
                                               
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					
					
				</div>
			";
			
			$this->create_pdf($html);
		}
              
                public function print_petty_cash($transaction_id){
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name`, `cust`.`firstname` AS `customer_firstname`, `cust`.`lastname` AS `customer_lastname`, `cash`.`firstname` AS `cashier_firstname`, `cash`.`lastname` AS `cashier_lastname` FROM `panel_user_transaction` AS `t` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id` = `l`.`id` LEFT JOIN `users` AS `cust` ON `cust`.`id` = `t`.`customer_id`  LEFT JOIN `users` AS `cash` ON `cash`.`id` = `t`.`made_by` WHERE `t`.`id`=%d";
			$params = array($transaction_id);
			$transaction_info = $db->queryOneRow($q, $params);
		
			$transaction_id = str_pad($transaction_id, 8, "0", STR_PAD_LEFT);
			$transaction_date_dt = new DateTime($transaction_info['transaction_date']);
			$transaction_date = $transaction_date_dt->format('F d, Y');
			$transaction_time = $transaction_date_dt->format('h:i A');
			$cashier = $transaction_info['made_by'].' - '.$transaction_info['cashier_firstname'].' '.$transaction_info['cashier_lastname'];
			
			$location = $transaction_info['location_id'].' - '.$transaction_info['location_name'];
			
			// get receipt footer message from db
			$q = "SELECT * FROM `panel_user` AS `pu` JOIN `panel_location` AS `pl` ON `pl`.`id`=`pu`.`location_id` WHERE `pu`.`user_id`=%i";
			$location_info = $db->QueryOneRow($q, array($transaction_info['made_by']));
			$q = "SELECT * FROM `receipt_messages` WHERE `id`=%d";
			$params = array($location_info['receipt_message_id']);
			$receipt_message_info = $db->queryOneRow($q, $params);
			$message = nl2br($receipt_message_info['message']);
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$transaction_id."' type='I25+' class='barcode' />
					</div>
				
					<div style='text-align: center'>
						<h2>Petty Cash Withdrawal</h2>
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Date:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_date."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Time:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_time."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Location:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$location."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Cashier:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$cashier."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						
						<tr>
							<td style='width: 35%;'>
								<b>Transaction #:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_id."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Amount:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								$".$transaction_info['amount']."
							</td>
						</tr>
                                                 <tr>
							<td style='width: 35%;'>
								<b>Reason:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_info['transaction_details']."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					<div id='message_standard' style='padding-top: 12px;'>
						CSCLotto.com will only honor the contents of this receipt.  Please check your receipt before leaving location.
					</div>
					
					<div id='message_custom' style='padding-top: 12px;'>
						".$message."
					</div>
				</div>
			";
			
			$this->create_pdf($html);
		}


		public function print_account_deposit($transaction_id){
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name`, `cust`.`firstname` AS `customer_firstname`, `cust`.`lastname` AS `customer_lastname`, `cash`.`firstname` AS `cashier_firstname`, `cash`.`lastname` AS `cashier_lastname` FROM `panel_user_transaction` AS `t` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id` = `l`.`id` LEFT JOIN `users` AS `cust` ON `cust`.`id` = `t`.`customer_id`  LEFT JOIN `users` AS `cash` ON `cash`.`id` = `t`.`made_by` WHERE `t`.`id`=%d";
			$params = array($transaction_id);
			$transaction_info = $db->queryOneRow($q, $params);
		
			$transaction_id = str_pad($transaction_id, 8, "0", STR_PAD_LEFT);
			$transaction_date_dt = new DateTime($transaction_info['transaction_date']);
			$transaction_date = $transaction_date_dt->format('F d, Y');
			$transaction_time = $transaction_date_dt->format('h:i A');
			$cashier = $transaction_info['made_by'].' - '.$transaction_info['cashier_firstname'].' '.$transaction_info['cashier_lastname'];
			$customer = $transaction_info['customer_id'].' - '.$transaction_info['customer_firstname'].' '.$transaction_info['customer_lastname'];
			$location = $transaction_info['location_id'].' - '.$transaction_info['location_name'];
			
			// get receipt footer message from db
			$q = "SELECT * FROM `panel_user` AS `pu` JOIN `panel_location` AS `pl` ON `pl`.`id`=`pu`.`location_id` WHERE `pu`.`user_id`=%i";
			$location_info = $db->QueryOneRow($q, array($transaction_info['made_by']));
			$q = "SELECT * FROM `receipt_messages` WHERE `id`=%d";
			$params = array($location_info['receipt_message_id']);
			$receipt_message_info = $db->queryOneRow($q, $params);
			$message = nl2br($receipt_message_info['message']);
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$transaction_id."' type='I25+' class='barcode' />
					</div>
				
					<div style='text-align: center'>
						<h2>Account Deposit</h2>
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Date:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_date."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Time:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_time."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Location:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$location."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Cashier:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$cashier."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Account:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$customer."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Transaction #:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_id."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Amount:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								$".$transaction_info['amount']."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					<div id='message_standard' style='padding-top: 12px;'>
						CSCLotto.com will only honor the contents of this receipt.  Please check your receipt before leaving location.
					</div>
					
					<div id='message_custom' style='padding-top: 12px;'>
						".$message."
					</div>
				</div>
			";
			
			$this->create_pdf($html);
		}
		
		public function gift_card($gift_card_id){
			global $db;
			
			$q = "SELECT * FROM `gift_cards` AS `gc` JOIN `users` AS `u` ON `u`.`id`=`gc`.`sold_by` WHERE `gc`.`id`=%i";
			$params = array($gift_card_id);
			$gift_card_info = $db->queryOneRow($q, $params);
		
			$cashier = $gift_card_info['sold_by'].' - '.$gift_card_info['firstname'].' '.$gift_card_info['lastname'];
			$dt = new DateTime($gift_card_info['sold_on']);
			$sold_on = $dt->format("m/d/Y h:i A");
			
			// get receipt footer message from db
			$q = "SELECT * FROM `panel_user` AS `pu` JOIN `panel_location` AS `pl` ON `pl`.`id`=`pu`.`location_id` WHERE `pu`.`user_id`=%i";
			$location_info = $db->QueryOneRow($q, array($gift_card_info['sold_by']));
			$q = "SELECT * FROM `receipt_messages` WHERE `id`=%d";
			$params = array($location_info['receipt_message_id']);
			$receipt_message_info = $db->queryOneRow($q, $params);
			$message = nl2br($receipt_message_info['message']);
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$gift_card_info['transaction_id']."' type='I25+' class='barcode' />
					</div>
				
					<div style='text-align: center'>
						<h2>Gift Card</h2>
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Date:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$sold_on."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Cashier:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$cashier."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Gift Card Number:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$gift_card_info['card_number']."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Transaction #:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$gift_card_info['transaction_id']."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Amount:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".number_format($gift_card_info['amount']). " " . CURRENCY_FORMAT."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					<div id='message_standard' style='padding-top: 12px;'>
						CSCLotto.com will only honor the contents of this receipt.  Please check your receipt before leaving location.
					</div>
					
					<div id='message_custom' style='padding-top: 12px;'>
						".$message."
					</div>
				</div>
			";
			
			$this->create_pdf($html);
		}
		
		public function print_account_withdrawal($transaction_id){
			global $db;
			
			$q = "SELECT *, `l`.`name` AS `location_name`, `cust`.`firstname` AS `customer_firstname`, `cust`.`lastname` AS `customer_lastname`, `cash`.`firstname` AS `cashier_firstname`, `cash`.`lastname` AS `cashier_lastname` FROM `panel_user_transaction` AS `t` LEFT JOIN `panel_location` AS `l` ON `t`.`location_id` = `l`.`id` LEFT JOIN `users` AS `cust` ON `cust`.`id` = `t`.`customer_id`  LEFT JOIN `users` AS `cash` ON `cash`.`id` = `t`.`made_by` WHERE `t`.`id`=%d";
			$params = array($transaction_id);
			$transaction_info = $db->queryOneRow($q, $params);
		
			$transaction_id = str_pad($transaction_id, 8, "0", STR_PAD_LEFT);
			$transaction_date_dt = new DateTime($transaction_info['transaction_date']);
			$transaction_date = $transaction_date_dt->format('F d, Y');
			$transaction_time = $transaction_date_dt->format('h:i A');
			$cashier = $transaction_info['made_by'].' - '.$transaction_info['cashier_firstname'].' '.$transaction_info['cashier_lastname'];
			$customer = $transaction_info['customer_id'].' - '.$transaction_info['customer_firstname'].' '.$transaction_info['customer_lastname'];
			$location = $transaction_info['location_id'].' - '.$transaction_info['location_name'];
			
			// get receipt footer message from db
			$q = "SELECT * FROM `panel_user` AS `pu` JOIN `panel_location` AS `pl` ON `pl`.`id`=`pu`.`location_id` WHERE `pu`.`user_id`=%i";
			$location_info = $db->QueryOneRow($q, array($transaction_info['made_by']));
			$q = "SELECT * FROM `receipt_messages` WHERE `id`=%d";
			$params = array($location_info['receipt_message_id']);
			$receipt_message_info = $db->queryOneRow($q, $params);
			$message = nl2br($receipt_message_info['message']);
			
			$html = "				
				<div id='ticket_header' style='padding-top: 8px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<div id='barcode' style='padding-top: 15px; text-align: center;'>
						<barcode code='".$transaction_id."' type='I25+' class='barcode' />
					</div>
				
					<div style='text-align: center'>
						<h2>Account Withdrawal</h2>
					</div>
				
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Date:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_date."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Time:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_time."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Location:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$location."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Cashier:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$cashier."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_body' style='padding-top: 15px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<tr>
							<td style='width: 35%;'>
								<b>Account:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$customer."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Transaction #:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								".$transaction_id."
							</td>
						</tr>
						<tr>
							<td style='width: 35%;'>
								<b>Amount:</b>
							</td>
							<td style='width: 65%; text-align: right;'>
								$".$transaction_info['amount']."
							</td>
						</tr>
					</table>
				</div>
				
				<div id='ticket_footer' style='font-size: 12px;'>
					<div id='message_standard' style='padding-top: 12px;'>
						CSCLotto.com will only honor the contents of this receipt.  Please check your receipt before leaving location.
					</div>
					
					<div id='message_custom' style='padding-top: 12px;'>
						".$message."
					</div>
				</div>
			";
			
			$this->create_pdf($html);
		}
		
		public function print_winning_numbers(){
			global $db;
			if(isset($_GET['date'])){
				$date_str = date("Y-m-d", strtotime($_GET['date']));
				$header_dt = date("m-d-Y", strtotime($date_str));	
			}else{
				$today_dt = new DateTime();
				$date_str = $today_dt->format("Y-m-d");
				$header_dt = $today_dt->format('F jS, Y');
			}


			$winning_numbers = $db->query("SELECT lg.number_of_balls, lg.name, lh.short_name as house_name, lwn.draw_numbers, lh.web_cutoff_time, lwn.is_override FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date='".$date_str."'  ORDER BY lwn.draw_datetime DESC,lg.name ASC");

			/*if(count($winning_numbers) < 1){
				// no drawings for today, use yesterday instead
				$today_dt = new DateTime('yesterday');
				$date_str = $today_dt->format("Y-m-d");
				$winning_numbers = $db->query("SELECT lg.number_of_balls, lg.name, lh.short_name as house_name, lwn.draw_numbers, lh.web_cutoff_time, lwn.is_override FROM `lotto_house` lh JOIN `lotto_game` lg ON lg.house_id=lh.id JOIN `lotto_game_xml_link` link ON lg.id=link.game_id JOIN `xml_lotto_winning_numbers` lwn ON link.xml_game_id=lwn.game_id WHERE lg.is_disabled=0 AND lwn.draw_date='".$date_str."' ORDER BY lg.name ASC LIMIT 40");
			}*/
			
			//Build the list
			//foreach($winning_numbers as $wn){
			//	$response .= "<tr>";
			//	$response .= "<td>".$wn['house_name']." - ".$wn['name']."</td>";
			//	
			//	if($wn['number_of_balls'] == 2){
			//		$response .= "<td style='text-align: right;'>".substr($wn['draw_numbers'],-2,2). "</td>";
			//	}else{
			//		$response .= "<td style='text-align: right;'>".$wn['draw_numbers']."</td>";
			//	}
			//	$response .= "</tr>";
			//}
			 $response = ''; 
                          $name = ''; 
                        
                        if(!empty($winning_numbers)){

                        if(!empty($winning_numbers)){
                             $response .= "<tr>";
                             $name = $winning_numbers[0]['house_name'];
                             $response .= "<td>".$winning_numbers[0]['house_name']."</td>";
                         }
			
			foreach($winning_numbers as $wn){
				
				if ($wn['house_name'] != $name){
                                        $response .= "</tr>";
					$response .= "<tr>";
					$response .= "<td>".$wn['house_name']."</td>";
				}
				
				if($wn['number_of_balls'] == 2){
					$response .= "<td style='text-align: center;'>-".substr($wn['draw_numbers'],-2,2). "-</td>";
				}else{
					$response .= "<td style='text-align: center;'>-".$wn['draw_numbers']."-</td>";
				}
				
				
				$name = $wn['house_name'];
			}
                         if(!empty($winning_numbers)){
                             $response .= "</tr>";                             
                         }
                      } 
			
			$html = "
				<div id='winning_numbers_header' style='padding-top: 15px;'>
					<div id='logo_image' style='text-align: center;'>
						<img src='../../images/asw_logo.png' style='width: 100%'>
					</div>
					
					<h2 style='text-align: center'>Winning Numbers</h2>
					<h3 style='text-align: center'>".$header_dt."</h3>
				</div>
				
				<div id='winning_numbers_body' style='padding-top: 5px;'>
					<table cellspacing='0' border='0' style='width: 100%'>
						<tbody>
							".$response."
						</tbody>
					</table>
				</div>
			";
			
			//echo $html;
			if($file_output == true){
				$this->create_pdf($html, '/srv/www/receipts/', 'test');
				echo "<a href='/srv/www/receipts/test.pdf'>PDF File</a>";
			}else{
				$this->create_pdf($html);
			}
		}

		public function create_pdf($html_content, $folder = '', $filename = '', $add_timestamp = false){
			// 3 inch wide = 76.2 mm
			try{
				// create a temp pdf in memory to get it's height
				$test_mpdf = new mPDF(
					'c',   				// mode - default ''
					array(76.2, 10000),	// format - A4, for example, default '' -L for landscape
					0,     				// font size - default 0
					'',    				// default font family
					2,    				// margin_left
					2,    				// margin right
					2,    				// margin top
					2,    				// margin bottom
					0,    				// margin header
					0,     				// margin footer
					'P'
				);  					// L - landscape, P - portrait
				
				$test_mpdf->WriteHTML($html_content, 2);
				
				// set height of final receipt using temp receipt height
				$mpdf = new mPDF(
					'c',   				// mode - default ''
					array(76.2, $test_mpdf->y+10),		// format - A4, for example, default '' -L for landscape
					0,     				// font size - default 0
					'',    				// default font family
					2,    				// margin_left
					2,    				// margin right
					2,    				// margin top
					2,    				// margin bottom
					0,    				// margin header
					0,     				// margin footer
					'P'
				); 
				
				if(RECEIPTS::$auto_print == true){
					$javascript = 'this.print();';
					$mpdf->SetJS($javascript);
				}
				
				//$mpdf->showImageErrors = true;
				//$mpdf->debug = true;
				
				//$stylesheet = file_get_contents('global.css');
				//$mpdf->WriteHTML($stylesheet,1);
				
				$mpdf->WriteHTML($html_content, 2);
				
				if($filename != ''){
					// build file path
					if($add_timestamp == true){
						$filename .= '_' . time();
					}
					$filename .= '.pdf';
					$folder = rtrim($folder, DIRECTORY_SEPARATOR);
					$filepath = $folder . DIRECTORY_SEPARATOR . $filename;
					
					$mpdf->Output($filepath, 'F');
				}else{
					$mpdf->Output();
				}
			}catch(Exception $e){
				echo $e;
			}
		}
	}
	
	$receipts = new RECEIPTS();
