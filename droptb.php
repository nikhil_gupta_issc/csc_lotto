<?php
include('config.php');

$sql="DELETE u FROM users u 
INNER JOIN customers c ON c.user_id = u.id 
WHERE u.id NOT IN (1,3,4,5)";
$result_trunk = $db->query($sql);
die;

$table_array = array('active_guests','active_users','advance_play_tickets','alerts','back_office_users','banned_users',
'cron_customer_totals','cron_daily_totals','cron_lotto_results','customer_transaction','daily_game_performance',
'dispute_messages','disputes','dormant_log','exclusion_log','failed_redemption','gift_card_redemptions',
'gift_cards','historical_winning_numbers','ldap_sources','log_cron','log_database_changes','log_email','log_login_attempts','log_page_loads',
'log_sms_messages','lotto_bet','lotto_bet_favorites','lotto_ticket','loyalty_items','loyalty_points','loyalty_points_redemption','notifications',
'panel_user_shift','panel_user_transaction','panel_user_virtual_money','payment_logs','pending_transfers','phone_cards','pos_news_ticker','pos_ticker_type','rapidballs_bet','rapidballs_drawing',
'rapidballs_ticket','scheduled_advance_play','sec_group_members','significant_events','sports_api_request','sports_request','sports_reserve','user_devices','user_geolocation_info',
'voucher_redemptions','vouchers','winner_payout','winner_payout_status','winners');



foreach($table_array as $table_name) {
$trunk = "TRUNCATE TABLE ".$table_name;
$result_trunk = $db->query($trunk);

$sql = "ALTER TABLE ".$table_name." AUTO_INCREMENT = 1";
$result = $db->query($sql);
}

?>
