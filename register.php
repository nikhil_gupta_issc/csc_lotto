<?php
	include("config.php");

	// see if a registration was attempted
	if(isset($_SESSION['regsuccess'])){
		if($_SESSION['regsuccess'] >= 0){
			/* Registration Successful */

			// add data to customer table
			$now_dt = new DateTime();
			$now = $now_dt->format("Y-m-d H:i:s");
			$cus_num = $core->new_customer_number();
			$card_num = $core->new_customer_card_number();
			$q = "INSERT INTO `customers` (
					`customer_id`,
					`user_id`,
					`customer_number`,
					`card_number`,
					`pin`,
					`needs_to_deposit`,
					`is_verified`,
					`verified_on`,
					`available_balance`,
					`bonus_balance`,
					`created_by`,
					`created_on`,
					`updated_by`,
					`updated_on`
				) VALUES (
					NULL,
					'".$_SESSION['user_id']."',
					'".$cus_num."',
					'".$card_num."',
					'".$_SESSION['value_array']['pin']."',
					'1',
					'0',
					'0',
					'0',
					'0',
					'0',
					'$now',
					'0',
					'$now'
				);";
			$customer_id = $db->queryInsert($q);

			$explodeDOB = explode("/", $_SESSION['value_array']['dob2']);
			$dob2 = $explodeDOB[2] . '-' . $explodeDOB[0] . '-' . $explodeDOB[1];
			$q = "INSERT INTO `user_info` (
					`user_id`,
					`cellphone`,
					`address`,
					`address2`,
					`country`,
					`city`,
					`island_id`,
					`gender`,
					`nationality`,
					`date_of_birth`
				) VALUES (
					'".$_SESSION['user_id']."',
					'".$_SESSION['value_array']['cellphone']."',
					'".$_SESSION['value_array']['address']."',
					'".$_SESSION['value_array']['address2']."',
					'".$_SESSION['value_array']['country']."',
					'".$_SESSION['value_array']['city']."',
					'".$_SESSION['value_array']['island_id']."',
					'".$_SESSION['value_array']['gender']."',
					'".$_SESSION['value_array']['nationality']."',
					'".$dob2."'
				);";
			$db->queryInsert($q);


			if($customer_id < 1){
				// couldn't add customer details...
				// undo the user account creation
				$q = "DELETE FROM `users` WHERE `id`=".$_SESSION['user_id'];
				$db->query($q);

				// set a form error to display to the user (1 = error, 0 = success)
				$_SESSION['regsuccess'] = 1;

				// add data back to POST so form can be repopulated
				$_POST = $_SESSION['value_array'];
			}
			// clear data from session
			unset($_SESSION['value_array']);
                        // save account id
			$q = "UPDATE `users` SET `csc_account_id` = %s WHERE `username`= %s";
			$db->query($q, array($_SESSION['csc_accountid'], $_SESSION['reguname']));
			// generate and get a destination tag
			$curl = curl_init('http://localhost:3001/csc/depositinfo/'.$_SESSION['user_id']);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$curl_response = curl_exec($curl);
			curl_close($curl);
			$json_deposit_info=json_decode($curl_response);
			// report tag back to brm and finish registration
			$finishurl = "https://brmdev.casinocoin.org/brm/user/".$_SESSION['csc_accountid']."/confirmRegistration";
			$curl = curl_init($finishurl);
			$post_data = json_encode(array('OperatorAccountID' => 'chwQSi2KV8hJEWSGL9cG32dviNp6LFqUKQ','DestinationTag' => $json_deposit_info->tag));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data );
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'X-API-JWT: '.$_SESSION['csc_token']));
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$curl_response = curl_exec($curl);
			curl_close($curl);
		}else{
			// registration failed
				echo " iam here";
		}
	}

	include("header.php");
	include('page_top.php');

	// handle email activations
	if(isset($_GET['activatecode'])){
		$activation_success = $core->activate_account($_GET['user'], $_GET['activatecode']);
	}

	// redirect to my account if already logged in
	if($session->logged_in){
		echo "<script>window.location='my_account.php';</script>";
		die();
	}else{
		// destroy session to prevent 2FA or form submission issues
		//session_destroy();
	}

        // populate form if we have a brm user
        if(isset($_SESSION["csc_user"])){
	   $fullname = explode(" ", $_SESSION["csc_user"]->Fullname);
	    if(count($fullname) >1){
		$firstname = array_shift($fullname);
		$lastname = implode("",$fullname);
		}else{$firstname = $_SESSION["csc_user"]->Fullname;$lastname = "";}

	   $_POST['firstname'] = $firstname;
	   $_POST['lastname'] = $lastname;
           //$_POST['firstname'] = $_SESSION["csc_user"]->Firstname;
           //$_POST['lastname'] = $_SESSION["csc_user"]->Lastname;
           $_POST['nationality'] = $_SESSION["csc_user"]->Country;
           $_POST['gender'] = strtolower($_SESSION["csc_user"]->Gender);
           $dobdatetime = new DateTime($_SESSION["csc_user"]->DateOfBirth);
           $_POST['dob'] = $dobdatetime->format('m/d/Y');
           $_POST['email'] = $_SESSION["csc_user"]->Emailaddress;
           $_POST['conf_email'] = $_SESSION["csc_user"]->Emailaddress;
           $_POST['cellphone'] = str_replace('+', '00', $_SESSION["csc_user"]->Telephonenumber);
           $_POST['address'] = $_SESSION["csc_user"]->Street;
           $_POST['address2'] = $_SESSION["csc_user"]->PostalCode.", ".$_SESSION["csc_user"]->City;
           $_POST['country'] = $_SESSION["csc_user"]->Country;
           //$_POST['username'] = strtolower(substr($_SESSION["csc_user"]->Firstname,0,1).str_replace(' ','',$_SESSION["csc_user"]->Lastname)).rand(1,99);
		$_POST['username'] = strtolower(substr($firstname,0,1).str_replace(' ','',$lastname)).rand(1,99);
        }
?>

<script type="text/javascript" src="/js/my_account.js?v=<?=JS_VERSION?>"></script>

<script>
	function getLocation(){
		// prompt for browser geolocation (most accurate)
		var x = document.getElementById("geolocation");
		if(navigator.geolocation){
			navigator.geolocation.getCurrentPosition(checkBrowserGeoLocation, browserGeoLocationError);
		}else{
			// not supported by browser
		}
	}

	function checkBrowserGeoLocation(position){
		var x = document.getElementById("geolocation");

		var latlon = position.coords.latitude + "," + position.coords.longitude;

		var x = document.getElementById("geolocation");
		var geo_url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+latlon+"&sensor=false";
		var ip_country = getLocationByIP('<?php echo $_SERVER['REMOTE_ADDR']; ?>');

		$.ajax({
			url: geo_url,
			method: "POST",
			dataType: "json"
		})
		.done(function(data){
			// data.status = "OK"
			var max_index = $(data.results).toArray().length - 1;

			x.innerHTML = "Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude + "<br>Country: " + data.results[max_index].formatted_address;
		});

		var img_url = "http://maps.googleapis.com/maps/api/staticmap?center="+latlon+"&zoom=10&size=120x90&sensor=false";

		document.getElementById("mapholder").innerHTML = "<img src='"+img_url+"'>";
	}

	function browserGeoLocationError(error){
		var x = document.getElementById("geolocation");
		switch(error.code){
			case error.PERMISSION_DENIED:
				x.innerHTML = "User denied the request for Geolocation."
				break;
			case error.POSITION_UNAVAILABLE:
				x.innerHTML = "Location information is unavailable."
				break;
			case error.TIMEOUT:
				x.innerHTML = "The request to get user location timed out."
				break;
			case error.UNKNOWN_ERROR:
				x.innerHTML = "An unknown error occurred."
				break;
		}
	}

	function getLocationByIP(ip_address){
		var x = document.getElementById("geolocation");

		var geo_url = 'http://freegeoip.net/json/'+ip_address;
		$.ajax({
			url: geo_url,
			method: "GET",
			dataType: "json"
		})
		.done(function(data){
			console.log(data);
			return data.country_name;
		});

		var geo_url = 'http://ipinfo.io/'+ip_address+'/json';
		$.ajax({
			url: geo_url,
			method: "GET",
			dataType: "json"
		})
		.done(function(data){
			console.log(data);
			return data.country_name;
		});
	}
</script>

<script>
	$(document).ready(function(){
		$('#signup_form').on('submit', function(e){
			var errors = "";
			if(!$('#privacy_policy').is(':checked')){
				errors += 'You must agree to the Privacy Policy before registering an account!<br>';
			}

			if(!$('#terms').is(':checked')){
				errors += 'You must agree to the Terms & Conditions before registering an account!<br>';
			}

			if(errors != ""){
				bootbox.alert(errors);
				e.preventDefault();
			}
		});

		$('#privacy_policy').on('change', function(e){
			if($('#privacy_policy').is(':checked') && $('#terms').is(':checked')){
				$('#register_btn').prop('disabled', false);
			}else{
				$('#register_btn').prop('disabled', true);
			}
		});

		$('#terms').on('change', function(e){
			if($('#privacy_policy').is(':checked') && $('#terms').is(':checked')){
				$('#register_btn').prop('disabled', false);
			}else{
				$('#register_btn').prop('disabled', true);
			}
		});

		<?php
			if(isset($_POST['dob'])){
				echo "$('#dob').data('DateTimePicker').clear().date(moment('".$_POST['dob']."'));";
			}
		?>

		getLocation();

		$('#signup_form')
			.find('[name="island_id"]')
				.selectpicker()
				.change(function(e) {
					// revalidate the language when it is changed
					$('#signup_form').formValidation('revalidateField', 'island');
				})
				.end()
			.formValidation({
				framework: 'bootstrap',
				excluded: ':disabled',
				icon: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					firstname: {
						validators: {
							notEmpty: {
								message: 'First name is required and cannot be empty'

							}
						}
					},
					lastname: {
						validators: {
							notEmpty: {
								message: 'Last name is required and cannot be empty'
							}
						}
					},
					country: {
						validators: {
							notEmpty: {
								message: 'Country is required and cannot be empty'
							}
						}
					},
					dob2: {
						validators: {
							date: {
								message: 'You must be 18 or older',
								<?php
									$now = new DateTime();
									$max = $now->modify("-18 years");
									$max_str = $max->format("m/d/Y");
								?>
								max: '<?php echo $max_str; ?>'
							}

						}
					},
					email: {
						validators: {
							notEmpty: {
								message: 'The email address is required and cannot be empty'
							},
							emailAddress: {
								message: 'The email address is not valid'
							}
						}
					},
					conf_email: {
						validators: {
							notEmpty: {
								message: 'The email address is required and cannot be empty'
							},
							emailAddress: {
								message: 'The email address is not valid'
							},
							identical: {
								field: 'email',
								message: 'The email and its confirmation are not the same'
							}
						}
					},
					username: {
						validators: {
							notEmpty: {
								message: 'Username is required and cannot be empty'
							},
							stringLength: {
								min: 5,
								max: 30,
								message: 'Please enter a username between %s and %s characters long'
							}
						}
					},
				/*	pin: {
						validators: {
							notEmpty: {
								message: 'PIN is required and cannot be empty'
							},
							stringLength: {
								min: 5,
								max: 8,
								message: 'Please enter a PIN between %s and %s characters long'
							},
							integer: {
								message: 'PIN must be a number'
							}
						}
					},*/
					pass: {
						validators: {
							notEmpty: {
								message: 'Password is required and cannot be empty'
							},
							stringLength: {
								min: 6,
								max: 50,
								message: 'Please enter a password between %s and %s characters long'
							},
							identical: {
								field: 'conf_pass',
								message: 'The password and its confirmation are not the same'
							}
						}
					},
					conf_pass: {
						validators: {
							notEmpty: {
								message: 'Password is required and cannot be empty'
							},
							stringLength: {
								min: 6,
								max: 50,
								message: 'Please enter a password between %s and %s characters long'
							},
							identical: {
								field: 'pass',
								message: 'The password and its confirmation are not the same'
							}
						}
					}
				}
		});
	});
</script>

<div class="clearfix" style="background:#fff;">
		<div class="row">
				<div class="container">
				   <div class="header_image">
						<img src="/images/signup_banner.jpg" class="img_grow_wide">
					</div>
				</div>
			</div>

	<div class="col-md-12" style="padding-top: 20px; padding-right: 0px; padding-left: 0px;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Create a New Account</h3>
			</div>
				<div class="panel-body" style="padding-left: 15px; padding-right: 15px;">
				<?php
					if(isset($_SESSION['regsuccess'])){
						if($_SESSION['regsuccess'] == 0){
							/* No Activation Needed and no Welcome Email */
							echo "<h3>New Account Created</h3>";
							echo "<h4>Please login!</h4>";
						}elseif($_SESSION['regsuccess'] == 3){
							/* E-mail Activation */
							echo "<h3>New Account Created</h3>";
							echo "<h4>Email Activation Required</h4>";
							echo "<br><br>Please click on the link in your email to activate your account.";
						}elseif($_SESSION['regsuccess'] == 4){
							/* Admin Activation */
							echo "<h3>New Account Created</h3>";
							echo "<h4>Admin Activation Required</h4>";
							echo "<br><br>Please wait for an administrator to review and activate your account.";
						}elseif($_SESSION['regsuccess'] == 5){
							/* No Activation Needed but E-mail going out */
							echo "<h3>New Account Created</h3>";
							echo "<h4>Please login!</h4>";
						}elseif($_SESSION['regsuccess'] == 5){
							/* No Activation Needed but E-mail going out */
							echo "<h3 style='color:red;'>ERROR: New user registration has been disabled.</h3>";
						}elseif($_SESSION['regsuccess'] == 2){
							/* Registration attempt failed */
							echo "<h3 style='color:red;'>ERROR: Account Could Not Be Created</h3>";
							if(isset($_SESSION['error_array'])){
								foreach($_SESSION['error_array'] as $err){
									echo "<br><h4 style='color:red;'>$err</h4>";
								}
							}
						}else{
							/* Error found with form */
							echo "<h3 style='color:red;'>ERROR: Account Could Not Be Created</h3>";
							if(isset($_SESSION['error_array'])){
								foreach($_SESSION['error_array'] as $err){
									echo "<br><h4 style='color:red;'>$err</h4>";
								}
							}
						}
						unset($_SESSION['regsuccess']);
					}elseif(isset($_GET['activatecode'])){
						// show results of email activation
						if($activation_success == true){
							echo "<h3>Account Activated</h3>";
							echo "<br>You may now login!";
						}else{
							echo "<h3 style='color:red;'>Account Activation Failed!</h3>";
						}
					}else{
						// show the new user registration form
				?>
					<div>
						<h3>IMPORTANT</h3>
						<p>
							Please use your <b>REAL email address and cell phone number</b> so we can send you emails and texts with announcements, prizes, coupons, bonuses, winning numbers etc.
						</p>



						<br>

						<div id="location_banner" class="well clearfix" style="padding-right: 15px; padding-left: 15px;">
							<h3 style="margin-top: -5px;">Your Location</h3>
							<span id="geolocation"></span>
							<span id="mapholder" class="pull-right" style="margin-top: -70px;"></span>
						</div>

						<br>
					</div>

					<form id="signup_form" class="form" action="/lib/framework/login.php" method="POST" autocomplete="off">
						<input type="hidden" name="subjoin" value="1">
						<input type="hidden" name="referrer" value="/register.php?<?=$_SERVER['QUERY_STRING']?>">
                                                <input type="hidden" name="csc_accountid" value="<?php echo $_SESSION['csc_accountid']; ?>">
                                                <input type="hidden" name="csc_token" value="<?php echo $_SESSION['csc_token']; ?>">

						<div class="col-sm-6">
							<div class="form-group">
								<label for="firstname">First Name:</label>
								<input type="text" name="firstname" id="firstname" class="form-control" value="<?php echo $_POST['firstname']; ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="lastname">Last Name:</label>
								<input type="text" name="lastname" id="lastname" class="form-control" value="<?php echo $_POST['lastname']; ?>">
							</div>
						</div>

						<div class="col-sm-6">
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="gender">Gender:</label>
								<select id="gender" name="gender" class="form-control selectpicker">
									<option value=""></option>
									<option <?php if($_POST['gender']=='female'){echo("selected");}?> value="female">Female</option>
									<option <?php if($_POST['gender']=='male'){echo("selected");}?> value="male">Male</option>
								</select>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label for="dob">Date of Birth:</label>
								<div class='input-group date dp' id='dob'>
									<input data-fv-date="true" type='text' name='dob2' class="form-control" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="username">Username:</label>
								<input type="text" name="username" id="username" class="form-control" value="<?php echo $_POST['username']; ?>">
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label for="pass">Password:</label>
								<input type="password" name="pass" id="pass" class="form-control" value="<?php echo $_POST['pass']; ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="conf_pass">Confirm Password:</label>
								<input type="password" name="conf_pass" id="conf_pass" class="form-control" value="<?php echo $_POST['conf_pass']; ?>">
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label for="email">Email:</label>
								<input type="email" name="email" id="email" class="form-control" value="<?php echo $_POST['email']; ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="conf_email">Confirm Email:</label>
								<input type="email" name="conf_email" id="conf_email" class="form-control" value="<?php echo $_POST['conf_email']; ?>">
							</div>
						</div>

						<!--div class="col-sm-6">
							<div class="form-group">
								<label for="telephone">Home Phone:</label>
								<input type="text" name="telephone" id="telephone" class="form-control">
							</div>
						</div-->
						<div class="col-sm-6">
							<div class="form-group">
								<label for="cellphone">Cell Phone:</label>
								<input type="text" name="cellphone" id="cellphone" class="form-control" value="<?php echo $_POST['cellphone']; ?>">
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label for="address">Address 1:</label>
								<input type="text" name="address" id="address" class="form-control" value="<?php echo $_POST['address']; ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="address2">Address 2:</label>
								<input type="text" name="address2" id="address2" class="form-control" value="<?php echo $_POST['address2']; ?>">
							</div>
						</div>


						<div class="col-sm-6">
							<div class="form-group">
								<label for="country">Country:</label>
								<input type="text" name="country" id="country" class="form-control" value="<?php echo isset($_POST['country']) ? $_POST['country'] : ''; ?>">
							</div>
						</div>

						<!--<div class="col-sm-6">
							<div class="form-group">
								<label for="pin">CSCLotto ATM PIN:</label>
								<input type="password" name="pin" id="pin" class="form-control" value="<?php echo $_POST['pin']; ?>">
							</div>
						</div>-->

						<div class="col-sm-12" style="height: 200px; overflow-y: scroll; margin-bottom: 10px;">
							<?php include("policies/privacy.php"); ?>
						</div>
						<div class="col-sm-12" style="margin-bottom: 20px;">
							<div class="checkbox">
								<label>
									<input id="privacy_policy" type="checkbox"> I agree to the Privacy Policy
								</label>
							</div>
						</div>

						<div class="col-sm-12" style="height: 200px; overflow-y: scroll; margin-bottom: 10px;">
							<?php include("policies/terms.php"); ?>
						</div>
						<div class="col-sm-12" style="margin-bottom: 20px;">
							<div class="checkbox">
								<label>
									<input id="terms" type="checkbox"> I agree to the Terms & Conditions
								</label>
							</div>
						</div>

						<div class="col-sm-12">
							<button disabled type="submit" id="register_btn" name="register_btn" class="btn btn-primary">Sign Up!</button>
						</div>
					</form>
				<?php
					}
				?>
				</div>
			</div>
		</div>
	</div>
<?php

	include("footer.php");
?>
