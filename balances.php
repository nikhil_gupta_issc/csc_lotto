<div class="panel panel-default balance_div">
	<div class="panel-body balances_panel_body">
		<div class="col-sm-12 balances">
			<ul>
				<li>
					<p class="header">Account Balances</p>
				</li>
				<!-- <li>
					<strong>Available Money:</strong>
					<p><span class="money avaiable_money"> ...</span></p>
				</li> !-->
				<li>
					<strong>Available Balance:</strong>
					<p><span class="money current_balance"> ...</span></p>
				</li>
				<li>
					<strong>Bonus Balance:</strong>
					<p><span class="money bonus_balance"> ...</span></p>
				</li>
				<li>
					<strong>Bonus Hold:</strong>
					<p><span class="money bonus_hold"> ...</span>
				</li>
				<li>
					<strong>Total Balance:</strong>
					<p><span class="money total_balance"> ...</span></p>
				</li>
			</ul>
		</div>
	</div>
</div>
