<?php
	include("header.php");
	include('page_top.php');

	// force password change
	if($session->userinfo['force_password_change'] != 0){
		//Force password change script here.
		include("footer.php");
		die();
	}
	
	if(!$session->logged_in){
		// show warning
		include('must_login.php');
		die;
	}
?>
	<style>
		.loyalty_type{
			background:#595959;
			background:-moz-linear-gradient(center top,#595959 0%,#1a1a1a 100%);
			-pie-background:linear-gradient(center top,#595959 0%,#1a1a1a 100%);
			background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#595959),color-stop(1,#1a1a1a));
			background-image:-o-linear-gradient(top,#595959 0%,#1a1a1a 100%);
			background:-ms-linear-gradient(center top,#595959 0%,#1a1a1a 100%);
			background:linear-gradient(center top,#595959 0%,#1a1a1a 100%);
			position:relative;
			color:#fff;
			margin:0px;
			overflow:hidden;
			border-bottom: 1px solid #8c8c8c;
		}
		
		.loyalty_inner:hover{
			outline: none;
			box-shadow: 0 0 10px #FBB4F0;
		}

		.loyalty_type li{
			display:inline-block;
			position:relative;
			line-height: 30px;
			padding:5px 20px 4px 20px;
			border-right:1px #8c8c8c solid;
			border-bottom:2px transparent solid;
			text-decoration:none;
			text-align:center;
			cursor:pointer;
			-webkit-transform:skew(0deg);
			-moz-transform:skew(0deg);
			-moz-transform:skewX(0deg);
			-ms-transform:skew(0deg);
			-o-transform:skew(0deg);
			transform:skew(0deg);
			outline:none!important;
		}

		.loyalty_type li.active{
			background:-moz-linear-gradient(center top,#212121 50%,#383e28 100%);
			background:-webkit-gradient(linear,left top,left bottom,color-stop(0.5,#212121),color-stop(1,#383e28));
			background:-o-linear-gradient(top,#212121 50%,#383e28 100%);
			background:-ms-linear-gradient(top,#212121 50%,#383e28 100%);
			background:linear-gradient(top,#212121 50%,#383e28 100%);
			border-right: #8c8c8c 1px solid;
			border-bottom: #FBB4F0 2px solid;
			margin-left: -5px;
			padding-right: 25px;
		}

		.loyalty_type li span{
			font-size:12px;
			font-weight:normal;
			color: white;
			display:block;
			text-decoration:none;
			-webkit-transform:skew(0deg);
			-moz-transform:skew(0deg);
			-moz-transform:skewX(0deg);
			-ms-transform:skew(0deg);
			-o-transform:skew(0deg);
			transform:skew(0deg);
			outline:none!important;
			-moz-user-select: none;
			-khtml-user-select: none;
			-webkit-user-select: none;
			-o-user-select: none;
			user-select: none;
		}

		.loyalty_type li.active span, .loyalty_type li span:hover{
			color: #FBB4F0;
			-moz-user-select: none;
			-khtml-user-select: none;
			-webkit-user-select: none;
			-o-user-select: none;
			user-select: none;
		}

		.loyalty_type li.active span{
			font-weight:bold;
			-moz-user-select: none;
			-khtml-user-select: none;
			-webkit-user-select: none;
			-o-user-select: none;
			user-select: none;
		}
	</style>
	
	<script>
		$(document).ready(function() {
			$('.loyalty_type_tab').click(function(e){
				e.preventDefault();
				
				$('#search').val("");
				
				if($(this).hasClass('active')){
					$(this).removeClass('active');
				}else{
					$(this).addClass('active');
				}
				
				// hide all items 
				$(".loyalty").hide();
				
				var sel_count = 0;
				var loyalty_types = $(".loyalty_type_tab");
				$.each(loyalty_types, function(index, g_type){
					if($(g_type).hasClass("active")){
						var selectors = $(g_type).data('loyalty_type').split(' ');
						$.each(selectors, function(index, sel){
							$("."+sel).show();
						});
						sel_count++;
					}
				});
				
				// if nothing selected, default to featured
				if(sel_count == 0){
					$("#loyalty_type_featured").trigger("click");
				}
			});
			
			$('#loyalty_search').keyup(function(){
				var search_term = escape($(this).val().toLowerCase());
				
				console.log(search_term);
				
				$('.loyalty_type_tab').removeClass('active');
				
				if(search_term != ""){
					$('.loyalty').hide();
					$("#loyalty_contain [id*='"+search_term+"']").show();
				}else{
					$('.loyalty').show();
				}
			});
			
			$(document).on('click', '.loyalty_redeem_btn', function(){
				// get item id
				var id = $(this).data("lotalty_id");
				
				$.ajax({
					url: "/ajax/loyalty.php",
					type: "POST",
					dataType: "json",
					data: {
						action: 'redeem',
						id: id
					},
					success: function(response){
						if(response.success == "true"){
							bootbox.alert("Redeemed!  Your voucher code is: "+response.voucher);
						}else{
							bootbox.alert(response.error);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Unexpected Error!");
					}
				});
			});
		});
	</script>

	<div class="clearfix" style="background-image: url('/images/purple.jpg');">
		<div class="header_image">
			<img src="/images/loyalty.jpg" class="img_grow_wide">
		</div>
		<ul class="loyalty_type">
			<div class="col-sm-9">
				<li class="loyalty_type_tab" data-loyalty_type="featured"><span>FEATURED</span></li>
				<li class="loyalty_type_tab" data-loyalty_type="popular"><span>MOST POPULAR</span></li>
				<li class="loyalty_type_tab" data-loyalty_type="vouchers"><span>VOUCHERS</span></li>
				<li class="loyalty_type_tab" data-loyalty_type="clothing"><span>CLOTHING</span></li>
				<li class="loyalty_type_tab" data-loyalty_type="technical"><span>TECHNICAL</span></li>
				<li class="loyalty_type_tab" data-loyalty_type="holidays"><span>HOLIDAYS</span></li>
			</div>
			<div class='col-sm-3' style='padding-top: 2.5px;'>
				<div class="input-group">
					<input id='loyalty_search' class='form-control' type='text' placeholder='Search'>
					<div class="input-group-btn">
						<button class="btn btn-primary" type="button"><i class="glyphicon glyphicon-search"></i></button>
					</div>
				</div>
			</div>
		</ul>
		<div class="col-md-9" style='padding-top: 20px; padding-bottom: 20px;'>
			<div class="clearfix" id="loyalty_contain">
				<table border="0" width="100%" >
					<tr class="text-left  win-list" style="height: 46px;color:#fff">
						<td class="col-md-2">Image</td>
						<td class="col-md-2">Reward Title</td>
						<td class="col-md-5">Description</td>
						<td class="col-md-1">Points</td>
						<td class="col-md-2">QTY</td>
					</tr>
				<?php
					$q = "SELECT * FROM `loyalty_items` WHERE `is_disabled` = 0 ORDER BY `value` ASC";
					$loyalty_items = $db->query($q);
					foreach($loyalty_items as $item){
						echo '
					<tr class="text-left win-list" style="height: 28px; color: #fff;">
						<td class="col-md-2"><img src="/images/loyalty/'.$item['image_path'].'" width="150"></td>
						<td class="col-md-2">'.$item['name'].'</td>
						<td class="col-md-5">'.$item['description'].'</td>
						<td class="col-md-1">'.$item['point_cost'].'</td>
						<td class="col-md-2">
							<button class="btn btn-sm btn-primary loyalty_redeem_btn" type="button" style="width: 100%" data-lotalty_id="'.$item['id'].'">Redeem</button> 
						</td>						
					</tr>
						';
					}
				?>
				</table>
			</div>
		</div>
		<?php
			// get loyalty point settings from db
			$settings = $db->getSettings();
		?>
		<div class='col-md-3' style='padding-top: 20px;'>
			<div class='col-md-12' style='border: 1px solid purple; background: #000; margin-bottom: 15px;'>
				<H4 style="color:#FFF;">Loyalty Scheme</H4>
				<p style="color:#FFF;">Earn Loyalty Points every time you play with CSCLotto.</p>
				
				<p style="color:#FFF;">
					Casino Play<br>
					<small style="color:#CCC;">Earn <?php echo $settings['loyalty_points_casino']; ?> points per $1 spent.</small>
				</p>
				<p style="color:#FFF;">
					Lottery Play<br>
					<small style="color:#CCC;">Earn <?php echo $settings['loyalty_points_lottery']; ?> points per $1 spent.</small>
				</p>
				<p style="color:#FFF;">
					Sportsbook Play<br>
					<small style="color:#CCC;">Earn <?php echo $settings['loyalty_points_sports']; ?> points per $1 spent.</small>
				</p>
				<p style="color:#FFF;">
					FastBallz Play<br>
					<small style="color:#CCC;">Earn <?php echo $settings['loyalty_points_rapidballs']; ?> points per $1 spent.</small>
				</p>
			</div>
		
			<div class='col-md-12' style='border: 1px solid purple; background:#000; margin-bottom: 15px;'>
				<H4 style="color:#FFF;">VIP Tiers</H4>
				<p style="color:#FFF;">Progress up the VIP Ladder by earning more points.</p>
				
				<p style="color:#FFF;">
					PLATINUM (0 - <?php echo intval($settings['platinum_point_amount'])-1; ?>)<br>
					<small style="color:#CCC;">
					<?php
						$loyalty_level = $core->get_loyalty_level();
						$loyalty_points = $core->get_user_loyalty_points(true);
						
						$points_needed = $settings['gold_point_amount'] - $loyalty_points;
						if($points_needed <= 0){
							if($loyalty_level == "Gold"){
								echo "Your Current Level.";
							}else{
								echo "Level Surpassed.";
							}
						}else{
							echo $points_needed." Points Needed.";
						}
					?>
					</small>
				</p>
				<p style="color:#FFF;">
					ELITE (<?php echo ($settings['platinum_point_amount'])." - 
".($settings['diamond_point_amount']-1); ?>)<br>
					<small style="color:#CCC;">
					<?php
						$loyalty_level = $core->get_loyalty_level();
						$loyalty_points = $core->get_user_loyalty_points(true);
						
						$points_needed = $settings['platinum_point_amount'] - $loyalty_points;
						if($points_needed <= 0){
							if($loyalty_level == "Platinum"){
								echo "Your Current Level.";
							}else{
								echo "Level Surpassed.";
							}
						}else{
							echo $points_needed." Points Needed.";
						}
					?>
					</small>
				</p>
				<p style="color:#FFF;">
					ACARD (<?php echo $settings['diamond_point_amount']."+"; ?>)<br>
					<small style="color:#CCC;">
					<?php
						$loyalty_level = $core->get_loyalty_level();
						$loyalty_points = $core->get_user_loyalty_points(true);
						
						$points_needed = $settings['diamond_point_amount'] - $loyalty_points;
						if($points_needed <= 0){
							echo "Your Current Level.";
						}else{
							echo $points_needed." Points Needed.";
						}
					?>
					</small>
				</p>
			</div>
		</div>
	</div>
	
<?php	
	include("footer.php");
?>
