	<?php if($session->logged_in){ ?>	
               <script type="text/javascript" src="/js/balances.js?v=<?=JS_VERSION?>"></script>
       <?php } ?>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$('#loginForm').attr('autocomplete', 'off');
				$(document).on('click', '.balance-show', function(e){
					e.preventDefault();
					
					$.ajax({
						url: "/ajax/hide_balances.php",
						type: "POST",
						success: function(data){

						}
					});
					
					$('.balance-show').addClass('balance-hide');
					$('.balance-show').removeClass('balance-show');
					$('.balance-hide').html('<span class="money total_balance">Refreshing...</span>');
				});
				
				$(document).on('click', '.balance-hide', function(e){
					e.preventDefault();
					
					$.ajax({
						url: "/ajax/hide_balances.php",
						type: "POST",
						success: function(data){

						}
					});
					
					$('.balance-hide').addClass('balance-show');
					$('.balance-hide').removeClass('balance-hide');
					$('.balance-show').html('Hidden');
				});
			});
		</script>
	<style type="text/css">
	.col-md-1.active > a
	{
			width:96px !important;
	}
	
	</style>		
		<div class="container" style="background: #fff; margin-top: 20px; margin-bottom: 20px;">
		<?php
			// show welcome message and last login time
			if($_SESSION['show_welcome'] == true){
				$last_dt = new DateTime($_SESSION['last_login']);
				$last_login = $last_dt->format('m/d/Y')." at ".$last_dt->format('h:i A');
				echo '
			
						<div class="modal fade" id="welcome_modal">
							<div class="modal-dialog">
								<div class="modal-content">
									
									<div class="modal-body" style="text-align:center; background-color:#DDD;color:#000;">
										<p>Welcome back '.$session->userinfo['firstname'].' '.$session->userinfo['lastname'].', Good Luck!!!</p>
										<p>Last Login: '.$last_login.'</p>
									</div>
									<div class="modal-footer" style="text-align:center; background-color:#DDD;color:#000;">
										<button type="button" class="btn btn-primary background-color:#000;color:#000" data-dismiss="modal">Close</button>
									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div><!-- /.modal -->
						<script>
							$(\'#welcome_modal\').modal("show");
						</script>
				';
                        $q="SELECT value FROM `settings`  WHERE setting = 'promo_url'" ; 
			$returned = $db->queryOneRow($q);
			
			
			$q1="SELECT value FROM `settings`  WHERE setting = 'has_video'" ; 
			$returned1 = $db->queryOneRow($q1);

	
			$str = $returned['value'];
			$temp= explode('=',$str);
			$url = $temp[1];
 			$datetime = new DateTime('now');	
			$last_dt = new DateTime($_SESSION['last_login']);			
			$last_login = $last_dt->format('m/d/Y');
			$today = $datetime->format('m/d/Y');
			$date1=date_create($last_login);
			
			$date2=date_create($today);
			$diff=date_diff($date1,$date2);
			$diffr = $diff->format("%a");
			
                        if($diffr > 0 && $returned1['value']=='1')  {
				
					echo '	<script>
						$( document ).ready(function(){	
						$("#overlay").modal("show");
						 });
				
						</script>
						<div class=" modal fade" id="overlay">
		 				 <div class="modal-dialog promptt">
		    					<div class="modal-content" id="modalthick">
								 
		     						 <div class="modal-body">
		      						 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>		     						
		       							 	<iframe width="577" height="345" id="cartoonVideo" src="http://www.youtube.com/embed/'.$url.'?autoplay=1" frameborder="0" allowfullscreen>
										</iframe>   
							
		     						 </div>
															
		   					 </div>
		  				</div>
					</div>';
				}
                                $_SESSION['show_welcome'] = false;
			}
			
			// show warning if password is too old
			if($session->logged_in){
				if($session->userinfo['password_changed_on']){
					$pass_changed_dt = new DateTime($session->userinfo['password_changed_on']);
					$now_dt = new DateTime();
					$pass_age = $now_dt->diff($pass_changed_dt);
					if($pass_age->days >= 90){
						echo '
					<div class="row">
						<div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 10px;">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<b>SECURITY WARNING!</b> <span class="pull-right">Your password is <b>'.$pass_age->days.' days old</b> and should be changed. <a href="my_account.php?tab=ops"><b>Click Here</b></a> to change it.</span>
						</div>
					</div>
						';
					}
				}else{
					// if never changed, see how long since account creation
					$reg_date = new DateTime();
					$reg_date->setTimestamp($session->userinfo['regdate']);
					$now_dt = new DateTime();
					$pass_age = $now_dt->diff($reg_date);
					if($pass_age->days >= 90){
						echo '
					<div class="row">
						<div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 10px;">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<b>SECURITY WARNING!</b> <span class="pull-right">Your password is <b>'.$pass_age->days.' days old</b> and should be changed. <a href="my_account.php?tab=ops"><b>Click Here</b></a> to change it.</span>
						</div>
					</div>
						';
					}
				}
			}
		?>
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-12 clearfix" style="display: inline-block; height: 110px; margin-top: 25px;">
					<a href="/index.php">
						<img src="images/casino/asw_logo.png" alt="#" />
					</a>
				</div>
				
				<?php
					if($session->logged_in){
						// get nickname
						$nickname = $session->userinfo['user_info']['nickname'] ? $session->userinfo['user_info']['nickname'] : $session->userinfo['firstname']." ".$session->userinfo['lastname'];
						
						$balance = $session->userinfo['user_info']['hide_balance'] == 1 ? 'Hidden' : '<span class="money total_balance"> ...</span>';
						$balance_class = $session->userinfo['user_info']['hide_balance'] == 1 ? 'balance-show' : 'balance-hide';
				?>	
				<div class="col-lg-9 col-md-8 col-sm-12 clearfix" style="display: inline-block; height: 70px;">
					<div class="row hidden-sm hidden-xs">
						<nav class="pull-right">
							<ul class="nav navbar-nav nav-small top-nav" style="color: #000;">
								<li><?php include($_SERVER['DOCUMENT_ROOT'].'/lib/assets/clock/dc_server_clock.php'); ?></li>
								<li>|</li>
								<!--<li><a href="/index.php">Home</a></li>
								li>|</li>
								<li ><a href="/locations.php">Locations</a></li>
								<li>|</li>-->
								<li><a href="/about_us.php">Need help?</a></li>
							</ul>
						</nav>
					</div>
					
					<div class="col-sm-12" style="margin-top: -15px; text-align: right; padding-left: 0px; padding-right: 0px;">
						<div class="top-form">
							<form action='<?php echo ROOTPATH; ?>/lib/framework/login.php' method='post'>
								<span id="lblSignedIn">
								
									<span style="text-align: right;">
										Total Balance: <a id="show_hide_balance" class="<?php echo $balance_class; ?>" title="Click to show/hide..." href='#'><?php echo $balance; ?></a> | Loyalty Points: <a href='/loyalty.php'><?php echo number_format($core->get_user_loyalty_points()); ?></a> | Tier Level: <a href='/loyalty.php'><?php echo $core->get_loyalty_level(); ?></a> | Promo Balance: <a  href='#'><?php echo round($core->get_promo_points(),2);?></a> 
									</span>
									<br>
									<span style="padding-top: 5px; display: inline-block; text-align: left;">
								<!--	<a id="page_top_nickname" href="/my_account.php#vouchers" >Redeem Voucher</a>&nbsp;|&nbsp;-->Nickname: <a id="page_top_nickname" href="/my_account.php" ><?php echo $nickname; ?></a>&nbsp;|&nbsp;Account # <?php echo $session->userinfo['customers']['customer_number']; ?>
									</span>
									<input type="hidden" id="referrer" value="/index.php"></input>
									<button name='logout' class='btn btn-primary btn-sm pull-right' type='submit' action='logout' value='Logout' style="margin-left: 10px; padding-right: 10px; padding-left: 10px;">LOG OUT</button>
								</span>
							</form>
						</div>
					</div>
				</div>					
				<?php
					}else{
				?>
				<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 clearfix" style="display: inline-block; height: 70px;">
					<div class="row hidden-sm hidden-xs">
						<nav class="pull-right">
							<ul class="nav navbar-nav nav-small top-nav" style="color: #000;">
									<li><?php include($_SERVER['DOCUMENT_ROOT'].'/lib/assets/clock/dc_server_clock.php'); ?></li>
								<!--<li>|</li>
							<li><a href="/index.php">Home</a></li>
								li>|</li>
								<li ><a href="/locations.php">Locations</a></li
								<li>|</li>
								<li><a href="/about_us.php">Support</a></li>-->
							</ul>
						</nav>
					</div>
					
					<div id="login_content" class="row">
						<form id="loginForm" action="/lib/framework/login.php" method="POST" class="clearfix form form-inline" role="form" autocomplete="off">
							<div id="spacer" class="hidden-xs hidden-sm hidden-md col-lg-1"></div>
						
							<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
								<div style="padding-top: 1px;">
									<label class="sr-only">Username</label>
									
									<input type="text" name="user" style="width: 100%;" id="username" class="form-control" placeholder="Account # or Username" >
								</div>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
								<div style="padding-top: 1px;">
									<label class="sr-only">Password</label>
									<input type="password" name="pass" style="width: 100%;" id="password" class="form-control" placeholder="PIN or Password" autocomplete="off">
								</div>
							</div>
							
							<div class="col-xs-7 col-sm-4 col-md-4 col-lg-3" style="padding-top: 4px;">
								<button type="submit" class="form-group btn btn-primary btn-sm">
									<span class="glyphicon glyphicon-user"> </span>
									<span class="lead">LOGIN</span>
								</button>
					
								<button type="button" class="form-group btn btn-primary btn-sm" onclick="location.href='/register.php';">
									<span class="glyphicon glyphicon-log-in"> </span>
									<span class="lead">SIGNUP</span>
								</button>
							</div>
							
							<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2" style="text-align: right; padding:0; padding-top: 8px; margin-left: -15px;">
								<a style="margin-left: -15px; margin-bottom: 15px;" href="/index.php?forgotpass" class="text-link">forgot password?</a>
							</div>
							
							<input type='hidden' name='sublogin' value='1'>
							<input type='hidden' name='referrer' value='<?php echo str_replace('action=logout', '', $_SERVER['REQUEST_URI']); ?>'>
						</form>	
						
						<div class="modal fade" id="error_modal">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title">Login Error</h4>
			
												
                                                                                              	</div>
									<div class="modal-body" style='text-align:center;'>
										<p>
											<?php
												
											        // print_r($form->errors["firstname"]);
											            // echo $form->errors["firstname"];
														
												if($form->num_errors >= 1){ 
											        //echo "iam here";		
												$st = $form->error("account");
															
												if(!empty($st)){
												
												echo $form->error("account")."<br>";
												}else{
													//echo"iamherein else";
												echo $form->error("user")."<br>".$form->error("pass");
													 echo $form->errors["firstname"]."<br>".$form->errors["lastname"]."<br>".$form->errors["email"];
								
													}
													
									if(!empty($st)){?>
					
									<form id="sessionch" action="/lib/framework/login.php" method="POST" class="clearfix form form-inline" role="form" autocomplete="off">	
		
								
									<label class="sr-only">Password</label>
									<input type="password" name="pass" style="width: 100%;" id="password" class="form-control" placeholder="PIN or Password" autocomplete="off"><br>
						<div style="padding-top: 4px;">			<button type="submit" class="form-group btn btn-primary btn-sm">
									<span class="glyphicon glyphicon-log-in"> </span>
									<span class="lead">Logout Other Session</span>
								</button></div>
							<input type='hidden' name='sessionlog' value='1'>
							<input type='hidden' name='user_id' value="<?php echo $form->getUserID(); ?>">	
							<input type='hidden' name='reason' value="<?php echo 'forced logout'; ?>">	
							<input type='hidden' name='user' value="<?php echo $form->getUserName(); ?>">	
							<input type='hidden' name='referrer' value='<?php echo str_replace('action=logout', '', $_SERVER['REQUEST_URI']); ?>'>			
		
		</form>	
									
									
										
													
												<?php }}
											?>
										</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						
						<?php
						
							if($form->num_errors >= 1){
						?>
						<script>
					$(document).ready(function(){
							$('#error_modal').modal("show");
						           //alert("i am here");
							});
						</script>
						<?php
							}
						?>
					</div>
					
					<div id="block_msg" class="col-md-offset-2" style="color: #fff; text-align: center;  padding: 4px 0px 0px 0px;  background-color: hsl(288, 100%, 18%) !important;display:none"><span>Real money gambling is not permitted outside of The Bahamas. If you are located in The Bahamas and viewing this message please <a href="http://help.asurewin.com" target="_blank" style="  color: palevioletred;">click here.</a></span></div>
					
				</div>
				<?php
					}
				?>
			</div>

			<div class="clear"></div>
			
			<!--navbar-->
			<nav class="navbar" role="navigation" style="margin-bottom: 0px;">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-ex1-collapse"  style="background:#111111;">
					<div class="nav-table">
					<ul id="horizontal-list" class="nav navbar-nav nav-main" style="background-color: #bf0a0a;">
					<?php
					if(!$session->logged_in){
					?>
					<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/index.php" ? "active" : "");?>"><a href="/index.php">Home</a></li>
					<?php
					}else{
					?>
					<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/my_account.php" ? "active" : "");?>"><a href="/my_account.php">My Account</a></li>
					<?php
					}
                            $site_settings = "SELECT value FROM `settings`  WHERE setting = 'rapidballs_enabled' ";
                            $t_site_settings = $db->query($site_settings);
				
						 	$site_settings1= "SELECT value FROM `settings`  WHERE setting = 'casino_enabled'" ; 	
							$t_site_settings1 = $db->query($site_settings1);

                                         $b_rapidball_settings = 0;
                                         $b_casino_settings = 0;
                                         $s_nav_class = 'col-sm-1';
                                     
                                     
                             $select_ip = "select count(id) as num from ipaddress where status=1 AND ipaddress='".$_SERVER["REMOTE_ADDR"]."'";
                             $ip = $db->query($select_ip);
                            
                                     
                                     
                                        
                                        if(!empty($t_site_settings)){
	                                          $b_rapidball_settings = $t_site_settings[0]['value'];
                                              $b_casino_settings = $t_site_settings1[0]['value']; 
                                              $s_nav_class = 'col-sm-1';
                                        }                                 

					?>
						<?php if($b_rapidball_settings!=1 || $b_casino_settings!=1){?>
					<!--<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/lottery.php" ? "active" : "");?>"><a href="/lottery.php">Lotteryy</a></li>-->
					<?php } 
					else { ?>

						<!--<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/lottery.php" ? "active" : "");?>"><a href="/lottery.php">Lotteryy</a></li>-->
						
						<?php 
					}?>
						
                                       <?php if($b_rapidball_settings ==1){ 
                                      
                                       ?>
						<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/fastballz.php" ? "active" : "");?>"><a href="/fastballz.php">FastBallz</a></li>
                            <?php } 
                            else {
                           
                             	if($ip[0]['num'] >0){ ?>
                          	
                             	<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/fastballz.php" ? "active" : "");?>"><a href="/fastballz.php">FastBallz</a></li>
                             	
                        <?php     	}
                            
                            
                            }
                            
                            
                        if(!empty($b_casino_settings) && $b_casino_settings==1 )
                        { ?>
						<li class="" <?php echo $s_nav_class.' '.($_SERVER['PHP_SELF'] == "/casinos.php" ? "active" : "");?>"><a href="/casinos.php"><img src="/images/icon/2_icon_16.png" alt="Casino">Casino</a></li>
                        <?php } ?>
						 <!--<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/sports.php" ? "active" : "");?>"><a href="/sports.php"><img src="/images/icon/18_icon_16.png" alt="Sportsbook">Sportsbook</a></li-->
						<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/results.php" ? "active" : "");?>"><a href="/results.php">Winning Numbers</a></li>
					
						<?php if($b_rapidball_settings!=1 && $ip[0]['num'] <= 0){?>
						<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/news.php" ? "active" : "");?>"><a href="/news.php">News</a></li>
						<?php }else
						{ ?>
						
						<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/news.php" ? "active" : "");?>"><a href="/news.php">News</a></li>
						
						<?php } ?>
						
						<?php
					if(!$session->logged_in){
					?>
				<!--	<li class="" <?php echo ($_SERVER['PHP_SELF'] == "/responsible_gaming.php" ? "active" : "");?>"><a href="/responsible_gaming.php">Responsible Gaming</a></li>
					<?php
					}else{
					?>
	                                      				

					<?php
					}
					?>
					
					<?php
					if(!$session->logged_in && $b_rapidball_settings!=1 || $b_casino_settings!=1){
						
					?>	
					 <li class="" <?php echo ($_SERVER['PHP_SELF'] == "/howto.php" ? "active" : "");?>"><a href="/howto.php" title="How To">How to</a></li>
					 <?php
					} else 
					 { ?>
						 <li class="" <?php echo ($_SERVER['PHP_SELF'] == "/howto.php" ? "active" : "");?>"><a href="/howto.php" title="How To">How to</a></li>
					 <?php
					 }
					 ?>
					 
					</ul>
					</div>
				</div><!-- /.navbar-collapse -->
			</nav>

			<div id="winnings_pullout" class="hidden-xs">
				<div class="row" style="padding-left: 20px; padding-right: 7px;">
					<button class="btn btn-primary btn-sm pull-left" style="margin-top: -35px; margin-left: 15px;" id="winning_pullout_today">Today</button>
					<button class="btn btn-primary btn-sm pull-right" style="margin-top: -35px; margin-right: 75px;" id="winning_pullout_yesterday">Yesterday</button>
				</div>
				<div class="well col-sm-10" id="winnings_pullout_list_container">

				</div>
				<div class="col-sm-2">
					<!--<img id="winning_pullout_button" src="/images/win_handle.png?v=<?=IMAGE_VERSION?>" class="cursor-pointer">-->
				</div>
			</div>

<script>	
$(".nav-main a").each(function() {
    //console.log($(this).attr('href'));
    if ((window.location.pathname.indexOf($(this).attr('href'))) > -1) {
        $(this).parent().addClass('current');
    }
});
</script>
