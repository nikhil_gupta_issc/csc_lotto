<?php 
	date_default_timezone_set('Etc/UTC');

	//=========================
	// Global Constants
	//=========================

	define('JS_VERSION', '1.0.0');
	define('CSS_VERSION', '1.0.0');
	define('IMAGE_VERSION', '1.0.0');
	define('CURRENCY_FORMAT', 'CSC');
	define('DB_TYPE', 'mysql');
	define('DB_HOST', 'localhost');
	define('DB_SERVER', 'localhost');
	define('DB_PORT', '3306');
	define('DB_USER', 'daniel');
	define('DB_PASSWORD', '7dwjI2McKrZUxQJ8Olgx');
	define('DB_NAME', 'casino');
	define('DB_PCONNECT', false);
	define('DB_INNODB', true);

	define('ROOTPATH', 'https://csclotto.com');
	if(!defined('DOMAIN_ROOT')){
		define('DOMAIN_ROOT', '/var/www/html');
	}

	define('CACHEOPT_METHOD', 'file');
	define('CACHEOPT_TTLFAST', '120');
	define('CACHEOPT_TTLMEDIUM', '600');
	define('CACHEOPT_TTLSLOW', '1800');

	define('RECEIPT_AUTO_PRINT', true);
	// must go to about:flags in Chrome and set print preview to enable
	// also add --kiosk-print to chrome link

	$USER_INFO_TABLES = array(
		"panel_user",
		"customers",
		"back_office_users",
		"user_info",
	);

	/**
	 * Database Table Constants - these constants
	 * hold the names of all the database tables used
	 * in the script.
	 */
	define("TBL_USERS", "users");
	define("TBL_ACTIVE_USERS",  "active_users");
	define("TBL_ACTIVE_GUESTS", "active_guests");
	define("TBL_BANNED_USERS",  "banned_users");
	define("TBL_CONFIGURATION", "configuration");

	/**
	 * Special Names and Level Constants - the admin
	 * page will only be accessible to the user with
	 * the admin name and also to those users at the
	 * admin user level. Feel free to change the names
	 * and level constants as you see fit, you may
	 * also add additional level specifications.
	 * Levels must be digits between 0-9.
	 */
	define("SITE_NAME", "CSCLotto");
	define("ADMIN_NAME", "admin");
	define("GUEST_NAME", "Guest");
	define("ADMIN_LEVEL", 9);
	define("PRIVUSER_LEVEL", 8);
	define("REGUSER_LEVEL", 3);
	define("ADMIN_ACT", 2);
	define("ACT_EMAIL", 1);
	define("GUEST_LEVEL", 0);
       define('PROTOCOL', 'https://');

	define("FORCE_2FA", 1);
	define("FORCE_2FA_USER_LEVEL", 8);
	//define("MAX_LOGIN_ATTEMPTS", 3);
	//define("LOCKED_MINUTES", 15);

	define("DENY_HTML_PAGE_URL", "/admin/access_denied.php");
	define("DENY_HTML_PAGE_TITLE", "Page Access Denied");
	define("DENY_AJAX_RETURN_TYPE", "json"); // or text
	define("DENY_AJAX_JSON_INDEX", "success");
	define("DENY_AJAX_JSON_VALUE", false);
	define("DENY_AJAX_JSON_MESSAGE_INDEX", "errors");
	define("DENY_DEFAULT_MESSAGE", "Your security settings do not permit access to this page!");

	define("EMAIL_WELCOME", true);
	define("ACCOUNT_ACTIVATION", 2);  // email activation required

	$url_info = array();
	if(isset($_SERVER['REQUEST_URI'])){
	      	$t_path = parse_url($_SERVER['REQUEST_URI']);
        	$url_info = explode('/',$t_path['path']);
	}
         
	if(!empty($url_info[1]) && $url_info[1] == 'admin'){
               define("LOCKED_MINUTES", 15);
               define("MAX_LOGIN_ATTEMPTS", 3);
        } else if(!empty($url_info[1]) && $url_info[1] == 'panel'){
               define("LOCKED_MINUTES", 1);
               define("MAX_LOGIN_ATTEMPTS", 15);
        } else {
               define("LOCKED_MINUTES", 2);
               define("MAX_LOGIN_ATTEMPTS", 3);
        }

	define("USER_TIMEOUT", 30);
        define("CASHIER_TIMEOUT", 600);
        define("ADMIN_TIMEOUT", 600);
 
	/**
	 * Timeout Constants - these constants refer to
	 * the maximum amount of time (in minutes) after
	 * their last page refresh that a user and guest
	 * are still considered active visitors.
	 */
	//define("USER_TIMEOUT", 60);
	define("GUEST_TIMEOUT", 60);
	define("TIMEOUT_MESSAGE", "For security purposes, you have been logged out due to inactivity.");

	define('APP_NAME', 'CSCLotto');
	define('COMPANY_NAME', 'CSCLotto');
	
	define('LARGE_WIN_THRESHOLD', 500);
	define('LARGE_TRANSFER_THRESHOLD', 4999);

	$www_top = str_replace("\\","/",dirname( $_SERVER['PHP_SELF'] ));
	if(strlen($www_top) == 1){
		$www_top = "";
	}

	// used everywhere an href is output, includes the full path to the install
	define('WWW_TOP', $www_top);

	// used to refer to the /www/lib class files
	define('WWW_DIR', realpath(dirname(__FILE__)).'/');

	// set DOCUMENT ROOT for crons to work
	if(PHP_SAPI == 'cli'){
		$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
	}
	
	// Add support for random_bytes() and random_int() PHP7 functions (Cryptographically Secure)
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/lib/assets/random_compat/lib/random.php");
	}else{
		require_once(DOMAIN_ROOT.'/lib/assets/random_compat/lib/random.php');
	}

	// Include database handler
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/lib/framework/db.php");
	}else{
		require_once(DOMAIN_ROOT.'/lib/framework/db.php');
	}
	$db = new DB();

	// Include session handler
	if(PHP_SAPI != 'cli'){
		require_once($_SERVER['DOCUMENT_ROOT']."/lib/framework/session.php");
	}else{
		require_once(DOMAIN_ROOT.'/lib/framework/session.php');
	}
