<?php
	class casino {
		private $api_key;
	
		public function __construct($ip = "", $api_key = ""){
			// check API key
			if($api_key != "" && $ip != ""){
				$result = $this->authenticate($ip, $api_key);
				if($result['success'] == false){
					// show error response
					echo json_encode($result);
					die();
				}else{
					// on success, set api_key
					$this->api_key = $api_key;
				}
			}
		}
		
		protected function retrieveJSON($URL){
			$result = array();
		
			$opts = array('http' =>
				array(
					'method'  => 'GET',
					'timeout' => 10 
				)
			);
			$context = stream_context_create($opts);
			$feed = @file_get_contents($URL, false, $context);
			if($feed){
				$result = json_decode($feed, true);
				$result['success'] = 'true';
			}else{
				$result['success'] = 'false';
				$result['message'] = "Error Receiving Data From $URL";
			}
			return $result;
		}
		
		private function array_msort($array, $cols){
			$colarr = array();
			foreach ($cols as $col => $order) {
				$colarr[$col] = array();
				foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
			}
			$eval = 'array_multisort(';
			foreach ($cols as $col => $order) {
				$eval .= '$colarr[\''.$col.'\'],'.$order.',';
			}
			$eval = substr($eval,0,-1).');';
			eval($eval);
			$ret = array();
			foreach ($colarr as $col => $arr) {
				foreach ($arr as $k => $v) {
					$k = substr($k,1);
					if (!isset($ret[$k])) $ret[$k] = $array[$k];
					$ret[$k][$col] = $array[$k][$col];
				}
			}
			return $ret;
		}
		
		private function get_vendor_id($ip_address = ''){
			if($ip_address == ''){
				$ip_address = $_SERVER['REMOTE_ADDR'];
			}
			switch ($ip_address){
				case '192.168.168.39':
					return 1;
					break;
				case '192.168.168.47':
					return 2;
					break;
				default:
					return -1;
					break;
			}
		}
		
		public function create_api_key($ip = ""){
			global $db;
			
			$result = array();
		
			// create hash using username and salt with timestamp
			$salt = md5(microtime().rand());
			$secret_key = $ip;
			
			// Compute the signature by hashing the salt with the secret key
			$api_key = hash_hmac('ripemd160', $salt, $secret_key);		

			// save to database
			$q = "REPLACE INTO ".CASINO_TABLE_PREFIX."api_keys "
					."(api_key, salt, ip_address, user_id) "
				."VALUES ('$api_key', '$salt', '$ip', 1)";
			$db_result = $db->queryDirect($q);
			
			if($db_result){
				$result['success'] = true;
				$result['api_key'] = $api_key;
			}else{
				$result['success'] = false;
				$result['message'] = "Database error executing query: $q";
			}
			
			echo json_encode($result);
			die();
		}
		
		private function authenticate($ip, $api_key){
			global $db;
			
			$result = array();
			
			// see if this api_key is in the database
			$q = "SELECT * FROM ".CASINO_TABLE_PREFIX."api_keys WHERE api_key='$api_key' LIMIT 1";
			$db_result = $db->queryOneRow($q);
			
			// if we found a match, verify hash match 
			if(!empty($db_result)){
				$secret_key = $ip;
				$salt = $db_result['salt'];
				
				// Compute the signature by hashing the salt with the secret key as the key
				$expected_hash = hash_hmac('ripemd160', $salt, $secret_key);		
			
				if($expected_hash == $api_key){
					$result['success'] = true;
				}else{
					$result['success'] = false;
					$result['message'] = "API key not valid";
				}
			}else{
				$result['success'] = false;
				$result['message'] = "API key not found";
			}
			
			return $result;
		}
		
		public function player_authenticate($username='', $password='', $token=''){
			global $db;
			
			if($token == ''){
				// get salt and user info
				$q = "SELECT `usersalt` FROM `users` WHERE `username`='".$username."' LIMIT 1";
				$salt_info = $db->queryOneRow($q);
				
				// create password hash
				$challenge = sha1($salt_info['usersalt'].$password);
				
				// see if hash matches
				$q = "SELECT * FROM `users` AS `u` LEFT JOIN `user_info` as `ui` ON `ui`.`user_id` = `u`.`id` WHERE `u`.`username` = '".$username."' AND `u`.`password` = '".$challenge."' LIMIT 1";
				$user_info = $db->queryOneRow($q);
			}else{
				$q = "SELECT * FROM `users` AS `u` LEFT JOIN `user_info` as `ui` ON `ui`.`user_id` = `u`.`id` WHERE `u`.`userid` = '".$token."' LIMIT 1";
				$user_info = $db->queryOneRow($q);
			}
			
			$response = array();
			if(!empty($user_info)){
				$time = time();
				$now_dt = new DateTime();
				$now = $now_dt->format('Y-m-d H:i:s');
				
				// update last login and timestamp
				$db->query("UPDATE ".TBL_USERS." SET last_login = '$now' WHERE id = '".$user_info['id']."'");
				$db->query("UPDATE ".TBL_USERS." SET timestamp = '$time' WHERE id = '".$user_info['id']."'");
				$db->query("REPLACE INTO ".TBL_ACTIVE_USERS." VALUES ('".$user_info['username']."', '$time')");
				
				// if so, create response
				$response['Login'] = $user_info['username'];
				$response['ScreenName'] = $user_info['nickname'] != "" ? $user_info['nickname'] : $user_info['username'];
				$response['FirstName'] = $user_info['firstname'];
				$response['LastName'] = $user_info['lastname'];
				$response['UserID'] = $user_info['id'];
				$response['Affiliate'] = 'CSCLotto';
				//$response['Partner'] = '';
				$response['Currency'] = 'USD';
				//$response['ExchangeRate'] = '';
				$response['Country'] = 'BHS';
				$response['BlockStatus'] = null;
				$response['ErrorCode'] = 0;
				$response['AccessLevel'] = 0;
				$response['Token'] = $user_info['userid'];
			}else{
				$response['ErrorCode'] = "Player authentication failed";
				$response['AccessLevel'] = -1;
			}
			
			return $response;
		}
		
		public function player_balance($username){
			global $db;
			global $core;
			
			$q = "SELECT `c`.`user_id`,`c`.`is_disabled`, `c`.`available_balance`, `c`.`daily_loss_limit`, `c`.`daily_wager_limit` FROM `customers` AS `c` JOIN `users` AS `u` ON `u`.`id` = `c`.`user_id` WHERE `u`.`username` = '".$username."'";
			$bal = $db->queryOneRow($q);
			
			$response = array();
			if($bal){
				// if limits are in place, send the following...
				/* 
				$response['AvailableBalance'] = 0;
				
				If the player has a loss limit, send the amount they have
				remaining to lose.
				 
				If the player multiple loss limits, send the lowest amount
				they have remaining to lose before triggering a limit block.
				 
				If the player has a cumulative wager limit, send the amount
				they have remaining to wager.
				 
				If the player has multiple cumulative wager limits, send the
				lowest amount the player may wager before triggering a limit
				block.
				 
				If the player has both loss and cumulative wager limits, send
				the lowest amount the player may lose or wager before triggering
				a limit block.
				 
				If multiple limits result in the same AvailableLimitBalance figure,
				send the one associated with the longest time period, and then
				wager limits ahead of loss limits.
				 
				$response['BlockNote'] = "";
				example:
				Of your daily loss limit of $100, you have only $10 remaining. This
				limit will reset in 6 hours and 10 minutes.
				
				Of your weekly wager limit of $10,000, you have only $100 left to
				wager. This limit will reset in 2 days.
				
				*/
				$remaining_wager_limit = $core->remaining_wager_limit($bal['user_id']);
				$loss_limit_remain = $core->remaining_loss_limit($bal['user_id']);
                                if($bal['is_disabled'] != 1){
				if($remaining_wager_limit > 0 && $loss_limit_remain > 0){
					
					if($remaining_wager_limit>$loss_limit_remain){
                                                // return remaining loss limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have only $".$loss_limit_remain." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $loss_limit_remain;
                                   
					 } else {
						// return remaining wager limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $remaining_wager_limit;
					}				
				} else if($remaining_wager_limit > 0){
				
						// return remaining wager limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $remaining_wager_limit;
				
				} else if($remaining_wager_limit == 0){
						// return remaining wager limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $remaining_wager_limit;
                                   
				} else if($loss_limit_remain > 0){
						// return remaining loss limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have only $".$loss_limit_remain." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $loss_limit_remain;
                                   
				} else if($loss_limit_remain == 0){
						// return remaining loss limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have $".$loss_limit_remain." remaining.";
					        $response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $loss_limit_remain;
				} else {
						// no self limit set
						$response['StatusId'] = 1;
						$response['ErrorCode'] = 0;
						$response['AvailableBalance'] = $bal['available_balance'];
				}
                              } else {
                                        // error code 41 if user is disabled
					$response['StatusId'] = 41;
					$response['ErrorCode'] = 0;
					$response['AvailableBalance'] = $bal['available_balance'];
                             }
			}else{
				$response['ErrorCode'] = 'Player balance not found';
			}
			
			return $response;			
		}
		
		public function player_balance_by_id($user_id){
			global $db;
			global $core;
			
			$q = "SELECT `c`.`available_balance`,`c`.`is_disabled`, `c`.`daily_loss_limit`, `c`.`daily_wager_limit` FROM `customers` AS `c` JOIN `users` AS `u` ON `u`.`id` = `c`.`user_id` WHERE `u`.`id` = '".$user_id."'";
			$bal = $db->queryOneRow($q);
			
			$response = array();
			if($bal){
                               if($bal['is_disabled'] != 1){
				$remaining_wager_limit = $core->remaining_wager_limit($user_id);
				$loss_limit_remain = $core->remaining_loss_limit($user_id);
				if($remaining_wager_limit > 0 && $loss_limit_remain > 0){
					
					if($remaining_wager_limit>$loss_limit_remain){
                                                // return remaining loss limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have only $".$loss_limit_remain." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $loss_limit_remain;
                                   
					} else {
						// return remaining wager limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $remaining_wager_limit;
					}					
				
				 } else if($remaining_wager_limit > 0){
				
						// return remaining wager limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $remaining_wager_limit;
				
				 } else if($remaining_wager_limit == 0){
						// return remaining wager limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $remaining_wager_limit;
                                   
				} else if($loss_limit_remain > 0){
						// return remaining loss limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have only $".$loss_limit_remain." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $loss_limit_remain;
				 } else if($loss_limit_remain == 0){
						// return remaining loss limit
						$response['StatusId'] = 40;
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have $".$loss_limit_remain." remaining.";
						$response['AvailableBalance'] = $bal['available_balance'];
		                                $response['AvailableLimitBalance'] = $loss_limit_remain;
				  }else{
						// no self limit set
						$response['StatusId'] = 1;
						$response['ErrorCode'] = 0;
						$response['AvailableBalance'] = $bal['available_balance'];
				 }
                              } else {

                                        $response['StatusId'] = 41;
					$response['ErrorCode'] = 0;
					$response['AvailableBalance'] = $bal['available_balance'];
                             }
			}else{
				$response['ErrorCode'] = 'Player balance not found';
			}
			
			return $response;			
		}
		
              public function casino_transaction_by_id($user_id,$game_id=0, $transaction_date, $total_wagered=0,$bal_adj, $session_id, $game_name,$jackpot_contribution=0,  $bonus_play=0,  $casino_game_type="",$casino_game_id,$description="", $wager=0){
			global $db;
			global $core;
			
			$response = array();
			
			// get username from user id
			$q = "SELECT id, username FROM `users` WHERE `id` = %s";
			$user_info = $db->queryOneRow($q, array($user_id));
			
			// see if this user is active.  if not, kill transaction
			$q = "SELECT username FROM `active_users` WHERE `username` = %s";
			$active_user_info = $db->queryOneRow($q, array($user_info['username']));
			if(empty($active_user_info )){
				$response['ErrorCode'] = "User not logged in";
				return $response;
			}
			
			// make our own description instead of using thiers
			if($bal_adj >= 0){
				$description = "Casino Win - ".$game_name;
			}else{
				$description = "Casino Bet - ".$game_name;
			}
			
			/*
			AvailableLimitBalance
			BlockNote
			------------------------------------------
			
			These reflect the status of the player *after* the transaction. They
			are optional. If AvailableLimitBalance was sent on GetBalance
			and was not refreshed on Transaction, we will simply call GetBalance
			again after the transaction to get the new value.
			*/
			
			if(empty($user_info )){
				$response['ErrorCode'] = "Invalid User ID";
			}else{
				// create transaction in db
				$trans_id = $core->make_casino_transaction($user_info['id'], $game_id, $transaction_date, $total_wagered, $bal_adj, $session_id, $game_name, $jackpot_contribution, $bonus_play, $casino_game_type, $casino_game_id, $description, $this->get_vendor_id());

				if($trans_id > 0){
					// get customers updated available balance to pass back
					$q = "SELECT * FROM `customer_transaction` WHERE `transaction_id` = %i";
					$trans_info = $db->queryOneRow($q, array($trans_id));
					
					$response['info'] = $trans_info;

                                         $q = "SELECT `c`.`daily_loss_limit`, `c`.`daily_wager_limit` FROM `customers` AS `c` JOIN `users` AS `u` ON `u`.`id` = `c`.`user_id` WHERE `u`.`id` = '".$user_info['id']."'";
			                 $bal = $db->queryOneRow($q);

					$remaining_wager_limit = $core->remaining_wager_limit($user_id);
					$loss_limit_remain = $core->remaining_loss_limit($user_info['id']);
					if($remaining_wager_limit > 0 && $loss_limit_remain > 0){
					
						if($remaining_wager_limit>$loss_limit_remain){
						        // return remaining loss limit
							$response['ErrorCode'] = 0;
							$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have only $".$loss_limit_remain." remaining.";
							$response['AvailableBalance'] = $trans_info['balance'];
				                        $response['AvailableLimitBalance'] = $loss_limit_remain;
		                           
						}else{
							// return remaining wager limit
					
							$response['ErrorCode'] = 0;
							$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
							$response['AvailableBalance'] = $trans_info['balance'];
				                        $response['AvailableLimitBalance'] = $remaining_wager_limit;
						}					
				
				       } elseif($remaining_wager_limit > 0){
				
						// return remaining wager limit					
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
						$response['AvailableBalance'] = $trans_info['balance'];
		                                $response['AvailableLimitBalance'] = $remaining_wager_limit;
				
				       } else if($remaining_wager_limit == 0){
				
						// return remaining wager limit					
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
						$response['AvailableBalance'] = $trans_info['balance'];
		                                $response['AvailableLimitBalance'] = $remaining_wager_limit;
				
				       } elseif($loss_limit_remain > 0){
						// return remaining loss limit
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily loss limit of $".$$bal['daily_loss_limit'].", you have only $".$loss_limit_remain." remaining.";
					        $response['AvailableBalance'] = $trans_info['balance'];
                                                $response['AvailableLimitBalance'] = $loss_limit_remain;
				       } elseif($loss_limit_remain == 0){
						// return remaining loss limit
						$response['ErrorCode'] = 0;
						$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have $".$loss_limit_remain." remaining.";
						$response['AvailableBalance'] = $trans_info['balance'];
                                                $response['AvailableLimitBalance'] = $loss_limit_remain;
				       } else {
						// no self limit set
						$response['AvailableBalance'] = $trans_info['balance'];
						$response['ErrorCode'] = 0;
					}
					
					$response['CMSTransaction'] = $trans_id;
					
					if(empty($trans_info)){
						$response['ErrorCode'] = "Failed to retrieve available balance for transaction ID: $trans_id";
					}
				}else{
					$response['ErrorCode'] = 'Failed to create transaction';
				}
			}
			
			return $response;
		}
		
		public function casino_transaction($username, $transaction_date, $bal_adj, $session_id, $game_name, $casino_game_id, $game_id=0, $total_wagered=0, $jackpot_contribution=0, $bonus_play=0, $casino_game_type="", $description="", $wager=0){
			global $db;
			global $core;
			
			$response = array();
			
			// see if this user is active.  if not, kill transaction
			$q = "SELECT username FROM `active_users` WHERE `username` = %s";
			$active_user_info = $db->queryOneRow($q, array($username));
			if(empty($active_user_info )){
				$response['ErrorCode'] = "User not logged in";
				return $response;
			}
			
			// make our own description instead of using thiers
			if($bal_adj >= 0){
				$description = "Casino Win - ".$game_name;
			}else{
				$description = "Casino Bet - ".$game_name;
			}
			
			/*
			AvailableLimitBalance
			BlockNote
			------------------------------------------
			
			These reflect the status of the player *after* the transaction. They
			are optional. If AvailableLimitBalance was sent on GetBalance
			and was not refreshed on Transaction, we will simply call GetBalance
			again after the transaction to get the new value.
			*/
			
			// get user id from username
			$q = "SELECT id FROM `users` WHERE `username` = %s";
			$user_info = $db->queryOneRow($q, array($username));
			
			if(empty($user_info )){
				$response['ErrorCode'] = "Invalid Username";
			}else{
				// create transaction in db
				$trans_id = $core->make_casino_transaction($user_info['id'], $game_id, $transaction_date, $total_wagered, $bal_adj, $session_id, $game_name, $jackpot_contribution, $bonus_play, $casino_game_type, $casino_game_id, $description, $this->get_vendor_id());

				if($trans_id > 0){
					// get customers updated available balance to pass back
					$q = "SELECT * FROM `customer_transaction` WHERE `transaction_id` = %i";
					$trans_info = $db->queryOneRow($q, array($trans_id));
					
					$response['info'] = $trans_info;

                                        $q = "SELECT `c`.`daily_loss_limit`, `c`.`daily_wager_limit` FROM `customers` AS `c` JOIN `users` AS `u` ON `u`.`id` = `c`.`user_id` WHERE `u`.`id` = '".$user_info['id']."'";
			                $bal = $db->queryOneRow($q);

					$remaining_wager_limit = $core->remaining_wager_limit($user_info['id']);
					$loss_limit_remain = $core->remaining_loss_limit($user_info['id']);
					if($remaining_wager_limit > 0 && $loss_limit_remain > 0){
					
						if($remaining_wager_limit>$loss_limit_remain){
						        // return remaining loss limit
							$response['ErrorCode'] = 0;
							$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have only $".$loss_limit_remain." remaining.";
							$response['AvailableBalance'] = $trans_info['balance'];
				                        $response['AvailableLimitBalance'] = $loss_limit_remain;
		                           
						} else {
							// return remaining wager limit					
							$response['ErrorCode'] = 0;
							$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
							$response['AvailableBalance'] = $trans_info['balance'];
				                        $response['AvailableLimitBalance'] = $remaining_wager_limit;
						}					
				
				        } elseif($remaining_wager_limit > 0){
				
							// return remaining wager limit
					
							$response['ErrorCode'] = 0;
							$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
							$response['AvailableBalance'] = $trans_info['balance'];
				                        $response['AvailableLimitBalance'] = $remaining_wager_limit;
				
				       } elseif($remaining_wager_limit == 0){
				
							// return remaining wager limit
					
							$response['ErrorCode'] = 0;
							$response['BlockNote'] = "Of your daily wager limit of $".$bal['daily_wager_limit'].", you have only $".$remaining_wager_limit." remaining.";
							$response['AvailableBalance'] = $trans_info['balance'];
				                        $response['AvailableLimitBalance'] = $remaining_wager_limit;
				
				       } else if($loss_limit_remain > 0){
							// return remaining wager limit
							$response['ErrorCode'] = 0;
							$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have only $".$loss_limit_remain." remaining.";
							$response['AvailableBalance'] =$trans_info['balance'];
		                                        $response['AvailableLimitBalance'] = $loss_limit_remain;
					} else if($loss_limit_remain == 0){
							// return remaining wager limit
							$response['ErrorCode'] = 40;
							$response['BlockNote'] = "Of your daily loss limit of $".$bal['daily_loss_limit'].", you have $".$loss_limit_remain." remaining.";
							$response['AvailableBalance'] = $trans_info['balance'];
		                                        $response['AvailableLimitBalance'] = $loss_limit_remain;
					} else {
							// no self limit set
							$response['AvailableBalance'] = $trans_info['balance'];
							$response['ErrorCode'] = 0;
					}
					
					$response['CMSTransaction'] = $trans_id;
					
					if(empty($trans_info)){
						$response['ErrorCode'] = "Failed to retrieve available balance for transaction ID: $trans_id";
					}
				}else{
					$response['ErrorCode'] = 'Failed to create transaction';
				}
			}
			
			return $response;
		}
	}
?>
