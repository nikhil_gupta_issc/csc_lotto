<?php
require_once("cache.php");

define('DEBUG_OUTPUT_OFF', 0);
define('DEBUG_OUTPUT_CONSOLE', 1);
define('DEBUG_OUTPUT_HTML', 2);
define('DEBUG_OUTPUT_FILE', 4);

define('DEBUG_OPT_NONE', 0);
define('DEBUG_OPT_MYSQL_ERRORS', 1);
define('DEBUG_OPT_MYSQL_QUERIES', 2);
define('DEBUG_OPT_MYSQL_RESULTS', 4);
define('DEBUG_OPT_POST', 8);
define('DEBUG_OPT_GET', 16);
define('DEBUG_OPT_SESSION', 32);
define('DEBUG_OPT_ADMIN_ONLY', 64);
define('DEBUG_OPT_ALWAYS_SHOW_ERRORS', 128);

// Note: Combine options using | (OR)
//		 Compare options using  & (AND)

define('LOG_CHANGES', true);
define('LOG_PREVIOUS_VALUES', true);
define('LOG_AFTER_VALUES', true);
define('LOG_CHANGES_TABLE', 'log_database_changes');
$LOG_CHANGE_EXCLUSIONS = array(
	'log_page_loads',
	'user_devices',
	'log_login_attempts',
	'panel_user_shift',
	'select ',
	'active_users',
	'active_guests',
	'update users set timestamp',
	'update users set last_login'
);

class DB
{
    private static $initialized = false;
    private static $mysqli = null;
    private static $usingInnoDB = null;
    private static $batchSize = 1000;
	private static $debugOptions = 0;
	private static $debugOutput = 0;
	
    function DB()
    {
        if (DB::$initialized === false) {
			if(defined('DB_PORT')){
				DB::$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT);
			}else{
				DB::$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
			}

            if (mysqli_connect_errno()) {
                printf("Fatal error: %s", mysqli_connect_error());
                exit();
            }

            DB::$mysqli->select_db(DB_NAME)
                or die("Fatal error: could not select database! Check your config.");

            DB::$mysqli->set_charset("utf8");

            DB::$usingInnoDB = defined('DB_INNODB') ? DB_INNODB : false;

            DB::$initialized = true;
			
			//$config = $this->getConfigs();
			
			//DB::$debugOptions = intval($config['debug_options']);
			//DB::$debugOutput = intval($config['debug_output']);
        }
    }
	
	private function refValues($arr){
        if(strnatcmp(phpversion(), '5.3') >= 0){ //Reference is required for PHP 5.3+
            $refs = array();
            foreach($arr as $key => $value)
                $refs[$key] = &$arr[$key];
            return $refs;
        }
        return $arr;
    }
	
	public function sanitizeInput($query){
		// attempt to prevent SQL injection attempts for older queries not using prepared statements
		
		// never allow UNION SELECT
		
		// prevent common sql injection strings
		
	}

    public function getBatchSize()
    {
        return DB::$batchSize;
    }

    public function escapeString($str)
    {
        return "'" . DB::$mysqli->real_escape_string($str) . "'";
    }

    public function makeLookupTable($rows, $keycol)
    {
        $arr = array();
        foreach ($rows as $row)
            $arr[$row[$keycol]] = $row;
        return $arr;
    }
	
	public function getLastError(){
		return DB::$mysqli->error;
	}
	
	public function logQuery($query, $parameters, $success, $log_id = -1)
    {
		global $LOG_CHANGE_EXCLUSIONS;		
		
		// get current date/time
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		
		// ignore if no one is logged in
        if(strtolower($_SESSION['username']) != 'guest'){
			if(LOG_CHANGES == true){
				// make sure query does not include any excluded words
				$exclude = false;
				foreach($LOG_CHANGE_EXCLUSIONS as $excluded_term){
					if(stripos(" ".$query, $excluded_term) !== false){
						$exclude = true;
						break;
					}
				}
				
				if($exclude === false){
					$referrer = str_ireplace($_SERVER['DOCUMENT_ROOT'], "", $_SERVER['SCRIPT_FILENAME']);
					if($log_id == -1){
						if($success == 1){
							$q = "INSERT INTO `".LOG_CHANGES_TABLE."` (`id`, `query`, `parameters`, `executed_by`, `executed_on`, `user_session_id`, `is_success`, `error_text`, `referrer`) VALUES (NULL, '".addslashes($query)."', '".json_encode($parameters)."', '".$_SESSION['username']."', '".$now."', '".$_SESSION['user_session_id']."', '".$success."', NULL, '".$referrer."');";
							$log_id = $this->queryInsert($q);
						}else{
							$q = "INSERT INTO `".LOG_CHANGES_TABLE."` (`id`, `query`, `parameters`, `executed_by`, `executed_on`, `user_session_id`, `is_success`, `error_text`) VALUES (NULL, '".addslashes($query)."', NULL, '".$_SESSION['username']."', '".$now."', '".$_SESSION['user_session_id']."', '".$success."', '".addslashes(json_encode(DB::$mysqli->error))."', '".$referrer."');";
							$log_id = $this->queryInsert($q);
						}
					}else{
						if($success == 1){
							$q = "UPDATE `".LOG_CHANGES_TABLE."` SET `is_success`='".$success."' WHERE `id` = '".$log_id."'";
							DB::$mysqli->query($q);
						}else{
							$q = "UPDATE `".LOG_CHANGES_TABLE."` SET `is_success`='".$success."', `error_text`='".addslashes(json_encode(DB::$mysqli->error))."' WHERE `id` = '".$log_id."'";
							DB::$mysqli->query($q);
						}
					}
					
					$this->logAfterValues($query, $log_id);
				}
			}
		} 
    }
	
	public function logPreviousValues($query, $parameters)
    {
		global $LOG_CHANGE_EXCLUSIONS;
		
		// get current date/time
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		
		// ignore if no one is logged in
        if(strtolower($_SESSION['username']) != 'guest'){
			if(LOG_PREVIOUS_VALUES == true){
				// make sure query does not include any excluded words
				$exclude = false;
				foreach($LOG_CHANGE_EXCLUSIONS as $excluded_term){
					if(stripos(" ".$query, $excluded_term) !== false){
						$exclude = true;
						break;
					}
				}
				
				if($exclude === false){
					// parse out table name and query type
					$words = explode(" ", strtolower($query));
					$q_arr = explode(" where ", strtolower($query));
					if($words[0] == 'update'){
						$table_name = $words[1];
						// get previous values
						$q = "SELECT * FROM ".$table_name." WHERE ".$q_arr[1];
						$values_before = json_encode($this->queryOneRow($q));
						
						// store this and grab log id
						$referrer = str_ireplace($_SERVER['DOCUMENT_ROOT'], "", $_SERVER['SCRIPT_FILENAME']);
						$q = "INSERT INTO `".LOG_CHANGES_TABLE."` (`id`, `query`, `parameters`, `executed_by`, `executed_on`, `user_session_id`, `is_success`, `error_text`, `referrer`, `values_before`, `values_after`) VALUES (NULL, '".addslashes($query)."', '".json_encode($parameters)."', '".$_SESSION['username']."', '".$now."', '".$_SESSION['user_session_id']."', '0', NULL, '".$referrer."', '".$values_before."', '');";
						$log_id = $this->queryInsert($q);
						return $log_id;
					}
				}
			}
		} 
    }
	
	public function logAfterValues($query, $log_id)
    {
		global $LOG_CHANGE_EXCLUSIONS;
		// ignore if no one is logged in
        if(strtolower($_SESSION['username']) != 'guest'){
			if(LOG_AFTER_VALUES == true){
				// make sure query does not include any excluded words
				$exclude = false;
				foreach($LOG_CHANGE_EXCLUSIONS as $excluded_term){
					if(stripos(" ".$query, $excluded_term) !== false){
						$exclude = true;
						break;
					}
				}
				
				if($exclude === false){
					// parse out table name and query type
					$words = explode(" ", strtolower($query));
					$q_arr = explode(" where ", strtolower($query));
					if($words[0] == 'update'){
						$table_name = $words[1];
						// get previous values
						$q = "SELECT * FROM ".$table_name." WHERE ".$q_arr[1];
						$values_after = json_encode($this->queryOneRow($q));
						
						// store this and grab log id
						$q = "UPDATE `".LOG_CHANGES_TABLE."` SET `values_after`='".$values_after."' WHERE `id` = '".$log_id."'";
						DB::$mysqli->query($q);
					}
				}
			}
		} 
    }

    public function queryInsert($query, $parameters = array(), $returnlastid = true)
    {
        if($query=="")
            return false;
		
		// get current date/time
		$now_dt = new DateTime();
		$now = $now_dt->format("Y-m-d H:i:s");
		
		if(LOG_PREVIOUS_VALUES == true){
			$log_id = $this->logPreviousValues($query, $parameters);
		}else{
			$log_id = -1;
		}

        // if parameters sent, use prepared statements
		if($parameters){
			$stmt = new MySqliPrep(DB::$mysqli);
			if($stmt = $stmt->prepare($query, $parameters)){
				$result = $stmt->execute();
				$ret = ($returnlastid) ? $stmt->insert_id : $result;
				$stmt->close();
				// log success
				$this->logQuery($query, $parameters, 1, $log_id);
			}else{
				// log failure
				$this->logQuery($query, $parameters, 0, $log_id);
				
				return false;
			}
		}else{
			$result = DB::$mysqli->query($query);
			$ret = ($returnlastid) ? DB::$mysqli->insert_id : $result;

			if($ret > 0){
				// log success
				$this->logQuery($query, $parameters, 1, $log_id);
			}else{
				// log failure
				$this->logQuery($query, $parameters, 0, $log_id);
			}
		}
		// show query if debugging is set
		//if(headers_sent()){
			if(DB::$debugOptions & DEBUG_OPT_MYSQL_QUERIES){
				if(DB::$debugOptions & DEBUG_OPT_ADMIN_ONLY){
					if($_SESSION['user_level'] >= 9){
						if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
							//html output
							echo "<br><b>MySQL Query Insert:</b> $query<br>\n";
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
							//console output
							printf("<script>console.log('<<DEBUG INFO>> MySQL Query Insert: %s');</script>\n", addslashes($query));
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
							//file output
							$datetime = date("m/d/Y h:i:s A");
							$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
							file_put_contents($log_file, $datetime.' >> MySQL Query Insert: '.var_export($query, true).PHP_EOL, FILE_APPEND);
						}						
					}
				}else{
					if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
						//html output
						echo "<br><b>MySQL Query Insert:</b> $query<br>\n";
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
						//console output
						printf("<script>console.log('<<DEBUG INFO>> MySQL Query Insert: %s');</script>\n", addslashes($query));
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
						//file output
						$datetime = date("m/d/Y h:i:s A");
						$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
						file_put_contents($log_file, $datetime.' >> MySQL Query Insert: '.var_export($query, true).PHP_EOL, FILE_APPEND);
					}
				}
			}
		//}
		
		if(!$result){
			// error occurred
			// show if debugging is set
			//if(headers_sent()){				
				if(DB::$debugOptions & DEBUG_OPT_MYSQL_ERRORS){
					if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
						//html output
						echo "<div class='alert alert-warning alert-block'>
								<i class='icon-exclamation-sign'></i>
								<button type='button' class='close' data-dismiss='alert'>&times;</button>";
						printf("<b>MySQL Error:</b> %s\n<br><b>Insert Query:</b> %s\n<br>", DB::$mysqli->error, $query);
						echo "</div>";
					}elseif(DB::$debugOptions & DEBUG_OPT_ALWAYS_SHOW_ERRORS){
						//html output
						echo "<div class='alert alert-warning alert-block'>
								<i class='icon-exclamation-sign'></i>
								<button type='button' class='close' data-dismiss='alert'>&times;</button>";
						printf("<b>MySQL Error:</b> %s\n<br><b>Insert Query:</b> %s\n<br>", DB::$mysqli->error, $query);
						echo "</div>";
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
						//console output
						printf("<script>console.log('<<DEBUG INFO>> MySQL Error: %s');\n console.log('<<DEBUG INFO>> Error Query: %s');</script>\n", addslashes(json_encode(DB::$mysqli->error)), addslashes(json_encode($query)));
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
						//file output
						$datetime = date("m/d/Y h:i:s A");
						$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
						file_put_contents($filename, $datetime.' >> MySQL Error: '.DB::$mysqli->error.PHP_EOL, FILE_APPEND);
						file_put_contents($filename, $datetime.' >> MySQL Error Query: '.$query, FILE_APPEND);
					}
				}
			//}
		}
		
		// show results if debugging is set
		if(headers_sent()){
			if(DB::$debugOptions & DEBUG_OPT_MYSQL_RESULTS){
				if(DB::$debugOptions & DEBUG_OPT_ADMIN_ONLY){
					if($_SESSION['user_level'] >= 9){
						if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
							//html output
							echo "<br><b>MySQL Results:</b><br>\n";
							echo "<pre>"; print_r($ret); echo "</pre>\n";
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
							//console output
							printf("<script>console.log('<<DEBUG INFO>> MySQL Results: %s');</script>\n", addslashes(json_encode($ret)));
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
							//file output
							$datetime = date("m/d/Y h:i:s A");
							$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
							file_put_contents($log_file, $datetime.' >> MySQL Results: '.var_export($ret, true).PHP_EOL, FILE_APPEND);
						}
					}
				}else{
					if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
						//html output
						echo "<br><b>MySQL Results:</b><br>\n";
						echo "<pre>"; print_r($ret); echo "</pre>\n";
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
						//console output
						printf("<script>console.log('<<DEBUG INFO>> MySQL Results: %s');</script>\n", addslashes(json_encode($ret)));
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
						//file output
						$datetime = date("m/d/Y h:i:s A");
						$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
						file_put_contents($log_file, $datetime.' >> MySQL Results: '.var_export($ret, true).PHP_EOL, FILE_APPEND);
					}				
				}
			}
		}
        return $ret;
    }

    public function getNextID($table)
    {
        if($table=="")
            return false;

		$query = "SELECT Auto_increment FROM information_schema.tables WHERE table_name = '".$table."' AND table_schema = DATABASE();";
        $rows = $this->query($query);
        return $rows[0]['Auto_increment'];
    }

    public function queryOneRow($query, $parameters = array(), $useCache = false, $cacheTTL = '')
    {
        if($query=="")
            return false;

		$rows = $this->query($query, $parameters, $useCache, $cacheTTL);
		
        return ($rows ? $rows[0] : false);
    }
	
	public function query($query, $parameters = array(), $useCache = false, $cacheTTL = '')
    {
		$rows = array();
		
        if($query=="")
            return false;

		if(LOG_PREVIOUS_VALUES == true){
			$log_id = $this->logPreviousValues($query, $parameters);
		}else{
			$log_id = -1;
		}
		
        if($useCache){
            $cache = new Cache();
            if ($cache->enabled && $cache->exists($query)) {
                $ret = $cache->fetch($query);
                if ($ret !== false)
                    return $ret;
            }
        }
		$date = new DateTime('now');
		$now = $date->format("Y-m-d H:i:s");
		// if parameters sent, use prepared statements
		if($parameters){
			$prep = new MySqliPrep(DB::$mysqli);
			if($stmt = $prep->prepare($query, $parameters)){
				$stmt->execute();
			
				if($meta = $stmt->result_metadata()){	
					while($field = $meta->fetch_field()){
						$field_names[] = &$row[$field->name];
					}  
				
					call_user_func_array(array($stmt, 'bind_result'), $this->refValues($field_names));

					while($stmt->fetch()){  
						$x = array();  
						foreach($row as $key => $val){  
							$x[$key] = $val;  
						}
						$rows[] = $x;  
					}
				
					$stmt->close();
					
					return $rows;
				}else{
				
					return false;
				}
				
				// log success
				$this->logQuery($query, $parameters, 1, $log_id);
			}else{
				// log failure
				$this->logQuery($query, $parameters, 0, $log_id);
				
				return false;
			}
		}else{
			$result = DB::$mysqli->query($query);
			 $ret = !empty($returnlastid) ? DB::$mysqli->insert_id : $result;

                        if(!empty($ret->num_rows) && $ret->num_rows > 0){

				// log success
				$this->logQuery($query, $parameters, 1, $log_id);
			}else{
				// log failure
				$this->logQuery($query, $parameters, 0, $log_id);
				
			}
		}
		
		// show query if debugging is set
		if(headers_sent()){
			if(DB::$debugOptions & DEBUG_OPT_MYSQL_QUERIES){
				if(DB::$debugOptions & DEBUG_OPT_ADMIN_ONLY){
					if($_SESSION['user_level'] >= 9){			
						if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
							//html output
							echo "<br><b>MySQL Query:</b> $query<br>\n";
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
							//console output
							printf("<script>console.log('<<DEBUG INFO>> MySQL Query: %s');</script>\n", addslashes($query));
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
							//file output
							$datetime = date("m/d/Y h:i:s A");
							$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
							file_put_contents($log_file, $datetime.' >> MySQL Query: '.var_export($query, true).PHP_EOL, FILE_APPEND);
						}
					}
				}else{
					if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
						//html output
						echo "<br><b>MySQL Query:</b> $query<br>\n";
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
						//console output
						printf("<script>console.log('<<DEBUG INFO>> MySQL Query: %s');</script>\n", addslashes($query));
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
						//file output
						$datetime = date("m/d/Y h:i:s A");
						$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
						file_put_contents($log_file, $datetime.' >> MySQL Query: '.var_export($query, true).PHP_EOL, FILE_APPEND);
					}				
				}
			}
		}
		
		if(!$result){
			// error occurred
			// show if debugging is set
			if(headers_sent()){
				if(DB::$debugOptions & DEBUG_OPT_MYSQL_ERRORS){
					if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
						//html output
						echo "<div class='alert alert-warning alert-block'>
								<i class='icon-exclamation-sign'></i>
								<button type='button' class='close' data-dismiss='alert'>&times;</button>";
						printf("<b>MySQL Error:</b> %s\n<br><b>Query:</b> %s\n<br>", DB::$mysqli->error, $query);
						echo "</div>";						
					}elseif(DB::$debugOptions & DEBUG_OPT_ALWAYS_SHOW_ERRORS){
						//html output
						echo "<div class='alert alert-warning alert-block'>
								<i class='icon-exclamation-sign'></i>
								<button type='button' class='close' data-dismiss='alert'>&times;</button>";
						printf("<b>MySQL Error:</b> %s\n<br><b>Query:</b> %s\n<br>", DB::$mysqli->error, $query);
						echo "</div>";
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
						//console output
						printf("<script>console.log('<<DEBUG INFO>> MySQL Error: %s);\n console.log('<<DEBUG INFO>> Error Query: %s');</script>\n", DB::$mysqli->error, addslashes($query));
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
						//file output
						$datetime = date("m/d/Y h:i:s A");
						$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
						file_put_contents($filename, $datetime.' >> MySQL Error: '.DB::$mysqli->error.PHP_EOL, FILE_APPEND);
						file_put_contents($filename, $datetime.' >> MySQL Error Query: '.$query, FILE_APPEND);
					}
				}
			}
		}
		
        if($result === false || $result === true){
			return array();
		}

        while ($row = $this->getAssocArray($result))
            $rows[] = $row;

		// show results if debugging is set
		if(headers_sent()){
			if(DB::$debugOptions & DEBUG_OPT_MYSQL_RESULTS){
				if(DB::$debugOptions & DEBUG_OPT_ADMIN_ONLY){
					if($_SESSION['user_level'] >= 9){				
						if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
							//html output
							echo "<br><b>MySQL Results:</b><br>\n";
							echo "<pre>"; print_r($rows); echo "</pre>\n";
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
							//console output
							printf("<script>console.log('<<DEBUG INFO>> MySQL Results: %s');</script>\n", addslashes(json_encode($rows)));
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
							//file output
							$datetime = date("m/d/Y h:i:s A");
							$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
							file_put_contents($log_file, $datetime.' >> MySQL Results: '.var_export($rows, true).PHP_EOL, FILE_APPEND);
						}
					}
				}else{
					if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
						//html output
						echo "<br><b>MySQL Results:</b><br>\n";
						echo "<pre>"; print_r($rows); echo "</pre>\n";
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
						//console output
						printf("<script>console.log('<<DEBUG INFO>> MySQL Results: %s');</script>\n", addslashes(json_encode($rows)));
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
						//file output
						$datetime = date("m/d/Y h:i:s A");
						$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
						file_put_contents($log_file, $datetime.' >> MySQL Results: '.var_export($rows, true).PHP_EOL, FILE_APPEND);
					}				
				}
			}
		}		
		
        $this->freeResult($result);

        if ($useCache)
            if ($cache->enabled)
                $cache->store($query, $rows, $cacheTTL);

        return $rows;
    }

    public function queryDirect($query, $parameters = array(), $unbuffered = false)
    {
        if($query=="")
            return false;
		
		if(LOG_PREVIOUS_VALUES == true){
			$log_id = $this->logPreviousValues($query, $parameters);
		}else{
			$log_id = -1;
		}
		
		if($parameters){
			$stmt = new MySqliPrep(DB::$mysqli);
			if($stmt = $stmt->prepare($query, $parameters)){
				$stmt->execute();
				$ret = $stmt->result_metadata();
				$stmt->close();
				
				// log success
				$this->logQuery($query, $parameters, 1, $log_id);
				
				return $ret;
			}else{
				// log failure
				$this->logQuery($query, $parameters, 0, $log_id);
				
				return false;
			}
		}

        if($unbuffered){
            $ret = DB::$mysqli->query($query, MYSQLI_USE_RESULT);
        }else{
            $ret = DB::$mysqli->query($query);
        }
		
		// show query if debugging is set
		if(headers_sent()){
			if(DB::$debugOptions & DEBUG_OPT_MYSQL_QUERIES){
				if(DB::$debugOptions & DEBUG_OPT_ADMIN_ONLY){
					if($_SESSION['user_level'] >= 9){				
						if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
							//html output
							echo "<br><b>MySQL Query Direct:</b> $query<br>\n";
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
							//console output
							printf("<script>console.log('<<DEBUG INFO>> MySQL Query Direct: %s');</script>\n", addslashes($query));
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
							//file output
							$datetime = date("m/d/Y h:i:s A");
							$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
							file_put_contents($log_file, $datetime.' >> MySQL Query Direct: '.var_export($query, true).PHP_EOL, FILE_APPEND);
						}
					}
				}else{
					if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
						//html output
						echo "<br><b>MySQL Query Direct:</b> $query<br>\n";
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
						//console output
						printf("<script>console.log('<<DEBUG INFO>> MySQL Query Direct: %s');</script>\n", addslashes($query));
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
						//file output
						$datetime = date("m/d/Y h:i:s A");
						$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
						file_put_contents($log_file, $datetime.' >> MySQL Query Direct: '.var_export($query, true).PHP_EOL, FILE_APPEND);
					}				
				}
			}
		}
		
		if(!$ret){
			// log failure
			$this->logQuery($query, $parameters, 0, $log_id);

			// show if debugging is set
			if(headers_sent()){
				if(DB::$debugOptions & DEBUG_OPT_MYSQL_ERRORS){
					if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
						//html output
						echo "<div class='alert alert-warning alert-block'>
								<i class='icon-exclamation-sign'></i>
								<button type='button' class='close' data-dismiss='alert'>&times;</button>";
						printf("<b>MySQL Error:</b> %s\n<br><b>Direct Query:</b> %s\n<br>", DB::$mysqli->error, $query);
						echo "</div>";
					}elseif(DB::$debugOptions & DEBUG_OPT_ALWAYS_SHOW_ERRORS){
						//html output
						echo "<div class='alert alert-warning alert-block'>
								<i class='icon-exclamation-sign'></i>
								<button type='button' class='close' data-dismiss='alert'>&times;</button>";
						printf("<b>MySQL Error:</b> %s\n<br><b>Direct Query:</b> %s\n<br>", DB::$mysqli->error, $query);
						echo "</div>";
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
						//console output
						printf("<script>console.log('<<DEBUG INFO>> MySQL Error: %s);\n console.log('<<DEBUG INFO>> Error Query: %s');</script>\n", DB::$mysqli->error, addslashes($query));
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
						//file output
						$datetime = date("m/d/Y h:i:s A");
						$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
						file_put_contents($filename, $datetime.' >> MySQL Error: '.DB::$mysqli->error.PHP_EOL, FILE_APPEND);
						file_put_contents($filename, $datetime.' >> MySQL Error Query: '.$query, FILE_APPEND);
					}
				}
			}
			//return array('success' => 'false');
		}else{
			// log success
			$this->logQuery($query, $parameters, 1, $log_id);
		}
		
		// show results if debugging is set
		if(headers_sent()){
			if(DB::$debugOptions & DEBUG_OPT_MYSQL_RESULTS){
				if(DB::$debugOptions & DEBUG_OPT_ADMIN_ONLY){
					if($_SESSION['user_level'] >= 9){				
						if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
							//html output
							echo "<br><b>MySQL Results:</b><br>\n";
							echo "<pre>"; print_r($ret); echo "</pre>\n";
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
							//console output
							printf("<script>console.log('<<DEBUG INFO>> MySQL Results: %s');</script>\n", addslashes(json_encode($ret)));
						}
						if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
							//file output
							$datetime = date("m/d/Y h:i:s A");
							$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
							file_put_contents($log_file, $datetime.' >> MySQL Results: '.var_export($ret, true).PHP_EOL, FILE_APPEND);
						}
					}
				}else{
					if(DB::$debugOutput & DEBUG_OUTPUT_HTML){
						//html output
						echo "<br><b>MySQL Results:</b><br>\n";
						echo "<pre>"; print_r($ret); echo "</pre>\n";
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_CONSOLE){
						//console output
						printf("<script>console.log('<<DEBUG INFO>> MySQL Results: %s');</script>\n", addslashes(json_encode($ret)));
					}
					if(DB::$debugOutput & DEBUG_OUTPUT_FILE){
						//file output
						$datetime = date("m/d/Y h:i:s A");
						$log_file = "logs".DIRECTORY_SEPARATOR."mysql_log.txt";
						file_put_contents($log_file, $datetime.' >> MySQL Results: '.var_export($ret, true).PHP_EOL, FILE_APPEND);
					}				
				}
			}
		}

        return $ret;
    }

    public function freeResult($result)
    {
        $result->free_result();
    }

    public function getNumRows($result)
    {
        return $result->num_rows;
    }

    public function disableAutoCommit()
    {
        if (DB::$usingInnoDB == false)
            return;

        DB::$mysqli->autocommit(FALSE);
    }

    public function disableForeignKeyChecks()
    {
        if (DB::$usingInnoDB == false)
            return;

        $this->query("SET foreign_key_checks=0;");
    }

    public function enableForeignKeyChecks()
    {
        if (DB::$usingInnoDB == false)
            return;

        $this->query("SET foreign_key_checks=1;");
    }

    public function commit($enableAutoCommit = true)
    {
        if (DB::$usingInnoDB == false)
            return;

        DB::$mysqli->commit();

        if ($enableAutoCommit == true)
            $this->enableAutoCommit();
    }

    public function rollback($enableAutoCommit = true)
    {
        if (DB::$usingInnoDB == false)
            return;

        DB::$mysqli->rollback();

        if ($enableAutoCommit == true)
            $this->enableAutoCommit();
    }

    public function enableAutoCommit()
    {
        if (DB::$usingInnoDB == false)
            return;

        DB::$mysqli->autocommit(TRUE);
    }

    public function usingInnoDB()
    {
        return DB::$usingInnoDB;
    }

    public function getAssocArray($result)
    {
        ini_set('memory_limit', '-1');
	return $result->fetch_assoc();
    }

    public function getRow($result)
    {
        return $result->fetch_row();
    }

    public function optimise($force = false)
    {
        $ret = array();
        if ($force)
            $alltables = $this->query("show table status");
        else
            $alltables = $this->query("show table status where Data_free != 0");

        foreach ($alltables as $tablename) {
            $ret[] = $tablename['Name'];
            $this->queryDirect("REPAIR TABLE `" . $tablename['Name'] . "`");
            $this->queryDirect("OPTIMIZE TABLE `" . $tablename['Name'] . "`");
            $this->queryDirect("ANALYZE TABLE `" . $tablename['Name'] . "`");
        }

        return $ret;
    }

	public function getSettings($table_name = "settings"){
		$config = array();  
		$query = "SELECT * FROM `".$table_name."`";
		$rows = $this->query($query);
		foreach($rows as $row){
			$config[$row['setting']] = $row['value'];
		}
		return $config;
	}
   
   public function updateSetting($setting, $value, $table_name = "settings"){
		$query = "UPDATE `".$table_name."` SET `value` = '$value' WHERE `setting` = '$setting'";
		return $this->queryDirect($query);
   }
   
	public function setDebugOptions($value){
		DB::$debugOptions = intval($value);
	}   
	
	public function setDebugOutput($value){
		DB::$debugOutput = intval($value);
	}   	
}

class MySqliPrep
{
	private $db;
	private $bind_map = array(
		'%d' => 'i', //integer
		'%i' => 'i', //integer
		'%f' => 'd', //float
		'%s' => 's', //string
	);

	public function __construct(mysqli $db){
		$this->db = $db;
	}

	public function prepare($query, $params = array()){ 
		$regex = '/('.implode('|', array_keys($this->bind_map)).')/';

		if(preg_match_all($regex, $query, $matches)){
			$types = implode('', $matches[0]);        
			$types = strtr($types, $this->bind_map);

			$query = preg_replace($regex, '?', $query);

			if($stmt = $this->db->prepare($query)){        
				array_unshift($params, $types);

				if(call_user_func_array(array($stmt, 'bind_param'), $this->refValues($params))){
					return $stmt;
				}else{
					echo "Error binding parameters for prepared statement; Query: $query";
					return false;
				}
			}else{
				echo "Error creating prepared statement for query: $query";
				print_r($stmt);
				die();
				return false;
			}
		}else{
			return $this->db->prepare($query);
		}
	}
	
	private function refValues($arr){
        if(strnatcmp(phpversion(),'5.3') >= 0){ //Reference is required for PHP 5.3+
            $refs = array();
            foreach($arr as $key => $value)
                $refs[$key] = &$arr[$key];
            return $refs;
        }
        return $arr;
    }
}
