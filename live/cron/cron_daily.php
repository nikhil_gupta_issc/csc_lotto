<?php
	include('/var/www/html/config.php');
	
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$now2 = $now_dt->format('m/d/y h:i:s A');

	// validate current date/time and results of last cron run
	$q = "SELECT * FROM `log_cron` WHERE `filename` = 'cron_daily' AND `function` = 'all' ORDER BY `start` DESC LIMIT 1";
	$last_run = $db->queryOneRow($q);
	
	// log cron start
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_daily', 'all', '0')";
	$cron_daily_log_id = $db->queryInsert($q);
	
	$last_start = new DateTime($last_run['start']);
	$expected_dt = new DateTime($last_run['start']);
	$expected_dt = $expected_dt->modify('+1 day');
	$expected = $expected_dt->format('m/d/y h:i:s A');
	$elapsed_interval = $now_dt->diff($last_start);
	$elapsed_seconds = $now_dt->getTimestamp() - $last_start->getTimestamp();
	$off_by_interval = $now_dt->diff($expected_dt);
	echo "Last Run - ".$last_run['start']."<br>";
	echo "This Run - ".$now."<br>";
	echo "Expected - ".$expected."<br>";
	echo "Time since last run: ".$core->format_interval($elapsed_interval)." (".$elapsed_seconds." seconds)<br>";
	$message = "";
	if($elapsed_seconds > 90000){
		// more than 1 day (86400) + 1 hour (3600) since last run
		$subject = "Server Date/Time Change Detected";
		$message = "Expected server date/time was off by ".$core->format_interval($off_by_interval)."<br><br>Expected: $expected<br>Actual: $now2".".";
		$group_id = 4;
		$core->send_alert_by_group($subject, $message, $group_id);
		$core->log_sigificant_event('Scheduled Tasks / Server Time', 'Daily Cron Did Not Run at Expected Time', $message);
	}elseif($off_by_seconds < 0){
		// date changed to time before last date
		$subject = "Server Date/Time Change Detected";
		$message = "Expected server date/time was off by ".$core->format_interval($off_by_interval)."<br><br>Expected: $expected<br>Actual: $now2".".";
		$group_id = 4;
		$core->send_alert_by_group($subject, $message, $group_id);
		$core->log_sigificant_event('Server Date/Time Change', 'Serve Clock Set Back In Time', $message);
	}
	echo $message;
	
	// see if last run processed completely
	if($last_run['success'] != '1'){
		$core->log_sigificant_event('Scheduled Tasks / Server Time', 'Daily Cron Did Not Complete Successfully', 'One or more scheduled tasks did not complete successfully during the previous Daily cron run.');
	}

	// generate lucky numbers
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_daily', 'generate_lucky_numbers', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->generate_lucky_numbers();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	//Add pending scheduled advance play tickets
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_daily', 'advance_play_cron_handler', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->advance_play_cron_handler();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	//Index all pages in the admin panel
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_daily', 'index_pages', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->index_pages();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	//Check for unprocessed tickets that weren't picked up for whatever reason on the winning draw
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_daily', 'check_unprocessed_tickets', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->check_unprocessed_tickets();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	//Update totals of the day before
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_daily', 'update_daily_totals', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->update_daily_totals(date("Y-m-d",strtotime("-1 day", time(date("Y-m-d", time())))));
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	//Send alerts for unprocessed bets
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_daily', 'alert_unprocessed_bets', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->alert_unprocessed_bets();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	//Disable Dormant Accounts
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "INSERT INTO `log_cron` (`id`, `start`, `finish`, `filename`, `function`, `success`) VALUES (NULL, '".$now."', NULL, 'cron_daily', 'disable_dormant_accounts', '0')";
	$cron_log_id = $db->queryInsert($q);
	$core->disable_dormant_accounts();
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_log_id."'";
	$db->query($q);

	// log cron end
	$now_dt = new DateTime();
	$now = $now_dt->format('Y-m-d H:i:s');
	$q = "UPDATE `log_cron` SET `success` = '1' WHERE `id` = '".$cron_daily_log_id."'";
	$db->query($q);
	$q = "UPDATE `log_cron` SET `finish` = '".$now."' WHERE `id` = '".$cron_daily_log_id."'";
	$db->query($q);