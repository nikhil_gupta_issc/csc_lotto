<?php
	$page_title = "Play Slots, Blackjack & Roulette!";
	
	$top_left_fixed_img = '<img src="/images/cas_hd_img_lft.png">';
	$top_right_fixed_img = '<img style="padding-top: 14px !important;" src="/images/cas_hd_img_rgt.png">';

	if(isset($_GET['exit_game'])){
		include("header.php");
		echo '
			<script>
				window.parent.$("#launch_game_modal").modal("hide");
				window.parent.ContainerCloseGame();
			</script>
		';
		die();
	}
	
	include("header.php");
	include('page_top.php');

	if(!$session->logged_in){
		include('public_casino.php');
		include("footer.php");
		die;
	}else{
?>

	<style>
		.bg{
			background: black url('/images/casino_background.jpg') no-repeat center top;
		}
		
		div:-webkit-full-screen {
			width: 100px;
			height: 100px;
			position: absolute;
			right: 0px;
			top: 0px;
			z-index: 999999;
			background: blue;
		}
		div:-moz-full-screen {
			width: 100px;
			height: 100px;
			position: absolute;
			right: 0px;
			top: 0px;
			z-index: 999999;
			background: blue;
		}
		div:-ms-fullscreen {
			width: 100px;
			height: 100px;
			position: absolute;
			right: 0px;
			top: 0px;
			z-index: 999999;
			background: blue;
		}
		#close_overlay_full:fullscreen {
			width: 100px;
			height: 100px;
			position: absolute;
			right: 0px;
			top: 0px;
			z-index: 999999;
			background: blue;
		}
	</style>
	
	<script type="text/javascript" language="javascript"  src='/lib/assets/bigscreen/bigscreen.min.js'></script>

	<script>
		function hide_game(element){
			$(element).closest('.casino_game').remove();
		}
		
		function ContainerCloseGame(){
			document.webkitExitFullscreen();
			document.mozCancelFullscreen();
			document.msExitFullscreen();
			document.exitFullscreen();
			$('#launch_game_modal').modal('hide');
		}
		
		$(document).ready(function() {
			$(document).on('click', '.casino_game', function(){
				$('#modal_iframe').attr('src', $(this).data('href'));
				$('#launch_game_label').html($(this).find('img').attr('alt'));
				$('#launch_game_modal').modal('show');
			});
			
			$('#launch_game_modal').on('hidden.bs.modal', function (){
				$('#modal_iframe').attr('src', "");
			});
			
			$(document).on('click', '#go_fullscreen', function(){
				var element = document.getElementById('modal_iframe');
				if (BigScreen.enabled) {
					BigScreen.request(element);
					$('body').append('<div class="close" data-dismiss="modal" id="close_overlay_full"></div>');
				}else{
					bootbox.alert("Sorry this feature is not supported by your browser");
				}
			});
			
			$(document).on('click', '#close_overlay_full', function(){
				ContainerCloseGame();
			});
			
			$('.game_type_tab').click(function(e){
				e.preventDefault();
				
				$('#casino_search').val("");

                                $('.active').removeClass('active');
                                $(this).addClass('active');
				
				
				// hide all games 
				$(".casino_game").hide();
				
				var sel_count = 0;
				var game_types = $(".game_type_tab");
				$.each(game_types, function(index, g_type){
					if($(g_type).hasClass("active")){
						var selectors = $(g_type).data('game_type').split(' ');
						$.each(selectors, function(index, sel){
							$("."+sel).show();
						});
						sel_count++;
					}
				});
				
				// if nothing selected, default to featured
				if(sel_count == 0){
					$("#game_type_featured").trigger("click");
				}
			});
			
			$('#casino_search').keyup(function(){
				var search_term = escape($(this).val().toLowerCase());
				
				console.log(search_term);
				
				$('.game_type_tab').removeClass('active');
				
				if(search_term != ""){
					$('.casino_game').hide();
					$("#casino_games_contain [id*='"+search_term+"']").show();
				}else{
					$('.casino_game').show();
				}
			});
			
			// get casino game list
			$.ajax({
				url: "/api/casino/get_casino_games.php",
				type: "POST",
				dataType : "json",
				success: function(response){
				
				if(response.gm == 'disabled'){
					$('#msg').show();
				}
					if(response.token != '' && response.games != null){
						$.each(response.games, function(index, game) {
							var tags = "";
							var new_ribbon = "";
							$.each(game.tags, function(index, tag){
								tags += tag+" ";
								if(tag == 'new'){
									new_ribbon = "	<div class='clearfix new_ribbon'></div>";
								}
								if(tag == 'least_played'){
									new_ribbon += "	<div class='clearfix featured_ribbon'></div>";
								}
							});							
							
							$('#casino_games_contain').append(
								"<div id='"+escape(game.LobbyImageAltText.toLowerCase())+"' class='casino_game "+game.GameType.toLowerCase()+" "+tags+"col-xs-6 col-sm-5 col-md-4 col-lg-3' style='padding: 5px;' data-href='"+game.launch_url+"'>"+
								new_ribbon+
								"	<div class='col-sm-12 panel casino_game_inner' style='border: 1px solid purple; background:#000; padding-top: 15px;'>"+
								"		<img onError='hide_game(this);' style='width: 100%;' alt='"+game.LobbyImageAltText+"' src='"+game.LobbyImage+"'>"+
								"		<p style='font-family: Droid Sans;font-size: 13px;color:white;margin-top: 5px;'><b>"+game.LobbyImageAltText+"</b><br><small>Min: "+game.MinBet+", Max: "+game.MaxBet+"</small></p>"+
								"	</div>"+
								"</div>"
							);
						});
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					//bootbox.alert("Unexpected Error!");
				}
			});
		});
	</script>
	
	<style>
		.casino_game_type{
			background:#595959;
			background:-moz-linear-gradient(center top,#595959 0%,#1a1a1a 100%);
			-pie-background:linear-gradient(center top,#595959 0%,#1a1a1a 100%);
			background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#595959),color-stop(1,#1a1a1a));
			background-image:-o-linear-gradient(top,#595959 0%,#1a1a1a 100%);
			background:-ms-linear-gradient(center top,#595959 0%,#1a1a1a 100%);
			background:linear-gradient(center top,#595959 0%,#1a1a1a 100%);
			position:relative;
			color:#fff;
			margin:0px;
			overflow:hidden;
			border-bottom: 1px solid #8c8c8c;
		}
		
		.casino_game_inner:hover{
			outline: none;
			box-shadow: 0 0 10px #FBB4F0;
		}

		.casino_game_type li{
			display:inline-block;
			position:relative;
			line-height: 30px;
			padding:5px 20px 4px 20px;
			border-right:1px #8c8c8c solid;
			border-bottom:2px transparent solid;
			text-decoration:none;
			text-align:center;
			cursor:pointer;
			-webkit-transform:skew(25deg);
			-moz-transform:skew(25deg);
			-moz-transform:skewX(25deg);
			-ms-transform:skew(25deg);
			-o-transform:skew(25deg);
			transform:skew(0deg);
			outline:none!important;
		}

		.casino_game_type li.active{
			background:-moz-linear-gradient(center top,#212121 50%,#383e28 100%);
			background:-webkit-gradient(linear,left top,left bottom,color-stop(0.5,#212121),color-stop(1,#383e28));
			background:-o-linear-gradient(top,#212121 50%,#383e28 100%);
			background:-ms-linear-gradient(top,#212121 50%,#383e28 100%);
			background:linear-gradient(top,#212121 50%,#383e28 100%);
			border-right: #8c8c8c 1px solid;
			border-bottom: #FBB4F0 2px solid;
			margin-left: -5px;
			padding-right: 25px;
		}

		.casino_game_type li span{
			font-size:12px;
			font-weight:normal;
			color: white;
			display:block;
			text-decoration:none;
			-webkit-transform:skew(-25deg);
			-moz-transform:skew(-25deg);
			-moz-transform:skewX(-25deg);
			-ms-transform:skew(-25deg);
			-o-transform:skew(-25deg);
			transform:skew(0deg);
			outline:none!important;
			-moz-user-select: none;
			-khtml-user-select: none;
			-webkit-user-select: none;
			-o-user-select: none;
			user-select: none;
		}

		.casino_game_type li.active span, .casino_game_type li span:hover{
			color: #FBB4F0;
			-moz-user-select: none;
			-khtml-user-select: none;
			-webkit-user-select: none;
			-o-user-select: none;
			user-select: none;
		}

		.casino_game_type li.active span{
			font-weight:bold;
			-moz-user-select: none;
			-khtml-user-select: none;
			-webkit-user-select: none;
			-o-user-select: none;
			user-select: none;
		}
		
		.new_ribbon{
			background: url('/images/casino/new_ribbon.png');
			background-size: 80px 80px;
			background-repeat: no-repeat;
			position: absolute;
			top: -2px;
			right: -1px;
			margin: 0;
			width: 80px;
			height: 80px;
			z-index: 999;
		}
		
		.featured_ribbon{
			background: url('/images/casino/featured_ribbon.png');
			background-size: 80px 80px;
			background-repeat: no-repeat;
			position: absolute;
			top: -2px;
			left: -1px;
			margin: 0;
			width: 80px;
			height: 80px;
			z-index: 999;
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#slider_photo_btn').click(function(){
				$("#slider_pic_modal").modal("show");
			});
		});
	</script>
<?php
			if($session->userlevel >= 9){
				echo '
		<div style="text-align: center;">
			<button class="btn btn-primary" id="slider_photo_btn" style="margin: 5px;">Change Photos</button>
		</div>
				';
			}
		?>
	<div class="clearfix" style="background-image: url('/images/purple.jpg');">
		<div class="clearfix">
<div class="header_image">
		<?php
			$files = scandir('images/casino_slider_image/');
			$total = count($files);
			$images = array();
			for($x = 0; $x <= $total; $x++){
				if($files[$x] != '.' && $files[$x] != '..' && is_file('images/casino_slider_image/'.$files[$x])){
					$images[] = $files[$x];
				}
			}
		?>
	
		<div id="carousel-index" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'" class="active"></li>';
						}else{
							echo '<li data-target="#carousel-index" data-slide-to="'.$x.'"></li>';
						}
					}
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php
					for($x = 0; $x <= count($images) - 1; $x++){
						if($x == 0){
							echo '
				<div class="item active">
					<a href="#">
						<img src="/images/casino_slider_image/'.$images[$x].'" class="carousel-image" width="100%" height="222px">
					</a>
				</div>
							';
						}else{
							echo '
				<div class="item">
					<a href="#">
						<img src="/images/casino_slider_image/'.$images[$x].'" class="carousel-image" width="100%" height="222px">
					</a>
				</div>
							';
						}
					}
				?>
			</div>
		 
				</div>
			</div>
		</div>
		<?php
		$settings = array();
		$q = "SELECT * FROM `settings`";
		$settings_info = $db->query($q);
		foreach($settings_info as $setting){
			$settings[$setting['setting']] = $setting['value'];
		}
if($settings['casino_enabled'] == 1){
?>
		   <!--<div class="header_image">
				<img src="/images/casino_header.jpg" class="img_grow_wide">
			</div>-->

			<ul class="casino_game_type">
				<div class="col-sm-9">
					<li id="game_type_featured" class="game_type_tab" data-game_type="least_played"><span>FEATURED</span></li>
					<li id="game_type_recent" class="game_type_tab" data-game_type="recent"><span>RECENTLY PLAYED</span></li>
					<li id="game_type_popular" class="game_type_tab" data-game_type="most_played"><span>POPULAR</span></li>
					<li id="game_type_new" class="game_type_tab" data-game_type="new"><span>NEW</span></li>
					<li id="game_type_slots" class="game_type_tab" data-game_type="sl"><span>SLOTS</span></li>
					<li id="game_type_roulette" class="game_type_tab" data-game_type="rl"><span>ROULETTE</span></li>
					<li id="game_type_cards" class="game_type_tab" data-game_type="bjk bac"><span>CARDS</span></li>
					<li id="game_type_favourites" class="game_type_tab" data-game_type="bjk bac"><span>FAVORITES</span></li>
				</div>
				<div class='col-sm-3' style='padding-top: 2.5px;'>
					<div class="input-group">
						<input id='casino_search' class='form-control' type='text' placeholder='Search'>
						<div class="input-group-btn">
							<button class="btn btn-primary" type="button"><i class="glyphicon glyphicon-search"></i></button>
						</div>
					</div>
				</div>
			</ul>
			
			<div class="col-md-9 clearfix" id="casino_games_contain" style="padding: 15px;">
			<div id="msg" style="height: 400px; text-align: center; margin:0;color: #fff;display:none;"><h2><br><br>We're sorry but Casino is currently Unavailable.<br><br>Please check back later.</h2></div>	
			</div>
			
			<div class="col-md-3 clearfix" id="casino_help" style="padding-top: 20px;">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_how_to" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_how_to" aria-expanded="true" aria-controls="accordion_how_to">
									How To Play Slots
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_how_to" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>To begin playing you need to select the game you want to play. You will then get your game within a popup window. Then select the amount of lines from one to nine you wish to play and press spin. The outcome of the reels will determine the payout.</p>
								<p style="text-align: center;">Theoritcal RTP: 97%</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_roulette" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_roulette" aria-expanded="true" aria-controls="accordion_how_to">
									How To Play Roulette
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_roulette" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>The first thing you'll notice when you play roulette online is the wheel. It has 38 numbers, including 1 through 36, a single zero, and a double zero. If you're playing the European version, only 37 numbers appear, as the double zero is absent. You'll also notice that the numbers are not listed consecutively. Instead, they're out of sequence, but they do alternate from black to red, with the exception of the single and double zeroes, which are green.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_place_bets" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_place_bets" aria-expanded="true" aria-controls="accordion_place_bets">
									Placing Bets
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_place_bets" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>You need to select the coin value by clicking on the + sign to increase the bet or on the - sign to decrease it. You also need to select at least one line in order to be able to spin the reel. Each line will increment the amount of the bet's total. The lines can be selected by clicking on the numbers on the left or right side of the reels.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_bet_one" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_bet_one" aria-expanded="true" aria-controls="accordion_bet_one">
									Bet One
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_bet_one" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>This button will select one line each time it is pressed, once you have the lines you wish to bet you may press the Spin button.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_bet_max" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_bet_max" aria-expanded="true" aria-controls="accordion_bet_max">
									Bet Max
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_bet_max" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>This button will select all the lines and automatically spin the reels.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_total_bet" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_total_bet" aria-expanded="true" aria-controls="accordion_total_bet">
									Total Bet
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_total_bet" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>This field indicates the total amount of money at risk during a spin.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_spinning_reels" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_spinning_reels" aria-expanded="true" aria-controls="accordion_spinning_reels">
									Spinning Reels
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_spinning_reels" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>Each time a spin is made, the five reels come to rest randomly with a selection of symbols showing in the game window. Certain combinations of symbols results in payoffs. Consult the payout chart for reference to the amounts paid.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_playouts" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_playouts" aria-expanded="true" aria-controls="accordion_playouts">
									Payouts
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_playouts" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>The payouts table shows the different winning combinations. You may access it anytime during your game by clicking on the Payout Table button on the bottom left side of the slot machine's console.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_free_spins" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_free_spins" aria-expanded="true" aria-controls="accordion_free_spins">
									Free Spins
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_free_spins" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>Each time the scatter symbol comes up three times or more you will get free spins. It is important to point out that if you win free spins in specific coin / line denomination, you will play your free spins in that same layout. This is why you might notice that the coin values and lines selected change when you play free spins that have accumulated.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_win" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_win" aria-expanded="true" aria-controls="accordion_win">
									Win
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_win" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>The Win box displays the amount of money you've won if you hit a winning combination.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_credits" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_credits" aria-expanded="true" aria-controls="accordion_credits">
									Credits
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_credits" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>The Credits box displays the amount of money you currently have available in your casino account. (The amount shown actively changes as you play</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_cashier_button" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_cashier_button" aria-expanded="true" aria-controls="accordion_cashier_button">
									Cashier Button
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_cashier_button" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>The cashier button takes you to the cashier, where you can transfer chips to and from the casino. It will not be enabled at all times; however-it will be operational when no game is being played.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading cursor-pointer" role="tab" id="heading_tasks" data-toggle="collapse" data-target="#accordion_exit_game" data-parent="#accordion">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#accordion_exit_game" aria-expanded="true" aria-controls="accordion_exit_game">
									Exit Game Button
								</a>
								<span style="margin-top: 7px;" class="caret pull-right"></span>
							</h4>
						</div>
						<div id="accordion_exit_game" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tasks">
							<div class="panel-body">
								<p>The exit button leaves the game and returns to the lobby. It will not be enabled at all times, however-it will be operational when no game is being played.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="launch_game_modal" tabindex="-1" role="dialog" aria-labelledby="launch_game_label" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<img class="pull-left" src='/images/asurewin_chip.png' style="height: 30px; display: inline-block;">
					<h4 class="modal-title col-sm-9" id="launch_game_label">Casino Game</h4>
					<span class='pull-right'>
						<button id="go_fullscreen" class="btn btn-primary">
							<span class="glyphicon glyphicon-fullscreen"></span> Fullscreen
						</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					</span>
				</div>
				<div id="iframe_contain" class="modal-body" style="position: relative;">
					<!-- <div class="close" data-dismiss="modal" id="close_overlay" style="width: 40px; height: 40px; position: absolute; right: 15px; top: 15px;"></div> -->
					<iframe id="modal_iframe" src="" width="100%" height="600px" frameborder="0" allowtransparency="true" allowfullscreen="true" webkitallowfullscreen ="true" mozallowfullscreen="true"></iframe>  
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
<!-- Modal for Slider Pic Selection -->
	<div class="modal fade" id="slider_pic_modal" tabindex="-1" role="dialog" aria-labelledby="slider_pic_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="slider_pic_modal_label">Change Slider Photos</h4>

					<div id="customer_name2" class="pull-right" style="padding-right: 6px;">
						
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div id="upload_photos" class="col-sm-12">
						<?php include($_SERVER['DOCUMENT_ROOT'].'/upload_slider_photos.php'); ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>	
<?php
	}
	else{
	echo "<div class='well' style='height: 400px; text-align: center; margin:0;'><h2><br><br>We're sorry but Casino is currently Unavailable.<br><br>Please check back later.</h2></div>";
	}
	}
?>

<?php	

	include("footer.php");
?>
