<?php
	$page_title = "Top ups";
	
	$top_left_fixed_img = "";
	$top_right_fixed_img = "<img src='/images/cards_rgt.png'>";

	include("header.php");
	include('page_top.php');

        require_once($_SERVER['DOCUMENT_ROOT']."/api/emida/api.php");
	
	$phone_card_api = new PHONE_CARD_API();
	
	$phone_card_api->login2();	
	$carriers = $phone_card_api->get_carrier_list();
	
	
        
     

      

?>

<script>
    $(document).ready(function(){
        $("#carrier_id").change(function () {
            var selectedText = $(this).find("option:selected").text();
            var selectedValue = $(this).val();
            window.location.href = "top_ups.php?car_name=" + selectedValue;
            
        });
        $("#category_id").change(function () {
            var selectedValue =$("#carrier_id").val();
            var selectedText1 = $(this).find("option:selected").text();
            var selectedValue1 = $(this).val();
           
            window.location.href = "top_ups.php?car_name=" + selectedValue + "&cat_name=" + selectedValue1;
            
        });
    });
</script>
<script>
	$(document).ready(function(){	
	      $(document).on('change','#carrier_id',function(){
                     $.ajax({
					url: "/ajax/top_ups.php",
					type: "POST",
					data: {
						action : "get_product",
						carrier_id : $(this).val()
					},
					dataType: "json",
					success: function(data){
						$("#voucher").val("");
						bootbox.alert(data.message);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						bootbox.alert("Something went wrong. Please try again later.", function(){ 
							window.location.reload();
						});
					}
				});

              });
	});
	
	$(document).on("click", "#phone_card_confirm", function(){
	
		$.ajax({ url: '/api/emida/index.php',
         	data: {action: $('#product_id').val() ,
         		phone_number: $('#phone_number').val(),
         		amount:$('#amount').val()},
         	dataType: 'json',
         	type: 'post',
         	success: function(success) {
                     
                      if(success.pro=='8000000'){
		              $("#phone_card_modal").modal("show");                     
		              $("#pin_number").text(success.Pin);
		              $("#amount1").text("$20");
		              $('#ResponseMessage').text(success.ResponseMessage);
                      }
                      else{
                      	      $("#phone_card_modal").modal("show");                     
		              $(".lab").hide();
		              $('#ResponseMessage').text("Your Topup is Successfully Done.");
                      }
                  }
});
		
	});
</script>
	
        <div class="header_image">
		<img src="/images/winning_hdd.jpg?v=<?=IMAGE_VERSION?>" class="img_grow_wide">
	</div>
<?php
	// get current settings from db
	$settings = array();
	$q = "SELECT * FROM `settings`";
	$settings_info = $db->query($q);
	foreach($settings_info as $setting){
		$settings[$setting['setting']] = $setting['value'];
	}
	
	// see if the game is disabled
	if($settings['topup_enabled'] == 0){
		echo "<div class='well' style='height: 400px; text-align: center; margin:0;'><h2><br><br>We're sorry currently Topup is Unavailable.<br><br>Please check back later.</h2></div>";
		include("footer.php");
		die();
	}
	else{
	?>
			<div class="win-bg-plain">
			     <div class="col-md-12">
				<div class="panel panel-default">
				     <div class="panel-heading clearfix">
					<h3 class="panel-title pull-left" style="padding-top: 7.5px;">Top ups</h3>
				     </div>
				    <div class="panel-body">
                                        <div class="col-sm-4">								
					    <div class="form-group">
						<label for="carrier_id">Select Carrier</label>
						  <select name="carrier_id" id="carrier_id" class="form-control selectpicker">
                                                      <option value="-1">--SELECT--</option>
                                                      <?php
                                                          if(!empty($carriers)){
                                                             foreach($carriers as $carrier){?>
                                                                 <option value="<?php echo $carrier['CarrierId'];?>" <?php if($_GET['car_name'] == $carrier['CarrierId']) { echo 'selected="selected"'; }?> ><?php echo $carrier['Description'];?></option>
                                                                 <?php
                                                             }
                                                          }
                                                      ?>
                                                 </select>	
					    </div>	
					</div>	
                                        <div class="col-sm-4">								
					    <div class="form-group">
						<label for="product_id">Select Category</label>
						  <select name="category_id" id="category_id" class="form-control selectpicker">
                                                      <option value="-1">--SELECT--</option>
                                                      <?php
                                                      
				$categories = $phone_card_api->get_category_list("",$_GET['car_name']);
				foreach($categories as $cat){?>
					<option value="<?php echo $cat['CategoryId'];?>" <?php if($_GET['cat_name'] == $cat['CategoryId']) { echo 'selected="selected"'; }?>><?php echo $cat['Description'];?></option>
					<?php	
					}
                 					
                 				?>
                                                 </select>	
					    </div>	
					</div>
					 <div class="col-sm-4">								
					    <div class="form-group">
						<label for="product_id">Select Product</label>
						  <select name="product_id" id="product_id" class="form-control selectpicker">
                                                      <option value="-1">--SELECT--</option>
                                                      <?php                                                      
				$products = $phone_card_api->get_product_list("", $_GET['car_name'], $_GET['cat_name']);
				foreach($products as $product){?>
				<option value="<?php echo $product['ProductId'];?>"><?php echo $product['ShortDescription'];?></option>	
				<?php			
				}
                 		?>
                                                 </select>	
					    </div>	
					</div>
					<?php if($_GET['cat_name']=='4'){?>
					 <div class="col-md-12">
					 	<div class="col-md-4"> </div>
					 <div class="col-md-4">Add Phone Number : </div>
					 <div class="col-md-4"> <div class="form-group"><input type="text" class="form-control" placeholder="Phone Number" name="phone_number" id="phone_number"></div></div>
					 </div>
					  <div class="col-md-12">
					 	<div class="col-md-4"> </div>
					 <div class="col-md-4">Add Amount : </div>
					 <div class="col-md-4"> <div class="form-group"><input type="text" class="form-control" placeholder="Amount" name="amount" id="amount"></div></div>
					 </div>
					 <?php }?>
					 <div class="col-md-offset-10">
				
				<button type="button" class="btn btn-primary" id="phone_card_confirm">Purchase Phone Card</button>
			
		</div>	
					 </div>

				<!--<button type="button" class="btn btn-default" id="cancel_replay_ticket" data-dismiss="modal">Cancel</button>-->					
    				   </div>
				</div>
			     </div>
                        </div>
<div class="modal fade" id="phone_card_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="phone_card_modal_label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="phone_card_modal_label">Phone Cards</h4>
			</div>
			<div class="modal-body clearfix">
			<div class="col-md-12">
			<div class="col-md-6 lab" style="text-align: center;"><label>Pin : </label><label id="pin_number"></label></div>
			<div class="col-md-6 lab" style="text-align: center;"><label>Amount : </label><label id="amount1"></label></div>
			</div>
			<div class="col-md-12">
			<label id="ResponseMessage"></label>
			</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="cancel_replay_ticket" data-dismiss="modal">Cancel</button>			
			</div>
		</div>
	</div>
</div>
        <!-- Modal for Slider Pic Selection -->
	<div class="modal fade" id="slider_pic_modal" tabindex="-1" role="dialog" aria-labelledby="slider_pic_modal_label" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header clearfix">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title pull-left" id="slider_pic_modal_label">Change Slider Photos</h4>

					<div id="customer_name2" class="pull-right" style="padding-right: 6px;">
					</div>
					
					<div id="modal_row_id" style="display: none;">
						
					</div>
				</div>
				<div class="modal-body clearfix">
					<div id="upload_photos" class="col-sm-12">
						<?php include($_SERVER['DOCUMENT_ROOT'].'/upload_slider_photos.php'); ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<?php
	include("footer.php");
	}
?>
